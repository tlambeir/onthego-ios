﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_SampleController2879308770.h"
#include "UnityEngine_UnityEngine_Color2555686324.h"
#include "WikitudeUnityPlugin_Wikitude_InstantTrackingState3973410783.h"

// UnityEngine.GameObject
struct GameObject_t1113636619;
// UnityEngine.UI.Text
struct Text_t1901882714;
// Wikitude.InstantTracker
struct InstantTracker_t684352120;
// UnityEngine.UI.Image
struct Image_t2670269651;
// MoveController
struct MoveController_t1157877467;
// GridRenderer
struct GridRenderer_t1964421403;
// System.Collections.Generic.HashSet`1<UnityEngine.GameObject>
struct HashSet_1_t3973553389;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// wikitudeController
struct  wikitudeController_t1069208782  : public SampleController_t2879308770
{
public:
	// UnityEngine.GameObject wikitudeController::ButtonDock
	GameObject_t1113636619 * ___ButtonDock_2;
	// UnityEngine.GameObject wikitudeController::InitializationControls
	GameObject_t1113636619 * ___InitializationControls_3;
	// UnityEngine.UI.Text wikitudeController::HeightLabel
	Text_t1901882714 * ___HeightLabel_4;
	// UnityEngine.UI.Text wikitudeController::ScaleLabel
	Text_t1901882714 * ___ScaleLabel_5;
	// Wikitude.InstantTracker wikitudeController::Tracker
	InstantTracker_t684352120 * ___Tracker_6;
	// UnityEngine.UI.Image wikitudeController::ActivityIndicator
	Image_t2670269651 * ___ActivityIndicator_7;
	// UnityEngine.Color wikitudeController::EnabledColor
	Color_t2555686324  ___EnabledColor_8;
	// UnityEngine.Color wikitudeController::DisabledColor
	Color_t2555686324  ___DisabledColor_9;
	// System.Single wikitudeController::_currentDeviceHeightAboveGround
	float ____currentDeviceHeightAboveGround_10;
	// MoveController wikitudeController::_moveController
	MoveController_t1157877467 * ____moveController_11;
	// GridRenderer wikitudeController::_gridRenderer
	GridRenderer_t1964421403 * ____gridRenderer_12;
	// System.Collections.Generic.HashSet`1<UnityEngine.GameObject> wikitudeController::_activeModels
	HashSet_1_t3973553389 * ____activeModels_13;
	// Wikitude.InstantTrackingState wikitudeController::_currentState
	int32_t ____currentState_14;
	// System.Boolean wikitudeController::_isTracking
	bool ____isTracking_15;
	// UnityEngine.GameObject wikitudeController::modelPrefabToLoad
	GameObject_t1113636619 * ___modelPrefabToLoad_16;

public:
	inline static int32_t get_offset_of_ButtonDock_2() { return static_cast<int32_t>(offsetof(wikitudeController_t1069208782, ___ButtonDock_2)); }
	inline GameObject_t1113636619 * get_ButtonDock_2() const { return ___ButtonDock_2; }
	inline GameObject_t1113636619 ** get_address_of_ButtonDock_2() { return &___ButtonDock_2; }
	inline void set_ButtonDock_2(GameObject_t1113636619 * value)
	{
		___ButtonDock_2 = value;
		Il2CppCodeGenWriteBarrier(&___ButtonDock_2, value);
	}

	inline static int32_t get_offset_of_InitializationControls_3() { return static_cast<int32_t>(offsetof(wikitudeController_t1069208782, ___InitializationControls_3)); }
	inline GameObject_t1113636619 * get_InitializationControls_3() const { return ___InitializationControls_3; }
	inline GameObject_t1113636619 ** get_address_of_InitializationControls_3() { return &___InitializationControls_3; }
	inline void set_InitializationControls_3(GameObject_t1113636619 * value)
	{
		___InitializationControls_3 = value;
		Il2CppCodeGenWriteBarrier(&___InitializationControls_3, value);
	}

	inline static int32_t get_offset_of_HeightLabel_4() { return static_cast<int32_t>(offsetof(wikitudeController_t1069208782, ___HeightLabel_4)); }
	inline Text_t1901882714 * get_HeightLabel_4() const { return ___HeightLabel_4; }
	inline Text_t1901882714 ** get_address_of_HeightLabel_4() { return &___HeightLabel_4; }
	inline void set_HeightLabel_4(Text_t1901882714 * value)
	{
		___HeightLabel_4 = value;
		Il2CppCodeGenWriteBarrier(&___HeightLabel_4, value);
	}

	inline static int32_t get_offset_of_ScaleLabel_5() { return static_cast<int32_t>(offsetof(wikitudeController_t1069208782, ___ScaleLabel_5)); }
	inline Text_t1901882714 * get_ScaleLabel_5() const { return ___ScaleLabel_5; }
	inline Text_t1901882714 ** get_address_of_ScaleLabel_5() { return &___ScaleLabel_5; }
	inline void set_ScaleLabel_5(Text_t1901882714 * value)
	{
		___ScaleLabel_5 = value;
		Il2CppCodeGenWriteBarrier(&___ScaleLabel_5, value);
	}

	inline static int32_t get_offset_of_Tracker_6() { return static_cast<int32_t>(offsetof(wikitudeController_t1069208782, ___Tracker_6)); }
	inline InstantTracker_t684352120 * get_Tracker_6() const { return ___Tracker_6; }
	inline InstantTracker_t684352120 ** get_address_of_Tracker_6() { return &___Tracker_6; }
	inline void set_Tracker_6(InstantTracker_t684352120 * value)
	{
		___Tracker_6 = value;
		Il2CppCodeGenWriteBarrier(&___Tracker_6, value);
	}

	inline static int32_t get_offset_of_ActivityIndicator_7() { return static_cast<int32_t>(offsetof(wikitudeController_t1069208782, ___ActivityIndicator_7)); }
	inline Image_t2670269651 * get_ActivityIndicator_7() const { return ___ActivityIndicator_7; }
	inline Image_t2670269651 ** get_address_of_ActivityIndicator_7() { return &___ActivityIndicator_7; }
	inline void set_ActivityIndicator_7(Image_t2670269651 * value)
	{
		___ActivityIndicator_7 = value;
		Il2CppCodeGenWriteBarrier(&___ActivityIndicator_7, value);
	}

	inline static int32_t get_offset_of_EnabledColor_8() { return static_cast<int32_t>(offsetof(wikitudeController_t1069208782, ___EnabledColor_8)); }
	inline Color_t2555686324  get_EnabledColor_8() const { return ___EnabledColor_8; }
	inline Color_t2555686324 * get_address_of_EnabledColor_8() { return &___EnabledColor_8; }
	inline void set_EnabledColor_8(Color_t2555686324  value)
	{
		___EnabledColor_8 = value;
	}

	inline static int32_t get_offset_of_DisabledColor_9() { return static_cast<int32_t>(offsetof(wikitudeController_t1069208782, ___DisabledColor_9)); }
	inline Color_t2555686324  get_DisabledColor_9() const { return ___DisabledColor_9; }
	inline Color_t2555686324 * get_address_of_DisabledColor_9() { return &___DisabledColor_9; }
	inline void set_DisabledColor_9(Color_t2555686324  value)
	{
		___DisabledColor_9 = value;
	}

	inline static int32_t get_offset_of__currentDeviceHeightAboveGround_10() { return static_cast<int32_t>(offsetof(wikitudeController_t1069208782, ____currentDeviceHeightAboveGround_10)); }
	inline float get__currentDeviceHeightAboveGround_10() const { return ____currentDeviceHeightAboveGround_10; }
	inline float* get_address_of__currentDeviceHeightAboveGround_10() { return &____currentDeviceHeightAboveGround_10; }
	inline void set__currentDeviceHeightAboveGround_10(float value)
	{
		____currentDeviceHeightAboveGround_10 = value;
	}

	inline static int32_t get_offset_of__moveController_11() { return static_cast<int32_t>(offsetof(wikitudeController_t1069208782, ____moveController_11)); }
	inline MoveController_t1157877467 * get__moveController_11() const { return ____moveController_11; }
	inline MoveController_t1157877467 ** get_address_of__moveController_11() { return &____moveController_11; }
	inline void set__moveController_11(MoveController_t1157877467 * value)
	{
		____moveController_11 = value;
		Il2CppCodeGenWriteBarrier(&____moveController_11, value);
	}

	inline static int32_t get_offset_of__gridRenderer_12() { return static_cast<int32_t>(offsetof(wikitudeController_t1069208782, ____gridRenderer_12)); }
	inline GridRenderer_t1964421403 * get__gridRenderer_12() const { return ____gridRenderer_12; }
	inline GridRenderer_t1964421403 ** get_address_of__gridRenderer_12() { return &____gridRenderer_12; }
	inline void set__gridRenderer_12(GridRenderer_t1964421403 * value)
	{
		____gridRenderer_12 = value;
		Il2CppCodeGenWriteBarrier(&____gridRenderer_12, value);
	}

	inline static int32_t get_offset_of__activeModels_13() { return static_cast<int32_t>(offsetof(wikitudeController_t1069208782, ____activeModels_13)); }
	inline HashSet_1_t3973553389 * get__activeModels_13() const { return ____activeModels_13; }
	inline HashSet_1_t3973553389 ** get_address_of__activeModels_13() { return &____activeModels_13; }
	inline void set__activeModels_13(HashSet_1_t3973553389 * value)
	{
		____activeModels_13 = value;
		Il2CppCodeGenWriteBarrier(&____activeModels_13, value);
	}

	inline static int32_t get_offset_of__currentState_14() { return static_cast<int32_t>(offsetof(wikitudeController_t1069208782, ____currentState_14)); }
	inline int32_t get__currentState_14() const { return ____currentState_14; }
	inline int32_t* get_address_of__currentState_14() { return &____currentState_14; }
	inline void set__currentState_14(int32_t value)
	{
		____currentState_14 = value;
	}

	inline static int32_t get_offset_of__isTracking_15() { return static_cast<int32_t>(offsetof(wikitudeController_t1069208782, ____isTracking_15)); }
	inline bool get__isTracking_15() const { return ____isTracking_15; }
	inline bool* get_address_of__isTracking_15() { return &____isTracking_15; }
	inline void set__isTracking_15(bool value)
	{
		____isTracking_15 = value;
	}

	inline static int32_t get_offset_of_modelPrefabToLoad_16() { return static_cast<int32_t>(offsetof(wikitudeController_t1069208782, ___modelPrefabToLoad_16)); }
	inline GameObject_t1113636619 * get_modelPrefabToLoad_16() const { return ___modelPrefabToLoad_16; }
	inline GameObject_t1113636619 ** get_address_of_modelPrefabToLoad_16() { return &___modelPrefabToLoad_16; }
	inline void set_modelPrefabToLoad_16(GameObject_t1113636619 * value)
	{
		___modelPrefabToLoad_16 = value;
		Il2CppCodeGenWriteBarrier(&___modelPrefabToLoad_16, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
