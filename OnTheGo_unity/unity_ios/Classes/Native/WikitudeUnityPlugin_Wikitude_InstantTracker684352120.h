﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "WikitudeUnityPlugin_Wikitude_TrackerBehaviour921360922.h"
#include "WikitudeUnityPlugin_Wikitude_InstantTrackingPlaneO1439566444.h"
#include "WikitudeUnityPlugin_Wikitude_InstantTrackingState3973410783.h"

// Wikitude.InstantTracker/OnStateChangedEvent
struct OnStateChangedEvent_t1359267737;
// Wikitude.InstantTracker/OnScreenConversionComputedEvent
struct OnScreenConversionComputedEvent_t608500589;
// Wikitude.InstantTracker/OnPointCloudRequestFinishedEvent
struct OnPointCloudRequestFinishedEvent_t2541379958;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Wikitude.InstantTracker
struct  InstantTracker_t684352120  : public TrackerBehaviour_t921360922
{
public:
	// Wikitude.InstantTracker/OnStateChangedEvent Wikitude.InstantTracker::OnStateChanged
	OnStateChangedEvent_t1359267737 * ___OnStateChanged_15;
	// System.Single Wikitude.InstantTracker::_deviceHeightAboveGround
	float ____deviceHeightAboveGround_16;
	// Wikitude.InstantTracker/OnScreenConversionComputedEvent Wikitude.InstantTracker::OnScreenConversionComputed
	OnScreenConversionComputedEvent_t608500589 * ___OnScreenConversionComputed_17;
	// Wikitude.InstantTracker/OnPointCloudRequestFinishedEvent Wikitude.InstantTracker::OnPointCloudRequestFinished
	OnPointCloudRequestFinishedEvent_t2541379958 * ___OnPointCloudRequestFinished_18;
	// Wikitude.InstantTrackingPlaneOrientation Wikitude.InstantTracker::_trackingPlaneOrientation
	int32_t ____trackingPlaneOrientation_19;
	// System.Single Wikitude.InstantTracker::_trackingPlaneOrientationAngle
	float ____trackingPlaneOrientationAngle_20;
	// System.Boolean Wikitude.InstantTracker::_pointCloudRequestPending
	bool ____pointCloudRequestPending_21;
	// Wikitude.InstantTrackingState Wikitude.InstantTracker::CurrentState
	int32_t ___CurrentState_22;

public:
	inline static int32_t get_offset_of_OnStateChanged_15() { return static_cast<int32_t>(offsetof(InstantTracker_t684352120, ___OnStateChanged_15)); }
	inline OnStateChangedEvent_t1359267737 * get_OnStateChanged_15() const { return ___OnStateChanged_15; }
	inline OnStateChangedEvent_t1359267737 ** get_address_of_OnStateChanged_15() { return &___OnStateChanged_15; }
	inline void set_OnStateChanged_15(OnStateChangedEvent_t1359267737 * value)
	{
		___OnStateChanged_15 = value;
		Il2CppCodeGenWriteBarrier(&___OnStateChanged_15, value);
	}

	inline static int32_t get_offset_of__deviceHeightAboveGround_16() { return static_cast<int32_t>(offsetof(InstantTracker_t684352120, ____deviceHeightAboveGround_16)); }
	inline float get__deviceHeightAboveGround_16() const { return ____deviceHeightAboveGround_16; }
	inline float* get_address_of__deviceHeightAboveGround_16() { return &____deviceHeightAboveGround_16; }
	inline void set__deviceHeightAboveGround_16(float value)
	{
		____deviceHeightAboveGround_16 = value;
	}

	inline static int32_t get_offset_of_OnScreenConversionComputed_17() { return static_cast<int32_t>(offsetof(InstantTracker_t684352120, ___OnScreenConversionComputed_17)); }
	inline OnScreenConversionComputedEvent_t608500589 * get_OnScreenConversionComputed_17() const { return ___OnScreenConversionComputed_17; }
	inline OnScreenConversionComputedEvent_t608500589 ** get_address_of_OnScreenConversionComputed_17() { return &___OnScreenConversionComputed_17; }
	inline void set_OnScreenConversionComputed_17(OnScreenConversionComputedEvent_t608500589 * value)
	{
		___OnScreenConversionComputed_17 = value;
		Il2CppCodeGenWriteBarrier(&___OnScreenConversionComputed_17, value);
	}

	inline static int32_t get_offset_of_OnPointCloudRequestFinished_18() { return static_cast<int32_t>(offsetof(InstantTracker_t684352120, ___OnPointCloudRequestFinished_18)); }
	inline OnPointCloudRequestFinishedEvent_t2541379958 * get_OnPointCloudRequestFinished_18() const { return ___OnPointCloudRequestFinished_18; }
	inline OnPointCloudRequestFinishedEvent_t2541379958 ** get_address_of_OnPointCloudRequestFinished_18() { return &___OnPointCloudRequestFinished_18; }
	inline void set_OnPointCloudRequestFinished_18(OnPointCloudRequestFinishedEvent_t2541379958 * value)
	{
		___OnPointCloudRequestFinished_18 = value;
		Il2CppCodeGenWriteBarrier(&___OnPointCloudRequestFinished_18, value);
	}

	inline static int32_t get_offset_of__trackingPlaneOrientation_19() { return static_cast<int32_t>(offsetof(InstantTracker_t684352120, ____trackingPlaneOrientation_19)); }
	inline int32_t get__trackingPlaneOrientation_19() const { return ____trackingPlaneOrientation_19; }
	inline int32_t* get_address_of__trackingPlaneOrientation_19() { return &____trackingPlaneOrientation_19; }
	inline void set__trackingPlaneOrientation_19(int32_t value)
	{
		____trackingPlaneOrientation_19 = value;
	}

	inline static int32_t get_offset_of__trackingPlaneOrientationAngle_20() { return static_cast<int32_t>(offsetof(InstantTracker_t684352120, ____trackingPlaneOrientationAngle_20)); }
	inline float get__trackingPlaneOrientationAngle_20() const { return ____trackingPlaneOrientationAngle_20; }
	inline float* get_address_of__trackingPlaneOrientationAngle_20() { return &____trackingPlaneOrientationAngle_20; }
	inline void set__trackingPlaneOrientationAngle_20(float value)
	{
		____trackingPlaneOrientationAngle_20 = value;
	}

	inline static int32_t get_offset_of__pointCloudRequestPending_21() { return static_cast<int32_t>(offsetof(InstantTracker_t684352120, ____pointCloudRequestPending_21)); }
	inline bool get__pointCloudRequestPending_21() const { return ____pointCloudRequestPending_21; }
	inline bool* get_address_of__pointCloudRequestPending_21() { return &____pointCloudRequestPending_21; }
	inline void set__pointCloudRequestPending_21(bool value)
	{
		____pointCloudRequestPending_21 = value;
	}

	inline static int32_t get_offset_of_CurrentState_22() { return static_cast<int32_t>(offsetof(InstantTracker_t684352120, ___CurrentState_22)); }
	inline int32_t get_CurrentState_22() const { return ___CurrentState_22; }
	inline int32_t* get_address_of_CurrentState_22() { return &___CurrentState_22; }
	inline void set_CurrentState_22(int32_t value)
	{
		___CurrentState_22 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
