﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour3962482529.h"

// UnityEngine.KeyCode[]
struct KeyCodeU5BU5D_t2223234056;
// System.String
struct String_t;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t1632706988;
// System.Collections.Generic.List`1<System.String>
struct List_1_t3319525431;
// UnityEngine.UI.Text
struct Text_t1901882714;
// UnityEngine.UI.Dropdown
struct Dropdown_t2274391225;
// UnityEngine.UI.Button
struct Button_t4055032469;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SceneLoader
struct  SceneLoader_t4130533360  : public MonoBehaviour_t3962482529
{
public:
	// System.Collections.Generic.List`1<System.String> SceneLoader::sceneNames
	List_1_t3319525431 * ___sceneNames_7;
	// UnityEngine.UI.Text SceneLoader::title
	Text_t1901882714 * ___title_8;
	// UnityEngine.UI.Text SceneLoader::description
	Text_t1901882714 * ___description_9;
	// UnityEngine.UI.Dropdown SceneLoader::dropdown
	Dropdown_t2274391225 * ___dropdown_10;
	// UnityEngine.UI.Button SceneLoader::previous
	Button_t4055032469 * ___previous_11;
	// UnityEngine.UI.Button SceneLoader::next
	Button_t4055032469 * ___next_12;

public:
	inline static int32_t get_offset_of_sceneNames_7() { return static_cast<int32_t>(offsetof(SceneLoader_t4130533360, ___sceneNames_7)); }
	inline List_1_t3319525431 * get_sceneNames_7() const { return ___sceneNames_7; }
	inline List_1_t3319525431 ** get_address_of_sceneNames_7() { return &___sceneNames_7; }
	inline void set_sceneNames_7(List_1_t3319525431 * value)
	{
		___sceneNames_7 = value;
		Il2CppCodeGenWriteBarrier(&___sceneNames_7, value);
	}

	inline static int32_t get_offset_of_title_8() { return static_cast<int32_t>(offsetof(SceneLoader_t4130533360, ___title_8)); }
	inline Text_t1901882714 * get_title_8() const { return ___title_8; }
	inline Text_t1901882714 ** get_address_of_title_8() { return &___title_8; }
	inline void set_title_8(Text_t1901882714 * value)
	{
		___title_8 = value;
		Il2CppCodeGenWriteBarrier(&___title_8, value);
	}

	inline static int32_t get_offset_of_description_9() { return static_cast<int32_t>(offsetof(SceneLoader_t4130533360, ___description_9)); }
	inline Text_t1901882714 * get_description_9() const { return ___description_9; }
	inline Text_t1901882714 ** get_address_of_description_9() { return &___description_9; }
	inline void set_description_9(Text_t1901882714 * value)
	{
		___description_9 = value;
		Il2CppCodeGenWriteBarrier(&___description_9, value);
	}

	inline static int32_t get_offset_of_dropdown_10() { return static_cast<int32_t>(offsetof(SceneLoader_t4130533360, ___dropdown_10)); }
	inline Dropdown_t2274391225 * get_dropdown_10() const { return ___dropdown_10; }
	inline Dropdown_t2274391225 ** get_address_of_dropdown_10() { return &___dropdown_10; }
	inline void set_dropdown_10(Dropdown_t2274391225 * value)
	{
		___dropdown_10 = value;
		Il2CppCodeGenWriteBarrier(&___dropdown_10, value);
	}

	inline static int32_t get_offset_of_previous_11() { return static_cast<int32_t>(offsetof(SceneLoader_t4130533360, ___previous_11)); }
	inline Button_t4055032469 * get_previous_11() const { return ___previous_11; }
	inline Button_t4055032469 ** get_address_of_previous_11() { return &___previous_11; }
	inline void set_previous_11(Button_t4055032469 * value)
	{
		___previous_11 = value;
		Il2CppCodeGenWriteBarrier(&___previous_11, value);
	}

	inline static int32_t get_offset_of_next_12() { return static_cast<int32_t>(offsetof(SceneLoader_t4130533360, ___next_12)); }
	inline Button_t4055032469 * get_next_12() const { return ___next_12; }
	inline Button_t4055032469 ** get_address_of_next_12() { return &___next_12; }
	inline void set_next_12(Button_t4055032469 * value)
	{
		___next_12 = value;
		Il2CppCodeGenWriteBarrier(&___next_12, value);
	}
};

struct SceneLoader_t4130533360_StaticFields
{
public:
	// UnityEngine.KeyCode[] SceneLoader::keysNext
	KeyCodeU5BU5D_t2223234056* ___keysNext_2;
	// UnityEngine.KeyCode[] SceneLoader::keysPrev
	KeyCodeU5BU5D_t2223234056* ___keysPrev_3;
	// System.String SceneLoader::descriptionDefault
	String_t* ___descriptionDefault_4;
	// System.String SceneLoader::descriptionConstant
	String_t* ___descriptionConstant_5;
	// System.Collections.Generic.Dictionary`2<System.String,System.String> SceneLoader::descriptions
	Dictionary_2_t1632706988 * ___descriptions_6;

public:
	inline static int32_t get_offset_of_keysNext_2() { return static_cast<int32_t>(offsetof(SceneLoader_t4130533360_StaticFields, ___keysNext_2)); }
	inline KeyCodeU5BU5D_t2223234056* get_keysNext_2() const { return ___keysNext_2; }
	inline KeyCodeU5BU5D_t2223234056** get_address_of_keysNext_2() { return &___keysNext_2; }
	inline void set_keysNext_2(KeyCodeU5BU5D_t2223234056* value)
	{
		___keysNext_2 = value;
		Il2CppCodeGenWriteBarrier(&___keysNext_2, value);
	}

	inline static int32_t get_offset_of_keysPrev_3() { return static_cast<int32_t>(offsetof(SceneLoader_t4130533360_StaticFields, ___keysPrev_3)); }
	inline KeyCodeU5BU5D_t2223234056* get_keysPrev_3() const { return ___keysPrev_3; }
	inline KeyCodeU5BU5D_t2223234056** get_address_of_keysPrev_3() { return &___keysPrev_3; }
	inline void set_keysPrev_3(KeyCodeU5BU5D_t2223234056* value)
	{
		___keysPrev_3 = value;
		Il2CppCodeGenWriteBarrier(&___keysPrev_3, value);
	}

	inline static int32_t get_offset_of_descriptionDefault_4() { return static_cast<int32_t>(offsetof(SceneLoader_t4130533360_StaticFields, ___descriptionDefault_4)); }
	inline String_t* get_descriptionDefault_4() const { return ___descriptionDefault_4; }
	inline String_t** get_address_of_descriptionDefault_4() { return &___descriptionDefault_4; }
	inline void set_descriptionDefault_4(String_t* value)
	{
		___descriptionDefault_4 = value;
		Il2CppCodeGenWriteBarrier(&___descriptionDefault_4, value);
	}

	inline static int32_t get_offset_of_descriptionConstant_5() { return static_cast<int32_t>(offsetof(SceneLoader_t4130533360_StaticFields, ___descriptionConstant_5)); }
	inline String_t* get_descriptionConstant_5() const { return ___descriptionConstant_5; }
	inline String_t** get_address_of_descriptionConstant_5() { return &___descriptionConstant_5; }
	inline void set_descriptionConstant_5(String_t* value)
	{
		___descriptionConstant_5 = value;
		Il2CppCodeGenWriteBarrier(&___descriptionConstant_5, value);
	}

	inline static int32_t get_offset_of_descriptions_6() { return static_cast<int32_t>(offsetof(SceneLoader_t4130533360_StaticFields, ___descriptions_6)); }
	inline Dictionary_2_t1632706988 * get_descriptions_6() const { return ___descriptions_6; }
	inline Dictionary_2_t1632706988 ** get_address_of_descriptions_6() { return &___descriptions_6; }
	inline void set_descriptions_6(Dictionary_2_t1632706988 * value)
	{
		___descriptions_6 = value;
		Il2CppCodeGenWriteBarrier(&___descriptions_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
