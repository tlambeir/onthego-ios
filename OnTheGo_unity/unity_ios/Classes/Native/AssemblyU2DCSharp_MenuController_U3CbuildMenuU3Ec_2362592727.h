﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// videoManager
struct videoManager_t104452510;
// UnityEngine.GameObject
struct GameObject_t1113636619;
// MenuController/<buildMenu>c__AnonStorey1
struct U3CbuildMenuU3Ec__AnonStorey1_t23940567;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MenuController/<buildMenu>c__AnonStorey0
struct  U3CbuildMenuU3Ec__AnonStorey0_t2362592727  : public Il2CppObject
{
public:
	// videoManager MenuController/<buildMenu>c__AnonStorey0::mVideoManager
	videoManager_t104452510 * ___mVideoManager_0;
	// UnityEngine.GameObject MenuController/<buildMenu>c__AnonStorey0::newMenuItem
	GameObject_t1113636619 * ___newMenuItem_1;
	// UnityEngine.GameObject MenuController/<buildMenu>c__AnonStorey0::mesh
	GameObject_t1113636619 * ___mesh_2;
	// MenuController/<buildMenu>c__AnonStorey1 MenuController/<buildMenu>c__AnonStorey0::<>f__ref$1
	U3CbuildMenuU3Ec__AnonStorey1_t23940567 * ___U3CU3Ef__refU241_3;

public:
	inline static int32_t get_offset_of_mVideoManager_0() { return static_cast<int32_t>(offsetof(U3CbuildMenuU3Ec__AnonStorey0_t2362592727, ___mVideoManager_0)); }
	inline videoManager_t104452510 * get_mVideoManager_0() const { return ___mVideoManager_0; }
	inline videoManager_t104452510 ** get_address_of_mVideoManager_0() { return &___mVideoManager_0; }
	inline void set_mVideoManager_0(videoManager_t104452510 * value)
	{
		___mVideoManager_0 = value;
		Il2CppCodeGenWriteBarrier(&___mVideoManager_0, value);
	}

	inline static int32_t get_offset_of_newMenuItem_1() { return static_cast<int32_t>(offsetof(U3CbuildMenuU3Ec__AnonStorey0_t2362592727, ___newMenuItem_1)); }
	inline GameObject_t1113636619 * get_newMenuItem_1() const { return ___newMenuItem_1; }
	inline GameObject_t1113636619 ** get_address_of_newMenuItem_1() { return &___newMenuItem_1; }
	inline void set_newMenuItem_1(GameObject_t1113636619 * value)
	{
		___newMenuItem_1 = value;
		Il2CppCodeGenWriteBarrier(&___newMenuItem_1, value);
	}

	inline static int32_t get_offset_of_mesh_2() { return static_cast<int32_t>(offsetof(U3CbuildMenuU3Ec__AnonStorey0_t2362592727, ___mesh_2)); }
	inline GameObject_t1113636619 * get_mesh_2() const { return ___mesh_2; }
	inline GameObject_t1113636619 ** get_address_of_mesh_2() { return &___mesh_2; }
	inline void set_mesh_2(GameObject_t1113636619 * value)
	{
		___mesh_2 = value;
		Il2CppCodeGenWriteBarrier(&___mesh_2, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__refU241_3() { return static_cast<int32_t>(offsetof(U3CbuildMenuU3Ec__AnonStorey0_t2362592727, ___U3CU3Ef__refU241_3)); }
	inline U3CbuildMenuU3Ec__AnonStorey1_t23940567 * get_U3CU3Ef__refU241_3() const { return ___U3CU3Ef__refU241_3; }
	inline U3CbuildMenuU3Ec__AnonStorey1_t23940567 ** get_address_of_U3CU3Ef__refU241_3() { return &___U3CU3Ef__refU241_3; }
	inline void set_U3CU3Ef__refU241_3(U3CbuildMenuU3Ec__AnonStorey1_t23940567 * value)
	{
		___U3CU3Ef__refU241_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__refU241_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
