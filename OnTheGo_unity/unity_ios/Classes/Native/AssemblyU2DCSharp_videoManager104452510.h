﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour3962482529.h"

// UnityEngine.MeshRenderer
struct MeshRenderer_t587009260;
// UnityEngine.RenderTexture
struct RenderTexture_t2108887433;
// UnityEngine.GameObject
struct GameObject_t1113636619;
// Wikitude.ImageTracker
struct ImageTracker_t271395264;
// MenuController
struct MenuController_t3930949237;
// UnityEngine.Video.VideoPlayer
struct VideoPlayer_t1683042537;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// videoManager
struct  videoManager_t104452510  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.MeshRenderer videoManager::targetMaterialRenderer
	MeshRenderer_t587009260 * ___targetMaterialRenderer_2;
	// UnityEngine.RenderTexture videoManager::targetTexture
	RenderTexture_t2108887433 * ___targetTexture_3;
	// UnityEngine.GameObject videoManager::button
	GameObject_t1113636619 * ___button_4;
	// Wikitude.ImageTracker videoManager::imageTracker
	ImageTracker_t271395264 * ___imageTracker_5;
	// MenuController videoManager::menuController
	MenuController_t3930949237 * ___menuController_6;
	// UnityEngine.GameObject videoManager::loadingTextMesh
	GameObject_t1113636619 * ___loadingTextMesh_7;
	// UnityEngine.GameObject videoManager::vidHolder
	GameObject_t1113636619 * ___vidHolder_8;
	// UnityEngine.Video.VideoPlayer videoManager::videoPlayer
	VideoPlayer_t1683042537 * ___videoPlayer_9;
	// UnityEngine.Video.VideoPlayer videoManager::currentVideoPlayer
	VideoPlayer_t1683042537 * ___currentVideoPlayer_10;

public:
	inline static int32_t get_offset_of_targetMaterialRenderer_2() { return static_cast<int32_t>(offsetof(videoManager_t104452510, ___targetMaterialRenderer_2)); }
	inline MeshRenderer_t587009260 * get_targetMaterialRenderer_2() const { return ___targetMaterialRenderer_2; }
	inline MeshRenderer_t587009260 ** get_address_of_targetMaterialRenderer_2() { return &___targetMaterialRenderer_2; }
	inline void set_targetMaterialRenderer_2(MeshRenderer_t587009260 * value)
	{
		___targetMaterialRenderer_2 = value;
		Il2CppCodeGenWriteBarrier(&___targetMaterialRenderer_2, value);
	}

	inline static int32_t get_offset_of_targetTexture_3() { return static_cast<int32_t>(offsetof(videoManager_t104452510, ___targetTexture_3)); }
	inline RenderTexture_t2108887433 * get_targetTexture_3() const { return ___targetTexture_3; }
	inline RenderTexture_t2108887433 ** get_address_of_targetTexture_3() { return &___targetTexture_3; }
	inline void set_targetTexture_3(RenderTexture_t2108887433 * value)
	{
		___targetTexture_3 = value;
		Il2CppCodeGenWriteBarrier(&___targetTexture_3, value);
	}

	inline static int32_t get_offset_of_button_4() { return static_cast<int32_t>(offsetof(videoManager_t104452510, ___button_4)); }
	inline GameObject_t1113636619 * get_button_4() const { return ___button_4; }
	inline GameObject_t1113636619 ** get_address_of_button_4() { return &___button_4; }
	inline void set_button_4(GameObject_t1113636619 * value)
	{
		___button_4 = value;
		Il2CppCodeGenWriteBarrier(&___button_4, value);
	}

	inline static int32_t get_offset_of_imageTracker_5() { return static_cast<int32_t>(offsetof(videoManager_t104452510, ___imageTracker_5)); }
	inline ImageTracker_t271395264 * get_imageTracker_5() const { return ___imageTracker_5; }
	inline ImageTracker_t271395264 ** get_address_of_imageTracker_5() { return &___imageTracker_5; }
	inline void set_imageTracker_5(ImageTracker_t271395264 * value)
	{
		___imageTracker_5 = value;
		Il2CppCodeGenWriteBarrier(&___imageTracker_5, value);
	}

	inline static int32_t get_offset_of_menuController_6() { return static_cast<int32_t>(offsetof(videoManager_t104452510, ___menuController_6)); }
	inline MenuController_t3930949237 * get_menuController_6() const { return ___menuController_6; }
	inline MenuController_t3930949237 ** get_address_of_menuController_6() { return &___menuController_6; }
	inline void set_menuController_6(MenuController_t3930949237 * value)
	{
		___menuController_6 = value;
		Il2CppCodeGenWriteBarrier(&___menuController_6, value);
	}

	inline static int32_t get_offset_of_loadingTextMesh_7() { return static_cast<int32_t>(offsetof(videoManager_t104452510, ___loadingTextMesh_7)); }
	inline GameObject_t1113636619 * get_loadingTextMesh_7() const { return ___loadingTextMesh_7; }
	inline GameObject_t1113636619 ** get_address_of_loadingTextMesh_7() { return &___loadingTextMesh_7; }
	inline void set_loadingTextMesh_7(GameObject_t1113636619 * value)
	{
		___loadingTextMesh_7 = value;
		Il2CppCodeGenWriteBarrier(&___loadingTextMesh_7, value);
	}

	inline static int32_t get_offset_of_vidHolder_8() { return static_cast<int32_t>(offsetof(videoManager_t104452510, ___vidHolder_8)); }
	inline GameObject_t1113636619 * get_vidHolder_8() const { return ___vidHolder_8; }
	inline GameObject_t1113636619 ** get_address_of_vidHolder_8() { return &___vidHolder_8; }
	inline void set_vidHolder_8(GameObject_t1113636619 * value)
	{
		___vidHolder_8 = value;
		Il2CppCodeGenWriteBarrier(&___vidHolder_8, value);
	}

	inline static int32_t get_offset_of_videoPlayer_9() { return static_cast<int32_t>(offsetof(videoManager_t104452510, ___videoPlayer_9)); }
	inline VideoPlayer_t1683042537 * get_videoPlayer_9() const { return ___videoPlayer_9; }
	inline VideoPlayer_t1683042537 ** get_address_of_videoPlayer_9() { return &___videoPlayer_9; }
	inline void set_videoPlayer_9(VideoPlayer_t1683042537 * value)
	{
		___videoPlayer_9 = value;
		Il2CppCodeGenWriteBarrier(&___videoPlayer_9, value);
	}

	inline static int32_t get_offset_of_currentVideoPlayer_10() { return static_cast<int32_t>(offsetof(videoManager_t104452510, ___currentVideoPlayer_10)); }
	inline VideoPlayer_t1683042537 * get_currentVideoPlayer_10() const { return ___currentVideoPlayer_10; }
	inline VideoPlayer_t1683042537 ** get_address_of_currentVideoPlayer_10() { return &___currentVideoPlayer_10; }
	inline void set_currentVideoPlayer_10(VideoPlayer_t1683042537 * value)
	{
		___currentVideoPlayer_10 = value;
		Il2CppCodeGenWriteBarrier(&___currentVideoPlayer_10, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
