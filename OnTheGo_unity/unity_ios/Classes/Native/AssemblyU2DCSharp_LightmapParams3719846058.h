﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour3962482529.h"
#include "UnityEngine_UnityEngine_Vector43319028937.h"

// UnityEngine.Texture2D
struct Texture2D_t3840446185;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LightmapParams
struct  LightmapParams_t3719846058  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Texture2D LightmapParams::lightmapDir
	Texture2D_t3840446185 * ___lightmapDir_2;
	// UnityEngine.Texture2D LightmapParams::lightmapColor
	Texture2D_t3840446185 * ___lightmapColor_3;
	// System.Int32 LightmapParams::lightmapIndex
	int32_t ___lightmapIndex_4;
	// UnityEngine.Vector4 LightmapParams::lightmapScaleOffset
	Vector4_t3319028937  ___lightmapScaleOffset_5;

public:
	inline static int32_t get_offset_of_lightmapDir_2() { return static_cast<int32_t>(offsetof(LightmapParams_t3719846058, ___lightmapDir_2)); }
	inline Texture2D_t3840446185 * get_lightmapDir_2() const { return ___lightmapDir_2; }
	inline Texture2D_t3840446185 ** get_address_of_lightmapDir_2() { return &___lightmapDir_2; }
	inline void set_lightmapDir_2(Texture2D_t3840446185 * value)
	{
		___lightmapDir_2 = value;
		Il2CppCodeGenWriteBarrier(&___lightmapDir_2, value);
	}

	inline static int32_t get_offset_of_lightmapColor_3() { return static_cast<int32_t>(offsetof(LightmapParams_t3719846058, ___lightmapColor_3)); }
	inline Texture2D_t3840446185 * get_lightmapColor_3() const { return ___lightmapColor_3; }
	inline Texture2D_t3840446185 ** get_address_of_lightmapColor_3() { return &___lightmapColor_3; }
	inline void set_lightmapColor_3(Texture2D_t3840446185 * value)
	{
		___lightmapColor_3 = value;
		Il2CppCodeGenWriteBarrier(&___lightmapColor_3, value);
	}

	inline static int32_t get_offset_of_lightmapIndex_4() { return static_cast<int32_t>(offsetof(LightmapParams_t3719846058, ___lightmapIndex_4)); }
	inline int32_t get_lightmapIndex_4() const { return ___lightmapIndex_4; }
	inline int32_t* get_address_of_lightmapIndex_4() { return &___lightmapIndex_4; }
	inline void set_lightmapIndex_4(int32_t value)
	{
		___lightmapIndex_4 = value;
	}

	inline static int32_t get_offset_of_lightmapScaleOffset_5() { return static_cast<int32_t>(offsetof(LightmapParams_t3719846058, ___lightmapScaleOffset_5)); }
	inline Vector4_t3319028937  get_lightmapScaleOffset_5() const { return ___lightmapScaleOffset_5; }
	inline Vector4_t3319028937 * get_address_of_lightmapScaleOffset_5() { return &___lightmapScaleOffset_5; }
	inline void set_lightmapScaleOffset_5(Vector4_t3319028937  value)
	{
		___lightmapScaleOffset_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
