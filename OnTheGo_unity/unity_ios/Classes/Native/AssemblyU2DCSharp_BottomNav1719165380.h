﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour3962482529.h"

// UnityEngine.GameObject
struct GameObject_t1113636619;
// UnityEngine.Animator
struct Animator_t434523843;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BottomNav
struct  BottomNav_t1719165380  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject BottomNav::panel
	GameObject_t1113636619 * ___panel_2;
	// UnityEngine.Animator BottomNav::anim
	Animator_t434523843 * ___anim_3;
	// System.Boolean BottomNav::isPaused
	bool ___isPaused_4;

public:
	inline static int32_t get_offset_of_panel_2() { return static_cast<int32_t>(offsetof(BottomNav_t1719165380, ___panel_2)); }
	inline GameObject_t1113636619 * get_panel_2() const { return ___panel_2; }
	inline GameObject_t1113636619 ** get_address_of_panel_2() { return &___panel_2; }
	inline void set_panel_2(GameObject_t1113636619 * value)
	{
		___panel_2 = value;
		Il2CppCodeGenWriteBarrier(&___panel_2, value);
	}

	inline static int32_t get_offset_of_anim_3() { return static_cast<int32_t>(offsetof(BottomNav_t1719165380, ___anim_3)); }
	inline Animator_t434523843 * get_anim_3() const { return ___anim_3; }
	inline Animator_t434523843 ** get_address_of_anim_3() { return &___anim_3; }
	inline void set_anim_3(Animator_t434523843 * value)
	{
		___anim_3 = value;
		Il2CppCodeGenWriteBarrier(&___anim_3, value);
	}

	inline static int32_t get_offset_of_isPaused_4() { return static_cast<int32_t>(offsetof(BottomNav_t1719165380, ___isPaused_4)); }
	inline bool get_isPaused_4() const { return ___isPaused_4; }
	inline bool* get_address_of_isPaused_4() { return &___isPaused_4; }
	inline void set_isPaused_4(bool value)
	{
		___isPaused_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
