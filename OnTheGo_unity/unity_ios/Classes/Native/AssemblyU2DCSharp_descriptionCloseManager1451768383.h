﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour3962482529.h"

// UnityEngine.GameObject
struct GameObject_t1113636619;
// UnityEngine.Transform
struct Transform_t3600365921;
// LeftNav
struct LeftNav_t2375583198;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// descriptionCloseManager
struct  descriptionCloseManager_t1451768383  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject descriptionCloseManager::description
	GameObject_t1113636619 * ___description_2;
	// UnityEngine.Transform descriptionCloseManager::verticalLayoutGroup
	Transform_t3600365921 * ___verticalLayoutGroup_3;
	// LeftNav descriptionCloseManager::LeftNav
	LeftNav_t2375583198 * ___LeftNav_4;

public:
	inline static int32_t get_offset_of_description_2() { return static_cast<int32_t>(offsetof(descriptionCloseManager_t1451768383, ___description_2)); }
	inline GameObject_t1113636619 * get_description_2() const { return ___description_2; }
	inline GameObject_t1113636619 ** get_address_of_description_2() { return &___description_2; }
	inline void set_description_2(GameObject_t1113636619 * value)
	{
		___description_2 = value;
		Il2CppCodeGenWriteBarrier(&___description_2, value);
	}

	inline static int32_t get_offset_of_verticalLayoutGroup_3() { return static_cast<int32_t>(offsetof(descriptionCloseManager_t1451768383, ___verticalLayoutGroup_3)); }
	inline Transform_t3600365921 * get_verticalLayoutGroup_3() const { return ___verticalLayoutGroup_3; }
	inline Transform_t3600365921 ** get_address_of_verticalLayoutGroup_3() { return &___verticalLayoutGroup_3; }
	inline void set_verticalLayoutGroup_3(Transform_t3600365921 * value)
	{
		___verticalLayoutGroup_3 = value;
		Il2CppCodeGenWriteBarrier(&___verticalLayoutGroup_3, value);
	}

	inline static int32_t get_offset_of_LeftNav_4() { return static_cast<int32_t>(offsetof(descriptionCloseManager_t1451768383, ___LeftNav_4)); }
	inline LeftNav_t2375583198 * get_LeftNav_4() const { return ___LeftNav_4; }
	inline LeftNav_t2375583198 ** get_address_of_LeftNav_4() { return &___LeftNav_4; }
	inline void set_LeftNav_4(LeftNav_t2375583198 * value)
	{
		___LeftNav_4 = value;
		Il2CppCodeGenWriteBarrier(&___LeftNav_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
