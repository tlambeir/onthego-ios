﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array3549286319.h"
#include "UnityEngine_UnityEngine_Object631007953.h"
#include "mscorlib_System_Void1185182177.h"
#include "mscorlib_System_Object3080106164.h"
#include "UnityEngine_UnityEngine_Transform3600365921.h"
#include "mscorlib_System_Boolean97287965.h"
#include "UnityEngine_UnityEngine_Vector33722313464.h"
#include "UnityEngine_UnityEngine_Quaternion2301928331.h"
#include "mscorlib_System_Int322950945753.h"
#include "mscorlib_System_Single1397266774.h"
#include "mscorlib_System_Type2483944760.h"
#include "mscorlib_System_String1847450689.h"
#include "UnityEngine_UnityEngine_HideFlags4250555765.h"
#include "mscorlib_System_IntPtr840150181.h"
#include "mscorlib_System_Int643736567304.h"
#include "UnityEngine_UnityEngine_ScriptableObject2528358522.h"
#include "mscorlib_System_ArgumentException132251570.h"
#include "UnityEngine_UnityEngine_OperatingSystemFamily1868066375.h"
#include "UnityEngine_UnityEngine_ParticleSystem1800779281.h"
#include "UnityEngine_UnityEngine_Component1923634451.h"
#include "UnityEngine_UnityEngine_ParticleSystem_EmissionModu311448003.h"
#include "UnityEngine_UnityEngine_ParticleSystem_MinMaxCurve1067599125.h"
#include "UnityEngine_UnityEngine_Color2555686324.h"
#include "UnityEngine_UnityEngine_ParticleSystemSimulationSp2969500608.h"
#include "UnityEngine_UnityEngine_ParticleSystemScalingMode2278533876.h"
#include "mscorlib_System_UInt322560061978.h"
#include "UnityEngine_UnityEngine_ParticleSystem_MainModule2320046318.h"
#include "UnityEngine_UnityEngine_ParticleSystem_ShapeModule3608330829.h"
#include "UnityEngine_UnityEngine_ParticleSystem_VelocityOve1982232382.h"
#include "UnityEngine_UnityEngine_ParticleSystem_LimitVelocit686589569.h"
#include "UnityEngine_UnityEngine_ParticleSystem_InheritVelo3940044026.h"
#include "UnityEngine_UnityEngine_ParticleSystem_ForceOverLi4029962193.h"
#include "UnityEngine_UnityEngine_ParticleSystem_ColorOverLi3039228654.h"
#include "UnityEngine_UnityEngine_ParticleSystem_ColorBySpee3740209408.h"
#include "UnityEngine_UnityEngine_ParticleSystem_SizeOverLif1101123803.h"
#include "UnityEngine_UnityEngine_ParticleSystem_SizeBySpeed1515126846.h"
#include "UnityEngine_UnityEngine_ParticleSystem_RotationOve1164372224.h"
#include "UnityEngine_UnityEngine_ParticleSystem_RotationByS3497409583.h"
#include "UnityEngine_UnityEngine_ParticleSystem_ExternalFor1424795933.h"
#include "UnityEngine_UnityEngine_ParticleSystem_NoiseModule962525627.h"
#include "UnityEngine_UnityEngine_ParticleSystem_CollisionMo1950979710.h"
#include "UnityEngine_UnityEngine_ParticleSystem_TriggerModu1157986180.h"
#include "UnityEngine_UnityEngine_ParticleSystem_SubEmittersM903775760.h"
#include "UnityEngine_UnityEngine_ParticleSystem_TextureSheet738696839.h"
#include "UnityEngine_UnityEngine_ParticleSystem_LightsModul3616883284.h"
#include "UnityEngine_UnityEngine_ParticleSystem_TrailModule2282589118.h"
#include "UnityEngine_UnityEngine_ParticleSystem_CustomDataM2135829708.h"
#include "UnityEngine_UnityEngine_ParticleSystem_Particle1882894987.h"
#include "UnityEngine_UnityEngine_ParticleSystemCustomData1949455375.h"
#include "mscorlib_System_Collections_Generic_List_1_gen496136383.h"
#include "UnityEngine_UnityEngine_ParticleSystemStopBehavior2808326180.h"
#include "UnityEngine_UnityEngine_ParticleSystem_IteratorDel2387635027.h"
#include "UnityEngine_UnityEngine_ParticleSystem_U3CSimulateU651750728.h"
#include "UnityEngine_UnityEngine_ParticleSystem_U3CStopU3Ec_849324769.h"
#include "UnityEngine_UnityEngine_Color322600501292.h"
#include "UnityEngine_UnityEngine_ParticleSystem_EmitParams2216423628.h"
#include "UnityEngine_UnityEngine_GameObject1113636619.h"
#include "mscorlib_System_AsyncCallback3962456242.h"
#include "UnityEngine_UnityEngine_ParticleSystemCurveMode3859704052.h"
#include "UnityEngine_UnityEngine_AnimationCurve3046754366.h"
#include "UnityEngine_UnityEngine_ParticleSystemRenderer2065813411.h"
#include "UnityEngine_UnityEngine_Physics2310948930.h"
#include "UnityEngine_UnityEngine_QueryTriggerInteraction962663221.h"
#include "UnityEngine_UnityEngine_RaycastHit1056001966.h"
#include "UnityEngine_UnityEngine_Ray3785851493.h"
#include "UnityEngine_UnityEngine_Collider1773347010.h"
#include "UnityEngine_UnityEngine_Physics2D1528932956.h"
#include "UnityEngine_UnityEngine_RaycastHit2D2279581989.h"
#include "UnityEngine_UnityEngine_Vector22156229523.h"
#include "UnityEngine_UnityEngine_ContactFilter2D3805203441.h"
#include "UnityEngine_UnityEngine_Collider2D2806799626.h"
#include "UnityEngine_UnityEngine_Rigidbody2D939494601.h"
#include "mscorlib_System_Collections_Generic_List_1_gen2411569343.h"
#include "UnityEngine_UnityEngine_Plane1000493321.h"
#include "UnityEngine_UnityEngine_PreferBinarySerialization2906007930.h"
#include "mscorlib_System_Attribute861562559.h"
#include "UnityEngine_UnityEngine_PrimitiveType3468579401.h"
#include "UnityEngine_UnityEngine_PropertyAttribute3677895545.h"
#include "UnityEngine_UnityEngine_QualitySettings3101090599.h"
#include "UnityEngine_UnityEngine_ColorSpace3453996949.h"
#include "UnityEngine_UnityEngine_Random635017412.h"
#include "UnityEngine_UnityEngine_RangeAttribute3337244227.h"
#include "UnityEngine_UnityEngine_RangeInt2094684618.h"
#include "UnityEngine_UnityEngine_Rigidbody3916780224.h"
#include "UnityEngine_UnityEngine_Rect2360479859.h"
#include "UnityEngine_UnityEngine_RectOffset1369453676.h"
#include "UnityEngine_UnityEngine_RectTransform3704657025.h"
#include "UnityEngine_UnityEngine_RectTransform_ReapplyDrive1258266594.h"
#include "mscorlib_System_Delegate1188392813.h"
#include "UnityEngine_UnityEngine_RectTransform_Edge1530570602.h"
#include "UnityEngine_UnityEngine_RectTransform_Axis1856666072.h"
#include "UnityEngine_UnityEngine_RectTransformUtility1743242446.h"
#include "UnityEngine_UnityEngine_Camera4157153871.h"
#include "UnityEngine_UnityEngine_Canvas3310196443.h"
#include "UnityEngine_UnityEngine_RemoteSettings1718627291.h"
#include "UnityEngine_UnityEngine_RemoteSettings_UpdatedEven1027848393.h"
#include "UnityEngine_UnityEngine_Renderer2627027031.h"
#include "UnityEngine_UnityEngine_Rendering_ShadowCastingMod2280965600.h"
#include "UnityEngine_UnityEngine_Material340375123.h"
#include "UnityEngine_UnityEngine_Bounds2266837910.h"
#include "UnityEngine_UnityEngine_Vector43319028937.h"
#include "UnityEngine_UnityEngine_Rendering_BuiltinRenderTex2399837169.h"
#include "UnityEngine_UnityEngine_Rendering_CameraEvent2033959522.h"
#include "UnityEngine_UnityEngine_Rendering_ColorWriteMask4282245599.h"
#include "UnityEngine_UnityEngine_Rendering_CommandBuffer2206337031.h"
#include "UnityEngine_UnityEngine_Mesh3648964284.h"
#include "UnityEngine_UnityEngine_Matrix4x41817901843.h"
#include "UnityEngine_UnityEngine_MaterialPropertyBlock3213117958.h"
#include "mscorlib_System_ArgumentNullException1615371798.h"
#include "UnityEngine_UnityEngine_Rendering_RenderTargetIden2079184500.h"
#include "UnityEngine_UnityEngine_CubemapFace1358225318.h"
#include "UnityEngine_UnityEngine_Texture3661962703.h"
#include "UnityEngine_UnityEngine_FilterMode3761284007.h"
#include "UnityEngine_UnityEngine_RenderTextureFormat962350765.h"
#include "UnityEngine_UnityEngine_RenderTextureReadWrite1793271918.h"
#include "UnityEngine_UnityEngine_Rendering_CompareFunction2171731108.h"
#include "UnityEngine_UnityEngine_Rendering_GraphicsDeviceTy1797077436.h"
#include "UnityEngine_UnityEngine_RenderTexture2108887433.h"
#include "UnityEngine_UnityEngine_Rendering_StencilOp3446174106.h"
#include "UnityEngine_UnityEngine_RenderingPath883966888.h"
#include "UnityEngine_UnityEngine_RenderMode4077056833.h"
#include "UnityEngine_UnityEngine_RequireComponent3490506609.h"
#include "UnityEngine_UnityEngine_Resolution2487619763.h"
#include "UnityEngine_UnityEngine_ResourceRequest3109103591.h"
#include "UnityEngine_UnityEngine_AsyncOperation1445031843.h"
#include "UnityEngine_UnityEngine_Resources2942265397.h"
#include "mscorlib_System_RuntimeTypeHandle3027515415.h"
#include "UnityEngine_UnityEngine_ForceMode3656391766.h"
#include "UnityEngine_UnityEngine_ForceMode2D255358695.h"
#include "UnityEngine_UnityEngine_RuntimeAnimatorController2933699135.h"
#include "UnityEngine_UnityEngine_RuntimeInitializeLoadType378148151.h"
#include "UnityEngine_UnityEngine_RuntimeInitializeOnLoadMet3192313494.h"
#include "UnityEngine_UnityEngine_Scripting_PreserveAttribut1583619344.h"
#include "UnityEngine_UnityEngine_RuntimePlatform4159857903.h"
#include "UnityEngine_UnityEngine_ScaleMode2341947364.h"
#include "UnityEngine_UnityEngine_SceneManagement_LoadSceneM3251202195.h"
#include "UnityEngine_UnityEngine_SceneManagement_Scene2348375561.h"
#include "UnityEngine_UnityEngine_SceneManagement_SceneManag2787271929.h"
#include "UnityEngine_UnityEngine_Events_UnityAction_2_gen2165061829.h"
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen2933211702.h"
#include "UnityEngine_UnityEngine_Events_UnityAction_2_gen1262235195.h"
#include "UnityEngine_UnityEngine_Screen3860757715.h"
#include "UnityEngine_UnityEngine_ScreenOrientation1705519499.h"
#include "UnityEngine_UnityEngine_Scripting_APIUpdating_Moved481952341.h"
#include "UnityEngine_UnityEngine_Scripting_GeneratedByOldBin433318409.h"
#include "UnityEngine_UnityEngine_Scripting_RequiredByNative4130846357.h"
#include "UnityEngine_UnityEngine_Scripting_UsedByNativeCode1703770351.h"
#include "UnityEngine_UnityEngine_ScrollViewState3797911395.h"
#include "UnityEngine_UnityEngine_SelectionBaseAttribute3493465804.h"

// UnityEngine.Object
struct Object_t631007953;
// System.Object
struct Il2CppObject;
// UnityEngine.Transform
struct Transform_t3600365921;
// UnityEngine.Object[]
struct ObjectU5BU5D_t1417781964;
// System.Type
struct Type_t;
// System.String
struct String_t;
// System.ArgumentException
struct ArgumentException_t132251570;
// UnityEngine.ParticleSystem
struct ParticleSystem_t1800779281;
// UnityEngine.Component
struct Component_t1923634451;
// UnityEngine.ParticleSystem/Particle[]
struct ParticleU5BU5D_t3069227754;
// System.Collections.Generic.List`1<UnityEngine.Vector4>
struct List_1_t496136383;
// UnityEngine.ParticleSystem/<Simulate>c__AnonStorey0
struct U3CSimulateU3Ec__AnonStorey0_t651750728;
// UnityEngine.ParticleSystem/IteratorDelegate
struct IteratorDelegate_t2387635027;
// UnityEngine.ParticleSystem/<Stop>c__AnonStorey1
struct U3CStopU3Ec__AnonStorey1_t849324769;
// UnityEngine.GameObject
struct GameObject_t1113636619;
// System.IAsyncResult
struct IAsyncResult_t767004451;
// System.AsyncCallback
struct AsyncCallback_t3962456242;
// UnityEngine.AnimationCurve
struct AnimationCurve_t3046754366;
// UnityEngine.RaycastHit[]
struct RaycastHitU5BU5D_t1690781147;
// UnityEngine.Collider[]
struct ColliderU5BU5D_t4234922487;
// UnityEngine.Collider
struct Collider_t1773347010;
// UnityEngine.RaycastHit2D[]
struct RaycastHit2DU5BU5D_t4286651560;
// UnityEngine.Collider2D
struct Collider2D_t2806799626;
// UnityEngine.Rigidbody2D
struct Rigidbody2D_t939494601;
// System.Collections.Generic.List`1<UnityEngine.Rigidbody2D>
struct List_1_t2411569343;
// System.Collections.Generic.List`1<System.Object>
struct List_1_t257213610;
// UnityEngine.PreferBinarySerialization
struct PreferBinarySerialization_t2906007930;
// System.Attribute
struct Attribute_t861562559;
// UnityEngine.PropertyAttribute
struct PropertyAttribute_t3677895545;
// System.Object[]
struct ObjectU5BU5D_t2843939325;
// UnityEngine.RangeAttribute
struct RangeAttribute_t3337244227;
// UnityEngine.Rigidbody
struct Rigidbody_t3916780224;
// UnityEngine.RectOffset
struct RectOffset_t1369453676;
// UnityEngine.RectTransform
struct RectTransform_t3704657025;
// UnityEngine.RectTransform/ReapplyDrivenProperties
struct ReapplyDrivenProperties_t1258266594;
// System.Delegate
struct Delegate_t1188392813;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t1718750761;
// UnityEngine.Camera
struct Camera_t4157153871;
// UnityEngine.Canvas
struct Canvas_t3310196443;
// UnityEngine.RemoteSettings/UpdatedEventHandler
struct UpdatedEventHandler_t1027848393;
// UnityEngine.Renderer
struct Renderer_t2627027031;
// UnityEngine.Material
struct Material_t340375123;
// UnityEngine.Material[]
struct MaterialU5BU5D_t561872642;
// UnityEngine.Rendering.CommandBuffer
struct CommandBuffer_t2206337031;
// UnityEngine.Mesh
struct Mesh_t3648964284;
// UnityEngine.MaterialPropertyBlock
struct MaterialPropertyBlock_t3213117958;
// System.ArgumentNullException
struct ArgumentNullException_t1615371798;
// UnityEngine.Texture
struct Texture_t3661962703;
// UnityEngine.RenderTexture
struct RenderTexture_t2108887433;
// UnityEngine.RequireComponent
struct RequireComponent_t3490506609;
// UnityEngine.ResourceRequest
struct ResourceRequest_t3109103591;
// UnityEngine.AsyncOperation
struct AsyncOperation_t1445031843;
// UnityEngine.RuntimeInitializeOnLoadMethodAttribute
struct RuntimeInitializeOnLoadMethodAttribute_t3192313494;
// UnityEngine.Scripting.PreserveAttribute
struct PreserveAttribute_t1583619344;
// UnityEngine.Events.UnityAction`2<UnityEngine.SceneManagement.Scene,UnityEngine.SceneManagement.LoadSceneMode>
struct UnityAction_2_t2165061829;
// UnityEngine.Events.UnityAction`1<UnityEngine.SceneManagement.Scene>
struct UnityAction_1_t2933211702;
// UnityEngine.Events.UnityAction`2<UnityEngine.SceneManagement.Scene,UnityEngine.SceneManagement.Scene>
struct UnityAction_2_t1262235195;
// UnityEngine.ScriptableObject
struct ScriptableObject_t2528358522;
// UnityEngine.Scripting.APIUpdating.MovedFromAttribute
struct MovedFromAttribute_t481952341;
// UnityEngine.Scripting.GeneratedByOldBindingsGeneratorAttribute
struct GeneratedByOldBindingsGeneratorAttribute_t433318409;
// UnityEngine.Scripting.RequiredByNativeCodeAttribute
struct RequiredByNativeCodeAttribute_t4130846357;
// UnityEngine.Scripting.UsedByNativeCodeAttribute
struct UsedByNativeCodeAttribute_t1703770351;
// UnityEngine.ScrollViewState
struct ScrollViewState_t3797911395;
// UnityEngine.SelectionBaseAttribute
struct SelectionBaseAttribute_t3493465804;
extern Il2CppClass* Object_t631007953_il2cpp_TypeInfo_var;
extern const uint32_t Object_Internal_InstantiateSingle_m2978472292_MetadataUsageId;
extern const uint32_t Object_Internal_InstantiateSingleWithParent_m1292193254_MetadataUsageId;
extern const uint32_t Object_Destroy_m2752645118_MetadataUsageId;
extern const uint32_t Object_DestroyImmediate_m1556866283_MetadataUsageId;
extern const uint32_t Object_DestroyObject_m2915873188_MetadataUsageId;
extern Il2CppClass* IntPtr_t_il2cpp_TypeInfo_var;
extern const uint32_t Object_GetInstanceID_m3267067959_MetadataUsageId;
extern const uint32_t Object_Equals_m2798019232_MetadataUsageId;
extern const uint32_t Object_op_Implicit_m487959476_MetadataUsageId;
extern const uint32_t Object_CompareBaseObjects_m1463006152_MetadataUsageId;
extern const uint32_t Object_IsNativeObjectAlive_m1963713004_MetadataUsageId;
extern Il2CppClass* ScriptableObject_t2528358522_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentException_t132251570_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2475671027;
extern Il2CppCodeGenString* _stringLiteral1766417507;
extern const uint32_t Object_Instantiate_m2563497874_MetadataUsageId;
extern const uint32_t Object_Instantiate_m3242828326_MetadataUsageId;
extern const uint32_t Object_Instantiate_m2891014423_MetadataUsageId;
extern const uint32_t Object_Instantiate_m2737875546_MetadataUsageId;
extern const uint32_t Object_Instantiate_m3931415074_MetadataUsageId;
extern const uint32_t Object_CheckNullArgument_m129989854_MetadataUsageId;
extern const uint32_t Object_FindObjectOfType_m1736538631_MetadataUsageId;
extern const uint32_t Object_op_Equality_m1454075600_MetadataUsageId;
extern const uint32_t Object_op_Inequality_m1920811489_MetadataUsageId;
extern const uint32_t Object__cctor_m1647271852_MetadataUsageId;
extern Il2CppClass* U3CSimulateU3Ec__AnonStorey0_t651750728_il2cpp_TypeInfo_var;
extern Il2CppClass* IteratorDelegate_t2387635027_il2cpp_TypeInfo_var;
extern const MethodInfo* U3CSimulateU3Ec__AnonStorey0_U3CU3Em__0_m1701395251_MethodInfo_var;
extern const uint32_t ParticleSystem_Simulate_m1816766114_MetadataUsageId;
extern Il2CppClass* ParticleSystem_t1800779281_il2cpp_TypeInfo_var;
extern const MethodInfo* ParticleSystem_U3CPlayU3Em__0_m2559235495_MethodInfo_var;
extern const uint32_t ParticleSystem_Play_m2540933819_MetadataUsageId;
extern Il2CppClass* U3CStopU3Ec__AnonStorey1_t849324769_il2cpp_TypeInfo_var;
extern const MethodInfo* U3CStopU3Ec__AnonStorey1_U3CU3Em__0_m1364512320_MethodInfo_var;
extern const uint32_t ParticleSystem_Stop_m2292358143_MetadataUsageId;
extern const MethodInfo* ParticleSystem_U3CPauseU3Em__1_m912216727_MethodInfo_var;
extern const uint32_t ParticleSystem_Pause_m3804393934_MetadataUsageId;
extern const MethodInfo* ParticleSystem_U3CClearU3Em__2_m1611879481_MethodInfo_var;
extern const uint32_t ParticleSystem_Clear_m579965638_MetadataUsageId;
extern const MethodInfo* ParticleSystem_U3CIsAliveU3Em__3_m1003188869_MethodInfo_var;
extern const uint32_t ParticleSystem_IsAlive_m4291441292_MetadataUsageId;
extern Il2CppClass* Particle_t1882894987_il2cpp_TypeInfo_var;
extern const uint32_t ParticleSystem_Emit_m3394618241_MetadataUsageId;
extern const MethodInfo* GameObject_GetComponent_TisParticleSystem_t1800779281_m3126536506_MethodInfo_var;
extern const uint32_t ParticleSystem_IterateParticleSystemsRecursive_m2529097660_MetadataUsageId;
struct AnimationCurve_t3046754366_marshaled_pinvoke;
struct AnimationCurve_t3046754366;;
struct AnimationCurve_t3046754366_marshaled_pinvoke;;
extern Il2CppClass* AnimationCurve_t3046754366_il2cpp_TypeInfo_var;
extern const uint32_t MinMaxCurve_t1067599125_pinvoke_FromNativeMethodDefinition_MetadataUsageId;
struct AnimationCurve_t3046754366_marshaled_com;
struct AnimationCurve_t3046754366_marshaled_com;;
extern Il2CppClass* Physics2D_t1528932956_il2cpp_TypeInfo_var;
extern const uint32_t Physics2D_Raycast_m2968679153_MetadataUsageId;
extern const uint32_t Physics2D_Raycast_m2839773815_MetadataUsageId;
extern const uint32_t Physics2D_Raycast_m500627388_MetadataUsageId;
extern const uint32_t Physics2D_Raycast_m4254966674_MetadataUsageId;
extern const uint32_t Physics2D_Raycast_m4289230241_MetadataUsageId;
extern const uint32_t Physics2D_Raycast_m3588436803_MetadataUsageId;
extern const uint32_t Physics2D_Raycast_m787477166_MetadataUsageId;
extern const uint32_t Physics2D_Internal_Raycast_m4120652668_MetadataUsageId;
extern const uint32_t Physics2D_Internal_RaycastNonAlloc_m3606848900_MetadataUsageId;
extern const uint32_t Physics2D_GetRayIntersectionAll_m2417435554_MetadataUsageId;
extern const uint32_t Physics2D_GetRayIntersectionAll_m570733055_MetadataUsageId;
extern const uint32_t Physics2D_GetRayIntersectionAll_m3245298148_MetadataUsageId;
extern const uint32_t Physics2D_OverlapPoint_m2391645110_MetadataUsageId;
extern const uint32_t Physics2D_OverlapPoint_m1306494458_MetadataUsageId;
extern const uint32_t Physics2D_Internal_OverlapPoint_m1360518232_MetadataUsageId;
extern Il2CppClass* List_1_t2411569343_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m1332252848_MethodInfo_var;
extern const uint32_t Physics2D__cctor_m2508016327_MetadataUsageId;
extern Il2CppClass* Mathf_t3464937446_il2cpp_TypeInfo_var;
extern const uint32_t Plane_Raycast_m1361173428_MetadataUsageId;
extern const uint32_t Quaternion_Angle_m4174488882_MetadataUsageId;
extern Il2CppClass* Quaternion_t2301928331_il2cpp_TypeInfo_var;
extern const uint32_t Quaternion_Equals_m1217639185_MetadataUsageId;
extern Il2CppClass* ObjectU5BU5D_t2843939325_il2cpp_TypeInfo_var;
extern Il2CppClass* Single_t1397266774_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3651359435;
extern const uint32_t Quaternion_ToString_m127166294_MetadataUsageId;
extern Il2CppClass* Vector3_t3722313464_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2872073641;
extern const uint32_t Ray_ToString_m943070480_MetadataUsageId;
extern const uint32_t RaycastHit_get_rigidbody_m846276486_MetadataUsageId;
extern const uint32_t RaycastHit_get_transform_m2094454139_MetadataUsageId;
extern Il2CppClass* Rect_t2360479859_il2cpp_TypeInfo_var;
extern const uint32_t Rect_Equals_m866904981_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral736029419;
extern const uint32_t Rect_ToString_m2230825896_MetadataUsageId;
extern Il2CppClass* Il2CppComObject_il2cpp_TypeInfo_var;
extern const uint32_t RectOffset_t1369453676_pinvoke_FromNativeMethodDefinition_MetadataUsageId;
extern const uint32_t RectOffset_t1369453676_com_FromNativeMethodDefinition_MetadataUsageId;
extern Il2CppClass* Int32_t2950945753_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral376327292;
extern const uint32_t RectOffset_ToString_m3907725150_MetadataUsageId;
extern Il2CppClass* RectTransform_t3704657025_il2cpp_TypeInfo_var;
extern Il2CppClass* ReapplyDrivenProperties_t1258266594_il2cpp_TypeInfo_var;
extern const uint32_t RectTransform_add_reapplyDrivenProperties_m1599223970_MetadataUsageId;
extern const uint32_t RectTransform_remove_reapplyDrivenProperties_m3160202629_MetadataUsageId;
extern const uint32_t RectTransform_SendReapplyDrivenProperties_m3862170524_MetadataUsageId;
extern Il2CppClass* Debug_t3317548046_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4243182802;
extern const uint32_t RectTransform_GetLocalCorners_m3023515545_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral13390833;
extern const uint32_t RectTransform_GetWorldCorners_m2125351209_MetadataUsageId;
extern const uint32_t RectTransform_GetParentSize_m3805664119_MetadataUsageId;
extern Il2CppClass* RectTransformUtility_t1743242446_il2cpp_TypeInfo_var;
extern const uint32_t RectTransformUtility_ScreenPointToWorldPointInRectangle_m1236369340_MetadataUsageId;
extern const uint32_t RectTransformUtility_ScreenPointToLocalPointInRectangle_m870181352_MetadataUsageId;
extern const uint32_t RectTransformUtility_ScreenPointToRay_m3051033457_MetadataUsageId;
extern const uint32_t RectTransformUtility_FlipLayoutOnAxis_m144305054_MetadataUsageId;
extern const uint32_t RectTransformUtility_FlipLayoutAxes_m3705369832_MetadataUsageId;
extern const uint32_t RectTransformUtility_RectangleContainsScreenPoint_m1731210517_MetadataUsageId;
extern const uint32_t RectTransformUtility_PixelAdjustPoint_m2058277111_MetadataUsageId;
extern const uint32_t RectTransformUtility_PixelAdjustRect_m1219181130_MetadataUsageId;
extern Il2CppClass* Vector3U5BU5D_t1718750761_il2cpp_TypeInfo_var;
extern const uint32_t RectTransformUtility__cctor_m1465726289_MetadataUsageId;
extern Il2CppClass* RemoteSettings_t1718627291_il2cpp_TypeInfo_var;
extern const uint32_t RemoteSettings_CallOnUpdate_m237135148_MetadataUsageId;
extern const uint32_t CommandBuffer__ctor_m1834978680_MetadataUsageId;
extern const uint32_t CommandBuffer_Dispose_m811468491_MetadataUsageId;
extern Il2CppClass* ArgumentNullException_t1615371798_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3176107630;
extern Il2CppCodeGenString* _stringLiteral4003009997;
extern Il2CppCodeGenString* _stringLiteral2281755610;
extern const uint32_t CommandBuffer_DrawMesh_m1771992862_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral3876461936;
extern const uint32_t CommandBuffer_DrawRenderer_m3940925401_MetadataUsageId;
extern Il2CppClass* RenderTexture_t2108887433_il2cpp_TypeInfo_var;
extern const uint32_t RenderTargetIdentifier__ctor_m2131406890_MetadataUsageId;
extern Il2CppClass* BuiltinRenderTextureType_t2399837169_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2730748001;
extern const uint32_t RenderTargetIdentifier_ToString_m1876804468_MetadataUsageId;
extern const uint32_t RenderTargetIdentifier_GetHashCode_m1613881668_MetadataUsageId;
extern Il2CppClass* RenderTargetIdentifier_t2079184500_il2cpp_TypeInfo_var;
extern const uint32_t RenderTargetIdentifier_Equals_m2411882201_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral158126097;
extern const uint32_t Resolution_ToString_m1856579011_MetadataUsageId;
extern const Il2CppType* Object_t631007953_0_0_0_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t Resources_Load_m4211401679_MetadataUsageId;
extern Il2CppClass* Scene_t2348375561_il2cpp_TypeInfo_var;
extern const uint32_t Scene_Equals_m3811894426_MetadataUsageId;
extern Il2CppClass* SceneManager_t2787271929_il2cpp_TypeInfo_var;
extern const MethodInfo* UnityAction_2_Invoke_m1297655681_MethodInfo_var;
extern const uint32_t SceneManager_Internal_SceneLoaded_m4023393011_MetadataUsageId;
extern const MethodInfo* UnityAction_1_Invoke_m3689685399_MethodInfo_var;
extern const uint32_t SceneManager_Internal_SceneUnloaded_m2496246859_MetadataUsageId;
extern const MethodInfo* UnityAction_2_Invoke_m770027984_MethodInfo_var;
extern const uint32_t SceneManager_Internal_ActiveSceneChanged_m1220784726_MetadataUsageId;
extern const uint32_t ScriptableObject__ctor_m4119605938_MetadataUsageId;

// UnityEngine.Object[]
struct ObjectU5BU5D_t1417781964  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Object_t631007953 * m_Items[1];

public:
	inline Object_t631007953 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Object_t631007953 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Object_t631007953 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline Object_t631007953 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Object_t631007953 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Object_t631007953 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UnityEngine.ParticleSystem/Particle[]
struct ParticleU5BU5D_t3069227754  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Particle_t1882894987  m_Items[1];

public:
	inline Particle_t1882894987  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Particle_t1882894987 * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Particle_t1882894987  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline Particle_t1882894987  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Particle_t1882894987 * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Particle_t1882894987  value)
	{
		m_Items[index] = value;
	}
};
// UnityEngine.RaycastHit[]
struct RaycastHitU5BU5D_t1690781147  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) RaycastHit_t1056001966  m_Items[1];

public:
	inline RaycastHit_t1056001966  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline RaycastHit_t1056001966 * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, RaycastHit_t1056001966  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline RaycastHit_t1056001966  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline RaycastHit_t1056001966 * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, RaycastHit_t1056001966  value)
	{
		m_Items[index] = value;
	}
};
// UnityEngine.Collider[]
struct ColliderU5BU5D_t4234922487  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Collider_t1773347010 * m_Items[1];

public:
	inline Collider_t1773347010 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Collider_t1773347010 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Collider_t1773347010 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline Collider_t1773347010 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Collider_t1773347010 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Collider_t1773347010 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UnityEngine.RaycastHit2D[]
struct RaycastHit2DU5BU5D_t4286651560  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) RaycastHit2D_t2279581989  m_Items[1];

public:
	inline RaycastHit2D_t2279581989  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline RaycastHit2D_t2279581989 * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, RaycastHit2D_t2279581989  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline RaycastHit2D_t2279581989  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline RaycastHit2D_t2279581989 * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, RaycastHit2D_t2279581989  value)
	{
		m_Items[index] = value;
	}
};
// System.Object[]
struct ObjectU5BU5D_t2843939325  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Il2CppObject * m_Items[1];

public:
	inline Il2CppObject * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Il2CppObject ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Il2CppObject * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline Il2CppObject * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Il2CppObject ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Il2CppObject * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t1718750761  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Vector3_t3722313464  m_Items[1];

public:
	inline Vector3_t3722313464  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Vector3_t3722313464 * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Vector3_t3722313464  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline Vector3_t3722313464  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Vector3_t3722313464 * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Vector3_t3722313464  value)
	{
		m_Items[index] = value;
	}
};
// UnityEngine.Material[]
struct MaterialU5BU5D_t561872642  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Material_t340375123 * m_Items[1];

public:
	inline Material_t340375123 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Material_t340375123 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Material_t340375123 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline Material_t340375123 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Material_t340375123 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Material_t340375123 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};

extern "C" void AnimationCurve_t3046754366_marshal_pinvoke(const AnimationCurve_t3046754366& unmarshaled, AnimationCurve_t3046754366_marshaled_pinvoke& marshaled);
extern "C" void AnimationCurve_t3046754366_marshal_pinvoke_back(const AnimationCurve_t3046754366_marshaled_pinvoke& marshaled, AnimationCurve_t3046754366& unmarshaled);
extern "C" void AnimationCurve_t3046754366_marshal_pinvoke_cleanup(AnimationCurve_t3046754366_marshaled_pinvoke& marshaled);
extern "C" void AnimationCurve_t3046754366_marshal_com(const AnimationCurve_t3046754366& unmarshaled, AnimationCurve_t3046754366_marshaled_com& marshaled);
extern "C" void AnimationCurve_t3046754366_marshal_com_back(const AnimationCurve_t3046754366_marshaled_com& marshaled, AnimationCurve_t3046754366& unmarshaled);
extern "C" void AnimationCurve_t3046754366_marshal_com_cleanup(AnimationCurve_t3046754366_marshaled_com& marshaled);

// T UnityEngine.GameObject::GetComponent<System.Object>()
extern "C"  Il2CppObject * GameObject_GetComponent_TisIl2CppObject_m2385344436_gshared (GameObject_t1113636619 * __this, const MethodInfo* method);
// System.Void System.Collections.Generic.List`1<System.Object>::.ctor()
extern "C"  void List_1__ctor_m2321703786_gshared (List_1_t257213610 * __this, const MethodInfo* method);
// System.Void UnityEngine.Events.UnityAction`2<UnityEngine.SceneManagement.Scene,UnityEngine.SceneManagement.LoadSceneMode>::Invoke(T0,T1)
extern "C"  void UnityAction_2_Invoke_m1297655681_gshared (UnityAction_2_t2165061829 * __this, Scene_t2348375561  p0, int32_t p1, const MethodInfo* method);
// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.SceneManagement.Scene>::Invoke(T0)
extern "C"  void UnityAction_1_Invoke_m3689685399_gshared (UnityAction_1_t2933211702 * __this, Scene_t2348375561  p0, const MethodInfo* method);
// System.Void UnityEngine.Events.UnityAction`2<UnityEngine.SceneManagement.Scene,UnityEngine.SceneManagement.Scene>::Invoke(T0,T1)
extern "C"  void UnityAction_2_Invoke_m770027984_gshared (UnityAction_2_t1262235195 * __this, Scene_t2348375561  p0, Scene_t2348375561  p1, const MethodInfo* method);

// System.Void System.Object::.ctor()
extern "C"  void Object__ctor_m297566312 (Il2CppObject * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Object UnityEngine.Object::INTERNAL_CALL_Internal_InstantiateSingle(UnityEngine.Object,UnityEngine.Vector3&,UnityEngine.Quaternion&)
extern "C"  Object_t631007953 * Object_INTERNAL_CALL_Internal_InstantiateSingle_m3963327137 (Il2CppObject * __this /* static, unused */, Object_t631007953 * ___data0, Vector3_t3722313464 * ___pos1, Quaternion_t2301928331 * ___rot2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Object UnityEngine.Object::INTERNAL_CALL_Internal_InstantiateSingleWithParent(UnityEngine.Object,UnityEngine.Transform,UnityEngine.Vector3&,UnityEngine.Quaternion&)
extern "C"  Object_t631007953 * Object_INTERNAL_CALL_Internal_InstantiateSingleWithParent_m3300138447 (Il2CppObject * __this /* static, unused */, Object_t631007953 * ___data0, Transform_t3600365921 * ___parent1, Vector3_t3722313464 * ___pos2, Quaternion_t2301928331 * ___rot3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Object::Destroy(UnityEngine.Object,System.Single)
extern "C"  void Object_Destroy_m1895515052 (Il2CppObject * __this /* static, unused */, Object_t631007953 * ___obj0, float ___t1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Object::DestroyImmediate(UnityEngine.Object,System.Boolean)
extern "C"  void Object_DestroyImmediate_m2539295074 (Il2CppObject * __this /* static, unused */, Object_t631007953 * ___obj0, bool ___allowDestroyingAssets1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Object::DestroyObject(UnityEngine.Object,System.Single)
extern "C"  void Object_DestroyObject_m869248974 (Il2CppObject * __this /* static, unused */, Object_t631007953 * ___obj0, float ___t1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.IntPtr::op_Equality(System.IntPtr,System.IntPtr)
extern "C"  bool IntPtr_op_Equality_m408849716 (Il2CppObject * __this /* static, unused */, IntPtr_t p0, IntPtr_t p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Object::GetOffsetOfInstanceIDInCPlusPlusObject()
extern "C"  int32_t Object_GetOffsetOfInstanceIDInCPlusPlusObject_m3574580840 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 System.IntPtr::ToInt64()
extern "C"  int64_t IntPtr_ToInt64_m192765549 (IntPtr_t* __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IntPtr::.ctor(System.Int64)
extern "C"  void IntPtr__ctor_m987476171 (IntPtr_t* __this, int64_t p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void* System.IntPtr::op_Explicit(System.IntPtr)
extern "C"  void* IntPtr_op_Explicit_m2520637223 (Il2CppObject * __this /* static, unused */, IntPtr_t p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Object::GetHashCode()
extern "C"  int32_t Object_GetHashCode_m2705121830 (Il2CppObject * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Object::op_Equality(UnityEngine.Object,UnityEngine.Object)
extern "C"  bool Object_op_Equality_m1454075600 (Il2CppObject * __this /* static, unused */, Object_t631007953 * ___x0, Object_t631007953 * ___y1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Object::CompareBaseObjects(UnityEngine.Object,UnityEngine.Object)
extern "C"  bool Object_CompareBaseObjects_m1463006152 (Il2CppObject * __this /* static, unused */, Object_t631007953 * ___lhs0, Object_t631007953 * ___rhs1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Object::IsNativeObjectAlive(UnityEngine.Object)
extern "C"  bool Object_IsNativeObjectAlive_m1963713004 (Il2CppObject * __this /* static, unused */, Object_t631007953 * ___o0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Object::ReferenceEquals(System.Object,System.Object)
extern "C"  bool Object_ReferenceEquals_m610702577 (Il2CppObject * __this /* static, unused */, Il2CppObject * p0, Il2CppObject * p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IntPtr UnityEngine.Object::GetCachedPtr()
extern "C"  IntPtr_t Object_GetCachedPtr_m589232273 (Object_t631007953 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.IntPtr::op_Inequality(System.IntPtr,System.IntPtr)
extern "C"  bool IntPtr_op_Inequality_m3063970704 (Il2CppObject * __this /* static, unused */, IntPtr_t p0, IntPtr_t p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Object::CheckNullArgument(System.Object,System.String)
extern "C"  void Object_CheckNullArgument_m129989854 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___arg0, String_t* ___message1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.ArgumentException::.ctor(System.String)
extern "C"  void ArgumentException__ctor_m1312628991 (ArgumentException_t132251570 * __this, String_t* p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Object UnityEngine.Object::Internal_InstantiateSingle(UnityEngine.Object,UnityEngine.Vector3,UnityEngine.Quaternion)
extern "C"  Object_t631007953 * Object_Internal_InstantiateSingle_m2978472292 (Il2CppObject * __this /* static, unused */, Object_t631007953 * ___data0, Vector3_t3722313464  ___pos1, Quaternion_t2301928331  ___rot2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Object UnityEngine.Object::Internal_InstantiateSingleWithParent(UnityEngine.Object,UnityEngine.Transform,UnityEngine.Vector3,UnityEngine.Quaternion)
extern "C"  Object_t631007953 * Object_Internal_InstantiateSingleWithParent_m1292193254 (Il2CppObject * __this /* static, unused */, Object_t631007953 * ___data0, Transform_t3600365921 * ___parent1, Vector3_t3722313464  ___pos2, Quaternion_t2301928331  ___rot3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Object UnityEngine.Object::Internal_CloneSingle(UnityEngine.Object)
extern "C"  Object_t631007953 * Object_Internal_CloneSingle_m1527886285 (Il2CppObject * __this /* static, unused */, Object_t631007953 * ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Object UnityEngine.Object::Instantiate(UnityEngine.Object,UnityEngine.Transform,System.Boolean)
extern "C"  Object_t631007953 * Object_Instantiate_m3931415074 (Il2CppObject * __this /* static, unused */, Object_t631007953 * ___original0, Transform_t3600365921 * ___parent1, bool ___instantiateInWorldSpace2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Object UnityEngine.Object::Internal_CloneSingleWithParent(UnityEngine.Object,UnityEngine.Transform,System.Boolean)
extern "C"  Object_t631007953 * Object_Internal_CloneSingleWithParent_m2047142090 (Il2CppObject * __this /* static, unused */, Object_t631007953 * ___data0, Transform_t3600365921 * ___parent1, bool ___worldPositionStays2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Object[] UnityEngine.Object::FindObjectsOfType(System.Type)
extern "C"  ObjectU5BU5D_t1417781964* Object_FindObjectsOfType_m2898745631 (Il2CppObject * __this /* static, unused */, Type_t * ___type0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Component::.ctor()
extern "C"  void Component__ctor_m3533368306 (Component_t1923634451 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.ParticleSystem/EmissionModule UnityEngine.ParticleSystem::get_emission()
extern "C"  EmissionModule_t311448003  ParticleSystem_get_emission_m1885497174 (ParticleSystem_t1800779281 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.ParticleSystem/EmissionModule::get_enabled()
extern "C"  bool EmissionModule_get_enabled_m1041402336 (EmissionModule_t311448003 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ParticleSystem/EmissionModule::set_enabled(System.Boolean)
extern "C"  void EmissionModule_set_enabled_m416122042 (EmissionModule_t311448003 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.ParticleSystem/EmissionModule::get_rateOverTimeMultiplier()
extern "C"  float EmissionModule_get_rateOverTimeMultiplier_m608658658 (EmissionModule_t311448003 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.ParticleSystem/MinMaxCurve UnityEngine.ParticleSystem/MinMaxCurve::op_Implicit(System.Single)
extern "C"  MinMaxCurve_t1067599125  MinMaxCurve_op_Implicit_m3696034763 (Il2CppObject * __this /* static, unused */, float ___constant0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ParticleSystem/EmissionModule::set_rateOverTime(UnityEngine.ParticleSystem/MinMaxCurve)
extern "C"  void EmissionModule_set_rateOverTime_m3483972715 (EmissionModule_t311448003 * __this, MinMaxCurve_t1067599125  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ParticleSystem::INTERNAL_get_startColor(UnityEngine.Color&)
extern "C"  void ParticleSystem_INTERNAL_get_startColor_m2957612057 (ParticleSystem_t1800779281 * __this, Color_t2555686324 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ParticleSystem::INTERNAL_set_startColor(UnityEngine.Color&)
extern "C"  void ParticleSystem_INTERNAL_set_startColor_m3394702698 (ParticleSystem_t1800779281 * __this, Color_t2555686324 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ParticleSystem::INTERNAL_get_startRotation3D(UnityEngine.Vector3&)
extern "C"  void ParticleSystem_INTERNAL_get_startRotation3D_m361332872 (ParticleSystem_t1800779281 * __this, Vector3_t3722313464 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ParticleSystem::INTERNAL_set_startRotation3D(UnityEngine.Vector3&)
extern "C"  void ParticleSystem_INTERNAL_set_startRotation3D_m3566101937 (ParticleSystem_t1800779281 * __this, Vector3_t3722313464 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ParticleSystem/MainModule::.ctor(UnityEngine.ParticleSystem)
extern "C"  void MainModule__ctor_m158988634 (MainModule_t2320046318 * __this, ParticleSystem_t1800779281 * ___particleSystem0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ParticleSystem/EmissionModule::.ctor(UnityEngine.ParticleSystem)
extern "C"  void EmissionModule__ctor_m1724379159 (EmissionModule_t311448003 * __this, ParticleSystem_t1800779281 * ___particleSystem0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ParticleSystem/ShapeModule::.ctor(UnityEngine.ParticleSystem)
extern "C"  void ShapeModule__ctor_m2339294796 (ShapeModule_t3608330829 * __this, ParticleSystem_t1800779281 * ___particleSystem0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ParticleSystem/VelocityOverLifetimeModule::.ctor(UnityEngine.ParticleSystem)
extern "C"  void VelocityOverLifetimeModule__ctor_m3772502470 (VelocityOverLifetimeModule_t1982232382 * __this, ParticleSystem_t1800779281 * ___particleSystem0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ParticleSystem/LimitVelocityOverLifetimeModule::.ctor(UnityEngine.ParticleSystem)
extern "C"  void LimitVelocityOverLifetimeModule__ctor_m3158309845 (LimitVelocityOverLifetimeModule_t686589569 * __this, ParticleSystem_t1800779281 * ___particleSystem0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ParticleSystem/InheritVelocityModule::.ctor(UnityEngine.ParticleSystem)
extern "C"  void InheritVelocityModule__ctor_m3717584807 (InheritVelocityModule_t3940044026 * __this, ParticleSystem_t1800779281 * ___particleSystem0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ParticleSystem/ForceOverLifetimeModule::.ctor(UnityEngine.ParticleSystem)
extern "C"  void ForceOverLifetimeModule__ctor_m3986876230 (ForceOverLifetimeModule_t4029962193 * __this, ParticleSystem_t1800779281 * ___particleSystem0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ParticleSystem/ColorOverLifetimeModule::.ctor(UnityEngine.ParticleSystem)
extern "C"  void ColorOverLifetimeModule__ctor_m1411701942 (ColorOverLifetimeModule_t3039228654 * __this, ParticleSystem_t1800779281 * ___particleSystem0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ParticleSystem/ColorBySpeedModule::.ctor(UnityEngine.ParticleSystem)
extern "C"  void ColorBySpeedModule__ctor_m4046611376 (ColorBySpeedModule_t3740209408 * __this, ParticleSystem_t1800779281 * ___particleSystem0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ParticleSystem/SizeOverLifetimeModule::.ctor(UnityEngine.ParticleSystem)
extern "C"  void SizeOverLifetimeModule__ctor_m3730960903 (SizeOverLifetimeModule_t1101123803 * __this, ParticleSystem_t1800779281 * ___particleSystem0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ParticleSystem/SizeBySpeedModule::.ctor(UnityEngine.ParticleSystem)
extern "C"  void SizeBySpeedModule__ctor_m3934889008 (SizeBySpeedModule_t1515126846 * __this, ParticleSystem_t1800779281 * ___particleSystem0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ParticleSystem/RotationOverLifetimeModule::.ctor(UnityEngine.ParticleSystem)
extern "C"  void RotationOverLifetimeModule__ctor_m1430131584 (RotationOverLifetimeModule_t1164372224 * __this, ParticleSystem_t1800779281 * ___particleSystem0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ParticleSystem/RotationBySpeedModule::.ctor(UnityEngine.ParticleSystem)
extern "C"  void RotationBySpeedModule__ctor_m4096345870 (RotationBySpeedModule_t3497409583 * __this, ParticleSystem_t1800779281 * ___particleSystem0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ParticleSystem/ExternalForcesModule::.ctor(UnityEngine.ParticleSystem)
extern "C"  void ExternalForcesModule__ctor_m3166001247 (ExternalForcesModule_t1424795933 * __this, ParticleSystem_t1800779281 * ___particleSystem0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ParticleSystem/NoiseModule::.ctor(UnityEngine.ParticleSystem)
extern "C"  void NoiseModule__ctor_m1277709549 (NoiseModule_t962525627 * __this, ParticleSystem_t1800779281 * ___particleSystem0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ParticleSystem/CollisionModule::.ctor(UnityEngine.ParticleSystem)
extern "C"  void CollisionModule__ctor_m760520803 (CollisionModule_t1950979710 * __this, ParticleSystem_t1800779281 * ___particleSystem0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ParticleSystem/TriggerModule::.ctor(UnityEngine.ParticleSystem)
extern "C"  void TriggerModule__ctor_m1363700541 (TriggerModule_t1157986180 * __this, ParticleSystem_t1800779281 * ___particleSystem0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ParticleSystem/SubEmittersModule::.ctor(UnityEngine.ParticleSystem)
extern "C"  void SubEmittersModule__ctor_m4074391005 (SubEmittersModule_t903775760 * __this, ParticleSystem_t1800779281 * ___particleSystem0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ParticleSystem/TextureSheetAnimationModule::.ctor(UnityEngine.ParticleSystem)
extern "C"  void TextureSheetAnimationModule__ctor_m1467095871 (TextureSheetAnimationModule_t738696839 * __this, ParticleSystem_t1800779281 * ___particleSystem0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ParticleSystem/LightsModule::.ctor(UnityEngine.ParticleSystem)
extern "C"  void LightsModule__ctor_m4219401727 (LightsModule_t3616883284 * __this, ParticleSystem_t1800779281 * ___particleSystem0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ParticleSystem/TrailModule::.ctor(UnityEngine.ParticleSystem)
extern "C"  void TrailModule__ctor_m1664590038 (TrailModule_t2282589118 * __this, ParticleSystem_t1800779281 * ___particleSystem0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ParticleSystem/CustomDataModule::.ctor(UnityEngine.ParticleSystem)
extern "C"  void CustomDataModule__ctor_m2822328115 (CustomDataModule_t2135829708 * __this, ParticleSystem_t1800779281 * ___particleSystem0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ParticleSystem::SetCustomParticleDataInternal(System.Object,System.Int32)
extern "C"  void ParticleSystem_SetCustomParticleDataInternal_m956969092 (ParticleSystem_t1800779281 * __this, Il2CppObject * ___customData0, int32_t ___streamIndex1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.ParticleSystem::GetCustomParticleDataInternal(System.Object,System.Int32)
extern "C"  int32_t ParticleSystem_GetCustomParticleDataInternal_m1088470722 (ParticleSystem_t1800779281 * __this, Il2CppObject * ___customData0, int32_t ___streamIndex1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ParticleSystem::Simulate(System.Single,System.Boolean,System.Boolean,System.Boolean)
extern "C"  void ParticleSystem_Simulate_m1816766114 (ParticleSystem_t1800779281 * __this, float ___t0, bool ___withChildren1, bool ___restart2, bool ___fixedTimeStep3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ParticleSystem/<Simulate>c__AnonStorey0::.ctor()
extern "C"  void U3CSimulateU3Ec__AnonStorey0__ctor_m493021975 (U3CSimulateU3Ec__AnonStorey0_t651750728 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ParticleSystem/IteratorDelegate::.ctor(System.Object,System.IntPtr)
extern "C"  void IteratorDelegate__ctor_m4019223695 (IteratorDelegate_t2387635027 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.ParticleSystem::IterateParticleSystems(System.Boolean,UnityEngine.ParticleSystem/IteratorDelegate)
extern "C"  bool ParticleSystem_IterateParticleSystems_m2561813326 (ParticleSystem_t1800779281 * __this, bool ___recurse0, IteratorDelegate_t2387635027 * ___func1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ParticleSystem::Play(System.Boolean)
extern "C"  void ParticleSystem_Play_m2540933819 (ParticleSystem_t1800779281 * __this, bool ___withChildren0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ParticleSystem::Stop(System.Boolean,UnityEngine.ParticleSystemStopBehavior)
extern "C"  void ParticleSystem_Stop_m2292358143 (ParticleSystem_t1800779281 * __this, bool ___withChildren0, int32_t ___stopBehavior1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ParticleSystem/<Stop>c__AnonStorey1::.ctor()
extern "C"  void U3CStopU3Ec__AnonStorey1__ctor_m234964104 (U3CStopU3Ec__AnonStorey1_t849324769 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ParticleSystem::Pause(System.Boolean)
extern "C"  void ParticleSystem_Pause_m3804393934 (ParticleSystem_t1800779281 * __this, bool ___withChildren0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ParticleSystem::Clear(System.Boolean)
extern "C"  void ParticleSystem_Clear_m579965638 (ParticleSystem_t1800779281 * __this, bool ___withChildren0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.ParticleSystem::IsAlive(System.Boolean)
extern "C"  bool ParticleSystem_IsAlive_m4291441292 (ParticleSystem_t1800779281 * __this, bool ___withChildren0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ParticleSystem::INTERNAL_CALL_Emit(UnityEngine.ParticleSystem,System.Int32)
extern "C"  void ParticleSystem_INTERNAL_CALL_Emit_m3019529794 (Il2CppObject * __this /* static, unused */, ParticleSystem_t1800779281 * ___self0, int32_t ___count1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ParticleSystem/Particle::set_position(UnityEngine.Vector3)
extern "C"  void Particle_set_position_m2092914191 (Particle_t1882894987 * __this, Vector3_t3722313464  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ParticleSystem/Particle::set_velocity(UnityEngine.Vector3)
extern "C"  void Particle_set_velocity_m3704745308 (Particle_t1882894987 * __this, Vector3_t3722313464  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ParticleSystem/Particle::set_lifetime(System.Single)
extern "C"  void Particle_set_lifetime_m1932789319 (Particle_t1882894987 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ParticleSystem/Particle::set_startLifetime(System.Single)
extern "C"  void Particle_set_startLifetime_m2269137591 (Particle_t1882894987 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ParticleSystem/Particle::set_startSize(System.Single)
extern "C"  void Particle_set_startSize_m662812576 (Particle_t1882894987 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Vector3::get_zero()
extern "C"  Vector3_t3722313464  Vector3_get_zero_m1640475482 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ParticleSystem/Particle::set_rotation3D(UnityEngine.Vector3)
extern "C"  void Particle_set_rotation3D_m2052300534 (Particle_t1882894987 * __this, Vector3_t3722313464  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ParticleSystem/Particle::set_angularVelocity3D(UnityEngine.Vector3)
extern "C"  void Particle_set_angularVelocity3D_m648168096 (Particle_t1882894987 * __this, Vector3_t3722313464  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ParticleSystem/Particle::set_startColor(UnityEngine.Color32)
extern "C"  void Particle_set_startColor_m3169687975 (Particle_t1882894987 * __this, Color32_t2600501292  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ParticleSystem/Particle::set_randomSeed(System.UInt32)
extern "C"  void Particle_set_randomSeed_m3843991369 (Particle_t1882894987 * __this, uint32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ParticleSystem::Internal_EmitOld(UnityEngine.ParticleSystem/Particle&)
extern "C"  void ParticleSystem_Internal_EmitOld_m2478817790 (ParticleSystem_t1800779281 * __this, Particle_t1882894987 * ___particle0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ParticleSystem::Internal_Emit(UnityEngine.ParticleSystem/EmitParams&,System.Int32)
extern "C"  void ParticleSystem_Internal_Emit_m2404017392 (ParticleSystem_t1800779281 * __this, EmitParams_t2216423628 * ___emitParams0, int32_t ___count1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.ParticleSystem/IteratorDelegate::Invoke(UnityEngine.ParticleSystem)
extern "C"  bool IteratorDelegate_Invoke_m84429716 (IteratorDelegate_t2387635027 * __this, ParticleSystem_t1800779281 * ___ps0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform UnityEngine.Component::get_transform()
extern "C"  Transform_t3600365921 * Component_get_transform_m2921103810 (Component_t1923634451 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.ParticleSystem::IterateParticleSystemsRecursive(UnityEngine.Transform,UnityEngine.ParticleSystem/IteratorDelegate)
extern "C"  bool ParticleSystem_IterateParticleSystemsRecursive_m2529097660 (Il2CppObject * __this /* static, unused */, Transform_t3600365921 * ___transform0, IteratorDelegate_t2387635027 * ___func1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Transform::get_childCount()
extern "C"  int32_t Transform_get_childCount_m4033131441 (Transform_t3600365921 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform UnityEngine.Transform::GetChild(System.Int32)
extern "C"  Transform_t3600365921 * Transform_GetChild_m3541171965 (Transform_t3600365921 * __this, int32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject UnityEngine.Component::get_gameObject()
extern "C"  GameObject_t1113636619 * Component_get_gameObject_m2648350745 (Component_t1923634451 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// T UnityEngine.GameObject::GetComponent<UnityEngine.ParticleSystem>()
#define GameObject_GetComponent_TisParticleSystem_t1800779281_m3126536506(__this, method) ((  ParticleSystem_t1800779281 * (*) (GameObject_t1113636619 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2385344436_gshared)(__this, method)
// System.Boolean UnityEngine.Object::op_Inequality(UnityEngine.Object,UnityEngine.Object)
extern "C"  bool Object_op_Inequality_m1920811489 (Il2CppObject * __this /* static, unused */, Object_t631007953 * ___x0, Object_t631007953 * ___y1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.ParticleSystem::Internal_Play(UnityEngine.ParticleSystem)
extern "C"  bool ParticleSystem_Internal_Play_m3248398569 (Il2CppObject * __this /* static, unused */, ParticleSystem_t1800779281 * ___self0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.ParticleSystem::Internal_Pause(UnityEngine.ParticleSystem)
extern "C"  bool ParticleSystem_Internal_Pause_m3444665175 (Il2CppObject * __this /* static, unused */, ParticleSystem_t1800779281 * ___self0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.ParticleSystem::Internal_Clear(UnityEngine.ParticleSystem)
extern "C"  bool ParticleSystem_Internal_Clear_m2418677798 (Il2CppObject * __this /* static, unused */, ParticleSystem_t1800779281 * ___self0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.ParticleSystem::Internal_IsAlive(UnityEngine.ParticleSystem)
extern "C"  bool ParticleSystem_Internal_IsAlive_m2609780182 (Il2CppObject * __this /* static, unused */, ParticleSystem_t1800779281 * ___self0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.ParticleSystem::Internal_Simulate(UnityEngine.ParticleSystem,System.Single,System.Boolean,System.Boolean)
extern "C"  bool ParticleSystem_Internal_Simulate_m30884640 (Il2CppObject * __this /* static, unused */, ParticleSystem_t1800779281 * ___self0, float ___t1, bool ___restart2, bool ___fixedTimeStep3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.ParticleSystem::Internal_Stop(UnityEngine.ParticleSystem,UnityEngine.ParticleSystemStopBehavior)
extern "C"  bool ParticleSystem_Internal_Stop_m883182992 (Il2CppObject * __this /* static, unused */, ParticleSystem_t1800779281 * ___self0, int32_t ___stopBehavior1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ParticleSystem/EmissionModule::SetEnabled(UnityEngine.ParticleSystem,System.Boolean)
extern "C"  void EmissionModule_SetEnabled_m2774397187 (Il2CppObject * __this /* static, unused */, ParticleSystem_t1800779281 * ___system0, bool ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.ParticleSystem/EmissionModule::GetEnabled(UnityEngine.ParticleSystem)
extern "C"  bool EmissionModule_GetEnabled_m623574933 (Il2CppObject * __this /* static, unused */, ParticleSystem_t1800779281 * ___system0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ParticleSystem/EmissionModule::SetRateOverTime(UnityEngine.ParticleSystem,UnityEngine.ParticleSystem/MinMaxCurve&)
extern "C"  void EmissionModule_SetRateOverTime_m2957047264 (Il2CppObject * __this /* static, unused */, ParticleSystem_t1800779281 * ___system0, MinMaxCurve_t1067599125 * ___curve1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.ParticleSystem/EmissionModule::GetRateOverTimeMultiplier(UnityEngine.ParticleSystem)
extern "C"  float EmissionModule_GetRateOverTimeMultiplier_m3575436295 (Il2CppObject * __this /* static, unused */, ParticleSystem_t1800779281 * ___system0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AnimationCurve::.ctor()
extern "C"  void AnimationCurve__ctor_m1499663178 (AnimationCurve_t3046754366 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ParticleSystem/MinMaxCurve::.ctor(System.Single)
extern "C"  void MinMaxCurve__ctor_m2986442616 (MinMaxCurve_t1067599125 * __this, float ___constant0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Vector3::.ctor(System.Single,System.Single,System.Single)
extern "C"  void Vector3__ctor_m1197556204 (Vector3_t3722313464 * __this, float ___x0, float ___y1, float ___z2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Vector3::op_Multiply(UnityEngine.Vector3,System.Single)
extern "C"  Vector3_t3722313464  Vector3_op_Multiply_m3506743150 (Il2CppObject * __this /* static, unused */, Vector3_t3722313464  ___a0, float ___d1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Physics::Raycast(UnityEngine.Vector3,UnityEngine.Vector3,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern "C"  bool Physics_Raycast_m325282035 (Il2CppObject * __this /* static, unused */, Vector3_t3722313464  ___origin0, Vector3_t3722313464  ___direction1, float ___maxDistance2, int32_t ___layerMask3, int32_t ___queryTriggerInteraction4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Physics::Internal_RaycastTest(UnityEngine.Vector3,UnityEngine.Vector3,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern "C"  bool Physics_Internal_RaycastTest_m2320488002 (Il2CppObject * __this /* static, unused */, Vector3_t3722313464  ___origin0, Vector3_t3722313464  ___direction1, float ___maxDistance2, int32_t ___layermask3, int32_t ___queryTriggerInteraction4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Physics::Raycast(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.RaycastHit&,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern "C"  bool Physics_Raycast_m3501191919 (Il2CppObject * __this /* static, unused */, Vector3_t3722313464  ___origin0, Vector3_t3722313464  ___direction1, RaycastHit_t1056001966 * ___hitInfo2, float ___maxDistance3, int32_t ___layerMask4, int32_t ___queryTriggerInteraction5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Physics::Internal_Raycast(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.RaycastHit&,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern "C"  bool Physics_Internal_Raycast_m1368166979 (Il2CppObject * __this /* static, unused */, Vector3_t3722313464  ___origin0, Vector3_t3722313464  ___direction1, RaycastHit_t1056001966 * ___hitInfo2, float ___maxDistance3, int32_t ___layermask4, int32_t ___queryTriggerInteraction5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Physics::Raycast(UnityEngine.Ray,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern "C"  bool Physics_Raycast_m1114390595 (Il2CppObject * __this /* static, unused */, Ray_t3785851493  ___ray0, float ___maxDistance1, int32_t ___layerMask2, int32_t ___queryTriggerInteraction3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Ray::get_origin()
extern "C"  Vector3_t3722313464  Ray_get_origin_m4290253200 (Ray_t3785851493 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Ray::get_direction()
extern "C"  Vector3_t3722313464  Ray_get_direction_m1991692996 (Ray_t3785851493 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Physics::Raycast(UnityEngine.Ray,UnityEngine.RaycastHit&,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern "C"  bool Physics_Raycast_m121696816 (Il2CppObject * __this /* static, unused */, Ray_t3785851493  ___ray0, RaycastHit_t1056001966 * ___hitInfo1, float ___maxDistance2, int32_t ___layerMask3, int32_t ___queryTriggerInteraction4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.RaycastHit[] UnityEngine.Physics::RaycastAll(UnityEngine.Ray,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern "C"  RaycastHitU5BU5D_t1690781147* Physics_RaycastAll_m2730568772 (Il2CppObject * __this /* static, unused */, Ray_t3785851493  ___ray0, float ___maxDistance1, int32_t ___layerMask2, int32_t ___queryTriggerInteraction3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.RaycastHit[] UnityEngine.Physics::RaycastAll(UnityEngine.Vector3,UnityEngine.Vector3,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern "C"  RaycastHitU5BU5D_t1690781147* Physics_RaycastAll_m1264095660 (Il2CppObject * __this /* static, unused */, Vector3_t3722313464  ___origin0, Vector3_t3722313464  ___direction1, float ___maxDistance2, int32_t ___layermask3, int32_t ___queryTriggerInteraction4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.RaycastHit[] UnityEngine.Physics::INTERNAL_CALL_RaycastAll(UnityEngine.Vector3&,UnityEngine.Vector3&,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern "C"  RaycastHitU5BU5D_t1690781147* Physics_INTERNAL_CALL_RaycastAll_m953202051 (Il2CppObject * __this /* static, unused */, Vector3_t3722313464 * ___origin0, Vector3_t3722313464 * ___direction1, float ___maxDistance2, int32_t ___layermask3, int32_t ___queryTriggerInteraction4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Physics::Linecast(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.RaycastHit&,System.Int32,UnityEngine.QueryTriggerInteraction)
extern "C"  bool Physics_Linecast_m93906766 (Il2CppObject * __this /* static, unused */, Vector3_t3722313464  ___start0, Vector3_t3722313464  ___end1, RaycastHit_t1056001966 * ___hitInfo2, int32_t ___layerMask3, int32_t ___queryTriggerInteraction4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Vector3::op_Subtraction(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  Vector3_t3722313464  Vector3_op_Subtraction_m2566684344 (Il2CppObject * __this /* static, unused */, Vector3_t3722313464  ___a0, Vector3_t3722313464  ___b1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Vector3::get_magnitude()
extern "C"  float Vector3_get_magnitude_m206834744 (Vector3_t3722313464 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Collider[] UnityEngine.Physics::INTERNAL_CALL_OverlapSphere(UnityEngine.Vector3&,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern "C"  ColliderU5BU5D_t4234922487* Physics_INTERNAL_CALL_OverlapSphere_m143667889 (Il2CppObject * __this /* static, unused */, Vector3_t3722313464 * ___position0, float ___radius1, int32_t ___layerMask2, int32_t ___queryTriggerInteraction3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Physics::INTERNAL_CALL_Internal_Raycast(UnityEngine.Vector3&,UnityEngine.Vector3&,UnityEngine.RaycastHit&,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern "C"  bool Physics_INTERNAL_CALL_Internal_Raycast_m741619868 (Il2CppObject * __this /* static, unused */, Vector3_t3722313464 * ___origin0, Vector3_t3722313464 * ___direction1, RaycastHit_t1056001966 * ___hitInfo2, float ___maxDistance3, int32_t ___layermask4, int32_t ___queryTriggerInteraction5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Physics::INTERNAL_CALL_Internal_RaycastTest(UnityEngine.Vector3&,UnityEngine.Vector3&,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern "C"  bool Physics_INTERNAL_CALL_Internal_RaycastTest_m1447703527 (Il2CppObject * __this /* static, unused */, Vector3_t3722313464 * ___origin0, Vector3_t3722313464 * ___direction1, float ___maxDistance2, int32_t ___layermask3, int32_t ___queryTriggerInteraction4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.RaycastHit2D UnityEngine.Physics2D::Raycast(UnityEngine.Vector2,UnityEngine.Vector2,System.Single,System.Int32,System.Single,System.Single)
extern "C"  RaycastHit2D_t2279581989  Physics2D_Raycast_m4289230241 (Il2CppObject * __this /* static, unused */, Vector2_t2156229523  ___origin0, Vector2_t2156229523  ___direction1, float ___distance2, int32_t ___layerMask3, float ___minDepth4, float ___maxDepth5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.ContactFilter2D UnityEngine.ContactFilter2D::CreateLegacyFilter(System.Int32,System.Single,System.Single)
extern "C"  ContactFilter2D_t3805203441  ContactFilter2D_CreateLegacyFilter_m949420789 (Il2CppObject * __this /* static, unused */, int32_t ___layerMask0, float ___minDepth1, float ___maxDepth2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Physics2D::Internal_Raycast(UnityEngine.Vector2,UnityEngine.Vector2,System.Single,UnityEngine.ContactFilter2D,UnityEngine.RaycastHit2D&)
extern "C"  void Physics2D_Internal_Raycast_m4120652668 (Il2CppObject * __this /* static, unused */, Vector2_t2156229523  ___origin0, Vector2_t2156229523  ___direction1, float ___distance2, ContactFilter2D_t3805203441  ___contactFilter3, RaycastHit2D_t2279581989 * ___raycastHit4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Physics2D::Raycast(UnityEngine.Vector2,UnityEngine.Vector2,UnityEngine.ContactFilter2D,UnityEngine.RaycastHit2D[],System.Single)
extern "C"  int32_t Physics2D_Raycast_m787477166 (Il2CppObject * __this /* static, unused */, Vector2_t2156229523  ___origin0, Vector2_t2156229523  ___direction1, ContactFilter2D_t3805203441  ___contactFilter2, RaycastHit2DU5BU5D_t4286651560* ___results3, float ___distance4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Physics2D::Internal_RaycastNonAlloc(UnityEngine.Vector2,UnityEngine.Vector2,System.Single,UnityEngine.ContactFilter2D,UnityEngine.RaycastHit2D[])
extern "C"  int32_t Physics2D_Internal_RaycastNonAlloc_m3606848900 (Il2CppObject * __this /* static, unused */, Vector2_t2156229523  ___origin0, Vector2_t2156229523  ___direction1, float ___distance2, ContactFilter2D_t3805203441  ___contactFilter3, RaycastHit2DU5BU5D_t4286651560* ___results4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Physics2D::INTERNAL_CALL_Internal_Raycast(UnityEngine.Vector2&,UnityEngine.Vector2&,System.Single,UnityEngine.ContactFilter2D&,UnityEngine.RaycastHit2D&)
extern "C"  void Physics2D_INTERNAL_CALL_Internal_Raycast_m2882919538 (Il2CppObject * __this /* static, unused */, Vector2_t2156229523 * ___origin0, Vector2_t2156229523 * ___direction1, float ___distance2, ContactFilter2D_t3805203441 * ___contactFilter3, RaycastHit2D_t2279581989 * ___raycastHit4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Physics2D::INTERNAL_CALL_Internal_RaycastNonAlloc(UnityEngine.Vector2&,UnityEngine.Vector2&,System.Single,UnityEngine.ContactFilter2D&,UnityEngine.RaycastHit2D[])
extern "C"  int32_t Physics2D_INTERNAL_CALL_Internal_RaycastNonAlloc_m3718512001 (Il2CppObject * __this /* static, unused */, Vector2_t2156229523 * ___origin0, Vector2_t2156229523 * ___direction1, float ___distance2, ContactFilter2D_t3805203441 * ___contactFilter3, RaycastHit2DU5BU5D_t4286651560* ___results4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.RaycastHit2D[] UnityEngine.Physics2D::INTERNAL_CALL_GetRayIntersectionAll(UnityEngine.Ray&,System.Single,System.Int32)
extern "C"  RaycastHit2DU5BU5D_t4286651560* Physics2D_INTERNAL_CALL_GetRayIntersectionAll_m1699480086 (Il2CppObject * __this /* static, unused */, Ray_t3785851493 * ___ray0, float ___distance1, int32_t ___layerMask2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Collider2D UnityEngine.Physics2D::OverlapPoint(UnityEngine.Vector2,System.Int32,System.Single,System.Single)
extern "C"  Collider2D_t2806799626 * Physics2D_OverlapPoint_m1306494458 (Il2CppObject * __this /* static, unused */, Vector2_t2156229523  ___point0, int32_t ___layerMask1, float ___minDepth2, float ___maxDepth3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Collider2D UnityEngine.Physics2D::Internal_OverlapPoint(UnityEngine.Vector2,UnityEngine.ContactFilter2D)
extern "C"  Collider2D_t2806799626 * Physics2D_Internal_OverlapPoint_m1360518232 (Il2CppObject * __this /* static, unused */, Vector2_t2156229523  ___point0, ContactFilter2D_t3805203441  ___contactFilter1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Collider2D UnityEngine.Physics2D::INTERNAL_CALL_Internal_OverlapPoint(UnityEngine.Vector2&,UnityEngine.ContactFilter2D&)
extern "C"  Collider2D_t2806799626 * Physics2D_INTERNAL_CALL_Internal_OverlapPoint_m1361933589 (Il2CppObject * __this /* static, unused */, Vector2_t2156229523 * ___point0, ContactFilter2D_t3805203441 * ___contactFilter1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.List`1<UnityEngine.Rigidbody2D>::.ctor()
#define List_1__ctor_m1332252848(__this, method) ((  void (*) (List_1_t2411569343 *, const MethodInfo*))List_1__ctor_m2321703786_gshared)(__this, method)
// UnityEngine.Vector3 UnityEngine.Vector3::Normalize(UnityEngine.Vector3)
extern "C"  Vector3_t3722313464  Vector3_Normalize_m2336073400 (Il2CppObject * __this /* static, unused */, Vector3_t3722313464  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Vector3::Dot(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  float Vector3_Dot_m240955101 (Il2CppObject * __this /* static, unused */, Vector3_t3722313464  ___lhs0, Vector3_t3722313464  ___rhs1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Plane::.ctor(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  void Plane__ctor_m2107222851 (Plane_t1000493321 * __this, Vector3_t3722313464  ___inNormal0, Vector3_t3722313464  ___inPoint1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Plane::get_normal()
extern "C"  Vector3_t3722313464  Plane_get_normal_m4269270389 (Plane_t1000493321 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Plane::get_distance()
extern "C"  float Plane_get_distance_m266757971 (Plane_t1000493321 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Mathf::Approximately(System.Single,System.Single)
extern "C"  bool Mathf_Approximately_m367990089 (Il2CppObject * __this /* static, unused */, float ___a0, float ___b1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Plane::Raycast(UnityEngine.Ray,System.Single&)
extern "C"  bool Plane_Raycast_m1361173428 (Plane_t1000493321 * __this, Ray_t3785851493  ___ray0, float* ___enter1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Attribute::.ctor()
extern "C"  void Attribute__ctor_m1529526131 (Attribute_t861562559 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Quaternion::.ctor(System.Single,System.Single,System.Single,System.Single)
extern "C"  void Quaternion__ctor_m8311269 (Quaternion_t2301928331 * __this, float ___x0, float ___y1, float ___z2, float ___w3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Quaternion::INTERNAL_CALL_AngleAxis(System.Single,UnityEngine.Vector3&,UnityEngine.Quaternion&)
extern "C"  void Quaternion_INTERNAL_CALL_AngleAxis_m2146356369 (Il2CppObject * __this /* static, unused */, float ___angle0, Vector3_t3722313464 * ___axis1, Quaternion_t2301928331 * ___value2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Quaternion::Internal_ToAxisAngleRad(UnityEngine.Quaternion,UnityEngine.Vector3&,System.Single&)
extern "C"  void Quaternion_Internal_ToAxisAngleRad_m1241608229 (Il2CppObject * __this /* static, unused */, Quaternion_t2301928331  ___q0, Vector3_t3722313464 * ___axis1, float* ___angle2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Quaternion::ToAngleAxis(System.Single&,UnityEngine.Vector3&)
extern "C"  void Quaternion_ToAngleAxis_m3045484821 (Quaternion_t2301928331 * __this, float* ___angle0, Vector3_t3722313464 * ___axis1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Quaternion::INTERNAL_CALL_FromToRotation(UnityEngine.Vector3&,UnityEngine.Vector3&,UnityEngine.Quaternion&)
extern "C"  void Quaternion_INTERNAL_CALL_FromToRotation_m3176884020 (Il2CppObject * __this /* static, unused */, Vector3_t3722313464 * ___fromDirection0, Vector3_t3722313464 * ___toDirection1, Quaternion_t2301928331 * ___value2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Quaternion::INTERNAL_CALL_LookRotation(UnityEngine.Vector3&,UnityEngine.Vector3&,UnityEngine.Quaternion&)
extern "C"  void Quaternion_INTERNAL_CALL_LookRotation_m1837014090 (Il2CppObject * __this /* static, unused */, Vector3_t3722313464 * ___forward0, Vector3_t3722313464 * ___upwards1, Quaternion_t2301928331 * ___value2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Vector3::get_up()
extern "C"  Vector3_t3722313464  Vector3_get_up_m2851907508 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Quaternion::INTERNAL_CALL_Slerp(UnityEngine.Quaternion&,UnityEngine.Quaternion&,System.Single,UnityEngine.Quaternion&)
extern "C"  void Quaternion_INTERNAL_CALL_Slerp_m3390229344 (Il2CppObject * __this /* static, unused */, Quaternion_t2301928331 * ___a0, Quaternion_t2301928331 * ___b1, float ___t2, Quaternion_t2301928331 * ___value3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Quaternion::INTERNAL_CALL_Lerp(UnityEngine.Quaternion&,UnityEngine.Quaternion&,System.Single,UnityEngine.Quaternion&)
extern "C"  void Quaternion_INTERNAL_CALL_Lerp_m1337588159 (Il2CppObject * __this /* static, unused */, Quaternion_t2301928331 * ___a0, Quaternion_t2301928331 * ___b1, float ___t2, Quaternion_t2301928331 * ___value3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Quaternion::INTERNAL_CALL_Inverse(UnityEngine.Quaternion&,UnityEngine.Quaternion&)
extern "C"  void Quaternion_INTERNAL_CALL_Inverse_m1744399623 (Il2CppObject * __this /* static, unused */, Quaternion_t2301928331 * ___rotation0, Quaternion_t2301928331 * ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Quaternion::Internal_ToEulerRad(UnityEngine.Quaternion)
extern "C"  Vector3_t3722313464  Quaternion_Internal_ToEulerRad_m3601113129 (Il2CppObject * __this /* static, unused */, Quaternion_t2301928331  ___rotation0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Quaternion::Internal_MakePositive(UnityEngine.Vector3)
extern "C"  Vector3_t3722313464  Quaternion_Internal_MakePositive_m107199717 (Il2CppObject * __this /* static, unused */, Vector3_t3722313464  ___euler0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Quaternion::get_eulerAngles()
extern "C"  Vector3_t3722313464  Quaternion_get_eulerAngles_m2166503968 (Quaternion_t2301928331 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Quaternion UnityEngine.Quaternion::Internal_FromEulerRad(UnityEngine.Vector3)
extern "C"  Quaternion_t2301928331  Quaternion_Internal_FromEulerRad_m2596673477 (Il2CppObject * __this /* static, unused */, Vector3_t3722313464  ___euler0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Quaternion::set_eulerAngles(UnityEngine.Vector3)
extern "C"  void Quaternion_set_eulerAngles_m4032549493 (Quaternion_t2301928331 * __this, Vector3_t3722313464  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Quaternion::INTERNAL_CALL_Internal_ToEulerRad(UnityEngine.Quaternion&,UnityEngine.Vector3&)
extern "C"  void Quaternion_INTERNAL_CALL_Internal_ToEulerRad_m3918304901 (Il2CppObject * __this /* static, unused */, Quaternion_t2301928331 * ___rotation0, Vector3_t3722313464 * ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Quaternion::INTERNAL_CALL_Internal_FromEulerRad(UnityEngine.Vector3&,UnityEngine.Quaternion&)
extern "C"  void Quaternion_INTERNAL_CALL_Internal_FromEulerRad_m1327416487 (Il2CppObject * __this /* static, unused */, Vector3_t3722313464 * ___euler0, Quaternion_t2301928331 * ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Quaternion::INTERNAL_CALL_Internal_ToAxisAngleRad(UnityEngine.Quaternion&,UnityEngine.Vector3&,System.Single&)
extern "C"  void Quaternion_INTERNAL_CALL_Internal_ToAxisAngleRad_m2865891006 (Il2CppObject * __this /* static, unused */, Quaternion_t2301928331 * ___q0, Vector3_t3722313464 * ___axis1, float* ___angle2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Quaternion::Dot(UnityEngine.Quaternion,UnityEngine.Quaternion)
extern "C"  float Quaternion_Dot_m3405174114 (Il2CppObject * __this /* static, unused */, Quaternion_t2301928331  ___a0, Quaternion_t2301928331  ___b1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Quaternion::op_Equality(UnityEngine.Quaternion,UnityEngine.Quaternion)
extern "C"  bool Quaternion_op_Equality_m1820728316 (Il2CppObject * __this /* static, unused */, Quaternion_t2301928331  ___lhs0, Quaternion_t2301928331  ___rhs1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Mathf::Min(System.Single,System.Single)
extern "C"  float Mathf_Min_m1901831667 (Il2CppObject * __this /* static, unused */, float ___a0, float ___b1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Single::GetHashCode()
extern "C"  int32_t Single_GetHashCode_m1558506138 (float* __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Quaternion::GetHashCode()
extern "C"  int32_t Quaternion_GetHashCode_m3399187295 (Quaternion_t2301928331 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Single::Equals(System.Single)
extern "C"  bool Single_Equals_m1601893879 (float* __this, float p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Quaternion::Equals(System.Object)
extern "C"  bool Quaternion_Equals_m1217639185 (Quaternion_t2301928331 * __this, Il2CppObject * ___other0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.UnityString::Format(System.String,System.Object[])
extern "C"  String_t* UnityString_Format_m3741272017 (Il2CppObject * __this /* static, unused */, String_t* ___fmt0, ObjectU5BU5D_t2843939325* ___args1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.Quaternion::ToString()
extern "C"  String_t* Quaternion_ToString_m127166294 (Quaternion_t2301928331 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Random::RandomRangeInt(System.Int32,System.Int32)
extern "C"  int32_t Random_RandomRangeInt_m3922158605 (Il2CppObject * __this /* static, unused */, int32_t ___min0, int32_t ___max1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Random::INTERNAL_get_insideUnitSphere(UnityEngine.Vector3&)
extern "C"  void Random_INTERNAL_get_insideUnitSphere_m2096637322 (Il2CppObject * __this /* static, unused */, Vector3_t3722313464 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.PropertyAttribute::.ctor()
extern "C"  void PropertyAttribute__ctor_m3954845021 (PropertyAttribute_t3677895545 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.RangeInt::get_end()
extern "C"  int32_t RangeInt_get_end_m2761784640 (RangeInt_t2094684618 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Vector3::get_normalized()
extern "C"  Vector3_t3722313464  Vector3_get_normalized_m1684899259 (Vector3_t3722313464 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Ray::.ctor(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  void Ray__ctor_m2095760679 (Ray_t3785851493 * __this, Vector3_t3722313464  ___origin0, Vector3_t3722313464  ___direction1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Ray::set_origin(UnityEngine.Vector3)
extern "C"  void Ray_set_origin_m1444122105 (Ray_t3785851493 * __this, Vector3_t3722313464  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Vector3::op_Addition(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  Vector3_t3722313464  Vector3_op_Addition_m1781942663 (Il2CppObject * __this /* static, unused */, Vector3_t3722313464  ___a0, Vector3_t3722313464  ___b1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Ray::GetPoint(System.Single)
extern "C"  Vector3_t3722313464  Ray_GetPoint_m2674993148 (Ray_t3785851493 * __this, float ___distance0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.Ray::ToString()
extern "C"  String_t* Ray_ToString_m943070480 (Ray_t3785851493 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.RaycastHit::get_point()
extern "C"  Vector3_t3722313464  RaycastHit_get_point_m177058539 (RaycastHit_t1056001966 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.RaycastHit::get_normal()
extern "C"  Vector3_t3722313464  RaycastHit_get_normal_m591687880 (RaycastHit_t1056001966 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.RaycastHit::get_distance()
extern "C"  float RaycastHit_get_distance_m2539638163 (RaycastHit_t1056001966 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Collider UnityEngine.RaycastHit::get_collider()
extern "C"  Collider_t1773347010 * RaycastHit_get_collider_m1442240336 (RaycastHit_t1056001966 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Rigidbody UnityEngine.Collider::get_attachedRigidbody()
extern "C"  Rigidbody_t3916780224 * Collider_get_attachedRigidbody_m4203494256 (Collider_t1773347010 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Rigidbody UnityEngine.RaycastHit::get_rigidbody()
extern "C"  Rigidbody_t3916780224 * RaycastHit_get_rigidbody_m846276486 (RaycastHit_t1056001966 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform UnityEngine.RaycastHit::get_transform()
extern "C"  Transform_t3600365921 * RaycastHit_get_transform_m2094454139 (RaycastHit_t1056001966 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.RaycastHit2D::get_point()
extern "C"  Vector2_t2156229523  RaycastHit2D_get_point_m3958944977 (RaycastHit2D_t2279581989 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.RaycastHit2D::get_normal()
extern "C"  Vector2_t2156229523  RaycastHit2D_get_normal_m3675591968 (RaycastHit2D_t2279581989 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.RaycastHit2D::get_fraction()
extern "C"  float RaycastHit2D_get_fraction_m2738084453 (RaycastHit2D_t2279581989 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Collider2D UnityEngine.RaycastHit2D::get_collider()
extern "C"  Collider2D_t2806799626 * RaycastHit2D_get_collider_m1860250292 (RaycastHit2D_t2279581989 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Rect::.ctor(System.Single,System.Single,System.Single,System.Single)
extern "C"  void Rect__ctor_m2635848439 (Rect_t2360479859 * __this, float ___x0, float ___y1, float ___width2, float ___height3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Rect::get_x()
extern "C"  float Rect_get_x_m3218181674 (Rect_t2360479859 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Rect::set_x(System.Single)
extern "C"  void Rect_set_x_m534803876 (Rect_t2360479859 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Rect::get_y()
extern "C"  float Rect_get_y_m3218181675 (Rect_t2360479859 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Rect::set_y(System.Single)
extern "C"  void Rect_set_y_m946788033 (Rect_t2360479859 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Vector2::.ctor(System.Single,System.Single)
extern "C"  void Vector2__ctor_m4060800441 (Vector2_t2156229523 * __this, float ___x0, float ___y1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.Rect::get_position()
extern "C"  Vector2_t2156229523  Rect_get_position_m2767609553 (Rect_t2360479859 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.Rect::get_center()
extern "C"  Vector2_t2156229523  Rect_get_center_m182049623 (Rect_t2360479859 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Rect::set_center(UnityEngine.Vector2)
extern "C"  void Rect_set_center_m4014133992 (Rect_t2360479859 * __this, Vector2_t2156229523  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Rect::get_xMin()
extern "C"  float Rect_get_xMin_m2659235730 (Rect_t2360479859 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Rect::get_yMin()
extern "C"  float Rect_get_yMin_m2659235699 (Rect_t2360479859 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.Rect::get_min()
extern "C"  Vector2_t2156229523  Rect_get_min_m3948069389 (Rect_t2360479859 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Rect::get_xMax()
extern "C"  float Rect_get_xMax_m2926206058 (Rect_t2360479859 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Rect::get_yMax()
extern "C"  float Rect_get_yMax_m2926206027 (Rect_t2360479859 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.Rect::get_max()
extern "C"  Vector2_t2156229523  Rect_get_max_m724222499 (Rect_t2360479859 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Rect::get_width()
extern "C"  float Rect_get_width_m3421965717 (Rect_t2360479859 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Rect::set_width(System.Single)
extern "C"  void Rect_set_width_m2263092238 (Rect_t2360479859 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Rect::get_height()
extern "C"  float Rect_get_height_m977101306 (Rect_t2360479859 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Rect::set_height(System.Single)
extern "C"  void Rect_set_height_m230303970 (Rect_t2360479859 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.Rect::get_size()
extern "C"  Vector2_t2156229523  Rect_get_size_m3542039952 (Rect_t2360479859 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Rect::set_xMin(System.Single)
extern "C"  void Rect_set_xMin_m2775136022 (Rect_t2360479859 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Rect::set_yMin(System.Single)
extern "C"  void Rect_set_yMin_m415158369 (Rect_t2360479859 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Rect::set_xMax(System.Single)
extern "C"  void Rect_set_xMax_m4050361423 (Rect_t2360479859 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Rect::set_yMax(System.Single)
extern "C"  void Rect_set_yMax_m1690383770 (Rect_t2360479859 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Rect::Contains(UnityEngine.Vector2)
extern "C"  bool Rect_Contains_m2308869928 (Rect_t2360479859 * __this, Vector2_t2156229523  ___point0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Rect::Contains(UnityEngine.Vector3)
extern "C"  bool Rect_Contains_m2308869927 (Rect_t2360479859 * __this, Vector3_t3722313464  ___point0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Rect::Overlaps(UnityEngine.Rect)
extern "C"  bool Rect_Overlaps_m743951238 (Rect_t2360479859 * __this, Rect_t2360479859  ___other0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Rect UnityEngine.Rect::OrderMinMax(UnityEngine.Rect)
extern "C"  Rect_t2360479859  Rect_OrderMinMax_m3546425063 (Il2CppObject * __this /* static, unused */, Rect_t2360479859  ___rect0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Rect::Overlaps(UnityEngine.Rect,System.Boolean)
extern "C"  bool Rect_Overlaps_m6060363 (Rect_t2360479859 * __this, Rect_t2360479859  ___other0, bool ___allowInverse1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Rect::op_Equality(UnityEngine.Rect,UnityEngine.Rect)
extern "C"  bool Rect_op_Equality_m1072810924 (Il2CppObject * __this /* static, unused */, Rect_t2360479859  ___lhs0, Rect_t2360479859  ___rhs1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Rect::GetHashCode()
extern "C"  int32_t Rect_GetHashCode_m637011824 (Rect_t2360479859 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Rect::Equals(System.Object)
extern "C"  bool Rect_Equals_m866904981 (Rect_t2360479859 * __this, Il2CppObject * ___other0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.Rect::ToString()
extern "C"  String_t* Rect_ToString_m2230825896 (Rect_t2360479859 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.RectOffset::Init()
extern "C"  void RectOffset_Init_m3786816665 (RectOffset_t1369453676 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.RectOffset::Cleanup()
extern "C"  void RectOffset_Cleanup_m83064950 (RectOffset_t1369453676 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Object::Finalize()
extern "C"  void Object_Finalize_m3076187857 (Il2CppObject * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.RectOffset::get_left()
extern "C"  int32_t RectOffset_get_left_m1621853196 (RectOffset_t1369453676 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.RectOffset::get_right()
extern "C"  int32_t RectOffset_get_right_m1276944919 (RectOffset_t1369453676 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.RectOffset::get_top()
extern "C"  int32_t RectOffset_get_top_m2983626523 (RectOffset_t1369453676 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.RectOffset::get_bottom()
extern "C"  int32_t RectOffset_get_bottom_m512985341 (RectOffset_t1369453676 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.RectTransform::INTERNAL_get_rect(UnityEngine.Rect&)
extern "C"  void RectTransform_INTERNAL_get_rect_m1623267656 (RectTransform_t3704657025 * __this, Rect_t2360479859 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.RectTransform::INTERNAL_get_anchorMin(UnityEngine.Vector2&)
extern "C"  void RectTransform_INTERNAL_get_anchorMin_m1256957125 (RectTransform_t3704657025 * __this, Vector2_t2156229523 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.RectTransform::INTERNAL_set_anchorMin(UnityEngine.Vector2&)
extern "C"  void RectTransform_INTERNAL_set_anchorMin_m776038519 (RectTransform_t3704657025 * __this, Vector2_t2156229523 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.RectTransform::INTERNAL_get_anchorMax(UnityEngine.Vector2&)
extern "C"  void RectTransform_INTERNAL_get_anchorMax_m2950850437 (RectTransform_t3704657025 * __this, Vector2_t2156229523 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.RectTransform::INTERNAL_set_anchorMax(UnityEngine.Vector2&)
extern "C"  void RectTransform_INTERNAL_set_anchorMax_m4025114487 (RectTransform_t3704657025 * __this, Vector2_t2156229523 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.RectTransform::set_anchoredPosition(UnityEngine.Vector2)
extern "C"  void RectTransform_set_anchoredPosition_m1454079598 (RectTransform_t3704657025 * __this, Vector2_t2156229523  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Transform::get_localPosition()
extern "C"  Vector3_t3722313464  Transform_get_localPosition_m265057664 (Transform_t3600365921 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Transform::set_localPosition(UnityEngine.Vector3)
extern "C"  void Transform_set_localPosition_m3327877514 (Transform_t3600365921 * __this, Vector3_t3722313464  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.RectTransform::INTERNAL_get_anchoredPosition(UnityEngine.Vector2&)
extern "C"  void RectTransform_INTERNAL_get_anchoredPosition_m918664281 (RectTransform_t3704657025 * __this, Vector2_t2156229523 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.RectTransform::INTERNAL_set_anchoredPosition(UnityEngine.Vector2&)
extern "C"  void RectTransform_INTERNAL_set_anchoredPosition_m3924630850 (RectTransform_t3704657025 * __this, Vector2_t2156229523 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.RectTransform::INTERNAL_get_sizeDelta(UnityEngine.Vector2&)
extern "C"  void RectTransform_INTERNAL_get_sizeDelta_m627343153 (RectTransform_t3704657025 * __this, Vector2_t2156229523 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.RectTransform::INTERNAL_set_sizeDelta(UnityEngine.Vector2&)
extern "C"  void RectTransform_INTERNAL_set_sizeDelta_m2933696437 (RectTransform_t3704657025 * __this, Vector2_t2156229523 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.RectTransform::INTERNAL_get_pivot(UnityEngine.Vector2&)
extern "C"  void RectTransform_INTERNAL_get_pivot_m878744508 (RectTransform_t3704657025 * __this, Vector2_t2156229523 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.RectTransform::INTERNAL_set_pivot(UnityEngine.Vector2&)
extern "C"  void RectTransform_INTERNAL_set_pivot_m1286097822 (RectTransform_t3704657025 * __this, Vector2_t2156229523 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Delegate System.Delegate::Combine(System.Delegate,System.Delegate)
extern "C"  Delegate_t1188392813 * Delegate_Combine_m1859655160 (Il2CppObject * __this /* static, unused */, Delegate_t1188392813 * p0, Delegate_t1188392813 * p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Delegate System.Delegate::Remove(System.Delegate,System.Delegate)
extern "C"  Delegate_t1188392813 * Delegate_Remove_m334097152 (Il2CppObject * __this /* static, unused */, Delegate_t1188392813 * p0, Delegate_t1188392813 * p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.RectTransform/ReapplyDrivenProperties::Invoke(UnityEngine.RectTransform)
extern "C"  void ReapplyDrivenProperties_Invoke_m3357881462 (ReapplyDrivenProperties_t1258266594 * __this, RectTransform_t3704657025 * ___driven0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Debug::LogError(System.Object)
extern "C"  void Debug_LogError_m2059623341 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Rect UnityEngine.RectTransform::get_rect()
extern "C"  Rect_t2360479859  RectTransform_get_rect_m1643570810 (RectTransform_t3704657025 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.RectTransform::GetLocalCorners(UnityEngine.Vector3[])
extern "C"  void RectTransform_GetLocalCorners_m3023515545 (RectTransform_t3704657025 * __this, Vector3U5BU5D_t1718750761* ___fourCornersArray0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Transform::TransformPoint(UnityEngine.Vector3)
extern "C"  Vector3_t3722313464  Transform_TransformPoint_m3184824030 (Transform_t3600365921 * __this, Vector3_t3722313464  ___position0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.RectTransform::get_anchoredPosition()
extern "C"  Vector2_t2156229523  RectTransform_get_anchoredPosition_m287009682 (RectTransform_t3704657025 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.RectTransform::get_sizeDelta()
extern "C"  Vector2_t2156229523  RectTransform_get_sizeDelta_m2136908840 (RectTransform_t3704657025 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.RectTransform::get_pivot()
extern "C"  Vector2_t2156229523  RectTransform_get_pivot_m1676241928 (RectTransform_t3704657025 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.Vector2::Scale(UnityEngine.Vector2,UnityEngine.Vector2)
extern "C"  Vector2_t2156229523  Vector2_Scale_m110064162 (Il2CppObject * __this /* static, unused */, Vector2_t2156229523  ___a0, Vector2_t2156229523  ___b1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.Vector2::op_Subtraction(UnityEngine.Vector2,UnityEngine.Vector2)
extern "C"  Vector2_t2156229523  Vector2_op_Subtraction_m1387382396 (Il2CppObject * __this /* static, unused */, Vector2_t2156229523  ___a0, Vector2_t2156229523  ___b1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.RectTransform::set_sizeDelta(UnityEngine.Vector2)
extern "C"  void RectTransform_set_sizeDelta_m344906562 (RectTransform_t3704657025 * __this, Vector2_t2156229523  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.Vector2::get_one()
extern "C"  Vector2_t2156229523  Vector2_get_one_m3275444361 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.Vector2::op_Addition(UnityEngine.Vector2,UnityEngine.Vector2)
extern "C"  Vector2_t2156229523  Vector2_op_Addition_m2157034339 (Il2CppObject * __this /* static, unused */, Vector2_t2156229523  ___a0, Vector2_t2156229523  ___b1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.RectTransform::get_anchorMin()
extern "C"  Vector2_t2156229523  RectTransform_get_anchorMin_m1342910522 (RectTransform_t3704657025 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Vector2::set_Item(System.Int32,System.Single)
extern "C"  void Vector2_set_Item_m1664083694 (Vector2_t2156229523 * __this, int32_t ___index0, float ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.RectTransform::set_anchorMin(UnityEngine.Vector2)
extern "C"  void RectTransform_set_anchorMin_m2068858122 (RectTransform_t3704657025 * __this, Vector2_t2156229523  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.RectTransform::get_anchorMax()
extern "C"  Vector2_t2156229523  RectTransform_get_anchorMax_m1075940194 (RectTransform_t3704657025 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.RectTransform::set_anchorMax(UnityEngine.Vector2)
extern "C"  void RectTransform_set_anchorMax_m2389806509 (RectTransform_t3704657025 * __this, Vector2_t2156229523  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Vector2::get_Item(System.Int32)
extern "C"  float Vector2_get_Item_m3129197029 (Vector2_t2156229523 * __this, int32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.RectTransform::GetParentSize()
extern "C"  Vector2_t2156229523  RectTransform_GetParentSize_m3805664119 (RectTransform_t3704657025 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform UnityEngine.Transform::get_parent()
extern "C"  Transform_t3600365921 * Transform_get_parent_m1293647796 (Transform_t3600365921 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Object::op_Implicit(UnityEngine.Object)
extern "C"  bool Object_op_Implicit_m487959476 (Il2CppObject * __this /* static, unused */, Object_t631007953 * ___exists0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.Vector2::get_zero()
extern "C"  Vector2_t2156229523  Vector2_get_zero_m3700876535 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Vector2::op_Implicit(UnityEngine.Vector2)
extern "C"  Vector3_t3722313464  Vector2_op_Implicit_m1988559315 (Il2CppObject * __this /* static, unused */, Vector2_t2156229523  ___v0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Ray UnityEngine.RectTransformUtility::ScreenPointToRay(UnityEngine.Camera,UnityEngine.Vector2)
extern "C"  Ray_t3785851493  RectTransformUtility_ScreenPointToRay_m3051033457 (Il2CppObject * __this /* static, unused */, Camera_t4157153871 * ___cam0, Vector2_t2156229523  ___screenPos1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Quaternion UnityEngine.Transform::get_rotation()
extern "C"  Quaternion_t2301928331  Transform_get_rotation_m2794111148 (Transform_t3600365921 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Vector3::get_back()
extern "C"  Vector3_t3722313464  Vector3_get_back_m4227631978 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Quaternion::op_Multiply(UnityEngine.Quaternion,UnityEngine.Vector3)
extern "C"  Vector3_t3722313464  Quaternion_op_Multiply_m1022106983 (Il2CppObject * __this /* static, unused */, Quaternion_t2301928331  ___rotation0, Vector3_t3722313464  ___point1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Transform::get_position()
extern "C"  Vector3_t3722313464  Transform_get_position_m102368104 (Transform_t3600365921 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.RectTransformUtility::ScreenPointToWorldPointInRectangle(UnityEngine.RectTransform,UnityEngine.Vector2,UnityEngine.Camera,UnityEngine.Vector3&)
extern "C"  bool RectTransformUtility_ScreenPointToWorldPointInRectangle_m1236369340 (Il2CppObject * __this /* static, unused */, RectTransform_t3704657025 * ___rect0, Vector2_t2156229523  ___screenPoint1, Camera_t4157153871 * ___cam2, Vector3_t3722313464 * ___worldPoint3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Transform::InverseTransformPoint(UnityEngine.Vector3)
extern "C"  Vector3_t3722313464  Transform_InverseTransformPoint_m1254110475 (Transform_t3600365921 * __this, Vector3_t3722313464  ___position0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.Vector2::op_Implicit(UnityEngine.Vector3)
extern "C"  Vector2_t2156229523  Vector2_op_Implicit_m1304503157 (Il2CppObject * __this /* static, unused */, Vector3_t3722313464  ___v0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Ray UnityEngine.Camera::ScreenPointToRay(UnityEngine.Vector3)
extern "C"  Ray_t3785851493  Camera_ScreenPointToRay_m1522780915 (Camera_t4157153871 * __this, Vector3_t3722313464  ___position0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Vector3::get_forward()
extern "C"  Vector3_t3722313464  Vector3_get_forward_m2293047824 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.RectTransformUtility::FlipLayoutOnAxis(UnityEngine.RectTransform,System.Int32,System.Boolean,System.Boolean)
extern "C"  void RectTransformUtility_FlipLayoutOnAxis_m144305054 (Il2CppObject * __this /* static, unused */, RectTransform_t3704657025 * ___rect0, int32_t ___axis1, bool ___keepPositioning2, bool ___recursive3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.RectTransform::set_pivot(UnityEngine.Vector2)
extern "C"  void RectTransform_set_pivot_m3678254456 (RectTransform_t3704657025 * __this, Vector2_t2156229523  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.RectTransformUtility::FlipLayoutAxes(UnityEngine.RectTransform,System.Boolean,System.Boolean)
extern "C"  void RectTransformUtility_FlipLayoutAxes_m3705369832 (Il2CppObject * __this /* static, unused */, RectTransform_t3704657025 * ___rect0, bool ___keepPositioning1, bool ___recursive2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.RectTransformUtility::GetTransposed(UnityEngine.Vector2)
extern "C"  Vector2_t2156229523  RectTransformUtility_GetTransposed_m440700723 (Il2CppObject * __this /* static, unused */, Vector2_t2156229523  ___input0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.RectTransformUtility::INTERNAL_CALL_RectangleContainsScreenPoint(UnityEngine.RectTransform,UnityEngine.Vector2&,UnityEngine.Camera)
extern "C"  bool RectTransformUtility_INTERNAL_CALL_RectangleContainsScreenPoint_m522477797 (Il2CppObject * __this /* static, unused */, RectTransform_t3704657025 * ___rect0, Vector2_t2156229523 * ___screenPoint1, Camera_t4157153871 * ___cam2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.RectTransformUtility::INTERNAL_CALL_PixelAdjustPoint(UnityEngine.Vector2&,UnityEngine.Transform,UnityEngine.Canvas,UnityEngine.Vector2&)
extern "C"  void RectTransformUtility_INTERNAL_CALL_PixelAdjustPoint_m2292821422 (Il2CppObject * __this /* static, unused */, Vector2_t2156229523 * ___point0, Transform_t3600365921 * ___elementTransform1, Canvas_t3310196443 * ___canvas2, Vector2_t2156229523 * ___value3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.RectTransformUtility::INTERNAL_CALL_PixelAdjustRect(UnityEngine.RectTransform,UnityEngine.Canvas,UnityEngine.Rect&)
extern "C"  void RectTransformUtility_INTERNAL_CALL_PixelAdjustRect_m1199355208 (Il2CppObject * __this /* static, unused */, RectTransform_t3704657025 * ___rectTransform0, Canvas_t3310196443 * ___canvas1, Rect_t2360479859 * ___value2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.RemoteSettings/UpdatedEventHandler::Invoke()
extern "C"  void UpdatedEventHandler_Invoke_m631751593 (UpdatedEventHandler_t1027848393 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Renderer::INTERNAL_get_bounds(UnityEngine.Bounds&)
extern "C"  void Renderer_INTERNAL_get_bounds_m2341376052 (Renderer_t2627027031 * __this, Bounds_t2266837910 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Renderer::INTERNAL_get_lightmapScaleOffset(UnityEngine.Vector4&)
extern "C"  void Renderer_INTERNAL_get_lightmapScaleOffset_m1480402966 (Renderer_t2627027031 * __this, Vector4_t3319028937 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Renderer::INTERNAL_set_lightmapScaleOffset(UnityEngine.Vector4&)
extern "C"  void Renderer_INTERNAL_set_lightmapScaleOffset_m2266823654 (Renderer_t2627027031 * __this, Vector4_t3319028937 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Rendering.CommandBuffer::InitBuffer(UnityEngine.Rendering.CommandBuffer)
extern "C"  void CommandBuffer_InitBuffer_m1799175099 (Il2CppObject * __this /* static, unused */, CommandBuffer_t2206337031 * ___buf0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Rendering.CommandBuffer::Dispose(System.Boolean)
extern "C"  void CommandBuffer_Dispose_m811468491 (CommandBuffer_t2206337031 * __this, bool ___disposing0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.GC::SuppressFinalize(System.Object)
extern "C"  void GC_SuppressFinalize_m1177400158 (Il2CppObject * __this /* static, unused */, Il2CppObject * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Rendering.CommandBuffer::ReleaseBuffer()
extern "C"  void CommandBuffer_ReleaseBuffer_m2596961287 (CommandBuffer_t2206337031 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Rendering.CommandBuffer::DrawMesh(UnityEngine.Mesh,UnityEngine.Matrix4x4,UnityEngine.Material,System.Int32,System.Int32,UnityEngine.MaterialPropertyBlock)
extern "C"  void CommandBuffer_DrawMesh_m1771992862 (CommandBuffer_t2206337031 * __this, Mesh_t3648964284 * ___mesh0, Matrix4x4_t1817901843  ___matrix1, Material_t340375123 * ___material2, int32_t ___submeshIndex3, int32_t ___shaderPass4, MaterialPropertyBlock_t3213117958 * ___properties5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.ArgumentNullException::.ctor(System.String)
extern "C"  void ArgumentNullException__ctor_m1170824041 (ArgumentNullException_t1615371798 * __this, String_t* p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Mesh::get_subMeshCount()
extern "C"  int32_t Mesh_get_subMeshCount_m652342060 (Mesh_t3648964284 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Mathf::Clamp(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t Mathf_Clamp_m2702811178 (Il2CppObject * __this /* static, unused */, int32_t ___value0, int32_t ___min1, int32_t ___max2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.String::Format(System.String,System.Object)
extern "C"  String_t* String_Format_m2844511972 (Il2CppObject * __this /* static, unused */, String_t* p0, Il2CppObject * p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Debug::LogWarning(System.Object)
extern "C"  void Debug_LogWarning_m3661709751 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Rendering.CommandBuffer::Internal_DrawMesh(UnityEngine.Mesh,UnityEngine.Matrix4x4,UnityEngine.Material,System.Int32,System.Int32,UnityEngine.MaterialPropertyBlock)
extern "C"  void CommandBuffer_Internal_DrawMesh_m3767491291 (CommandBuffer_t2206337031 * __this, Mesh_t3648964284 * ___mesh0, Matrix4x4_t1817901843  ___matrix1, Material_t340375123 * ___material2, int32_t ___submeshIndex3, int32_t ___shaderPass4, MaterialPropertyBlock_t3213117958 * ___properties5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Rendering.CommandBuffer::INTERNAL_CALL_Internal_DrawMesh(UnityEngine.Rendering.CommandBuffer,UnityEngine.Mesh,UnityEngine.Matrix4x4&,UnityEngine.Material,System.Int32,System.Int32,UnityEngine.MaterialPropertyBlock)
extern "C"  void CommandBuffer_INTERNAL_CALL_Internal_DrawMesh_m3335436528 (Il2CppObject * __this /* static, unused */, CommandBuffer_t2206337031 * ___self0, Mesh_t3648964284 * ___mesh1, Matrix4x4_t1817901843 * ___matrix2, Material_t340375123 * ___material3, int32_t ___submeshIndex4, int32_t ___shaderPass5, MaterialPropertyBlock_t3213117958 * ___properties6, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Rendering.CommandBuffer::DrawRenderer(UnityEngine.Renderer,UnityEngine.Material,System.Int32,System.Int32)
extern "C"  void CommandBuffer_DrawRenderer_m3940925401 (CommandBuffer_t2206337031 * __this, Renderer_t2627027031 * ___renderer0, Material_t340375123 * ___material1, int32_t ___submeshIndex2, int32_t ___shaderPass3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Mathf::Max(System.Int32,System.Int32)
extern "C"  int32_t Mathf_Max_m2931752728 (Il2CppObject * __this /* static, unused */, int32_t ___a0, int32_t ___b1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Rendering.CommandBuffer::Internal_DrawRenderer(UnityEngine.Renderer,UnityEngine.Material,System.Int32,System.Int32)
extern "C"  void CommandBuffer_Internal_DrawRenderer_m3421431924 (CommandBuffer_t2206337031 * __this, Renderer_t2627027031 * ___renderer0, Material_t340375123 * ___material1, int32_t ___submeshIndex2, int32_t ___shaderPass3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Rendering.CommandBuffer::SetRenderTarget_Single(UnityEngine.Rendering.RenderTargetIdentifier&,System.Int32,UnityEngine.CubemapFace,System.Int32)
extern "C"  void CommandBuffer_SetRenderTarget_Single_m963511059 (CommandBuffer_t2206337031 * __this, RenderTargetIdentifier_t2079184500 * ___rt0, int32_t ___mipLevel1, int32_t ___cubemapFace2, int32_t ___depthSlice3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Rendering.CommandBuffer::Blit_Texture(UnityEngine.Texture,UnityEngine.Rendering.RenderTargetIdentifier&,UnityEngine.Material,System.Int32)
extern "C"  void CommandBuffer_Blit_Texture_m3866661524 (CommandBuffer_t2206337031 * __this, Texture_t3661962703 * ___source0, RenderTargetIdentifier_t2079184500 * ___dest1, Material_t340375123 * ___mat2, int32_t ___pass3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Rendering.CommandBuffer::Blit_Identifier(UnityEngine.Rendering.RenderTargetIdentifier&,UnityEngine.Rendering.RenderTargetIdentifier&,UnityEngine.Material,System.Int32)
extern "C"  void CommandBuffer_Blit_Identifier_m669956952 (CommandBuffer_t2206337031 * __this, RenderTargetIdentifier_t2079184500 * ___source0, RenderTargetIdentifier_t2079184500 * ___dest1, Material_t340375123 * ___mat2, int32_t ___pass3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Rendering.CommandBuffer::GetTemporaryRT(System.Int32,System.Int32,System.Int32,System.Int32,UnityEngine.FilterMode,UnityEngine.RenderTextureFormat,UnityEngine.RenderTextureReadWrite,System.Int32,System.Boolean)
extern "C"  void CommandBuffer_GetTemporaryRT_m2964136030 (CommandBuffer_t2206337031 * __this, int32_t ___nameID0, int32_t ___width1, int32_t ___height2, int32_t ___depthBuffer3, int32_t ___filter4, int32_t ___format5, int32_t ___readWrite6, int32_t ___antiAliasing7, bool ___enableRandomWrite8, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Rendering.CommandBuffer::INTERNAL_CALL_ClearRenderTarget(UnityEngine.Rendering.CommandBuffer,System.Boolean,System.Boolean,UnityEngine.Color&,System.Single)
extern "C"  void CommandBuffer_INTERNAL_CALL_ClearRenderTarget_m4109146477 (Il2CppObject * __this /* static, unused */, CommandBuffer_t2206337031 * ___self0, bool ___clearDepth1, bool ___clearColor2, Color_t2555686324 * ___backgroundColor3, float ___depth4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Rendering.RenderTargetIdentifier::.ctor(UnityEngine.Rendering.BuiltinRenderTextureType)
extern "C"  void RenderTargetIdentifier__ctor_m59141446 (RenderTargetIdentifier_t2079184500 * __this, int32_t ___type0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Rendering.RenderTargetIdentifier::.ctor(System.Int32)
extern "C"  void RenderTargetIdentifier__ctor_m1871346085 (RenderTargetIdentifier_t2079184500 * __this, int32_t ___nameID0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Object::GetInstanceID()
extern "C"  int32_t Object_GetInstanceID_m3267067959 (Object_t631007953 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Rendering.RenderTargetIdentifier::.ctor(UnityEngine.Texture)
extern "C"  void RenderTargetIdentifier__ctor_m2131406890 (RenderTargetIdentifier_t2079184500 * __this, Texture_t3661962703 * ___tex0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.Rendering.RenderTargetIdentifier::ToString()
extern "C"  String_t* RenderTargetIdentifier_ToString_m1876804468 (RenderTargetIdentifier_t2079184500 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Int32::GetHashCode()
extern "C"  int32_t Int32_GetHashCode_m1876651407 (int32_t* __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Rendering.RenderTargetIdentifier::GetHashCode()
extern "C"  int32_t RenderTargetIdentifier_GetHashCode_m1613881668 (RenderTargetIdentifier_t2079184500 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Rendering.RenderTargetIdentifier::Equals(System.Object)
extern "C"  bool RenderTargetIdentifier_Equals_m2411882201 (RenderTargetIdentifier_t2079184500 * __this, Il2CppObject * ___obj0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Texture::.ctor()
extern "C"  void Texture__ctor_m3534016699 (Texture_t3661962703 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.RenderTexture::Internal_CreateRenderTexture(UnityEngine.RenderTexture)
extern "C"  void RenderTexture_Internal_CreateRenderTexture_m2106023999 (Il2CppObject * __this /* static, unused */, RenderTexture_t2108887433 * ___rt0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.RenderTexture::set_depth(System.Int32)
extern "C"  void RenderTexture_set_depth_m1608130061 (RenderTexture_t2108887433 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.RenderTexture::set_format(UnityEngine.RenderTextureFormat)
extern "C"  void RenderTexture_set_format_m1605175643 (RenderTexture_t2108887433 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.ColorSpace UnityEngine.QualitySettings::get_activeColorSpace()
extern "C"  int32_t QualitySettings_get_activeColorSpace_m1763800780 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.RenderTexture::Internal_SetSRGBReadWrite(UnityEngine.RenderTexture,System.Boolean)
extern "C"  void RenderTexture_Internal_SetSRGBReadWrite_m4197499185 (Il2CppObject * __this /* static, unused */, RenderTexture_t2108887433 * ___mono0, bool ___sRGB1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.RenderTexture::Internal_GetWidth(UnityEngine.RenderTexture)
extern "C"  int32_t RenderTexture_Internal_GetWidth_m3496340582 (Il2CppObject * __this /* static, unused */, RenderTexture_t2108887433 * ___mono0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.RenderTexture::Internal_SetWidth(UnityEngine.RenderTexture,System.Int32)
extern "C"  void RenderTexture_Internal_SetWidth_m4105368391 (Il2CppObject * __this /* static, unused */, RenderTexture_t2108887433 * ___mono0, int32_t ___width1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.RenderTexture::Internal_GetHeight(UnityEngine.RenderTexture)
extern "C"  int32_t RenderTexture_Internal_GetHeight_m3135665775 (Il2CppObject * __this /* static, unused */, RenderTexture_t2108887433 * ___mono0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.RenderTexture::Internal_SetHeight(UnityEngine.RenderTexture,System.Int32)
extern "C"  void RenderTexture_Internal_SetHeight_m4049466681 (Il2CppObject * __this /* static, unused */, RenderTexture_t2108887433 * ___mono0, int32_t ___width1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.RenderTexture::INTERNAL_CALL_Create(UnityEngine.RenderTexture)
extern "C"  bool RenderTexture_INTERNAL_CALL_Create_m3116277818 (Il2CppObject * __this /* static, unused */, RenderTexture_t2108887433 * ___self0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.RenderTexture::INTERNAL_CALL_Release(UnityEngine.RenderTexture)
extern "C"  void RenderTexture_INTERNAL_CALL_Release_m2580591883 (Il2CppObject * __this /* static, unused */, RenderTexture_t2108887433 * ___self0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.RenderTexture::INTERNAL_CALL_IsCreated(UnityEngine.RenderTexture)
extern "C"  bool RenderTexture_INTERNAL_CALL_IsCreated_m2038887254 (Il2CppObject * __this /* static, unused */, RenderTexture_t2108887433 * ___self0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Resolution::get_width()
extern "C"  int32_t Resolution_get_width_m1405037768 (Resolution_t2487619763 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Resolution::get_height()
extern "C"  int32_t Resolution_get_height_m1134023428 (Resolution_t2487619763 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.Resolution::ToString()
extern "C"  String_t* Resolution_ToString_m1856579011 (Resolution_t2487619763 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AsyncOperation::.ctor()
extern "C"  void AsyncOperation__ctor_m2683541089 (AsyncOperation_t1445031843 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Object UnityEngine.Resources::Load(System.String,System.Type)
extern "C"  Object_t631007953 * Resources_Load_m4202687077 (Il2CppObject * __this /* static, unused */, String_t* ___path0, Type_t * ___systemTypeInstance1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type System.Type::GetTypeFromHandle(System.RuntimeTypeHandle)
extern "C"  Type_t * Type_GetTypeFromHandle_m1620074514 (Il2CppObject * __this /* static, unused */, RuntimeTypeHandle_t3027515415  p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Rigidbody::INTERNAL_set_velocity(UnityEngine.Vector3&)
extern "C"  void Rigidbody_INTERNAL_set_velocity_m3212559553 (Rigidbody_t3916780224 * __this, Vector3_t3722313464 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Rigidbody::INTERNAL_CALL_AddForce(UnityEngine.Rigidbody,UnityEngine.Vector3&,UnityEngine.ForceMode)
extern "C"  void Rigidbody_INTERNAL_CALL_AddForce_m2680516995 (Il2CppObject * __this /* static, unused */, Rigidbody_t3916780224 * ___self0, Vector3_t3722313464 * ___force1, int32_t ___mode2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Rigidbody::INTERNAL_CALL_MovePosition(UnityEngine.Rigidbody,UnityEngine.Vector3&)
extern "C"  void Rigidbody_INTERNAL_CALL_MovePosition_m1701944164 (Il2CppObject * __this /* static, unused */, Rigidbody_t3916780224 * ___self0, Vector3_t3722313464 * ___position1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Rigidbody::INTERNAL_CALL_MoveRotation(UnityEngine.Rigidbody,UnityEngine.Quaternion&)
extern "C"  void Rigidbody_INTERNAL_CALL_MoveRotation_m2836158393 (Il2CppObject * __this /* static, unused */, Rigidbody_t3916780224 * ___self0, Quaternion_t2301928331 * ___rot1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Rigidbody2D::INTERNAL_set_velocity(UnityEngine.Vector2&)
extern "C"  void Rigidbody2D_INTERNAL_set_velocity_m3111408962 (Rigidbody2D_t939494601 * __this, Vector2_t2156229523 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Rigidbody2D::INTERNAL_CALL_AddForce(UnityEngine.Rigidbody2D,UnityEngine.Vector2&,UnityEngine.ForceMode2D)
extern "C"  void Rigidbody2D_INTERNAL_CALL_AddForce_m3533004671 (Il2CppObject * __this /* static, unused */, Rigidbody2D_t939494601 * ___self0, Vector2_t2156229523 * ___force1, int32_t ___mode2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Scripting.PreserveAttribute::.ctor()
extern "C"  void PreserveAttribute__ctor_m728193880 (PreserveAttribute_t1583619344 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.RuntimeInitializeOnLoadMethodAttribute::set_loadType(UnityEngine.RuntimeInitializeLoadType)
extern "C"  void RuntimeInitializeOnLoadMethodAttribute_set_loadType_m227377850 (RuntimeInitializeOnLoadMethodAttribute_t3192313494 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.SceneManagement.Scene::get_handle()
extern "C"  int32_t Scene_get_handle_m1480030146 (Scene_t2348375561 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.SceneManagement.Scene::GetNameInternal(System.Int32)
extern "C"  String_t* Scene_GetNameInternal_m3643126100 (Il2CppObject * __this /* static, unused */, int32_t ___sceneHandle0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.SceneManagement.Scene::get_name()
extern "C"  String_t* Scene_get_name_m2213167406 (Scene_t2348375561 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.SceneManagement.Scene::GetHashCode()
extern "C"  int32_t Scene_GetHashCode_m42646907 (Scene_t2348375561 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.SceneManagement.Scene::Equals(System.Object)
extern "C"  bool Scene_Equals_m3811894426 (Scene_t2348375561 * __this, Il2CppObject * ___other0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.SceneManagement.SceneManager::INTERNAL_CALL_GetActiveScene(UnityEngine.SceneManagement.Scene&)
extern "C"  void SceneManager_INTERNAL_CALL_GetActiveScene_m756000170 (Il2CppObject * __this /* static, unused */, Scene_t2348375561 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.SceneManagement.SceneManager::INTERNAL_CALL_SetActiveScene(UnityEngine.SceneManagement.Scene&)
extern "C"  bool SceneManager_INTERNAL_CALL_SetActiveScene_m4174233036 (Il2CppObject * __this /* static, unused */, Scene_t2348375561 * ___scene0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.SceneManagement.SceneManager::INTERNAL_CALL_GetSceneByName(System.String,UnityEngine.SceneManagement.Scene&)
extern "C"  void SceneManager_INTERNAL_CALL_GetSceneByName_m4198796804 (Il2CppObject * __this /* static, unused */, String_t* ___name0, Scene_t2348375561 * ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.SceneManagement.SceneManager::LoadScene(System.String,UnityEngine.SceneManagement.LoadSceneMode)
extern "C"  void SceneManager_LoadScene_m752772171 (Il2CppObject * __this /* static, unused */, String_t* ___sceneName0, int32_t ___mode1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.AsyncOperation UnityEngine.SceneManagement.SceneManager::LoadSceneAsyncNameIndexInternal(System.String,System.Int32,System.Boolean,System.Boolean)
extern "C"  AsyncOperation_t1445031843 * SceneManager_LoadSceneAsyncNameIndexInternal_m756833036 (Il2CppObject * __this /* static, unused */, String_t* ___sceneName0, int32_t ___sceneBuildIndex1, bool ___isAdditive2, bool ___mustCompleteNextFrame3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.AsyncOperation UnityEngine.SceneManagement.SceneManager::LoadSceneAsync(System.String,UnityEngine.SceneManagement.LoadSceneMode)
extern "C"  AsyncOperation_t1445031843 * SceneManager_LoadSceneAsync_m2750151360 (Il2CppObject * __this /* static, unused */, String_t* ___sceneName0, int32_t ___mode1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Events.UnityAction`2<UnityEngine.SceneManagement.Scene,UnityEngine.SceneManagement.LoadSceneMode>::Invoke(T0,T1)
#define UnityAction_2_Invoke_m1297655681(__this, p0, p1, method) ((  void (*) (UnityAction_2_t2165061829 *, Scene_t2348375561 , int32_t, const MethodInfo*))UnityAction_2_Invoke_m1297655681_gshared)(__this, p0, p1, method)
// System.Void UnityEngine.Events.UnityAction`1<UnityEngine.SceneManagement.Scene>::Invoke(T0)
#define UnityAction_1_Invoke_m3689685399(__this, p0, method) ((  void (*) (UnityAction_1_t2933211702 *, Scene_t2348375561 , const MethodInfo*))UnityAction_1_Invoke_m3689685399_gshared)(__this, p0, method)
// System.Void UnityEngine.Events.UnityAction`2<UnityEngine.SceneManagement.Scene,UnityEngine.SceneManagement.Scene>::Invoke(T0,T1)
#define UnityAction_2_Invoke_m770027984(__this, p0, p1, method) ((  void (*) (UnityAction_2_t1262235195 *, Scene_t2348375561 , Scene_t2348375561 , const MethodInfo*))UnityAction_2_Invoke_m770027984_gshared)(__this, p0, p1, method)
// System.Void UnityEngine.Object::.ctor()
extern "C"  void Object__ctor_m1560822313 (Object_t631007953 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ScriptableObject::Internal_CreateScriptableObject(UnityEngine.ScriptableObject)
extern "C"  void ScriptableObject_Internal_CreateScriptableObject_m163809621 (Il2CppObject * __this /* static, unused */, ScriptableObject_t2528358522 * ___self0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.ScriptableObject UnityEngine.ScriptableObject::CreateInstanceFromType(System.Type)
extern "C"  ScriptableObject_t2528358522 * ScriptableObject_CreateInstanceFromType_m3616970571 (Il2CppObject * __this /* static, unused */, Type_t * ___type0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Scripting.APIUpdating.MovedFromAttribute::.ctor(System.String,System.Boolean)
extern "C"  void MovedFromAttribute__ctor_m1576224919 (MovedFromAttribute_t481952341 * __this, String_t* ___sourceNamespace0, bool ___isInDifferentAssembly1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Scripting.APIUpdating.MovedFromAttribute::set_Namespace(System.String)
extern "C"  void MovedFromAttribute_set_Namespace_m2813913251 (MovedFromAttribute_t481952341 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Scripting.APIUpdating.MovedFromAttribute::set_IsInDifferentAssembly(System.Boolean)
extern "C"  void MovedFromAttribute_set_IsInDifferentAssembly_m1665805492 (MovedFromAttribute_t481952341 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Scripting.RequiredByNativeCodeAttribute::set_Name(System.String)
extern "C"  void RequiredByNativeCodeAttribute_set_Name_m3281191310 (RequiredByNativeCodeAttribute_t4130846357 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// Conversion methods for marshalling of: UnityEngine.Object
extern "C" void Object_t631007953_marshal_pinvoke(const Object_t631007953& unmarshaled, Object_t631007953_marshaled_pinvoke& marshaled)
{
	marshaled.___m_CachedPtr_0 = reinterpret_cast<intptr_t>((unmarshaled.get_m_CachedPtr_0()).get_m_value_0());
}
extern "C" void Object_t631007953_marshal_pinvoke_back(const Object_t631007953_marshaled_pinvoke& marshaled, Object_t631007953& unmarshaled)
{
	IntPtr_t unmarshaled_m_CachedPtr_temp_0;
	memset(&unmarshaled_m_CachedPtr_temp_0, 0, sizeof(unmarshaled_m_CachedPtr_temp_0));
	IntPtr_t unmarshaled_m_CachedPtr_temp_0_temp;
	unmarshaled_m_CachedPtr_temp_0_temp.set_m_value_0(reinterpret_cast<void*>((intptr_t)(marshaled.___m_CachedPtr_0)));
	unmarshaled_m_CachedPtr_temp_0 = unmarshaled_m_CachedPtr_temp_0_temp;
	unmarshaled.set_m_CachedPtr_0(unmarshaled_m_CachedPtr_temp_0);
}
// Conversion method for clean up from marshalling of: UnityEngine.Object
extern "C" void Object_t631007953_marshal_pinvoke_cleanup(Object_t631007953_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.Object
extern "C" void Object_t631007953_marshal_com(const Object_t631007953& unmarshaled, Object_t631007953_marshaled_com& marshaled)
{
	marshaled.___m_CachedPtr_0 = reinterpret_cast<intptr_t>((unmarshaled.get_m_CachedPtr_0()).get_m_value_0());
}
extern "C" void Object_t631007953_marshal_com_back(const Object_t631007953_marshaled_com& marshaled, Object_t631007953& unmarshaled)
{
	IntPtr_t unmarshaled_m_CachedPtr_temp_0;
	memset(&unmarshaled_m_CachedPtr_temp_0, 0, sizeof(unmarshaled_m_CachedPtr_temp_0));
	IntPtr_t unmarshaled_m_CachedPtr_temp_0_temp;
	unmarshaled_m_CachedPtr_temp_0_temp.set_m_value_0(reinterpret_cast<void*>((intptr_t)(marshaled.___m_CachedPtr_0)));
	unmarshaled_m_CachedPtr_temp_0 = unmarshaled_m_CachedPtr_temp_0_temp;
	unmarshaled.set_m_CachedPtr_0(unmarshaled_m_CachedPtr_temp_0);
}
// Conversion method for clean up from marshalling of: UnityEngine.Object
extern "C" void Object_t631007953_marshal_com_cleanup(Object_t631007953_marshaled_com& marshaled)
{
}
// System.Void UnityEngine.Object::.ctor()
extern "C"  void Object__ctor_m1560822313 (Object_t631007953 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m297566312(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Object UnityEngine.Object::Internal_CloneSingle(UnityEngine.Object)
extern "C"  Object_t631007953 * Object_Internal_CloneSingle_m1527886285 (Il2CppObject * __this /* static, unused */, Object_t631007953 * ___data0, const MethodInfo* method)
{
	typedef Object_t631007953 * (*Object_Internal_CloneSingle_m1527886285_ftn) (Object_t631007953 *);
	static Object_Internal_CloneSingle_m1527886285_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Object_Internal_CloneSingle_m1527886285_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Object::Internal_CloneSingle(UnityEngine.Object)");
	return _il2cpp_icall_func(___data0);
}
// UnityEngine.Object UnityEngine.Object::Internal_CloneSingleWithParent(UnityEngine.Object,UnityEngine.Transform,System.Boolean)
extern "C"  Object_t631007953 * Object_Internal_CloneSingleWithParent_m2047142090 (Il2CppObject * __this /* static, unused */, Object_t631007953 * ___data0, Transform_t3600365921 * ___parent1, bool ___worldPositionStays2, const MethodInfo* method)
{
	typedef Object_t631007953 * (*Object_Internal_CloneSingleWithParent_m2047142090_ftn) (Object_t631007953 *, Transform_t3600365921 *, bool);
	static Object_Internal_CloneSingleWithParent_m2047142090_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Object_Internal_CloneSingleWithParent_m2047142090_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Object::Internal_CloneSingleWithParent(UnityEngine.Object,UnityEngine.Transform,System.Boolean)");
	return _il2cpp_icall_func(___data0, ___parent1, ___worldPositionStays2);
}
// UnityEngine.Object UnityEngine.Object::Internal_InstantiateSingle(UnityEngine.Object,UnityEngine.Vector3,UnityEngine.Quaternion)
extern "C"  Object_t631007953 * Object_Internal_InstantiateSingle_m2978472292 (Il2CppObject * __this /* static, unused */, Object_t631007953 * ___data0, Vector3_t3722313464  ___pos1, Quaternion_t2301928331  ___rot2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Object_Internal_InstantiateSingle_m2978472292_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Object_t631007953 * V_0 = NULL;
	{
		Object_t631007953 * L_0 = ___data0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		Object_t631007953 * L_1 = Object_INTERNAL_CALL_Internal_InstantiateSingle_m3963327137(NULL /*static, unused*/, L_0, (&___pos1), (&___rot2), /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_0011;
	}

IL_0011:
	{
		Object_t631007953 * L_2 = V_0;
		return L_2;
	}
}
// UnityEngine.Object UnityEngine.Object::INTERNAL_CALL_Internal_InstantiateSingle(UnityEngine.Object,UnityEngine.Vector3&,UnityEngine.Quaternion&)
extern "C"  Object_t631007953 * Object_INTERNAL_CALL_Internal_InstantiateSingle_m3963327137 (Il2CppObject * __this /* static, unused */, Object_t631007953 * ___data0, Vector3_t3722313464 * ___pos1, Quaternion_t2301928331 * ___rot2, const MethodInfo* method)
{
	typedef Object_t631007953 * (*Object_INTERNAL_CALL_Internal_InstantiateSingle_m3963327137_ftn) (Object_t631007953 *, Vector3_t3722313464 *, Quaternion_t2301928331 *);
	static Object_INTERNAL_CALL_Internal_InstantiateSingle_m3963327137_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Object_INTERNAL_CALL_Internal_InstantiateSingle_m3963327137_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Object::INTERNAL_CALL_Internal_InstantiateSingle(UnityEngine.Object,UnityEngine.Vector3&,UnityEngine.Quaternion&)");
	return _il2cpp_icall_func(___data0, ___pos1, ___rot2);
}
// UnityEngine.Object UnityEngine.Object::Internal_InstantiateSingleWithParent(UnityEngine.Object,UnityEngine.Transform,UnityEngine.Vector3,UnityEngine.Quaternion)
extern "C"  Object_t631007953 * Object_Internal_InstantiateSingleWithParent_m1292193254 (Il2CppObject * __this /* static, unused */, Object_t631007953 * ___data0, Transform_t3600365921 * ___parent1, Vector3_t3722313464  ___pos2, Quaternion_t2301928331  ___rot3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Object_Internal_InstantiateSingleWithParent_m1292193254_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Object_t631007953 * V_0 = NULL;
	{
		Object_t631007953 * L_0 = ___data0;
		Transform_t3600365921 * L_1 = ___parent1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		Object_t631007953 * L_2 = Object_INTERNAL_CALL_Internal_InstantiateSingleWithParent_m3300138447(NULL /*static, unused*/, L_0, L_1, (&___pos2), (&___rot3), /*hidden argument*/NULL);
		V_0 = L_2;
		goto IL_0012;
	}

IL_0012:
	{
		Object_t631007953 * L_3 = V_0;
		return L_3;
	}
}
// UnityEngine.Object UnityEngine.Object::INTERNAL_CALL_Internal_InstantiateSingleWithParent(UnityEngine.Object,UnityEngine.Transform,UnityEngine.Vector3&,UnityEngine.Quaternion&)
extern "C"  Object_t631007953 * Object_INTERNAL_CALL_Internal_InstantiateSingleWithParent_m3300138447 (Il2CppObject * __this /* static, unused */, Object_t631007953 * ___data0, Transform_t3600365921 * ___parent1, Vector3_t3722313464 * ___pos2, Quaternion_t2301928331 * ___rot3, const MethodInfo* method)
{
	typedef Object_t631007953 * (*Object_INTERNAL_CALL_Internal_InstantiateSingleWithParent_m3300138447_ftn) (Object_t631007953 *, Transform_t3600365921 *, Vector3_t3722313464 *, Quaternion_t2301928331 *);
	static Object_INTERNAL_CALL_Internal_InstantiateSingleWithParent_m3300138447_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Object_INTERNAL_CALL_Internal_InstantiateSingleWithParent_m3300138447_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Object::INTERNAL_CALL_Internal_InstantiateSingleWithParent(UnityEngine.Object,UnityEngine.Transform,UnityEngine.Vector3&,UnityEngine.Quaternion&)");
	return _il2cpp_icall_func(___data0, ___parent1, ___pos2, ___rot3);
}
// System.Int32 UnityEngine.Object::GetOffsetOfInstanceIDInCPlusPlusObject()
extern "C"  int32_t Object_GetOffsetOfInstanceIDInCPlusPlusObject_m3574580840 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef int32_t (*Object_GetOffsetOfInstanceIDInCPlusPlusObject_m3574580840_ftn) ();
	static Object_GetOffsetOfInstanceIDInCPlusPlusObject_m3574580840_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Object_GetOffsetOfInstanceIDInCPlusPlusObject_m3574580840_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Object::GetOffsetOfInstanceIDInCPlusPlusObject()");
	return _il2cpp_icall_func();
}
// System.Void UnityEngine.Object::EnsureRunningOnMainThread()
extern "C"  void Object_EnsureRunningOnMainThread_m2049148642 (Object_t631007953 * __this, const MethodInfo* method)
{
	typedef void (*Object_EnsureRunningOnMainThread_m2049148642_ftn) (Object_t631007953 *);
	static Object_EnsureRunningOnMainThread_m2049148642_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Object_EnsureRunningOnMainThread_m2049148642_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Object::EnsureRunningOnMainThread()");
	_il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Object::Destroy(UnityEngine.Object,System.Single)
extern "C"  void Object_Destroy_m1895515052 (Il2CppObject * __this /* static, unused */, Object_t631007953 * ___obj0, float ___t1, const MethodInfo* method)
{
	typedef void (*Object_Destroy_m1895515052_ftn) (Object_t631007953 *, float);
	static Object_Destroy_m1895515052_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Object_Destroy_m1895515052_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Object::Destroy(UnityEngine.Object,System.Single)");
	_il2cpp_icall_func(___obj0, ___t1);
}
// System.Void UnityEngine.Object::Destroy(UnityEngine.Object)
extern "C"  void Object_Destroy_m2752645118 (Il2CppObject * __this /* static, unused */, Object_t631007953 * ___obj0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Object_Destroy_m2752645118_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	{
		V_0 = (0.0f);
		Object_t631007953 * L_0 = ___obj0;
		float L_1 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		Object_Destroy_m1895515052(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Object::DestroyImmediate(UnityEngine.Object,System.Boolean)
extern "C"  void Object_DestroyImmediate_m2539295074 (Il2CppObject * __this /* static, unused */, Object_t631007953 * ___obj0, bool ___allowDestroyingAssets1, const MethodInfo* method)
{
	typedef void (*Object_DestroyImmediate_m2539295074_ftn) (Object_t631007953 *, bool);
	static Object_DestroyImmediate_m2539295074_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Object_DestroyImmediate_m2539295074_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Object::DestroyImmediate(UnityEngine.Object,System.Boolean)");
	_il2cpp_icall_func(___obj0, ___allowDestroyingAssets1);
}
// System.Void UnityEngine.Object::DestroyImmediate(UnityEngine.Object)
extern "C"  void Object_DestroyImmediate_m1556866283 (Il2CppObject * __this /* static, unused */, Object_t631007953 * ___obj0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Object_DestroyImmediate_m1556866283_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	{
		V_0 = (bool)0;
		Object_t631007953 * L_0 = ___obj0;
		bool L_1 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		Object_DestroyImmediate_m2539295074(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Object[] UnityEngine.Object::FindObjectsOfType(System.Type)
extern "C"  ObjectU5BU5D_t1417781964* Object_FindObjectsOfType_m2898745631 (Il2CppObject * __this /* static, unused */, Type_t * ___type0, const MethodInfo* method)
{
	typedef ObjectU5BU5D_t1417781964* (*Object_FindObjectsOfType_m2898745631_ftn) (Type_t *);
	static Object_FindObjectsOfType_m2898745631_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Object_FindObjectsOfType_m2898745631_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Object::FindObjectsOfType(System.Type)");
	return _il2cpp_icall_func(___type0);
}
// System.String UnityEngine.Object::get_name()
extern "C"  String_t* Object_get_name_m1414505214 (Object_t631007953 * __this, const MethodInfo* method)
{
	typedef String_t* (*Object_get_name_m1414505214_ftn) (Object_t631007953 *);
	static Object_get_name_m1414505214_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Object_get_name_m1414505214_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Object::get_name()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Object::set_name(System.String)
extern "C"  void Object_set_name_m653319976 (Object_t631007953 * __this, String_t* ___value0, const MethodInfo* method)
{
	typedef void (*Object_set_name_m653319976_ftn) (Object_t631007953 *, String_t*);
	static Object_set_name_m653319976_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Object_set_name_m653319976_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Object::set_name(System.String)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.Object::DontDestroyOnLoad(UnityEngine.Object)
extern "C"  void Object_DontDestroyOnLoad_m3153277066 (Il2CppObject * __this /* static, unused */, Object_t631007953 * ___target0, const MethodInfo* method)
{
	typedef void (*Object_DontDestroyOnLoad_m3153277066_ftn) (Object_t631007953 *);
	static Object_DontDestroyOnLoad_m3153277066_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Object_DontDestroyOnLoad_m3153277066_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Object::DontDestroyOnLoad(UnityEngine.Object)");
	_il2cpp_icall_func(___target0);
}
// UnityEngine.HideFlags UnityEngine.Object::get_hideFlags()
extern "C"  int32_t Object_get_hideFlags_m1322794507 (Object_t631007953 * __this, const MethodInfo* method)
{
	typedef int32_t (*Object_get_hideFlags_m1322794507_ftn) (Object_t631007953 *);
	static Object_get_hideFlags_m1322794507_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Object_get_hideFlags_m1322794507_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Object::get_hideFlags()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Object::set_hideFlags(UnityEngine.HideFlags)
extern "C"  void Object_set_hideFlags_m395729791 (Object_t631007953 * __this, int32_t ___value0, const MethodInfo* method)
{
	typedef void (*Object_set_hideFlags_m395729791_ftn) (Object_t631007953 *, int32_t);
	static Object_set_hideFlags_m395729791_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Object_set_hideFlags_m395729791_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Object::set_hideFlags(UnityEngine.HideFlags)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.Object::DestroyObject(UnityEngine.Object,System.Single)
extern "C"  void Object_DestroyObject_m869248974 (Il2CppObject * __this /* static, unused */, Object_t631007953 * ___obj0, float ___t1, const MethodInfo* method)
{
	typedef void (*Object_DestroyObject_m869248974_ftn) (Object_t631007953 *, float);
	static Object_DestroyObject_m869248974_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Object_DestroyObject_m869248974_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Object::DestroyObject(UnityEngine.Object,System.Single)");
	_il2cpp_icall_func(___obj0, ___t1);
}
// System.Void UnityEngine.Object::DestroyObject(UnityEngine.Object)
extern "C"  void Object_DestroyObject_m2915873188 (Il2CppObject * __this /* static, unused */, Object_t631007953 * ___obj0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Object_DestroyObject_m2915873188_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	{
		V_0 = (0.0f);
		Object_t631007953 * L_0 = ___obj0;
		float L_1 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		Object_DestroyObject_m869248974(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Object[] UnityEngine.Object::FindSceneObjectsOfType(System.Type)
extern "C"  ObjectU5BU5D_t1417781964* Object_FindSceneObjectsOfType_m1091134095 (Il2CppObject * __this /* static, unused */, Type_t * ___type0, const MethodInfo* method)
{
	typedef ObjectU5BU5D_t1417781964* (*Object_FindSceneObjectsOfType_m1091134095_ftn) (Type_t *);
	static Object_FindSceneObjectsOfType_m1091134095_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Object_FindSceneObjectsOfType_m1091134095_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Object::FindSceneObjectsOfType(System.Type)");
	return _il2cpp_icall_func(___type0);
}
// UnityEngine.Object[] UnityEngine.Object::FindObjectsOfTypeIncludingAssets(System.Type)
extern "C"  ObjectU5BU5D_t1417781964* Object_FindObjectsOfTypeIncludingAssets_m4262988351 (Il2CppObject * __this /* static, unused */, Type_t * ___type0, const MethodInfo* method)
{
	typedef ObjectU5BU5D_t1417781964* (*Object_FindObjectsOfTypeIncludingAssets_m4262988351_ftn) (Type_t *);
	static Object_FindObjectsOfTypeIncludingAssets_m4262988351_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Object_FindObjectsOfTypeIncludingAssets_m4262988351_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Object::FindObjectsOfTypeIncludingAssets(System.Type)");
	return _il2cpp_icall_func(___type0);
}
// System.String UnityEngine.Object::ToString()
extern "C"  String_t* Object_ToString_m426284353 (Object_t631007953 * __this, const MethodInfo* method)
{
	typedef String_t* (*Object_ToString_m426284353_ftn) (Object_t631007953 *);
	static Object_ToString_m426284353_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Object_ToString_m426284353_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Object::ToString()");
	return _il2cpp_icall_func(__this);
}
// System.Boolean UnityEngine.Object::DoesObjectWithInstanceIDExist(System.Int32)
extern "C"  bool Object_DoesObjectWithInstanceIDExist_m2303124163 (Il2CppObject * __this /* static, unused */, int32_t ___instanceID0, const MethodInfo* method)
{
	typedef bool (*Object_DoesObjectWithInstanceIDExist_m2303124163_ftn) (int32_t);
	static Object_DoesObjectWithInstanceIDExist_m2303124163_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Object_DoesObjectWithInstanceIDExist_m2303124163_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Object::DoesObjectWithInstanceIDExist(System.Int32)");
	return _il2cpp_icall_func(___instanceID0);
}
// System.Int32 UnityEngine.Object::GetInstanceID()
extern "C"  int32_t Object_GetInstanceID_m3267067959 (Object_t631007953 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Object_GetInstanceID_m3267067959_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		IntPtr_t L_0 = __this->get_m_CachedPtr_0();
		IntPtr_t L_1 = ((IntPtr_t_StaticFields*)IntPtr_t_il2cpp_TypeInfo_var->static_fields)->get_Zero_1();
		bool L_2 = IntPtr_op_Equality_m408849716(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_001d;
		}
	}
	{
		V_0 = 0;
		goto IL_0056;
	}

IL_001d:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		int32_t L_3 = ((Object_t631007953_StaticFields*)Object_t631007953_il2cpp_TypeInfo_var->static_fields)->get_OffsetOfInstanceIDInCPlusPlusObject_1();
		if ((!(((uint32_t)L_3) == ((uint32_t)(-1)))))
		{
			goto IL_0032;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		int32_t L_4 = Object_GetOffsetOfInstanceIDInCPlusPlusObject_m3574580840(NULL /*static, unused*/, /*hidden argument*/NULL);
		((Object_t631007953_StaticFields*)Object_t631007953_il2cpp_TypeInfo_var->static_fields)->set_OffsetOfInstanceIDInCPlusPlusObject_1(L_4);
	}

IL_0032:
	{
		IntPtr_t* L_5 = __this->get_address_of_m_CachedPtr_0();
		int64_t L_6 = IntPtr_ToInt64_m192765549(L_5, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		int32_t L_7 = ((Object_t631007953_StaticFields*)Object_t631007953_il2cpp_TypeInfo_var->static_fields)->get_OffsetOfInstanceIDInCPlusPlusObject_1();
		IntPtr_t L_8;
		memset(&L_8, 0, sizeof(L_8));
		IntPtr__ctor_m987476171(&L_8, ((int64_t)((int64_t)L_6+(int64_t)(((int64_t)((int64_t)L_7))))), /*hidden argument*/NULL);
		void* L_9 = IntPtr_op_Explicit_m2520637223(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
		V_0 = (*((int32_t*)L_9));
		goto IL_0056;
	}

IL_0056:
	{
		int32_t L_10 = V_0;
		return L_10;
	}
}
// System.Int32 UnityEngine.Object::GetHashCode()
extern "C"  int32_t Object_GetHashCode_m305416696 (Object_t631007953 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = Object_GetHashCode_m2705121830(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		int32_t L_1 = V_0;
		return L_1;
	}
}
// System.Boolean UnityEngine.Object::Equals(System.Object)
extern "C"  bool Object_Equals_m2798019232 (Object_t631007953 * __this, Il2CppObject * ___other0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Object_Equals_m2798019232_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Object_t631007953 * V_0 = NULL;
	bool V_1 = false;
	{
		Il2CppObject * L_0 = ___other0;
		V_0 = ((Object_t631007953 *)IsInstClass(L_0, Object_t631007953_il2cpp_TypeInfo_var));
		Object_t631007953 * L_1 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_2 = Object_op_Equality_m1454075600(NULL /*static, unused*/, L_1, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_002c;
		}
	}
	{
		Il2CppObject * L_3 = ___other0;
		if (!L_3)
		{
			goto IL_002c;
		}
	}
	{
		Il2CppObject * L_4 = ___other0;
		if (((Object_t631007953 *)IsInstClass(L_4, Object_t631007953_il2cpp_TypeInfo_var)))
		{
			goto IL_002c;
		}
	}
	{
		V_1 = (bool)0;
		goto IL_0039;
	}

IL_002c:
	{
		Object_t631007953 * L_5 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_6 = Object_CompareBaseObjects_m1463006152(NULL /*static, unused*/, __this, L_5, /*hidden argument*/NULL);
		V_1 = L_6;
		goto IL_0039;
	}

IL_0039:
	{
		bool L_7 = V_1;
		return L_7;
	}
}
// System.Boolean UnityEngine.Object::op_Implicit(UnityEngine.Object)
extern "C"  bool Object_op_Implicit_m487959476 (Il2CppObject * __this /* static, unused */, Object_t631007953 * ___exists0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Object_op_Implicit_m487959476_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	{
		Object_t631007953 * L_0 = ___exists0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_1 = Object_CompareBaseObjects_m1463006152(NULL /*static, unused*/, L_0, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		V_0 = (bool)((((int32_t)L_1) == ((int32_t)0))? 1 : 0);
		goto IL_0011;
	}

IL_0011:
	{
		bool L_2 = V_0;
		return L_2;
	}
}
// System.Boolean UnityEngine.Object::CompareBaseObjects(UnityEngine.Object,UnityEngine.Object)
extern "C"  bool Object_CompareBaseObjects_m1463006152 (Il2CppObject * __this /* static, unused */, Object_t631007953 * ___lhs0, Object_t631007953 * ___rhs1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Object_CompareBaseObjects_m1463006152_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	bool V_1 = false;
	bool V_2 = false;
	{
		Object_t631007953 * L_0 = ___lhs0;
		V_0 = (bool)((((Il2CppObject*)(Object_t631007953 *)L_0) == ((Il2CppObject*)(Il2CppObject *)NULL))? 1 : 0);
		Object_t631007953 * L_1 = ___rhs1;
		V_1 = (bool)((((Il2CppObject*)(Object_t631007953 *)L_1) == ((Il2CppObject*)(Il2CppObject *)NULL))? 1 : 0);
		bool L_2 = V_1;
		if (!L_2)
		{
			goto IL_001e;
		}
	}
	{
		bool L_3 = V_0;
		if (!L_3)
		{
			goto IL_001e;
		}
	}
	{
		V_2 = (bool)1;
		goto IL_0055;
	}

IL_001e:
	{
		bool L_4 = V_1;
		if (!L_4)
		{
			goto IL_0033;
		}
	}
	{
		Object_t631007953 * L_5 = ___lhs0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_6 = Object_IsNativeObjectAlive_m1963713004(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		V_2 = (bool)((((int32_t)L_6) == ((int32_t)0))? 1 : 0);
		goto IL_0055;
	}

IL_0033:
	{
		bool L_7 = V_0;
		if (!L_7)
		{
			goto IL_0048;
		}
	}
	{
		Object_t631007953 * L_8 = ___rhs1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_9 = Object_IsNativeObjectAlive_m1963713004(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
		V_2 = (bool)((((int32_t)L_9) == ((int32_t)0))? 1 : 0);
		goto IL_0055;
	}

IL_0048:
	{
		Object_t631007953 * L_10 = ___lhs0;
		Object_t631007953 * L_11 = ___rhs1;
		bool L_12 = Object_ReferenceEquals_m610702577(NULL /*static, unused*/, L_10, L_11, /*hidden argument*/NULL);
		V_2 = L_12;
		goto IL_0055;
	}

IL_0055:
	{
		bool L_13 = V_2;
		return L_13;
	}
}
// System.Boolean UnityEngine.Object::IsNativeObjectAlive(UnityEngine.Object)
extern "C"  bool Object_IsNativeObjectAlive_m1963713004 (Il2CppObject * __this /* static, unused */, Object_t631007953 * ___o0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Object_IsNativeObjectAlive_m1963713004_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	{
		Object_t631007953 * L_0 = ___o0;
		NullCheck(L_0);
		IntPtr_t L_1 = Object_GetCachedPtr_m589232273(L_0, /*hidden argument*/NULL);
		IntPtr_t L_2 = ((IntPtr_t_StaticFields*)IntPtr_t_il2cpp_TypeInfo_var->static_fields)->get_Zero_1();
		bool L_3 = IntPtr_op_Inequality_m3063970704(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		goto IL_0017;
	}

IL_0017:
	{
		bool L_4 = V_0;
		return L_4;
	}
}
// System.IntPtr UnityEngine.Object::GetCachedPtr()
extern "C"  IntPtr_t Object_GetCachedPtr_m589232273 (Object_t631007953 * __this, const MethodInfo* method)
{
	IntPtr_t V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		IntPtr_t L_0 = __this->get_m_CachedPtr_0();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		IntPtr_t L_1 = V_0;
		return L_1;
	}
}
// UnityEngine.Object UnityEngine.Object::Instantiate(UnityEngine.Object,UnityEngine.Vector3,UnityEngine.Quaternion)
extern "C"  Object_t631007953 * Object_Instantiate_m2563497874 (Il2CppObject * __this /* static, unused */, Object_t631007953 * ___original0, Vector3_t3722313464  ___position1, Quaternion_t2301928331  ___rotation2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Object_Instantiate_m2563497874_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Object_t631007953 * V_0 = NULL;
	{
		Object_t631007953 * L_0 = ___original0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		Object_CheckNullArgument_m129989854(NULL /*static, unused*/, L_0, _stringLiteral2475671027, /*hidden argument*/NULL);
		Object_t631007953 * L_1 = ___original0;
		if (!((ScriptableObject_t2528358522 *)IsInstClass(L_1, ScriptableObject_t2528358522_il2cpp_TypeInfo_var)))
		{
			goto IL_0022;
		}
	}
	{
		ArgumentException_t132251570 * L_2 = (ArgumentException_t132251570 *)il2cpp_codegen_object_new(ArgumentException_t132251570_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m1312628991(L_2, _stringLiteral1766417507, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0022:
	{
		Object_t631007953 * L_3 = ___original0;
		Vector3_t3722313464  L_4 = ___position1;
		Quaternion_t2301928331  L_5 = ___rotation2;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		Object_t631007953 * L_6 = Object_Internal_InstantiateSingle_m2978472292(NULL /*static, unused*/, L_3, L_4, L_5, /*hidden argument*/NULL);
		V_0 = L_6;
		goto IL_0030;
	}

IL_0030:
	{
		Object_t631007953 * L_7 = V_0;
		return L_7;
	}
}
// UnityEngine.Object UnityEngine.Object::Instantiate(UnityEngine.Object,UnityEngine.Vector3,UnityEngine.Quaternion,UnityEngine.Transform)
extern "C"  Object_t631007953 * Object_Instantiate_m3242828326 (Il2CppObject * __this /* static, unused */, Object_t631007953 * ___original0, Vector3_t3722313464  ___position1, Quaternion_t2301928331  ___rotation2, Transform_t3600365921 * ___parent3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Object_Instantiate_m3242828326_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Object_t631007953 * V_0 = NULL;
	{
		Transform_t3600365921 * L_0 = ___parent3;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_m1454075600(NULL /*static, unused*/, L_0, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001b;
		}
	}
	{
		Object_t631007953 * L_2 = ___original0;
		Vector3_t3722313464  L_3 = ___position1;
		Quaternion_t2301928331  L_4 = ___rotation2;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		Object_t631007953 * L_5 = Object_Internal_InstantiateSingle_m2978472292(NULL /*static, unused*/, L_2, L_3, L_4, /*hidden argument*/NULL);
		V_0 = L_5;
		goto IL_0035;
	}

IL_001b:
	{
		Object_t631007953 * L_6 = ___original0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		Object_CheckNullArgument_m129989854(NULL /*static, unused*/, L_6, _stringLiteral2475671027, /*hidden argument*/NULL);
		Object_t631007953 * L_7 = ___original0;
		Transform_t3600365921 * L_8 = ___parent3;
		Vector3_t3722313464  L_9 = ___position1;
		Quaternion_t2301928331  L_10 = ___rotation2;
		Object_t631007953 * L_11 = Object_Internal_InstantiateSingleWithParent_m1292193254(NULL /*static, unused*/, L_7, L_8, L_9, L_10, /*hidden argument*/NULL);
		V_0 = L_11;
		goto IL_0035;
	}

IL_0035:
	{
		Object_t631007953 * L_12 = V_0;
		return L_12;
	}
}
// UnityEngine.Object UnityEngine.Object::Instantiate(UnityEngine.Object)
extern "C"  Object_t631007953 * Object_Instantiate_m2891014423 (Il2CppObject * __this /* static, unused */, Object_t631007953 * ___original0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Object_Instantiate_m2891014423_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Object_t631007953 * V_0 = NULL;
	{
		Object_t631007953 * L_0 = ___original0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		Object_CheckNullArgument_m129989854(NULL /*static, unused*/, L_0, _stringLiteral2475671027, /*hidden argument*/NULL);
		Object_t631007953 * L_1 = ___original0;
		Object_t631007953 * L_2 = Object_Internal_CloneSingle_m1527886285(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		goto IL_0018;
	}

IL_0018:
	{
		Object_t631007953 * L_3 = V_0;
		return L_3;
	}
}
// UnityEngine.Object UnityEngine.Object::Instantiate(UnityEngine.Object,UnityEngine.Transform)
extern "C"  Object_t631007953 * Object_Instantiate_m2737875546 (Il2CppObject * __this /* static, unused */, Object_t631007953 * ___original0, Transform_t3600365921 * ___parent1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Object_Instantiate_m2737875546_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Object_t631007953 * V_0 = NULL;
	{
		Object_t631007953 * L_0 = ___original0;
		Transform_t3600365921 * L_1 = ___parent1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		Object_t631007953 * L_2 = Object_Instantiate_m3931415074(NULL /*static, unused*/, L_0, L_1, (bool)0, /*hidden argument*/NULL);
		V_0 = L_2;
		goto IL_000f;
	}

IL_000f:
	{
		Object_t631007953 * L_3 = V_0;
		return L_3;
	}
}
// UnityEngine.Object UnityEngine.Object::Instantiate(UnityEngine.Object,UnityEngine.Transform,System.Boolean)
extern "C"  Object_t631007953 * Object_Instantiate_m3931415074 (Il2CppObject * __this /* static, unused */, Object_t631007953 * ___original0, Transform_t3600365921 * ___parent1, bool ___instantiateInWorldSpace2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Object_Instantiate_m3931415074_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Object_t631007953 * V_0 = NULL;
	{
		Transform_t3600365921 * L_0 = ___parent1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_m1454075600(NULL /*static, unused*/, L_0, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0019;
		}
	}
	{
		Object_t631007953 * L_2 = ___original0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		Object_t631007953 * L_3 = Object_Internal_CloneSingle_m1527886285(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		goto IL_0032;
	}

IL_0019:
	{
		Object_t631007953 * L_4 = ___original0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		Object_CheckNullArgument_m129989854(NULL /*static, unused*/, L_4, _stringLiteral2475671027, /*hidden argument*/NULL);
		Object_t631007953 * L_5 = ___original0;
		Transform_t3600365921 * L_6 = ___parent1;
		bool L_7 = ___instantiateInWorldSpace2;
		Object_t631007953 * L_8 = Object_Internal_CloneSingleWithParent_m2047142090(NULL /*static, unused*/, L_5, L_6, L_7, /*hidden argument*/NULL);
		V_0 = L_8;
		goto IL_0032;
	}

IL_0032:
	{
		Object_t631007953 * L_9 = V_0;
		return L_9;
	}
}
// System.Void UnityEngine.Object::CheckNullArgument(System.Object,System.String)
extern "C"  void Object_CheckNullArgument_m129989854 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___arg0, String_t* ___message1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Object_CheckNullArgument_m129989854_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppObject * L_0 = ___arg0;
		if (L_0)
		{
			goto IL_000e;
		}
	}
	{
		String_t* L_1 = ___message1;
		ArgumentException_t132251570 * L_2 = (ArgumentException_t132251570 *)il2cpp_codegen_object_new(ArgumentException_t132251570_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m1312628991(L_2, L_1, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_000e:
	{
		return;
	}
}
// UnityEngine.Object UnityEngine.Object::FindObjectOfType(System.Type)
extern "C"  Object_t631007953 * Object_FindObjectOfType_m1736538631 (Il2CppObject * __this /* static, unused */, Type_t * ___type0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Object_FindObjectOfType_m1736538631_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ObjectU5BU5D_t1417781964* V_0 = NULL;
	Object_t631007953 * V_1 = NULL;
	{
		Type_t * L_0 = ___type0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		ObjectU5BU5D_t1417781964* L_1 = Object_FindObjectsOfType_m2898745631(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		ObjectU5BU5D_t1417781964* L_2 = V_0;
		NullCheck(L_2);
		if ((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_2)->max_length))))) <= ((int32_t)0)))
		{
			goto IL_001a;
		}
	}
	{
		ObjectU5BU5D_t1417781964* L_3 = V_0;
		NullCheck(L_3);
		int32_t L_4 = 0;
		Object_t631007953 * L_5 = (L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_4));
		V_1 = L_5;
		goto IL_0021;
	}

IL_001a:
	{
		V_1 = (Object_t631007953 *)NULL;
		goto IL_0021;
	}

IL_0021:
	{
		Object_t631007953 * L_6 = V_1;
		return L_6;
	}
}
// System.Boolean UnityEngine.Object::op_Equality(UnityEngine.Object,UnityEngine.Object)
extern "C"  bool Object_op_Equality_m1454075600 (Il2CppObject * __this /* static, unused */, Object_t631007953 * ___x0, Object_t631007953 * ___y1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Object_op_Equality_m1454075600_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	{
		Object_t631007953 * L_0 = ___x0;
		Object_t631007953 * L_1 = ___y1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_2 = Object_CompareBaseObjects_m1463006152(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		goto IL_000e;
	}

IL_000e:
	{
		bool L_3 = V_0;
		return L_3;
	}
}
// System.Boolean UnityEngine.Object::op_Inequality(UnityEngine.Object,UnityEngine.Object)
extern "C"  bool Object_op_Inequality_m1920811489 (Il2CppObject * __this /* static, unused */, Object_t631007953 * ___x0, Object_t631007953 * ___y1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Object_op_Inequality_m1920811489_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	{
		Object_t631007953 * L_0 = ___x0;
		Object_t631007953 * L_1 = ___y1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_2 = Object_CompareBaseObjects_m1463006152(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		V_0 = (bool)((((int32_t)L_2) == ((int32_t)0))? 1 : 0);
		goto IL_0011;
	}

IL_0011:
	{
		bool L_3 = V_0;
		return L_3;
	}
}
// System.Void UnityEngine.Object::.cctor()
extern "C"  void Object__cctor_m1647271852 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Object__cctor_m1647271852_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		((Object_t631007953_StaticFields*)Object_t631007953_il2cpp_TypeInfo_var->static_fields)->set_OffsetOfInstanceIDInCPlusPlusObject_1((-1));
		return;
	}
}
// System.Void UnityEngine.ParticleSystem::.ctor()
extern "C"  void ParticleSystem__ctor_m1548272662 (ParticleSystem_t1800779281 * __this, const MethodInfo* method)
{
	{
		Component__ctor_m3533368306(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Single UnityEngine.ParticleSystem::get_startDelay()
extern "C"  float ParticleSystem_get_startDelay_m1512412192 (ParticleSystem_t1800779281 * __this, const MethodInfo* method)
{
	typedef float (*ParticleSystem_get_startDelay_m1512412192_ftn) (ParticleSystem_t1800779281 *);
	static ParticleSystem_get_startDelay_m1512412192_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (ParticleSystem_get_startDelay_m1512412192_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystem::get_startDelay()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.ParticleSystem::set_startDelay(System.Single)
extern "C"  void ParticleSystem_set_startDelay_m2150878026 (ParticleSystem_t1800779281 * __this, float ___value0, const MethodInfo* method)
{
	typedef void (*ParticleSystem_set_startDelay_m2150878026_ftn) (ParticleSystem_t1800779281 *, float);
	static ParticleSystem_set_startDelay_m2150878026_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (ParticleSystem_set_startDelay_m2150878026_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystem::set_startDelay(System.Single)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Boolean UnityEngine.ParticleSystem::get_isPlaying()
extern "C"  bool ParticleSystem_get_isPlaying_m191788555 (ParticleSystem_t1800779281 * __this, const MethodInfo* method)
{
	typedef bool (*ParticleSystem_get_isPlaying_m191788555_ftn) (ParticleSystem_t1800779281 *);
	static ParticleSystem_get_isPlaying_m191788555_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (ParticleSystem_get_isPlaying_m191788555_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystem::get_isPlaying()");
	return _il2cpp_icall_func(__this);
}
// System.Boolean UnityEngine.ParticleSystem::get_isEmitting()
extern "C"  bool ParticleSystem_get_isEmitting_m3267601838 (ParticleSystem_t1800779281 * __this, const MethodInfo* method)
{
	typedef bool (*ParticleSystem_get_isEmitting_m3267601838_ftn) (ParticleSystem_t1800779281 *);
	static ParticleSystem_get_isEmitting_m3267601838_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (ParticleSystem_get_isEmitting_m3267601838_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystem::get_isEmitting()");
	return _il2cpp_icall_func(__this);
}
// System.Boolean UnityEngine.ParticleSystem::get_isStopped()
extern "C"  bool ParticleSystem_get_isStopped_m3436862387 (ParticleSystem_t1800779281 * __this, const MethodInfo* method)
{
	typedef bool (*ParticleSystem_get_isStopped_m3436862387_ftn) (ParticleSystem_t1800779281 *);
	static ParticleSystem_get_isStopped_m3436862387_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (ParticleSystem_get_isStopped_m3436862387_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystem::get_isStopped()");
	return _il2cpp_icall_func(__this);
}
// System.Boolean UnityEngine.ParticleSystem::get_isPaused()
extern "C"  bool ParticleSystem_get_isPaused_m1972161743 (ParticleSystem_t1800779281 * __this, const MethodInfo* method)
{
	typedef bool (*ParticleSystem_get_isPaused_m1972161743_ftn) (ParticleSystem_t1800779281 *);
	static ParticleSystem_get_isPaused_m1972161743_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (ParticleSystem_get_isPaused_m1972161743_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystem::get_isPaused()");
	return _il2cpp_icall_func(__this);
}
// System.Boolean UnityEngine.ParticleSystem::get_loop()
extern "C"  bool ParticleSystem_get_loop_m2932087482 (ParticleSystem_t1800779281 * __this, const MethodInfo* method)
{
	typedef bool (*ParticleSystem_get_loop_m2932087482_ftn) (ParticleSystem_t1800779281 *);
	static ParticleSystem_get_loop_m2932087482_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (ParticleSystem_get_loop_m2932087482_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystem::get_loop()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.ParticleSystem::set_loop(System.Boolean)
extern "C"  void ParticleSystem_set_loop_m3917970914 (ParticleSystem_t1800779281 * __this, bool ___value0, const MethodInfo* method)
{
	typedef void (*ParticleSystem_set_loop_m3917970914_ftn) (ParticleSystem_t1800779281 *, bool);
	static ParticleSystem_set_loop_m3917970914_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (ParticleSystem_set_loop_m3917970914_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystem::set_loop(System.Boolean)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Boolean UnityEngine.ParticleSystem::get_playOnAwake()
extern "C"  bool ParticleSystem_get_playOnAwake_m390229457 (ParticleSystem_t1800779281 * __this, const MethodInfo* method)
{
	typedef bool (*ParticleSystem_get_playOnAwake_m390229457_ftn) (ParticleSystem_t1800779281 *);
	static ParticleSystem_get_playOnAwake_m390229457_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (ParticleSystem_get_playOnAwake_m390229457_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystem::get_playOnAwake()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.ParticleSystem::set_playOnAwake(System.Boolean)
extern "C"  void ParticleSystem_set_playOnAwake_m224077176 (ParticleSystem_t1800779281 * __this, bool ___value0, const MethodInfo* method)
{
	typedef void (*ParticleSystem_set_playOnAwake_m224077176_ftn) (ParticleSystem_t1800779281 *, bool);
	static ParticleSystem_set_playOnAwake_m224077176_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (ParticleSystem_set_playOnAwake_m224077176_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystem::set_playOnAwake(System.Boolean)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Single UnityEngine.ParticleSystem::get_time()
extern "C"  float ParticleSystem_get_time_m3957651383 (ParticleSystem_t1800779281 * __this, const MethodInfo* method)
{
	typedef float (*ParticleSystem_get_time_m3957651383_ftn) (ParticleSystem_t1800779281 *);
	static ParticleSystem_get_time_m3957651383_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (ParticleSystem_get_time_m3957651383_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystem::get_time()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.ParticleSystem::set_time(System.Single)
extern "C"  void ParticleSystem_set_time_m982994956 (ParticleSystem_t1800779281 * __this, float ___value0, const MethodInfo* method)
{
	typedef void (*ParticleSystem_set_time_m982994956_ftn) (ParticleSystem_t1800779281 *, float);
	static ParticleSystem_set_time_m982994956_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (ParticleSystem_set_time_m982994956_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystem::set_time(System.Single)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Single UnityEngine.ParticleSystem::get_duration()
extern "C"  float ParticleSystem_get_duration_m3687501964 (ParticleSystem_t1800779281 * __this, const MethodInfo* method)
{
	typedef float (*ParticleSystem_get_duration_m3687501964_ftn) (ParticleSystem_t1800779281 *);
	static ParticleSystem_get_duration_m3687501964_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (ParticleSystem_get_duration_m3687501964_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystem::get_duration()");
	return _il2cpp_icall_func(__this);
}
// System.Single UnityEngine.ParticleSystem::get_playbackSpeed()
extern "C"  float ParticleSystem_get_playbackSpeed_m4039408021 (ParticleSystem_t1800779281 * __this, const MethodInfo* method)
{
	typedef float (*ParticleSystem_get_playbackSpeed_m4039408021_ftn) (ParticleSystem_t1800779281 *);
	static ParticleSystem_get_playbackSpeed_m4039408021_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (ParticleSystem_get_playbackSpeed_m4039408021_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystem::get_playbackSpeed()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.ParticleSystem::set_playbackSpeed(System.Single)
extern "C"  void ParticleSystem_set_playbackSpeed_m1005754038 (ParticleSystem_t1800779281 * __this, float ___value0, const MethodInfo* method)
{
	typedef void (*ParticleSystem_set_playbackSpeed_m1005754038_ftn) (ParticleSystem_t1800779281 *, float);
	static ParticleSystem_set_playbackSpeed_m1005754038_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (ParticleSystem_set_playbackSpeed_m1005754038_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystem::set_playbackSpeed(System.Single)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Int32 UnityEngine.ParticleSystem::get_particleCount()
extern "C"  int32_t ParticleSystem_get_particleCount_m2215853663 (ParticleSystem_t1800779281 * __this, const MethodInfo* method)
{
	typedef int32_t (*ParticleSystem_get_particleCount_m2215853663_ftn) (ParticleSystem_t1800779281 *);
	static ParticleSystem_get_particleCount_m2215853663_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (ParticleSystem_get_particleCount_m2215853663_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystem::get_particleCount()");
	return _il2cpp_icall_func(__this);
}
// System.Boolean UnityEngine.ParticleSystem::get_enableEmission()
extern "C"  bool ParticleSystem_get_enableEmission_m4048845525 (ParticleSystem_t1800779281 * __this, const MethodInfo* method)
{
	EmissionModule_t311448003  V_0;
	memset(&V_0, 0, sizeof(V_0));
	bool V_1 = false;
	{
		EmissionModule_t311448003  L_0 = ParticleSystem_get_emission_m1885497174(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		bool L_1 = EmissionModule_get_enabled_m1041402336((&V_0), /*hidden argument*/NULL);
		V_1 = L_1;
		goto IL_0015;
	}

IL_0015:
	{
		bool L_2 = V_1;
		return L_2;
	}
}
// System.Void UnityEngine.ParticleSystem::set_enableEmission(System.Boolean)
extern "C"  void ParticleSystem_set_enableEmission_m3100194443 (ParticleSystem_t1800779281 * __this, bool ___value0, const MethodInfo* method)
{
	EmissionModule_t311448003  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		EmissionModule_t311448003  L_0 = ParticleSystem_get_emission_m1885497174(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		bool L_1 = ___value0;
		EmissionModule_set_enabled_m416122042((&V_0), L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Single UnityEngine.ParticleSystem::get_emissionRate()
extern "C"  float ParticleSystem_get_emissionRate_m3797748718 (ParticleSystem_t1800779281 * __this, const MethodInfo* method)
{
	EmissionModule_t311448003  V_0;
	memset(&V_0, 0, sizeof(V_0));
	float V_1 = 0.0f;
	{
		EmissionModule_t311448003  L_0 = ParticleSystem_get_emission_m1885497174(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		float L_1 = EmissionModule_get_rateOverTimeMultiplier_m608658658((&V_0), /*hidden argument*/NULL);
		V_1 = L_1;
		goto IL_0015;
	}

IL_0015:
	{
		float L_2 = V_1;
		return L_2;
	}
}
// System.Void UnityEngine.ParticleSystem::set_emissionRate(System.Single)
extern "C"  void ParticleSystem_set_emissionRate_m605924837 (ParticleSystem_t1800779281 * __this, float ___value0, const MethodInfo* method)
{
	EmissionModule_t311448003  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		EmissionModule_t311448003  L_0 = ParticleSystem_get_emission_m1885497174(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		float L_1 = ___value0;
		MinMaxCurve_t1067599125  L_2 = MinMaxCurve_op_Implicit_m3696034763(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		EmissionModule_set_rateOverTime_m3483972715((&V_0), L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Single UnityEngine.ParticleSystem::get_startSpeed()
extern "C"  float ParticleSystem_get_startSpeed_m3561275513 (ParticleSystem_t1800779281 * __this, const MethodInfo* method)
{
	typedef float (*ParticleSystem_get_startSpeed_m3561275513_ftn) (ParticleSystem_t1800779281 *);
	static ParticleSystem_get_startSpeed_m3561275513_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (ParticleSystem_get_startSpeed_m3561275513_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystem::get_startSpeed()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.ParticleSystem::set_startSpeed(System.Single)
extern "C"  void ParticleSystem_set_startSpeed_m3732495396 (ParticleSystem_t1800779281 * __this, float ___value0, const MethodInfo* method)
{
	typedef void (*ParticleSystem_set_startSpeed_m3732495396_ftn) (ParticleSystem_t1800779281 *, float);
	static ParticleSystem_set_startSpeed_m3732495396_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (ParticleSystem_set_startSpeed_m3732495396_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystem::set_startSpeed(System.Single)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Single UnityEngine.ParticleSystem::get_startSize()
extern "C"  float ParticleSystem_get_startSize_m2159985455 (ParticleSystem_t1800779281 * __this, const MethodInfo* method)
{
	typedef float (*ParticleSystem_get_startSize_m2159985455_ftn) (ParticleSystem_t1800779281 *);
	static ParticleSystem_get_startSize_m2159985455_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (ParticleSystem_get_startSize_m2159985455_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystem::get_startSize()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.ParticleSystem::set_startSize(System.Single)
extern "C"  void ParticleSystem_set_startSize_m2064414800 (ParticleSystem_t1800779281 * __this, float ___value0, const MethodInfo* method)
{
	typedef void (*ParticleSystem_set_startSize_m2064414800_ftn) (ParticleSystem_t1800779281 *, float);
	static ParticleSystem_set_startSize_m2064414800_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (ParticleSystem_set_startSize_m2064414800_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystem::set_startSize(System.Single)");
	_il2cpp_icall_func(__this, ___value0);
}
// UnityEngine.Color UnityEngine.ParticleSystem::get_startColor()
extern "C"  Color_t2555686324  ParticleSystem_get_startColor_m3639013554 (ParticleSystem_t1800779281 * __this, const MethodInfo* method)
{
	Color_t2555686324  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Color_t2555686324  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		ParticleSystem_INTERNAL_get_startColor_m2957612057(__this, (&V_0), /*hidden argument*/NULL);
		Color_t2555686324  L_0 = V_0;
		V_1 = L_0;
		goto IL_0010;
	}

IL_0010:
	{
		Color_t2555686324  L_1 = V_1;
		return L_1;
	}
}
// System.Void UnityEngine.ParticleSystem::set_startColor(UnityEngine.Color)
extern "C"  void ParticleSystem_set_startColor_m2038655190 (ParticleSystem_t1800779281 * __this, Color_t2555686324  ___value0, const MethodInfo* method)
{
	{
		ParticleSystem_INTERNAL_set_startColor_m3394702698(__this, (&___value0), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.ParticleSystem::INTERNAL_get_startColor(UnityEngine.Color&)
extern "C"  void ParticleSystem_INTERNAL_get_startColor_m2957612057 (ParticleSystem_t1800779281 * __this, Color_t2555686324 * ___value0, const MethodInfo* method)
{
	typedef void (*ParticleSystem_INTERNAL_get_startColor_m2957612057_ftn) (ParticleSystem_t1800779281 *, Color_t2555686324 *);
	static ParticleSystem_INTERNAL_get_startColor_m2957612057_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (ParticleSystem_INTERNAL_get_startColor_m2957612057_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystem::INTERNAL_get_startColor(UnityEngine.Color&)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.ParticleSystem::INTERNAL_set_startColor(UnityEngine.Color&)
extern "C"  void ParticleSystem_INTERNAL_set_startColor_m3394702698 (ParticleSystem_t1800779281 * __this, Color_t2555686324 * ___value0, const MethodInfo* method)
{
	typedef void (*ParticleSystem_INTERNAL_set_startColor_m3394702698_ftn) (ParticleSystem_t1800779281 *, Color_t2555686324 *);
	static ParticleSystem_INTERNAL_set_startColor_m3394702698_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (ParticleSystem_INTERNAL_set_startColor_m3394702698_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystem::INTERNAL_set_startColor(UnityEngine.Color&)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Single UnityEngine.ParticleSystem::get_startRotation()
extern "C"  float ParticleSystem_get_startRotation_m901008553 (ParticleSystem_t1800779281 * __this, const MethodInfo* method)
{
	typedef float (*ParticleSystem_get_startRotation_m901008553_ftn) (ParticleSystem_t1800779281 *);
	static ParticleSystem_get_startRotation_m901008553_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (ParticleSystem_get_startRotation_m901008553_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystem::get_startRotation()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.ParticleSystem::set_startRotation(System.Single)
extern "C"  void ParticleSystem_set_startRotation_m2311911976 (ParticleSystem_t1800779281 * __this, float ___value0, const MethodInfo* method)
{
	typedef void (*ParticleSystem_set_startRotation_m2311911976_ftn) (ParticleSystem_t1800779281 *, float);
	static ParticleSystem_set_startRotation_m2311911976_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (ParticleSystem_set_startRotation_m2311911976_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystem::set_startRotation(System.Single)");
	_il2cpp_icall_func(__this, ___value0);
}
// UnityEngine.Vector3 UnityEngine.ParticleSystem::get_startRotation3D()
extern "C"  Vector3_t3722313464  ParticleSystem_get_startRotation3D_m2248049046 (ParticleSystem_t1800779281 * __this, const MethodInfo* method)
{
	Vector3_t3722313464  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector3_t3722313464  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		ParticleSystem_INTERNAL_get_startRotation3D_m361332872(__this, (&V_0), /*hidden argument*/NULL);
		Vector3_t3722313464  L_0 = V_0;
		V_1 = L_0;
		goto IL_0010;
	}

IL_0010:
	{
		Vector3_t3722313464  L_1 = V_1;
		return L_1;
	}
}
// System.Void UnityEngine.ParticleSystem::set_startRotation3D(UnityEngine.Vector3)
extern "C"  void ParticleSystem_set_startRotation3D_m1697718949 (ParticleSystem_t1800779281 * __this, Vector3_t3722313464  ___value0, const MethodInfo* method)
{
	{
		ParticleSystem_INTERNAL_set_startRotation3D_m3566101937(__this, (&___value0), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.ParticleSystem::INTERNAL_get_startRotation3D(UnityEngine.Vector3&)
extern "C"  void ParticleSystem_INTERNAL_get_startRotation3D_m361332872 (ParticleSystem_t1800779281 * __this, Vector3_t3722313464 * ___value0, const MethodInfo* method)
{
	typedef void (*ParticleSystem_INTERNAL_get_startRotation3D_m361332872_ftn) (ParticleSystem_t1800779281 *, Vector3_t3722313464 *);
	static ParticleSystem_INTERNAL_get_startRotation3D_m361332872_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (ParticleSystem_INTERNAL_get_startRotation3D_m361332872_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystem::INTERNAL_get_startRotation3D(UnityEngine.Vector3&)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.ParticleSystem::INTERNAL_set_startRotation3D(UnityEngine.Vector3&)
extern "C"  void ParticleSystem_INTERNAL_set_startRotation3D_m3566101937 (ParticleSystem_t1800779281 * __this, Vector3_t3722313464 * ___value0, const MethodInfo* method)
{
	typedef void (*ParticleSystem_INTERNAL_set_startRotation3D_m3566101937_ftn) (ParticleSystem_t1800779281 *, Vector3_t3722313464 *);
	static ParticleSystem_INTERNAL_set_startRotation3D_m3566101937_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (ParticleSystem_INTERNAL_set_startRotation3D_m3566101937_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystem::INTERNAL_set_startRotation3D(UnityEngine.Vector3&)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Single UnityEngine.ParticleSystem::get_startLifetime()
extern "C"  float ParticleSystem_get_startLifetime_m3395547700 (ParticleSystem_t1800779281 * __this, const MethodInfo* method)
{
	typedef float (*ParticleSystem_get_startLifetime_m3395547700_ftn) (ParticleSystem_t1800779281 *);
	static ParticleSystem_get_startLifetime_m3395547700_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (ParticleSystem_get_startLifetime_m3395547700_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystem::get_startLifetime()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.ParticleSystem::set_startLifetime(System.Single)
extern "C"  void ParticleSystem_set_startLifetime_m1164351017 (ParticleSystem_t1800779281 * __this, float ___value0, const MethodInfo* method)
{
	typedef void (*ParticleSystem_set_startLifetime_m1164351017_ftn) (ParticleSystem_t1800779281 *, float);
	static ParticleSystem_set_startLifetime_m1164351017_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (ParticleSystem_set_startLifetime_m1164351017_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystem::set_startLifetime(System.Single)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Single UnityEngine.ParticleSystem::get_gravityModifier()
extern "C"  float ParticleSystem_get_gravityModifier_m3257258046 (ParticleSystem_t1800779281 * __this, const MethodInfo* method)
{
	typedef float (*ParticleSystem_get_gravityModifier_m3257258046_ftn) (ParticleSystem_t1800779281 *);
	static ParticleSystem_get_gravityModifier_m3257258046_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (ParticleSystem_get_gravityModifier_m3257258046_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystem::get_gravityModifier()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.ParticleSystem::set_gravityModifier(System.Single)
extern "C"  void ParticleSystem_set_gravityModifier_m3332919233 (ParticleSystem_t1800779281 * __this, float ___value0, const MethodInfo* method)
{
	typedef void (*ParticleSystem_set_gravityModifier_m3332919233_ftn) (ParticleSystem_t1800779281 *, float);
	static ParticleSystem_set_gravityModifier_m3332919233_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (ParticleSystem_set_gravityModifier_m3332919233_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystem::set_gravityModifier(System.Single)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Int32 UnityEngine.ParticleSystem::get_maxParticles()
extern "C"  int32_t ParticleSystem_get_maxParticles_m3104164362 (ParticleSystem_t1800779281 * __this, const MethodInfo* method)
{
	typedef int32_t (*ParticleSystem_get_maxParticles_m3104164362_ftn) (ParticleSystem_t1800779281 *);
	static ParticleSystem_get_maxParticles_m3104164362_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (ParticleSystem_get_maxParticles_m3104164362_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystem::get_maxParticles()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.ParticleSystem::set_maxParticles(System.Int32)
extern "C"  void ParticleSystem_set_maxParticles_m3072274340 (ParticleSystem_t1800779281 * __this, int32_t ___value0, const MethodInfo* method)
{
	typedef void (*ParticleSystem_set_maxParticles_m3072274340_ftn) (ParticleSystem_t1800779281 *, int32_t);
	static ParticleSystem_set_maxParticles_m3072274340_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (ParticleSystem_set_maxParticles_m3072274340_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystem::set_maxParticles(System.Int32)");
	_il2cpp_icall_func(__this, ___value0);
}
// UnityEngine.ParticleSystemSimulationSpace UnityEngine.ParticleSystem::get_simulationSpace()
extern "C"  int32_t ParticleSystem_get_simulationSpace_m3314424027 (ParticleSystem_t1800779281 * __this, const MethodInfo* method)
{
	typedef int32_t (*ParticleSystem_get_simulationSpace_m3314424027_ftn) (ParticleSystem_t1800779281 *);
	static ParticleSystem_get_simulationSpace_m3314424027_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (ParticleSystem_get_simulationSpace_m3314424027_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystem::get_simulationSpace()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.ParticleSystem::set_simulationSpace(UnityEngine.ParticleSystemSimulationSpace)
extern "C"  void ParticleSystem_set_simulationSpace_m3610948568 (ParticleSystem_t1800779281 * __this, int32_t ___value0, const MethodInfo* method)
{
	typedef void (*ParticleSystem_set_simulationSpace_m3610948568_ftn) (ParticleSystem_t1800779281 *, int32_t);
	static ParticleSystem_set_simulationSpace_m3610948568_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (ParticleSystem_set_simulationSpace_m3610948568_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystem::set_simulationSpace(UnityEngine.ParticleSystemSimulationSpace)");
	_il2cpp_icall_func(__this, ___value0);
}
// UnityEngine.ParticleSystemScalingMode UnityEngine.ParticleSystem::get_scalingMode()
extern "C"  int32_t ParticleSystem_get_scalingMode_m4221152189 (ParticleSystem_t1800779281 * __this, const MethodInfo* method)
{
	typedef int32_t (*ParticleSystem_get_scalingMode_m4221152189_ftn) (ParticleSystem_t1800779281 *);
	static ParticleSystem_get_scalingMode_m4221152189_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (ParticleSystem_get_scalingMode_m4221152189_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystem::get_scalingMode()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.ParticleSystem::set_scalingMode(UnityEngine.ParticleSystemScalingMode)
extern "C"  void ParticleSystem_set_scalingMode_m1131080189 (ParticleSystem_t1800779281 * __this, int32_t ___value0, const MethodInfo* method)
{
	typedef void (*ParticleSystem_set_scalingMode_m1131080189_ftn) (ParticleSystem_t1800779281 *, int32_t);
	static ParticleSystem_set_scalingMode_m1131080189_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (ParticleSystem_set_scalingMode_m1131080189_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystem::set_scalingMode(UnityEngine.ParticleSystemScalingMode)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.UInt32 UnityEngine.ParticleSystem::get_randomSeed()
extern "C"  uint32_t ParticleSystem_get_randomSeed_m2487762561 (ParticleSystem_t1800779281 * __this, const MethodInfo* method)
{
	typedef uint32_t (*ParticleSystem_get_randomSeed_m2487762561_ftn) (ParticleSystem_t1800779281 *);
	static ParticleSystem_get_randomSeed_m2487762561_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (ParticleSystem_get_randomSeed_m2487762561_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystem::get_randomSeed()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.ParticleSystem::set_randomSeed(System.UInt32)
extern "C"  void ParticleSystem_set_randomSeed_m2214570703 (ParticleSystem_t1800779281 * __this, uint32_t ___value0, const MethodInfo* method)
{
	typedef void (*ParticleSystem_set_randomSeed_m2214570703_ftn) (ParticleSystem_t1800779281 *, uint32_t);
	static ParticleSystem_set_randomSeed_m2214570703_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (ParticleSystem_set_randomSeed_m2214570703_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystem::set_randomSeed(System.UInt32)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Boolean UnityEngine.ParticleSystem::get_useAutoRandomSeed()
extern "C"  bool ParticleSystem_get_useAutoRandomSeed_m747206249 (ParticleSystem_t1800779281 * __this, const MethodInfo* method)
{
	typedef bool (*ParticleSystem_get_useAutoRandomSeed_m747206249_ftn) (ParticleSystem_t1800779281 *);
	static ParticleSystem_get_useAutoRandomSeed_m747206249_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (ParticleSystem_get_useAutoRandomSeed_m747206249_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystem::get_useAutoRandomSeed()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.ParticleSystem::set_useAutoRandomSeed(System.Boolean)
extern "C"  void ParticleSystem_set_useAutoRandomSeed_m4177901628 (ParticleSystem_t1800779281 * __this, bool ___value0, const MethodInfo* method)
{
	typedef void (*ParticleSystem_set_useAutoRandomSeed_m4177901628_ftn) (ParticleSystem_t1800779281 *, bool);
	static ParticleSystem_set_useAutoRandomSeed_m4177901628_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (ParticleSystem_set_useAutoRandomSeed_m4177901628_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystem::set_useAutoRandomSeed(System.Boolean)");
	_il2cpp_icall_func(__this, ___value0);
}
// UnityEngine.ParticleSystem/MainModule UnityEngine.ParticleSystem::get_main()
extern "C"  MainModule_t2320046318  ParticleSystem_get_main_m1320020692 (ParticleSystem_t1800779281 * __this, const MethodInfo* method)
{
	MainModule_t2320046318  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		MainModule_t2320046318  L_0;
		memset(&L_0, 0, sizeof(L_0));
		MainModule__ctor_m158988634(&L_0, __this, /*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		MainModule_t2320046318  L_1 = V_0;
		return L_1;
	}
}
// UnityEngine.ParticleSystem/EmissionModule UnityEngine.ParticleSystem::get_emission()
extern "C"  EmissionModule_t311448003  ParticleSystem_get_emission_m1885497174 (ParticleSystem_t1800779281 * __this, const MethodInfo* method)
{
	EmissionModule_t311448003  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		EmissionModule_t311448003  L_0;
		memset(&L_0, 0, sizeof(L_0));
		EmissionModule__ctor_m1724379159(&L_0, __this, /*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		EmissionModule_t311448003  L_1 = V_0;
		return L_1;
	}
}
// UnityEngine.ParticleSystem/ShapeModule UnityEngine.ParticleSystem::get_shape()
extern "C"  ShapeModule_t3608330829  ParticleSystem_get_shape_m2843945440 (ParticleSystem_t1800779281 * __this, const MethodInfo* method)
{
	ShapeModule_t3608330829  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		ShapeModule_t3608330829  L_0;
		memset(&L_0, 0, sizeof(L_0));
		ShapeModule__ctor_m2339294796(&L_0, __this, /*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		ShapeModule_t3608330829  L_1 = V_0;
		return L_1;
	}
}
// UnityEngine.ParticleSystem/VelocityOverLifetimeModule UnityEngine.ParticleSystem::get_velocityOverLifetime()
extern "C"  VelocityOverLifetimeModule_t1982232382  ParticleSystem_get_velocityOverLifetime_m65312362 (ParticleSystem_t1800779281 * __this, const MethodInfo* method)
{
	VelocityOverLifetimeModule_t1982232382  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		VelocityOverLifetimeModule_t1982232382  L_0;
		memset(&L_0, 0, sizeof(L_0));
		VelocityOverLifetimeModule__ctor_m3772502470(&L_0, __this, /*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		VelocityOverLifetimeModule_t1982232382  L_1 = V_0;
		return L_1;
	}
}
// UnityEngine.ParticleSystem/LimitVelocityOverLifetimeModule UnityEngine.ParticleSystem::get_limitVelocityOverLifetime()
extern "C"  LimitVelocityOverLifetimeModule_t686589569  ParticleSystem_get_limitVelocityOverLifetime_m3763307969 (ParticleSystem_t1800779281 * __this, const MethodInfo* method)
{
	LimitVelocityOverLifetimeModule_t686589569  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		LimitVelocityOverLifetimeModule_t686589569  L_0;
		memset(&L_0, 0, sizeof(L_0));
		LimitVelocityOverLifetimeModule__ctor_m3158309845(&L_0, __this, /*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		LimitVelocityOverLifetimeModule_t686589569  L_1 = V_0;
		return L_1;
	}
}
// UnityEngine.ParticleSystem/InheritVelocityModule UnityEngine.ParticleSystem::get_inheritVelocity()
extern "C"  InheritVelocityModule_t3940044026  ParticleSystem_get_inheritVelocity_m816696734 (ParticleSystem_t1800779281 * __this, const MethodInfo* method)
{
	InheritVelocityModule_t3940044026  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		InheritVelocityModule_t3940044026  L_0;
		memset(&L_0, 0, sizeof(L_0));
		InheritVelocityModule__ctor_m3717584807(&L_0, __this, /*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		InheritVelocityModule_t3940044026  L_1 = V_0;
		return L_1;
	}
}
// UnityEngine.ParticleSystem/ForceOverLifetimeModule UnityEngine.ParticleSystem::get_forceOverLifetime()
extern "C"  ForceOverLifetimeModule_t4029962193  ParticleSystem_get_forceOverLifetime_m1376676148 (ParticleSystem_t1800779281 * __this, const MethodInfo* method)
{
	ForceOverLifetimeModule_t4029962193  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		ForceOverLifetimeModule_t4029962193  L_0;
		memset(&L_0, 0, sizeof(L_0));
		ForceOverLifetimeModule__ctor_m3986876230(&L_0, __this, /*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		ForceOverLifetimeModule_t4029962193  L_1 = V_0;
		return L_1;
	}
}
// UnityEngine.ParticleSystem/ColorOverLifetimeModule UnityEngine.ParticleSystem::get_colorOverLifetime()
extern "C"  ColorOverLifetimeModule_t3039228654  ParticleSystem_get_colorOverLifetime_m4153860379 (ParticleSystem_t1800779281 * __this, const MethodInfo* method)
{
	ColorOverLifetimeModule_t3039228654  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		ColorOverLifetimeModule_t3039228654  L_0;
		memset(&L_0, 0, sizeof(L_0));
		ColorOverLifetimeModule__ctor_m1411701942(&L_0, __this, /*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		ColorOverLifetimeModule_t3039228654  L_1 = V_0;
		return L_1;
	}
}
// UnityEngine.ParticleSystem/ColorBySpeedModule UnityEngine.ParticleSystem::get_colorBySpeed()
extern "C"  ColorBySpeedModule_t3740209408  ParticleSystem_get_colorBySpeed_m3466255905 (ParticleSystem_t1800779281 * __this, const MethodInfo* method)
{
	ColorBySpeedModule_t3740209408  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		ColorBySpeedModule_t3740209408  L_0;
		memset(&L_0, 0, sizeof(L_0));
		ColorBySpeedModule__ctor_m4046611376(&L_0, __this, /*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		ColorBySpeedModule_t3740209408  L_1 = V_0;
		return L_1;
	}
}
// UnityEngine.ParticleSystem/SizeOverLifetimeModule UnityEngine.ParticleSystem::get_sizeOverLifetime()
extern "C"  SizeOverLifetimeModule_t1101123803  ParticleSystem_get_sizeOverLifetime_m1676296001 (ParticleSystem_t1800779281 * __this, const MethodInfo* method)
{
	SizeOverLifetimeModule_t1101123803  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		SizeOverLifetimeModule_t1101123803  L_0;
		memset(&L_0, 0, sizeof(L_0));
		SizeOverLifetimeModule__ctor_m3730960903(&L_0, __this, /*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		SizeOverLifetimeModule_t1101123803  L_1 = V_0;
		return L_1;
	}
}
// UnityEngine.ParticleSystem/SizeBySpeedModule UnityEngine.ParticleSystem::get_sizeBySpeed()
extern "C"  SizeBySpeedModule_t1515126846  ParticleSystem_get_sizeBySpeed_m3223729278 (ParticleSystem_t1800779281 * __this, const MethodInfo* method)
{
	SizeBySpeedModule_t1515126846  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		SizeBySpeedModule_t1515126846  L_0;
		memset(&L_0, 0, sizeof(L_0));
		SizeBySpeedModule__ctor_m3934889008(&L_0, __this, /*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		SizeBySpeedModule_t1515126846  L_1 = V_0;
		return L_1;
	}
}
// UnityEngine.ParticleSystem/RotationOverLifetimeModule UnityEngine.ParticleSystem::get_rotationOverLifetime()
extern "C"  RotationOverLifetimeModule_t1164372224  ParticleSystem_get_rotationOverLifetime_m1749853397 (ParticleSystem_t1800779281 * __this, const MethodInfo* method)
{
	RotationOverLifetimeModule_t1164372224  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		RotationOverLifetimeModule_t1164372224  L_0;
		memset(&L_0, 0, sizeof(L_0));
		RotationOverLifetimeModule__ctor_m1430131584(&L_0, __this, /*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		RotationOverLifetimeModule_t1164372224  L_1 = V_0;
		return L_1;
	}
}
// UnityEngine.ParticleSystem/RotationBySpeedModule UnityEngine.ParticleSystem::get_rotationBySpeed()
extern "C"  RotationBySpeedModule_t3497409583  ParticleSystem_get_rotationBySpeed_m3862190279 (ParticleSystem_t1800779281 * __this, const MethodInfo* method)
{
	RotationBySpeedModule_t3497409583  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		RotationBySpeedModule_t3497409583  L_0;
		memset(&L_0, 0, sizeof(L_0));
		RotationBySpeedModule__ctor_m4096345870(&L_0, __this, /*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		RotationBySpeedModule_t3497409583  L_1 = V_0;
		return L_1;
	}
}
// UnityEngine.ParticleSystem/ExternalForcesModule UnityEngine.ParticleSystem::get_externalForces()
extern "C"  ExternalForcesModule_t1424795933  ParticleSystem_get_externalForces_m2427583606 (ParticleSystem_t1800779281 * __this, const MethodInfo* method)
{
	ExternalForcesModule_t1424795933  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		ExternalForcesModule_t1424795933  L_0;
		memset(&L_0, 0, sizeof(L_0));
		ExternalForcesModule__ctor_m3166001247(&L_0, __this, /*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		ExternalForcesModule_t1424795933  L_1 = V_0;
		return L_1;
	}
}
// UnityEngine.ParticleSystem/NoiseModule UnityEngine.ParticleSystem::get_noise()
extern "C"  NoiseModule_t962525627  ParticleSystem_get_noise_m2942489872 (ParticleSystem_t1800779281 * __this, const MethodInfo* method)
{
	NoiseModule_t962525627  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		NoiseModule_t962525627  L_0;
		memset(&L_0, 0, sizeof(L_0));
		NoiseModule__ctor_m1277709549(&L_0, __this, /*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		NoiseModule_t962525627  L_1 = V_0;
		return L_1;
	}
}
// UnityEngine.ParticleSystem/CollisionModule UnityEngine.ParticleSystem::get_collision()
extern "C"  CollisionModule_t1950979710  ParticleSystem_get_collision_m2870633412 (ParticleSystem_t1800779281 * __this, const MethodInfo* method)
{
	CollisionModule_t1950979710  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		CollisionModule_t1950979710  L_0;
		memset(&L_0, 0, sizeof(L_0));
		CollisionModule__ctor_m760520803(&L_0, __this, /*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		CollisionModule_t1950979710  L_1 = V_0;
		return L_1;
	}
}
// UnityEngine.ParticleSystem/TriggerModule UnityEngine.ParticleSystem::get_trigger()
extern "C"  TriggerModule_t1157986180  ParticleSystem_get_trigger_m2994846871 (ParticleSystem_t1800779281 * __this, const MethodInfo* method)
{
	TriggerModule_t1157986180  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		TriggerModule_t1157986180  L_0;
		memset(&L_0, 0, sizeof(L_0));
		TriggerModule__ctor_m1363700541(&L_0, __this, /*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		TriggerModule_t1157986180  L_1 = V_0;
		return L_1;
	}
}
// UnityEngine.ParticleSystem/SubEmittersModule UnityEngine.ParticleSystem::get_subEmitters()
extern "C"  SubEmittersModule_t903775760  ParticleSystem_get_subEmitters_m184453921 (ParticleSystem_t1800779281 * __this, const MethodInfo* method)
{
	SubEmittersModule_t903775760  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		SubEmittersModule_t903775760  L_0;
		memset(&L_0, 0, sizeof(L_0));
		SubEmittersModule__ctor_m4074391005(&L_0, __this, /*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		SubEmittersModule_t903775760  L_1 = V_0;
		return L_1;
	}
}
// UnityEngine.ParticleSystem/TextureSheetAnimationModule UnityEngine.ParticleSystem::get_textureSheetAnimation()
extern "C"  TextureSheetAnimationModule_t738696839  ParticleSystem_get_textureSheetAnimation_m1071993721 (ParticleSystem_t1800779281 * __this, const MethodInfo* method)
{
	TextureSheetAnimationModule_t738696839  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		TextureSheetAnimationModule_t738696839  L_0;
		memset(&L_0, 0, sizeof(L_0));
		TextureSheetAnimationModule__ctor_m1467095871(&L_0, __this, /*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		TextureSheetAnimationModule_t738696839  L_1 = V_0;
		return L_1;
	}
}
// UnityEngine.ParticleSystem/LightsModule UnityEngine.ParticleSystem::get_lights()
extern "C"  LightsModule_t3616883284  ParticleSystem_get_lights_m2494618544 (ParticleSystem_t1800779281 * __this, const MethodInfo* method)
{
	LightsModule_t3616883284  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		LightsModule_t3616883284  L_0;
		memset(&L_0, 0, sizeof(L_0));
		LightsModule__ctor_m4219401727(&L_0, __this, /*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		LightsModule_t3616883284  L_1 = V_0;
		return L_1;
	}
}
// UnityEngine.ParticleSystem/TrailModule UnityEngine.ParticleSystem::get_trails()
extern "C"  TrailModule_t2282589118  ParticleSystem_get_trails_m3028257377 (ParticleSystem_t1800779281 * __this, const MethodInfo* method)
{
	TrailModule_t2282589118  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		TrailModule_t2282589118  L_0;
		memset(&L_0, 0, sizeof(L_0));
		TrailModule__ctor_m1664590038(&L_0, __this, /*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		TrailModule_t2282589118  L_1 = V_0;
		return L_1;
	}
}
// UnityEngine.ParticleSystem/CustomDataModule UnityEngine.ParticleSystem::get_customData()
extern "C"  CustomDataModule_t2135829708  ParticleSystem_get_customData_m914372068 (ParticleSystem_t1800779281 * __this, const MethodInfo* method)
{
	CustomDataModule_t2135829708  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		CustomDataModule_t2135829708  L_0;
		memset(&L_0, 0, sizeof(L_0));
		CustomDataModule__ctor_m2822328115(&L_0, __this, /*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		CustomDataModule_t2135829708  L_1 = V_0;
		return L_1;
	}
}
// System.Void UnityEngine.ParticleSystem::SetParticles(UnityEngine.ParticleSystem/Particle[],System.Int32)
extern "C"  void ParticleSystem_SetParticles_m4180411805 (ParticleSystem_t1800779281 * __this, ParticleU5BU5D_t3069227754* ___particles0, int32_t ___size1, const MethodInfo* method)
{
	typedef void (*ParticleSystem_SetParticles_m4180411805_ftn) (ParticleSystem_t1800779281 *, ParticleU5BU5D_t3069227754*, int32_t);
	static ParticleSystem_SetParticles_m4180411805_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (ParticleSystem_SetParticles_m4180411805_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystem::SetParticles(UnityEngine.ParticleSystem/Particle[],System.Int32)");
	_il2cpp_icall_func(__this, ___particles0, ___size1);
}
// System.Int32 UnityEngine.ParticleSystem::GetParticles(UnityEngine.ParticleSystem/Particle[])
extern "C"  int32_t ParticleSystem_GetParticles_m3335888851 (ParticleSystem_t1800779281 * __this, ParticleU5BU5D_t3069227754* ___particles0, const MethodInfo* method)
{
	typedef int32_t (*ParticleSystem_GetParticles_m3335888851_ftn) (ParticleSystem_t1800779281 *, ParticleU5BU5D_t3069227754*);
	static ParticleSystem_GetParticles_m3335888851_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (ParticleSystem_GetParticles_m3335888851_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystem::GetParticles(UnityEngine.ParticleSystem/Particle[])");
	return _il2cpp_icall_func(__this, ___particles0);
}
// System.Void UnityEngine.ParticleSystem::SetCustomParticleData(System.Collections.Generic.List`1<UnityEngine.Vector4>,UnityEngine.ParticleSystemCustomData)
extern "C"  void ParticleSystem_SetCustomParticleData_m4070111455 (ParticleSystem_t1800779281 * __this, List_1_t496136383 * ___customData0, int32_t ___streamIndex1, const MethodInfo* method)
{
	{
		List_1_t496136383 * L_0 = ___customData0;
		int32_t L_1 = ___streamIndex1;
		ParticleSystem_SetCustomParticleDataInternal_m956969092(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Int32 UnityEngine.ParticleSystem::GetCustomParticleData(System.Collections.Generic.List`1<UnityEngine.Vector4>,UnityEngine.ParticleSystemCustomData)
extern "C"  int32_t ParticleSystem_GetCustomParticleData_m3794941335 (ParticleSystem_t1800779281 * __this, List_1_t496136383 * ___customData0, int32_t ___streamIndex1, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		List_1_t496136383 * L_0 = ___customData0;
		int32_t L_1 = ___streamIndex1;
		int32_t L_2 = ParticleSystem_GetCustomParticleDataInternal_m1088470722(__this, L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		goto IL_000f;
	}

IL_000f:
	{
		int32_t L_3 = V_0;
		return L_3;
	}
}
// System.Void UnityEngine.ParticleSystem::SetCustomParticleDataInternal(System.Object,System.Int32)
extern "C"  void ParticleSystem_SetCustomParticleDataInternal_m956969092 (ParticleSystem_t1800779281 * __this, Il2CppObject * ___customData0, int32_t ___streamIndex1, const MethodInfo* method)
{
	typedef void (*ParticleSystem_SetCustomParticleDataInternal_m956969092_ftn) (ParticleSystem_t1800779281 *, Il2CppObject *, int32_t);
	static ParticleSystem_SetCustomParticleDataInternal_m956969092_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (ParticleSystem_SetCustomParticleDataInternal_m956969092_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystem::SetCustomParticleDataInternal(System.Object,System.Int32)");
	_il2cpp_icall_func(__this, ___customData0, ___streamIndex1);
}
// System.Int32 UnityEngine.ParticleSystem::GetCustomParticleDataInternal(System.Object,System.Int32)
extern "C"  int32_t ParticleSystem_GetCustomParticleDataInternal_m1088470722 (ParticleSystem_t1800779281 * __this, Il2CppObject * ___customData0, int32_t ___streamIndex1, const MethodInfo* method)
{
	typedef int32_t (*ParticleSystem_GetCustomParticleDataInternal_m1088470722_ftn) (ParticleSystem_t1800779281 *, Il2CppObject *, int32_t);
	static ParticleSystem_GetCustomParticleDataInternal_m1088470722_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (ParticleSystem_GetCustomParticleDataInternal_m1088470722_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystem::GetCustomParticleDataInternal(System.Object,System.Int32)");
	return _il2cpp_icall_func(__this, ___customData0, ___streamIndex1);
}
// System.Boolean UnityEngine.ParticleSystem::Internal_Simulate(UnityEngine.ParticleSystem,System.Single,System.Boolean,System.Boolean)
extern "C"  bool ParticleSystem_Internal_Simulate_m30884640 (Il2CppObject * __this /* static, unused */, ParticleSystem_t1800779281 * ___self0, float ___t1, bool ___restart2, bool ___fixedTimeStep3, const MethodInfo* method)
{
	typedef bool (*ParticleSystem_Internal_Simulate_m30884640_ftn) (ParticleSystem_t1800779281 *, float, bool, bool);
	static ParticleSystem_Internal_Simulate_m30884640_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (ParticleSystem_Internal_Simulate_m30884640_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystem::Internal_Simulate(UnityEngine.ParticleSystem,System.Single,System.Boolean,System.Boolean)");
	return _il2cpp_icall_func(___self0, ___t1, ___restart2, ___fixedTimeStep3);
}
// System.Boolean UnityEngine.ParticleSystem::Internal_Play(UnityEngine.ParticleSystem)
extern "C"  bool ParticleSystem_Internal_Play_m3248398569 (Il2CppObject * __this /* static, unused */, ParticleSystem_t1800779281 * ___self0, const MethodInfo* method)
{
	typedef bool (*ParticleSystem_Internal_Play_m3248398569_ftn) (ParticleSystem_t1800779281 *);
	static ParticleSystem_Internal_Play_m3248398569_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (ParticleSystem_Internal_Play_m3248398569_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystem::Internal_Play(UnityEngine.ParticleSystem)");
	return _il2cpp_icall_func(___self0);
}
// System.Boolean UnityEngine.ParticleSystem::Internal_Stop(UnityEngine.ParticleSystem,UnityEngine.ParticleSystemStopBehavior)
extern "C"  bool ParticleSystem_Internal_Stop_m883182992 (Il2CppObject * __this /* static, unused */, ParticleSystem_t1800779281 * ___self0, int32_t ___stopBehavior1, const MethodInfo* method)
{
	typedef bool (*ParticleSystem_Internal_Stop_m883182992_ftn) (ParticleSystem_t1800779281 *, int32_t);
	static ParticleSystem_Internal_Stop_m883182992_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (ParticleSystem_Internal_Stop_m883182992_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystem::Internal_Stop(UnityEngine.ParticleSystem,UnityEngine.ParticleSystemStopBehavior)");
	return _il2cpp_icall_func(___self0, ___stopBehavior1);
}
// System.Boolean UnityEngine.ParticleSystem::Internal_Pause(UnityEngine.ParticleSystem)
extern "C"  bool ParticleSystem_Internal_Pause_m3444665175 (Il2CppObject * __this /* static, unused */, ParticleSystem_t1800779281 * ___self0, const MethodInfo* method)
{
	typedef bool (*ParticleSystem_Internal_Pause_m3444665175_ftn) (ParticleSystem_t1800779281 *);
	static ParticleSystem_Internal_Pause_m3444665175_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (ParticleSystem_Internal_Pause_m3444665175_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystem::Internal_Pause(UnityEngine.ParticleSystem)");
	return _il2cpp_icall_func(___self0);
}
// System.Boolean UnityEngine.ParticleSystem::Internal_Clear(UnityEngine.ParticleSystem)
extern "C"  bool ParticleSystem_Internal_Clear_m2418677798 (Il2CppObject * __this /* static, unused */, ParticleSystem_t1800779281 * ___self0, const MethodInfo* method)
{
	typedef bool (*ParticleSystem_Internal_Clear_m2418677798_ftn) (ParticleSystem_t1800779281 *);
	static ParticleSystem_Internal_Clear_m2418677798_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (ParticleSystem_Internal_Clear_m2418677798_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystem::Internal_Clear(UnityEngine.ParticleSystem)");
	return _il2cpp_icall_func(___self0);
}
// System.Boolean UnityEngine.ParticleSystem::Internal_IsAlive(UnityEngine.ParticleSystem)
extern "C"  bool ParticleSystem_Internal_IsAlive_m2609780182 (Il2CppObject * __this /* static, unused */, ParticleSystem_t1800779281 * ___self0, const MethodInfo* method)
{
	typedef bool (*ParticleSystem_Internal_IsAlive_m2609780182_ftn) (ParticleSystem_t1800779281 *);
	static ParticleSystem_Internal_IsAlive_m2609780182_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (ParticleSystem_Internal_IsAlive_m2609780182_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystem::Internal_IsAlive(UnityEngine.ParticleSystem)");
	return _il2cpp_icall_func(___self0);
}
// System.Void UnityEngine.ParticleSystem::Simulate(System.Single,System.Boolean,System.Boolean)
extern "C"  void ParticleSystem_Simulate_m266467848 (ParticleSystem_t1800779281 * __this, float ___t0, bool ___withChildren1, bool ___restart2, const MethodInfo* method)
{
	bool V_0 = false;
	{
		V_0 = (bool)1;
		float L_0 = ___t0;
		bool L_1 = ___withChildren1;
		bool L_2 = ___restart2;
		bool L_3 = V_0;
		ParticleSystem_Simulate_m1816766114(__this, L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.ParticleSystem::Simulate(System.Single,System.Boolean)
extern "C"  void ParticleSystem_Simulate_m893964799 (ParticleSystem_t1800779281 * __this, float ___t0, bool ___withChildren1, const MethodInfo* method)
{
	bool V_0 = false;
	bool V_1 = false;
	{
		V_0 = (bool)1;
		V_1 = (bool)1;
		float L_0 = ___t0;
		bool L_1 = ___withChildren1;
		bool L_2 = V_1;
		bool L_3 = V_0;
		ParticleSystem_Simulate_m1816766114(__this, L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.ParticleSystem::Simulate(System.Single)
extern "C"  void ParticleSystem_Simulate_m3846389385 (ParticleSystem_t1800779281 * __this, float ___t0, const MethodInfo* method)
{
	bool V_0 = false;
	bool V_1 = false;
	bool V_2 = false;
	{
		V_0 = (bool)1;
		V_1 = (bool)1;
		V_2 = (bool)1;
		float L_0 = ___t0;
		bool L_1 = V_2;
		bool L_2 = V_1;
		bool L_3 = V_0;
		ParticleSystem_Simulate_m1816766114(__this, L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.ParticleSystem::Simulate(System.Single,System.Boolean,System.Boolean,System.Boolean)
extern "C"  void ParticleSystem_Simulate_m1816766114 (ParticleSystem_t1800779281 * __this, float ___t0, bool ___withChildren1, bool ___restart2, bool ___fixedTimeStep3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ParticleSystem_Simulate_m1816766114_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CSimulateU3Ec__AnonStorey0_t651750728 * V_0 = NULL;
	{
		U3CSimulateU3Ec__AnonStorey0_t651750728 * L_0 = (U3CSimulateU3Ec__AnonStorey0_t651750728 *)il2cpp_codegen_object_new(U3CSimulateU3Ec__AnonStorey0_t651750728_il2cpp_TypeInfo_var);
		U3CSimulateU3Ec__AnonStorey0__ctor_m493021975(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CSimulateU3Ec__AnonStorey0_t651750728 * L_1 = V_0;
		float L_2 = ___t0;
		NullCheck(L_1);
		L_1->set_t_0(L_2);
		U3CSimulateU3Ec__AnonStorey0_t651750728 * L_3 = V_0;
		bool L_4 = ___restart2;
		NullCheck(L_3);
		L_3->set_restart_1(L_4);
		U3CSimulateU3Ec__AnonStorey0_t651750728 * L_5 = V_0;
		bool L_6 = ___fixedTimeStep3;
		NullCheck(L_5);
		L_5->set_fixedTimeStep_2(L_6);
		bool L_7 = ___withChildren1;
		U3CSimulateU3Ec__AnonStorey0_t651750728 * L_8 = V_0;
		IntPtr_t L_9;
		L_9.set_m_value_0((void*)(void*)U3CSimulateU3Ec__AnonStorey0_U3CU3Em__0_m1701395251_MethodInfo_var);
		IteratorDelegate_t2387635027 * L_10 = (IteratorDelegate_t2387635027 *)il2cpp_codegen_object_new(IteratorDelegate_t2387635027_il2cpp_TypeInfo_var);
		IteratorDelegate__ctor_m4019223695(L_10, L_8, L_9, /*hidden argument*/NULL);
		ParticleSystem_IterateParticleSystems_m2561813326(__this, L_7, L_10, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.ParticleSystem::Play()
extern "C"  void ParticleSystem_Play_m3463513544 (ParticleSystem_t1800779281 * __this, const MethodInfo* method)
{
	bool V_0 = false;
	{
		V_0 = (bool)1;
		bool L_0 = V_0;
		ParticleSystem_Play_m2540933819(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.ParticleSystem::Play(System.Boolean)
extern "C"  void ParticleSystem_Play_m2540933819 (ParticleSystem_t1800779281 * __this, bool ___withChildren0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ParticleSystem_Play_m2540933819_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool G_B2_0 = false;
	ParticleSystem_t1800779281 * G_B2_1 = NULL;
	bool G_B1_0 = false;
	ParticleSystem_t1800779281 * G_B1_1 = NULL;
	{
		bool L_0 = ___withChildren0;
		IteratorDelegate_t2387635027 * L_1 = ((ParticleSystem_t1800779281_StaticFields*)ParticleSystem_t1800779281_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache0_2();
		G_B1_0 = L_0;
		G_B1_1 = __this;
		if (L_1)
		{
			G_B2_0 = L_0;
			G_B2_1 = __this;
			goto IL_001b;
		}
	}
	{
		IntPtr_t L_2;
		L_2.set_m_value_0((void*)(void*)ParticleSystem_U3CPlayU3Em__0_m2559235495_MethodInfo_var);
		IteratorDelegate_t2387635027 * L_3 = (IteratorDelegate_t2387635027 *)il2cpp_codegen_object_new(IteratorDelegate_t2387635027_il2cpp_TypeInfo_var);
		IteratorDelegate__ctor_m4019223695(L_3, NULL, L_2, /*hidden argument*/NULL);
		((ParticleSystem_t1800779281_StaticFields*)ParticleSystem_t1800779281_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__amU24cache0_2(L_3);
		G_B2_0 = G_B1_0;
		G_B2_1 = G_B1_1;
	}

IL_001b:
	{
		IteratorDelegate_t2387635027 * L_4 = ((ParticleSystem_t1800779281_StaticFields*)ParticleSystem_t1800779281_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache0_2();
		NullCheck(G_B2_1);
		ParticleSystem_IterateParticleSystems_m2561813326(G_B2_1, G_B2_0, L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.ParticleSystem::Stop(System.Boolean)
extern "C"  void ParticleSystem_Stop_m3694011735 (ParticleSystem_t1800779281 * __this, bool ___withChildren0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		V_0 = 1;
		bool L_0 = ___withChildren0;
		int32_t L_1 = V_0;
		ParticleSystem_Stop_m2292358143(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.ParticleSystem::Stop()
extern "C"  void ParticleSystem_Stop_m1924602863 (ParticleSystem_t1800779281 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	bool V_1 = false;
	{
		V_0 = 1;
		V_1 = (bool)1;
		bool L_0 = V_1;
		int32_t L_1 = V_0;
		ParticleSystem_Stop_m2292358143(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.ParticleSystem::Stop(System.Boolean,UnityEngine.ParticleSystemStopBehavior)
extern "C"  void ParticleSystem_Stop_m2292358143 (ParticleSystem_t1800779281 * __this, bool ___withChildren0, int32_t ___stopBehavior1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ParticleSystem_Stop_m2292358143_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CStopU3Ec__AnonStorey1_t849324769 * V_0 = NULL;
	{
		U3CStopU3Ec__AnonStorey1_t849324769 * L_0 = (U3CStopU3Ec__AnonStorey1_t849324769 *)il2cpp_codegen_object_new(U3CStopU3Ec__AnonStorey1_t849324769_il2cpp_TypeInfo_var);
		U3CStopU3Ec__AnonStorey1__ctor_m234964104(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CStopU3Ec__AnonStorey1_t849324769 * L_1 = V_0;
		int32_t L_2 = ___stopBehavior1;
		NullCheck(L_1);
		L_1->set_stopBehavior_0(L_2);
		bool L_3 = ___withChildren0;
		U3CStopU3Ec__AnonStorey1_t849324769 * L_4 = V_0;
		IntPtr_t L_5;
		L_5.set_m_value_0((void*)(void*)U3CStopU3Ec__AnonStorey1_U3CU3Em__0_m1364512320_MethodInfo_var);
		IteratorDelegate_t2387635027 * L_6 = (IteratorDelegate_t2387635027 *)il2cpp_codegen_object_new(IteratorDelegate_t2387635027_il2cpp_TypeInfo_var);
		IteratorDelegate__ctor_m4019223695(L_6, L_4, L_5, /*hidden argument*/NULL);
		ParticleSystem_IterateParticleSystems_m2561813326(__this, L_3, L_6, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.ParticleSystem::Pause()
extern "C"  void ParticleSystem_Pause_m1604746708 (ParticleSystem_t1800779281 * __this, const MethodInfo* method)
{
	bool V_0 = false;
	{
		V_0 = (bool)1;
		bool L_0 = V_0;
		ParticleSystem_Pause_m3804393934(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.ParticleSystem::Pause(System.Boolean)
extern "C"  void ParticleSystem_Pause_m3804393934 (ParticleSystem_t1800779281 * __this, bool ___withChildren0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ParticleSystem_Pause_m3804393934_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool G_B2_0 = false;
	ParticleSystem_t1800779281 * G_B2_1 = NULL;
	bool G_B1_0 = false;
	ParticleSystem_t1800779281 * G_B1_1 = NULL;
	{
		bool L_0 = ___withChildren0;
		IteratorDelegate_t2387635027 * L_1 = ((ParticleSystem_t1800779281_StaticFields*)ParticleSystem_t1800779281_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache1_3();
		G_B1_0 = L_0;
		G_B1_1 = __this;
		if (L_1)
		{
			G_B2_0 = L_0;
			G_B2_1 = __this;
			goto IL_001b;
		}
	}
	{
		IntPtr_t L_2;
		L_2.set_m_value_0((void*)(void*)ParticleSystem_U3CPauseU3Em__1_m912216727_MethodInfo_var);
		IteratorDelegate_t2387635027 * L_3 = (IteratorDelegate_t2387635027 *)il2cpp_codegen_object_new(IteratorDelegate_t2387635027_il2cpp_TypeInfo_var);
		IteratorDelegate__ctor_m4019223695(L_3, NULL, L_2, /*hidden argument*/NULL);
		((ParticleSystem_t1800779281_StaticFields*)ParticleSystem_t1800779281_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__amU24cache1_3(L_3);
		G_B2_0 = G_B1_0;
		G_B2_1 = G_B1_1;
	}

IL_001b:
	{
		IteratorDelegate_t2387635027 * L_4 = ((ParticleSystem_t1800779281_StaticFields*)ParticleSystem_t1800779281_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache1_3();
		NullCheck(G_B2_1);
		ParticleSystem_IterateParticleSystems_m2561813326(G_B2_1, G_B2_0, L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.ParticleSystem::Clear()
extern "C"  void ParticleSystem_Clear_m1764358916 (ParticleSystem_t1800779281 * __this, const MethodInfo* method)
{
	bool V_0 = false;
	{
		V_0 = (bool)1;
		bool L_0 = V_0;
		ParticleSystem_Clear_m579965638(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.ParticleSystem::Clear(System.Boolean)
extern "C"  void ParticleSystem_Clear_m579965638 (ParticleSystem_t1800779281 * __this, bool ___withChildren0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ParticleSystem_Clear_m579965638_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool G_B2_0 = false;
	ParticleSystem_t1800779281 * G_B2_1 = NULL;
	bool G_B1_0 = false;
	ParticleSystem_t1800779281 * G_B1_1 = NULL;
	{
		bool L_0 = ___withChildren0;
		IteratorDelegate_t2387635027 * L_1 = ((ParticleSystem_t1800779281_StaticFields*)ParticleSystem_t1800779281_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache2_4();
		G_B1_0 = L_0;
		G_B1_1 = __this;
		if (L_1)
		{
			G_B2_0 = L_0;
			G_B2_1 = __this;
			goto IL_001b;
		}
	}
	{
		IntPtr_t L_2;
		L_2.set_m_value_0((void*)(void*)ParticleSystem_U3CClearU3Em__2_m1611879481_MethodInfo_var);
		IteratorDelegate_t2387635027 * L_3 = (IteratorDelegate_t2387635027 *)il2cpp_codegen_object_new(IteratorDelegate_t2387635027_il2cpp_TypeInfo_var);
		IteratorDelegate__ctor_m4019223695(L_3, NULL, L_2, /*hidden argument*/NULL);
		((ParticleSystem_t1800779281_StaticFields*)ParticleSystem_t1800779281_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__amU24cache2_4(L_3);
		G_B2_0 = G_B1_0;
		G_B2_1 = G_B1_1;
	}

IL_001b:
	{
		IteratorDelegate_t2387635027 * L_4 = ((ParticleSystem_t1800779281_StaticFields*)ParticleSystem_t1800779281_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache2_4();
		NullCheck(G_B2_1);
		ParticleSystem_IterateParticleSystems_m2561813326(G_B2_1, G_B2_0, L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean UnityEngine.ParticleSystem::IsAlive()
extern "C"  bool ParticleSystem_IsAlive_m2961387221 (ParticleSystem_t1800779281 * __this, const MethodInfo* method)
{
	bool V_0 = false;
	bool V_1 = false;
	{
		V_0 = (bool)1;
		bool L_0 = V_0;
		bool L_1 = ParticleSystem_IsAlive_m4291441292(__this, L_0, /*hidden argument*/NULL);
		V_1 = L_1;
		goto IL_0010;
	}

IL_0010:
	{
		bool L_2 = V_1;
		return L_2;
	}
}
// System.Boolean UnityEngine.ParticleSystem::IsAlive(System.Boolean)
extern "C"  bool ParticleSystem_IsAlive_m4291441292 (ParticleSystem_t1800779281 * __this, bool ___withChildren0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ParticleSystem_IsAlive_m4291441292_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	bool G_B2_0 = false;
	ParticleSystem_t1800779281 * G_B2_1 = NULL;
	bool G_B1_0 = false;
	ParticleSystem_t1800779281 * G_B1_1 = NULL;
	{
		bool L_0 = ___withChildren0;
		IteratorDelegate_t2387635027 * L_1 = ((ParticleSystem_t1800779281_StaticFields*)ParticleSystem_t1800779281_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache3_5();
		G_B1_0 = L_0;
		G_B1_1 = __this;
		if (L_1)
		{
			G_B2_0 = L_0;
			G_B2_1 = __this;
			goto IL_001b;
		}
	}
	{
		IntPtr_t L_2;
		L_2.set_m_value_0((void*)(void*)ParticleSystem_U3CIsAliveU3Em__3_m1003188869_MethodInfo_var);
		IteratorDelegate_t2387635027 * L_3 = (IteratorDelegate_t2387635027 *)il2cpp_codegen_object_new(IteratorDelegate_t2387635027_il2cpp_TypeInfo_var);
		IteratorDelegate__ctor_m4019223695(L_3, NULL, L_2, /*hidden argument*/NULL);
		((ParticleSystem_t1800779281_StaticFields*)ParticleSystem_t1800779281_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__amU24cache3_5(L_3);
		G_B2_0 = G_B1_0;
		G_B2_1 = G_B1_1;
	}

IL_001b:
	{
		IteratorDelegate_t2387635027 * L_4 = ((ParticleSystem_t1800779281_StaticFields*)ParticleSystem_t1800779281_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache3_5();
		NullCheck(G_B2_1);
		bool L_5 = ParticleSystem_IterateParticleSystems_m2561813326(G_B2_1, G_B2_0, L_4, /*hidden argument*/NULL);
		V_0 = L_5;
		goto IL_002b;
	}

IL_002b:
	{
		bool L_6 = V_0;
		return L_6;
	}
}
// System.Void UnityEngine.ParticleSystem::Emit(System.Int32)
extern "C"  void ParticleSystem_Emit_m1710861365 (ParticleSystem_t1800779281 * __this, int32_t ___count0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___count0;
		ParticleSystem_INTERNAL_CALL_Emit_m3019529794(NULL /*static, unused*/, __this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.ParticleSystem::INTERNAL_CALL_Emit(UnityEngine.ParticleSystem,System.Int32)
extern "C"  void ParticleSystem_INTERNAL_CALL_Emit_m3019529794 (Il2CppObject * __this /* static, unused */, ParticleSystem_t1800779281 * ___self0, int32_t ___count1, const MethodInfo* method)
{
	typedef void (*ParticleSystem_INTERNAL_CALL_Emit_m3019529794_ftn) (ParticleSystem_t1800779281 *, int32_t);
	static ParticleSystem_INTERNAL_CALL_Emit_m3019529794_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (ParticleSystem_INTERNAL_CALL_Emit_m3019529794_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystem::INTERNAL_CALL_Emit(UnityEngine.ParticleSystem,System.Int32)");
	_il2cpp_icall_func(___self0, ___count1);
}
// System.Void UnityEngine.ParticleSystem::Emit(UnityEngine.Vector3,UnityEngine.Vector3,System.Single,System.Single,UnityEngine.Color32)
extern "C"  void ParticleSystem_Emit_m3394618241 (ParticleSystem_t1800779281 * __this, Vector3_t3722313464  ___position0, Vector3_t3722313464  ___velocity1, float ___size2, float ___lifetime3, Color32_t2600501292  ___color4, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ParticleSystem_Emit_m3394618241_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Particle_t1882894987  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Initobj (Particle_t1882894987_il2cpp_TypeInfo_var, (&V_0));
		Vector3_t3722313464  L_0 = ___position0;
		Particle_set_position_m2092914191((&V_0), L_0, /*hidden argument*/NULL);
		Vector3_t3722313464  L_1 = ___velocity1;
		Particle_set_velocity_m3704745308((&V_0), L_1, /*hidden argument*/NULL);
		float L_2 = ___lifetime3;
		Particle_set_lifetime_m1932789319((&V_0), L_2, /*hidden argument*/NULL);
		float L_3 = ___lifetime3;
		Particle_set_startLifetime_m2269137591((&V_0), L_3, /*hidden argument*/NULL);
		float L_4 = ___size2;
		Particle_set_startSize_m662812576((&V_0), L_4, /*hidden argument*/NULL);
		Vector3_t3722313464  L_5 = Vector3_get_zero_m1640475482(NULL /*static, unused*/, /*hidden argument*/NULL);
		Particle_set_rotation3D_m2052300534((&V_0), L_5, /*hidden argument*/NULL);
		Vector3_t3722313464  L_6 = Vector3_get_zero_m1640475482(NULL /*static, unused*/, /*hidden argument*/NULL);
		Particle_set_angularVelocity3D_m648168096((&V_0), L_6, /*hidden argument*/NULL);
		Color32_t2600501292  L_7 = ___color4;
		Particle_set_startColor_m3169687975((&V_0), L_7, /*hidden argument*/NULL);
		Particle_set_randomSeed_m3843991369((&V_0), 5, /*hidden argument*/NULL);
		ParticleSystem_Internal_EmitOld_m2478817790(__this, (&V_0), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.ParticleSystem::Emit(UnityEngine.ParticleSystem/Particle)
extern "C"  void ParticleSystem_Emit_m1736982569 (ParticleSystem_t1800779281 * __this, Particle_t1882894987  ___particle0, const MethodInfo* method)
{
	{
		ParticleSystem_Internal_EmitOld_m2478817790(__this, (&___particle0), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.ParticleSystem::Internal_EmitOld(UnityEngine.ParticleSystem/Particle&)
extern "C"  void ParticleSystem_Internal_EmitOld_m2478817790 (ParticleSystem_t1800779281 * __this, Particle_t1882894987 * ___particle0, const MethodInfo* method)
{
	typedef void (*ParticleSystem_Internal_EmitOld_m2478817790_ftn) (ParticleSystem_t1800779281 *, Particle_t1882894987 *);
	static ParticleSystem_Internal_EmitOld_m2478817790_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (ParticleSystem_Internal_EmitOld_m2478817790_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystem::Internal_EmitOld(UnityEngine.ParticleSystem/Particle&)");
	_il2cpp_icall_func(__this, ___particle0);
}
// System.Void UnityEngine.ParticleSystem::Emit(UnityEngine.ParticleSystem/EmitParams,System.Int32)
extern "C"  void ParticleSystem_Emit_m2002292557 (ParticleSystem_t1800779281 * __this, EmitParams_t2216423628  ___emitParams0, int32_t ___count1, const MethodInfo* method)
{
	{
		int32_t L_0 = ___count1;
		ParticleSystem_Internal_Emit_m2404017392(__this, (&___emitParams0), L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.ParticleSystem::Internal_Emit(UnityEngine.ParticleSystem/EmitParams&,System.Int32)
extern "C"  void ParticleSystem_Internal_Emit_m2404017392 (ParticleSystem_t1800779281 * __this, EmitParams_t2216423628 * ___emitParams0, int32_t ___count1, const MethodInfo* method)
{
	typedef void (*ParticleSystem_Internal_Emit_m2404017392_ftn) (ParticleSystem_t1800779281 *, EmitParams_t2216423628 *, int32_t);
	static ParticleSystem_Internal_Emit_m2404017392_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (ParticleSystem_Internal_Emit_m2404017392_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystem::Internal_Emit(UnityEngine.ParticleSystem/EmitParams&,System.Int32)");
	_il2cpp_icall_func(__this, ___emitParams0, ___count1);
}
// System.Boolean UnityEngine.ParticleSystem::IterateParticleSystems(System.Boolean,UnityEngine.ParticleSystem/IteratorDelegate)
extern "C"  bool ParticleSystem_IterateParticleSystems_m2561813326 (ParticleSystem_t1800779281 * __this, bool ___recurse0, IteratorDelegate_t2387635027 * ___func1, const MethodInfo* method)
{
	bool V_0 = false;
	bool V_1 = false;
	{
		IteratorDelegate_t2387635027 * L_0 = ___func1;
		NullCheck(L_0);
		bool L_1 = IteratorDelegate_Invoke_m84429716(L_0, __this, /*hidden argument*/NULL);
		V_0 = L_1;
		bool L_2 = ___recurse0;
		if (!L_2)
		{
			goto IL_001e;
		}
	}
	{
		bool L_3 = V_0;
		Transform_t3600365921 * L_4 = Component_get_transform_m2921103810(__this, /*hidden argument*/NULL);
		IteratorDelegate_t2387635027 * L_5 = ___func1;
		bool L_6 = ParticleSystem_IterateParticleSystemsRecursive_m2529097660(NULL /*static, unused*/, L_4, L_5, /*hidden argument*/NULL);
		V_0 = (bool)((int32_t)((int32_t)L_3|(int32_t)L_6));
	}

IL_001e:
	{
		bool L_7 = V_0;
		V_1 = L_7;
		goto IL_0025;
	}

IL_0025:
	{
		bool L_8 = V_1;
		return L_8;
	}
}
// System.Boolean UnityEngine.ParticleSystem::IterateParticleSystemsRecursive(UnityEngine.Transform,UnityEngine.ParticleSystem/IteratorDelegate)
extern "C"  bool ParticleSystem_IterateParticleSystemsRecursive_m2529097660 (Il2CppObject * __this /* static, unused */, Transform_t3600365921 * ___transform0, IteratorDelegate_t2387635027 * ___func1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ParticleSystem_IterateParticleSystemsRecursive_m2529097660_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	Transform_t3600365921 * V_3 = NULL;
	ParticleSystem_t1800779281 * V_4 = NULL;
	bool V_5 = false;
	{
		V_0 = (bool)0;
		Transform_t3600365921 * L_0 = ___transform0;
		NullCheck(L_0);
		int32_t L_1 = Transform_get_childCount_m4033131441(L_0, /*hidden argument*/NULL);
		V_1 = L_1;
		V_2 = 0;
		goto IL_0057;
	}

IL_0011:
	{
		Transform_t3600365921 * L_2 = ___transform0;
		int32_t L_3 = V_2;
		NullCheck(L_2);
		Transform_t3600365921 * L_4 = Transform_GetChild_m3541171965(L_2, L_3, /*hidden argument*/NULL);
		V_3 = L_4;
		Transform_t3600365921 * L_5 = V_3;
		NullCheck(L_5);
		GameObject_t1113636619 * L_6 = Component_get_gameObject_m2648350745(L_5, /*hidden argument*/NULL);
		NullCheck(L_6);
		ParticleSystem_t1800779281 * L_7 = GameObject_GetComponent_TisParticleSystem_t1800779281_m3126536506(L_6, /*hidden argument*/GameObject_GetComponent_TisParticleSystem_t1800779281_m3126536506_MethodInfo_var);
		V_4 = L_7;
		ParticleSystem_t1800779281 * L_8 = V_4;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_9 = Object_op_Inequality_m1920811489(NULL /*static, unused*/, L_8, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_9)
		{
			goto IL_0052;
		}
	}
	{
		IteratorDelegate_t2387635027 * L_10 = ___func1;
		ParticleSystem_t1800779281 * L_11 = V_4;
		NullCheck(L_10);
		bool L_12 = IteratorDelegate_Invoke_m84429716(L_10, L_11, /*hidden argument*/NULL);
		V_0 = L_12;
		bool L_13 = V_0;
		if (!L_13)
		{
			goto IL_0049;
		}
	}
	{
		goto IL_005e;
	}

IL_0049:
	{
		Transform_t3600365921 * L_14 = V_3;
		IteratorDelegate_t2387635027 * L_15 = ___func1;
		ParticleSystem_IterateParticleSystemsRecursive_m2529097660(NULL /*static, unused*/, L_14, L_15, /*hidden argument*/NULL);
	}

IL_0052:
	{
		int32_t L_16 = V_2;
		V_2 = ((int32_t)((int32_t)L_16+(int32_t)1));
	}

IL_0057:
	{
		int32_t L_17 = V_2;
		int32_t L_18 = V_1;
		if ((((int32_t)L_17) < ((int32_t)L_18)))
		{
			goto IL_0011;
		}
	}

IL_005e:
	{
		bool L_19 = V_0;
		V_5 = L_19;
		goto IL_0066;
	}

IL_0066:
	{
		bool L_20 = V_5;
		return L_20;
	}
}
// System.Boolean UnityEngine.ParticleSystem::<Play>m__0(UnityEngine.ParticleSystem)
extern "C"  bool ParticleSystem_U3CPlayU3Em__0_m2559235495 (Il2CppObject * __this /* static, unused */, ParticleSystem_t1800779281 * ___ps0, const MethodInfo* method)
{
	bool V_0 = false;
	{
		ParticleSystem_t1800779281 * L_0 = ___ps0;
		bool L_1 = ParticleSystem_Internal_Play_m3248398569(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_000c;
	}

IL_000c:
	{
		bool L_2 = V_0;
		return L_2;
	}
}
// System.Boolean UnityEngine.ParticleSystem::<Pause>m__1(UnityEngine.ParticleSystem)
extern "C"  bool ParticleSystem_U3CPauseU3Em__1_m912216727 (Il2CppObject * __this /* static, unused */, ParticleSystem_t1800779281 * ___ps0, const MethodInfo* method)
{
	bool V_0 = false;
	{
		ParticleSystem_t1800779281 * L_0 = ___ps0;
		bool L_1 = ParticleSystem_Internal_Pause_m3444665175(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_000c;
	}

IL_000c:
	{
		bool L_2 = V_0;
		return L_2;
	}
}
// System.Boolean UnityEngine.ParticleSystem::<Clear>m__2(UnityEngine.ParticleSystem)
extern "C"  bool ParticleSystem_U3CClearU3Em__2_m1611879481 (Il2CppObject * __this /* static, unused */, ParticleSystem_t1800779281 * ___ps0, const MethodInfo* method)
{
	bool V_0 = false;
	{
		ParticleSystem_t1800779281 * L_0 = ___ps0;
		bool L_1 = ParticleSystem_Internal_Clear_m2418677798(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_000c;
	}

IL_000c:
	{
		bool L_2 = V_0;
		return L_2;
	}
}
// System.Boolean UnityEngine.ParticleSystem::<IsAlive>m__3(UnityEngine.ParticleSystem)
extern "C"  bool ParticleSystem_U3CIsAliveU3Em__3_m1003188869 (Il2CppObject * __this /* static, unused */, ParticleSystem_t1800779281 * ___ps0, const MethodInfo* method)
{
	bool V_0 = false;
	{
		ParticleSystem_t1800779281 * L_0 = ___ps0;
		bool L_1 = ParticleSystem_Internal_IsAlive_m2609780182(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_000c;
	}

IL_000c:
	{
		bool L_2 = V_0;
		return L_2;
	}
}
// System.Void UnityEngine.ParticleSystem/<Simulate>c__AnonStorey0::.ctor()
extern "C"  void U3CSimulateU3Ec__AnonStorey0__ctor_m493021975 (U3CSimulateU3Ec__AnonStorey0_t651750728 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m297566312(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean UnityEngine.ParticleSystem/<Simulate>c__AnonStorey0::<>m__0(UnityEngine.ParticleSystem)
extern "C"  bool U3CSimulateU3Ec__AnonStorey0_U3CU3Em__0_m1701395251 (U3CSimulateU3Ec__AnonStorey0_t651750728 * __this, ParticleSystem_t1800779281 * ___ps0, const MethodInfo* method)
{
	bool V_0 = false;
	{
		ParticleSystem_t1800779281 * L_0 = ___ps0;
		float L_1 = __this->get_t_0();
		bool L_2 = __this->get_restart_1();
		bool L_3 = __this->get_fixedTimeStep_2();
		bool L_4 = ParticleSystem_Internal_Simulate_m30884640(NULL /*static, unused*/, L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		goto IL_001e;
	}

IL_001e:
	{
		bool L_5 = V_0;
		return L_5;
	}
}
// System.Void UnityEngine.ParticleSystem/<Stop>c__AnonStorey1::.ctor()
extern "C"  void U3CStopU3Ec__AnonStorey1__ctor_m234964104 (U3CStopU3Ec__AnonStorey1_t849324769 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m297566312(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean UnityEngine.ParticleSystem/<Stop>c__AnonStorey1::<>m__0(UnityEngine.ParticleSystem)
extern "C"  bool U3CStopU3Ec__AnonStorey1_U3CU3Em__0_m1364512320 (U3CStopU3Ec__AnonStorey1_t849324769 * __this, ParticleSystem_t1800779281 * ___ps0, const MethodInfo* method)
{
	bool V_0 = false;
	{
		ParticleSystem_t1800779281 * L_0 = ___ps0;
		int32_t L_1 = __this->get_stopBehavior_0();
		bool L_2 = ParticleSystem_Internal_Stop_m883182992(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		goto IL_0012;
	}

IL_0012:
	{
		bool L_3 = V_0;
		return L_3;
	}
}
// Conversion methods for marshalling of: UnityEngine.ParticleSystem/CollisionModule
extern "C" void CollisionModule_t1950979710_marshal_pinvoke(const CollisionModule_t1950979710& unmarshaled, CollisionModule_t1950979710_marshaled_pinvoke& marshaled)
{
	Il2CppCodeGenException* ___m_ParticleSystem_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_ParticleSystem' of type 'CollisionModule': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_ParticleSystem_0Exception);
}
extern "C" void CollisionModule_t1950979710_marshal_pinvoke_back(const CollisionModule_t1950979710_marshaled_pinvoke& marshaled, CollisionModule_t1950979710& unmarshaled)
{
	Il2CppCodeGenException* ___m_ParticleSystem_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_ParticleSystem' of type 'CollisionModule': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_ParticleSystem_0Exception);
}
// Conversion method for clean up from marshalling of: UnityEngine.ParticleSystem/CollisionModule
extern "C" void CollisionModule_t1950979710_marshal_pinvoke_cleanup(CollisionModule_t1950979710_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.ParticleSystem/CollisionModule
extern "C" void CollisionModule_t1950979710_marshal_com(const CollisionModule_t1950979710& unmarshaled, CollisionModule_t1950979710_marshaled_com& marshaled)
{
	Il2CppCodeGenException* ___m_ParticleSystem_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_ParticleSystem' of type 'CollisionModule': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_ParticleSystem_0Exception);
}
extern "C" void CollisionModule_t1950979710_marshal_com_back(const CollisionModule_t1950979710_marshaled_com& marshaled, CollisionModule_t1950979710& unmarshaled)
{
	Il2CppCodeGenException* ___m_ParticleSystem_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_ParticleSystem' of type 'CollisionModule': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_ParticleSystem_0Exception);
}
// Conversion method for clean up from marshalling of: UnityEngine.ParticleSystem/CollisionModule
extern "C" void CollisionModule_t1950979710_marshal_com_cleanup(CollisionModule_t1950979710_marshaled_com& marshaled)
{
}
// System.Void UnityEngine.ParticleSystem/CollisionModule::.ctor(UnityEngine.ParticleSystem)
extern "C"  void CollisionModule__ctor_m760520803 (CollisionModule_t1950979710 * __this, ParticleSystem_t1800779281 * ___particleSystem0, const MethodInfo* method)
{
	{
		ParticleSystem_t1800779281 * L_0 = ___particleSystem0;
		__this->set_m_ParticleSystem_0(L_0);
		return;
	}
}
extern "C"  void CollisionModule__ctor_m760520803_AdjustorThunk (Il2CppObject * __this, ParticleSystem_t1800779281 * ___particleSystem0, const MethodInfo* method)
{
	CollisionModule_t1950979710 * _thisAdjusted = reinterpret_cast<CollisionModule_t1950979710 *>(__this + 1);
	CollisionModule__ctor_m760520803(_thisAdjusted, ___particleSystem0, method);
}
// Conversion methods for marshalling of: UnityEngine.ParticleSystem/ColorBySpeedModule
extern "C" void ColorBySpeedModule_t3740209408_marshal_pinvoke(const ColorBySpeedModule_t3740209408& unmarshaled, ColorBySpeedModule_t3740209408_marshaled_pinvoke& marshaled)
{
	Il2CppCodeGenException* ___m_ParticleSystem_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_ParticleSystem' of type 'ColorBySpeedModule': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_ParticleSystem_0Exception);
}
extern "C" void ColorBySpeedModule_t3740209408_marshal_pinvoke_back(const ColorBySpeedModule_t3740209408_marshaled_pinvoke& marshaled, ColorBySpeedModule_t3740209408& unmarshaled)
{
	Il2CppCodeGenException* ___m_ParticleSystem_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_ParticleSystem' of type 'ColorBySpeedModule': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_ParticleSystem_0Exception);
}
// Conversion method for clean up from marshalling of: UnityEngine.ParticleSystem/ColorBySpeedModule
extern "C" void ColorBySpeedModule_t3740209408_marshal_pinvoke_cleanup(ColorBySpeedModule_t3740209408_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.ParticleSystem/ColorBySpeedModule
extern "C" void ColorBySpeedModule_t3740209408_marshal_com(const ColorBySpeedModule_t3740209408& unmarshaled, ColorBySpeedModule_t3740209408_marshaled_com& marshaled)
{
	Il2CppCodeGenException* ___m_ParticleSystem_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_ParticleSystem' of type 'ColorBySpeedModule': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_ParticleSystem_0Exception);
}
extern "C" void ColorBySpeedModule_t3740209408_marshal_com_back(const ColorBySpeedModule_t3740209408_marshaled_com& marshaled, ColorBySpeedModule_t3740209408& unmarshaled)
{
	Il2CppCodeGenException* ___m_ParticleSystem_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_ParticleSystem' of type 'ColorBySpeedModule': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_ParticleSystem_0Exception);
}
// Conversion method for clean up from marshalling of: UnityEngine.ParticleSystem/ColorBySpeedModule
extern "C" void ColorBySpeedModule_t3740209408_marshal_com_cleanup(ColorBySpeedModule_t3740209408_marshaled_com& marshaled)
{
}
// System.Void UnityEngine.ParticleSystem/ColorBySpeedModule::.ctor(UnityEngine.ParticleSystem)
extern "C"  void ColorBySpeedModule__ctor_m4046611376 (ColorBySpeedModule_t3740209408 * __this, ParticleSystem_t1800779281 * ___particleSystem0, const MethodInfo* method)
{
	{
		ParticleSystem_t1800779281 * L_0 = ___particleSystem0;
		__this->set_m_ParticleSystem_0(L_0);
		return;
	}
}
extern "C"  void ColorBySpeedModule__ctor_m4046611376_AdjustorThunk (Il2CppObject * __this, ParticleSystem_t1800779281 * ___particleSystem0, const MethodInfo* method)
{
	ColorBySpeedModule_t3740209408 * _thisAdjusted = reinterpret_cast<ColorBySpeedModule_t3740209408 *>(__this + 1);
	ColorBySpeedModule__ctor_m4046611376(_thisAdjusted, ___particleSystem0, method);
}
// Conversion methods for marshalling of: UnityEngine.ParticleSystem/ColorOverLifetimeModule
extern "C" void ColorOverLifetimeModule_t3039228654_marshal_pinvoke(const ColorOverLifetimeModule_t3039228654& unmarshaled, ColorOverLifetimeModule_t3039228654_marshaled_pinvoke& marshaled)
{
	Il2CppCodeGenException* ___m_ParticleSystem_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_ParticleSystem' of type 'ColorOverLifetimeModule': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_ParticleSystem_0Exception);
}
extern "C" void ColorOverLifetimeModule_t3039228654_marshal_pinvoke_back(const ColorOverLifetimeModule_t3039228654_marshaled_pinvoke& marshaled, ColorOverLifetimeModule_t3039228654& unmarshaled)
{
	Il2CppCodeGenException* ___m_ParticleSystem_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_ParticleSystem' of type 'ColorOverLifetimeModule': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_ParticleSystem_0Exception);
}
// Conversion method for clean up from marshalling of: UnityEngine.ParticleSystem/ColorOverLifetimeModule
extern "C" void ColorOverLifetimeModule_t3039228654_marshal_pinvoke_cleanup(ColorOverLifetimeModule_t3039228654_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.ParticleSystem/ColorOverLifetimeModule
extern "C" void ColorOverLifetimeModule_t3039228654_marshal_com(const ColorOverLifetimeModule_t3039228654& unmarshaled, ColorOverLifetimeModule_t3039228654_marshaled_com& marshaled)
{
	Il2CppCodeGenException* ___m_ParticleSystem_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_ParticleSystem' of type 'ColorOverLifetimeModule': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_ParticleSystem_0Exception);
}
extern "C" void ColorOverLifetimeModule_t3039228654_marshal_com_back(const ColorOverLifetimeModule_t3039228654_marshaled_com& marshaled, ColorOverLifetimeModule_t3039228654& unmarshaled)
{
	Il2CppCodeGenException* ___m_ParticleSystem_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_ParticleSystem' of type 'ColorOverLifetimeModule': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_ParticleSystem_0Exception);
}
// Conversion method for clean up from marshalling of: UnityEngine.ParticleSystem/ColorOverLifetimeModule
extern "C" void ColorOverLifetimeModule_t3039228654_marshal_com_cleanup(ColorOverLifetimeModule_t3039228654_marshaled_com& marshaled)
{
}
// System.Void UnityEngine.ParticleSystem/ColorOverLifetimeModule::.ctor(UnityEngine.ParticleSystem)
extern "C"  void ColorOverLifetimeModule__ctor_m1411701942 (ColorOverLifetimeModule_t3039228654 * __this, ParticleSystem_t1800779281 * ___particleSystem0, const MethodInfo* method)
{
	{
		ParticleSystem_t1800779281 * L_0 = ___particleSystem0;
		__this->set_m_ParticleSystem_0(L_0);
		return;
	}
}
extern "C"  void ColorOverLifetimeModule__ctor_m1411701942_AdjustorThunk (Il2CppObject * __this, ParticleSystem_t1800779281 * ___particleSystem0, const MethodInfo* method)
{
	ColorOverLifetimeModule_t3039228654 * _thisAdjusted = reinterpret_cast<ColorOverLifetimeModule_t3039228654 *>(__this + 1);
	ColorOverLifetimeModule__ctor_m1411701942(_thisAdjusted, ___particleSystem0, method);
}
// Conversion methods for marshalling of: UnityEngine.ParticleSystem/CustomDataModule
extern "C" void CustomDataModule_t2135829708_marshal_pinvoke(const CustomDataModule_t2135829708& unmarshaled, CustomDataModule_t2135829708_marshaled_pinvoke& marshaled)
{
	Il2CppCodeGenException* ___m_ParticleSystem_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_ParticleSystem' of type 'CustomDataModule': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_ParticleSystem_0Exception);
}
extern "C" void CustomDataModule_t2135829708_marshal_pinvoke_back(const CustomDataModule_t2135829708_marshaled_pinvoke& marshaled, CustomDataModule_t2135829708& unmarshaled)
{
	Il2CppCodeGenException* ___m_ParticleSystem_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_ParticleSystem' of type 'CustomDataModule': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_ParticleSystem_0Exception);
}
// Conversion method for clean up from marshalling of: UnityEngine.ParticleSystem/CustomDataModule
extern "C" void CustomDataModule_t2135829708_marshal_pinvoke_cleanup(CustomDataModule_t2135829708_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.ParticleSystem/CustomDataModule
extern "C" void CustomDataModule_t2135829708_marshal_com(const CustomDataModule_t2135829708& unmarshaled, CustomDataModule_t2135829708_marshaled_com& marshaled)
{
	Il2CppCodeGenException* ___m_ParticleSystem_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_ParticleSystem' of type 'CustomDataModule': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_ParticleSystem_0Exception);
}
extern "C" void CustomDataModule_t2135829708_marshal_com_back(const CustomDataModule_t2135829708_marshaled_com& marshaled, CustomDataModule_t2135829708& unmarshaled)
{
	Il2CppCodeGenException* ___m_ParticleSystem_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_ParticleSystem' of type 'CustomDataModule': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_ParticleSystem_0Exception);
}
// Conversion method for clean up from marshalling of: UnityEngine.ParticleSystem/CustomDataModule
extern "C" void CustomDataModule_t2135829708_marshal_com_cleanup(CustomDataModule_t2135829708_marshaled_com& marshaled)
{
}
// System.Void UnityEngine.ParticleSystem/CustomDataModule::.ctor(UnityEngine.ParticleSystem)
extern "C"  void CustomDataModule__ctor_m2822328115 (CustomDataModule_t2135829708 * __this, ParticleSystem_t1800779281 * ___particleSystem0, const MethodInfo* method)
{
	{
		ParticleSystem_t1800779281 * L_0 = ___particleSystem0;
		__this->set_m_ParticleSystem_0(L_0);
		return;
	}
}
extern "C"  void CustomDataModule__ctor_m2822328115_AdjustorThunk (Il2CppObject * __this, ParticleSystem_t1800779281 * ___particleSystem0, const MethodInfo* method)
{
	CustomDataModule_t2135829708 * _thisAdjusted = reinterpret_cast<CustomDataModule_t2135829708 *>(__this + 1);
	CustomDataModule__ctor_m2822328115(_thisAdjusted, ___particleSystem0, method);
}
// Conversion methods for marshalling of: UnityEngine.ParticleSystem/EmissionModule
extern "C" void EmissionModule_t311448003_marshal_pinvoke(const EmissionModule_t311448003& unmarshaled, EmissionModule_t311448003_marshaled_pinvoke& marshaled)
{
	Il2CppCodeGenException* ___m_ParticleSystem_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_ParticleSystem' of type 'EmissionModule': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_ParticleSystem_0Exception);
}
extern "C" void EmissionModule_t311448003_marshal_pinvoke_back(const EmissionModule_t311448003_marshaled_pinvoke& marshaled, EmissionModule_t311448003& unmarshaled)
{
	Il2CppCodeGenException* ___m_ParticleSystem_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_ParticleSystem' of type 'EmissionModule': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_ParticleSystem_0Exception);
}
// Conversion method for clean up from marshalling of: UnityEngine.ParticleSystem/EmissionModule
extern "C" void EmissionModule_t311448003_marshal_pinvoke_cleanup(EmissionModule_t311448003_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.ParticleSystem/EmissionModule
extern "C" void EmissionModule_t311448003_marshal_com(const EmissionModule_t311448003& unmarshaled, EmissionModule_t311448003_marshaled_com& marshaled)
{
	Il2CppCodeGenException* ___m_ParticleSystem_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_ParticleSystem' of type 'EmissionModule': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_ParticleSystem_0Exception);
}
extern "C" void EmissionModule_t311448003_marshal_com_back(const EmissionModule_t311448003_marshaled_com& marshaled, EmissionModule_t311448003& unmarshaled)
{
	Il2CppCodeGenException* ___m_ParticleSystem_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_ParticleSystem' of type 'EmissionModule': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_ParticleSystem_0Exception);
}
// Conversion method for clean up from marshalling of: UnityEngine.ParticleSystem/EmissionModule
extern "C" void EmissionModule_t311448003_marshal_com_cleanup(EmissionModule_t311448003_marshaled_com& marshaled)
{
}
// System.Void UnityEngine.ParticleSystem/EmissionModule::.ctor(UnityEngine.ParticleSystem)
extern "C"  void EmissionModule__ctor_m1724379159 (EmissionModule_t311448003 * __this, ParticleSystem_t1800779281 * ___particleSystem0, const MethodInfo* method)
{
	{
		ParticleSystem_t1800779281 * L_0 = ___particleSystem0;
		__this->set_m_ParticleSystem_0(L_0);
		return;
	}
}
extern "C"  void EmissionModule__ctor_m1724379159_AdjustorThunk (Il2CppObject * __this, ParticleSystem_t1800779281 * ___particleSystem0, const MethodInfo* method)
{
	EmissionModule_t311448003 * _thisAdjusted = reinterpret_cast<EmissionModule_t311448003 *>(__this + 1);
	EmissionModule__ctor_m1724379159(_thisAdjusted, ___particleSystem0, method);
}
// System.Void UnityEngine.ParticleSystem/EmissionModule::set_enabled(System.Boolean)
extern "C"  void EmissionModule_set_enabled_m416122042 (EmissionModule_t311448003 * __this, bool ___value0, const MethodInfo* method)
{
	{
		ParticleSystem_t1800779281 * L_0 = __this->get_m_ParticleSystem_0();
		bool L_1 = ___value0;
		EmissionModule_SetEnabled_m2774397187(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
extern "C"  void EmissionModule_set_enabled_m416122042_AdjustorThunk (Il2CppObject * __this, bool ___value0, const MethodInfo* method)
{
	EmissionModule_t311448003 * _thisAdjusted = reinterpret_cast<EmissionModule_t311448003 *>(__this + 1);
	EmissionModule_set_enabled_m416122042(_thisAdjusted, ___value0, method);
}
// System.Boolean UnityEngine.ParticleSystem/EmissionModule::get_enabled()
extern "C"  bool EmissionModule_get_enabled_m1041402336 (EmissionModule_t311448003 * __this, const MethodInfo* method)
{
	bool V_0 = false;
	{
		ParticleSystem_t1800779281 * L_0 = __this->get_m_ParticleSystem_0();
		bool L_1 = EmissionModule_GetEnabled_m623574933(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_0012;
	}

IL_0012:
	{
		bool L_2 = V_0;
		return L_2;
	}
}
extern "C"  bool EmissionModule_get_enabled_m1041402336_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	EmissionModule_t311448003 * _thisAdjusted = reinterpret_cast<EmissionModule_t311448003 *>(__this + 1);
	return EmissionModule_get_enabled_m1041402336(_thisAdjusted, method);
}
// System.Void UnityEngine.ParticleSystem/EmissionModule::set_rateOverTime(UnityEngine.ParticleSystem/MinMaxCurve)
extern "C"  void EmissionModule_set_rateOverTime_m3483972715 (EmissionModule_t311448003 * __this, MinMaxCurve_t1067599125  ___value0, const MethodInfo* method)
{
	{
		ParticleSystem_t1800779281 * L_0 = __this->get_m_ParticleSystem_0();
		EmissionModule_SetRateOverTime_m2957047264(NULL /*static, unused*/, L_0, (&___value0), /*hidden argument*/NULL);
		return;
	}
}
extern "C"  void EmissionModule_set_rateOverTime_m3483972715_AdjustorThunk (Il2CppObject * __this, MinMaxCurve_t1067599125  ___value0, const MethodInfo* method)
{
	EmissionModule_t311448003 * _thisAdjusted = reinterpret_cast<EmissionModule_t311448003 *>(__this + 1);
	EmissionModule_set_rateOverTime_m3483972715(_thisAdjusted, ___value0, method);
}
// System.Single UnityEngine.ParticleSystem/EmissionModule::get_rateOverTimeMultiplier()
extern "C"  float EmissionModule_get_rateOverTimeMultiplier_m608658658 (EmissionModule_t311448003 * __this, const MethodInfo* method)
{
	float V_0 = 0.0f;
	{
		ParticleSystem_t1800779281 * L_0 = __this->get_m_ParticleSystem_0();
		float L_1 = EmissionModule_GetRateOverTimeMultiplier_m3575436295(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_0012;
	}

IL_0012:
	{
		float L_2 = V_0;
		return L_2;
	}
}
extern "C"  float EmissionModule_get_rateOverTimeMultiplier_m608658658_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	EmissionModule_t311448003 * _thisAdjusted = reinterpret_cast<EmissionModule_t311448003 *>(__this + 1);
	return EmissionModule_get_rateOverTimeMultiplier_m608658658(_thisAdjusted, method);
}
// System.Void UnityEngine.ParticleSystem/EmissionModule::SetEnabled(UnityEngine.ParticleSystem,System.Boolean)
extern "C"  void EmissionModule_SetEnabled_m2774397187 (Il2CppObject * __this /* static, unused */, ParticleSystem_t1800779281 * ___system0, bool ___value1, const MethodInfo* method)
{
	typedef void (*EmissionModule_SetEnabled_m2774397187_ftn) (ParticleSystem_t1800779281 *, bool);
	static EmissionModule_SetEnabled_m2774397187_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (EmissionModule_SetEnabled_m2774397187_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystem/EmissionModule::SetEnabled(UnityEngine.ParticleSystem,System.Boolean)");
	_il2cpp_icall_func(___system0, ___value1);
}
// System.Boolean UnityEngine.ParticleSystem/EmissionModule::GetEnabled(UnityEngine.ParticleSystem)
extern "C"  bool EmissionModule_GetEnabled_m623574933 (Il2CppObject * __this /* static, unused */, ParticleSystem_t1800779281 * ___system0, const MethodInfo* method)
{
	typedef bool (*EmissionModule_GetEnabled_m623574933_ftn) (ParticleSystem_t1800779281 *);
	static EmissionModule_GetEnabled_m623574933_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (EmissionModule_GetEnabled_m623574933_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystem/EmissionModule::GetEnabled(UnityEngine.ParticleSystem)");
	return _il2cpp_icall_func(___system0);
}
// System.Void UnityEngine.ParticleSystem/EmissionModule::SetRateOverTime(UnityEngine.ParticleSystem,UnityEngine.ParticleSystem/MinMaxCurve&)
extern "C"  void EmissionModule_SetRateOverTime_m2957047264 (Il2CppObject * __this /* static, unused */, ParticleSystem_t1800779281 * ___system0, MinMaxCurve_t1067599125 * ___curve1, const MethodInfo* method)
{
	typedef void (*EmissionModule_SetRateOverTime_m2957047264_ftn) (ParticleSystem_t1800779281 *, MinMaxCurve_t1067599125 *);
	static EmissionModule_SetRateOverTime_m2957047264_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (EmissionModule_SetRateOverTime_m2957047264_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystem/EmissionModule::SetRateOverTime(UnityEngine.ParticleSystem,UnityEngine.ParticleSystem/MinMaxCurve&)");
	_il2cpp_icall_func(___system0, ___curve1);
}
// System.Single UnityEngine.ParticleSystem/EmissionModule::GetRateOverTimeMultiplier(UnityEngine.ParticleSystem)
extern "C"  float EmissionModule_GetRateOverTimeMultiplier_m3575436295 (Il2CppObject * __this /* static, unused */, ParticleSystem_t1800779281 * ___system0, const MethodInfo* method)
{
	typedef float (*EmissionModule_GetRateOverTimeMultiplier_m3575436295_ftn) (ParticleSystem_t1800779281 *);
	static EmissionModule_GetRateOverTimeMultiplier_m3575436295_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (EmissionModule_GetRateOverTimeMultiplier_m3575436295_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ParticleSystem/EmissionModule::GetRateOverTimeMultiplier(UnityEngine.ParticleSystem)");
	return _il2cpp_icall_func(___system0);
}
// Conversion methods for marshalling of: UnityEngine.ParticleSystem/EmitParams
extern "C" void EmitParams_t2216423628_marshal_pinvoke(const EmitParams_t2216423628& unmarshaled, EmitParams_t2216423628_marshaled_pinvoke& marshaled)
{
	marshaled.___m_Particle_0 = unmarshaled.get_m_Particle_0();
	marshaled.___m_PositionSet_1 = static_cast<int32_t>(unmarshaled.get_m_PositionSet_1());
	marshaled.___m_VelocitySet_2 = static_cast<int32_t>(unmarshaled.get_m_VelocitySet_2());
	marshaled.___m_AxisOfRotationSet_3 = static_cast<int32_t>(unmarshaled.get_m_AxisOfRotationSet_3());
	marshaled.___m_RotationSet_4 = static_cast<int32_t>(unmarshaled.get_m_RotationSet_4());
	marshaled.___m_AngularVelocitySet_5 = static_cast<int32_t>(unmarshaled.get_m_AngularVelocitySet_5());
	marshaled.___m_StartSizeSet_6 = static_cast<int32_t>(unmarshaled.get_m_StartSizeSet_6());
	marshaled.___m_StartColorSet_7 = static_cast<int32_t>(unmarshaled.get_m_StartColorSet_7());
	marshaled.___m_RandomSeedSet_8 = static_cast<int32_t>(unmarshaled.get_m_RandomSeedSet_8());
	marshaled.___m_StartLifetimeSet_9 = static_cast<int32_t>(unmarshaled.get_m_StartLifetimeSet_9());
	marshaled.___m_ApplyShapeToPosition_10 = static_cast<int32_t>(unmarshaled.get_m_ApplyShapeToPosition_10());
}
extern "C" void EmitParams_t2216423628_marshal_pinvoke_back(const EmitParams_t2216423628_marshaled_pinvoke& marshaled, EmitParams_t2216423628& unmarshaled)
{
	Particle_t1882894987  unmarshaled_m_Particle_temp_0;
	memset(&unmarshaled_m_Particle_temp_0, 0, sizeof(unmarshaled_m_Particle_temp_0));
	unmarshaled_m_Particle_temp_0 = marshaled.___m_Particle_0;
	unmarshaled.set_m_Particle_0(unmarshaled_m_Particle_temp_0);
	bool unmarshaled_m_PositionSet_temp_1 = false;
	unmarshaled_m_PositionSet_temp_1 = static_cast<bool>(marshaled.___m_PositionSet_1);
	unmarshaled.set_m_PositionSet_1(unmarshaled_m_PositionSet_temp_1);
	bool unmarshaled_m_VelocitySet_temp_2 = false;
	unmarshaled_m_VelocitySet_temp_2 = static_cast<bool>(marshaled.___m_VelocitySet_2);
	unmarshaled.set_m_VelocitySet_2(unmarshaled_m_VelocitySet_temp_2);
	bool unmarshaled_m_AxisOfRotationSet_temp_3 = false;
	unmarshaled_m_AxisOfRotationSet_temp_3 = static_cast<bool>(marshaled.___m_AxisOfRotationSet_3);
	unmarshaled.set_m_AxisOfRotationSet_3(unmarshaled_m_AxisOfRotationSet_temp_3);
	bool unmarshaled_m_RotationSet_temp_4 = false;
	unmarshaled_m_RotationSet_temp_4 = static_cast<bool>(marshaled.___m_RotationSet_4);
	unmarshaled.set_m_RotationSet_4(unmarshaled_m_RotationSet_temp_4);
	bool unmarshaled_m_AngularVelocitySet_temp_5 = false;
	unmarshaled_m_AngularVelocitySet_temp_5 = static_cast<bool>(marshaled.___m_AngularVelocitySet_5);
	unmarshaled.set_m_AngularVelocitySet_5(unmarshaled_m_AngularVelocitySet_temp_5);
	bool unmarshaled_m_StartSizeSet_temp_6 = false;
	unmarshaled_m_StartSizeSet_temp_6 = static_cast<bool>(marshaled.___m_StartSizeSet_6);
	unmarshaled.set_m_StartSizeSet_6(unmarshaled_m_StartSizeSet_temp_6);
	bool unmarshaled_m_StartColorSet_temp_7 = false;
	unmarshaled_m_StartColorSet_temp_7 = static_cast<bool>(marshaled.___m_StartColorSet_7);
	unmarshaled.set_m_StartColorSet_7(unmarshaled_m_StartColorSet_temp_7);
	bool unmarshaled_m_RandomSeedSet_temp_8 = false;
	unmarshaled_m_RandomSeedSet_temp_8 = static_cast<bool>(marshaled.___m_RandomSeedSet_8);
	unmarshaled.set_m_RandomSeedSet_8(unmarshaled_m_RandomSeedSet_temp_8);
	bool unmarshaled_m_StartLifetimeSet_temp_9 = false;
	unmarshaled_m_StartLifetimeSet_temp_9 = static_cast<bool>(marshaled.___m_StartLifetimeSet_9);
	unmarshaled.set_m_StartLifetimeSet_9(unmarshaled_m_StartLifetimeSet_temp_9);
	bool unmarshaled_m_ApplyShapeToPosition_temp_10 = false;
	unmarshaled_m_ApplyShapeToPosition_temp_10 = static_cast<bool>(marshaled.___m_ApplyShapeToPosition_10);
	unmarshaled.set_m_ApplyShapeToPosition_10(unmarshaled_m_ApplyShapeToPosition_temp_10);
}
// Conversion method for clean up from marshalling of: UnityEngine.ParticleSystem/EmitParams
extern "C" void EmitParams_t2216423628_marshal_pinvoke_cleanup(EmitParams_t2216423628_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.ParticleSystem/EmitParams
extern "C" void EmitParams_t2216423628_marshal_com(const EmitParams_t2216423628& unmarshaled, EmitParams_t2216423628_marshaled_com& marshaled)
{
	marshaled.___m_Particle_0 = unmarshaled.get_m_Particle_0();
	marshaled.___m_PositionSet_1 = static_cast<int32_t>(unmarshaled.get_m_PositionSet_1());
	marshaled.___m_VelocitySet_2 = static_cast<int32_t>(unmarshaled.get_m_VelocitySet_2());
	marshaled.___m_AxisOfRotationSet_3 = static_cast<int32_t>(unmarshaled.get_m_AxisOfRotationSet_3());
	marshaled.___m_RotationSet_4 = static_cast<int32_t>(unmarshaled.get_m_RotationSet_4());
	marshaled.___m_AngularVelocitySet_5 = static_cast<int32_t>(unmarshaled.get_m_AngularVelocitySet_5());
	marshaled.___m_StartSizeSet_6 = static_cast<int32_t>(unmarshaled.get_m_StartSizeSet_6());
	marshaled.___m_StartColorSet_7 = static_cast<int32_t>(unmarshaled.get_m_StartColorSet_7());
	marshaled.___m_RandomSeedSet_8 = static_cast<int32_t>(unmarshaled.get_m_RandomSeedSet_8());
	marshaled.___m_StartLifetimeSet_9 = static_cast<int32_t>(unmarshaled.get_m_StartLifetimeSet_9());
	marshaled.___m_ApplyShapeToPosition_10 = static_cast<int32_t>(unmarshaled.get_m_ApplyShapeToPosition_10());
}
extern "C" void EmitParams_t2216423628_marshal_com_back(const EmitParams_t2216423628_marshaled_com& marshaled, EmitParams_t2216423628& unmarshaled)
{
	Particle_t1882894987  unmarshaled_m_Particle_temp_0;
	memset(&unmarshaled_m_Particle_temp_0, 0, sizeof(unmarshaled_m_Particle_temp_0));
	unmarshaled_m_Particle_temp_0 = marshaled.___m_Particle_0;
	unmarshaled.set_m_Particle_0(unmarshaled_m_Particle_temp_0);
	bool unmarshaled_m_PositionSet_temp_1 = false;
	unmarshaled_m_PositionSet_temp_1 = static_cast<bool>(marshaled.___m_PositionSet_1);
	unmarshaled.set_m_PositionSet_1(unmarshaled_m_PositionSet_temp_1);
	bool unmarshaled_m_VelocitySet_temp_2 = false;
	unmarshaled_m_VelocitySet_temp_2 = static_cast<bool>(marshaled.___m_VelocitySet_2);
	unmarshaled.set_m_VelocitySet_2(unmarshaled_m_VelocitySet_temp_2);
	bool unmarshaled_m_AxisOfRotationSet_temp_3 = false;
	unmarshaled_m_AxisOfRotationSet_temp_3 = static_cast<bool>(marshaled.___m_AxisOfRotationSet_3);
	unmarshaled.set_m_AxisOfRotationSet_3(unmarshaled_m_AxisOfRotationSet_temp_3);
	bool unmarshaled_m_RotationSet_temp_4 = false;
	unmarshaled_m_RotationSet_temp_4 = static_cast<bool>(marshaled.___m_RotationSet_4);
	unmarshaled.set_m_RotationSet_4(unmarshaled_m_RotationSet_temp_4);
	bool unmarshaled_m_AngularVelocitySet_temp_5 = false;
	unmarshaled_m_AngularVelocitySet_temp_5 = static_cast<bool>(marshaled.___m_AngularVelocitySet_5);
	unmarshaled.set_m_AngularVelocitySet_5(unmarshaled_m_AngularVelocitySet_temp_5);
	bool unmarshaled_m_StartSizeSet_temp_6 = false;
	unmarshaled_m_StartSizeSet_temp_6 = static_cast<bool>(marshaled.___m_StartSizeSet_6);
	unmarshaled.set_m_StartSizeSet_6(unmarshaled_m_StartSizeSet_temp_6);
	bool unmarshaled_m_StartColorSet_temp_7 = false;
	unmarshaled_m_StartColorSet_temp_7 = static_cast<bool>(marshaled.___m_StartColorSet_7);
	unmarshaled.set_m_StartColorSet_7(unmarshaled_m_StartColorSet_temp_7);
	bool unmarshaled_m_RandomSeedSet_temp_8 = false;
	unmarshaled_m_RandomSeedSet_temp_8 = static_cast<bool>(marshaled.___m_RandomSeedSet_8);
	unmarshaled.set_m_RandomSeedSet_8(unmarshaled_m_RandomSeedSet_temp_8);
	bool unmarshaled_m_StartLifetimeSet_temp_9 = false;
	unmarshaled_m_StartLifetimeSet_temp_9 = static_cast<bool>(marshaled.___m_StartLifetimeSet_9);
	unmarshaled.set_m_StartLifetimeSet_9(unmarshaled_m_StartLifetimeSet_temp_9);
	bool unmarshaled_m_ApplyShapeToPosition_temp_10 = false;
	unmarshaled_m_ApplyShapeToPosition_temp_10 = static_cast<bool>(marshaled.___m_ApplyShapeToPosition_10);
	unmarshaled.set_m_ApplyShapeToPosition_10(unmarshaled_m_ApplyShapeToPosition_temp_10);
}
// Conversion method for clean up from marshalling of: UnityEngine.ParticleSystem/EmitParams
extern "C" void EmitParams_t2216423628_marshal_com_cleanup(EmitParams_t2216423628_marshaled_com& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.ParticleSystem/ExternalForcesModule
extern "C" void ExternalForcesModule_t1424795933_marshal_pinvoke(const ExternalForcesModule_t1424795933& unmarshaled, ExternalForcesModule_t1424795933_marshaled_pinvoke& marshaled)
{
	Il2CppCodeGenException* ___m_ParticleSystem_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_ParticleSystem' of type 'ExternalForcesModule': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_ParticleSystem_0Exception);
}
extern "C" void ExternalForcesModule_t1424795933_marshal_pinvoke_back(const ExternalForcesModule_t1424795933_marshaled_pinvoke& marshaled, ExternalForcesModule_t1424795933& unmarshaled)
{
	Il2CppCodeGenException* ___m_ParticleSystem_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_ParticleSystem' of type 'ExternalForcesModule': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_ParticleSystem_0Exception);
}
// Conversion method for clean up from marshalling of: UnityEngine.ParticleSystem/ExternalForcesModule
extern "C" void ExternalForcesModule_t1424795933_marshal_pinvoke_cleanup(ExternalForcesModule_t1424795933_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.ParticleSystem/ExternalForcesModule
extern "C" void ExternalForcesModule_t1424795933_marshal_com(const ExternalForcesModule_t1424795933& unmarshaled, ExternalForcesModule_t1424795933_marshaled_com& marshaled)
{
	Il2CppCodeGenException* ___m_ParticleSystem_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_ParticleSystem' of type 'ExternalForcesModule': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_ParticleSystem_0Exception);
}
extern "C" void ExternalForcesModule_t1424795933_marshal_com_back(const ExternalForcesModule_t1424795933_marshaled_com& marshaled, ExternalForcesModule_t1424795933& unmarshaled)
{
	Il2CppCodeGenException* ___m_ParticleSystem_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_ParticleSystem' of type 'ExternalForcesModule': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_ParticleSystem_0Exception);
}
// Conversion method for clean up from marshalling of: UnityEngine.ParticleSystem/ExternalForcesModule
extern "C" void ExternalForcesModule_t1424795933_marshal_com_cleanup(ExternalForcesModule_t1424795933_marshaled_com& marshaled)
{
}
// System.Void UnityEngine.ParticleSystem/ExternalForcesModule::.ctor(UnityEngine.ParticleSystem)
extern "C"  void ExternalForcesModule__ctor_m3166001247 (ExternalForcesModule_t1424795933 * __this, ParticleSystem_t1800779281 * ___particleSystem0, const MethodInfo* method)
{
	{
		ParticleSystem_t1800779281 * L_0 = ___particleSystem0;
		__this->set_m_ParticleSystem_0(L_0);
		return;
	}
}
extern "C"  void ExternalForcesModule__ctor_m3166001247_AdjustorThunk (Il2CppObject * __this, ParticleSystem_t1800779281 * ___particleSystem0, const MethodInfo* method)
{
	ExternalForcesModule_t1424795933 * _thisAdjusted = reinterpret_cast<ExternalForcesModule_t1424795933 *>(__this + 1);
	ExternalForcesModule__ctor_m3166001247(_thisAdjusted, ___particleSystem0, method);
}
// Conversion methods for marshalling of: UnityEngine.ParticleSystem/ForceOverLifetimeModule
extern "C" void ForceOverLifetimeModule_t4029962193_marshal_pinvoke(const ForceOverLifetimeModule_t4029962193& unmarshaled, ForceOverLifetimeModule_t4029962193_marshaled_pinvoke& marshaled)
{
	Il2CppCodeGenException* ___m_ParticleSystem_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_ParticleSystem' of type 'ForceOverLifetimeModule': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_ParticleSystem_0Exception);
}
extern "C" void ForceOverLifetimeModule_t4029962193_marshal_pinvoke_back(const ForceOverLifetimeModule_t4029962193_marshaled_pinvoke& marshaled, ForceOverLifetimeModule_t4029962193& unmarshaled)
{
	Il2CppCodeGenException* ___m_ParticleSystem_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_ParticleSystem' of type 'ForceOverLifetimeModule': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_ParticleSystem_0Exception);
}
// Conversion method for clean up from marshalling of: UnityEngine.ParticleSystem/ForceOverLifetimeModule
extern "C" void ForceOverLifetimeModule_t4029962193_marshal_pinvoke_cleanup(ForceOverLifetimeModule_t4029962193_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.ParticleSystem/ForceOverLifetimeModule
extern "C" void ForceOverLifetimeModule_t4029962193_marshal_com(const ForceOverLifetimeModule_t4029962193& unmarshaled, ForceOverLifetimeModule_t4029962193_marshaled_com& marshaled)
{
	Il2CppCodeGenException* ___m_ParticleSystem_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_ParticleSystem' of type 'ForceOverLifetimeModule': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_ParticleSystem_0Exception);
}
extern "C" void ForceOverLifetimeModule_t4029962193_marshal_com_back(const ForceOverLifetimeModule_t4029962193_marshaled_com& marshaled, ForceOverLifetimeModule_t4029962193& unmarshaled)
{
	Il2CppCodeGenException* ___m_ParticleSystem_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_ParticleSystem' of type 'ForceOverLifetimeModule': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_ParticleSystem_0Exception);
}
// Conversion method for clean up from marshalling of: UnityEngine.ParticleSystem/ForceOverLifetimeModule
extern "C" void ForceOverLifetimeModule_t4029962193_marshal_com_cleanup(ForceOverLifetimeModule_t4029962193_marshaled_com& marshaled)
{
}
// System.Void UnityEngine.ParticleSystem/ForceOverLifetimeModule::.ctor(UnityEngine.ParticleSystem)
extern "C"  void ForceOverLifetimeModule__ctor_m3986876230 (ForceOverLifetimeModule_t4029962193 * __this, ParticleSystem_t1800779281 * ___particleSystem0, const MethodInfo* method)
{
	{
		ParticleSystem_t1800779281 * L_0 = ___particleSystem0;
		__this->set_m_ParticleSystem_0(L_0);
		return;
	}
}
extern "C"  void ForceOverLifetimeModule__ctor_m3986876230_AdjustorThunk (Il2CppObject * __this, ParticleSystem_t1800779281 * ___particleSystem0, const MethodInfo* method)
{
	ForceOverLifetimeModule_t4029962193 * _thisAdjusted = reinterpret_cast<ForceOverLifetimeModule_t4029962193 *>(__this + 1);
	ForceOverLifetimeModule__ctor_m3986876230(_thisAdjusted, ___particleSystem0, method);
}
// Conversion methods for marshalling of: UnityEngine.ParticleSystem/InheritVelocityModule
extern "C" void InheritVelocityModule_t3940044026_marshal_pinvoke(const InheritVelocityModule_t3940044026& unmarshaled, InheritVelocityModule_t3940044026_marshaled_pinvoke& marshaled)
{
	Il2CppCodeGenException* ___m_ParticleSystem_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_ParticleSystem' of type 'InheritVelocityModule': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_ParticleSystem_0Exception);
}
extern "C" void InheritVelocityModule_t3940044026_marshal_pinvoke_back(const InheritVelocityModule_t3940044026_marshaled_pinvoke& marshaled, InheritVelocityModule_t3940044026& unmarshaled)
{
	Il2CppCodeGenException* ___m_ParticleSystem_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_ParticleSystem' of type 'InheritVelocityModule': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_ParticleSystem_0Exception);
}
// Conversion method for clean up from marshalling of: UnityEngine.ParticleSystem/InheritVelocityModule
extern "C" void InheritVelocityModule_t3940044026_marshal_pinvoke_cleanup(InheritVelocityModule_t3940044026_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.ParticleSystem/InheritVelocityModule
extern "C" void InheritVelocityModule_t3940044026_marshal_com(const InheritVelocityModule_t3940044026& unmarshaled, InheritVelocityModule_t3940044026_marshaled_com& marshaled)
{
	Il2CppCodeGenException* ___m_ParticleSystem_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_ParticleSystem' of type 'InheritVelocityModule': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_ParticleSystem_0Exception);
}
extern "C" void InheritVelocityModule_t3940044026_marshal_com_back(const InheritVelocityModule_t3940044026_marshaled_com& marshaled, InheritVelocityModule_t3940044026& unmarshaled)
{
	Il2CppCodeGenException* ___m_ParticleSystem_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_ParticleSystem' of type 'InheritVelocityModule': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_ParticleSystem_0Exception);
}
// Conversion method for clean up from marshalling of: UnityEngine.ParticleSystem/InheritVelocityModule
extern "C" void InheritVelocityModule_t3940044026_marshal_com_cleanup(InheritVelocityModule_t3940044026_marshaled_com& marshaled)
{
}
// System.Void UnityEngine.ParticleSystem/InheritVelocityModule::.ctor(UnityEngine.ParticleSystem)
extern "C"  void InheritVelocityModule__ctor_m3717584807 (InheritVelocityModule_t3940044026 * __this, ParticleSystem_t1800779281 * ___particleSystem0, const MethodInfo* method)
{
	{
		ParticleSystem_t1800779281 * L_0 = ___particleSystem0;
		__this->set_m_ParticleSystem_0(L_0);
		return;
	}
}
extern "C"  void InheritVelocityModule__ctor_m3717584807_AdjustorThunk (Il2CppObject * __this, ParticleSystem_t1800779281 * ___particleSystem0, const MethodInfo* method)
{
	InheritVelocityModule_t3940044026 * _thisAdjusted = reinterpret_cast<InheritVelocityModule_t3940044026 *>(__this + 1);
	InheritVelocityModule__ctor_m3717584807(_thisAdjusted, ___particleSystem0, method);
}
// System.Void UnityEngine.ParticleSystem/IteratorDelegate::.ctor(System.Object,System.IntPtr)
extern "C"  void IteratorDelegate__ctor_m4019223695 (IteratorDelegate_t2387635027 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Boolean UnityEngine.ParticleSystem/IteratorDelegate::Invoke(UnityEngine.ParticleSystem)
extern "C"  bool IteratorDelegate_Invoke_m84429716 (IteratorDelegate_t2387635027 * __this, ParticleSystem_t1800779281 * ___ps0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		IteratorDelegate_Invoke_m84429716((IteratorDelegate_t2387635027 *)__this->get_prev_9(),___ps0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef bool (*FunctionPointerType) (Il2CppObject *, void* __this, ParticleSystem_t1800779281 * ___ps0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___ps0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef bool (*FunctionPointerType) (void* __this, ParticleSystem_t1800779281 * ___ps0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___ps0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef bool (*FunctionPointerType) (void* __this, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(___ps0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult UnityEngine.ParticleSystem/IteratorDelegate::BeginInvoke(UnityEngine.ParticleSystem,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * IteratorDelegate_BeginInvoke_m2347885122 (IteratorDelegate_t2387635027 * __this, ParticleSystem_t1800779281 * ___ps0, AsyncCallback_t3962456242 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	void *__d_args[2] = {0};
	__d_args[0] = ___ps0;
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Boolean UnityEngine.ParticleSystem/IteratorDelegate::EndInvoke(System.IAsyncResult)
extern "C"  bool IteratorDelegate_EndInvoke_m4174988220 (IteratorDelegate_t2387635027 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(bool*)UnBox ((Il2CppCodeGenObject*)__result);
}
// Conversion methods for marshalling of: UnityEngine.ParticleSystem/LightsModule
extern "C" void LightsModule_t3616883284_marshal_pinvoke(const LightsModule_t3616883284& unmarshaled, LightsModule_t3616883284_marshaled_pinvoke& marshaled)
{
	Il2CppCodeGenException* ___m_ParticleSystem_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_ParticleSystem' of type 'LightsModule': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_ParticleSystem_0Exception);
}
extern "C" void LightsModule_t3616883284_marshal_pinvoke_back(const LightsModule_t3616883284_marshaled_pinvoke& marshaled, LightsModule_t3616883284& unmarshaled)
{
	Il2CppCodeGenException* ___m_ParticleSystem_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_ParticleSystem' of type 'LightsModule': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_ParticleSystem_0Exception);
}
// Conversion method for clean up from marshalling of: UnityEngine.ParticleSystem/LightsModule
extern "C" void LightsModule_t3616883284_marshal_pinvoke_cleanup(LightsModule_t3616883284_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.ParticleSystem/LightsModule
extern "C" void LightsModule_t3616883284_marshal_com(const LightsModule_t3616883284& unmarshaled, LightsModule_t3616883284_marshaled_com& marshaled)
{
	Il2CppCodeGenException* ___m_ParticleSystem_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_ParticleSystem' of type 'LightsModule': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_ParticleSystem_0Exception);
}
extern "C" void LightsModule_t3616883284_marshal_com_back(const LightsModule_t3616883284_marshaled_com& marshaled, LightsModule_t3616883284& unmarshaled)
{
	Il2CppCodeGenException* ___m_ParticleSystem_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_ParticleSystem' of type 'LightsModule': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_ParticleSystem_0Exception);
}
// Conversion method for clean up from marshalling of: UnityEngine.ParticleSystem/LightsModule
extern "C" void LightsModule_t3616883284_marshal_com_cleanup(LightsModule_t3616883284_marshaled_com& marshaled)
{
}
// System.Void UnityEngine.ParticleSystem/LightsModule::.ctor(UnityEngine.ParticleSystem)
extern "C"  void LightsModule__ctor_m4219401727 (LightsModule_t3616883284 * __this, ParticleSystem_t1800779281 * ___particleSystem0, const MethodInfo* method)
{
	{
		ParticleSystem_t1800779281 * L_0 = ___particleSystem0;
		__this->set_m_ParticleSystem_0(L_0);
		return;
	}
}
extern "C"  void LightsModule__ctor_m4219401727_AdjustorThunk (Il2CppObject * __this, ParticleSystem_t1800779281 * ___particleSystem0, const MethodInfo* method)
{
	LightsModule_t3616883284 * _thisAdjusted = reinterpret_cast<LightsModule_t3616883284 *>(__this + 1);
	LightsModule__ctor_m4219401727(_thisAdjusted, ___particleSystem0, method);
}
// Conversion methods for marshalling of: UnityEngine.ParticleSystem/LimitVelocityOverLifetimeModule
extern "C" void LimitVelocityOverLifetimeModule_t686589569_marshal_pinvoke(const LimitVelocityOverLifetimeModule_t686589569& unmarshaled, LimitVelocityOverLifetimeModule_t686589569_marshaled_pinvoke& marshaled)
{
	Il2CppCodeGenException* ___m_ParticleSystem_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_ParticleSystem' of type 'LimitVelocityOverLifetimeModule': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_ParticleSystem_0Exception);
}
extern "C" void LimitVelocityOverLifetimeModule_t686589569_marshal_pinvoke_back(const LimitVelocityOverLifetimeModule_t686589569_marshaled_pinvoke& marshaled, LimitVelocityOverLifetimeModule_t686589569& unmarshaled)
{
	Il2CppCodeGenException* ___m_ParticleSystem_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_ParticleSystem' of type 'LimitVelocityOverLifetimeModule': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_ParticleSystem_0Exception);
}
// Conversion method for clean up from marshalling of: UnityEngine.ParticleSystem/LimitVelocityOverLifetimeModule
extern "C" void LimitVelocityOverLifetimeModule_t686589569_marshal_pinvoke_cleanup(LimitVelocityOverLifetimeModule_t686589569_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.ParticleSystem/LimitVelocityOverLifetimeModule
extern "C" void LimitVelocityOverLifetimeModule_t686589569_marshal_com(const LimitVelocityOverLifetimeModule_t686589569& unmarshaled, LimitVelocityOverLifetimeModule_t686589569_marshaled_com& marshaled)
{
	Il2CppCodeGenException* ___m_ParticleSystem_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_ParticleSystem' of type 'LimitVelocityOverLifetimeModule': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_ParticleSystem_0Exception);
}
extern "C" void LimitVelocityOverLifetimeModule_t686589569_marshal_com_back(const LimitVelocityOverLifetimeModule_t686589569_marshaled_com& marshaled, LimitVelocityOverLifetimeModule_t686589569& unmarshaled)
{
	Il2CppCodeGenException* ___m_ParticleSystem_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_ParticleSystem' of type 'LimitVelocityOverLifetimeModule': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_ParticleSystem_0Exception);
}
// Conversion method for clean up from marshalling of: UnityEngine.ParticleSystem/LimitVelocityOverLifetimeModule
extern "C" void LimitVelocityOverLifetimeModule_t686589569_marshal_com_cleanup(LimitVelocityOverLifetimeModule_t686589569_marshaled_com& marshaled)
{
}
// System.Void UnityEngine.ParticleSystem/LimitVelocityOverLifetimeModule::.ctor(UnityEngine.ParticleSystem)
extern "C"  void LimitVelocityOverLifetimeModule__ctor_m3158309845 (LimitVelocityOverLifetimeModule_t686589569 * __this, ParticleSystem_t1800779281 * ___particleSystem0, const MethodInfo* method)
{
	{
		ParticleSystem_t1800779281 * L_0 = ___particleSystem0;
		__this->set_m_ParticleSystem_0(L_0);
		return;
	}
}
extern "C"  void LimitVelocityOverLifetimeModule__ctor_m3158309845_AdjustorThunk (Il2CppObject * __this, ParticleSystem_t1800779281 * ___particleSystem0, const MethodInfo* method)
{
	LimitVelocityOverLifetimeModule_t686589569 * _thisAdjusted = reinterpret_cast<LimitVelocityOverLifetimeModule_t686589569 *>(__this + 1);
	LimitVelocityOverLifetimeModule__ctor_m3158309845(_thisAdjusted, ___particleSystem0, method);
}
// Conversion methods for marshalling of: UnityEngine.ParticleSystem/MainModule
extern "C" void MainModule_t2320046318_marshal_pinvoke(const MainModule_t2320046318& unmarshaled, MainModule_t2320046318_marshaled_pinvoke& marshaled)
{
	Il2CppCodeGenException* ___m_ParticleSystem_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_ParticleSystem' of type 'MainModule': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_ParticleSystem_0Exception);
}
extern "C" void MainModule_t2320046318_marshal_pinvoke_back(const MainModule_t2320046318_marshaled_pinvoke& marshaled, MainModule_t2320046318& unmarshaled)
{
	Il2CppCodeGenException* ___m_ParticleSystem_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_ParticleSystem' of type 'MainModule': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_ParticleSystem_0Exception);
}
// Conversion method for clean up from marshalling of: UnityEngine.ParticleSystem/MainModule
extern "C" void MainModule_t2320046318_marshal_pinvoke_cleanup(MainModule_t2320046318_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.ParticleSystem/MainModule
extern "C" void MainModule_t2320046318_marshal_com(const MainModule_t2320046318& unmarshaled, MainModule_t2320046318_marshaled_com& marshaled)
{
	Il2CppCodeGenException* ___m_ParticleSystem_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_ParticleSystem' of type 'MainModule': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_ParticleSystem_0Exception);
}
extern "C" void MainModule_t2320046318_marshal_com_back(const MainModule_t2320046318_marshaled_com& marshaled, MainModule_t2320046318& unmarshaled)
{
	Il2CppCodeGenException* ___m_ParticleSystem_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_ParticleSystem' of type 'MainModule': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_ParticleSystem_0Exception);
}
// Conversion method for clean up from marshalling of: UnityEngine.ParticleSystem/MainModule
extern "C" void MainModule_t2320046318_marshal_com_cleanup(MainModule_t2320046318_marshaled_com& marshaled)
{
}
// System.Void UnityEngine.ParticleSystem/MainModule::.ctor(UnityEngine.ParticleSystem)
extern "C"  void MainModule__ctor_m158988634 (MainModule_t2320046318 * __this, ParticleSystem_t1800779281 * ___particleSystem0, const MethodInfo* method)
{
	{
		ParticleSystem_t1800779281 * L_0 = ___particleSystem0;
		__this->set_m_ParticleSystem_0(L_0);
		return;
	}
}
extern "C"  void MainModule__ctor_m158988634_AdjustorThunk (Il2CppObject * __this, ParticleSystem_t1800779281 * ___particleSystem0, const MethodInfo* method)
{
	MainModule_t2320046318 * _thisAdjusted = reinterpret_cast<MainModule_t2320046318 *>(__this + 1);
	MainModule__ctor_m158988634(_thisAdjusted, ___particleSystem0, method);
}




// Conversion methods for marshalling of: UnityEngine.ParticleSystem/MinMaxCurve
extern "C" void MinMaxCurve_t1067599125_marshal_pinvoke(const MinMaxCurve_t1067599125& unmarshaled, MinMaxCurve_t1067599125_marshaled_pinvoke& marshaled)
{
	marshaled.___m_Mode_0 = unmarshaled.get_m_Mode_0();
	marshaled.___m_CurveMultiplier_1 = unmarshaled.get_m_CurveMultiplier_1();
	if (unmarshaled.get_m_CurveMin_2() != NULL) AnimationCurve_t3046754366_marshal_pinvoke(*unmarshaled.get_m_CurveMin_2(), marshaled.___m_CurveMin_2);
	if (unmarshaled.get_m_CurveMax_3() != NULL) AnimationCurve_t3046754366_marshal_pinvoke(*unmarshaled.get_m_CurveMax_3(), marshaled.___m_CurveMax_3);
	marshaled.___m_ConstantMin_4 = unmarshaled.get_m_ConstantMin_4();
	marshaled.___m_ConstantMax_5 = unmarshaled.get_m_ConstantMax_5();
}
extern "C" void MinMaxCurve_t1067599125_marshal_pinvoke_back(const MinMaxCurve_t1067599125_marshaled_pinvoke& marshaled, MinMaxCurve_t1067599125& unmarshaled)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MinMaxCurve_t1067599125_pinvoke_FromNativeMethodDefinition_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t unmarshaled_m_Mode_temp_0 = 0;
	unmarshaled_m_Mode_temp_0 = marshaled.___m_Mode_0;
	unmarshaled.set_m_Mode_0(unmarshaled_m_Mode_temp_0);
	float unmarshaled_m_CurveMultiplier_temp_1 = 0.0f;
	unmarshaled_m_CurveMultiplier_temp_1 = marshaled.___m_CurveMultiplier_1;
	unmarshaled.set_m_CurveMultiplier_1(unmarshaled_m_CurveMultiplier_temp_1);
	unmarshaled.set_m_CurveMin_2((AnimationCurve_t3046754366 *)il2cpp_codegen_object_new(AnimationCurve_t3046754366_il2cpp_TypeInfo_var));
	AnimationCurve__ctor_m1499663178(unmarshaled.get_m_CurveMin_2(), NULL);
	AnimationCurve_t3046754366_marshal_pinvoke_back(marshaled.___m_CurveMin_2, *unmarshaled.get_m_CurveMin_2());
	unmarshaled.set_m_CurveMax_3((AnimationCurve_t3046754366 *)il2cpp_codegen_object_new(AnimationCurve_t3046754366_il2cpp_TypeInfo_var));
	AnimationCurve__ctor_m1499663178(unmarshaled.get_m_CurveMax_3(), NULL);
	AnimationCurve_t3046754366_marshal_pinvoke_back(marshaled.___m_CurveMax_3, *unmarshaled.get_m_CurveMax_3());
	float unmarshaled_m_ConstantMin_temp_4 = 0.0f;
	unmarshaled_m_ConstantMin_temp_4 = marshaled.___m_ConstantMin_4;
	unmarshaled.set_m_ConstantMin_4(unmarshaled_m_ConstantMin_temp_4);
	float unmarshaled_m_ConstantMax_temp_5 = 0.0f;
	unmarshaled_m_ConstantMax_temp_5 = marshaled.___m_ConstantMax_5;
	unmarshaled.set_m_ConstantMax_5(unmarshaled_m_ConstantMax_temp_5);
}
// Conversion method for clean up from marshalling of: UnityEngine.ParticleSystem/MinMaxCurve
extern "C" void MinMaxCurve_t1067599125_marshal_pinvoke_cleanup(MinMaxCurve_t1067599125_marshaled_pinvoke& marshaled)
{
	AnimationCurve_t3046754366_marshal_pinvoke_cleanup(marshaled.___m_CurveMin_2);
	AnimationCurve_t3046754366_marshal_pinvoke_cleanup(marshaled.___m_CurveMax_3);
}




// Conversion methods for marshalling of: UnityEngine.ParticleSystem/MinMaxCurve
extern "C" void MinMaxCurve_t1067599125_marshal_com(const MinMaxCurve_t1067599125& unmarshaled, MinMaxCurve_t1067599125_marshaled_com& marshaled)
{
	marshaled.___m_Mode_0 = unmarshaled.get_m_Mode_0();
	marshaled.___m_CurveMultiplier_1 = unmarshaled.get_m_CurveMultiplier_1();
	if (unmarshaled.get_m_CurveMin_2() != NULL) AnimationCurve_t3046754366_marshal_com(*unmarshaled.get_m_CurveMin_2(), *marshaled.___m_CurveMin_2);
	if (unmarshaled.get_m_CurveMax_3() != NULL) AnimationCurve_t3046754366_marshal_com(*unmarshaled.get_m_CurveMax_3(), *marshaled.___m_CurveMax_3);
	marshaled.___m_ConstantMin_4 = unmarshaled.get_m_ConstantMin_4();
	marshaled.___m_ConstantMax_5 = unmarshaled.get_m_ConstantMax_5();
}
extern "C" void MinMaxCurve_t1067599125_marshal_com_back(const MinMaxCurve_t1067599125_marshaled_com& marshaled, MinMaxCurve_t1067599125& unmarshaled)
{
	int32_t unmarshaled_m_Mode_temp_0 = 0;
	unmarshaled_m_Mode_temp_0 = marshaled.___m_Mode_0;
	unmarshaled.set_m_Mode_0(unmarshaled_m_Mode_temp_0);
	float unmarshaled_m_CurveMultiplier_temp_1 = 0.0f;
	unmarshaled_m_CurveMultiplier_temp_1 = marshaled.___m_CurveMultiplier_1;
	unmarshaled.set_m_CurveMultiplier_1(unmarshaled_m_CurveMultiplier_temp_1);
	if (unmarshaled.get_m_CurveMin_2() != NULL)
	{
		AnimationCurve__ctor_m1499663178(unmarshaled.get_m_CurveMin_2(), NULL);
		AnimationCurve_t3046754366_marshal_com_back(*marshaled.___m_CurveMin_2, *unmarshaled.get_m_CurveMin_2());
	}
	if (unmarshaled.get_m_CurveMax_3() != NULL)
	{
		AnimationCurve__ctor_m1499663178(unmarshaled.get_m_CurveMax_3(), NULL);
		AnimationCurve_t3046754366_marshal_com_back(*marshaled.___m_CurveMax_3, *unmarshaled.get_m_CurveMax_3());
	}
	float unmarshaled_m_ConstantMin_temp_4 = 0.0f;
	unmarshaled_m_ConstantMin_temp_4 = marshaled.___m_ConstantMin_4;
	unmarshaled.set_m_ConstantMin_4(unmarshaled_m_ConstantMin_temp_4);
	float unmarshaled_m_ConstantMax_temp_5 = 0.0f;
	unmarshaled_m_ConstantMax_temp_5 = marshaled.___m_ConstantMax_5;
	unmarshaled.set_m_ConstantMax_5(unmarshaled_m_ConstantMax_temp_5);
}
// Conversion method for clean up from marshalling of: UnityEngine.ParticleSystem/MinMaxCurve
extern "C" void MinMaxCurve_t1067599125_marshal_com_cleanup(MinMaxCurve_t1067599125_marshaled_com& marshaled)
{
	if (&(*marshaled.___m_CurveMin_2) != NULL) AnimationCurve_t3046754366_marshal_com_cleanup(*marshaled.___m_CurveMin_2);
	if (&(*marshaled.___m_CurveMax_3) != NULL) AnimationCurve_t3046754366_marshal_com_cleanup(*marshaled.___m_CurveMax_3);
}
// System.Void UnityEngine.ParticleSystem/MinMaxCurve::.ctor(System.Single)
extern "C"  void MinMaxCurve__ctor_m2986442616 (MinMaxCurve_t1067599125 * __this, float ___constant0, const MethodInfo* method)
{
	{
		__this->set_m_Mode_0(0);
		__this->set_m_CurveMultiplier_1((0.0f));
		__this->set_m_CurveMin_2((AnimationCurve_t3046754366 *)NULL);
		__this->set_m_CurveMax_3((AnimationCurve_t3046754366 *)NULL);
		__this->set_m_ConstantMin_4((0.0f));
		float L_0 = ___constant0;
		__this->set_m_ConstantMax_5(L_0);
		return;
	}
}
extern "C"  void MinMaxCurve__ctor_m2986442616_AdjustorThunk (Il2CppObject * __this, float ___constant0, const MethodInfo* method)
{
	MinMaxCurve_t1067599125 * _thisAdjusted = reinterpret_cast<MinMaxCurve_t1067599125 *>(__this + 1);
	MinMaxCurve__ctor_m2986442616(_thisAdjusted, ___constant0, method);
}
// UnityEngine.ParticleSystem/MinMaxCurve UnityEngine.ParticleSystem/MinMaxCurve::op_Implicit(System.Single)
extern "C"  MinMaxCurve_t1067599125  MinMaxCurve_op_Implicit_m3696034763 (Il2CppObject * __this /* static, unused */, float ___constant0, const MethodInfo* method)
{
	MinMaxCurve_t1067599125  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		float L_0 = ___constant0;
		MinMaxCurve_t1067599125  L_1;
		memset(&L_1, 0, sizeof(L_1));
		MinMaxCurve__ctor_m2986442616(&L_1, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_000d;
	}

IL_000d:
	{
		MinMaxCurve_t1067599125  L_2 = V_0;
		return L_2;
	}
}
// Conversion methods for marshalling of: UnityEngine.ParticleSystem/NoiseModule
extern "C" void NoiseModule_t962525627_marshal_pinvoke(const NoiseModule_t962525627& unmarshaled, NoiseModule_t962525627_marshaled_pinvoke& marshaled)
{
	Il2CppCodeGenException* ___m_ParticleSystem_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_ParticleSystem' of type 'NoiseModule': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_ParticleSystem_0Exception);
}
extern "C" void NoiseModule_t962525627_marshal_pinvoke_back(const NoiseModule_t962525627_marshaled_pinvoke& marshaled, NoiseModule_t962525627& unmarshaled)
{
	Il2CppCodeGenException* ___m_ParticleSystem_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_ParticleSystem' of type 'NoiseModule': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_ParticleSystem_0Exception);
}
// Conversion method for clean up from marshalling of: UnityEngine.ParticleSystem/NoiseModule
extern "C" void NoiseModule_t962525627_marshal_pinvoke_cleanup(NoiseModule_t962525627_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.ParticleSystem/NoiseModule
extern "C" void NoiseModule_t962525627_marshal_com(const NoiseModule_t962525627& unmarshaled, NoiseModule_t962525627_marshaled_com& marshaled)
{
	Il2CppCodeGenException* ___m_ParticleSystem_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_ParticleSystem' of type 'NoiseModule': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_ParticleSystem_0Exception);
}
extern "C" void NoiseModule_t962525627_marshal_com_back(const NoiseModule_t962525627_marshaled_com& marshaled, NoiseModule_t962525627& unmarshaled)
{
	Il2CppCodeGenException* ___m_ParticleSystem_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_ParticleSystem' of type 'NoiseModule': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_ParticleSystem_0Exception);
}
// Conversion method for clean up from marshalling of: UnityEngine.ParticleSystem/NoiseModule
extern "C" void NoiseModule_t962525627_marshal_com_cleanup(NoiseModule_t962525627_marshaled_com& marshaled)
{
}
// System.Void UnityEngine.ParticleSystem/NoiseModule::.ctor(UnityEngine.ParticleSystem)
extern "C"  void NoiseModule__ctor_m1277709549 (NoiseModule_t962525627 * __this, ParticleSystem_t1800779281 * ___particleSystem0, const MethodInfo* method)
{
	{
		ParticleSystem_t1800779281 * L_0 = ___particleSystem0;
		__this->set_m_ParticleSystem_0(L_0);
		return;
	}
}
extern "C"  void NoiseModule__ctor_m1277709549_AdjustorThunk (Il2CppObject * __this, ParticleSystem_t1800779281 * ___particleSystem0, const MethodInfo* method)
{
	NoiseModule_t962525627 * _thisAdjusted = reinterpret_cast<NoiseModule_t962525627 *>(__this + 1);
	NoiseModule__ctor_m1277709549(_thisAdjusted, ___particleSystem0, method);
}
// System.Void UnityEngine.ParticleSystem/Particle::set_position(UnityEngine.Vector3)
extern "C"  void Particle_set_position_m2092914191 (Particle_t1882894987 * __this, Vector3_t3722313464  ___value0, const MethodInfo* method)
{
	{
		Vector3_t3722313464  L_0 = ___value0;
		__this->set_m_Position_0(L_0);
		return;
	}
}
extern "C"  void Particle_set_position_m2092914191_AdjustorThunk (Il2CppObject * __this, Vector3_t3722313464  ___value0, const MethodInfo* method)
{
	Particle_t1882894987 * _thisAdjusted = reinterpret_cast<Particle_t1882894987 *>(__this + 1);
	Particle_set_position_m2092914191(_thisAdjusted, ___value0, method);
}
// System.Void UnityEngine.ParticleSystem/Particle::set_velocity(UnityEngine.Vector3)
extern "C"  void Particle_set_velocity_m3704745308 (Particle_t1882894987 * __this, Vector3_t3722313464  ___value0, const MethodInfo* method)
{
	{
		Vector3_t3722313464  L_0 = ___value0;
		__this->set_m_Velocity_1(L_0);
		return;
	}
}
extern "C"  void Particle_set_velocity_m3704745308_AdjustorThunk (Il2CppObject * __this, Vector3_t3722313464  ___value0, const MethodInfo* method)
{
	Particle_t1882894987 * _thisAdjusted = reinterpret_cast<Particle_t1882894987 *>(__this + 1);
	Particle_set_velocity_m3704745308(_thisAdjusted, ___value0, method);
}
// System.Void UnityEngine.ParticleSystem/Particle::set_lifetime(System.Single)
extern "C"  void Particle_set_lifetime_m1932789319 (Particle_t1882894987 * __this, float ___value0, const MethodInfo* method)
{
	{
		float L_0 = ___value0;
		__this->set_m_Lifetime_10(L_0);
		return;
	}
}
extern "C"  void Particle_set_lifetime_m1932789319_AdjustorThunk (Il2CppObject * __this, float ___value0, const MethodInfo* method)
{
	Particle_t1882894987 * _thisAdjusted = reinterpret_cast<Particle_t1882894987 *>(__this + 1);
	Particle_set_lifetime_m1932789319(_thisAdjusted, ___value0, method);
}
// System.Void UnityEngine.ParticleSystem/Particle::set_startLifetime(System.Single)
extern "C"  void Particle_set_startLifetime_m2269137591 (Particle_t1882894987 * __this, float ___value0, const MethodInfo* method)
{
	{
		float L_0 = ___value0;
		__this->set_m_StartLifetime_11(L_0);
		return;
	}
}
extern "C"  void Particle_set_startLifetime_m2269137591_AdjustorThunk (Il2CppObject * __this, float ___value0, const MethodInfo* method)
{
	Particle_t1882894987 * _thisAdjusted = reinterpret_cast<Particle_t1882894987 *>(__this + 1);
	Particle_set_startLifetime_m2269137591(_thisAdjusted, ___value0, method);
}
// System.Void UnityEngine.ParticleSystem/Particle::set_startSize(System.Single)
extern "C"  void Particle_set_startSize_m662812576 (Particle_t1882894987 * __this, float ___value0, const MethodInfo* method)
{
	{
		float L_0 = ___value0;
		float L_1 = ___value0;
		float L_2 = ___value0;
		Vector3_t3722313464  L_3;
		memset(&L_3, 0, sizeof(L_3));
		Vector3__ctor_m1197556204(&L_3, L_0, L_1, L_2, /*hidden argument*/NULL);
		__this->set_m_StartSize_7(L_3);
		return;
	}
}
extern "C"  void Particle_set_startSize_m662812576_AdjustorThunk (Il2CppObject * __this, float ___value0, const MethodInfo* method)
{
	Particle_t1882894987 * _thisAdjusted = reinterpret_cast<Particle_t1882894987 *>(__this + 1);
	Particle_set_startSize_m662812576(_thisAdjusted, ___value0, method);
}
// System.Void UnityEngine.ParticleSystem/Particle::set_rotation3D(UnityEngine.Vector3)
extern "C"  void Particle_set_rotation3D_m2052300534 (Particle_t1882894987 * __this, Vector3_t3722313464  ___value0, const MethodInfo* method)
{
	{
		Vector3_t3722313464  L_0 = ___value0;
		Vector3_t3722313464  L_1 = Vector3_op_Multiply_m3506743150(NULL /*static, unused*/, L_0, (0.0174532924f), /*hidden argument*/NULL);
		__this->set_m_Rotation_5(L_1);
		return;
	}
}
extern "C"  void Particle_set_rotation3D_m2052300534_AdjustorThunk (Il2CppObject * __this, Vector3_t3722313464  ___value0, const MethodInfo* method)
{
	Particle_t1882894987 * _thisAdjusted = reinterpret_cast<Particle_t1882894987 *>(__this + 1);
	Particle_set_rotation3D_m2052300534(_thisAdjusted, ___value0, method);
}
// System.Void UnityEngine.ParticleSystem/Particle::set_angularVelocity3D(UnityEngine.Vector3)
extern "C"  void Particle_set_angularVelocity3D_m648168096 (Particle_t1882894987 * __this, Vector3_t3722313464  ___value0, const MethodInfo* method)
{
	{
		Vector3_t3722313464  L_0 = ___value0;
		Vector3_t3722313464  L_1 = Vector3_op_Multiply_m3506743150(NULL /*static, unused*/, L_0, (0.0174532924f), /*hidden argument*/NULL);
		__this->set_m_AngularVelocity_6(L_1);
		return;
	}
}
extern "C"  void Particle_set_angularVelocity3D_m648168096_AdjustorThunk (Il2CppObject * __this, Vector3_t3722313464  ___value0, const MethodInfo* method)
{
	Particle_t1882894987 * _thisAdjusted = reinterpret_cast<Particle_t1882894987 *>(__this + 1);
	Particle_set_angularVelocity3D_m648168096(_thisAdjusted, ___value0, method);
}
// System.Void UnityEngine.ParticleSystem/Particle::set_startColor(UnityEngine.Color32)
extern "C"  void Particle_set_startColor_m3169687975 (Particle_t1882894987 * __this, Color32_t2600501292  ___value0, const MethodInfo* method)
{
	{
		Color32_t2600501292  L_0 = ___value0;
		__this->set_m_StartColor_8(L_0);
		return;
	}
}
extern "C"  void Particle_set_startColor_m3169687975_AdjustorThunk (Il2CppObject * __this, Color32_t2600501292  ___value0, const MethodInfo* method)
{
	Particle_t1882894987 * _thisAdjusted = reinterpret_cast<Particle_t1882894987 *>(__this + 1);
	Particle_set_startColor_m3169687975(_thisAdjusted, ___value0, method);
}
// System.Void UnityEngine.ParticleSystem/Particle::set_randomSeed(System.UInt32)
extern "C"  void Particle_set_randomSeed_m3843991369 (Particle_t1882894987 * __this, uint32_t ___value0, const MethodInfo* method)
{
	{
		uint32_t L_0 = ___value0;
		__this->set_m_RandomSeed_9(L_0);
		return;
	}
}
extern "C"  void Particle_set_randomSeed_m3843991369_AdjustorThunk (Il2CppObject * __this, uint32_t ___value0, const MethodInfo* method)
{
	Particle_t1882894987 * _thisAdjusted = reinterpret_cast<Particle_t1882894987 *>(__this + 1);
	Particle_set_randomSeed_m3843991369(_thisAdjusted, ___value0, method);
}
// Conversion methods for marshalling of: UnityEngine.ParticleSystem/RotationBySpeedModule
extern "C" void RotationBySpeedModule_t3497409583_marshal_pinvoke(const RotationBySpeedModule_t3497409583& unmarshaled, RotationBySpeedModule_t3497409583_marshaled_pinvoke& marshaled)
{
	Il2CppCodeGenException* ___m_ParticleSystem_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_ParticleSystem' of type 'RotationBySpeedModule': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_ParticleSystem_0Exception);
}
extern "C" void RotationBySpeedModule_t3497409583_marshal_pinvoke_back(const RotationBySpeedModule_t3497409583_marshaled_pinvoke& marshaled, RotationBySpeedModule_t3497409583& unmarshaled)
{
	Il2CppCodeGenException* ___m_ParticleSystem_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_ParticleSystem' of type 'RotationBySpeedModule': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_ParticleSystem_0Exception);
}
// Conversion method for clean up from marshalling of: UnityEngine.ParticleSystem/RotationBySpeedModule
extern "C" void RotationBySpeedModule_t3497409583_marshal_pinvoke_cleanup(RotationBySpeedModule_t3497409583_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.ParticleSystem/RotationBySpeedModule
extern "C" void RotationBySpeedModule_t3497409583_marshal_com(const RotationBySpeedModule_t3497409583& unmarshaled, RotationBySpeedModule_t3497409583_marshaled_com& marshaled)
{
	Il2CppCodeGenException* ___m_ParticleSystem_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_ParticleSystem' of type 'RotationBySpeedModule': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_ParticleSystem_0Exception);
}
extern "C" void RotationBySpeedModule_t3497409583_marshal_com_back(const RotationBySpeedModule_t3497409583_marshaled_com& marshaled, RotationBySpeedModule_t3497409583& unmarshaled)
{
	Il2CppCodeGenException* ___m_ParticleSystem_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_ParticleSystem' of type 'RotationBySpeedModule': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_ParticleSystem_0Exception);
}
// Conversion method for clean up from marshalling of: UnityEngine.ParticleSystem/RotationBySpeedModule
extern "C" void RotationBySpeedModule_t3497409583_marshal_com_cleanup(RotationBySpeedModule_t3497409583_marshaled_com& marshaled)
{
}
// System.Void UnityEngine.ParticleSystem/RotationBySpeedModule::.ctor(UnityEngine.ParticleSystem)
extern "C"  void RotationBySpeedModule__ctor_m4096345870 (RotationBySpeedModule_t3497409583 * __this, ParticleSystem_t1800779281 * ___particleSystem0, const MethodInfo* method)
{
	{
		ParticleSystem_t1800779281 * L_0 = ___particleSystem0;
		__this->set_m_ParticleSystem_0(L_0);
		return;
	}
}
extern "C"  void RotationBySpeedModule__ctor_m4096345870_AdjustorThunk (Il2CppObject * __this, ParticleSystem_t1800779281 * ___particleSystem0, const MethodInfo* method)
{
	RotationBySpeedModule_t3497409583 * _thisAdjusted = reinterpret_cast<RotationBySpeedModule_t3497409583 *>(__this + 1);
	RotationBySpeedModule__ctor_m4096345870(_thisAdjusted, ___particleSystem0, method);
}
// Conversion methods for marshalling of: UnityEngine.ParticleSystem/RotationOverLifetimeModule
extern "C" void RotationOverLifetimeModule_t1164372224_marshal_pinvoke(const RotationOverLifetimeModule_t1164372224& unmarshaled, RotationOverLifetimeModule_t1164372224_marshaled_pinvoke& marshaled)
{
	Il2CppCodeGenException* ___m_ParticleSystem_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_ParticleSystem' of type 'RotationOverLifetimeModule': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_ParticleSystem_0Exception);
}
extern "C" void RotationOverLifetimeModule_t1164372224_marshal_pinvoke_back(const RotationOverLifetimeModule_t1164372224_marshaled_pinvoke& marshaled, RotationOverLifetimeModule_t1164372224& unmarshaled)
{
	Il2CppCodeGenException* ___m_ParticleSystem_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_ParticleSystem' of type 'RotationOverLifetimeModule': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_ParticleSystem_0Exception);
}
// Conversion method for clean up from marshalling of: UnityEngine.ParticleSystem/RotationOverLifetimeModule
extern "C" void RotationOverLifetimeModule_t1164372224_marshal_pinvoke_cleanup(RotationOverLifetimeModule_t1164372224_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.ParticleSystem/RotationOverLifetimeModule
extern "C" void RotationOverLifetimeModule_t1164372224_marshal_com(const RotationOverLifetimeModule_t1164372224& unmarshaled, RotationOverLifetimeModule_t1164372224_marshaled_com& marshaled)
{
	Il2CppCodeGenException* ___m_ParticleSystem_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_ParticleSystem' of type 'RotationOverLifetimeModule': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_ParticleSystem_0Exception);
}
extern "C" void RotationOverLifetimeModule_t1164372224_marshal_com_back(const RotationOverLifetimeModule_t1164372224_marshaled_com& marshaled, RotationOverLifetimeModule_t1164372224& unmarshaled)
{
	Il2CppCodeGenException* ___m_ParticleSystem_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_ParticleSystem' of type 'RotationOverLifetimeModule': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_ParticleSystem_0Exception);
}
// Conversion method for clean up from marshalling of: UnityEngine.ParticleSystem/RotationOverLifetimeModule
extern "C" void RotationOverLifetimeModule_t1164372224_marshal_com_cleanup(RotationOverLifetimeModule_t1164372224_marshaled_com& marshaled)
{
}
// System.Void UnityEngine.ParticleSystem/RotationOverLifetimeModule::.ctor(UnityEngine.ParticleSystem)
extern "C"  void RotationOverLifetimeModule__ctor_m1430131584 (RotationOverLifetimeModule_t1164372224 * __this, ParticleSystem_t1800779281 * ___particleSystem0, const MethodInfo* method)
{
	{
		ParticleSystem_t1800779281 * L_0 = ___particleSystem0;
		__this->set_m_ParticleSystem_0(L_0);
		return;
	}
}
extern "C"  void RotationOverLifetimeModule__ctor_m1430131584_AdjustorThunk (Il2CppObject * __this, ParticleSystem_t1800779281 * ___particleSystem0, const MethodInfo* method)
{
	RotationOverLifetimeModule_t1164372224 * _thisAdjusted = reinterpret_cast<RotationOverLifetimeModule_t1164372224 *>(__this + 1);
	RotationOverLifetimeModule__ctor_m1430131584(_thisAdjusted, ___particleSystem0, method);
}
// Conversion methods for marshalling of: UnityEngine.ParticleSystem/ShapeModule
extern "C" void ShapeModule_t3608330829_marshal_pinvoke(const ShapeModule_t3608330829& unmarshaled, ShapeModule_t3608330829_marshaled_pinvoke& marshaled)
{
	Il2CppCodeGenException* ___m_ParticleSystem_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_ParticleSystem' of type 'ShapeModule': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_ParticleSystem_0Exception);
}
extern "C" void ShapeModule_t3608330829_marshal_pinvoke_back(const ShapeModule_t3608330829_marshaled_pinvoke& marshaled, ShapeModule_t3608330829& unmarshaled)
{
	Il2CppCodeGenException* ___m_ParticleSystem_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_ParticleSystem' of type 'ShapeModule': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_ParticleSystem_0Exception);
}
// Conversion method for clean up from marshalling of: UnityEngine.ParticleSystem/ShapeModule
extern "C" void ShapeModule_t3608330829_marshal_pinvoke_cleanup(ShapeModule_t3608330829_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.ParticleSystem/ShapeModule
extern "C" void ShapeModule_t3608330829_marshal_com(const ShapeModule_t3608330829& unmarshaled, ShapeModule_t3608330829_marshaled_com& marshaled)
{
	Il2CppCodeGenException* ___m_ParticleSystem_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_ParticleSystem' of type 'ShapeModule': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_ParticleSystem_0Exception);
}
extern "C" void ShapeModule_t3608330829_marshal_com_back(const ShapeModule_t3608330829_marshaled_com& marshaled, ShapeModule_t3608330829& unmarshaled)
{
	Il2CppCodeGenException* ___m_ParticleSystem_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_ParticleSystem' of type 'ShapeModule': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_ParticleSystem_0Exception);
}
// Conversion method for clean up from marshalling of: UnityEngine.ParticleSystem/ShapeModule
extern "C" void ShapeModule_t3608330829_marshal_com_cleanup(ShapeModule_t3608330829_marshaled_com& marshaled)
{
}
// System.Void UnityEngine.ParticleSystem/ShapeModule::.ctor(UnityEngine.ParticleSystem)
extern "C"  void ShapeModule__ctor_m2339294796 (ShapeModule_t3608330829 * __this, ParticleSystem_t1800779281 * ___particleSystem0, const MethodInfo* method)
{
	{
		ParticleSystem_t1800779281 * L_0 = ___particleSystem0;
		__this->set_m_ParticleSystem_0(L_0);
		return;
	}
}
extern "C"  void ShapeModule__ctor_m2339294796_AdjustorThunk (Il2CppObject * __this, ParticleSystem_t1800779281 * ___particleSystem0, const MethodInfo* method)
{
	ShapeModule_t3608330829 * _thisAdjusted = reinterpret_cast<ShapeModule_t3608330829 *>(__this + 1);
	ShapeModule__ctor_m2339294796(_thisAdjusted, ___particleSystem0, method);
}
// Conversion methods for marshalling of: UnityEngine.ParticleSystem/SizeBySpeedModule
extern "C" void SizeBySpeedModule_t1515126846_marshal_pinvoke(const SizeBySpeedModule_t1515126846& unmarshaled, SizeBySpeedModule_t1515126846_marshaled_pinvoke& marshaled)
{
	Il2CppCodeGenException* ___m_ParticleSystem_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_ParticleSystem' of type 'SizeBySpeedModule': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_ParticleSystem_0Exception);
}
extern "C" void SizeBySpeedModule_t1515126846_marshal_pinvoke_back(const SizeBySpeedModule_t1515126846_marshaled_pinvoke& marshaled, SizeBySpeedModule_t1515126846& unmarshaled)
{
	Il2CppCodeGenException* ___m_ParticleSystem_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_ParticleSystem' of type 'SizeBySpeedModule': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_ParticleSystem_0Exception);
}
// Conversion method for clean up from marshalling of: UnityEngine.ParticleSystem/SizeBySpeedModule
extern "C" void SizeBySpeedModule_t1515126846_marshal_pinvoke_cleanup(SizeBySpeedModule_t1515126846_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.ParticleSystem/SizeBySpeedModule
extern "C" void SizeBySpeedModule_t1515126846_marshal_com(const SizeBySpeedModule_t1515126846& unmarshaled, SizeBySpeedModule_t1515126846_marshaled_com& marshaled)
{
	Il2CppCodeGenException* ___m_ParticleSystem_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_ParticleSystem' of type 'SizeBySpeedModule': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_ParticleSystem_0Exception);
}
extern "C" void SizeBySpeedModule_t1515126846_marshal_com_back(const SizeBySpeedModule_t1515126846_marshaled_com& marshaled, SizeBySpeedModule_t1515126846& unmarshaled)
{
	Il2CppCodeGenException* ___m_ParticleSystem_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_ParticleSystem' of type 'SizeBySpeedModule': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_ParticleSystem_0Exception);
}
// Conversion method for clean up from marshalling of: UnityEngine.ParticleSystem/SizeBySpeedModule
extern "C" void SizeBySpeedModule_t1515126846_marshal_com_cleanup(SizeBySpeedModule_t1515126846_marshaled_com& marshaled)
{
}
// System.Void UnityEngine.ParticleSystem/SizeBySpeedModule::.ctor(UnityEngine.ParticleSystem)
extern "C"  void SizeBySpeedModule__ctor_m3934889008 (SizeBySpeedModule_t1515126846 * __this, ParticleSystem_t1800779281 * ___particleSystem0, const MethodInfo* method)
{
	{
		ParticleSystem_t1800779281 * L_0 = ___particleSystem0;
		__this->set_m_ParticleSystem_0(L_0);
		return;
	}
}
extern "C"  void SizeBySpeedModule__ctor_m3934889008_AdjustorThunk (Il2CppObject * __this, ParticleSystem_t1800779281 * ___particleSystem0, const MethodInfo* method)
{
	SizeBySpeedModule_t1515126846 * _thisAdjusted = reinterpret_cast<SizeBySpeedModule_t1515126846 *>(__this + 1);
	SizeBySpeedModule__ctor_m3934889008(_thisAdjusted, ___particleSystem0, method);
}
// Conversion methods for marshalling of: UnityEngine.ParticleSystem/SizeOverLifetimeModule
extern "C" void SizeOverLifetimeModule_t1101123803_marshal_pinvoke(const SizeOverLifetimeModule_t1101123803& unmarshaled, SizeOverLifetimeModule_t1101123803_marshaled_pinvoke& marshaled)
{
	Il2CppCodeGenException* ___m_ParticleSystem_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_ParticleSystem' of type 'SizeOverLifetimeModule': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_ParticleSystem_0Exception);
}
extern "C" void SizeOverLifetimeModule_t1101123803_marshal_pinvoke_back(const SizeOverLifetimeModule_t1101123803_marshaled_pinvoke& marshaled, SizeOverLifetimeModule_t1101123803& unmarshaled)
{
	Il2CppCodeGenException* ___m_ParticleSystem_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_ParticleSystem' of type 'SizeOverLifetimeModule': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_ParticleSystem_0Exception);
}
// Conversion method for clean up from marshalling of: UnityEngine.ParticleSystem/SizeOverLifetimeModule
extern "C" void SizeOverLifetimeModule_t1101123803_marshal_pinvoke_cleanup(SizeOverLifetimeModule_t1101123803_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.ParticleSystem/SizeOverLifetimeModule
extern "C" void SizeOverLifetimeModule_t1101123803_marshal_com(const SizeOverLifetimeModule_t1101123803& unmarshaled, SizeOverLifetimeModule_t1101123803_marshaled_com& marshaled)
{
	Il2CppCodeGenException* ___m_ParticleSystem_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_ParticleSystem' of type 'SizeOverLifetimeModule': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_ParticleSystem_0Exception);
}
extern "C" void SizeOverLifetimeModule_t1101123803_marshal_com_back(const SizeOverLifetimeModule_t1101123803_marshaled_com& marshaled, SizeOverLifetimeModule_t1101123803& unmarshaled)
{
	Il2CppCodeGenException* ___m_ParticleSystem_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_ParticleSystem' of type 'SizeOverLifetimeModule': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_ParticleSystem_0Exception);
}
// Conversion method for clean up from marshalling of: UnityEngine.ParticleSystem/SizeOverLifetimeModule
extern "C" void SizeOverLifetimeModule_t1101123803_marshal_com_cleanup(SizeOverLifetimeModule_t1101123803_marshaled_com& marshaled)
{
}
// System.Void UnityEngine.ParticleSystem/SizeOverLifetimeModule::.ctor(UnityEngine.ParticleSystem)
extern "C"  void SizeOverLifetimeModule__ctor_m3730960903 (SizeOverLifetimeModule_t1101123803 * __this, ParticleSystem_t1800779281 * ___particleSystem0, const MethodInfo* method)
{
	{
		ParticleSystem_t1800779281 * L_0 = ___particleSystem0;
		__this->set_m_ParticleSystem_0(L_0);
		return;
	}
}
extern "C"  void SizeOverLifetimeModule__ctor_m3730960903_AdjustorThunk (Il2CppObject * __this, ParticleSystem_t1800779281 * ___particleSystem0, const MethodInfo* method)
{
	SizeOverLifetimeModule_t1101123803 * _thisAdjusted = reinterpret_cast<SizeOverLifetimeModule_t1101123803 *>(__this + 1);
	SizeOverLifetimeModule__ctor_m3730960903(_thisAdjusted, ___particleSystem0, method);
}
// Conversion methods for marshalling of: UnityEngine.ParticleSystem/SubEmittersModule
extern "C" void SubEmittersModule_t903775760_marshal_pinvoke(const SubEmittersModule_t903775760& unmarshaled, SubEmittersModule_t903775760_marshaled_pinvoke& marshaled)
{
	Il2CppCodeGenException* ___m_ParticleSystem_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_ParticleSystem' of type 'SubEmittersModule': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_ParticleSystem_0Exception);
}
extern "C" void SubEmittersModule_t903775760_marshal_pinvoke_back(const SubEmittersModule_t903775760_marshaled_pinvoke& marshaled, SubEmittersModule_t903775760& unmarshaled)
{
	Il2CppCodeGenException* ___m_ParticleSystem_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_ParticleSystem' of type 'SubEmittersModule': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_ParticleSystem_0Exception);
}
// Conversion method for clean up from marshalling of: UnityEngine.ParticleSystem/SubEmittersModule
extern "C" void SubEmittersModule_t903775760_marshal_pinvoke_cleanup(SubEmittersModule_t903775760_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.ParticleSystem/SubEmittersModule
extern "C" void SubEmittersModule_t903775760_marshal_com(const SubEmittersModule_t903775760& unmarshaled, SubEmittersModule_t903775760_marshaled_com& marshaled)
{
	Il2CppCodeGenException* ___m_ParticleSystem_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_ParticleSystem' of type 'SubEmittersModule': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_ParticleSystem_0Exception);
}
extern "C" void SubEmittersModule_t903775760_marshal_com_back(const SubEmittersModule_t903775760_marshaled_com& marshaled, SubEmittersModule_t903775760& unmarshaled)
{
	Il2CppCodeGenException* ___m_ParticleSystem_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_ParticleSystem' of type 'SubEmittersModule': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_ParticleSystem_0Exception);
}
// Conversion method for clean up from marshalling of: UnityEngine.ParticleSystem/SubEmittersModule
extern "C" void SubEmittersModule_t903775760_marshal_com_cleanup(SubEmittersModule_t903775760_marshaled_com& marshaled)
{
}
// System.Void UnityEngine.ParticleSystem/SubEmittersModule::.ctor(UnityEngine.ParticleSystem)
extern "C"  void SubEmittersModule__ctor_m4074391005 (SubEmittersModule_t903775760 * __this, ParticleSystem_t1800779281 * ___particleSystem0, const MethodInfo* method)
{
	{
		ParticleSystem_t1800779281 * L_0 = ___particleSystem0;
		__this->set_m_ParticleSystem_0(L_0);
		return;
	}
}
extern "C"  void SubEmittersModule__ctor_m4074391005_AdjustorThunk (Il2CppObject * __this, ParticleSystem_t1800779281 * ___particleSystem0, const MethodInfo* method)
{
	SubEmittersModule_t903775760 * _thisAdjusted = reinterpret_cast<SubEmittersModule_t903775760 *>(__this + 1);
	SubEmittersModule__ctor_m4074391005(_thisAdjusted, ___particleSystem0, method);
}
// Conversion methods for marshalling of: UnityEngine.ParticleSystem/TextureSheetAnimationModule
extern "C" void TextureSheetAnimationModule_t738696839_marshal_pinvoke(const TextureSheetAnimationModule_t738696839& unmarshaled, TextureSheetAnimationModule_t738696839_marshaled_pinvoke& marshaled)
{
	Il2CppCodeGenException* ___m_ParticleSystem_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_ParticleSystem' of type 'TextureSheetAnimationModule': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_ParticleSystem_0Exception);
}
extern "C" void TextureSheetAnimationModule_t738696839_marshal_pinvoke_back(const TextureSheetAnimationModule_t738696839_marshaled_pinvoke& marshaled, TextureSheetAnimationModule_t738696839& unmarshaled)
{
	Il2CppCodeGenException* ___m_ParticleSystem_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_ParticleSystem' of type 'TextureSheetAnimationModule': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_ParticleSystem_0Exception);
}
// Conversion method for clean up from marshalling of: UnityEngine.ParticleSystem/TextureSheetAnimationModule
extern "C" void TextureSheetAnimationModule_t738696839_marshal_pinvoke_cleanup(TextureSheetAnimationModule_t738696839_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.ParticleSystem/TextureSheetAnimationModule
extern "C" void TextureSheetAnimationModule_t738696839_marshal_com(const TextureSheetAnimationModule_t738696839& unmarshaled, TextureSheetAnimationModule_t738696839_marshaled_com& marshaled)
{
	Il2CppCodeGenException* ___m_ParticleSystem_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_ParticleSystem' of type 'TextureSheetAnimationModule': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_ParticleSystem_0Exception);
}
extern "C" void TextureSheetAnimationModule_t738696839_marshal_com_back(const TextureSheetAnimationModule_t738696839_marshaled_com& marshaled, TextureSheetAnimationModule_t738696839& unmarshaled)
{
	Il2CppCodeGenException* ___m_ParticleSystem_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_ParticleSystem' of type 'TextureSheetAnimationModule': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_ParticleSystem_0Exception);
}
// Conversion method for clean up from marshalling of: UnityEngine.ParticleSystem/TextureSheetAnimationModule
extern "C" void TextureSheetAnimationModule_t738696839_marshal_com_cleanup(TextureSheetAnimationModule_t738696839_marshaled_com& marshaled)
{
}
// System.Void UnityEngine.ParticleSystem/TextureSheetAnimationModule::.ctor(UnityEngine.ParticleSystem)
extern "C"  void TextureSheetAnimationModule__ctor_m1467095871 (TextureSheetAnimationModule_t738696839 * __this, ParticleSystem_t1800779281 * ___particleSystem0, const MethodInfo* method)
{
	{
		ParticleSystem_t1800779281 * L_0 = ___particleSystem0;
		__this->set_m_ParticleSystem_0(L_0);
		return;
	}
}
extern "C"  void TextureSheetAnimationModule__ctor_m1467095871_AdjustorThunk (Il2CppObject * __this, ParticleSystem_t1800779281 * ___particleSystem0, const MethodInfo* method)
{
	TextureSheetAnimationModule_t738696839 * _thisAdjusted = reinterpret_cast<TextureSheetAnimationModule_t738696839 *>(__this + 1);
	TextureSheetAnimationModule__ctor_m1467095871(_thisAdjusted, ___particleSystem0, method);
}
// Conversion methods for marshalling of: UnityEngine.ParticleSystem/TrailModule
extern "C" void TrailModule_t2282589118_marshal_pinvoke(const TrailModule_t2282589118& unmarshaled, TrailModule_t2282589118_marshaled_pinvoke& marshaled)
{
	Il2CppCodeGenException* ___m_ParticleSystem_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_ParticleSystem' of type 'TrailModule': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_ParticleSystem_0Exception);
}
extern "C" void TrailModule_t2282589118_marshal_pinvoke_back(const TrailModule_t2282589118_marshaled_pinvoke& marshaled, TrailModule_t2282589118& unmarshaled)
{
	Il2CppCodeGenException* ___m_ParticleSystem_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_ParticleSystem' of type 'TrailModule': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_ParticleSystem_0Exception);
}
// Conversion method for clean up from marshalling of: UnityEngine.ParticleSystem/TrailModule
extern "C" void TrailModule_t2282589118_marshal_pinvoke_cleanup(TrailModule_t2282589118_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.ParticleSystem/TrailModule
extern "C" void TrailModule_t2282589118_marshal_com(const TrailModule_t2282589118& unmarshaled, TrailModule_t2282589118_marshaled_com& marshaled)
{
	Il2CppCodeGenException* ___m_ParticleSystem_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_ParticleSystem' of type 'TrailModule': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_ParticleSystem_0Exception);
}
extern "C" void TrailModule_t2282589118_marshal_com_back(const TrailModule_t2282589118_marshaled_com& marshaled, TrailModule_t2282589118& unmarshaled)
{
	Il2CppCodeGenException* ___m_ParticleSystem_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_ParticleSystem' of type 'TrailModule': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_ParticleSystem_0Exception);
}
// Conversion method for clean up from marshalling of: UnityEngine.ParticleSystem/TrailModule
extern "C" void TrailModule_t2282589118_marshal_com_cleanup(TrailModule_t2282589118_marshaled_com& marshaled)
{
}
// System.Void UnityEngine.ParticleSystem/TrailModule::.ctor(UnityEngine.ParticleSystem)
extern "C"  void TrailModule__ctor_m1664590038 (TrailModule_t2282589118 * __this, ParticleSystem_t1800779281 * ___particleSystem0, const MethodInfo* method)
{
	{
		ParticleSystem_t1800779281 * L_0 = ___particleSystem0;
		__this->set_m_ParticleSystem_0(L_0);
		return;
	}
}
extern "C"  void TrailModule__ctor_m1664590038_AdjustorThunk (Il2CppObject * __this, ParticleSystem_t1800779281 * ___particleSystem0, const MethodInfo* method)
{
	TrailModule_t2282589118 * _thisAdjusted = reinterpret_cast<TrailModule_t2282589118 *>(__this + 1);
	TrailModule__ctor_m1664590038(_thisAdjusted, ___particleSystem0, method);
}
// Conversion methods for marshalling of: UnityEngine.ParticleSystem/TriggerModule
extern "C" void TriggerModule_t1157986180_marshal_pinvoke(const TriggerModule_t1157986180& unmarshaled, TriggerModule_t1157986180_marshaled_pinvoke& marshaled)
{
	Il2CppCodeGenException* ___m_ParticleSystem_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_ParticleSystem' of type 'TriggerModule': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_ParticleSystem_0Exception);
}
extern "C" void TriggerModule_t1157986180_marshal_pinvoke_back(const TriggerModule_t1157986180_marshaled_pinvoke& marshaled, TriggerModule_t1157986180& unmarshaled)
{
	Il2CppCodeGenException* ___m_ParticleSystem_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_ParticleSystem' of type 'TriggerModule': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_ParticleSystem_0Exception);
}
// Conversion method for clean up from marshalling of: UnityEngine.ParticleSystem/TriggerModule
extern "C" void TriggerModule_t1157986180_marshal_pinvoke_cleanup(TriggerModule_t1157986180_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.ParticleSystem/TriggerModule
extern "C" void TriggerModule_t1157986180_marshal_com(const TriggerModule_t1157986180& unmarshaled, TriggerModule_t1157986180_marshaled_com& marshaled)
{
	Il2CppCodeGenException* ___m_ParticleSystem_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_ParticleSystem' of type 'TriggerModule': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_ParticleSystem_0Exception);
}
extern "C" void TriggerModule_t1157986180_marshal_com_back(const TriggerModule_t1157986180_marshaled_com& marshaled, TriggerModule_t1157986180& unmarshaled)
{
	Il2CppCodeGenException* ___m_ParticleSystem_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_ParticleSystem' of type 'TriggerModule': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_ParticleSystem_0Exception);
}
// Conversion method for clean up from marshalling of: UnityEngine.ParticleSystem/TriggerModule
extern "C" void TriggerModule_t1157986180_marshal_com_cleanup(TriggerModule_t1157986180_marshaled_com& marshaled)
{
}
// System.Void UnityEngine.ParticleSystem/TriggerModule::.ctor(UnityEngine.ParticleSystem)
extern "C"  void TriggerModule__ctor_m1363700541 (TriggerModule_t1157986180 * __this, ParticleSystem_t1800779281 * ___particleSystem0, const MethodInfo* method)
{
	{
		ParticleSystem_t1800779281 * L_0 = ___particleSystem0;
		__this->set_m_ParticleSystem_0(L_0);
		return;
	}
}
extern "C"  void TriggerModule__ctor_m1363700541_AdjustorThunk (Il2CppObject * __this, ParticleSystem_t1800779281 * ___particleSystem0, const MethodInfo* method)
{
	TriggerModule_t1157986180 * _thisAdjusted = reinterpret_cast<TriggerModule_t1157986180 *>(__this + 1);
	TriggerModule__ctor_m1363700541(_thisAdjusted, ___particleSystem0, method);
}
// Conversion methods for marshalling of: UnityEngine.ParticleSystem/VelocityOverLifetimeModule
extern "C" void VelocityOverLifetimeModule_t1982232382_marshal_pinvoke(const VelocityOverLifetimeModule_t1982232382& unmarshaled, VelocityOverLifetimeModule_t1982232382_marshaled_pinvoke& marshaled)
{
	Il2CppCodeGenException* ___m_ParticleSystem_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_ParticleSystem' of type 'VelocityOverLifetimeModule': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_ParticleSystem_0Exception);
}
extern "C" void VelocityOverLifetimeModule_t1982232382_marshal_pinvoke_back(const VelocityOverLifetimeModule_t1982232382_marshaled_pinvoke& marshaled, VelocityOverLifetimeModule_t1982232382& unmarshaled)
{
	Il2CppCodeGenException* ___m_ParticleSystem_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_ParticleSystem' of type 'VelocityOverLifetimeModule': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_ParticleSystem_0Exception);
}
// Conversion method for clean up from marshalling of: UnityEngine.ParticleSystem/VelocityOverLifetimeModule
extern "C" void VelocityOverLifetimeModule_t1982232382_marshal_pinvoke_cleanup(VelocityOverLifetimeModule_t1982232382_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.ParticleSystem/VelocityOverLifetimeModule
extern "C" void VelocityOverLifetimeModule_t1982232382_marshal_com(const VelocityOverLifetimeModule_t1982232382& unmarshaled, VelocityOverLifetimeModule_t1982232382_marshaled_com& marshaled)
{
	Il2CppCodeGenException* ___m_ParticleSystem_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_ParticleSystem' of type 'VelocityOverLifetimeModule': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_ParticleSystem_0Exception);
}
extern "C" void VelocityOverLifetimeModule_t1982232382_marshal_com_back(const VelocityOverLifetimeModule_t1982232382_marshaled_com& marshaled, VelocityOverLifetimeModule_t1982232382& unmarshaled)
{
	Il2CppCodeGenException* ___m_ParticleSystem_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_ParticleSystem' of type 'VelocityOverLifetimeModule': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_ParticleSystem_0Exception);
}
// Conversion method for clean up from marshalling of: UnityEngine.ParticleSystem/VelocityOverLifetimeModule
extern "C" void VelocityOverLifetimeModule_t1982232382_marshal_com_cleanup(VelocityOverLifetimeModule_t1982232382_marshaled_com& marshaled)
{
}
// System.Void UnityEngine.ParticleSystem/VelocityOverLifetimeModule::.ctor(UnityEngine.ParticleSystem)
extern "C"  void VelocityOverLifetimeModule__ctor_m3772502470 (VelocityOverLifetimeModule_t1982232382 * __this, ParticleSystem_t1800779281 * ___particleSystem0, const MethodInfo* method)
{
	{
		ParticleSystem_t1800779281 * L_0 = ___particleSystem0;
		__this->set_m_ParticleSystem_0(L_0);
		return;
	}
}
extern "C"  void VelocityOverLifetimeModule__ctor_m3772502470_AdjustorThunk (Il2CppObject * __this, ParticleSystem_t1800779281 * ___particleSystem0, const MethodInfo* method)
{
	VelocityOverLifetimeModule_t1982232382 * _thisAdjusted = reinterpret_cast<VelocityOverLifetimeModule_t1982232382 *>(__this + 1);
	VelocityOverLifetimeModule__ctor_m3772502470(_thisAdjusted, ___particleSystem0, method);
}
// System.Boolean UnityEngine.Physics::Raycast(UnityEngine.Vector3,UnityEngine.Vector3,System.Single,System.Int32)
extern "C"  bool Physics_Raycast_m1337615513 (Il2CppObject * __this /* static, unused */, Vector3_t3722313464  ___origin0, Vector3_t3722313464  ___direction1, float ___maxDistance2, int32_t ___layerMask3, const MethodInfo* method)
{
	int32_t V_0 = 0;
	bool V_1 = false;
	{
		V_0 = 0;
		Vector3_t3722313464  L_0 = ___origin0;
		Vector3_t3722313464  L_1 = ___direction1;
		float L_2 = ___maxDistance2;
		int32_t L_3 = ___layerMask3;
		int32_t L_4 = V_0;
		bool L_5 = Physics_Raycast_m325282035(NULL /*static, unused*/, L_0, L_1, L_2, L_3, L_4, /*hidden argument*/NULL);
		V_1 = L_5;
		goto IL_0013;
	}

IL_0013:
	{
		bool L_6 = V_1;
		return L_6;
	}
}
// System.Boolean UnityEngine.Physics::Raycast(UnityEngine.Vector3,UnityEngine.Vector3,System.Single)
extern "C"  bool Physics_Raycast_m1939787828 (Il2CppObject * __this /* static, unused */, Vector3_t3722313464  ___origin0, Vector3_t3722313464  ___direction1, float ___maxDistance2, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	bool V_2 = false;
	{
		V_0 = 0;
		V_1 = ((int32_t)-5);
		Vector3_t3722313464  L_0 = ___origin0;
		Vector3_t3722313464  L_1 = ___direction1;
		float L_2 = ___maxDistance2;
		int32_t L_3 = V_1;
		int32_t L_4 = V_0;
		bool L_5 = Physics_Raycast_m325282035(NULL /*static, unused*/, L_0, L_1, L_2, L_3, L_4, /*hidden argument*/NULL);
		V_2 = L_5;
		goto IL_0016;
	}

IL_0016:
	{
		bool L_6 = V_2;
		return L_6;
	}
}
// System.Boolean UnityEngine.Physics::Raycast(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  bool Physics_Raycast_m2941825911 (Il2CppObject * __this /* static, unused */, Vector3_t3722313464  ___origin0, Vector3_t3722313464  ___direction1, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	float V_2 = 0.0f;
	bool V_3 = false;
	{
		V_0 = 0;
		V_1 = ((int32_t)-5);
		V_2 = (std::numeric_limits<float>::infinity());
		Vector3_t3722313464  L_0 = ___origin0;
		Vector3_t3722313464  L_1 = ___direction1;
		float L_2 = V_2;
		int32_t L_3 = V_1;
		int32_t L_4 = V_0;
		bool L_5 = Physics_Raycast_m325282035(NULL /*static, unused*/, L_0, L_1, L_2, L_3, L_4, /*hidden argument*/NULL);
		V_3 = L_5;
		goto IL_001c;
	}

IL_001c:
	{
		bool L_6 = V_3;
		return L_6;
	}
}
// System.Boolean UnityEngine.Physics::Raycast(UnityEngine.Vector3,UnityEngine.Vector3,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern "C"  bool Physics_Raycast_m325282035 (Il2CppObject * __this /* static, unused */, Vector3_t3722313464  ___origin0, Vector3_t3722313464  ___direction1, float ___maxDistance2, int32_t ___layerMask3, int32_t ___queryTriggerInteraction4, const MethodInfo* method)
{
	bool V_0 = false;
	{
		Vector3_t3722313464  L_0 = ___origin0;
		Vector3_t3722313464  L_1 = ___direction1;
		float L_2 = ___maxDistance2;
		int32_t L_3 = ___layerMask3;
		int32_t L_4 = ___queryTriggerInteraction4;
		bool L_5 = Physics_Internal_RaycastTest_m2320488002(NULL /*static, unused*/, L_0, L_1, L_2, L_3, L_4, /*hidden argument*/NULL);
		V_0 = L_5;
		goto IL_0012;
	}

IL_0012:
	{
		bool L_6 = V_0;
		return L_6;
	}
}
// System.Boolean UnityEngine.Physics::Raycast(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.RaycastHit&,System.Single,System.Int32)
extern "C"  bool Physics_Raycast_m313226229 (Il2CppObject * __this /* static, unused */, Vector3_t3722313464  ___origin0, Vector3_t3722313464  ___direction1, RaycastHit_t1056001966 * ___hitInfo2, float ___maxDistance3, int32_t ___layerMask4, const MethodInfo* method)
{
	int32_t V_0 = 0;
	bool V_1 = false;
	{
		V_0 = 0;
		Vector3_t3722313464  L_0 = ___origin0;
		Vector3_t3722313464  L_1 = ___direction1;
		RaycastHit_t1056001966 * L_2 = ___hitInfo2;
		float L_3 = ___maxDistance3;
		int32_t L_4 = ___layerMask4;
		int32_t L_5 = V_0;
		bool L_6 = Physics_Raycast_m3501191919(NULL /*static, unused*/, L_0, L_1, L_2, L_3, L_4, L_5, /*hidden argument*/NULL);
		V_1 = L_6;
		goto IL_0015;
	}

IL_0015:
	{
		bool L_7 = V_1;
		return L_7;
	}
}
// System.Boolean UnityEngine.Physics::Raycast(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.RaycastHit&,System.Single)
extern "C"  bool Physics_Raycast_m2950885040 (Il2CppObject * __this /* static, unused */, Vector3_t3722313464  ___origin0, Vector3_t3722313464  ___direction1, RaycastHit_t1056001966 * ___hitInfo2, float ___maxDistance3, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	bool V_2 = false;
	{
		V_0 = 0;
		V_1 = ((int32_t)-5);
		Vector3_t3722313464  L_0 = ___origin0;
		Vector3_t3722313464  L_1 = ___direction1;
		RaycastHit_t1056001966 * L_2 = ___hitInfo2;
		float L_3 = ___maxDistance3;
		int32_t L_4 = V_1;
		int32_t L_5 = V_0;
		bool L_6 = Physics_Raycast_m3501191919(NULL /*static, unused*/, L_0, L_1, L_2, L_3, L_4, L_5, /*hidden argument*/NULL);
		V_2 = L_6;
		goto IL_0017;
	}

IL_0017:
	{
		bool L_7 = V_2;
		return L_7;
	}
}
// System.Boolean UnityEngine.Physics::Raycast(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.RaycastHit&)
extern "C"  bool Physics_Raycast_m1953748314 (Il2CppObject * __this /* static, unused */, Vector3_t3722313464  ___origin0, Vector3_t3722313464  ___direction1, RaycastHit_t1056001966 * ___hitInfo2, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	float V_2 = 0.0f;
	bool V_3 = false;
	{
		V_0 = 0;
		V_1 = ((int32_t)-5);
		V_2 = (std::numeric_limits<float>::infinity());
		Vector3_t3722313464  L_0 = ___origin0;
		Vector3_t3722313464  L_1 = ___direction1;
		RaycastHit_t1056001966 * L_2 = ___hitInfo2;
		float L_3 = V_2;
		int32_t L_4 = V_1;
		int32_t L_5 = V_0;
		bool L_6 = Physics_Raycast_m3501191919(NULL /*static, unused*/, L_0, L_1, L_2, L_3, L_4, L_5, /*hidden argument*/NULL);
		V_3 = L_6;
		goto IL_001d;
	}

IL_001d:
	{
		bool L_7 = V_3;
		return L_7;
	}
}
// System.Boolean UnityEngine.Physics::Raycast(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.RaycastHit&,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern "C"  bool Physics_Raycast_m3501191919 (Il2CppObject * __this /* static, unused */, Vector3_t3722313464  ___origin0, Vector3_t3722313464  ___direction1, RaycastHit_t1056001966 * ___hitInfo2, float ___maxDistance3, int32_t ___layerMask4, int32_t ___queryTriggerInteraction5, const MethodInfo* method)
{
	bool V_0 = false;
	{
		Vector3_t3722313464  L_0 = ___origin0;
		Vector3_t3722313464  L_1 = ___direction1;
		RaycastHit_t1056001966 * L_2 = ___hitInfo2;
		float L_3 = ___maxDistance3;
		int32_t L_4 = ___layerMask4;
		int32_t L_5 = ___queryTriggerInteraction5;
		bool L_6 = Physics_Internal_Raycast_m1368166979(NULL /*static, unused*/, L_0, L_1, L_2, L_3, L_4, L_5, /*hidden argument*/NULL);
		V_0 = L_6;
		goto IL_0014;
	}

IL_0014:
	{
		bool L_7 = V_0;
		return L_7;
	}
}
// System.Boolean UnityEngine.Physics::Raycast(UnityEngine.Ray,System.Single,System.Int32)
extern "C"  bool Physics_Raycast_m964732411 (Il2CppObject * __this /* static, unused */, Ray_t3785851493  ___ray0, float ___maxDistance1, int32_t ___layerMask2, const MethodInfo* method)
{
	int32_t V_0 = 0;
	bool V_1 = false;
	{
		V_0 = 0;
		Ray_t3785851493  L_0 = ___ray0;
		float L_1 = ___maxDistance1;
		int32_t L_2 = ___layerMask2;
		int32_t L_3 = V_0;
		bool L_4 = Physics_Raycast_m1114390595(NULL /*static, unused*/, L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		V_1 = L_4;
		goto IL_0012;
	}

IL_0012:
	{
		bool L_5 = V_1;
		return L_5;
	}
}
// System.Boolean UnityEngine.Physics::Raycast(UnityEngine.Ray,System.Single)
extern "C"  bool Physics_Raycast_m2203245451 (Il2CppObject * __this /* static, unused */, Ray_t3785851493  ___ray0, float ___maxDistance1, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	bool V_2 = false;
	{
		V_0 = 0;
		V_1 = ((int32_t)-5);
		Ray_t3785851493  L_0 = ___ray0;
		float L_1 = ___maxDistance1;
		int32_t L_2 = V_1;
		int32_t L_3 = V_0;
		bool L_4 = Physics_Raycast_m1114390595(NULL /*static, unused*/, L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		V_2 = L_4;
		goto IL_0015;
	}

IL_0015:
	{
		bool L_5 = V_2;
		return L_5;
	}
}
// System.Boolean UnityEngine.Physics::Raycast(UnityEngine.Ray)
extern "C"  bool Physics_Raycast_m2622874622 (Il2CppObject * __this /* static, unused */, Ray_t3785851493  ___ray0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	float V_2 = 0.0f;
	bool V_3 = false;
	{
		V_0 = 0;
		V_1 = ((int32_t)-5);
		V_2 = (std::numeric_limits<float>::infinity());
		Ray_t3785851493  L_0 = ___ray0;
		float L_1 = V_2;
		int32_t L_2 = V_1;
		int32_t L_3 = V_0;
		bool L_4 = Physics_Raycast_m1114390595(NULL /*static, unused*/, L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		V_3 = L_4;
		goto IL_001b;
	}

IL_001b:
	{
		bool L_5 = V_3;
		return L_5;
	}
}
// System.Boolean UnityEngine.Physics::Raycast(UnityEngine.Ray,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern "C"  bool Physics_Raycast_m1114390595 (Il2CppObject * __this /* static, unused */, Ray_t3785851493  ___ray0, float ___maxDistance1, int32_t ___layerMask2, int32_t ___queryTriggerInteraction3, const MethodInfo* method)
{
	bool V_0 = false;
	{
		Vector3_t3722313464  L_0 = Ray_get_origin_m4290253200((&___ray0), /*hidden argument*/NULL);
		Vector3_t3722313464  L_1 = Ray_get_direction_m1991692996((&___ray0), /*hidden argument*/NULL);
		float L_2 = ___maxDistance1;
		int32_t L_3 = ___layerMask2;
		int32_t L_4 = ___queryTriggerInteraction3;
		bool L_5 = Physics_Raycast_m325282035(NULL /*static, unused*/, L_0, L_1, L_2, L_3, L_4, /*hidden argument*/NULL);
		V_0 = L_5;
		goto IL_001d;
	}

IL_001d:
	{
		bool L_6 = V_0;
		return L_6;
	}
}
// System.Boolean UnityEngine.Physics::Raycast(UnityEngine.Ray,UnityEngine.RaycastHit&,System.Single,System.Int32)
extern "C"  bool Physics_Raycast_m3286047692 (Il2CppObject * __this /* static, unused */, Ray_t3785851493  ___ray0, RaycastHit_t1056001966 * ___hitInfo1, float ___maxDistance2, int32_t ___layerMask3, const MethodInfo* method)
{
	int32_t V_0 = 0;
	bool V_1 = false;
	{
		V_0 = 0;
		Ray_t3785851493  L_0 = ___ray0;
		RaycastHit_t1056001966 * L_1 = ___hitInfo1;
		float L_2 = ___maxDistance2;
		int32_t L_3 = ___layerMask3;
		int32_t L_4 = V_0;
		bool L_5 = Physics_Raycast_m121696816(NULL /*static, unused*/, L_0, L_1, L_2, L_3, L_4, /*hidden argument*/NULL);
		V_1 = L_5;
		goto IL_0013;
	}

IL_0013:
	{
		bool L_6 = V_1;
		return L_6;
	}
}
// System.Boolean UnityEngine.Physics::Raycast(UnityEngine.Ray,UnityEngine.RaycastHit&,System.Single)
extern "C"  bool Physics_Raycast_m2500195530 (Il2CppObject * __this /* static, unused */, Ray_t3785851493  ___ray0, RaycastHit_t1056001966 * ___hitInfo1, float ___maxDistance2, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	bool V_2 = false;
	{
		V_0 = 0;
		V_1 = ((int32_t)-5);
		Ray_t3785851493  L_0 = ___ray0;
		RaycastHit_t1056001966 * L_1 = ___hitInfo1;
		float L_2 = ___maxDistance2;
		int32_t L_3 = V_1;
		int32_t L_4 = V_0;
		bool L_5 = Physics_Raycast_m121696816(NULL /*static, unused*/, L_0, L_1, L_2, L_3, L_4, /*hidden argument*/NULL);
		V_2 = L_5;
		goto IL_0016;
	}

IL_0016:
	{
		bool L_6 = V_2;
		return L_6;
	}
}
// System.Boolean UnityEngine.Physics::Raycast(UnityEngine.Ray,UnityEngine.RaycastHit&)
extern "C"  bool Physics_Raycast_m3906114327 (Il2CppObject * __this /* static, unused */, Ray_t3785851493  ___ray0, RaycastHit_t1056001966 * ___hitInfo1, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	float V_2 = 0.0f;
	bool V_3 = false;
	{
		V_0 = 0;
		V_1 = ((int32_t)-5);
		V_2 = (std::numeric_limits<float>::infinity());
		Ray_t3785851493  L_0 = ___ray0;
		RaycastHit_t1056001966 * L_1 = ___hitInfo1;
		float L_2 = V_2;
		int32_t L_3 = V_1;
		int32_t L_4 = V_0;
		bool L_5 = Physics_Raycast_m121696816(NULL /*static, unused*/, L_0, L_1, L_2, L_3, L_4, /*hidden argument*/NULL);
		V_3 = L_5;
		goto IL_001c;
	}

IL_001c:
	{
		bool L_6 = V_3;
		return L_6;
	}
}
// System.Boolean UnityEngine.Physics::Raycast(UnityEngine.Ray,UnityEngine.RaycastHit&,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern "C"  bool Physics_Raycast_m121696816 (Il2CppObject * __this /* static, unused */, Ray_t3785851493  ___ray0, RaycastHit_t1056001966 * ___hitInfo1, float ___maxDistance2, int32_t ___layerMask3, int32_t ___queryTriggerInteraction4, const MethodInfo* method)
{
	bool V_0 = false;
	{
		Vector3_t3722313464  L_0 = Ray_get_origin_m4290253200((&___ray0), /*hidden argument*/NULL);
		Vector3_t3722313464  L_1 = Ray_get_direction_m1991692996((&___ray0), /*hidden argument*/NULL);
		RaycastHit_t1056001966 * L_2 = ___hitInfo1;
		float L_3 = ___maxDistance2;
		int32_t L_4 = ___layerMask3;
		int32_t L_5 = ___queryTriggerInteraction4;
		bool L_6 = Physics_Raycast_m3501191919(NULL /*static, unused*/, L_0, L_1, L_2, L_3, L_4, L_5, /*hidden argument*/NULL);
		V_0 = L_6;
		goto IL_001f;
	}

IL_001f:
	{
		bool L_7 = V_0;
		return L_7;
	}
}
// UnityEngine.RaycastHit[] UnityEngine.Physics::RaycastAll(UnityEngine.Ray,System.Single,System.Int32)
extern "C"  RaycastHitU5BU5D_t1690781147* Physics_RaycastAll_m2697494287 (Il2CppObject * __this /* static, unused */, Ray_t3785851493  ___ray0, float ___maxDistance1, int32_t ___layerMask2, const MethodInfo* method)
{
	int32_t V_0 = 0;
	RaycastHitU5BU5D_t1690781147* V_1 = NULL;
	{
		V_0 = 0;
		Ray_t3785851493  L_0 = ___ray0;
		float L_1 = ___maxDistance1;
		int32_t L_2 = ___layerMask2;
		int32_t L_3 = V_0;
		RaycastHitU5BU5D_t1690781147* L_4 = Physics_RaycastAll_m2730568772(NULL /*static, unused*/, L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		V_1 = L_4;
		goto IL_0012;
	}

IL_0012:
	{
		RaycastHitU5BU5D_t1690781147* L_5 = V_1;
		return L_5;
	}
}
// UnityEngine.RaycastHit[] UnityEngine.Physics::RaycastAll(UnityEngine.Ray,System.Single)
extern "C"  RaycastHitU5BU5D_t1690781147* Physics_RaycastAll_m124807154 (Il2CppObject * __this /* static, unused */, Ray_t3785851493  ___ray0, float ___maxDistance1, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	RaycastHitU5BU5D_t1690781147* V_2 = NULL;
	{
		V_0 = 0;
		V_1 = ((int32_t)-5);
		Ray_t3785851493  L_0 = ___ray0;
		float L_1 = ___maxDistance1;
		int32_t L_2 = V_1;
		int32_t L_3 = V_0;
		RaycastHitU5BU5D_t1690781147* L_4 = Physics_RaycastAll_m2730568772(NULL /*static, unused*/, L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		V_2 = L_4;
		goto IL_0015;
	}

IL_0015:
	{
		RaycastHitU5BU5D_t1690781147* L_5 = V_2;
		return L_5;
	}
}
// UnityEngine.RaycastHit[] UnityEngine.Physics::RaycastAll(UnityEngine.Ray)
extern "C"  RaycastHitU5BU5D_t1690781147* Physics_RaycastAll_m4127090769 (Il2CppObject * __this /* static, unused */, Ray_t3785851493  ___ray0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	float V_2 = 0.0f;
	RaycastHitU5BU5D_t1690781147* V_3 = NULL;
	{
		V_0 = 0;
		V_1 = ((int32_t)-5);
		V_2 = (std::numeric_limits<float>::infinity());
		Ray_t3785851493  L_0 = ___ray0;
		float L_1 = V_2;
		int32_t L_2 = V_1;
		int32_t L_3 = V_0;
		RaycastHitU5BU5D_t1690781147* L_4 = Physics_RaycastAll_m2730568772(NULL /*static, unused*/, L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		V_3 = L_4;
		goto IL_001b;
	}

IL_001b:
	{
		RaycastHitU5BU5D_t1690781147* L_5 = V_3;
		return L_5;
	}
}
// UnityEngine.RaycastHit[] UnityEngine.Physics::RaycastAll(UnityEngine.Ray,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern "C"  RaycastHitU5BU5D_t1690781147* Physics_RaycastAll_m2730568772 (Il2CppObject * __this /* static, unused */, Ray_t3785851493  ___ray0, float ___maxDistance1, int32_t ___layerMask2, int32_t ___queryTriggerInteraction3, const MethodInfo* method)
{
	RaycastHitU5BU5D_t1690781147* V_0 = NULL;
	{
		Vector3_t3722313464  L_0 = Ray_get_origin_m4290253200((&___ray0), /*hidden argument*/NULL);
		Vector3_t3722313464  L_1 = Ray_get_direction_m1991692996((&___ray0), /*hidden argument*/NULL);
		float L_2 = ___maxDistance1;
		int32_t L_3 = ___layerMask2;
		int32_t L_4 = ___queryTriggerInteraction3;
		RaycastHitU5BU5D_t1690781147* L_5 = Physics_RaycastAll_m1264095660(NULL /*static, unused*/, L_0, L_1, L_2, L_3, L_4, /*hidden argument*/NULL);
		V_0 = L_5;
		goto IL_001d;
	}

IL_001d:
	{
		RaycastHitU5BU5D_t1690781147* L_6 = V_0;
		return L_6;
	}
}
// UnityEngine.RaycastHit[] UnityEngine.Physics::RaycastAll(UnityEngine.Vector3,UnityEngine.Vector3,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern "C"  RaycastHitU5BU5D_t1690781147* Physics_RaycastAll_m1264095660 (Il2CppObject * __this /* static, unused */, Vector3_t3722313464  ___origin0, Vector3_t3722313464  ___direction1, float ___maxDistance2, int32_t ___layermask3, int32_t ___queryTriggerInteraction4, const MethodInfo* method)
{
	RaycastHitU5BU5D_t1690781147* V_0 = NULL;
	{
		float L_0 = ___maxDistance2;
		int32_t L_1 = ___layermask3;
		int32_t L_2 = ___queryTriggerInteraction4;
		RaycastHitU5BU5D_t1690781147* L_3 = Physics_INTERNAL_CALL_RaycastAll_m953202051(NULL /*static, unused*/, (&___origin0), (&___direction1), L_0, L_1, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		goto IL_0014;
	}

IL_0014:
	{
		RaycastHitU5BU5D_t1690781147* L_4 = V_0;
		return L_4;
	}
}
// UnityEngine.RaycastHit[] UnityEngine.Physics::RaycastAll(UnityEngine.Vector3,UnityEngine.Vector3,System.Single,System.Int32)
extern "C"  RaycastHitU5BU5D_t1690781147* Physics_RaycastAll_m4049773700 (Il2CppObject * __this /* static, unused */, Vector3_t3722313464  ___origin0, Vector3_t3722313464  ___direction1, float ___maxDistance2, int32_t ___layermask3, const MethodInfo* method)
{
	int32_t V_0 = 0;
	RaycastHitU5BU5D_t1690781147* V_1 = NULL;
	{
		V_0 = 0;
		float L_0 = ___maxDistance2;
		int32_t L_1 = ___layermask3;
		int32_t L_2 = V_0;
		RaycastHitU5BU5D_t1690781147* L_3 = Physics_INTERNAL_CALL_RaycastAll_m953202051(NULL /*static, unused*/, (&___origin0), (&___direction1), L_0, L_1, L_2, /*hidden argument*/NULL);
		V_1 = L_3;
		goto IL_0015;
	}

IL_0015:
	{
		RaycastHitU5BU5D_t1690781147* L_4 = V_1;
		return L_4;
	}
}
// UnityEngine.RaycastHit[] UnityEngine.Physics::RaycastAll(UnityEngine.Vector3,UnityEngine.Vector3,System.Single)
extern "C"  RaycastHitU5BU5D_t1690781147* Physics_RaycastAll_m571232817 (Il2CppObject * __this /* static, unused */, Vector3_t3722313464  ___origin0, Vector3_t3722313464  ___direction1, float ___maxDistance2, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	RaycastHitU5BU5D_t1690781147* V_2 = NULL;
	{
		V_0 = 0;
		V_1 = ((int32_t)-5);
		float L_0 = ___maxDistance2;
		int32_t L_1 = V_1;
		int32_t L_2 = V_0;
		RaycastHitU5BU5D_t1690781147* L_3 = Physics_INTERNAL_CALL_RaycastAll_m953202051(NULL /*static, unused*/, (&___origin0), (&___direction1), L_0, L_1, L_2, /*hidden argument*/NULL);
		V_2 = L_3;
		goto IL_0018;
	}

IL_0018:
	{
		RaycastHitU5BU5D_t1690781147* L_4 = V_2;
		return L_4;
	}
}
// UnityEngine.RaycastHit[] UnityEngine.Physics::RaycastAll(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  RaycastHitU5BU5D_t1690781147* Physics_RaycastAll_m3736319814 (Il2CppObject * __this /* static, unused */, Vector3_t3722313464  ___origin0, Vector3_t3722313464  ___direction1, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	float V_2 = 0.0f;
	RaycastHitU5BU5D_t1690781147* V_3 = NULL;
	{
		V_0 = 0;
		V_1 = ((int32_t)-5);
		V_2 = (std::numeric_limits<float>::infinity());
		float L_0 = V_2;
		int32_t L_1 = V_1;
		int32_t L_2 = V_0;
		RaycastHitU5BU5D_t1690781147* L_3 = Physics_INTERNAL_CALL_RaycastAll_m953202051(NULL /*static, unused*/, (&___origin0), (&___direction1), L_0, L_1, L_2, /*hidden argument*/NULL);
		V_3 = L_3;
		goto IL_001e;
	}

IL_001e:
	{
		RaycastHitU5BU5D_t1690781147* L_4 = V_3;
		return L_4;
	}
}
// UnityEngine.RaycastHit[] UnityEngine.Physics::INTERNAL_CALL_RaycastAll(UnityEngine.Vector3&,UnityEngine.Vector3&,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern "C"  RaycastHitU5BU5D_t1690781147* Physics_INTERNAL_CALL_RaycastAll_m953202051 (Il2CppObject * __this /* static, unused */, Vector3_t3722313464 * ___origin0, Vector3_t3722313464 * ___direction1, float ___maxDistance2, int32_t ___layermask3, int32_t ___queryTriggerInteraction4, const MethodInfo* method)
{
	typedef RaycastHitU5BU5D_t1690781147* (*Physics_INTERNAL_CALL_RaycastAll_m953202051_ftn) (Vector3_t3722313464 *, Vector3_t3722313464 *, float, int32_t, int32_t);
	static Physics_INTERNAL_CALL_RaycastAll_m953202051_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Physics_INTERNAL_CALL_RaycastAll_m953202051_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Physics::INTERNAL_CALL_RaycastAll(UnityEngine.Vector3&,UnityEngine.Vector3&,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)");
	return _il2cpp_icall_func(___origin0, ___direction1, ___maxDistance2, ___layermask3, ___queryTriggerInteraction4);
}
// System.Boolean UnityEngine.Physics::Linecast(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.RaycastHit&)
extern "C"  bool Physics_Linecast_m4224048496 (Il2CppObject * __this /* static, unused */, Vector3_t3722313464  ___start0, Vector3_t3722313464  ___end1, RaycastHit_t1056001966 * ___hitInfo2, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	bool V_2 = false;
	{
		V_0 = 0;
		V_1 = ((int32_t)-5);
		Vector3_t3722313464  L_0 = ___start0;
		Vector3_t3722313464  L_1 = ___end1;
		RaycastHit_t1056001966 * L_2 = ___hitInfo2;
		int32_t L_3 = V_1;
		int32_t L_4 = V_0;
		bool L_5 = Physics_Linecast_m93906766(NULL /*static, unused*/, L_0, L_1, L_2, L_3, L_4, /*hidden argument*/NULL);
		V_2 = L_5;
		goto IL_0016;
	}

IL_0016:
	{
		bool L_6 = V_2;
		return L_6;
	}
}
// System.Boolean UnityEngine.Physics::Linecast(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.RaycastHit&,System.Int32,UnityEngine.QueryTriggerInteraction)
extern "C"  bool Physics_Linecast_m93906766 (Il2CppObject * __this /* static, unused */, Vector3_t3722313464  ___start0, Vector3_t3722313464  ___end1, RaycastHit_t1056001966 * ___hitInfo2, int32_t ___layerMask3, int32_t ___queryTriggerInteraction4, const MethodInfo* method)
{
	Vector3_t3722313464  V_0;
	memset(&V_0, 0, sizeof(V_0));
	bool V_1 = false;
	{
		Vector3_t3722313464  L_0 = ___end1;
		Vector3_t3722313464  L_1 = ___start0;
		Vector3_t3722313464  L_2 = Vector3_op_Subtraction_m2566684344(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		Vector3_t3722313464  L_3 = ___start0;
		Vector3_t3722313464  L_4 = V_0;
		RaycastHit_t1056001966 * L_5 = ___hitInfo2;
		float L_6 = Vector3_get_magnitude_m206834744((&V_0), /*hidden argument*/NULL);
		int32_t L_7 = ___layerMask3;
		int32_t L_8 = ___queryTriggerInteraction4;
		bool L_9 = Physics_Raycast_m3501191919(NULL /*static, unused*/, L_3, L_4, L_5, L_6, L_7, L_8, /*hidden argument*/NULL);
		V_1 = L_9;
		goto IL_0021;
	}

IL_0021:
	{
		bool L_10 = V_1;
		return L_10;
	}
}
// UnityEngine.Collider[] UnityEngine.Physics::OverlapSphere(UnityEngine.Vector3,System.Single,System.Int32)
extern "C"  ColliderU5BU5D_t4234922487* Physics_OverlapSphere_m794301753 (Il2CppObject * __this /* static, unused */, Vector3_t3722313464  ___position0, float ___radius1, int32_t ___layerMask2, const MethodInfo* method)
{
	int32_t V_0 = 0;
	ColliderU5BU5D_t4234922487* V_1 = NULL;
	{
		V_0 = 0;
		float L_0 = ___radius1;
		int32_t L_1 = ___layerMask2;
		int32_t L_2 = V_0;
		ColliderU5BU5D_t4234922487* L_3 = Physics_INTERNAL_CALL_OverlapSphere_m143667889(NULL /*static, unused*/, (&___position0), L_0, L_1, L_2, /*hidden argument*/NULL);
		V_1 = L_3;
		goto IL_0013;
	}

IL_0013:
	{
		ColliderU5BU5D_t4234922487* L_4 = V_1;
		return L_4;
	}
}
// UnityEngine.Collider[] UnityEngine.Physics::INTERNAL_CALL_OverlapSphere(UnityEngine.Vector3&,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern "C"  ColliderU5BU5D_t4234922487* Physics_INTERNAL_CALL_OverlapSphere_m143667889 (Il2CppObject * __this /* static, unused */, Vector3_t3722313464 * ___position0, float ___radius1, int32_t ___layerMask2, int32_t ___queryTriggerInteraction3, const MethodInfo* method)
{
	typedef ColliderU5BU5D_t4234922487* (*Physics_INTERNAL_CALL_OverlapSphere_m143667889_ftn) (Vector3_t3722313464 *, float, int32_t, int32_t);
	static Physics_INTERNAL_CALL_OverlapSphere_m143667889_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Physics_INTERNAL_CALL_OverlapSphere_m143667889_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Physics::INTERNAL_CALL_OverlapSphere(UnityEngine.Vector3&,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)");
	return _il2cpp_icall_func(___position0, ___radius1, ___layerMask2, ___queryTriggerInteraction3);
}
// System.Boolean UnityEngine.Physics::Internal_Raycast(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.RaycastHit&,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern "C"  bool Physics_Internal_Raycast_m1368166979 (Il2CppObject * __this /* static, unused */, Vector3_t3722313464  ___origin0, Vector3_t3722313464  ___direction1, RaycastHit_t1056001966 * ___hitInfo2, float ___maxDistance3, int32_t ___layermask4, int32_t ___queryTriggerInteraction5, const MethodInfo* method)
{
	bool V_0 = false;
	{
		RaycastHit_t1056001966 * L_0 = ___hitInfo2;
		float L_1 = ___maxDistance3;
		int32_t L_2 = ___layermask4;
		int32_t L_3 = ___queryTriggerInteraction5;
		bool L_4 = Physics_INTERNAL_CALL_Internal_Raycast_m741619868(NULL /*static, unused*/, (&___origin0), (&___direction1), L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		goto IL_0016;
	}

IL_0016:
	{
		bool L_5 = V_0;
		return L_5;
	}
}
// System.Boolean UnityEngine.Physics::INTERNAL_CALL_Internal_Raycast(UnityEngine.Vector3&,UnityEngine.Vector3&,UnityEngine.RaycastHit&,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern "C"  bool Physics_INTERNAL_CALL_Internal_Raycast_m741619868 (Il2CppObject * __this /* static, unused */, Vector3_t3722313464 * ___origin0, Vector3_t3722313464 * ___direction1, RaycastHit_t1056001966 * ___hitInfo2, float ___maxDistance3, int32_t ___layermask4, int32_t ___queryTriggerInteraction5, const MethodInfo* method)
{
	typedef bool (*Physics_INTERNAL_CALL_Internal_Raycast_m741619868_ftn) (Vector3_t3722313464 *, Vector3_t3722313464 *, RaycastHit_t1056001966 *, float, int32_t, int32_t);
	static Physics_INTERNAL_CALL_Internal_Raycast_m741619868_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Physics_INTERNAL_CALL_Internal_Raycast_m741619868_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Physics::INTERNAL_CALL_Internal_Raycast(UnityEngine.Vector3&,UnityEngine.Vector3&,UnityEngine.RaycastHit&,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)");
	return _il2cpp_icall_func(___origin0, ___direction1, ___hitInfo2, ___maxDistance3, ___layermask4, ___queryTriggerInteraction5);
}
// System.Boolean UnityEngine.Physics::Internal_RaycastTest(UnityEngine.Vector3,UnityEngine.Vector3,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern "C"  bool Physics_Internal_RaycastTest_m2320488002 (Il2CppObject * __this /* static, unused */, Vector3_t3722313464  ___origin0, Vector3_t3722313464  ___direction1, float ___maxDistance2, int32_t ___layermask3, int32_t ___queryTriggerInteraction4, const MethodInfo* method)
{
	bool V_0 = false;
	{
		float L_0 = ___maxDistance2;
		int32_t L_1 = ___layermask3;
		int32_t L_2 = ___queryTriggerInteraction4;
		bool L_3 = Physics_INTERNAL_CALL_Internal_RaycastTest_m1447703527(NULL /*static, unused*/, (&___origin0), (&___direction1), L_0, L_1, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		goto IL_0014;
	}

IL_0014:
	{
		bool L_4 = V_0;
		return L_4;
	}
}
// System.Boolean UnityEngine.Physics::INTERNAL_CALL_Internal_RaycastTest(UnityEngine.Vector3&,UnityEngine.Vector3&,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)
extern "C"  bool Physics_INTERNAL_CALL_Internal_RaycastTest_m1447703527 (Il2CppObject * __this /* static, unused */, Vector3_t3722313464 * ___origin0, Vector3_t3722313464 * ___direction1, float ___maxDistance2, int32_t ___layermask3, int32_t ___queryTriggerInteraction4, const MethodInfo* method)
{
	typedef bool (*Physics_INTERNAL_CALL_Internal_RaycastTest_m1447703527_ftn) (Vector3_t3722313464 *, Vector3_t3722313464 *, float, int32_t, int32_t);
	static Physics_INTERNAL_CALL_Internal_RaycastTest_m1447703527_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Physics_INTERNAL_CALL_Internal_RaycastTest_m1447703527_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Physics::INTERNAL_CALL_Internal_RaycastTest(UnityEngine.Vector3&,UnityEngine.Vector3&,System.Single,System.Int32,UnityEngine.QueryTriggerInteraction)");
	return _il2cpp_icall_func(___origin0, ___direction1, ___maxDistance2, ___layermask3, ___queryTriggerInteraction4);
}
// System.Boolean UnityEngine.Physics2D::get_queriesHitTriggers()
extern "C"  bool Physics2D_get_queriesHitTriggers_m3312014212 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef bool (*Physics2D_get_queriesHitTriggers_m3312014212_ftn) ();
	static Physics2D_get_queriesHitTriggers_m3312014212_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Physics2D_get_queriesHitTriggers_m3312014212_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Physics2D::get_queriesHitTriggers()");
	return _il2cpp_icall_func();
}
// UnityEngine.RaycastHit2D UnityEngine.Physics2D::Raycast(UnityEngine.Vector2,UnityEngine.Vector2,System.Single,System.Int32,System.Single)
extern "C"  RaycastHit2D_t2279581989  Physics2D_Raycast_m2968679153 (Il2CppObject * __this /* static, unused */, Vector2_t2156229523  ___origin0, Vector2_t2156229523  ___direction1, float ___distance2, int32_t ___layerMask3, float ___minDepth4, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Physics2D_Raycast_m2968679153_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	RaycastHit2D_t2279581989  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		V_0 = (std::numeric_limits<float>::infinity());
		Vector2_t2156229523  L_0 = ___origin0;
		Vector2_t2156229523  L_1 = ___direction1;
		float L_2 = ___distance2;
		int32_t L_3 = ___layerMask3;
		float L_4 = ___minDepth4;
		float L_5 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Physics2D_t1528932956_il2cpp_TypeInfo_var);
		RaycastHit2D_t2279581989  L_6 = Physics2D_Raycast_m4289230241(NULL /*static, unused*/, L_0, L_1, L_2, L_3, L_4, L_5, /*hidden argument*/NULL);
		V_1 = L_6;
		goto IL_0019;
	}

IL_0019:
	{
		RaycastHit2D_t2279581989  L_7 = V_1;
		return L_7;
	}
}
// UnityEngine.RaycastHit2D UnityEngine.Physics2D::Raycast(UnityEngine.Vector2,UnityEngine.Vector2,System.Single,System.Int32)
extern "C"  RaycastHit2D_t2279581989  Physics2D_Raycast_m2839773815 (Il2CppObject * __this /* static, unused */, Vector2_t2156229523  ___origin0, Vector2_t2156229523  ___direction1, float ___distance2, int32_t ___layerMask3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Physics2D_Raycast_m2839773815_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	RaycastHit2D_t2279581989  V_2;
	memset(&V_2, 0, sizeof(V_2));
	{
		V_0 = (std::numeric_limits<float>::infinity());
		V_1 = (-std::numeric_limits<float>::infinity());
		Vector2_t2156229523  L_0 = ___origin0;
		Vector2_t2156229523  L_1 = ___direction1;
		float L_2 = ___distance2;
		int32_t L_3 = ___layerMask3;
		float L_4 = V_1;
		float L_5 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Physics2D_t1528932956_il2cpp_TypeInfo_var);
		RaycastHit2D_t2279581989  L_6 = Physics2D_Raycast_m4289230241(NULL /*static, unused*/, L_0, L_1, L_2, L_3, L_4, L_5, /*hidden argument*/NULL);
		V_2 = L_6;
		goto IL_001e;
	}

IL_001e:
	{
		RaycastHit2D_t2279581989  L_7 = V_2;
		return L_7;
	}
}
// UnityEngine.RaycastHit2D UnityEngine.Physics2D::Raycast(UnityEngine.Vector2,UnityEngine.Vector2,System.Single)
extern "C"  RaycastHit2D_t2279581989  Physics2D_Raycast_m500627388 (Il2CppObject * __this /* static, unused */, Vector2_t2156229523  ___origin0, Vector2_t2156229523  ___direction1, float ___distance2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Physics2D_Raycast_m500627388_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	int32_t V_2 = 0;
	RaycastHit2D_t2279581989  V_3;
	memset(&V_3, 0, sizeof(V_3));
	{
		V_0 = (std::numeric_limits<float>::infinity());
		V_1 = (-std::numeric_limits<float>::infinity());
		V_2 = ((int32_t)-5);
		Vector2_t2156229523  L_0 = ___origin0;
		Vector2_t2156229523  L_1 = ___direction1;
		float L_2 = ___distance2;
		int32_t L_3 = V_2;
		float L_4 = V_1;
		float L_5 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Physics2D_t1528932956_il2cpp_TypeInfo_var);
		RaycastHit2D_t2279581989  L_6 = Physics2D_Raycast_m4289230241(NULL /*static, unused*/, L_0, L_1, L_2, L_3, L_4, L_5, /*hidden argument*/NULL);
		V_3 = L_6;
		goto IL_0021;
	}

IL_0021:
	{
		RaycastHit2D_t2279581989  L_7 = V_3;
		return L_7;
	}
}
// UnityEngine.RaycastHit2D UnityEngine.Physics2D::Raycast(UnityEngine.Vector2,UnityEngine.Vector2)
extern "C"  RaycastHit2D_t2279581989  Physics2D_Raycast_m4254966674 (Il2CppObject * __this /* static, unused */, Vector2_t2156229523  ___origin0, Vector2_t2156229523  ___direction1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Physics2D_Raycast_m4254966674_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	int32_t V_2 = 0;
	float V_3 = 0.0f;
	RaycastHit2D_t2279581989  V_4;
	memset(&V_4, 0, sizeof(V_4));
	{
		V_0 = (std::numeric_limits<float>::infinity());
		V_1 = (-std::numeric_limits<float>::infinity());
		V_2 = ((int32_t)-5);
		V_3 = (std::numeric_limits<float>::infinity());
		Vector2_t2156229523  L_0 = ___origin0;
		Vector2_t2156229523  L_1 = ___direction1;
		float L_2 = V_3;
		int32_t L_3 = V_2;
		float L_4 = V_1;
		float L_5 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Physics2D_t1528932956_il2cpp_TypeInfo_var);
		RaycastHit2D_t2279581989  L_6 = Physics2D_Raycast_m4289230241(NULL /*static, unused*/, L_0, L_1, L_2, L_3, L_4, L_5, /*hidden argument*/NULL);
		V_4 = L_6;
		goto IL_0028;
	}

IL_0028:
	{
		RaycastHit2D_t2279581989  L_7 = V_4;
		return L_7;
	}
}
// UnityEngine.RaycastHit2D UnityEngine.Physics2D::Raycast(UnityEngine.Vector2,UnityEngine.Vector2,System.Single,System.Int32,System.Single,System.Single)
extern "C"  RaycastHit2D_t2279581989  Physics2D_Raycast_m4289230241 (Il2CppObject * __this /* static, unused */, Vector2_t2156229523  ___origin0, Vector2_t2156229523  ___direction1, float ___distance2, int32_t ___layerMask3, float ___minDepth4, float ___maxDepth5, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Physics2D_Raycast_m4289230241_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ContactFilter2D_t3805203441  V_0;
	memset(&V_0, 0, sizeof(V_0));
	RaycastHit2D_t2279581989  V_1;
	memset(&V_1, 0, sizeof(V_1));
	RaycastHit2D_t2279581989  V_2;
	memset(&V_2, 0, sizeof(V_2));
	{
		int32_t L_0 = ___layerMask3;
		float L_1 = ___minDepth4;
		float L_2 = ___maxDepth5;
		ContactFilter2D_t3805203441  L_3 = ContactFilter2D_CreateLegacyFilter_m949420789(NULL /*static, unused*/, L_0, L_1, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		Vector2_t2156229523  L_4 = ___origin0;
		Vector2_t2156229523  L_5 = ___direction1;
		float L_6 = ___distance2;
		ContactFilter2D_t3805203441  L_7 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Physics2D_t1528932956_il2cpp_TypeInfo_var);
		Physics2D_Internal_Raycast_m4120652668(NULL /*static, unused*/, L_4, L_5, L_6, L_7, (&V_1), /*hidden argument*/NULL);
		RaycastHit2D_t2279581989  L_8 = V_1;
		V_2 = L_8;
		goto IL_001e;
	}

IL_001e:
	{
		RaycastHit2D_t2279581989  L_9 = V_2;
		return L_9;
	}
}
// System.Int32 UnityEngine.Physics2D::Raycast(UnityEngine.Vector2,UnityEngine.Vector2,UnityEngine.ContactFilter2D,UnityEngine.RaycastHit2D[])
extern "C"  int32_t Physics2D_Raycast_m3588436803 (Il2CppObject * __this /* static, unused */, Vector2_t2156229523  ___origin0, Vector2_t2156229523  ___direction1, ContactFilter2D_t3805203441  ___contactFilter2, RaycastHit2DU5BU5D_t4286651560* ___results3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Physics2D_Raycast_m3588436803_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	int32_t V_1 = 0;
	{
		V_0 = (std::numeric_limits<float>::infinity());
		Vector2_t2156229523  L_0 = ___origin0;
		Vector2_t2156229523  L_1 = ___direction1;
		ContactFilter2D_t3805203441  L_2 = ___contactFilter2;
		RaycastHit2DU5BU5D_t4286651560* L_3 = ___results3;
		float L_4 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Physics2D_t1528932956_il2cpp_TypeInfo_var);
		int32_t L_5 = Physics2D_Raycast_m787477166(NULL /*static, unused*/, L_0, L_1, L_2, L_3, L_4, /*hidden argument*/NULL);
		V_1 = L_5;
		goto IL_0017;
	}

IL_0017:
	{
		int32_t L_6 = V_1;
		return L_6;
	}
}
// System.Int32 UnityEngine.Physics2D::Raycast(UnityEngine.Vector2,UnityEngine.Vector2,UnityEngine.ContactFilter2D,UnityEngine.RaycastHit2D[],System.Single)
extern "C"  int32_t Physics2D_Raycast_m787477166 (Il2CppObject * __this /* static, unused */, Vector2_t2156229523  ___origin0, Vector2_t2156229523  ___direction1, ContactFilter2D_t3805203441  ___contactFilter2, RaycastHit2DU5BU5D_t4286651560* ___results3, float ___distance4, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Physics2D_Raycast_m787477166_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		Vector2_t2156229523  L_0 = ___origin0;
		Vector2_t2156229523  L_1 = ___direction1;
		float L_2 = ___distance4;
		ContactFilter2D_t3805203441  L_3 = ___contactFilter2;
		RaycastHit2DU5BU5D_t4286651560* L_4 = ___results3;
		IL2CPP_RUNTIME_CLASS_INIT(Physics2D_t1528932956_il2cpp_TypeInfo_var);
		int32_t L_5 = Physics2D_Internal_RaycastNonAlloc_m3606848900(NULL /*static, unused*/, L_0, L_1, L_2, L_3, L_4, /*hidden argument*/NULL);
		V_0 = L_5;
		goto IL_0012;
	}

IL_0012:
	{
		int32_t L_6 = V_0;
		return L_6;
	}
}
// System.Void UnityEngine.Physics2D::Internal_Raycast(UnityEngine.Vector2,UnityEngine.Vector2,System.Single,UnityEngine.ContactFilter2D,UnityEngine.RaycastHit2D&)
extern "C"  void Physics2D_Internal_Raycast_m4120652668 (Il2CppObject * __this /* static, unused */, Vector2_t2156229523  ___origin0, Vector2_t2156229523  ___direction1, float ___distance2, ContactFilter2D_t3805203441  ___contactFilter3, RaycastHit2D_t2279581989 * ___raycastHit4, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Physics2D_Internal_Raycast_m4120652668_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		float L_0 = ___distance2;
		RaycastHit2D_t2279581989 * L_1 = ___raycastHit4;
		IL2CPP_RUNTIME_CLASS_INIT(Physics2D_t1528932956_il2cpp_TypeInfo_var);
		Physics2D_INTERNAL_CALL_Internal_Raycast_m2882919538(NULL /*static, unused*/, (&___origin0), (&___direction1), L_0, (&___contactFilter3), L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Physics2D::INTERNAL_CALL_Internal_Raycast(UnityEngine.Vector2&,UnityEngine.Vector2&,System.Single,UnityEngine.ContactFilter2D&,UnityEngine.RaycastHit2D&)
extern "C"  void Physics2D_INTERNAL_CALL_Internal_Raycast_m2882919538 (Il2CppObject * __this /* static, unused */, Vector2_t2156229523 * ___origin0, Vector2_t2156229523 * ___direction1, float ___distance2, ContactFilter2D_t3805203441 * ___contactFilter3, RaycastHit2D_t2279581989 * ___raycastHit4, const MethodInfo* method)
{
	typedef void (*Physics2D_INTERNAL_CALL_Internal_Raycast_m2882919538_ftn) (Vector2_t2156229523 *, Vector2_t2156229523 *, float, ContactFilter2D_t3805203441 *, RaycastHit2D_t2279581989 *);
	static Physics2D_INTERNAL_CALL_Internal_Raycast_m2882919538_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Physics2D_INTERNAL_CALL_Internal_Raycast_m2882919538_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Physics2D::INTERNAL_CALL_Internal_Raycast(UnityEngine.Vector2&,UnityEngine.Vector2&,System.Single,UnityEngine.ContactFilter2D&,UnityEngine.RaycastHit2D&)");
	_il2cpp_icall_func(___origin0, ___direction1, ___distance2, ___contactFilter3, ___raycastHit4);
}
// System.Int32 UnityEngine.Physics2D::Internal_RaycastNonAlloc(UnityEngine.Vector2,UnityEngine.Vector2,System.Single,UnityEngine.ContactFilter2D,UnityEngine.RaycastHit2D[])
extern "C"  int32_t Physics2D_Internal_RaycastNonAlloc_m3606848900 (Il2CppObject * __this /* static, unused */, Vector2_t2156229523  ___origin0, Vector2_t2156229523  ___direction1, float ___distance2, ContactFilter2D_t3805203441  ___contactFilter3, RaycastHit2DU5BU5D_t4286651560* ___results4, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Physics2D_Internal_RaycastNonAlloc_m3606848900_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		float L_0 = ___distance2;
		RaycastHit2DU5BU5D_t4286651560* L_1 = ___results4;
		IL2CPP_RUNTIME_CLASS_INIT(Physics2D_t1528932956_il2cpp_TypeInfo_var);
		int32_t L_2 = Physics2D_INTERNAL_CALL_Internal_RaycastNonAlloc_m3718512001(NULL /*static, unused*/, (&___origin0), (&___direction1), L_0, (&___contactFilter3), L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		goto IL_0015;
	}

IL_0015:
	{
		int32_t L_3 = V_0;
		return L_3;
	}
}
// System.Int32 UnityEngine.Physics2D::INTERNAL_CALL_Internal_RaycastNonAlloc(UnityEngine.Vector2&,UnityEngine.Vector2&,System.Single,UnityEngine.ContactFilter2D&,UnityEngine.RaycastHit2D[])
extern "C"  int32_t Physics2D_INTERNAL_CALL_Internal_RaycastNonAlloc_m3718512001 (Il2CppObject * __this /* static, unused */, Vector2_t2156229523 * ___origin0, Vector2_t2156229523 * ___direction1, float ___distance2, ContactFilter2D_t3805203441 * ___contactFilter3, RaycastHit2DU5BU5D_t4286651560* ___results4, const MethodInfo* method)
{
	typedef int32_t (*Physics2D_INTERNAL_CALL_Internal_RaycastNonAlloc_m3718512001_ftn) (Vector2_t2156229523 *, Vector2_t2156229523 *, float, ContactFilter2D_t3805203441 *, RaycastHit2DU5BU5D_t4286651560*);
	static Physics2D_INTERNAL_CALL_Internal_RaycastNonAlloc_m3718512001_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Physics2D_INTERNAL_CALL_Internal_RaycastNonAlloc_m3718512001_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Physics2D::INTERNAL_CALL_Internal_RaycastNonAlloc(UnityEngine.Vector2&,UnityEngine.Vector2&,System.Single,UnityEngine.ContactFilter2D&,UnityEngine.RaycastHit2D[])");
	return _il2cpp_icall_func(___origin0, ___direction1, ___distance2, ___contactFilter3, ___results4);
}
// UnityEngine.RaycastHit2D[] UnityEngine.Physics2D::GetRayIntersectionAll(UnityEngine.Ray,System.Single,System.Int32)
extern "C"  RaycastHit2DU5BU5D_t4286651560* Physics2D_GetRayIntersectionAll_m2417435554 (Il2CppObject * __this /* static, unused */, Ray_t3785851493  ___ray0, float ___distance1, int32_t ___layerMask2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Physics2D_GetRayIntersectionAll_m2417435554_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RaycastHit2DU5BU5D_t4286651560* V_0 = NULL;
	{
		float L_0 = ___distance1;
		int32_t L_1 = ___layerMask2;
		IL2CPP_RUNTIME_CLASS_INIT(Physics2D_t1528932956_il2cpp_TypeInfo_var);
		RaycastHit2DU5BU5D_t4286651560* L_2 = Physics2D_INTERNAL_CALL_GetRayIntersectionAll_m1699480086(NULL /*static, unused*/, (&___ray0), L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		goto IL_0010;
	}

IL_0010:
	{
		RaycastHit2DU5BU5D_t4286651560* L_3 = V_0;
		return L_3;
	}
}
// UnityEngine.RaycastHit2D[] UnityEngine.Physics2D::GetRayIntersectionAll(UnityEngine.Ray,System.Single)
extern "C"  RaycastHit2DU5BU5D_t4286651560* Physics2D_GetRayIntersectionAll_m570733055 (Il2CppObject * __this /* static, unused */, Ray_t3785851493  ___ray0, float ___distance1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Physics2D_GetRayIntersectionAll_m570733055_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	RaycastHit2DU5BU5D_t4286651560* V_1 = NULL;
	{
		V_0 = ((int32_t)-5);
		float L_0 = ___distance1;
		int32_t L_1 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Physics2D_t1528932956_il2cpp_TypeInfo_var);
		RaycastHit2DU5BU5D_t4286651560* L_2 = Physics2D_INTERNAL_CALL_GetRayIntersectionAll_m1699480086(NULL /*static, unused*/, (&___ray0), L_0, L_1, /*hidden argument*/NULL);
		V_1 = L_2;
		goto IL_0013;
	}

IL_0013:
	{
		RaycastHit2DU5BU5D_t4286651560* L_3 = V_1;
		return L_3;
	}
}
// UnityEngine.RaycastHit2D[] UnityEngine.Physics2D::GetRayIntersectionAll(UnityEngine.Ray)
extern "C"  RaycastHit2DU5BU5D_t4286651560* Physics2D_GetRayIntersectionAll_m3245298148 (Il2CppObject * __this /* static, unused */, Ray_t3785851493  ___ray0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Physics2D_GetRayIntersectionAll_m3245298148_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	float V_1 = 0.0f;
	RaycastHit2DU5BU5D_t4286651560* V_2 = NULL;
	{
		V_0 = ((int32_t)-5);
		V_1 = (std::numeric_limits<float>::infinity());
		float L_0 = V_1;
		int32_t L_1 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Physics2D_t1528932956_il2cpp_TypeInfo_var);
		RaycastHit2DU5BU5D_t4286651560* L_2 = Physics2D_INTERNAL_CALL_GetRayIntersectionAll_m1699480086(NULL /*static, unused*/, (&___ray0), L_0, L_1, /*hidden argument*/NULL);
		V_2 = L_2;
		goto IL_0019;
	}

IL_0019:
	{
		RaycastHit2DU5BU5D_t4286651560* L_3 = V_2;
		return L_3;
	}
}
// UnityEngine.RaycastHit2D[] UnityEngine.Physics2D::INTERNAL_CALL_GetRayIntersectionAll(UnityEngine.Ray&,System.Single,System.Int32)
extern "C"  RaycastHit2DU5BU5D_t4286651560* Physics2D_INTERNAL_CALL_GetRayIntersectionAll_m1699480086 (Il2CppObject * __this /* static, unused */, Ray_t3785851493 * ___ray0, float ___distance1, int32_t ___layerMask2, const MethodInfo* method)
{
	typedef RaycastHit2DU5BU5D_t4286651560* (*Physics2D_INTERNAL_CALL_GetRayIntersectionAll_m1699480086_ftn) (Ray_t3785851493 *, float, int32_t);
	static Physics2D_INTERNAL_CALL_GetRayIntersectionAll_m1699480086_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Physics2D_INTERNAL_CALL_GetRayIntersectionAll_m1699480086_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Physics2D::INTERNAL_CALL_GetRayIntersectionAll(UnityEngine.Ray&,System.Single,System.Int32)");
	return _il2cpp_icall_func(___ray0, ___distance1, ___layerMask2);
}
// UnityEngine.Collider2D UnityEngine.Physics2D::OverlapPoint(UnityEngine.Vector2,System.Int32)
extern "C"  Collider2D_t2806799626 * Physics2D_OverlapPoint_m2391645110 (Il2CppObject * __this /* static, unused */, Vector2_t2156229523  ___point0, int32_t ___layerMask1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Physics2D_OverlapPoint_m2391645110_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	Collider2D_t2806799626 * V_2 = NULL;
	{
		V_0 = (std::numeric_limits<float>::infinity());
		V_1 = (-std::numeric_limits<float>::infinity());
		Vector2_t2156229523  L_0 = ___point0;
		int32_t L_1 = ___layerMask1;
		float L_2 = V_1;
		float L_3 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Physics2D_t1528932956_il2cpp_TypeInfo_var);
		Collider2D_t2806799626 * L_4 = Physics2D_OverlapPoint_m1306494458(NULL /*static, unused*/, L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		V_2 = L_4;
		goto IL_001c;
	}

IL_001c:
	{
		Collider2D_t2806799626 * L_5 = V_2;
		return L_5;
	}
}
// UnityEngine.Collider2D UnityEngine.Physics2D::OverlapPoint(UnityEngine.Vector2,System.Int32,System.Single,System.Single)
extern "C"  Collider2D_t2806799626 * Physics2D_OverlapPoint_m1306494458 (Il2CppObject * __this /* static, unused */, Vector2_t2156229523  ___point0, int32_t ___layerMask1, float ___minDepth2, float ___maxDepth3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Physics2D_OverlapPoint_m1306494458_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ContactFilter2D_t3805203441  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Collider2D_t2806799626 * V_1 = NULL;
	{
		int32_t L_0 = ___layerMask1;
		float L_1 = ___minDepth2;
		float L_2 = ___maxDepth3;
		ContactFilter2D_t3805203441  L_3 = ContactFilter2D_CreateLegacyFilter_m949420789(NULL /*static, unused*/, L_0, L_1, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		Vector2_t2156229523  L_4 = ___point0;
		ContactFilter2D_t3805203441  L_5 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Physics2D_t1528932956_il2cpp_TypeInfo_var);
		Collider2D_t2806799626 * L_6 = Physics2D_Internal_OverlapPoint_m1360518232(NULL /*static, unused*/, L_4, L_5, /*hidden argument*/NULL);
		V_1 = L_6;
		goto IL_0017;
	}

IL_0017:
	{
		Collider2D_t2806799626 * L_7 = V_1;
		return L_7;
	}
}
// UnityEngine.Collider2D UnityEngine.Physics2D::Internal_OverlapPoint(UnityEngine.Vector2,UnityEngine.ContactFilter2D)
extern "C"  Collider2D_t2806799626 * Physics2D_Internal_OverlapPoint_m1360518232 (Il2CppObject * __this /* static, unused */, Vector2_t2156229523  ___point0, ContactFilter2D_t3805203441  ___contactFilter1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Physics2D_Internal_OverlapPoint_m1360518232_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Collider2D_t2806799626 * V_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(Physics2D_t1528932956_il2cpp_TypeInfo_var);
		Collider2D_t2806799626 * L_0 = Physics2D_INTERNAL_CALL_Internal_OverlapPoint_m1361933589(NULL /*static, unused*/, (&___point0), (&___contactFilter1), /*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_0010;
	}

IL_0010:
	{
		Collider2D_t2806799626 * L_1 = V_0;
		return L_1;
	}
}
// UnityEngine.Collider2D UnityEngine.Physics2D::INTERNAL_CALL_Internal_OverlapPoint(UnityEngine.Vector2&,UnityEngine.ContactFilter2D&)
extern "C"  Collider2D_t2806799626 * Physics2D_INTERNAL_CALL_Internal_OverlapPoint_m1361933589 (Il2CppObject * __this /* static, unused */, Vector2_t2156229523 * ___point0, ContactFilter2D_t3805203441 * ___contactFilter1, const MethodInfo* method)
{
	typedef Collider2D_t2806799626 * (*Physics2D_INTERNAL_CALL_Internal_OverlapPoint_m1361933589_ftn) (Vector2_t2156229523 *, ContactFilter2D_t3805203441 *);
	static Physics2D_INTERNAL_CALL_Internal_OverlapPoint_m1361933589_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Physics2D_INTERNAL_CALL_Internal_OverlapPoint_m1361933589_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Physics2D::INTERNAL_CALL_Internal_OverlapPoint(UnityEngine.Vector2&,UnityEngine.ContactFilter2D&)");
	return _il2cpp_icall_func(___point0, ___contactFilter1);
}
// UnityEngine.Rigidbody2D UnityEngine.Physics2D::GetRigidbodyFromInstanceID(System.Int32)
extern "C"  Rigidbody2D_t939494601 * Physics2D_GetRigidbodyFromInstanceID_m3174084804 (Il2CppObject * __this /* static, unused */, int32_t ___instanceID0, const MethodInfo* method)
{
	typedef Rigidbody2D_t939494601 * (*Physics2D_GetRigidbodyFromInstanceID_m3174084804_ftn) (int32_t);
	static Physics2D_GetRigidbodyFromInstanceID_m3174084804_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Physics2D_GetRigidbodyFromInstanceID_m3174084804_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Physics2D::GetRigidbodyFromInstanceID(System.Int32)");
	return _il2cpp_icall_func(___instanceID0);
}
// UnityEngine.Collider2D UnityEngine.Physics2D::GetColliderFromInstanceID(System.Int32)
extern "C"  Collider2D_t2806799626 * Physics2D_GetColliderFromInstanceID_m2016026012 (Il2CppObject * __this /* static, unused */, int32_t ___instanceID0, const MethodInfo* method)
{
	typedef Collider2D_t2806799626 * (*Physics2D_GetColliderFromInstanceID_m2016026012_ftn) (int32_t);
	static Physics2D_GetColliderFromInstanceID_m2016026012_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Physics2D_GetColliderFromInstanceID_m2016026012_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Physics2D::GetColliderFromInstanceID(System.Int32)");
	return _il2cpp_icall_func(___instanceID0);
}
// System.Void UnityEngine.Physics2D::.cctor()
extern "C"  void Physics2D__cctor_m2508016327 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Physics2D__cctor_m2508016327_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		List_1_t2411569343 * L_0 = (List_1_t2411569343 *)il2cpp_codegen_object_new(List_1_t2411569343_il2cpp_TypeInfo_var);
		List_1__ctor_m1332252848(L_0, /*hidden argument*/List_1__ctor_m1332252848_MethodInfo_var);
		((Physics2D_t1528932956_StaticFields*)Physics2D_t1528932956_il2cpp_TypeInfo_var->static_fields)->set_m_LastDisabledRigidbody2D_0(L_0);
		return;
	}
}
// System.Void UnityEngine.Plane::.ctor(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  void Plane__ctor_m2107222851 (Plane_t1000493321 * __this, Vector3_t3722313464  ___inNormal0, Vector3_t3722313464  ___inPoint1, const MethodInfo* method)
{
	{
		Vector3_t3722313464  L_0 = ___inNormal0;
		Vector3_t3722313464  L_1 = Vector3_Normalize_m2336073400(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		__this->set_m_Normal_0(L_1);
		Vector3_t3722313464  L_2 = __this->get_m_Normal_0();
		Vector3_t3722313464  L_3 = ___inPoint1;
		float L_4 = Vector3_Dot_m240955101(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		__this->set_m_Distance_1(((-L_4)));
		return;
	}
}
extern "C"  void Plane__ctor_m2107222851_AdjustorThunk (Il2CppObject * __this, Vector3_t3722313464  ___inNormal0, Vector3_t3722313464  ___inPoint1, const MethodInfo* method)
{
	Plane_t1000493321 * _thisAdjusted = reinterpret_cast<Plane_t1000493321 *>(__this + 1);
	Plane__ctor_m2107222851(_thisAdjusted, ___inNormal0, ___inPoint1, method);
}
// UnityEngine.Vector3 UnityEngine.Plane::get_normal()
extern "C"  Vector3_t3722313464  Plane_get_normal_m4269270389 (Plane_t1000493321 * __this, const MethodInfo* method)
{
	Vector3_t3722313464  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Vector3_t3722313464  L_0 = __this->get_m_Normal_0();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		Vector3_t3722313464  L_1 = V_0;
		return L_1;
	}
}
extern "C"  Vector3_t3722313464  Plane_get_normal_m4269270389_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Plane_t1000493321 * _thisAdjusted = reinterpret_cast<Plane_t1000493321 *>(__this + 1);
	return Plane_get_normal_m4269270389(_thisAdjusted, method);
}
// System.Single UnityEngine.Plane::get_distance()
extern "C"  float Plane_get_distance_m266757971 (Plane_t1000493321 * __this, const MethodInfo* method)
{
	float V_0 = 0.0f;
	{
		float L_0 = __this->get_m_Distance_1();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		float L_1 = V_0;
		return L_1;
	}
}
extern "C"  float Plane_get_distance_m266757971_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Plane_t1000493321 * _thisAdjusted = reinterpret_cast<Plane_t1000493321 *>(__this + 1);
	return Plane_get_distance_m266757971(_thisAdjusted, method);
}
// System.Boolean UnityEngine.Plane::Raycast(UnityEngine.Ray,System.Single&)
extern "C"  bool Plane_Raycast_m1361173428 (Plane_t1000493321 * __this, Ray_t3785851493  ___ray0, float* ___enter1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Plane_Raycast_m1361173428_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	bool V_2 = false;
	{
		Vector3_t3722313464  L_0 = Ray_get_direction_m1991692996((&___ray0), /*hidden argument*/NULL);
		Vector3_t3722313464  L_1 = Plane_get_normal_m4269270389(__this, /*hidden argument*/NULL);
		float L_2 = Vector3_Dot_m240955101(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		Vector3_t3722313464  L_3 = Ray_get_origin_m4290253200((&___ray0), /*hidden argument*/NULL);
		Vector3_t3722313464  L_4 = Plane_get_normal_m4269270389(__this, /*hidden argument*/NULL);
		float L_5 = Vector3_Dot_m240955101(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		float L_6 = Plane_get_distance_m266757971(__this, /*hidden argument*/NULL);
		V_1 = ((float)((float)((-L_5))-(float)L_6));
		float L_7 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t3464937446_il2cpp_TypeInfo_var);
		bool L_8 = Mathf_Approximately_m367990089(NULL /*static, unused*/, L_7, (0.0f), /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_004e;
		}
	}
	{
		float* L_9 = ___enter1;
		*((float*)(L_9)) = (float)(0.0f);
		V_2 = (bool)0;
		goto IL_0062;
	}

IL_004e:
	{
		float* L_10 = ___enter1;
		float L_11 = V_1;
		float L_12 = V_0;
		*((float*)(L_10)) = (float)((float)((float)L_11/(float)L_12));
		float* L_13 = ___enter1;
		V_2 = (bool)((((float)(*((float*)L_13))) > ((float)(0.0f)))? 1 : 0);
		goto IL_0062;
	}

IL_0062:
	{
		bool L_14 = V_2;
		return L_14;
	}
}
extern "C"  bool Plane_Raycast_m1361173428_AdjustorThunk (Il2CppObject * __this, Ray_t3785851493  ___ray0, float* ___enter1, const MethodInfo* method)
{
	Plane_t1000493321 * _thisAdjusted = reinterpret_cast<Plane_t1000493321 *>(__this + 1);
	return Plane_Raycast_m1361173428(_thisAdjusted, ___ray0, ___enter1, method);
}
// System.Void UnityEngine.PreferBinarySerialization::.ctor()
extern "C"  void PreferBinarySerialization__ctor_m4071697298 (PreferBinarySerialization_t2906007930 * __this, const MethodInfo* method)
{
	{
		Attribute__ctor_m1529526131(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.PropertyAttribute::.ctor()
extern "C"  void PropertyAttribute__ctor_m3954845021 (PropertyAttribute_t3677895545 * __this, const MethodInfo* method)
{
	{
		Attribute__ctor_m1529526131(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.QualitySettings::set_shadowDistance(System.Single)
extern "C"  void QualitySettings_set_shadowDistance_m2031262593 (Il2CppObject * __this /* static, unused */, float ___value0, const MethodInfo* method)
{
	typedef void (*QualitySettings_set_shadowDistance_m2031262593_ftn) (float);
	static QualitySettings_set_shadowDistance_m2031262593_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (QualitySettings_set_shadowDistance_m2031262593_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.QualitySettings::set_shadowDistance(System.Single)");
	_il2cpp_icall_func(___value0);
}
// System.Int32 UnityEngine.QualitySettings::get_vSyncCount()
extern "C"  int32_t QualitySettings_get_vSyncCount_m1814762262 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef int32_t (*QualitySettings_get_vSyncCount_m1814762262_ftn) ();
	static QualitySettings_get_vSyncCount_m1814762262_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (QualitySettings_get_vSyncCount_m1814762262_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.QualitySettings::get_vSyncCount()");
	return _il2cpp_icall_func();
}
// System.Int32 UnityEngine.QualitySettings::get_antiAliasing()
extern "C"  int32_t QualitySettings_get_antiAliasing_m601833413 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef int32_t (*QualitySettings_get_antiAliasing_m601833413_ftn) ();
	static QualitySettings_get_antiAliasing_m601833413_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (QualitySettings_get_antiAliasing_m601833413_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.QualitySettings::get_antiAliasing()");
	return _il2cpp_icall_func();
}
// UnityEngine.ColorSpace UnityEngine.QualitySettings::get_activeColorSpace()
extern "C"  int32_t QualitySettings_get_activeColorSpace_m1763800780 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef int32_t (*QualitySettings_get_activeColorSpace_m1763800780_ftn) ();
	static QualitySettings_get_activeColorSpace_m1763800780_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (QualitySettings_get_activeColorSpace_m1763800780_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.QualitySettings::get_activeColorSpace()");
	return _il2cpp_icall_func();
}
// System.Void UnityEngine.Quaternion::.ctor(System.Single,System.Single,System.Single,System.Single)
extern "C"  void Quaternion__ctor_m8311269 (Quaternion_t2301928331 * __this, float ___x0, float ___y1, float ___z2, float ___w3, const MethodInfo* method)
{
	{
		float L_0 = ___x0;
		__this->set_x_0(L_0);
		float L_1 = ___y1;
		__this->set_y_1(L_1);
		float L_2 = ___z2;
		__this->set_z_2(L_2);
		float L_3 = ___w3;
		__this->set_w_3(L_3);
		return;
	}
}
extern "C"  void Quaternion__ctor_m8311269_AdjustorThunk (Il2CppObject * __this, float ___x0, float ___y1, float ___z2, float ___w3, const MethodInfo* method)
{
	Quaternion_t2301928331 * _thisAdjusted = reinterpret_cast<Quaternion_t2301928331 *>(__this + 1);
	Quaternion__ctor_m8311269(_thisAdjusted, ___x0, ___y1, ___z2, ___w3, method);
}
// UnityEngine.Quaternion UnityEngine.Quaternion::AngleAxis(System.Single,UnityEngine.Vector3)
extern "C"  Quaternion_t2301928331  Quaternion_AngleAxis_m598562303 (Il2CppObject * __this /* static, unused */, float ___angle0, Vector3_t3722313464  ___axis1, const MethodInfo* method)
{
	Quaternion_t2301928331  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Quaternion_t2301928331  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		float L_0 = ___angle0;
		Quaternion_INTERNAL_CALL_AngleAxis_m2146356369(NULL /*static, unused*/, L_0, (&___axis1), (&V_0), /*hidden argument*/NULL);
		Quaternion_t2301928331  L_1 = V_0;
		V_1 = L_1;
		goto IL_0012;
	}

IL_0012:
	{
		Quaternion_t2301928331  L_2 = V_1;
		return L_2;
	}
}
// System.Void UnityEngine.Quaternion::INTERNAL_CALL_AngleAxis(System.Single,UnityEngine.Vector3&,UnityEngine.Quaternion&)
extern "C"  void Quaternion_INTERNAL_CALL_AngleAxis_m2146356369 (Il2CppObject * __this /* static, unused */, float ___angle0, Vector3_t3722313464 * ___axis1, Quaternion_t2301928331 * ___value2, const MethodInfo* method)
{
	typedef void (*Quaternion_INTERNAL_CALL_AngleAxis_m2146356369_ftn) (float, Vector3_t3722313464 *, Quaternion_t2301928331 *);
	static Quaternion_INTERNAL_CALL_AngleAxis_m2146356369_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Quaternion_INTERNAL_CALL_AngleAxis_m2146356369_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Quaternion::INTERNAL_CALL_AngleAxis(System.Single,UnityEngine.Vector3&,UnityEngine.Quaternion&)");
	_il2cpp_icall_func(___angle0, ___axis1, ___value2);
}
// System.Void UnityEngine.Quaternion::ToAngleAxis(System.Single&,UnityEngine.Vector3&)
extern "C"  void Quaternion_ToAngleAxis_m3045484821 (Quaternion_t2301928331 * __this, float* ___angle0, Vector3_t3722313464 * ___axis1, const MethodInfo* method)
{
	{
		Vector3_t3722313464 * L_0 = ___axis1;
		float* L_1 = ___angle0;
		Quaternion_Internal_ToAxisAngleRad_m1241608229(NULL /*static, unused*/, (*(Quaternion_t2301928331 *)__this), L_0, L_1, /*hidden argument*/NULL);
		float* L_2 = ___angle0;
		float* L_3 = ___angle0;
		*((float*)(L_2)) = (float)((float)((float)(*((float*)L_3))*(float)(57.29578f)));
		return;
	}
}
extern "C"  void Quaternion_ToAngleAxis_m3045484821_AdjustorThunk (Il2CppObject * __this, float* ___angle0, Vector3_t3722313464 * ___axis1, const MethodInfo* method)
{
	Quaternion_t2301928331 * _thisAdjusted = reinterpret_cast<Quaternion_t2301928331 *>(__this + 1);
	Quaternion_ToAngleAxis_m3045484821(_thisAdjusted, ___angle0, ___axis1, method);
}
// UnityEngine.Quaternion UnityEngine.Quaternion::FromToRotation(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  Quaternion_t2301928331  Quaternion_FromToRotation_m3177481454 (Il2CppObject * __this /* static, unused */, Vector3_t3722313464  ___fromDirection0, Vector3_t3722313464  ___toDirection1, const MethodInfo* method)
{
	Quaternion_t2301928331  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Quaternion_t2301928331  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		Quaternion_INTERNAL_CALL_FromToRotation_m3176884020(NULL /*static, unused*/, (&___fromDirection0), (&___toDirection1), (&V_0), /*hidden argument*/NULL);
		Quaternion_t2301928331  L_0 = V_0;
		V_1 = L_0;
		goto IL_0013;
	}

IL_0013:
	{
		Quaternion_t2301928331  L_1 = V_1;
		return L_1;
	}
}
// System.Void UnityEngine.Quaternion::INTERNAL_CALL_FromToRotation(UnityEngine.Vector3&,UnityEngine.Vector3&,UnityEngine.Quaternion&)
extern "C"  void Quaternion_INTERNAL_CALL_FromToRotation_m3176884020 (Il2CppObject * __this /* static, unused */, Vector3_t3722313464 * ___fromDirection0, Vector3_t3722313464 * ___toDirection1, Quaternion_t2301928331 * ___value2, const MethodInfo* method)
{
	typedef void (*Quaternion_INTERNAL_CALL_FromToRotation_m3176884020_ftn) (Vector3_t3722313464 *, Vector3_t3722313464 *, Quaternion_t2301928331 *);
	static Quaternion_INTERNAL_CALL_FromToRotation_m3176884020_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Quaternion_INTERNAL_CALL_FromToRotation_m3176884020_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Quaternion::INTERNAL_CALL_FromToRotation(UnityEngine.Vector3&,UnityEngine.Vector3&,UnityEngine.Quaternion&)");
	_il2cpp_icall_func(___fromDirection0, ___toDirection1, ___value2);
}
// UnityEngine.Quaternion UnityEngine.Quaternion::LookRotation(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  Quaternion_t2301928331  Quaternion_LookRotation_m3872434363 (Il2CppObject * __this /* static, unused */, Vector3_t3722313464  ___forward0, Vector3_t3722313464  ___upwards1, const MethodInfo* method)
{
	Quaternion_t2301928331  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Quaternion_t2301928331  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		Quaternion_INTERNAL_CALL_LookRotation_m1837014090(NULL /*static, unused*/, (&___forward0), (&___upwards1), (&V_0), /*hidden argument*/NULL);
		Quaternion_t2301928331  L_0 = V_0;
		V_1 = L_0;
		goto IL_0013;
	}

IL_0013:
	{
		Quaternion_t2301928331  L_1 = V_1;
		return L_1;
	}
}
// UnityEngine.Quaternion UnityEngine.Quaternion::LookRotation(UnityEngine.Vector3)
extern "C"  Quaternion_t2301928331  Quaternion_LookRotation_m3621429911 (Il2CppObject * __this /* static, unused */, Vector3_t3722313464  ___forward0, const MethodInfo* method)
{
	Vector3_t3722313464  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Quaternion_t2301928331  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Quaternion_t2301928331  V_2;
	memset(&V_2, 0, sizeof(V_2));
	{
		Vector3_t3722313464  L_0 = Vector3_get_up_m2851907508(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		Quaternion_INTERNAL_CALL_LookRotation_m1837014090(NULL /*static, unused*/, (&___forward0), (&V_0), (&V_1), /*hidden argument*/NULL);
		Quaternion_t2301928331  L_1 = V_1;
		V_2 = L_1;
		goto IL_0019;
	}

IL_0019:
	{
		Quaternion_t2301928331  L_2 = V_2;
		return L_2;
	}
}
// System.Void UnityEngine.Quaternion::INTERNAL_CALL_LookRotation(UnityEngine.Vector3&,UnityEngine.Vector3&,UnityEngine.Quaternion&)
extern "C"  void Quaternion_INTERNAL_CALL_LookRotation_m1837014090 (Il2CppObject * __this /* static, unused */, Vector3_t3722313464 * ___forward0, Vector3_t3722313464 * ___upwards1, Quaternion_t2301928331 * ___value2, const MethodInfo* method)
{
	typedef void (*Quaternion_INTERNAL_CALL_LookRotation_m1837014090_ftn) (Vector3_t3722313464 *, Vector3_t3722313464 *, Quaternion_t2301928331 *);
	static Quaternion_INTERNAL_CALL_LookRotation_m1837014090_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Quaternion_INTERNAL_CALL_LookRotation_m1837014090_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Quaternion::INTERNAL_CALL_LookRotation(UnityEngine.Vector3&,UnityEngine.Vector3&,UnityEngine.Quaternion&)");
	_il2cpp_icall_func(___forward0, ___upwards1, ___value2);
}
// UnityEngine.Quaternion UnityEngine.Quaternion::Slerp(UnityEngine.Quaternion,UnityEngine.Quaternion,System.Single)
extern "C"  Quaternion_t2301928331  Quaternion_Slerp_m1657218709 (Il2CppObject * __this /* static, unused */, Quaternion_t2301928331  ___a0, Quaternion_t2301928331  ___b1, float ___t2, const MethodInfo* method)
{
	Quaternion_t2301928331  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Quaternion_t2301928331  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		float L_0 = ___t2;
		Quaternion_INTERNAL_CALL_Slerp_m3390229344(NULL /*static, unused*/, (&___a0), (&___b1), L_0, (&V_0), /*hidden argument*/NULL);
		Quaternion_t2301928331  L_1 = V_0;
		V_1 = L_1;
		goto IL_0014;
	}

IL_0014:
	{
		Quaternion_t2301928331  L_2 = V_1;
		return L_2;
	}
}
// System.Void UnityEngine.Quaternion::INTERNAL_CALL_Slerp(UnityEngine.Quaternion&,UnityEngine.Quaternion&,System.Single,UnityEngine.Quaternion&)
extern "C"  void Quaternion_INTERNAL_CALL_Slerp_m3390229344 (Il2CppObject * __this /* static, unused */, Quaternion_t2301928331 * ___a0, Quaternion_t2301928331 * ___b1, float ___t2, Quaternion_t2301928331 * ___value3, const MethodInfo* method)
{
	typedef void (*Quaternion_INTERNAL_CALL_Slerp_m3390229344_ftn) (Quaternion_t2301928331 *, Quaternion_t2301928331 *, float, Quaternion_t2301928331 *);
	static Quaternion_INTERNAL_CALL_Slerp_m3390229344_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Quaternion_INTERNAL_CALL_Slerp_m3390229344_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Quaternion::INTERNAL_CALL_Slerp(UnityEngine.Quaternion&,UnityEngine.Quaternion&,System.Single,UnityEngine.Quaternion&)");
	_il2cpp_icall_func(___a0, ___b1, ___t2, ___value3);
}
// UnityEngine.Quaternion UnityEngine.Quaternion::Lerp(UnityEngine.Quaternion,UnityEngine.Quaternion,System.Single)
extern "C"  Quaternion_t2301928331  Quaternion_Lerp_m1168743595 (Il2CppObject * __this /* static, unused */, Quaternion_t2301928331  ___a0, Quaternion_t2301928331  ___b1, float ___t2, const MethodInfo* method)
{
	Quaternion_t2301928331  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Quaternion_t2301928331  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		float L_0 = ___t2;
		Quaternion_INTERNAL_CALL_Lerp_m1337588159(NULL /*static, unused*/, (&___a0), (&___b1), L_0, (&V_0), /*hidden argument*/NULL);
		Quaternion_t2301928331  L_1 = V_0;
		V_1 = L_1;
		goto IL_0014;
	}

IL_0014:
	{
		Quaternion_t2301928331  L_2 = V_1;
		return L_2;
	}
}
// System.Void UnityEngine.Quaternion::INTERNAL_CALL_Lerp(UnityEngine.Quaternion&,UnityEngine.Quaternion&,System.Single,UnityEngine.Quaternion&)
extern "C"  void Quaternion_INTERNAL_CALL_Lerp_m1337588159 (Il2CppObject * __this /* static, unused */, Quaternion_t2301928331 * ___a0, Quaternion_t2301928331 * ___b1, float ___t2, Quaternion_t2301928331 * ___value3, const MethodInfo* method)
{
	typedef void (*Quaternion_INTERNAL_CALL_Lerp_m1337588159_ftn) (Quaternion_t2301928331 *, Quaternion_t2301928331 *, float, Quaternion_t2301928331 *);
	static Quaternion_INTERNAL_CALL_Lerp_m1337588159_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Quaternion_INTERNAL_CALL_Lerp_m1337588159_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Quaternion::INTERNAL_CALL_Lerp(UnityEngine.Quaternion&,UnityEngine.Quaternion&,System.Single,UnityEngine.Quaternion&)");
	_il2cpp_icall_func(___a0, ___b1, ___t2, ___value3);
}
// UnityEngine.Quaternion UnityEngine.Quaternion::Inverse(UnityEngine.Quaternion)
extern "C"  Quaternion_t2301928331  Quaternion_Inverse_m2799544503 (Il2CppObject * __this /* static, unused */, Quaternion_t2301928331  ___rotation0, const MethodInfo* method)
{
	Quaternion_t2301928331  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Quaternion_t2301928331  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		Quaternion_INTERNAL_CALL_Inverse_m1744399623(NULL /*static, unused*/, (&___rotation0), (&V_0), /*hidden argument*/NULL);
		Quaternion_t2301928331  L_0 = V_0;
		V_1 = L_0;
		goto IL_0011;
	}

IL_0011:
	{
		Quaternion_t2301928331  L_1 = V_1;
		return L_1;
	}
}
// System.Void UnityEngine.Quaternion::INTERNAL_CALL_Inverse(UnityEngine.Quaternion&,UnityEngine.Quaternion&)
extern "C"  void Quaternion_INTERNAL_CALL_Inverse_m1744399623 (Il2CppObject * __this /* static, unused */, Quaternion_t2301928331 * ___rotation0, Quaternion_t2301928331 * ___value1, const MethodInfo* method)
{
	typedef void (*Quaternion_INTERNAL_CALL_Inverse_m1744399623_ftn) (Quaternion_t2301928331 *, Quaternion_t2301928331 *);
	static Quaternion_INTERNAL_CALL_Inverse_m1744399623_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Quaternion_INTERNAL_CALL_Inverse_m1744399623_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Quaternion::INTERNAL_CALL_Inverse(UnityEngine.Quaternion&,UnityEngine.Quaternion&)");
	_il2cpp_icall_func(___rotation0, ___value1);
}
// UnityEngine.Vector3 UnityEngine.Quaternion::get_eulerAngles()
extern "C"  Vector3_t3722313464  Quaternion_get_eulerAngles_m2166503968 (Quaternion_t2301928331 * __this, const MethodInfo* method)
{
	Vector3_t3722313464  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Vector3_t3722313464  L_0 = Quaternion_Internal_ToEulerRad_m3601113129(NULL /*static, unused*/, (*(Quaternion_t2301928331 *)__this), /*hidden argument*/NULL);
		Vector3_t3722313464  L_1 = Vector3_op_Multiply_m3506743150(NULL /*static, unused*/, L_0, (57.29578f), /*hidden argument*/NULL);
		Vector3_t3722313464  L_2 = Quaternion_Internal_MakePositive_m107199717(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		goto IL_0021;
	}

IL_0021:
	{
		Vector3_t3722313464  L_3 = V_0;
		return L_3;
	}
}
extern "C"  Vector3_t3722313464  Quaternion_get_eulerAngles_m2166503968_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Quaternion_t2301928331 * _thisAdjusted = reinterpret_cast<Quaternion_t2301928331 *>(__this + 1);
	return Quaternion_get_eulerAngles_m2166503968(_thisAdjusted, method);
}
// System.Void UnityEngine.Quaternion::set_eulerAngles(UnityEngine.Vector3)
extern "C"  void Quaternion_set_eulerAngles_m4032549493 (Quaternion_t2301928331 * __this, Vector3_t3722313464  ___value0, const MethodInfo* method)
{
	{
		Vector3_t3722313464  L_0 = ___value0;
		Vector3_t3722313464  L_1 = Vector3_op_Multiply_m3506743150(NULL /*static, unused*/, L_0, (0.0174532924f), /*hidden argument*/NULL);
		Quaternion_t2301928331  L_2 = Quaternion_Internal_FromEulerRad_m2596673477(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		(*(Quaternion_t2301928331 *)__this) = L_2;
		return;
	}
}
extern "C"  void Quaternion_set_eulerAngles_m4032549493_AdjustorThunk (Il2CppObject * __this, Vector3_t3722313464  ___value0, const MethodInfo* method)
{
	Quaternion_t2301928331 * _thisAdjusted = reinterpret_cast<Quaternion_t2301928331 *>(__this + 1);
	Quaternion_set_eulerAngles_m4032549493(_thisAdjusted, ___value0, method);
}
// UnityEngine.Quaternion UnityEngine.Quaternion::Euler(System.Single,System.Single,System.Single)
extern "C"  Quaternion_t2301928331  Quaternion_Euler_m955484618 (Il2CppObject * __this /* static, unused */, float ___x0, float ___y1, float ___z2, const MethodInfo* method)
{
	Quaternion_t2301928331  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		float L_0 = ___x0;
		float L_1 = ___y1;
		float L_2 = ___z2;
		Vector3_t3722313464  L_3;
		memset(&L_3, 0, sizeof(L_3));
		Vector3__ctor_m1197556204(&L_3, L_0, L_1, L_2, /*hidden argument*/NULL);
		Vector3_t3722313464  L_4 = Vector3_op_Multiply_m3506743150(NULL /*static, unused*/, L_3, (0.0174532924f), /*hidden argument*/NULL);
		Quaternion_t2301928331  L_5 = Quaternion_Internal_FromEulerRad_m2596673477(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		V_0 = L_5;
		goto IL_001e;
	}

IL_001e:
	{
		Quaternion_t2301928331  L_6 = V_0;
		return L_6;
	}
}
// UnityEngine.Quaternion UnityEngine.Quaternion::Euler(UnityEngine.Vector3)
extern "C"  Quaternion_t2301928331  Quaternion_Euler_m4079491635 (Il2CppObject * __this /* static, unused */, Vector3_t3722313464  ___euler0, const MethodInfo* method)
{
	Quaternion_t2301928331  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Vector3_t3722313464  L_0 = ___euler0;
		Vector3_t3722313464  L_1 = Vector3_op_Multiply_m3506743150(NULL /*static, unused*/, L_0, (0.0174532924f), /*hidden argument*/NULL);
		Quaternion_t2301928331  L_2 = Quaternion_Internal_FromEulerRad_m2596673477(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		goto IL_0017;
	}

IL_0017:
	{
		Quaternion_t2301928331  L_3 = V_0;
		return L_3;
	}
}
// UnityEngine.Vector3 UnityEngine.Quaternion::Internal_ToEulerRad(UnityEngine.Quaternion)
extern "C"  Vector3_t3722313464  Quaternion_Internal_ToEulerRad_m3601113129 (Il2CppObject * __this /* static, unused */, Quaternion_t2301928331  ___rotation0, const MethodInfo* method)
{
	Vector3_t3722313464  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector3_t3722313464  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		Quaternion_INTERNAL_CALL_Internal_ToEulerRad_m3918304901(NULL /*static, unused*/, (&___rotation0), (&V_0), /*hidden argument*/NULL);
		Vector3_t3722313464  L_0 = V_0;
		V_1 = L_0;
		goto IL_0011;
	}

IL_0011:
	{
		Vector3_t3722313464  L_1 = V_1;
		return L_1;
	}
}
// System.Void UnityEngine.Quaternion::INTERNAL_CALL_Internal_ToEulerRad(UnityEngine.Quaternion&,UnityEngine.Vector3&)
extern "C"  void Quaternion_INTERNAL_CALL_Internal_ToEulerRad_m3918304901 (Il2CppObject * __this /* static, unused */, Quaternion_t2301928331 * ___rotation0, Vector3_t3722313464 * ___value1, const MethodInfo* method)
{
	typedef void (*Quaternion_INTERNAL_CALL_Internal_ToEulerRad_m3918304901_ftn) (Quaternion_t2301928331 *, Vector3_t3722313464 *);
	static Quaternion_INTERNAL_CALL_Internal_ToEulerRad_m3918304901_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Quaternion_INTERNAL_CALL_Internal_ToEulerRad_m3918304901_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Quaternion::INTERNAL_CALL_Internal_ToEulerRad(UnityEngine.Quaternion&,UnityEngine.Vector3&)");
	_il2cpp_icall_func(___rotation0, ___value1);
}
// UnityEngine.Quaternion UnityEngine.Quaternion::Internal_FromEulerRad(UnityEngine.Vector3)
extern "C"  Quaternion_t2301928331  Quaternion_Internal_FromEulerRad_m2596673477 (Il2CppObject * __this /* static, unused */, Vector3_t3722313464  ___euler0, const MethodInfo* method)
{
	Quaternion_t2301928331  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Quaternion_t2301928331  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		Quaternion_INTERNAL_CALL_Internal_FromEulerRad_m1327416487(NULL /*static, unused*/, (&___euler0), (&V_0), /*hidden argument*/NULL);
		Quaternion_t2301928331  L_0 = V_0;
		V_1 = L_0;
		goto IL_0011;
	}

IL_0011:
	{
		Quaternion_t2301928331  L_1 = V_1;
		return L_1;
	}
}
// System.Void UnityEngine.Quaternion::INTERNAL_CALL_Internal_FromEulerRad(UnityEngine.Vector3&,UnityEngine.Quaternion&)
extern "C"  void Quaternion_INTERNAL_CALL_Internal_FromEulerRad_m1327416487 (Il2CppObject * __this /* static, unused */, Vector3_t3722313464 * ___euler0, Quaternion_t2301928331 * ___value1, const MethodInfo* method)
{
	typedef void (*Quaternion_INTERNAL_CALL_Internal_FromEulerRad_m1327416487_ftn) (Vector3_t3722313464 *, Quaternion_t2301928331 *);
	static Quaternion_INTERNAL_CALL_Internal_FromEulerRad_m1327416487_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Quaternion_INTERNAL_CALL_Internal_FromEulerRad_m1327416487_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Quaternion::INTERNAL_CALL_Internal_FromEulerRad(UnityEngine.Vector3&,UnityEngine.Quaternion&)");
	_il2cpp_icall_func(___euler0, ___value1);
}
// System.Void UnityEngine.Quaternion::Internal_ToAxisAngleRad(UnityEngine.Quaternion,UnityEngine.Vector3&,System.Single&)
extern "C"  void Quaternion_Internal_ToAxisAngleRad_m1241608229 (Il2CppObject * __this /* static, unused */, Quaternion_t2301928331  ___q0, Vector3_t3722313464 * ___axis1, float* ___angle2, const MethodInfo* method)
{
	{
		Vector3_t3722313464 * L_0 = ___axis1;
		float* L_1 = ___angle2;
		Quaternion_INTERNAL_CALL_Internal_ToAxisAngleRad_m2865891006(NULL /*static, unused*/, (&___q0), L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Quaternion::INTERNAL_CALL_Internal_ToAxisAngleRad(UnityEngine.Quaternion&,UnityEngine.Vector3&,System.Single&)
extern "C"  void Quaternion_INTERNAL_CALL_Internal_ToAxisAngleRad_m2865891006 (Il2CppObject * __this /* static, unused */, Quaternion_t2301928331 * ___q0, Vector3_t3722313464 * ___axis1, float* ___angle2, const MethodInfo* method)
{
	typedef void (*Quaternion_INTERNAL_CALL_Internal_ToAxisAngleRad_m2865891006_ftn) (Quaternion_t2301928331 *, Vector3_t3722313464 *, float*);
	static Quaternion_INTERNAL_CALL_Internal_ToAxisAngleRad_m2865891006_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Quaternion_INTERNAL_CALL_Internal_ToAxisAngleRad_m2865891006_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Quaternion::INTERNAL_CALL_Internal_ToAxisAngleRad(UnityEngine.Quaternion&,UnityEngine.Vector3&,System.Single&)");
	_il2cpp_icall_func(___q0, ___axis1, ___angle2);
}
// UnityEngine.Quaternion UnityEngine.Quaternion::get_identity()
extern "C"  Quaternion_t2301928331  Quaternion_get_identity_m3578010038 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	Quaternion_t2301928331  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Quaternion_t2301928331  L_0;
		memset(&L_0, 0, sizeof(L_0));
		Quaternion__ctor_m8311269(&L_0, (0.0f), (0.0f), (0.0f), (1.0f), /*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_0020;
	}

IL_0020:
	{
		Quaternion_t2301928331  L_1 = V_0;
		return L_1;
	}
}
// UnityEngine.Quaternion UnityEngine.Quaternion::op_Multiply(UnityEngine.Quaternion,UnityEngine.Quaternion)
extern "C"  Quaternion_t2301928331  Quaternion_op_Multiply_m1827210722 (Il2CppObject * __this /* static, unused */, Quaternion_t2301928331  ___lhs0, Quaternion_t2301928331  ___rhs1, const MethodInfo* method)
{
	Quaternion_t2301928331  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		float L_0 = (&___lhs0)->get_w_3();
		float L_1 = (&___rhs1)->get_x_0();
		float L_2 = (&___lhs0)->get_x_0();
		float L_3 = (&___rhs1)->get_w_3();
		float L_4 = (&___lhs0)->get_y_1();
		float L_5 = (&___rhs1)->get_z_2();
		float L_6 = (&___lhs0)->get_z_2();
		float L_7 = (&___rhs1)->get_y_1();
		float L_8 = (&___lhs0)->get_w_3();
		float L_9 = (&___rhs1)->get_y_1();
		float L_10 = (&___lhs0)->get_y_1();
		float L_11 = (&___rhs1)->get_w_3();
		float L_12 = (&___lhs0)->get_z_2();
		float L_13 = (&___rhs1)->get_x_0();
		float L_14 = (&___lhs0)->get_x_0();
		float L_15 = (&___rhs1)->get_z_2();
		float L_16 = (&___lhs0)->get_w_3();
		float L_17 = (&___rhs1)->get_z_2();
		float L_18 = (&___lhs0)->get_z_2();
		float L_19 = (&___rhs1)->get_w_3();
		float L_20 = (&___lhs0)->get_x_0();
		float L_21 = (&___rhs1)->get_y_1();
		float L_22 = (&___lhs0)->get_y_1();
		float L_23 = (&___rhs1)->get_x_0();
		float L_24 = (&___lhs0)->get_w_3();
		float L_25 = (&___rhs1)->get_w_3();
		float L_26 = (&___lhs0)->get_x_0();
		float L_27 = (&___rhs1)->get_x_0();
		float L_28 = (&___lhs0)->get_y_1();
		float L_29 = (&___rhs1)->get_y_1();
		float L_30 = (&___lhs0)->get_z_2();
		float L_31 = (&___rhs1)->get_z_2();
		Quaternion_t2301928331  L_32;
		memset(&L_32, 0, sizeof(L_32));
		Quaternion__ctor_m8311269(&L_32, ((float)((float)((float)((float)((float)((float)((float)((float)L_0*(float)L_1))+(float)((float)((float)L_2*(float)L_3))))+(float)((float)((float)L_4*(float)L_5))))-(float)((float)((float)L_6*(float)L_7)))), ((float)((float)((float)((float)((float)((float)((float)((float)L_8*(float)L_9))+(float)((float)((float)L_10*(float)L_11))))+(float)((float)((float)L_12*(float)L_13))))-(float)((float)((float)L_14*(float)L_15)))), ((float)((float)((float)((float)((float)((float)((float)((float)L_16*(float)L_17))+(float)((float)((float)L_18*(float)L_19))))+(float)((float)((float)L_20*(float)L_21))))-(float)((float)((float)L_22*(float)L_23)))), ((float)((float)((float)((float)((float)((float)((float)((float)L_24*(float)L_25))-(float)((float)((float)L_26*(float)L_27))))-(float)((float)((float)L_28*(float)L_29))))-(float)((float)((float)L_30*(float)L_31)))), /*hidden argument*/NULL);
		V_0 = L_32;
		goto IL_0108;
	}

IL_0108:
	{
		Quaternion_t2301928331  L_33 = V_0;
		return L_33;
	}
}
// UnityEngine.Vector3 UnityEngine.Quaternion::op_Multiply(UnityEngine.Quaternion,UnityEngine.Vector3)
extern "C"  Vector3_t3722313464  Quaternion_op_Multiply_m1022106983 (Il2CppObject * __this /* static, unused */, Quaternion_t2301928331  ___rotation0, Vector3_t3722313464  ___point1, const MethodInfo* method)
{
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	float V_3 = 0.0f;
	float V_4 = 0.0f;
	float V_5 = 0.0f;
	float V_6 = 0.0f;
	float V_7 = 0.0f;
	float V_8 = 0.0f;
	float V_9 = 0.0f;
	float V_10 = 0.0f;
	float V_11 = 0.0f;
	Vector3_t3722313464  V_12;
	memset(&V_12, 0, sizeof(V_12));
	Vector3_t3722313464  V_13;
	memset(&V_13, 0, sizeof(V_13));
	{
		float L_0 = (&___rotation0)->get_x_0();
		V_0 = ((float)((float)L_0*(float)(2.0f)));
		float L_1 = (&___rotation0)->get_y_1();
		V_1 = ((float)((float)L_1*(float)(2.0f)));
		float L_2 = (&___rotation0)->get_z_2();
		V_2 = ((float)((float)L_2*(float)(2.0f)));
		float L_3 = (&___rotation0)->get_x_0();
		float L_4 = V_0;
		V_3 = ((float)((float)L_3*(float)L_4));
		float L_5 = (&___rotation0)->get_y_1();
		float L_6 = V_1;
		V_4 = ((float)((float)L_5*(float)L_6));
		float L_7 = (&___rotation0)->get_z_2();
		float L_8 = V_2;
		V_5 = ((float)((float)L_7*(float)L_8));
		float L_9 = (&___rotation0)->get_x_0();
		float L_10 = V_1;
		V_6 = ((float)((float)L_9*(float)L_10));
		float L_11 = (&___rotation0)->get_x_0();
		float L_12 = V_2;
		V_7 = ((float)((float)L_11*(float)L_12));
		float L_13 = (&___rotation0)->get_y_1();
		float L_14 = V_2;
		V_8 = ((float)((float)L_13*(float)L_14));
		float L_15 = (&___rotation0)->get_w_3();
		float L_16 = V_0;
		V_9 = ((float)((float)L_15*(float)L_16));
		float L_17 = (&___rotation0)->get_w_3();
		float L_18 = V_1;
		V_10 = ((float)((float)L_17*(float)L_18));
		float L_19 = (&___rotation0)->get_w_3();
		float L_20 = V_2;
		V_11 = ((float)((float)L_19*(float)L_20));
		float L_21 = V_4;
		float L_22 = V_5;
		float L_23 = (&___point1)->get_x_1();
		float L_24 = V_6;
		float L_25 = V_11;
		float L_26 = (&___point1)->get_y_2();
		float L_27 = V_7;
		float L_28 = V_10;
		float L_29 = (&___point1)->get_z_3();
		(&V_12)->set_x_1(((float)((float)((float)((float)((float)((float)((float)((float)(1.0f)-(float)((float)((float)L_21+(float)L_22))))*(float)L_23))+(float)((float)((float)((float)((float)L_24-(float)L_25))*(float)L_26))))+(float)((float)((float)((float)((float)L_27+(float)L_28))*(float)L_29)))));
		float L_30 = V_6;
		float L_31 = V_11;
		float L_32 = (&___point1)->get_x_1();
		float L_33 = V_3;
		float L_34 = V_5;
		float L_35 = (&___point1)->get_y_2();
		float L_36 = V_8;
		float L_37 = V_9;
		float L_38 = (&___point1)->get_z_3();
		(&V_12)->set_y_2(((float)((float)((float)((float)((float)((float)((float)((float)L_30+(float)L_31))*(float)L_32))+(float)((float)((float)((float)((float)(1.0f)-(float)((float)((float)L_33+(float)L_34))))*(float)L_35))))+(float)((float)((float)((float)((float)L_36-(float)L_37))*(float)L_38)))));
		float L_39 = V_7;
		float L_40 = V_10;
		float L_41 = (&___point1)->get_x_1();
		float L_42 = V_8;
		float L_43 = V_9;
		float L_44 = (&___point1)->get_y_2();
		float L_45 = V_3;
		float L_46 = V_4;
		float L_47 = (&___point1)->get_z_3();
		(&V_12)->set_z_3(((float)((float)((float)((float)((float)((float)((float)((float)L_39-(float)L_40))*(float)L_41))+(float)((float)((float)((float)((float)L_42+(float)L_43))*(float)L_44))))+(float)((float)((float)((float)((float)(1.0f)-(float)((float)((float)L_45+(float)L_46))))*(float)L_47)))));
		Vector3_t3722313464  L_48 = V_12;
		V_13 = L_48;
		goto IL_0136;
	}

IL_0136:
	{
		Vector3_t3722313464  L_49 = V_13;
		return L_49;
	}
}
// System.Boolean UnityEngine.Quaternion::op_Equality(UnityEngine.Quaternion,UnityEngine.Quaternion)
extern "C"  bool Quaternion_op_Equality_m1820728316 (Il2CppObject * __this /* static, unused */, Quaternion_t2301928331  ___lhs0, Quaternion_t2301928331  ___rhs1, const MethodInfo* method)
{
	bool V_0 = false;
	{
		Quaternion_t2301928331  L_0 = ___lhs0;
		Quaternion_t2301928331  L_1 = ___rhs1;
		float L_2 = Quaternion_Dot_m3405174114(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		V_0 = (bool)((((float)L_2) > ((float)(0.999999f)))? 1 : 0);
		goto IL_0015;
	}

IL_0015:
	{
		bool L_3 = V_0;
		return L_3;
	}
}
// System.Boolean UnityEngine.Quaternion::op_Inequality(UnityEngine.Quaternion,UnityEngine.Quaternion)
extern "C"  bool Quaternion_op_Inequality_m1226915970 (Il2CppObject * __this /* static, unused */, Quaternion_t2301928331  ___lhs0, Quaternion_t2301928331  ___rhs1, const MethodInfo* method)
{
	bool V_0 = false;
	{
		Quaternion_t2301928331  L_0 = ___lhs0;
		Quaternion_t2301928331  L_1 = ___rhs1;
		bool L_2 = Quaternion_op_Equality_m1820728316(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		V_0 = (bool)((((int32_t)L_2) == ((int32_t)0))? 1 : 0);
		goto IL_0011;
	}

IL_0011:
	{
		bool L_3 = V_0;
		return L_3;
	}
}
// System.Single UnityEngine.Quaternion::Dot(UnityEngine.Quaternion,UnityEngine.Quaternion)
extern "C"  float Quaternion_Dot_m3405174114 (Il2CppObject * __this /* static, unused */, Quaternion_t2301928331  ___a0, Quaternion_t2301928331  ___b1, const MethodInfo* method)
{
	float V_0 = 0.0f;
	{
		float L_0 = (&___a0)->get_x_0();
		float L_1 = (&___b1)->get_x_0();
		float L_2 = (&___a0)->get_y_1();
		float L_3 = (&___b1)->get_y_1();
		float L_4 = (&___a0)->get_z_2();
		float L_5 = (&___b1)->get_z_2();
		float L_6 = (&___a0)->get_w_3();
		float L_7 = (&___b1)->get_w_3();
		V_0 = ((float)((float)((float)((float)((float)((float)((float)((float)L_0*(float)L_1))+(float)((float)((float)L_2*(float)L_3))))+(float)((float)((float)L_4*(float)L_5))))+(float)((float)((float)L_6*(float)L_7))));
		goto IL_0046;
	}

IL_0046:
	{
		float L_8 = V_0;
		return L_8;
	}
}
// System.Single UnityEngine.Quaternion::Angle(UnityEngine.Quaternion,UnityEngine.Quaternion)
extern "C"  float Quaternion_Angle_m4174488882 (Il2CppObject * __this /* static, unused */, Quaternion_t2301928331  ___a0, Quaternion_t2301928331  ___b1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Quaternion_Angle_m4174488882_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	{
		Quaternion_t2301928331  L_0 = ___a0;
		Quaternion_t2301928331  L_1 = ___b1;
		float L_2 = Quaternion_Dot_m3405174114(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		float L_3 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t3464937446_il2cpp_TypeInfo_var);
		float L_4 = fabsf(L_3);
		float L_5 = Mathf_Min_m1901831667(NULL /*static, unused*/, L_4, (1.0f), /*hidden argument*/NULL);
		float L_6 = acosf(L_5);
		V_1 = ((float)((float)((float)((float)L_6*(float)(2.0f)))*(float)(57.29578f)));
		goto IL_0030;
	}

IL_0030:
	{
		float L_7 = V_1;
		return L_7;
	}
}
// UnityEngine.Vector3 UnityEngine.Quaternion::Internal_MakePositive(UnityEngine.Vector3)
extern "C"  Vector3_t3722313464  Quaternion_Internal_MakePositive_m107199717 (Il2CppObject * __this /* static, unused */, Vector3_t3722313464  ___euler0, const MethodInfo* method)
{
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	Vector3_t3722313464  V_2;
	memset(&V_2, 0, sizeof(V_2));
	{
		V_0 = (-0.005729578f);
		float L_0 = V_0;
		V_1 = ((float)((float)(360.0f)+(float)L_0));
		float L_1 = (&___euler0)->get_x_1();
		float L_2 = V_0;
		if ((!(((float)L_1) < ((float)L_2))))
		{
			goto IL_0034;
		}
	}
	{
		Vector3_t3722313464 * L_3 = (&___euler0);
		float L_4 = L_3->get_x_1();
		L_3->set_x_1(((float)((float)L_4+(float)(360.0f))));
		goto IL_0054;
	}

IL_0034:
	{
		float L_5 = (&___euler0)->get_x_1();
		float L_6 = V_1;
		if ((!(((float)L_5) > ((float)L_6))))
		{
			goto IL_0054;
		}
	}
	{
		Vector3_t3722313464 * L_7 = (&___euler0);
		float L_8 = L_7->get_x_1();
		L_7->set_x_1(((float)((float)L_8-(float)(360.0f))));
	}

IL_0054:
	{
		float L_9 = (&___euler0)->get_y_2();
		float L_10 = V_0;
		if ((!(((float)L_9) < ((float)L_10))))
		{
			goto IL_0079;
		}
	}
	{
		Vector3_t3722313464 * L_11 = (&___euler0);
		float L_12 = L_11->get_y_2();
		L_11->set_y_2(((float)((float)L_12+(float)(360.0f))));
		goto IL_0099;
	}

IL_0079:
	{
		float L_13 = (&___euler0)->get_y_2();
		float L_14 = V_1;
		if ((!(((float)L_13) > ((float)L_14))))
		{
			goto IL_0099;
		}
	}
	{
		Vector3_t3722313464 * L_15 = (&___euler0);
		float L_16 = L_15->get_y_2();
		L_15->set_y_2(((float)((float)L_16-(float)(360.0f))));
	}

IL_0099:
	{
		float L_17 = (&___euler0)->get_z_3();
		float L_18 = V_0;
		if ((!(((float)L_17) < ((float)L_18))))
		{
			goto IL_00be;
		}
	}
	{
		Vector3_t3722313464 * L_19 = (&___euler0);
		float L_20 = L_19->get_z_3();
		L_19->set_z_3(((float)((float)L_20+(float)(360.0f))));
		goto IL_00de;
	}

IL_00be:
	{
		float L_21 = (&___euler0)->get_z_3();
		float L_22 = V_1;
		if ((!(((float)L_21) > ((float)L_22))))
		{
			goto IL_00de;
		}
	}
	{
		Vector3_t3722313464 * L_23 = (&___euler0);
		float L_24 = L_23->get_z_3();
		L_23->set_z_3(((float)((float)L_24-(float)(360.0f))));
	}

IL_00de:
	{
		Vector3_t3722313464  L_25 = ___euler0;
		V_2 = L_25;
		goto IL_00e5;
	}

IL_00e5:
	{
		Vector3_t3722313464  L_26 = V_2;
		return L_26;
	}
}
// System.Int32 UnityEngine.Quaternion::GetHashCode()
extern "C"  int32_t Quaternion_GetHashCode_m3399187295 (Quaternion_t2301928331 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		float* L_0 = __this->get_address_of_x_0();
		int32_t L_1 = Single_GetHashCode_m1558506138(L_0, /*hidden argument*/NULL);
		float* L_2 = __this->get_address_of_y_1();
		int32_t L_3 = Single_GetHashCode_m1558506138(L_2, /*hidden argument*/NULL);
		float* L_4 = __this->get_address_of_z_2();
		int32_t L_5 = Single_GetHashCode_m1558506138(L_4, /*hidden argument*/NULL);
		float* L_6 = __this->get_address_of_w_3();
		int32_t L_7 = Single_GetHashCode_m1558506138(L_6, /*hidden argument*/NULL);
		V_0 = ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_1^(int32_t)((int32_t)((int32_t)L_3<<(int32_t)2))))^(int32_t)((int32_t)((int32_t)L_5>>(int32_t)2))))^(int32_t)((int32_t)((int32_t)L_7>>(int32_t)1))));
		goto IL_0054;
	}

IL_0054:
	{
		int32_t L_8 = V_0;
		return L_8;
	}
}
extern "C"  int32_t Quaternion_GetHashCode_m3399187295_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Quaternion_t2301928331 * _thisAdjusted = reinterpret_cast<Quaternion_t2301928331 *>(__this + 1);
	return Quaternion_GetHashCode_m3399187295(_thisAdjusted, method);
}
// System.Boolean UnityEngine.Quaternion::Equals(System.Object)
extern "C"  bool Quaternion_Equals_m1217639185 (Quaternion_t2301928331 * __this, Il2CppObject * ___other0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Quaternion_Equals_m1217639185_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	Quaternion_t2301928331  V_1;
	memset(&V_1, 0, sizeof(V_1));
	int32_t G_B7_0 = 0;
	{
		Il2CppObject * L_0 = ___other0;
		if (((Il2CppObject *)IsInstSealed(L_0, Quaternion_t2301928331_il2cpp_TypeInfo_var)))
		{
			goto IL_0013;
		}
	}
	{
		V_0 = (bool)0;
		goto IL_007a;
	}

IL_0013:
	{
		Il2CppObject * L_1 = ___other0;
		V_1 = ((*(Quaternion_t2301928331 *)((Quaternion_t2301928331 *)UnBox(L_1, Quaternion_t2301928331_il2cpp_TypeInfo_var))));
		float* L_2 = __this->get_address_of_x_0();
		float L_3 = (&V_1)->get_x_0();
		bool L_4 = Single_Equals_m1601893879(L_2, L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0073;
		}
	}
	{
		float* L_5 = __this->get_address_of_y_1();
		float L_6 = (&V_1)->get_y_1();
		bool L_7 = Single_Equals_m1601893879(L_5, L_6, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0073;
		}
	}
	{
		float* L_8 = __this->get_address_of_z_2();
		float L_9 = (&V_1)->get_z_2();
		bool L_10 = Single_Equals_m1601893879(L_8, L_9, /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_0073;
		}
	}
	{
		float* L_11 = __this->get_address_of_w_3();
		float L_12 = (&V_1)->get_w_3();
		bool L_13 = Single_Equals_m1601893879(L_11, L_12, /*hidden argument*/NULL);
		G_B7_0 = ((int32_t)(L_13));
		goto IL_0074;
	}

IL_0073:
	{
		G_B7_0 = 0;
	}

IL_0074:
	{
		V_0 = (bool)G_B7_0;
		goto IL_007a;
	}

IL_007a:
	{
		bool L_14 = V_0;
		return L_14;
	}
}
extern "C"  bool Quaternion_Equals_m1217639185_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___other0, const MethodInfo* method)
{
	Quaternion_t2301928331 * _thisAdjusted = reinterpret_cast<Quaternion_t2301928331 *>(__this + 1);
	return Quaternion_Equals_m1217639185(_thisAdjusted, ___other0, method);
}
// System.String UnityEngine.Quaternion::ToString()
extern "C"  String_t* Quaternion_ToString_m127166294 (Quaternion_t2301928331 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Quaternion_ToString_m127166294_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	{
		ObjectU5BU5D_t2843939325* L_0 = ((ObjectU5BU5D_t2843939325*)SZArrayNew(ObjectU5BU5D_t2843939325_il2cpp_TypeInfo_var, (uint32_t)4));
		float L_1 = __this->get_x_0();
		float L_2 = L_1;
		Il2CppObject * L_3 = Box(Single_t1397266774_il2cpp_TypeInfo_var, &L_2);
		NullCheck(L_0);
		ArrayElementTypeCheck (L_0, L_3);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_3);
		ObjectU5BU5D_t2843939325* L_4 = L_0;
		float L_5 = __this->get_y_1();
		float L_6 = L_5;
		Il2CppObject * L_7 = Box(Single_t1397266774_il2cpp_TypeInfo_var, &L_6);
		NullCheck(L_4);
		ArrayElementTypeCheck (L_4, L_7);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_7);
		ObjectU5BU5D_t2843939325* L_8 = L_4;
		float L_9 = __this->get_z_2();
		float L_10 = L_9;
		Il2CppObject * L_11 = Box(Single_t1397266774_il2cpp_TypeInfo_var, &L_10);
		NullCheck(L_8);
		ArrayElementTypeCheck (L_8, L_11);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)L_11);
		ObjectU5BU5D_t2843939325* L_12 = L_8;
		float L_13 = __this->get_w_3();
		float L_14 = L_13;
		Il2CppObject * L_15 = Box(Single_t1397266774_il2cpp_TypeInfo_var, &L_14);
		NullCheck(L_12);
		ArrayElementTypeCheck (L_12, L_15);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_15);
		String_t* L_16 = UnityString_Format_m3741272017(NULL /*static, unused*/, _stringLiteral3651359435, L_12, /*hidden argument*/NULL);
		V_0 = L_16;
		goto IL_004f;
	}

IL_004f:
	{
		String_t* L_17 = V_0;
		return L_17;
	}
}
extern "C"  String_t* Quaternion_ToString_m127166294_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Quaternion_t2301928331 * _thisAdjusted = reinterpret_cast<Quaternion_t2301928331 *>(__this + 1);
	return Quaternion_ToString_m127166294(_thisAdjusted, method);
}
// System.Single UnityEngine.Random::Range(System.Single,System.Single)
extern "C"  float Random_Range_m2870227214 (Il2CppObject * __this /* static, unused */, float ___min0, float ___max1, const MethodInfo* method)
{
	typedef float (*Random_Range_m2870227214_ftn) (float, float);
	static Random_Range_m2870227214_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Random_Range_m2870227214_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Random::Range(System.Single,System.Single)");
	return _il2cpp_icall_func(___min0, ___max1);
}
// System.Int32 UnityEngine.Random::Range(System.Int32,System.Int32)
extern "C"  int32_t Random_Range_m110872341 (Il2CppObject * __this /* static, unused */, int32_t ___min0, int32_t ___max1, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = ___min0;
		int32_t L_1 = ___max1;
		int32_t L_2 = Random_RandomRangeInt_m3922158605(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		goto IL_000e;
	}

IL_000e:
	{
		int32_t L_3 = V_0;
		return L_3;
	}
}
// System.Int32 UnityEngine.Random::RandomRangeInt(System.Int32,System.Int32)
extern "C"  int32_t Random_RandomRangeInt_m3922158605 (Il2CppObject * __this /* static, unused */, int32_t ___min0, int32_t ___max1, const MethodInfo* method)
{
	typedef int32_t (*Random_RandomRangeInt_m3922158605_ftn) (int32_t, int32_t);
	static Random_RandomRangeInt_m3922158605_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Random_RandomRangeInt_m3922158605_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Random::RandomRangeInt(System.Int32,System.Int32)");
	return _il2cpp_icall_func(___min0, ___max1);
}
// System.Single UnityEngine.Random::get_value()
extern "C"  float Random_get_value_m712423571 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef float (*Random_get_value_m712423571_ftn) ();
	static Random_get_value_m712423571_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Random_get_value_m712423571_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Random::get_value()");
	return _il2cpp_icall_func();
}
// UnityEngine.Vector3 UnityEngine.Random::get_insideUnitSphere()
extern "C"  Vector3_t3722313464  Random_get_insideUnitSphere_m1640908543 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	Vector3_t3722313464  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector3_t3722313464  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		Random_INTERNAL_get_insideUnitSphere_m2096637322(NULL /*static, unused*/, (&V_0), /*hidden argument*/NULL);
		Vector3_t3722313464  L_0 = V_0;
		V_1 = L_0;
		goto IL_000f;
	}

IL_000f:
	{
		Vector3_t3722313464  L_1 = V_1;
		return L_1;
	}
}
// System.Void UnityEngine.Random::INTERNAL_get_insideUnitSphere(UnityEngine.Vector3&)
extern "C"  void Random_INTERNAL_get_insideUnitSphere_m2096637322 (Il2CppObject * __this /* static, unused */, Vector3_t3722313464 * ___value0, const MethodInfo* method)
{
	typedef void (*Random_INTERNAL_get_insideUnitSphere_m2096637322_ftn) (Vector3_t3722313464 *);
	static Random_INTERNAL_get_insideUnitSphere_m2096637322_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Random_INTERNAL_get_insideUnitSphere_m2096637322_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Random::INTERNAL_get_insideUnitSphere(UnityEngine.Vector3&)");
	_il2cpp_icall_func(___value0);
}
// System.Void UnityEngine.RangeAttribute::.ctor(System.Single,System.Single)
extern "C"  void RangeAttribute__ctor_m3938290347 (RangeAttribute_t3337244227 * __this, float ___min0, float ___max1, const MethodInfo* method)
{
	{
		PropertyAttribute__ctor_m3954845021(__this, /*hidden argument*/NULL);
		float L_0 = ___min0;
		__this->set_min_0(L_0);
		float L_1 = ___max1;
		__this->set_max_1(L_1);
		return;
	}
}
// System.Int32 UnityEngine.RangeInt::get_end()
extern "C"  int32_t RangeInt_get_end_m2761784640 (RangeInt_t2094684618 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_start_0();
		int32_t L_1 = __this->get_length_1();
		V_0 = ((int32_t)((int32_t)L_0+(int32_t)L_1));
		goto IL_0014;
	}

IL_0014:
	{
		int32_t L_2 = V_0;
		return L_2;
	}
}
extern "C"  int32_t RangeInt_get_end_m2761784640_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	RangeInt_t2094684618 * _thisAdjusted = reinterpret_cast<RangeInt_t2094684618 *>(__this + 1);
	return RangeInt_get_end_m2761784640(_thisAdjusted, method);
}
// System.Void UnityEngine.Ray::.ctor(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  void Ray__ctor_m2095760679 (Ray_t3785851493 * __this, Vector3_t3722313464  ___origin0, Vector3_t3722313464  ___direction1, const MethodInfo* method)
{
	{
		Vector3_t3722313464  L_0 = ___origin0;
		__this->set_m_Origin_0(L_0);
		Vector3_t3722313464  L_1 = Vector3_get_normalized_m1684899259((&___direction1), /*hidden argument*/NULL);
		__this->set_m_Direction_1(L_1);
		return;
	}
}
extern "C"  void Ray__ctor_m2095760679_AdjustorThunk (Il2CppObject * __this, Vector3_t3722313464  ___origin0, Vector3_t3722313464  ___direction1, const MethodInfo* method)
{
	Ray_t3785851493 * _thisAdjusted = reinterpret_cast<Ray_t3785851493 *>(__this + 1);
	Ray__ctor_m2095760679(_thisAdjusted, ___origin0, ___direction1, method);
}
// UnityEngine.Vector3 UnityEngine.Ray::get_origin()
extern "C"  Vector3_t3722313464  Ray_get_origin_m4290253200 (Ray_t3785851493 * __this, const MethodInfo* method)
{
	Vector3_t3722313464  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Vector3_t3722313464  L_0 = __this->get_m_Origin_0();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		Vector3_t3722313464  L_1 = V_0;
		return L_1;
	}
}
extern "C"  Vector3_t3722313464  Ray_get_origin_m4290253200_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Ray_t3785851493 * _thisAdjusted = reinterpret_cast<Ray_t3785851493 *>(__this + 1);
	return Ray_get_origin_m4290253200(_thisAdjusted, method);
}
// System.Void UnityEngine.Ray::set_origin(UnityEngine.Vector3)
extern "C"  void Ray_set_origin_m1444122105 (Ray_t3785851493 * __this, Vector3_t3722313464  ___value0, const MethodInfo* method)
{
	{
		Vector3_t3722313464  L_0 = ___value0;
		__this->set_m_Origin_0(L_0);
		return;
	}
}
extern "C"  void Ray_set_origin_m1444122105_AdjustorThunk (Il2CppObject * __this, Vector3_t3722313464  ___value0, const MethodInfo* method)
{
	Ray_t3785851493 * _thisAdjusted = reinterpret_cast<Ray_t3785851493 *>(__this + 1);
	Ray_set_origin_m1444122105(_thisAdjusted, ___value0, method);
}
// UnityEngine.Vector3 UnityEngine.Ray::get_direction()
extern "C"  Vector3_t3722313464  Ray_get_direction_m1991692996 (Ray_t3785851493 * __this, const MethodInfo* method)
{
	Vector3_t3722313464  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Vector3_t3722313464  L_0 = __this->get_m_Direction_1();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		Vector3_t3722313464  L_1 = V_0;
		return L_1;
	}
}
extern "C"  Vector3_t3722313464  Ray_get_direction_m1991692996_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Ray_t3785851493 * _thisAdjusted = reinterpret_cast<Ray_t3785851493 *>(__this + 1);
	return Ray_get_direction_m1991692996(_thisAdjusted, method);
}
// UnityEngine.Vector3 UnityEngine.Ray::GetPoint(System.Single)
extern "C"  Vector3_t3722313464  Ray_GetPoint_m2674993148 (Ray_t3785851493 * __this, float ___distance0, const MethodInfo* method)
{
	Vector3_t3722313464  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Vector3_t3722313464  L_0 = __this->get_m_Origin_0();
		Vector3_t3722313464  L_1 = __this->get_m_Direction_1();
		float L_2 = ___distance0;
		Vector3_t3722313464  L_3 = Vector3_op_Multiply_m3506743150(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		Vector3_t3722313464  L_4 = Vector3_op_Addition_m1781942663(NULL /*static, unused*/, L_0, L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		goto IL_001e;
	}

IL_001e:
	{
		Vector3_t3722313464  L_5 = V_0;
		return L_5;
	}
}
extern "C"  Vector3_t3722313464  Ray_GetPoint_m2674993148_AdjustorThunk (Il2CppObject * __this, float ___distance0, const MethodInfo* method)
{
	Ray_t3785851493 * _thisAdjusted = reinterpret_cast<Ray_t3785851493 *>(__this + 1);
	return Ray_GetPoint_m2674993148(_thisAdjusted, ___distance0, method);
}
// System.String UnityEngine.Ray::ToString()
extern "C"  String_t* Ray_ToString_m943070480 (Ray_t3785851493 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Ray_ToString_m943070480_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	{
		ObjectU5BU5D_t2843939325* L_0 = ((ObjectU5BU5D_t2843939325*)SZArrayNew(ObjectU5BU5D_t2843939325_il2cpp_TypeInfo_var, (uint32_t)2));
		Vector3_t3722313464  L_1 = __this->get_m_Origin_0();
		Vector3_t3722313464  L_2 = L_1;
		Il2CppObject * L_3 = Box(Vector3_t3722313464_il2cpp_TypeInfo_var, &L_2);
		NullCheck(L_0);
		ArrayElementTypeCheck (L_0, L_3);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_3);
		ObjectU5BU5D_t2843939325* L_4 = L_0;
		Vector3_t3722313464  L_5 = __this->get_m_Direction_1();
		Vector3_t3722313464  L_6 = L_5;
		Il2CppObject * L_7 = Box(Vector3_t3722313464_il2cpp_TypeInfo_var, &L_6);
		NullCheck(L_4);
		ArrayElementTypeCheck (L_4, L_7);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_7);
		String_t* L_8 = UnityString_Format_m3741272017(NULL /*static, unused*/, _stringLiteral2872073641, L_4, /*hidden argument*/NULL);
		V_0 = L_8;
		goto IL_0033;
	}

IL_0033:
	{
		String_t* L_9 = V_0;
		return L_9;
	}
}
extern "C"  String_t* Ray_ToString_m943070480_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Ray_t3785851493 * _thisAdjusted = reinterpret_cast<Ray_t3785851493 *>(__this + 1);
	return Ray_ToString_m943070480(_thisAdjusted, method);
}
// Conversion methods for marshalling of: UnityEngine.RaycastHit
extern "C" void RaycastHit_t1056001966_marshal_pinvoke(const RaycastHit_t1056001966& unmarshaled, RaycastHit_t1056001966_marshaled_pinvoke& marshaled)
{
	Il2CppCodeGenException* ___m_Collider_5Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_Collider' of type 'RaycastHit': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_Collider_5Exception);
}
extern "C" void RaycastHit_t1056001966_marshal_pinvoke_back(const RaycastHit_t1056001966_marshaled_pinvoke& marshaled, RaycastHit_t1056001966& unmarshaled)
{
	Il2CppCodeGenException* ___m_Collider_5Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_Collider' of type 'RaycastHit': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_Collider_5Exception);
}
// Conversion method for clean up from marshalling of: UnityEngine.RaycastHit
extern "C" void RaycastHit_t1056001966_marshal_pinvoke_cleanup(RaycastHit_t1056001966_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.RaycastHit
extern "C" void RaycastHit_t1056001966_marshal_com(const RaycastHit_t1056001966& unmarshaled, RaycastHit_t1056001966_marshaled_com& marshaled)
{
	Il2CppCodeGenException* ___m_Collider_5Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_Collider' of type 'RaycastHit': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_Collider_5Exception);
}
extern "C" void RaycastHit_t1056001966_marshal_com_back(const RaycastHit_t1056001966_marshaled_com& marshaled, RaycastHit_t1056001966& unmarshaled)
{
	Il2CppCodeGenException* ___m_Collider_5Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_Collider' of type 'RaycastHit': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_Collider_5Exception);
}
// Conversion method for clean up from marshalling of: UnityEngine.RaycastHit
extern "C" void RaycastHit_t1056001966_marshal_com_cleanup(RaycastHit_t1056001966_marshaled_com& marshaled)
{
}
// UnityEngine.Vector3 UnityEngine.RaycastHit::get_point()
extern "C"  Vector3_t3722313464  RaycastHit_get_point_m177058539 (RaycastHit_t1056001966 * __this, const MethodInfo* method)
{
	Vector3_t3722313464  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Vector3_t3722313464  L_0 = __this->get_m_Point_0();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		Vector3_t3722313464  L_1 = V_0;
		return L_1;
	}
}
extern "C"  Vector3_t3722313464  RaycastHit_get_point_m177058539_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	RaycastHit_t1056001966 * _thisAdjusted = reinterpret_cast<RaycastHit_t1056001966 *>(__this + 1);
	return RaycastHit_get_point_m177058539(_thisAdjusted, method);
}
// UnityEngine.Vector3 UnityEngine.RaycastHit::get_normal()
extern "C"  Vector3_t3722313464  RaycastHit_get_normal_m591687880 (RaycastHit_t1056001966 * __this, const MethodInfo* method)
{
	Vector3_t3722313464  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Vector3_t3722313464  L_0 = __this->get_m_Normal_1();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		Vector3_t3722313464  L_1 = V_0;
		return L_1;
	}
}
extern "C"  Vector3_t3722313464  RaycastHit_get_normal_m591687880_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	RaycastHit_t1056001966 * _thisAdjusted = reinterpret_cast<RaycastHit_t1056001966 *>(__this + 1);
	return RaycastHit_get_normal_m591687880(_thisAdjusted, method);
}
// System.Single UnityEngine.RaycastHit::get_distance()
extern "C"  float RaycastHit_get_distance_m2539638163 (RaycastHit_t1056001966 * __this, const MethodInfo* method)
{
	float V_0 = 0.0f;
	{
		float L_0 = __this->get_m_Distance_3();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		float L_1 = V_0;
		return L_1;
	}
}
extern "C"  float RaycastHit_get_distance_m2539638163_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	RaycastHit_t1056001966 * _thisAdjusted = reinterpret_cast<RaycastHit_t1056001966 *>(__this + 1);
	return RaycastHit_get_distance_m2539638163(_thisAdjusted, method);
}
// UnityEngine.Collider UnityEngine.RaycastHit::get_collider()
extern "C"  Collider_t1773347010 * RaycastHit_get_collider_m1442240336 (RaycastHit_t1056001966 * __this, const MethodInfo* method)
{
	Collider_t1773347010 * V_0 = NULL;
	{
		Collider_t1773347010 * L_0 = __this->get_m_Collider_5();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		Collider_t1773347010 * L_1 = V_0;
		return L_1;
	}
}
extern "C"  Collider_t1773347010 * RaycastHit_get_collider_m1442240336_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	RaycastHit_t1056001966 * _thisAdjusted = reinterpret_cast<RaycastHit_t1056001966 *>(__this + 1);
	return RaycastHit_get_collider_m1442240336(_thisAdjusted, method);
}
// UnityEngine.Rigidbody UnityEngine.RaycastHit::get_rigidbody()
extern "C"  Rigidbody_t3916780224 * RaycastHit_get_rigidbody_m846276486 (RaycastHit_t1056001966 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (RaycastHit_get_rigidbody_m846276486_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Rigidbody_t3916780224 * V_0 = NULL;
	Rigidbody_t3916780224 * G_B3_0 = NULL;
	{
		Collider_t1773347010 * L_0 = RaycastHit_get_collider_m1442240336(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m1920811489(NULL /*static, unused*/, L_0, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0022;
		}
	}
	{
		Collider_t1773347010 * L_2 = RaycastHit_get_collider_m1442240336(__this, /*hidden argument*/NULL);
		NullCheck(L_2);
		Rigidbody_t3916780224 * L_3 = Collider_get_attachedRigidbody_m4203494256(L_2, /*hidden argument*/NULL);
		G_B3_0 = L_3;
		goto IL_0023;
	}

IL_0022:
	{
		G_B3_0 = ((Rigidbody_t3916780224 *)(NULL));
	}

IL_0023:
	{
		V_0 = G_B3_0;
		goto IL_0029;
	}

IL_0029:
	{
		Rigidbody_t3916780224 * L_4 = V_0;
		return L_4;
	}
}
extern "C"  Rigidbody_t3916780224 * RaycastHit_get_rigidbody_m846276486_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	RaycastHit_t1056001966 * _thisAdjusted = reinterpret_cast<RaycastHit_t1056001966 *>(__this + 1);
	return RaycastHit_get_rigidbody_m846276486(_thisAdjusted, method);
}
// UnityEngine.Transform UnityEngine.RaycastHit::get_transform()
extern "C"  Transform_t3600365921 * RaycastHit_get_transform_m2094454139 (RaycastHit_t1056001966 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (RaycastHit_get_transform_m2094454139_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Rigidbody_t3916780224 * V_0 = NULL;
	Transform_t3600365921 * V_1 = NULL;
	{
		Rigidbody_t3916780224 * L_0 = RaycastHit_get_rigidbody_m846276486(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		Rigidbody_t3916780224 * L_1 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_2 = Object_op_Inequality_m1920811489(NULL /*static, unused*/, L_1, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0020;
		}
	}
	{
		Rigidbody_t3916780224 * L_3 = V_0;
		NullCheck(L_3);
		Transform_t3600365921 * L_4 = Component_get_transform_m2921103810(L_3, /*hidden argument*/NULL);
		V_1 = L_4;
		goto IL_0049;
	}

IL_0020:
	{
		Collider_t1773347010 * L_5 = RaycastHit_get_collider_m1442240336(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_6 = Object_op_Inequality_m1920811489(NULL /*static, unused*/, L_5, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0042;
		}
	}
	{
		Collider_t1773347010 * L_7 = RaycastHit_get_collider_m1442240336(__this, /*hidden argument*/NULL);
		NullCheck(L_7);
		Transform_t3600365921 * L_8 = Component_get_transform_m2921103810(L_7, /*hidden argument*/NULL);
		V_1 = L_8;
		goto IL_0049;
	}

IL_0042:
	{
		V_1 = (Transform_t3600365921 *)NULL;
		goto IL_0049;
	}

IL_0049:
	{
		Transform_t3600365921 * L_9 = V_1;
		return L_9;
	}
}
extern "C"  Transform_t3600365921 * RaycastHit_get_transform_m2094454139_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	RaycastHit_t1056001966 * _thisAdjusted = reinterpret_cast<RaycastHit_t1056001966 *>(__this + 1);
	return RaycastHit_get_transform_m2094454139(_thisAdjusted, method);
}
// Conversion methods for marshalling of: UnityEngine.RaycastHit2D
extern "C" void RaycastHit2D_t2279581989_marshal_pinvoke(const RaycastHit2D_t2279581989& unmarshaled, RaycastHit2D_t2279581989_marshaled_pinvoke& marshaled)
{
	Il2CppCodeGenException* ___m_Collider_5Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_Collider' of type 'RaycastHit2D': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_Collider_5Exception);
}
extern "C" void RaycastHit2D_t2279581989_marshal_pinvoke_back(const RaycastHit2D_t2279581989_marshaled_pinvoke& marshaled, RaycastHit2D_t2279581989& unmarshaled)
{
	Il2CppCodeGenException* ___m_Collider_5Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_Collider' of type 'RaycastHit2D': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_Collider_5Exception);
}
// Conversion method for clean up from marshalling of: UnityEngine.RaycastHit2D
extern "C" void RaycastHit2D_t2279581989_marshal_pinvoke_cleanup(RaycastHit2D_t2279581989_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.RaycastHit2D
extern "C" void RaycastHit2D_t2279581989_marshal_com(const RaycastHit2D_t2279581989& unmarshaled, RaycastHit2D_t2279581989_marshaled_com& marshaled)
{
	Il2CppCodeGenException* ___m_Collider_5Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_Collider' of type 'RaycastHit2D': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_Collider_5Exception);
}
extern "C" void RaycastHit2D_t2279581989_marshal_com_back(const RaycastHit2D_t2279581989_marshaled_com& marshaled, RaycastHit2D_t2279581989& unmarshaled)
{
	Il2CppCodeGenException* ___m_Collider_5Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_Collider' of type 'RaycastHit2D': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_Collider_5Exception);
}
// Conversion method for clean up from marshalling of: UnityEngine.RaycastHit2D
extern "C" void RaycastHit2D_t2279581989_marshal_com_cleanup(RaycastHit2D_t2279581989_marshaled_com& marshaled)
{
}
// UnityEngine.Vector2 UnityEngine.RaycastHit2D::get_point()
extern "C"  Vector2_t2156229523  RaycastHit2D_get_point_m3958944977 (RaycastHit2D_t2279581989 * __this, const MethodInfo* method)
{
	Vector2_t2156229523  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Vector2_t2156229523  L_0 = __this->get_m_Point_1();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		Vector2_t2156229523  L_1 = V_0;
		return L_1;
	}
}
extern "C"  Vector2_t2156229523  RaycastHit2D_get_point_m3958944977_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	RaycastHit2D_t2279581989 * _thisAdjusted = reinterpret_cast<RaycastHit2D_t2279581989 *>(__this + 1);
	return RaycastHit2D_get_point_m3958944977(_thisAdjusted, method);
}
// UnityEngine.Vector2 UnityEngine.RaycastHit2D::get_normal()
extern "C"  Vector2_t2156229523  RaycastHit2D_get_normal_m3675591968 (RaycastHit2D_t2279581989 * __this, const MethodInfo* method)
{
	Vector2_t2156229523  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Vector2_t2156229523  L_0 = __this->get_m_Normal_2();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		Vector2_t2156229523  L_1 = V_0;
		return L_1;
	}
}
extern "C"  Vector2_t2156229523  RaycastHit2D_get_normal_m3675591968_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	RaycastHit2D_t2279581989 * _thisAdjusted = reinterpret_cast<RaycastHit2D_t2279581989 *>(__this + 1);
	return RaycastHit2D_get_normal_m3675591968(_thisAdjusted, method);
}
// System.Single UnityEngine.RaycastHit2D::get_fraction()
extern "C"  float RaycastHit2D_get_fraction_m2738084453 (RaycastHit2D_t2279581989 * __this, const MethodInfo* method)
{
	float V_0 = 0.0f;
	{
		float L_0 = __this->get_m_Fraction_4();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		float L_1 = V_0;
		return L_1;
	}
}
extern "C"  float RaycastHit2D_get_fraction_m2738084453_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	RaycastHit2D_t2279581989 * _thisAdjusted = reinterpret_cast<RaycastHit2D_t2279581989 *>(__this + 1);
	return RaycastHit2D_get_fraction_m2738084453(_thisAdjusted, method);
}
// UnityEngine.Collider2D UnityEngine.RaycastHit2D::get_collider()
extern "C"  Collider2D_t2806799626 * RaycastHit2D_get_collider_m1860250292 (RaycastHit2D_t2279581989 * __this, const MethodInfo* method)
{
	Collider2D_t2806799626 * V_0 = NULL;
	{
		Collider2D_t2806799626 * L_0 = __this->get_m_Collider_5();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		Collider2D_t2806799626 * L_1 = V_0;
		return L_1;
	}
}
extern "C"  Collider2D_t2806799626 * RaycastHit2D_get_collider_m1860250292_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	RaycastHit2D_t2279581989 * _thisAdjusted = reinterpret_cast<RaycastHit2D_t2279581989 *>(__this + 1);
	return RaycastHit2D_get_collider_m1860250292(_thisAdjusted, method);
}
// System.Void UnityEngine.Rect::.ctor(System.Single,System.Single,System.Single,System.Single)
extern "C"  void Rect__ctor_m2635848439 (Rect_t2360479859 * __this, float ___x0, float ___y1, float ___width2, float ___height3, const MethodInfo* method)
{
	{
		float L_0 = ___x0;
		__this->set_m_XMin_0(L_0);
		float L_1 = ___y1;
		__this->set_m_YMin_1(L_1);
		float L_2 = ___width2;
		__this->set_m_Width_2(L_2);
		float L_3 = ___height3;
		__this->set_m_Height_3(L_3);
		return;
	}
}
extern "C"  void Rect__ctor_m2635848439_AdjustorThunk (Il2CppObject * __this, float ___x0, float ___y1, float ___width2, float ___height3, const MethodInfo* method)
{
	Rect_t2360479859 * _thisAdjusted = reinterpret_cast<Rect_t2360479859 *>(__this + 1);
	Rect__ctor_m2635848439(_thisAdjusted, ___x0, ___y1, ___width2, ___height3, method);
}
// System.Single UnityEngine.Rect::get_x()
extern "C"  float Rect_get_x_m3218181674 (Rect_t2360479859 * __this, const MethodInfo* method)
{
	float V_0 = 0.0f;
	{
		float L_0 = __this->get_m_XMin_0();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		float L_1 = V_0;
		return L_1;
	}
}
extern "C"  float Rect_get_x_m3218181674_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Rect_t2360479859 * _thisAdjusted = reinterpret_cast<Rect_t2360479859 *>(__this + 1);
	return Rect_get_x_m3218181674(_thisAdjusted, method);
}
// System.Void UnityEngine.Rect::set_x(System.Single)
extern "C"  void Rect_set_x_m534803876 (Rect_t2360479859 * __this, float ___value0, const MethodInfo* method)
{
	{
		float L_0 = ___value0;
		__this->set_m_XMin_0(L_0);
		return;
	}
}
extern "C"  void Rect_set_x_m534803876_AdjustorThunk (Il2CppObject * __this, float ___value0, const MethodInfo* method)
{
	Rect_t2360479859 * _thisAdjusted = reinterpret_cast<Rect_t2360479859 *>(__this + 1);
	Rect_set_x_m534803876(_thisAdjusted, ___value0, method);
}
// System.Single UnityEngine.Rect::get_y()
extern "C"  float Rect_get_y_m3218181675 (Rect_t2360479859 * __this, const MethodInfo* method)
{
	float V_0 = 0.0f;
	{
		float L_0 = __this->get_m_YMin_1();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		float L_1 = V_0;
		return L_1;
	}
}
extern "C"  float Rect_get_y_m3218181675_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Rect_t2360479859 * _thisAdjusted = reinterpret_cast<Rect_t2360479859 *>(__this + 1);
	return Rect_get_y_m3218181675(_thisAdjusted, method);
}
// System.Void UnityEngine.Rect::set_y(System.Single)
extern "C"  void Rect_set_y_m946788033 (Rect_t2360479859 * __this, float ___value0, const MethodInfo* method)
{
	{
		float L_0 = ___value0;
		__this->set_m_YMin_1(L_0);
		return;
	}
}
extern "C"  void Rect_set_y_m946788033_AdjustorThunk (Il2CppObject * __this, float ___value0, const MethodInfo* method)
{
	Rect_t2360479859 * _thisAdjusted = reinterpret_cast<Rect_t2360479859 *>(__this + 1);
	Rect_set_y_m946788033(_thisAdjusted, ___value0, method);
}
// UnityEngine.Vector2 UnityEngine.Rect::get_position()
extern "C"  Vector2_t2156229523  Rect_get_position_m2767609553 (Rect_t2360479859 * __this, const MethodInfo* method)
{
	Vector2_t2156229523  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		float L_0 = __this->get_m_XMin_0();
		float L_1 = __this->get_m_YMin_1();
		Vector2_t2156229523  L_2;
		memset(&L_2, 0, sizeof(L_2));
		Vector2__ctor_m4060800441(&L_2, L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		goto IL_0018;
	}

IL_0018:
	{
		Vector2_t2156229523  L_3 = V_0;
		return L_3;
	}
}
extern "C"  Vector2_t2156229523  Rect_get_position_m2767609553_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Rect_t2360479859 * _thisAdjusted = reinterpret_cast<Rect_t2360479859 *>(__this + 1);
	return Rect_get_position_m2767609553(_thisAdjusted, method);
}
// UnityEngine.Vector2 UnityEngine.Rect::get_center()
extern "C"  Vector2_t2156229523  Rect_get_center_m182049623 (Rect_t2360479859 * __this, const MethodInfo* method)
{
	Vector2_t2156229523  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		float L_0 = Rect_get_x_m3218181674(__this, /*hidden argument*/NULL);
		float L_1 = __this->get_m_Width_2();
		float L_2 = Rect_get_y_m3218181675(__this, /*hidden argument*/NULL);
		float L_3 = __this->get_m_Height_3();
		Vector2_t2156229523  L_4;
		memset(&L_4, 0, sizeof(L_4));
		Vector2__ctor_m4060800441(&L_4, ((float)((float)L_0+(float)((float)((float)L_1/(float)(2.0f))))), ((float)((float)L_2+(float)((float)((float)L_3/(float)(2.0f))))), /*hidden argument*/NULL);
		V_0 = L_4;
		goto IL_0032;
	}

IL_0032:
	{
		Vector2_t2156229523  L_5 = V_0;
		return L_5;
	}
}
extern "C"  Vector2_t2156229523  Rect_get_center_m182049623_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Rect_t2360479859 * _thisAdjusted = reinterpret_cast<Rect_t2360479859 *>(__this + 1);
	return Rect_get_center_m182049623(_thisAdjusted, method);
}
// System.Void UnityEngine.Rect::set_center(UnityEngine.Vector2)
extern "C"  void Rect_set_center_m4014133992 (Rect_t2360479859 * __this, Vector2_t2156229523  ___value0, const MethodInfo* method)
{
	{
		float L_0 = (&___value0)->get_x_0();
		float L_1 = __this->get_m_Width_2();
		__this->set_m_XMin_0(((float)((float)L_0-(float)((float)((float)L_1/(float)(2.0f))))));
		float L_2 = (&___value0)->get_y_1();
		float L_3 = __this->get_m_Height_3();
		__this->set_m_YMin_1(((float)((float)L_2-(float)((float)((float)L_3/(float)(2.0f))))));
		return;
	}
}
extern "C"  void Rect_set_center_m4014133992_AdjustorThunk (Il2CppObject * __this, Vector2_t2156229523  ___value0, const MethodInfo* method)
{
	Rect_t2360479859 * _thisAdjusted = reinterpret_cast<Rect_t2360479859 *>(__this + 1);
	Rect_set_center_m4014133992(_thisAdjusted, ___value0, method);
}
// UnityEngine.Vector2 UnityEngine.Rect::get_min()
extern "C"  Vector2_t2156229523  Rect_get_min_m3948069389 (Rect_t2360479859 * __this, const MethodInfo* method)
{
	Vector2_t2156229523  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		float L_0 = Rect_get_xMin_m2659235730(__this, /*hidden argument*/NULL);
		float L_1 = Rect_get_yMin_m2659235699(__this, /*hidden argument*/NULL);
		Vector2_t2156229523  L_2;
		memset(&L_2, 0, sizeof(L_2));
		Vector2__ctor_m4060800441(&L_2, L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		goto IL_0018;
	}

IL_0018:
	{
		Vector2_t2156229523  L_3 = V_0;
		return L_3;
	}
}
extern "C"  Vector2_t2156229523  Rect_get_min_m3948069389_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Rect_t2360479859 * _thisAdjusted = reinterpret_cast<Rect_t2360479859 *>(__this + 1);
	return Rect_get_min_m3948069389(_thisAdjusted, method);
}
// UnityEngine.Vector2 UnityEngine.Rect::get_max()
extern "C"  Vector2_t2156229523  Rect_get_max_m724222499 (Rect_t2360479859 * __this, const MethodInfo* method)
{
	Vector2_t2156229523  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		float L_0 = Rect_get_xMax_m2926206058(__this, /*hidden argument*/NULL);
		float L_1 = Rect_get_yMax_m2926206027(__this, /*hidden argument*/NULL);
		Vector2_t2156229523  L_2;
		memset(&L_2, 0, sizeof(L_2));
		Vector2__ctor_m4060800441(&L_2, L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		goto IL_0018;
	}

IL_0018:
	{
		Vector2_t2156229523  L_3 = V_0;
		return L_3;
	}
}
extern "C"  Vector2_t2156229523  Rect_get_max_m724222499_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Rect_t2360479859 * _thisAdjusted = reinterpret_cast<Rect_t2360479859 *>(__this + 1);
	return Rect_get_max_m724222499(_thisAdjusted, method);
}
// System.Single UnityEngine.Rect::get_width()
extern "C"  float Rect_get_width_m3421965717 (Rect_t2360479859 * __this, const MethodInfo* method)
{
	float V_0 = 0.0f;
	{
		float L_0 = __this->get_m_Width_2();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		float L_1 = V_0;
		return L_1;
	}
}
extern "C"  float Rect_get_width_m3421965717_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Rect_t2360479859 * _thisAdjusted = reinterpret_cast<Rect_t2360479859 *>(__this + 1);
	return Rect_get_width_m3421965717(_thisAdjusted, method);
}
// System.Void UnityEngine.Rect::set_width(System.Single)
extern "C"  void Rect_set_width_m2263092238 (Rect_t2360479859 * __this, float ___value0, const MethodInfo* method)
{
	{
		float L_0 = ___value0;
		__this->set_m_Width_2(L_0);
		return;
	}
}
extern "C"  void Rect_set_width_m2263092238_AdjustorThunk (Il2CppObject * __this, float ___value0, const MethodInfo* method)
{
	Rect_t2360479859 * _thisAdjusted = reinterpret_cast<Rect_t2360479859 *>(__this + 1);
	Rect_set_width_m2263092238(_thisAdjusted, ___value0, method);
}
// System.Single UnityEngine.Rect::get_height()
extern "C"  float Rect_get_height_m977101306 (Rect_t2360479859 * __this, const MethodInfo* method)
{
	float V_0 = 0.0f;
	{
		float L_0 = __this->get_m_Height_3();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		float L_1 = V_0;
		return L_1;
	}
}
extern "C"  float Rect_get_height_m977101306_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Rect_t2360479859 * _thisAdjusted = reinterpret_cast<Rect_t2360479859 *>(__this + 1);
	return Rect_get_height_m977101306(_thisAdjusted, method);
}
// System.Void UnityEngine.Rect::set_height(System.Single)
extern "C"  void Rect_set_height_m230303970 (Rect_t2360479859 * __this, float ___value0, const MethodInfo* method)
{
	{
		float L_0 = ___value0;
		__this->set_m_Height_3(L_0);
		return;
	}
}
extern "C"  void Rect_set_height_m230303970_AdjustorThunk (Il2CppObject * __this, float ___value0, const MethodInfo* method)
{
	Rect_t2360479859 * _thisAdjusted = reinterpret_cast<Rect_t2360479859 *>(__this + 1);
	Rect_set_height_m230303970(_thisAdjusted, ___value0, method);
}
// UnityEngine.Vector2 UnityEngine.Rect::get_size()
extern "C"  Vector2_t2156229523  Rect_get_size_m3542039952 (Rect_t2360479859 * __this, const MethodInfo* method)
{
	Vector2_t2156229523  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		float L_0 = __this->get_m_Width_2();
		float L_1 = __this->get_m_Height_3();
		Vector2_t2156229523  L_2;
		memset(&L_2, 0, sizeof(L_2));
		Vector2__ctor_m4060800441(&L_2, L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		goto IL_0018;
	}

IL_0018:
	{
		Vector2_t2156229523  L_3 = V_0;
		return L_3;
	}
}
extern "C"  Vector2_t2156229523  Rect_get_size_m3542039952_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Rect_t2360479859 * _thisAdjusted = reinterpret_cast<Rect_t2360479859 *>(__this + 1);
	return Rect_get_size_m3542039952(_thisAdjusted, method);
}
// System.Single UnityEngine.Rect::get_xMin()
extern "C"  float Rect_get_xMin_m2659235730 (Rect_t2360479859 * __this, const MethodInfo* method)
{
	float V_0 = 0.0f;
	{
		float L_0 = __this->get_m_XMin_0();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		float L_1 = V_0;
		return L_1;
	}
}
extern "C"  float Rect_get_xMin_m2659235730_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Rect_t2360479859 * _thisAdjusted = reinterpret_cast<Rect_t2360479859 *>(__this + 1);
	return Rect_get_xMin_m2659235730(_thisAdjusted, method);
}
// System.Void UnityEngine.Rect::set_xMin(System.Single)
extern "C"  void Rect_set_xMin_m2775136022 (Rect_t2360479859 * __this, float ___value0, const MethodInfo* method)
{
	float V_0 = 0.0f;
	{
		float L_0 = Rect_get_xMax_m2926206058(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		float L_1 = ___value0;
		__this->set_m_XMin_0(L_1);
		float L_2 = V_0;
		float L_3 = __this->get_m_XMin_0();
		__this->set_m_Width_2(((float)((float)L_2-(float)L_3)));
		return;
	}
}
extern "C"  void Rect_set_xMin_m2775136022_AdjustorThunk (Il2CppObject * __this, float ___value0, const MethodInfo* method)
{
	Rect_t2360479859 * _thisAdjusted = reinterpret_cast<Rect_t2360479859 *>(__this + 1);
	Rect_set_xMin_m2775136022(_thisAdjusted, ___value0, method);
}
// System.Single UnityEngine.Rect::get_yMin()
extern "C"  float Rect_get_yMin_m2659235699 (Rect_t2360479859 * __this, const MethodInfo* method)
{
	float V_0 = 0.0f;
	{
		float L_0 = __this->get_m_YMin_1();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		float L_1 = V_0;
		return L_1;
	}
}
extern "C"  float Rect_get_yMin_m2659235699_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Rect_t2360479859 * _thisAdjusted = reinterpret_cast<Rect_t2360479859 *>(__this + 1);
	return Rect_get_yMin_m2659235699(_thisAdjusted, method);
}
// System.Void UnityEngine.Rect::set_yMin(System.Single)
extern "C"  void Rect_set_yMin_m415158369 (Rect_t2360479859 * __this, float ___value0, const MethodInfo* method)
{
	float V_0 = 0.0f;
	{
		float L_0 = Rect_get_yMax_m2926206027(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		float L_1 = ___value0;
		__this->set_m_YMin_1(L_1);
		float L_2 = V_0;
		float L_3 = __this->get_m_YMin_1();
		__this->set_m_Height_3(((float)((float)L_2-(float)L_3)));
		return;
	}
}
extern "C"  void Rect_set_yMin_m415158369_AdjustorThunk (Il2CppObject * __this, float ___value0, const MethodInfo* method)
{
	Rect_t2360479859 * _thisAdjusted = reinterpret_cast<Rect_t2360479859 *>(__this + 1);
	Rect_set_yMin_m415158369(_thisAdjusted, ___value0, method);
}
// System.Single UnityEngine.Rect::get_xMax()
extern "C"  float Rect_get_xMax_m2926206058 (Rect_t2360479859 * __this, const MethodInfo* method)
{
	float V_0 = 0.0f;
	{
		float L_0 = __this->get_m_Width_2();
		float L_1 = __this->get_m_XMin_0();
		V_0 = ((float)((float)L_0+(float)L_1));
		goto IL_0014;
	}

IL_0014:
	{
		float L_2 = V_0;
		return L_2;
	}
}
extern "C"  float Rect_get_xMax_m2926206058_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Rect_t2360479859 * _thisAdjusted = reinterpret_cast<Rect_t2360479859 *>(__this + 1);
	return Rect_get_xMax_m2926206058(_thisAdjusted, method);
}
// System.Void UnityEngine.Rect::set_xMax(System.Single)
extern "C"  void Rect_set_xMax_m4050361423 (Rect_t2360479859 * __this, float ___value0, const MethodInfo* method)
{
	{
		float L_0 = ___value0;
		float L_1 = __this->get_m_XMin_0();
		__this->set_m_Width_2(((float)((float)L_0-(float)L_1)));
		return;
	}
}
extern "C"  void Rect_set_xMax_m4050361423_AdjustorThunk (Il2CppObject * __this, float ___value0, const MethodInfo* method)
{
	Rect_t2360479859 * _thisAdjusted = reinterpret_cast<Rect_t2360479859 *>(__this + 1);
	Rect_set_xMax_m4050361423(_thisAdjusted, ___value0, method);
}
// System.Single UnityEngine.Rect::get_yMax()
extern "C"  float Rect_get_yMax_m2926206027 (Rect_t2360479859 * __this, const MethodInfo* method)
{
	float V_0 = 0.0f;
	{
		float L_0 = __this->get_m_Height_3();
		float L_1 = __this->get_m_YMin_1();
		V_0 = ((float)((float)L_0+(float)L_1));
		goto IL_0014;
	}

IL_0014:
	{
		float L_2 = V_0;
		return L_2;
	}
}
extern "C"  float Rect_get_yMax_m2926206027_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Rect_t2360479859 * _thisAdjusted = reinterpret_cast<Rect_t2360479859 *>(__this + 1);
	return Rect_get_yMax_m2926206027(_thisAdjusted, method);
}
// System.Void UnityEngine.Rect::set_yMax(System.Single)
extern "C"  void Rect_set_yMax_m1690383770 (Rect_t2360479859 * __this, float ___value0, const MethodInfo* method)
{
	{
		float L_0 = ___value0;
		float L_1 = __this->get_m_YMin_1();
		__this->set_m_Height_3(((float)((float)L_0-(float)L_1)));
		return;
	}
}
extern "C"  void Rect_set_yMax_m1690383770_AdjustorThunk (Il2CppObject * __this, float ___value0, const MethodInfo* method)
{
	Rect_t2360479859 * _thisAdjusted = reinterpret_cast<Rect_t2360479859 *>(__this + 1);
	Rect_set_yMax_m1690383770(_thisAdjusted, ___value0, method);
}
// System.Boolean UnityEngine.Rect::Contains(UnityEngine.Vector2)
extern "C"  bool Rect_Contains_m2308869928 (Rect_t2360479859 * __this, Vector2_t2156229523  ___point0, const MethodInfo* method)
{
	bool V_0 = false;
	int32_t G_B5_0 = 0;
	{
		float L_0 = (&___point0)->get_x_0();
		float L_1 = Rect_get_xMin_m2659235730(__this, /*hidden argument*/NULL);
		if ((!(((float)L_0) >= ((float)L_1))))
		{
			goto IL_0048;
		}
	}
	{
		float L_2 = (&___point0)->get_x_0();
		float L_3 = Rect_get_xMax_m2926206058(__this, /*hidden argument*/NULL);
		if ((!(((float)L_2) < ((float)L_3))))
		{
			goto IL_0048;
		}
	}
	{
		float L_4 = (&___point0)->get_y_1();
		float L_5 = Rect_get_yMin_m2659235699(__this, /*hidden argument*/NULL);
		if ((!(((float)L_4) >= ((float)L_5))))
		{
			goto IL_0048;
		}
	}
	{
		float L_6 = (&___point0)->get_y_1();
		float L_7 = Rect_get_yMax_m2926206027(__this, /*hidden argument*/NULL);
		G_B5_0 = ((((float)L_6) < ((float)L_7))? 1 : 0);
		goto IL_0049;
	}

IL_0048:
	{
		G_B5_0 = 0;
	}

IL_0049:
	{
		V_0 = (bool)G_B5_0;
		goto IL_004f;
	}

IL_004f:
	{
		bool L_8 = V_0;
		return L_8;
	}
}
extern "C"  bool Rect_Contains_m2308869928_AdjustorThunk (Il2CppObject * __this, Vector2_t2156229523  ___point0, const MethodInfo* method)
{
	Rect_t2360479859 * _thisAdjusted = reinterpret_cast<Rect_t2360479859 *>(__this + 1);
	return Rect_Contains_m2308869928(_thisAdjusted, ___point0, method);
}
// System.Boolean UnityEngine.Rect::Contains(UnityEngine.Vector3)
extern "C"  bool Rect_Contains_m2308869927 (Rect_t2360479859 * __this, Vector3_t3722313464  ___point0, const MethodInfo* method)
{
	bool V_0 = false;
	int32_t G_B5_0 = 0;
	{
		float L_0 = (&___point0)->get_x_1();
		float L_1 = Rect_get_xMin_m2659235730(__this, /*hidden argument*/NULL);
		if ((!(((float)L_0) >= ((float)L_1))))
		{
			goto IL_0048;
		}
	}
	{
		float L_2 = (&___point0)->get_x_1();
		float L_3 = Rect_get_xMax_m2926206058(__this, /*hidden argument*/NULL);
		if ((!(((float)L_2) < ((float)L_3))))
		{
			goto IL_0048;
		}
	}
	{
		float L_4 = (&___point0)->get_y_2();
		float L_5 = Rect_get_yMin_m2659235699(__this, /*hidden argument*/NULL);
		if ((!(((float)L_4) >= ((float)L_5))))
		{
			goto IL_0048;
		}
	}
	{
		float L_6 = (&___point0)->get_y_2();
		float L_7 = Rect_get_yMax_m2926206027(__this, /*hidden argument*/NULL);
		G_B5_0 = ((((float)L_6) < ((float)L_7))? 1 : 0);
		goto IL_0049;
	}

IL_0048:
	{
		G_B5_0 = 0;
	}

IL_0049:
	{
		V_0 = (bool)G_B5_0;
		goto IL_004f;
	}

IL_004f:
	{
		bool L_8 = V_0;
		return L_8;
	}
}
extern "C"  bool Rect_Contains_m2308869927_AdjustorThunk (Il2CppObject * __this, Vector3_t3722313464  ___point0, const MethodInfo* method)
{
	Rect_t2360479859 * _thisAdjusted = reinterpret_cast<Rect_t2360479859 *>(__this + 1);
	return Rect_Contains_m2308869927(_thisAdjusted, ___point0, method);
}
// UnityEngine.Rect UnityEngine.Rect::OrderMinMax(UnityEngine.Rect)
extern "C"  Rect_t2360479859  Rect_OrderMinMax_m3546425063 (Il2CppObject * __this /* static, unused */, Rect_t2360479859  ___rect0, const MethodInfo* method)
{
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	Rect_t2360479859  V_2;
	memset(&V_2, 0, sizeof(V_2));
	{
		float L_0 = Rect_get_xMin_m2659235730((&___rect0), /*hidden argument*/NULL);
		float L_1 = Rect_get_xMax_m2926206058((&___rect0), /*hidden argument*/NULL);
		if ((!(((float)L_0) > ((float)L_1))))
		{
			goto IL_0034;
		}
	}
	{
		float L_2 = Rect_get_xMin_m2659235730((&___rect0), /*hidden argument*/NULL);
		V_0 = L_2;
		float L_3 = Rect_get_xMax_m2926206058((&___rect0), /*hidden argument*/NULL);
		Rect_set_xMin_m2775136022((&___rect0), L_3, /*hidden argument*/NULL);
		float L_4 = V_0;
		Rect_set_xMax_m4050361423((&___rect0), L_4, /*hidden argument*/NULL);
	}

IL_0034:
	{
		float L_5 = Rect_get_yMin_m2659235699((&___rect0), /*hidden argument*/NULL);
		float L_6 = Rect_get_yMax_m2926206027((&___rect0), /*hidden argument*/NULL);
		if ((!(((float)L_5) > ((float)L_6))))
		{
			goto IL_0067;
		}
	}
	{
		float L_7 = Rect_get_yMin_m2659235699((&___rect0), /*hidden argument*/NULL);
		V_1 = L_7;
		float L_8 = Rect_get_yMax_m2926206027((&___rect0), /*hidden argument*/NULL);
		Rect_set_yMin_m415158369((&___rect0), L_8, /*hidden argument*/NULL);
		float L_9 = V_1;
		Rect_set_yMax_m1690383770((&___rect0), L_9, /*hidden argument*/NULL);
	}

IL_0067:
	{
		Rect_t2360479859  L_10 = ___rect0;
		V_2 = L_10;
		goto IL_006e;
	}

IL_006e:
	{
		Rect_t2360479859  L_11 = V_2;
		return L_11;
	}
}
// System.Boolean UnityEngine.Rect::Overlaps(UnityEngine.Rect)
extern "C"  bool Rect_Overlaps_m743951238 (Rect_t2360479859 * __this, Rect_t2360479859  ___other0, const MethodInfo* method)
{
	bool V_0 = false;
	int32_t G_B5_0 = 0;
	{
		float L_0 = Rect_get_xMax_m2926206058((&___other0), /*hidden argument*/NULL);
		float L_1 = Rect_get_xMin_m2659235730(__this, /*hidden argument*/NULL);
		if ((!(((float)L_0) > ((float)L_1))))
		{
			goto IL_0048;
		}
	}
	{
		float L_2 = Rect_get_xMin_m2659235730((&___other0), /*hidden argument*/NULL);
		float L_3 = Rect_get_xMax_m2926206058(__this, /*hidden argument*/NULL);
		if ((!(((float)L_2) < ((float)L_3))))
		{
			goto IL_0048;
		}
	}
	{
		float L_4 = Rect_get_yMax_m2926206027((&___other0), /*hidden argument*/NULL);
		float L_5 = Rect_get_yMin_m2659235699(__this, /*hidden argument*/NULL);
		if ((!(((float)L_4) > ((float)L_5))))
		{
			goto IL_0048;
		}
	}
	{
		float L_6 = Rect_get_yMin_m2659235699((&___other0), /*hidden argument*/NULL);
		float L_7 = Rect_get_yMax_m2926206027(__this, /*hidden argument*/NULL);
		G_B5_0 = ((((float)L_6) < ((float)L_7))? 1 : 0);
		goto IL_0049;
	}

IL_0048:
	{
		G_B5_0 = 0;
	}

IL_0049:
	{
		V_0 = (bool)G_B5_0;
		goto IL_004f;
	}

IL_004f:
	{
		bool L_8 = V_0;
		return L_8;
	}
}
extern "C"  bool Rect_Overlaps_m743951238_AdjustorThunk (Il2CppObject * __this, Rect_t2360479859  ___other0, const MethodInfo* method)
{
	Rect_t2360479859 * _thisAdjusted = reinterpret_cast<Rect_t2360479859 *>(__this + 1);
	return Rect_Overlaps_m743951238(_thisAdjusted, ___other0, method);
}
// System.Boolean UnityEngine.Rect::Overlaps(UnityEngine.Rect,System.Boolean)
extern "C"  bool Rect_Overlaps_m6060363 (Rect_t2360479859 * __this, Rect_t2360479859  ___other0, bool ___allowInverse1, const MethodInfo* method)
{
	Rect_t2360479859  V_0;
	memset(&V_0, 0, sizeof(V_0));
	bool V_1 = false;
	{
		V_0 = (*(Rect_t2360479859 *)__this);
		bool L_0 = ___allowInverse1;
		if (!L_0)
		{
			goto IL_001f;
		}
	}
	{
		Rect_t2360479859  L_1 = V_0;
		Rect_t2360479859  L_2 = Rect_OrderMinMax_m3546425063(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		Rect_t2360479859  L_3 = ___other0;
		Rect_t2360479859  L_4 = Rect_OrderMinMax_m3546425063(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		___other0 = L_4;
	}

IL_001f:
	{
		Rect_t2360479859  L_5 = ___other0;
		bool L_6 = Rect_Overlaps_m743951238((&V_0), L_5, /*hidden argument*/NULL);
		V_1 = L_6;
		goto IL_002d;
	}

IL_002d:
	{
		bool L_7 = V_1;
		return L_7;
	}
}
extern "C"  bool Rect_Overlaps_m6060363_AdjustorThunk (Il2CppObject * __this, Rect_t2360479859  ___other0, bool ___allowInverse1, const MethodInfo* method)
{
	Rect_t2360479859 * _thisAdjusted = reinterpret_cast<Rect_t2360479859 *>(__this + 1);
	return Rect_Overlaps_m6060363(_thisAdjusted, ___other0, ___allowInverse1, method);
}
// System.Boolean UnityEngine.Rect::op_Inequality(UnityEngine.Rect,UnityEngine.Rect)
extern "C"  bool Rect_op_Inequality_m9517823 (Il2CppObject * __this /* static, unused */, Rect_t2360479859  ___lhs0, Rect_t2360479859  ___rhs1, const MethodInfo* method)
{
	bool V_0 = false;
	{
		Rect_t2360479859  L_0 = ___lhs0;
		Rect_t2360479859  L_1 = ___rhs1;
		bool L_2 = Rect_op_Equality_m1072810924(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		V_0 = (bool)((((int32_t)L_2) == ((int32_t)0))? 1 : 0);
		goto IL_0011;
	}

IL_0011:
	{
		bool L_3 = V_0;
		return L_3;
	}
}
// System.Boolean UnityEngine.Rect::op_Equality(UnityEngine.Rect,UnityEngine.Rect)
extern "C"  bool Rect_op_Equality_m1072810924 (Il2CppObject * __this /* static, unused */, Rect_t2360479859  ___lhs0, Rect_t2360479859  ___rhs1, const MethodInfo* method)
{
	bool V_0 = false;
	int32_t G_B5_0 = 0;
	{
		float L_0 = Rect_get_x_m3218181674((&___lhs0), /*hidden argument*/NULL);
		float L_1 = Rect_get_x_m3218181674((&___rhs1), /*hidden argument*/NULL);
		if ((!(((float)L_0) == ((float)L_1))))
		{
			goto IL_004c;
		}
	}
	{
		float L_2 = Rect_get_y_m3218181675((&___lhs0), /*hidden argument*/NULL);
		float L_3 = Rect_get_y_m3218181675((&___rhs1), /*hidden argument*/NULL);
		if ((!(((float)L_2) == ((float)L_3))))
		{
			goto IL_004c;
		}
	}
	{
		float L_4 = Rect_get_width_m3421965717((&___lhs0), /*hidden argument*/NULL);
		float L_5 = Rect_get_width_m3421965717((&___rhs1), /*hidden argument*/NULL);
		if ((!(((float)L_4) == ((float)L_5))))
		{
			goto IL_004c;
		}
	}
	{
		float L_6 = Rect_get_height_m977101306((&___lhs0), /*hidden argument*/NULL);
		float L_7 = Rect_get_height_m977101306((&___rhs1), /*hidden argument*/NULL);
		G_B5_0 = ((((float)L_6) == ((float)L_7))? 1 : 0);
		goto IL_004d;
	}

IL_004c:
	{
		G_B5_0 = 0;
	}

IL_004d:
	{
		V_0 = (bool)G_B5_0;
		goto IL_0053;
	}

IL_0053:
	{
		bool L_8 = V_0;
		return L_8;
	}
}
// System.Int32 UnityEngine.Rect::GetHashCode()
extern "C"  int32_t Rect_GetHashCode_m637011824 (Rect_t2360479859 * __this, const MethodInfo* method)
{
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	float V_3 = 0.0f;
	int32_t V_4 = 0;
	{
		float L_0 = Rect_get_x_m3218181674(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		int32_t L_1 = Single_GetHashCode_m1558506138((&V_0), /*hidden argument*/NULL);
		float L_2 = Rect_get_width_m3421965717(__this, /*hidden argument*/NULL);
		V_1 = L_2;
		int32_t L_3 = Single_GetHashCode_m1558506138((&V_1), /*hidden argument*/NULL);
		float L_4 = Rect_get_y_m3218181675(__this, /*hidden argument*/NULL);
		V_2 = L_4;
		int32_t L_5 = Single_GetHashCode_m1558506138((&V_2), /*hidden argument*/NULL);
		float L_6 = Rect_get_height_m977101306(__this, /*hidden argument*/NULL);
		V_3 = L_6;
		int32_t L_7 = Single_GetHashCode_m1558506138((&V_3), /*hidden argument*/NULL);
		V_4 = ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_1^(int32_t)((int32_t)((int32_t)L_3<<(int32_t)2))))^(int32_t)((int32_t)((int32_t)L_5>>(int32_t)2))))^(int32_t)((int32_t)((int32_t)L_7>>(int32_t)1))));
		goto IL_0061;
	}

IL_0061:
	{
		int32_t L_8 = V_4;
		return L_8;
	}
}
extern "C"  int32_t Rect_GetHashCode_m637011824_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Rect_t2360479859 * _thisAdjusted = reinterpret_cast<Rect_t2360479859 *>(__this + 1);
	return Rect_GetHashCode_m637011824(_thisAdjusted, method);
}
// System.Boolean UnityEngine.Rect::Equals(System.Object)
extern "C"  bool Rect_Equals_m866904981 (Rect_t2360479859 * __this, Il2CppObject * ___other0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Rect_Equals_m866904981_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	Rect_t2360479859  V_1;
	memset(&V_1, 0, sizeof(V_1));
	float V_2 = 0.0f;
	float V_3 = 0.0f;
	float V_4 = 0.0f;
	float V_5 = 0.0f;
	int32_t G_B7_0 = 0;
	{
		Il2CppObject * L_0 = ___other0;
		if (((Il2CppObject *)IsInstSealed(L_0, Rect_t2360479859_il2cpp_TypeInfo_var)))
		{
			goto IL_0013;
		}
	}
	{
		V_0 = (bool)0;
		goto IL_0088;
	}

IL_0013:
	{
		Il2CppObject * L_1 = ___other0;
		V_1 = ((*(Rect_t2360479859 *)((Rect_t2360479859 *)UnBox(L_1, Rect_t2360479859_il2cpp_TypeInfo_var))));
		float L_2 = Rect_get_x_m3218181674(__this, /*hidden argument*/NULL);
		V_2 = L_2;
		float L_3 = Rect_get_x_m3218181674((&V_1), /*hidden argument*/NULL);
		bool L_4 = Single_Equals_m1601893879((&V_2), L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0081;
		}
	}
	{
		float L_5 = Rect_get_y_m3218181675(__this, /*hidden argument*/NULL);
		V_3 = L_5;
		float L_6 = Rect_get_y_m3218181675((&V_1), /*hidden argument*/NULL);
		bool L_7 = Single_Equals_m1601893879((&V_3), L_6, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0081;
		}
	}
	{
		float L_8 = Rect_get_width_m3421965717(__this, /*hidden argument*/NULL);
		V_4 = L_8;
		float L_9 = Rect_get_width_m3421965717((&V_1), /*hidden argument*/NULL);
		bool L_10 = Single_Equals_m1601893879((&V_4), L_9, /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_0081;
		}
	}
	{
		float L_11 = Rect_get_height_m977101306(__this, /*hidden argument*/NULL);
		V_5 = L_11;
		float L_12 = Rect_get_height_m977101306((&V_1), /*hidden argument*/NULL);
		bool L_13 = Single_Equals_m1601893879((&V_5), L_12, /*hidden argument*/NULL);
		G_B7_0 = ((int32_t)(L_13));
		goto IL_0082;
	}

IL_0081:
	{
		G_B7_0 = 0;
	}

IL_0082:
	{
		V_0 = (bool)G_B7_0;
		goto IL_0088;
	}

IL_0088:
	{
		bool L_14 = V_0;
		return L_14;
	}
}
extern "C"  bool Rect_Equals_m866904981_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___other0, const MethodInfo* method)
{
	Rect_t2360479859 * _thisAdjusted = reinterpret_cast<Rect_t2360479859 *>(__this + 1);
	return Rect_Equals_m866904981(_thisAdjusted, ___other0, method);
}
// System.String UnityEngine.Rect::ToString()
extern "C"  String_t* Rect_ToString_m2230825896 (Rect_t2360479859 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Rect_ToString_m2230825896_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	{
		ObjectU5BU5D_t2843939325* L_0 = ((ObjectU5BU5D_t2843939325*)SZArrayNew(ObjectU5BU5D_t2843939325_il2cpp_TypeInfo_var, (uint32_t)4));
		float L_1 = Rect_get_x_m3218181674(__this, /*hidden argument*/NULL);
		float L_2 = L_1;
		Il2CppObject * L_3 = Box(Single_t1397266774_il2cpp_TypeInfo_var, &L_2);
		NullCheck(L_0);
		ArrayElementTypeCheck (L_0, L_3);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_3);
		ObjectU5BU5D_t2843939325* L_4 = L_0;
		float L_5 = Rect_get_y_m3218181675(__this, /*hidden argument*/NULL);
		float L_6 = L_5;
		Il2CppObject * L_7 = Box(Single_t1397266774_il2cpp_TypeInfo_var, &L_6);
		NullCheck(L_4);
		ArrayElementTypeCheck (L_4, L_7);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_7);
		ObjectU5BU5D_t2843939325* L_8 = L_4;
		float L_9 = Rect_get_width_m3421965717(__this, /*hidden argument*/NULL);
		float L_10 = L_9;
		Il2CppObject * L_11 = Box(Single_t1397266774_il2cpp_TypeInfo_var, &L_10);
		NullCheck(L_8);
		ArrayElementTypeCheck (L_8, L_11);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)L_11);
		ObjectU5BU5D_t2843939325* L_12 = L_8;
		float L_13 = Rect_get_height_m977101306(__this, /*hidden argument*/NULL);
		float L_14 = L_13;
		Il2CppObject * L_15 = Box(Single_t1397266774_il2cpp_TypeInfo_var, &L_14);
		NullCheck(L_12);
		ArrayElementTypeCheck (L_12, L_15);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_15);
		String_t* L_16 = UnityString_Format_m3741272017(NULL /*static, unused*/, _stringLiteral736029419, L_12, /*hidden argument*/NULL);
		V_0 = L_16;
		goto IL_004f;
	}

IL_004f:
	{
		String_t* L_17 = V_0;
		return L_17;
	}
}
extern "C"  String_t* Rect_ToString_m2230825896_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Rect_t2360479859 * _thisAdjusted = reinterpret_cast<Rect_t2360479859 *>(__this + 1);
	return Rect_ToString_m2230825896(_thisAdjusted, method);
}
// Conversion methods for marshalling of: UnityEngine.RectOffset
extern "C" void RectOffset_t1369453676_marshal_pinvoke(const RectOffset_t1369453676& unmarshaled, RectOffset_t1369453676_marshaled_pinvoke& marshaled)
{
	marshaled.___m_Ptr_0 = reinterpret_cast<intptr_t>((unmarshaled.get_m_Ptr_0()).get_m_value_0());
	if (unmarshaled.get_m_SourceStyle_1() != NULL)
	{
		if ((unmarshaled.get_m_SourceStyle_1())->klass->is_import_or_windows_runtime)
		{
			il2cpp_hresult_t hr = ((Il2CppComObject *)unmarshaled.get_m_SourceStyle_1())->identity->QueryInterface(Il2CppIUnknown::IID, reinterpret_cast<void**>(&marshaled.___m_SourceStyle_1));
			il2cpp_codegen_com_raise_exception_if_failed(hr, false);
		}
		else
		{
			marshaled.___m_SourceStyle_1 = il2cpp_codegen_com_get_or_create_ccw<Il2CppIUnknown>(unmarshaled.get_m_SourceStyle_1());
		}
	}
	else
	{
		marshaled.___m_SourceStyle_1 = NULL;
	}
}
extern "C" void RectOffset_t1369453676_marshal_pinvoke_back(const RectOffset_t1369453676_marshaled_pinvoke& marshaled, RectOffset_t1369453676& unmarshaled)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (RectOffset_t1369453676_pinvoke_FromNativeMethodDefinition_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	IntPtr_t unmarshaled_m_Ptr_temp_0;
	memset(&unmarshaled_m_Ptr_temp_0, 0, sizeof(unmarshaled_m_Ptr_temp_0));
	IntPtr_t unmarshaled_m_Ptr_temp_0_temp;
	unmarshaled_m_Ptr_temp_0_temp.set_m_value_0(reinterpret_cast<void*>((intptr_t)(marshaled.___m_Ptr_0)));
	unmarshaled_m_Ptr_temp_0 = unmarshaled_m_Ptr_temp_0_temp;
	unmarshaled.set_m_Ptr_0(unmarshaled_m_Ptr_temp_0);
	if (marshaled.___m_SourceStyle_1 != NULL)
	{
		unmarshaled.set_m_SourceStyle_1(il2cpp_codegen_com_get_or_create_rcw_from_iunknown<Il2CppObject>(marshaled.___m_SourceStyle_1, Il2CppComObject_il2cpp_TypeInfo_var));
	}
	else
	{
		unmarshaled.set_m_SourceStyle_1(NULL);
	}
}
// Conversion method for clean up from marshalling of: UnityEngine.RectOffset
extern "C" void RectOffset_t1369453676_marshal_pinvoke_cleanup(RectOffset_t1369453676_marshaled_pinvoke& marshaled)
{
	if (marshaled.___m_SourceStyle_1 != NULL)
	{
		(marshaled.___m_SourceStyle_1)->Release();
		marshaled.___m_SourceStyle_1 = NULL;
	}
}
// Conversion methods for marshalling of: UnityEngine.RectOffset
extern "C" void RectOffset_t1369453676_marshal_com(const RectOffset_t1369453676& unmarshaled, RectOffset_t1369453676_marshaled_com& marshaled)
{
	marshaled.___m_Ptr_0 = reinterpret_cast<intptr_t>((unmarshaled.get_m_Ptr_0()).get_m_value_0());
	if (unmarshaled.get_m_SourceStyle_1() != NULL)
	{
		if ((unmarshaled.get_m_SourceStyle_1())->klass->is_import_or_windows_runtime)
		{
			il2cpp_hresult_t hr = ((Il2CppComObject *)unmarshaled.get_m_SourceStyle_1())->identity->QueryInterface(Il2CppIUnknown::IID, reinterpret_cast<void**>(&marshaled.___m_SourceStyle_1));
			il2cpp_codegen_com_raise_exception_if_failed(hr, true);
		}
		else
		{
			marshaled.___m_SourceStyle_1 = il2cpp_codegen_com_get_or_create_ccw<Il2CppIUnknown>(unmarshaled.get_m_SourceStyle_1());
		}
	}
	else
	{
		marshaled.___m_SourceStyle_1 = NULL;
	}
}
extern "C" void RectOffset_t1369453676_marshal_com_back(const RectOffset_t1369453676_marshaled_com& marshaled, RectOffset_t1369453676& unmarshaled)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (RectOffset_t1369453676_com_FromNativeMethodDefinition_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	IntPtr_t unmarshaled_m_Ptr_temp_0;
	memset(&unmarshaled_m_Ptr_temp_0, 0, sizeof(unmarshaled_m_Ptr_temp_0));
	IntPtr_t unmarshaled_m_Ptr_temp_0_temp;
	unmarshaled_m_Ptr_temp_0_temp.set_m_value_0(reinterpret_cast<void*>((intptr_t)(marshaled.___m_Ptr_0)));
	unmarshaled_m_Ptr_temp_0 = unmarshaled_m_Ptr_temp_0_temp;
	unmarshaled.set_m_Ptr_0(unmarshaled_m_Ptr_temp_0);
	if (marshaled.___m_SourceStyle_1 != NULL)
	{
		unmarshaled.set_m_SourceStyle_1(il2cpp_codegen_com_get_or_create_rcw_from_iunknown<Il2CppObject>(marshaled.___m_SourceStyle_1, Il2CppComObject_il2cpp_TypeInfo_var));
	}
	else
	{
		unmarshaled.set_m_SourceStyle_1(NULL);
	}
}
// Conversion method for clean up from marshalling of: UnityEngine.RectOffset
extern "C" void RectOffset_t1369453676_marshal_com_cleanup(RectOffset_t1369453676_marshaled_com& marshaled)
{
	if (marshaled.___m_SourceStyle_1 != NULL)
	{
		(marshaled.___m_SourceStyle_1)->Release();
		marshaled.___m_SourceStyle_1 = NULL;
	}
}
// System.Void UnityEngine.RectOffset::.ctor()
extern "C"  void RectOffset__ctor_m2446822316 (RectOffset_t1369453676 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m297566312(__this, /*hidden argument*/NULL);
		RectOffset_Init_m3786816665(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.RectOffset::.ctor(System.Object,System.IntPtr)
extern "C"  void RectOffset__ctor_m1319932452 (RectOffset_t1369453676 * __this, Il2CppObject * ___sourceStyle0, IntPtr_t ___source1, const MethodInfo* method)
{
	{
		Object__ctor_m297566312(__this, /*hidden argument*/NULL);
		Il2CppObject * L_0 = ___sourceStyle0;
		__this->set_m_SourceStyle_1(L_0);
		IntPtr_t L_1 = ___source1;
		__this->set_m_Ptr_0(L_1);
		return;
	}
}
// System.Void UnityEngine.RectOffset::Init()
extern "C"  void RectOffset_Init_m3786816665 (RectOffset_t1369453676 * __this, const MethodInfo* method)
{
	typedef void (*RectOffset_Init_m3786816665_ftn) (RectOffset_t1369453676 *);
	static RectOffset_Init_m3786816665_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectOffset_Init_m3786816665_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectOffset::Init()");
	_il2cpp_icall_func(__this);
}
// System.Void UnityEngine.RectOffset::Cleanup()
extern "C"  void RectOffset_Cleanup_m83064950 (RectOffset_t1369453676 * __this, const MethodInfo* method)
{
	typedef void (*RectOffset_Cleanup_m83064950_ftn) (RectOffset_t1369453676 *);
	static RectOffset_Cleanup_m83064950_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectOffset_Cleanup_m83064950_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectOffset::Cleanup()");
	_il2cpp_icall_func(__this);
}
// System.Int32 UnityEngine.RectOffset::get_left()
extern "C"  int32_t RectOffset_get_left_m1621853196 (RectOffset_t1369453676 * __this, const MethodInfo* method)
{
	typedef int32_t (*RectOffset_get_left_m1621853196_ftn) (RectOffset_t1369453676 *);
	static RectOffset_get_left_m1621853196_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectOffset_get_left_m1621853196_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectOffset::get_left()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.RectOffset::set_left(System.Int32)
extern "C"  void RectOffset_set_left_m1696616647 (RectOffset_t1369453676 * __this, int32_t ___value0, const MethodInfo* method)
{
	typedef void (*RectOffset_set_left_m1696616647_ftn) (RectOffset_t1369453676 *, int32_t);
	static RectOffset_set_left_m1696616647_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectOffset_set_left_m1696616647_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectOffset::set_left(System.Int32)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Int32 UnityEngine.RectOffset::get_right()
extern "C"  int32_t RectOffset_get_right_m1276944919 (RectOffset_t1369453676 * __this, const MethodInfo* method)
{
	typedef int32_t (*RectOffset_get_right_m1276944919_ftn) (RectOffset_t1369453676 *);
	static RectOffset_get_right_m1276944919_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectOffset_get_right_m1276944919_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectOffset::get_right()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.RectOffset::set_right(System.Int32)
extern "C"  void RectOffset_set_right_m3458453237 (RectOffset_t1369453676 * __this, int32_t ___value0, const MethodInfo* method)
{
	typedef void (*RectOffset_set_right_m3458453237_ftn) (RectOffset_t1369453676 *, int32_t);
	static RectOffset_set_right_m3458453237_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectOffset_set_right_m3458453237_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectOffset::set_right(System.Int32)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Int32 UnityEngine.RectOffset::get_top()
extern "C"  int32_t RectOffset_get_top_m2983626523 (RectOffset_t1369453676 * __this, const MethodInfo* method)
{
	typedef int32_t (*RectOffset_get_top_m2983626523_ftn) (RectOffset_t1369453676 *);
	static RectOffset_get_top_m2983626523_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectOffset_get_top_m2983626523_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectOffset::get_top()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.RectOffset::set_top(System.Int32)
extern "C"  void RectOffset_set_top_m1791258838 (RectOffset_t1369453676 * __this, int32_t ___value0, const MethodInfo* method)
{
	typedef void (*RectOffset_set_top_m1791258838_ftn) (RectOffset_t1369453676 *, int32_t);
	static RectOffset_set_top_m1791258838_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectOffset_set_top_m1791258838_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectOffset::set_top(System.Int32)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Int32 UnityEngine.RectOffset::get_bottom()
extern "C"  int32_t RectOffset_get_bottom_m512985341 (RectOffset_t1369453676 * __this, const MethodInfo* method)
{
	typedef int32_t (*RectOffset_get_bottom_m512985341_ftn) (RectOffset_t1369453676 *);
	static RectOffset_get_bottom_m512985341_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectOffset_get_bottom_m512985341_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectOffset::get_bottom()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.RectOffset::set_bottom(System.Int32)
extern "C"  void RectOffset_set_bottom_m4044310107 (RectOffset_t1369453676 * __this, int32_t ___value0, const MethodInfo* method)
{
	typedef void (*RectOffset_set_bottom_m4044310107_ftn) (RectOffset_t1369453676 *, int32_t);
	static RectOffset_set_bottom_m4044310107_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectOffset_set_bottom_m4044310107_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectOffset::set_bottom(System.Int32)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Int32 UnityEngine.RectOffset::get_horizontal()
extern "C"  int32_t RectOffset_get_horizontal_m4040981669 (RectOffset_t1369453676 * __this, const MethodInfo* method)
{
	typedef int32_t (*RectOffset_get_horizontal_m4040981669_ftn) (RectOffset_t1369453676 *);
	static RectOffset_get_horizontal_m4040981669_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectOffset_get_horizontal_m4040981669_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectOffset::get_horizontal()");
	return _il2cpp_icall_func(__this);
}
// System.Int32 UnityEngine.RectOffset::get_vertical()
extern "C"  int32_t RectOffset_get_vertical_m481877294 (RectOffset_t1369453676 * __this, const MethodInfo* method)
{
	typedef int32_t (*RectOffset_get_vertical_m481877294_ftn) (RectOffset_t1369453676 *);
	static RectOffset_get_vertical_m481877294_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectOffset_get_vertical_m481877294_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectOffset::get_vertical()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.RectOffset::Finalize()
extern "C"  void RectOffset_Finalize_m3590399475 (RectOffset_t1369453676 * __this, const MethodInfo* method)
{
	Exception_t1436737249 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1436737249 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
	}

IL_0001:
	try
	{ // begin try (depth: 1)
		{
			Il2CppObject * L_0 = __this->get_m_SourceStyle_1();
			if (L_0)
			{
				goto IL_0012;
			}
		}

IL_000c:
		{
			RectOffset_Cleanup_m83064950(__this, /*hidden argument*/NULL);
		}

IL_0012:
		{
			IL2CPP_LEAVE(0x1E, FINALLY_0017);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1436737249 *)e.ex;
		goto FINALLY_0017;
	}

FINALLY_0017:
	{ // begin finally (depth: 1)
		Object_Finalize_m3076187857(__this, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(23)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(23)
	{
		IL2CPP_JUMP_TBL(0x1E, IL_001e)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1436737249 *)
	}

IL_001e:
	{
		return;
	}
}
// System.String UnityEngine.RectOffset::ToString()
extern "C"  String_t* RectOffset_ToString_m3907725150 (RectOffset_t1369453676 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (RectOffset_ToString_m3907725150_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	{
		ObjectU5BU5D_t2843939325* L_0 = ((ObjectU5BU5D_t2843939325*)SZArrayNew(ObjectU5BU5D_t2843939325_il2cpp_TypeInfo_var, (uint32_t)4));
		int32_t L_1 = RectOffset_get_left_m1621853196(__this, /*hidden argument*/NULL);
		int32_t L_2 = L_1;
		Il2CppObject * L_3 = Box(Int32_t2950945753_il2cpp_TypeInfo_var, &L_2);
		NullCheck(L_0);
		ArrayElementTypeCheck (L_0, L_3);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_3);
		ObjectU5BU5D_t2843939325* L_4 = L_0;
		int32_t L_5 = RectOffset_get_right_m1276944919(__this, /*hidden argument*/NULL);
		int32_t L_6 = L_5;
		Il2CppObject * L_7 = Box(Int32_t2950945753_il2cpp_TypeInfo_var, &L_6);
		NullCheck(L_4);
		ArrayElementTypeCheck (L_4, L_7);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_7);
		ObjectU5BU5D_t2843939325* L_8 = L_4;
		int32_t L_9 = RectOffset_get_top_m2983626523(__this, /*hidden argument*/NULL);
		int32_t L_10 = L_9;
		Il2CppObject * L_11 = Box(Int32_t2950945753_il2cpp_TypeInfo_var, &L_10);
		NullCheck(L_8);
		ArrayElementTypeCheck (L_8, L_11);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)L_11);
		ObjectU5BU5D_t2843939325* L_12 = L_8;
		int32_t L_13 = RectOffset_get_bottom_m512985341(__this, /*hidden argument*/NULL);
		int32_t L_14 = L_13;
		Il2CppObject * L_15 = Box(Int32_t2950945753_il2cpp_TypeInfo_var, &L_14);
		NullCheck(L_12);
		ArrayElementTypeCheck (L_12, L_15);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_15);
		String_t* L_16 = UnityString_Format_m3741272017(NULL /*static, unused*/, _stringLiteral376327292, L_12, /*hidden argument*/NULL);
		V_0 = L_16;
		goto IL_004f;
	}

IL_004f:
	{
		String_t* L_17 = V_0;
		return L_17;
	}
}
// UnityEngine.Rect UnityEngine.RectTransform::get_rect()
extern "C"  Rect_t2360479859  RectTransform_get_rect_m1643570810 (RectTransform_t3704657025 * __this, const MethodInfo* method)
{
	Rect_t2360479859  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Rect_t2360479859  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		RectTransform_INTERNAL_get_rect_m1623267656(__this, (&V_0), /*hidden argument*/NULL);
		Rect_t2360479859  L_0 = V_0;
		V_1 = L_0;
		goto IL_0010;
	}

IL_0010:
	{
		Rect_t2360479859  L_1 = V_1;
		return L_1;
	}
}
// System.Void UnityEngine.RectTransform::INTERNAL_get_rect(UnityEngine.Rect&)
extern "C"  void RectTransform_INTERNAL_get_rect_m1623267656 (RectTransform_t3704657025 * __this, Rect_t2360479859 * ___value0, const MethodInfo* method)
{
	typedef void (*RectTransform_INTERNAL_get_rect_m1623267656_ftn) (RectTransform_t3704657025 *, Rect_t2360479859 *);
	static RectTransform_INTERNAL_get_rect_m1623267656_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectTransform_INTERNAL_get_rect_m1623267656_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectTransform::INTERNAL_get_rect(UnityEngine.Rect&)");
	_il2cpp_icall_func(__this, ___value0);
}
// UnityEngine.Vector2 UnityEngine.RectTransform::get_anchorMin()
extern "C"  Vector2_t2156229523  RectTransform_get_anchorMin_m1342910522 (RectTransform_t3704657025 * __this, const MethodInfo* method)
{
	Vector2_t2156229523  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector2_t2156229523  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		RectTransform_INTERNAL_get_anchorMin_m1256957125(__this, (&V_0), /*hidden argument*/NULL);
		Vector2_t2156229523  L_0 = V_0;
		V_1 = L_0;
		goto IL_0010;
	}

IL_0010:
	{
		Vector2_t2156229523  L_1 = V_1;
		return L_1;
	}
}
// System.Void UnityEngine.RectTransform::set_anchorMin(UnityEngine.Vector2)
extern "C"  void RectTransform_set_anchorMin_m2068858122 (RectTransform_t3704657025 * __this, Vector2_t2156229523  ___value0, const MethodInfo* method)
{
	{
		RectTransform_INTERNAL_set_anchorMin_m776038519(__this, (&___value0), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.RectTransform::INTERNAL_get_anchorMin(UnityEngine.Vector2&)
extern "C"  void RectTransform_INTERNAL_get_anchorMin_m1256957125 (RectTransform_t3704657025 * __this, Vector2_t2156229523 * ___value0, const MethodInfo* method)
{
	typedef void (*RectTransform_INTERNAL_get_anchorMin_m1256957125_ftn) (RectTransform_t3704657025 *, Vector2_t2156229523 *);
	static RectTransform_INTERNAL_get_anchorMin_m1256957125_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectTransform_INTERNAL_get_anchorMin_m1256957125_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectTransform::INTERNAL_get_anchorMin(UnityEngine.Vector2&)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.RectTransform::INTERNAL_set_anchorMin(UnityEngine.Vector2&)
extern "C"  void RectTransform_INTERNAL_set_anchorMin_m776038519 (RectTransform_t3704657025 * __this, Vector2_t2156229523 * ___value0, const MethodInfo* method)
{
	typedef void (*RectTransform_INTERNAL_set_anchorMin_m776038519_ftn) (RectTransform_t3704657025 *, Vector2_t2156229523 *);
	static RectTransform_INTERNAL_set_anchorMin_m776038519_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectTransform_INTERNAL_set_anchorMin_m776038519_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectTransform::INTERNAL_set_anchorMin(UnityEngine.Vector2&)");
	_il2cpp_icall_func(__this, ___value0);
}
// UnityEngine.Vector2 UnityEngine.RectTransform::get_anchorMax()
extern "C"  Vector2_t2156229523  RectTransform_get_anchorMax_m1075940194 (RectTransform_t3704657025 * __this, const MethodInfo* method)
{
	Vector2_t2156229523  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector2_t2156229523  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		RectTransform_INTERNAL_get_anchorMax_m2950850437(__this, (&V_0), /*hidden argument*/NULL);
		Vector2_t2156229523  L_0 = V_0;
		V_1 = L_0;
		goto IL_0010;
	}

IL_0010:
	{
		Vector2_t2156229523  L_1 = V_1;
		return L_1;
	}
}
// System.Void UnityEngine.RectTransform::set_anchorMax(UnityEngine.Vector2)
extern "C"  void RectTransform_set_anchorMax_m2389806509 (RectTransform_t3704657025 * __this, Vector2_t2156229523  ___value0, const MethodInfo* method)
{
	{
		RectTransform_INTERNAL_set_anchorMax_m4025114487(__this, (&___value0), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.RectTransform::INTERNAL_get_anchorMax(UnityEngine.Vector2&)
extern "C"  void RectTransform_INTERNAL_get_anchorMax_m2950850437 (RectTransform_t3704657025 * __this, Vector2_t2156229523 * ___value0, const MethodInfo* method)
{
	typedef void (*RectTransform_INTERNAL_get_anchorMax_m2950850437_ftn) (RectTransform_t3704657025 *, Vector2_t2156229523 *);
	static RectTransform_INTERNAL_get_anchorMax_m2950850437_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectTransform_INTERNAL_get_anchorMax_m2950850437_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectTransform::INTERNAL_get_anchorMax(UnityEngine.Vector2&)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.RectTransform::INTERNAL_set_anchorMax(UnityEngine.Vector2&)
extern "C"  void RectTransform_INTERNAL_set_anchorMax_m4025114487 (RectTransform_t3704657025 * __this, Vector2_t2156229523 * ___value0, const MethodInfo* method)
{
	typedef void (*RectTransform_INTERNAL_set_anchorMax_m4025114487_ftn) (RectTransform_t3704657025 *, Vector2_t2156229523 *);
	static RectTransform_INTERNAL_set_anchorMax_m4025114487_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectTransform_INTERNAL_set_anchorMax_m4025114487_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectTransform::INTERNAL_set_anchorMax(UnityEngine.Vector2&)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.RectTransform::set_anchoredPosition3D(UnityEngine.Vector3)
extern "C"  void RectTransform_set_anchoredPosition3D_m4046877666 (RectTransform_t3704657025 * __this, Vector3_t3722313464  ___value0, const MethodInfo* method)
{
	Vector3_t3722313464  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		float L_0 = (&___value0)->get_x_1();
		float L_1 = (&___value0)->get_y_2();
		Vector2_t2156229523  L_2;
		memset(&L_2, 0, sizeof(L_2));
		Vector2__ctor_m4060800441(&L_2, L_0, L_1, /*hidden argument*/NULL);
		RectTransform_set_anchoredPosition_m1454079598(__this, L_2, /*hidden argument*/NULL);
		Vector3_t3722313464  L_3 = Transform_get_localPosition_m265057664(__this, /*hidden argument*/NULL);
		V_0 = L_3;
		float L_4 = (&___value0)->get_z_3();
		(&V_0)->set_z_3(L_4);
		Vector3_t3722313464  L_5 = V_0;
		Transform_set_localPosition_m3327877514(__this, L_5, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Vector2 UnityEngine.RectTransform::get_anchoredPosition()
extern "C"  Vector2_t2156229523  RectTransform_get_anchoredPosition_m287009682 (RectTransform_t3704657025 * __this, const MethodInfo* method)
{
	Vector2_t2156229523  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector2_t2156229523  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		RectTransform_INTERNAL_get_anchoredPosition_m918664281(__this, (&V_0), /*hidden argument*/NULL);
		Vector2_t2156229523  L_0 = V_0;
		V_1 = L_0;
		goto IL_0010;
	}

IL_0010:
	{
		Vector2_t2156229523  L_1 = V_1;
		return L_1;
	}
}
// System.Void UnityEngine.RectTransform::set_anchoredPosition(UnityEngine.Vector2)
extern "C"  void RectTransform_set_anchoredPosition_m1454079598 (RectTransform_t3704657025 * __this, Vector2_t2156229523  ___value0, const MethodInfo* method)
{
	{
		RectTransform_INTERNAL_set_anchoredPosition_m3924630850(__this, (&___value0), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.RectTransform::INTERNAL_get_anchoredPosition(UnityEngine.Vector2&)
extern "C"  void RectTransform_INTERNAL_get_anchoredPosition_m918664281 (RectTransform_t3704657025 * __this, Vector2_t2156229523 * ___value0, const MethodInfo* method)
{
	typedef void (*RectTransform_INTERNAL_get_anchoredPosition_m918664281_ftn) (RectTransform_t3704657025 *, Vector2_t2156229523 *);
	static RectTransform_INTERNAL_get_anchoredPosition_m918664281_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectTransform_INTERNAL_get_anchoredPosition_m918664281_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectTransform::INTERNAL_get_anchoredPosition(UnityEngine.Vector2&)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.RectTransform::INTERNAL_set_anchoredPosition(UnityEngine.Vector2&)
extern "C"  void RectTransform_INTERNAL_set_anchoredPosition_m3924630850 (RectTransform_t3704657025 * __this, Vector2_t2156229523 * ___value0, const MethodInfo* method)
{
	typedef void (*RectTransform_INTERNAL_set_anchoredPosition_m3924630850_ftn) (RectTransform_t3704657025 *, Vector2_t2156229523 *);
	static RectTransform_INTERNAL_set_anchoredPosition_m3924630850_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectTransform_INTERNAL_set_anchoredPosition_m3924630850_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectTransform::INTERNAL_set_anchoredPosition(UnityEngine.Vector2&)");
	_il2cpp_icall_func(__this, ___value0);
}
// UnityEngine.Vector2 UnityEngine.RectTransform::get_sizeDelta()
extern "C"  Vector2_t2156229523  RectTransform_get_sizeDelta_m2136908840 (RectTransform_t3704657025 * __this, const MethodInfo* method)
{
	Vector2_t2156229523  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector2_t2156229523  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		RectTransform_INTERNAL_get_sizeDelta_m627343153(__this, (&V_0), /*hidden argument*/NULL);
		Vector2_t2156229523  L_0 = V_0;
		V_1 = L_0;
		goto IL_0010;
	}

IL_0010:
	{
		Vector2_t2156229523  L_1 = V_1;
		return L_1;
	}
}
// System.Void UnityEngine.RectTransform::set_sizeDelta(UnityEngine.Vector2)
extern "C"  void RectTransform_set_sizeDelta_m344906562 (RectTransform_t3704657025 * __this, Vector2_t2156229523  ___value0, const MethodInfo* method)
{
	{
		RectTransform_INTERNAL_set_sizeDelta_m2933696437(__this, (&___value0), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.RectTransform::INTERNAL_get_sizeDelta(UnityEngine.Vector2&)
extern "C"  void RectTransform_INTERNAL_get_sizeDelta_m627343153 (RectTransform_t3704657025 * __this, Vector2_t2156229523 * ___value0, const MethodInfo* method)
{
	typedef void (*RectTransform_INTERNAL_get_sizeDelta_m627343153_ftn) (RectTransform_t3704657025 *, Vector2_t2156229523 *);
	static RectTransform_INTERNAL_get_sizeDelta_m627343153_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectTransform_INTERNAL_get_sizeDelta_m627343153_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectTransform::INTERNAL_get_sizeDelta(UnityEngine.Vector2&)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.RectTransform::INTERNAL_set_sizeDelta(UnityEngine.Vector2&)
extern "C"  void RectTransform_INTERNAL_set_sizeDelta_m2933696437 (RectTransform_t3704657025 * __this, Vector2_t2156229523 * ___value0, const MethodInfo* method)
{
	typedef void (*RectTransform_INTERNAL_set_sizeDelta_m2933696437_ftn) (RectTransform_t3704657025 *, Vector2_t2156229523 *);
	static RectTransform_INTERNAL_set_sizeDelta_m2933696437_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectTransform_INTERNAL_set_sizeDelta_m2933696437_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectTransform::INTERNAL_set_sizeDelta(UnityEngine.Vector2&)");
	_il2cpp_icall_func(__this, ___value0);
}
// UnityEngine.Vector2 UnityEngine.RectTransform::get_pivot()
extern "C"  Vector2_t2156229523  RectTransform_get_pivot_m1676241928 (RectTransform_t3704657025 * __this, const MethodInfo* method)
{
	Vector2_t2156229523  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector2_t2156229523  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		RectTransform_INTERNAL_get_pivot_m878744508(__this, (&V_0), /*hidden argument*/NULL);
		Vector2_t2156229523  L_0 = V_0;
		V_1 = L_0;
		goto IL_0010;
	}

IL_0010:
	{
		Vector2_t2156229523  L_1 = V_1;
		return L_1;
	}
}
// System.Void UnityEngine.RectTransform::set_pivot(UnityEngine.Vector2)
extern "C"  void RectTransform_set_pivot_m3678254456 (RectTransform_t3704657025 * __this, Vector2_t2156229523  ___value0, const MethodInfo* method)
{
	{
		RectTransform_INTERNAL_set_pivot_m1286097822(__this, (&___value0), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.RectTransform::INTERNAL_get_pivot(UnityEngine.Vector2&)
extern "C"  void RectTransform_INTERNAL_get_pivot_m878744508 (RectTransform_t3704657025 * __this, Vector2_t2156229523 * ___value0, const MethodInfo* method)
{
	typedef void (*RectTransform_INTERNAL_get_pivot_m878744508_ftn) (RectTransform_t3704657025 *, Vector2_t2156229523 *);
	static RectTransform_INTERNAL_get_pivot_m878744508_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectTransform_INTERNAL_get_pivot_m878744508_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectTransform::INTERNAL_get_pivot(UnityEngine.Vector2&)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.RectTransform::INTERNAL_set_pivot(UnityEngine.Vector2&)
extern "C"  void RectTransform_INTERNAL_set_pivot_m1286097822 (RectTransform_t3704657025 * __this, Vector2_t2156229523 * ___value0, const MethodInfo* method)
{
	typedef void (*RectTransform_INTERNAL_set_pivot_m1286097822_ftn) (RectTransform_t3704657025 *, Vector2_t2156229523 *);
	static RectTransform_INTERNAL_set_pivot_m1286097822_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectTransform_INTERNAL_set_pivot_m1286097822_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectTransform::INTERNAL_set_pivot(UnityEngine.Vector2&)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.RectTransform::add_reapplyDrivenProperties(UnityEngine.RectTransform/ReapplyDrivenProperties)
extern "C"  void RectTransform_add_reapplyDrivenProperties_m1599223970 (Il2CppObject * __this /* static, unused */, ReapplyDrivenProperties_t1258266594 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (RectTransform_add_reapplyDrivenProperties_m1599223970_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ReapplyDrivenProperties_t1258266594 * V_0 = NULL;
	ReapplyDrivenProperties_t1258266594 * V_1 = NULL;
	{
		ReapplyDrivenProperties_t1258266594 * L_0 = ((RectTransform_t3704657025_StaticFields*)RectTransform_t3704657025_il2cpp_TypeInfo_var->static_fields)->get_reapplyDrivenProperties_2();
		V_0 = L_0;
	}

IL_0006:
	{
		ReapplyDrivenProperties_t1258266594 * L_1 = V_0;
		V_1 = L_1;
		ReapplyDrivenProperties_t1258266594 * L_2 = V_1;
		ReapplyDrivenProperties_t1258266594 * L_3 = ___value0;
		Delegate_t1188392813 * L_4 = Delegate_Combine_m1859655160(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		ReapplyDrivenProperties_t1258266594 * L_5 = V_0;
		ReapplyDrivenProperties_t1258266594 * L_6 = InterlockedCompareExchangeImpl<ReapplyDrivenProperties_t1258266594 *>((((RectTransform_t3704657025_StaticFields*)RectTransform_t3704657025_il2cpp_TypeInfo_var->static_fields)->get_address_of_reapplyDrivenProperties_2()), ((ReapplyDrivenProperties_t1258266594 *)CastclassSealed(L_4, ReapplyDrivenProperties_t1258266594_il2cpp_TypeInfo_var)), L_5);
		V_0 = L_6;
		ReapplyDrivenProperties_t1258266594 * L_7 = V_0;
		ReapplyDrivenProperties_t1258266594 * L_8 = V_1;
		if ((!(((Il2CppObject*)(ReapplyDrivenProperties_t1258266594 *)L_7) == ((Il2CppObject*)(ReapplyDrivenProperties_t1258266594 *)L_8))))
		{
			goto IL_0006;
		}
	}
	{
		return;
	}
}
// System.Void UnityEngine.RectTransform::remove_reapplyDrivenProperties(UnityEngine.RectTransform/ReapplyDrivenProperties)
extern "C"  void RectTransform_remove_reapplyDrivenProperties_m3160202629 (Il2CppObject * __this /* static, unused */, ReapplyDrivenProperties_t1258266594 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (RectTransform_remove_reapplyDrivenProperties_m3160202629_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ReapplyDrivenProperties_t1258266594 * V_0 = NULL;
	ReapplyDrivenProperties_t1258266594 * V_1 = NULL;
	{
		ReapplyDrivenProperties_t1258266594 * L_0 = ((RectTransform_t3704657025_StaticFields*)RectTransform_t3704657025_il2cpp_TypeInfo_var->static_fields)->get_reapplyDrivenProperties_2();
		V_0 = L_0;
	}

IL_0006:
	{
		ReapplyDrivenProperties_t1258266594 * L_1 = V_0;
		V_1 = L_1;
		ReapplyDrivenProperties_t1258266594 * L_2 = V_1;
		ReapplyDrivenProperties_t1258266594 * L_3 = ___value0;
		Delegate_t1188392813 * L_4 = Delegate_Remove_m334097152(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		ReapplyDrivenProperties_t1258266594 * L_5 = V_0;
		ReapplyDrivenProperties_t1258266594 * L_6 = InterlockedCompareExchangeImpl<ReapplyDrivenProperties_t1258266594 *>((((RectTransform_t3704657025_StaticFields*)RectTransform_t3704657025_il2cpp_TypeInfo_var->static_fields)->get_address_of_reapplyDrivenProperties_2()), ((ReapplyDrivenProperties_t1258266594 *)CastclassSealed(L_4, ReapplyDrivenProperties_t1258266594_il2cpp_TypeInfo_var)), L_5);
		V_0 = L_6;
		ReapplyDrivenProperties_t1258266594 * L_7 = V_0;
		ReapplyDrivenProperties_t1258266594 * L_8 = V_1;
		if ((!(((Il2CppObject*)(ReapplyDrivenProperties_t1258266594 *)L_7) == ((Il2CppObject*)(ReapplyDrivenProperties_t1258266594 *)L_8))))
		{
			goto IL_0006;
		}
	}
	{
		return;
	}
}
// System.Void UnityEngine.RectTransform::SendReapplyDrivenProperties(UnityEngine.RectTransform)
extern "C"  void RectTransform_SendReapplyDrivenProperties_m3862170524 (Il2CppObject * __this /* static, unused */, RectTransform_t3704657025 * ___driven0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (RectTransform_SendReapplyDrivenProperties_m3862170524_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		ReapplyDrivenProperties_t1258266594 * L_0 = ((RectTransform_t3704657025_StaticFields*)RectTransform_t3704657025_il2cpp_TypeInfo_var->static_fields)->get_reapplyDrivenProperties_2();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		ReapplyDrivenProperties_t1258266594 * L_1 = ((RectTransform_t3704657025_StaticFields*)RectTransform_t3704657025_il2cpp_TypeInfo_var->static_fields)->get_reapplyDrivenProperties_2();
		RectTransform_t3704657025 * L_2 = ___driven0;
		NullCheck(L_1);
		ReapplyDrivenProperties_Invoke_m3357881462(L_1, L_2, /*hidden argument*/NULL);
	}

IL_0016:
	{
		return;
	}
}
// System.Void UnityEngine.RectTransform::GetLocalCorners(UnityEngine.Vector3[])
extern "C"  void RectTransform_GetLocalCorners_m3023515545 (RectTransform_t3704657025 * __this, Vector3U5BU5D_t1718750761* ___fourCornersArray0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (RectTransform_GetLocalCorners_m3023515545_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Rect_t2360479859  V_0;
	memset(&V_0, 0, sizeof(V_0));
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	float V_3 = 0.0f;
	float V_4 = 0.0f;
	{
		Vector3U5BU5D_t1718750761* L_0 = ___fourCornersArray0;
		if (!L_0)
		{
			goto IL_0010;
		}
	}
	{
		Vector3U5BU5D_t1718750761* L_1 = ___fourCornersArray0;
		NullCheck(L_1);
		if ((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_1)->max_length))))) >= ((int32_t)4)))
		{
			goto IL_0020;
		}
	}

IL_0010:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3317548046_il2cpp_TypeInfo_var);
		Debug_LogError_m2059623341(NULL /*static, unused*/, _stringLiteral4243182802, /*hidden argument*/NULL);
		goto IL_00aa;
	}

IL_0020:
	{
		Rect_t2360479859  L_2 = RectTransform_get_rect_m1643570810(__this, /*hidden argument*/NULL);
		V_0 = L_2;
		float L_3 = Rect_get_x_m3218181674((&V_0), /*hidden argument*/NULL);
		V_1 = L_3;
		float L_4 = Rect_get_y_m3218181675((&V_0), /*hidden argument*/NULL);
		V_2 = L_4;
		float L_5 = Rect_get_xMax_m2926206058((&V_0), /*hidden argument*/NULL);
		V_3 = L_5;
		float L_6 = Rect_get_yMax_m2926206027((&V_0), /*hidden argument*/NULL);
		V_4 = L_6;
		Vector3U5BU5D_t1718750761* L_7 = ___fourCornersArray0;
		NullCheck(L_7);
		float L_8 = V_1;
		float L_9 = V_2;
		Vector3_t3722313464  L_10;
		memset(&L_10, 0, sizeof(L_10));
		Vector3__ctor_m1197556204(&L_10, L_8, L_9, (0.0f), /*hidden argument*/NULL);
		(*(Vector3_t3722313464 *)((L_7)->GetAddressAt(static_cast<il2cpp_array_size_t>(0)))) = L_10;
		Vector3U5BU5D_t1718750761* L_11 = ___fourCornersArray0;
		NullCheck(L_11);
		float L_12 = V_1;
		float L_13 = V_4;
		Vector3_t3722313464  L_14;
		memset(&L_14, 0, sizeof(L_14));
		Vector3__ctor_m1197556204(&L_14, L_12, L_13, (0.0f), /*hidden argument*/NULL);
		(*(Vector3_t3722313464 *)((L_11)->GetAddressAt(static_cast<il2cpp_array_size_t>(1)))) = L_14;
		Vector3U5BU5D_t1718750761* L_15 = ___fourCornersArray0;
		NullCheck(L_15);
		float L_16 = V_3;
		float L_17 = V_4;
		Vector3_t3722313464  L_18;
		memset(&L_18, 0, sizeof(L_18));
		Vector3__ctor_m1197556204(&L_18, L_16, L_17, (0.0f), /*hidden argument*/NULL);
		(*(Vector3_t3722313464 *)((L_15)->GetAddressAt(static_cast<il2cpp_array_size_t>(2)))) = L_18;
		Vector3U5BU5D_t1718750761* L_19 = ___fourCornersArray0;
		NullCheck(L_19);
		float L_20 = V_3;
		float L_21 = V_2;
		Vector3_t3722313464  L_22;
		memset(&L_22, 0, sizeof(L_22));
		Vector3__ctor_m1197556204(&L_22, L_20, L_21, (0.0f), /*hidden argument*/NULL);
		(*(Vector3_t3722313464 *)((L_19)->GetAddressAt(static_cast<il2cpp_array_size_t>(3)))) = L_22;
	}

IL_00aa:
	{
		return;
	}
}
// System.Void UnityEngine.RectTransform::GetWorldCorners(UnityEngine.Vector3[])
extern "C"  void RectTransform_GetWorldCorners_m2125351209 (RectTransform_t3704657025 * __this, Vector3U5BU5D_t1718750761* ___fourCornersArray0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (RectTransform_GetWorldCorners_m2125351209_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Transform_t3600365921 * V_0 = NULL;
	int32_t V_1 = 0;
	{
		Vector3U5BU5D_t1718750761* L_0 = ___fourCornersArray0;
		if (!L_0)
		{
			goto IL_0010;
		}
	}
	{
		Vector3U5BU5D_t1718750761* L_1 = ___fourCornersArray0;
		NullCheck(L_1);
		if ((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_1)->max_length))))) >= ((int32_t)4)))
		{
			goto IL_0020;
		}
	}

IL_0010:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3317548046_il2cpp_TypeInfo_var);
		Debug_LogError_m2059623341(NULL /*static, unused*/, _stringLiteral13390833, /*hidden argument*/NULL);
		goto IL_005e;
	}

IL_0020:
	{
		Vector3U5BU5D_t1718750761* L_2 = ___fourCornersArray0;
		RectTransform_GetLocalCorners_m3023515545(__this, L_2, /*hidden argument*/NULL);
		Transform_t3600365921 * L_3 = Component_get_transform_m2921103810(__this, /*hidden argument*/NULL);
		V_0 = L_3;
		V_1 = 0;
		goto IL_0057;
	}

IL_0035:
	{
		Vector3U5BU5D_t1718750761* L_4 = ___fourCornersArray0;
		int32_t L_5 = V_1;
		NullCheck(L_4);
		Transform_t3600365921 * L_6 = V_0;
		Vector3U5BU5D_t1718750761* L_7 = ___fourCornersArray0;
		int32_t L_8 = V_1;
		NullCheck(L_7);
		NullCheck(L_6);
		Vector3_t3722313464  L_9 = Transform_TransformPoint_m3184824030(L_6, (*(Vector3_t3722313464 *)((L_7)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_8)))), /*hidden argument*/NULL);
		(*(Vector3_t3722313464 *)((L_4)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_5)))) = L_9;
		int32_t L_10 = V_1;
		V_1 = ((int32_t)((int32_t)L_10+(int32_t)1));
	}

IL_0057:
	{
		int32_t L_11 = V_1;
		if ((((int32_t)L_11) < ((int32_t)4)))
		{
			goto IL_0035;
		}
	}

IL_005e:
	{
		return;
	}
}
// UnityEngine.Vector2 UnityEngine.RectTransform::get_offsetMin()
extern "C"  Vector2_t2156229523  RectTransform_get_offsetMin_m3164461009 (RectTransform_t3704657025 * __this, const MethodInfo* method)
{
	Vector2_t2156229523  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Vector2_t2156229523  L_0 = RectTransform_get_anchoredPosition_m287009682(__this, /*hidden argument*/NULL);
		Vector2_t2156229523  L_1 = RectTransform_get_sizeDelta_m2136908840(__this, /*hidden argument*/NULL);
		Vector2_t2156229523  L_2 = RectTransform_get_pivot_m1676241928(__this, /*hidden argument*/NULL);
		Vector2_t2156229523  L_3 = Vector2_Scale_m110064162(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		Vector2_t2156229523  L_4 = Vector2_op_Subtraction_m1387382396(NULL /*static, unused*/, L_0, L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		goto IL_0023;
	}

IL_0023:
	{
		Vector2_t2156229523  L_5 = V_0;
		return L_5;
	}
}
// System.Void UnityEngine.RectTransform::set_offsetMin(UnityEngine.Vector2)
extern "C"  void RectTransform_set_offsetMin_m2924112 (RectTransform_t3704657025 * __this, Vector2_t2156229523  ___value0, const MethodInfo* method)
{
	Vector2_t2156229523  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Vector2_t2156229523  L_0 = ___value0;
		Vector2_t2156229523  L_1 = RectTransform_get_anchoredPosition_m287009682(__this, /*hidden argument*/NULL);
		Vector2_t2156229523  L_2 = RectTransform_get_sizeDelta_m2136908840(__this, /*hidden argument*/NULL);
		Vector2_t2156229523  L_3 = RectTransform_get_pivot_m1676241928(__this, /*hidden argument*/NULL);
		Vector2_t2156229523  L_4 = Vector2_Scale_m110064162(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		Vector2_t2156229523  L_5 = Vector2_op_Subtraction_m1387382396(NULL /*static, unused*/, L_1, L_4, /*hidden argument*/NULL);
		Vector2_t2156229523  L_6 = Vector2_op_Subtraction_m1387382396(NULL /*static, unused*/, L_0, L_5, /*hidden argument*/NULL);
		V_0 = L_6;
		Vector2_t2156229523  L_7 = RectTransform_get_sizeDelta_m2136908840(__this, /*hidden argument*/NULL);
		Vector2_t2156229523  L_8 = V_0;
		Vector2_t2156229523  L_9 = Vector2_op_Subtraction_m1387382396(NULL /*static, unused*/, L_7, L_8, /*hidden argument*/NULL);
		RectTransform_set_sizeDelta_m344906562(__this, L_9, /*hidden argument*/NULL);
		Vector2_t2156229523  L_10 = RectTransform_get_anchoredPosition_m287009682(__this, /*hidden argument*/NULL);
		Vector2_t2156229523  L_11 = V_0;
		Vector2_t2156229523  L_12 = Vector2_get_one_m3275444361(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector2_t2156229523  L_13 = RectTransform_get_pivot_m1676241928(__this, /*hidden argument*/NULL);
		Vector2_t2156229523  L_14 = Vector2_op_Subtraction_m1387382396(NULL /*static, unused*/, L_12, L_13, /*hidden argument*/NULL);
		Vector2_t2156229523  L_15 = Vector2_Scale_m110064162(NULL /*static, unused*/, L_11, L_14, /*hidden argument*/NULL);
		Vector2_t2156229523  L_16 = Vector2_op_Addition_m2157034339(NULL /*static, unused*/, L_10, L_15, /*hidden argument*/NULL);
		RectTransform_set_anchoredPosition_m1454079598(__this, L_16, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Vector2 UnityEngine.RectTransform::get_offsetMax()
extern "C"  Vector2_t2156229523  RectTransform_get_offsetMax_m2666757289 (RectTransform_t3704657025 * __this, const MethodInfo* method)
{
	Vector2_t2156229523  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Vector2_t2156229523  L_0 = RectTransform_get_anchoredPosition_m287009682(__this, /*hidden argument*/NULL);
		Vector2_t2156229523  L_1 = RectTransform_get_sizeDelta_m2136908840(__this, /*hidden argument*/NULL);
		Vector2_t2156229523  L_2 = Vector2_get_one_m3275444361(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector2_t2156229523  L_3 = RectTransform_get_pivot_m1676241928(__this, /*hidden argument*/NULL);
		Vector2_t2156229523  L_4 = Vector2_op_Subtraction_m1387382396(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		Vector2_t2156229523  L_5 = Vector2_Scale_m110064162(NULL /*static, unused*/, L_1, L_4, /*hidden argument*/NULL);
		Vector2_t2156229523  L_6 = Vector2_op_Addition_m2157034339(NULL /*static, unused*/, L_0, L_5, /*hidden argument*/NULL);
		V_0 = L_6;
		goto IL_002d;
	}

IL_002d:
	{
		Vector2_t2156229523  L_7 = V_0;
		return L_7;
	}
}
// System.Void UnityEngine.RectTransform::set_offsetMax(UnityEngine.Vector2)
extern "C"  void RectTransform_set_offsetMax_m2352202302 (RectTransform_t3704657025 * __this, Vector2_t2156229523  ___value0, const MethodInfo* method)
{
	Vector2_t2156229523  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Vector2_t2156229523  L_0 = ___value0;
		Vector2_t2156229523  L_1 = RectTransform_get_anchoredPosition_m287009682(__this, /*hidden argument*/NULL);
		Vector2_t2156229523  L_2 = RectTransform_get_sizeDelta_m2136908840(__this, /*hidden argument*/NULL);
		Vector2_t2156229523  L_3 = Vector2_get_one_m3275444361(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector2_t2156229523  L_4 = RectTransform_get_pivot_m1676241928(__this, /*hidden argument*/NULL);
		Vector2_t2156229523  L_5 = Vector2_op_Subtraction_m1387382396(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		Vector2_t2156229523  L_6 = Vector2_Scale_m110064162(NULL /*static, unused*/, L_2, L_5, /*hidden argument*/NULL);
		Vector2_t2156229523  L_7 = Vector2_op_Addition_m2157034339(NULL /*static, unused*/, L_1, L_6, /*hidden argument*/NULL);
		Vector2_t2156229523  L_8 = Vector2_op_Subtraction_m1387382396(NULL /*static, unused*/, L_0, L_7, /*hidden argument*/NULL);
		V_0 = L_8;
		Vector2_t2156229523  L_9 = RectTransform_get_sizeDelta_m2136908840(__this, /*hidden argument*/NULL);
		Vector2_t2156229523  L_10 = V_0;
		Vector2_t2156229523  L_11 = Vector2_op_Addition_m2157034339(NULL /*static, unused*/, L_9, L_10, /*hidden argument*/NULL);
		RectTransform_set_sizeDelta_m344906562(__this, L_11, /*hidden argument*/NULL);
		Vector2_t2156229523  L_12 = RectTransform_get_anchoredPosition_m287009682(__this, /*hidden argument*/NULL);
		Vector2_t2156229523  L_13 = V_0;
		Vector2_t2156229523  L_14 = RectTransform_get_pivot_m1676241928(__this, /*hidden argument*/NULL);
		Vector2_t2156229523  L_15 = Vector2_Scale_m110064162(NULL /*static, unused*/, L_13, L_14, /*hidden argument*/NULL);
		Vector2_t2156229523  L_16 = Vector2_op_Addition_m2157034339(NULL /*static, unused*/, L_12, L_15, /*hidden argument*/NULL);
		RectTransform_set_anchoredPosition_m1454079598(__this, L_16, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.RectTransform::SetInsetAndSizeFromParentEdge(UnityEngine.RectTransform/Edge,System.Single,System.Single)
extern "C"  void RectTransform_SetInsetAndSizeFromParentEdge_m133498675 (RectTransform_t3704657025 * __this, int32_t ___edge0, float ___inset1, float ___size2, const MethodInfo* method)
{
	int32_t V_0 = 0;
	bool V_1 = false;
	float V_2 = 0.0f;
	Vector2_t2156229523  V_3;
	memset(&V_3, 0, sizeof(V_3));
	Vector2_t2156229523  V_4;
	memset(&V_4, 0, sizeof(V_4));
	Vector2_t2156229523  V_5;
	memset(&V_5, 0, sizeof(V_5));
	Vector2_t2156229523  V_6;
	memset(&V_6, 0, sizeof(V_6));
	Vector2_t2156229523  V_7;
	memset(&V_7, 0, sizeof(V_7));
	int32_t G_B4_0 = 0;
	int32_t G_B7_0 = 0;
	int32_t G_B10_0 = 0;
	int32_t G_B12_0 = 0;
	Vector2_t2156229523 * G_B12_1 = NULL;
	int32_t G_B11_0 = 0;
	Vector2_t2156229523 * G_B11_1 = NULL;
	float G_B13_0 = 0.0f;
	int32_t G_B13_1 = 0;
	Vector2_t2156229523 * G_B13_2 = NULL;
	{
		int32_t L_0 = ___edge0;
		if ((((int32_t)L_0) == ((int32_t)2)))
		{
			goto IL_000f;
		}
	}
	{
		int32_t L_1 = ___edge0;
		if ((!(((uint32_t)L_1) == ((uint32_t)3))))
		{
			goto IL_0015;
		}
	}

IL_000f:
	{
		G_B4_0 = 1;
		goto IL_0016;
	}

IL_0015:
	{
		G_B4_0 = 0;
	}

IL_0016:
	{
		V_0 = G_B4_0;
		int32_t L_2 = ___edge0;
		if ((((int32_t)L_2) == ((int32_t)2)))
		{
			goto IL_0024;
		}
	}
	{
		int32_t L_3 = ___edge0;
		G_B7_0 = ((((int32_t)L_3) == ((int32_t)1))? 1 : 0);
		goto IL_0025;
	}

IL_0024:
	{
		G_B7_0 = 1;
	}

IL_0025:
	{
		V_1 = (bool)G_B7_0;
		bool L_4 = V_1;
		if (!L_4)
		{
			goto IL_0032;
		}
	}
	{
		G_B10_0 = 1;
		goto IL_0033;
	}

IL_0032:
	{
		G_B10_0 = 0;
	}

IL_0033:
	{
		V_2 = (((float)((float)G_B10_0)));
		Vector2_t2156229523  L_5 = RectTransform_get_anchorMin_m1342910522(__this, /*hidden argument*/NULL);
		V_3 = L_5;
		int32_t L_6 = V_0;
		float L_7 = V_2;
		Vector2_set_Item_m1664083694((&V_3), L_6, L_7, /*hidden argument*/NULL);
		Vector2_t2156229523  L_8 = V_3;
		RectTransform_set_anchorMin_m2068858122(__this, L_8, /*hidden argument*/NULL);
		Vector2_t2156229523  L_9 = RectTransform_get_anchorMax_m1075940194(__this, /*hidden argument*/NULL);
		V_3 = L_9;
		int32_t L_10 = V_0;
		float L_11 = V_2;
		Vector2_set_Item_m1664083694((&V_3), L_10, L_11, /*hidden argument*/NULL);
		Vector2_t2156229523  L_12 = V_3;
		RectTransform_set_anchorMax_m2389806509(__this, L_12, /*hidden argument*/NULL);
		Vector2_t2156229523  L_13 = RectTransform_get_sizeDelta_m2136908840(__this, /*hidden argument*/NULL);
		V_4 = L_13;
		int32_t L_14 = V_0;
		float L_15 = ___size2;
		Vector2_set_Item_m1664083694((&V_4), L_14, L_15, /*hidden argument*/NULL);
		Vector2_t2156229523  L_16 = V_4;
		RectTransform_set_sizeDelta_m344906562(__this, L_16, /*hidden argument*/NULL);
		Vector2_t2156229523  L_17 = RectTransform_get_anchoredPosition_m287009682(__this, /*hidden argument*/NULL);
		V_5 = L_17;
		int32_t L_18 = V_0;
		bool L_19 = V_1;
		G_B11_0 = L_18;
		G_B11_1 = (&V_5);
		if (!L_19)
		{
			G_B12_0 = L_18;
			G_B12_1 = (&V_5);
			goto IL_00ad;
		}
	}
	{
		float L_20 = ___inset1;
		float L_21 = ___size2;
		Vector2_t2156229523  L_22 = RectTransform_get_pivot_m1676241928(__this, /*hidden argument*/NULL);
		V_6 = L_22;
		int32_t L_23 = V_0;
		float L_24 = Vector2_get_Item_m3129197029((&V_6), L_23, /*hidden argument*/NULL);
		G_B13_0 = ((float)((float)((-L_20))-(float)((float)((float)L_21*(float)((float)((float)(1.0f)-(float)L_24))))));
		G_B13_1 = G_B11_0;
		G_B13_2 = G_B11_1;
		goto IL_00c1;
	}

IL_00ad:
	{
		float L_25 = ___inset1;
		float L_26 = ___size2;
		Vector2_t2156229523  L_27 = RectTransform_get_pivot_m1676241928(__this, /*hidden argument*/NULL);
		V_7 = L_27;
		int32_t L_28 = V_0;
		float L_29 = Vector2_get_Item_m3129197029((&V_7), L_28, /*hidden argument*/NULL);
		G_B13_0 = ((float)((float)L_25+(float)((float)((float)L_26*(float)L_29))));
		G_B13_1 = G_B12_0;
		G_B13_2 = G_B12_1;
	}

IL_00c1:
	{
		Vector2_set_Item_m1664083694(G_B13_2, G_B13_1, G_B13_0, /*hidden argument*/NULL);
		Vector2_t2156229523  L_30 = V_5;
		RectTransform_set_anchoredPosition_m1454079598(__this, L_30, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.RectTransform::SetSizeWithCurrentAnchors(UnityEngine.RectTransform/Axis,System.Single)
extern "C"  void RectTransform_SetSizeWithCurrentAnchors_m3629118313 (RectTransform_t3704657025 * __this, int32_t ___axis0, float ___size1, const MethodInfo* method)
{
	int32_t V_0 = 0;
	Vector2_t2156229523  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Vector2_t2156229523  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Vector2_t2156229523  V_3;
	memset(&V_3, 0, sizeof(V_3));
	Vector2_t2156229523  V_4;
	memset(&V_4, 0, sizeof(V_4));
	{
		int32_t L_0 = ___axis0;
		V_0 = L_0;
		Vector2_t2156229523  L_1 = RectTransform_get_sizeDelta_m2136908840(__this, /*hidden argument*/NULL);
		V_1 = L_1;
		int32_t L_2 = V_0;
		float L_3 = ___size1;
		Vector2_t2156229523  L_4 = RectTransform_GetParentSize_m3805664119(__this, /*hidden argument*/NULL);
		V_2 = L_4;
		int32_t L_5 = V_0;
		float L_6 = Vector2_get_Item_m3129197029((&V_2), L_5, /*hidden argument*/NULL);
		Vector2_t2156229523  L_7 = RectTransform_get_anchorMax_m1075940194(__this, /*hidden argument*/NULL);
		V_3 = L_7;
		int32_t L_8 = V_0;
		float L_9 = Vector2_get_Item_m3129197029((&V_3), L_8, /*hidden argument*/NULL);
		Vector2_t2156229523  L_10 = RectTransform_get_anchorMin_m1342910522(__this, /*hidden argument*/NULL);
		V_4 = L_10;
		int32_t L_11 = V_0;
		float L_12 = Vector2_get_Item_m3129197029((&V_4), L_11, /*hidden argument*/NULL);
		Vector2_set_Item_m1664083694((&V_1), L_2, ((float)((float)L_3-(float)((float)((float)L_6*(float)((float)((float)L_9-(float)L_12)))))), /*hidden argument*/NULL);
		Vector2_t2156229523  L_13 = V_1;
		RectTransform_set_sizeDelta_m344906562(__this, L_13, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Vector2 UnityEngine.RectTransform::GetParentSize()
extern "C"  Vector2_t2156229523  RectTransform_GetParentSize_m3805664119 (RectTransform_t3704657025 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (RectTransform_GetParentSize_m3805664119_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RectTransform_t3704657025 * V_0 = NULL;
	Vector2_t2156229523  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Rect_t2360479859  V_2;
	memset(&V_2, 0, sizeof(V_2));
	{
		Transform_t3600365921 * L_0 = Transform_get_parent_m1293647796(__this, /*hidden argument*/NULL);
		V_0 = ((RectTransform_t3704657025 *)IsInstSealed(L_0, RectTransform_t3704657025_il2cpp_TypeInfo_var));
		RectTransform_t3704657025 * L_1 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_2 = Object_op_Implicit_m487959476(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		if (L_2)
		{
			goto IL_0023;
		}
	}
	{
		Vector2_t2156229523  L_3 = Vector2_get_zero_m3700876535(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_1 = L_3;
		goto IL_0037;
	}

IL_0023:
	{
		RectTransform_t3704657025 * L_4 = V_0;
		NullCheck(L_4);
		Rect_t2360479859  L_5 = RectTransform_get_rect_m1643570810(L_4, /*hidden argument*/NULL);
		V_2 = L_5;
		Vector2_t2156229523  L_6 = Rect_get_size_m3542039952((&V_2), /*hidden argument*/NULL);
		V_1 = L_6;
		goto IL_0037;
	}

IL_0037:
	{
		Vector2_t2156229523  L_7 = V_1;
		return L_7;
	}
}
// System.Void UnityEngine.RectTransform/ReapplyDrivenProperties::.ctor(System.Object,System.IntPtr)
extern "C"  void ReapplyDrivenProperties__ctor_m3278743496 (ReapplyDrivenProperties_t1258266594 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void UnityEngine.RectTransform/ReapplyDrivenProperties::Invoke(UnityEngine.RectTransform)
extern "C"  void ReapplyDrivenProperties_Invoke_m3357881462 (ReapplyDrivenProperties_t1258266594 * __this, RectTransform_t3704657025 * ___driven0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		ReapplyDrivenProperties_Invoke_m3357881462((ReapplyDrivenProperties_t1258266594 *)__this->get_prev_9(),___driven0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, RectTransform_t3704657025 * ___driven0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___driven0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (void* __this, RectTransform_t3704657025 * ___driven0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___driven0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(___driven0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult UnityEngine.RectTransform/ReapplyDrivenProperties::BeginInvoke(UnityEngine.RectTransform,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * ReapplyDrivenProperties_BeginInvoke_m1645183702 (ReapplyDrivenProperties_t1258266594 * __this, RectTransform_t3704657025 * ___driven0, AsyncCallback_t3962456242 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	void *__d_args[2] = {0};
	__d_args[0] = ___driven0;
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void UnityEngine.RectTransform/ReapplyDrivenProperties::EndInvoke(System.IAsyncResult)
extern "C"  void ReapplyDrivenProperties_EndInvoke_m1013390948 (ReapplyDrivenProperties_t1258266594 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Boolean UnityEngine.RectTransformUtility::ScreenPointToWorldPointInRectangle(UnityEngine.RectTransform,UnityEngine.Vector2,UnityEngine.Camera,UnityEngine.Vector3&)
extern "C"  bool RectTransformUtility_ScreenPointToWorldPointInRectangle_m1236369340 (Il2CppObject * __this /* static, unused */, RectTransform_t3704657025 * ___rect0, Vector2_t2156229523  ___screenPoint1, Camera_t4157153871 * ___cam2, Vector3_t3722313464 * ___worldPoint3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (RectTransformUtility_ScreenPointToWorldPointInRectangle_m1236369340_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Ray_t3785851493  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Plane_t1000493321  V_1;
	memset(&V_1, 0, sizeof(V_1));
	float V_2 = 0.0f;
	bool V_3 = false;
	{
		Vector3_t3722313464 * L_0 = ___worldPoint3;
		Vector2_t2156229523  L_1 = Vector2_get_zero_m3700876535(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t3722313464  L_2 = Vector2_op_Implicit_m1988559315(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		(*(Vector3_t3722313464 *)L_0) = L_2;
		Camera_t4157153871 * L_3 = ___cam2;
		Vector2_t2156229523  L_4 = ___screenPoint1;
		IL2CPP_RUNTIME_CLASS_INIT(RectTransformUtility_t1743242446_il2cpp_TypeInfo_var);
		Ray_t3785851493  L_5 = RectTransformUtility_ScreenPointToRay_m3051033457(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		V_0 = L_5;
		RectTransform_t3704657025 * L_6 = ___rect0;
		NullCheck(L_6);
		Quaternion_t2301928331  L_7 = Transform_get_rotation_m2794111148(L_6, /*hidden argument*/NULL);
		Vector3_t3722313464  L_8 = Vector3_get_back_m4227631978(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t3722313464  L_9 = Quaternion_op_Multiply_m1022106983(NULL /*static, unused*/, L_7, L_8, /*hidden argument*/NULL);
		RectTransform_t3704657025 * L_10 = ___rect0;
		NullCheck(L_10);
		Vector3_t3722313464  L_11 = Transform_get_position_m102368104(L_10, /*hidden argument*/NULL);
		Plane__ctor_m2107222851((&V_1), L_9, L_11, /*hidden argument*/NULL);
		Ray_t3785851493  L_12 = V_0;
		bool L_13 = Plane_Raycast_m1361173428((&V_1), L_12, (&V_2), /*hidden argument*/NULL);
		if (L_13)
		{
			goto IL_004c;
		}
	}
	{
		V_3 = (bool)0;
		goto IL_0061;
	}

IL_004c:
	{
		Vector3_t3722313464 * L_14 = ___worldPoint3;
		float L_15 = V_2;
		Vector3_t3722313464  L_16 = Ray_GetPoint_m2674993148((&V_0), L_15, /*hidden argument*/NULL);
		(*(Vector3_t3722313464 *)L_14) = L_16;
		V_3 = (bool)1;
		goto IL_0061;
	}

IL_0061:
	{
		bool L_17 = V_3;
		return L_17;
	}
}
// System.Boolean UnityEngine.RectTransformUtility::ScreenPointToLocalPointInRectangle(UnityEngine.RectTransform,UnityEngine.Vector2,UnityEngine.Camera,UnityEngine.Vector2&)
extern "C"  bool RectTransformUtility_ScreenPointToLocalPointInRectangle_m870181352 (Il2CppObject * __this /* static, unused */, RectTransform_t3704657025 * ___rect0, Vector2_t2156229523  ___screenPoint1, Camera_t4157153871 * ___cam2, Vector2_t2156229523 * ___localPoint3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (RectTransformUtility_ScreenPointToLocalPointInRectangle_m870181352_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t3722313464  V_0;
	memset(&V_0, 0, sizeof(V_0));
	bool V_1 = false;
	{
		Vector2_t2156229523 * L_0 = ___localPoint3;
		Vector2_t2156229523  L_1 = Vector2_get_zero_m3700876535(NULL /*static, unused*/, /*hidden argument*/NULL);
		(*(Vector2_t2156229523 *)L_0) = L_1;
		RectTransform_t3704657025 * L_2 = ___rect0;
		Vector2_t2156229523  L_3 = ___screenPoint1;
		Camera_t4157153871 * L_4 = ___cam2;
		IL2CPP_RUNTIME_CLASS_INIT(RectTransformUtility_t1743242446_il2cpp_TypeInfo_var);
		bool L_5 = RectTransformUtility_ScreenPointToWorldPointInRectangle_m1236369340(NULL /*static, unused*/, L_2, L_3, L_4, (&V_0), /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_0035;
		}
	}
	{
		Vector2_t2156229523 * L_6 = ___localPoint3;
		RectTransform_t3704657025 * L_7 = ___rect0;
		Vector3_t3722313464  L_8 = V_0;
		NullCheck(L_7);
		Vector3_t3722313464  L_9 = Transform_InverseTransformPoint_m1254110475(L_7, L_8, /*hidden argument*/NULL);
		Vector2_t2156229523  L_10 = Vector2_op_Implicit_m1304503157(NULL /*static, unused*/, L_9, /*hidden argument*/NULL);
		(*(Vector2_t2156229523 *)L_6) = L_10;
		V_1 = (bool)1;
		goto IL_003c;
	}

IL_0035:
	{
		V_1 = (bool)0;
		goto IL_003c;
	}

IL_003c:
	{
		bool L_11 = V_1;
		return L_11;
	}
}
// UnityEngine.Ray UnityEngine.RectTransformUtility::ScreenPointToRay(UnityEngine.Camera,UnityEngine.Vector2)
extern "C"  Ray_t3785851493  RectTransformUtility_ScreenPointToRay_m3051033457 (Il2CppObject * __this /* static, unused */, Camera_t4157153871 * ___cam0, Vector2_t2156229523  ___screenPos1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (RectTransformUtility_ScreenPointToRay_m3051033457_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Ray_t3785851493  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector3_t3722313464  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		Camera_t4157153871 * L_0 = ___cam0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m1920811489(NULL /*static, unused*/, L_0, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001f;
		}
	}
	{
		Camera_t4157153871 * L_2 = ___cam0;
		Vector2_t2156229523  L_3 = ___screenPos1;
		Vector3_t3722313464  L_4 = Vector2_op_Implicit_m1988559315(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		NullCheck(L_2);
		Ray_t3785851493  L_5 = Camera_ScreenPointToRay_m1522780915(L_2, L_4, /*hidden argument*/NULL);
		V_0 = L_5;
		goto IL_004a;
	}

IL_001f:
	{
		Vector2_t2156229523  L_6 = ___screenPos1;
		Vector3_t3722313464  L_7 = Vector2_op_Implicit_m1988559315(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		V_1 = L_7;
		Vector3_t3722313464 * L_8 = (&V_1);
		float L_9 = L_8->get_z_3();
		L_8->set_z_3(((float)((float)L_9-(float)(100.0f))));
		Vector3_t3722313464  L_10 = V_1;
		Vector3_t3722313464  L_11 = Vector3_get_forward_m2293047824(NULL /*static, unused*/, /*hidden argument*/NULL);
		Ray_t3785851493  L_12;
		memset(&L_12, 0, sizeof(L_12));
		Ray__ctor_m2095760679(&L_12, L_10, L_11, /*hidden argument*/NULL);
		V_0 = L_12;
		goto IL_004a;
	}

IL_004a:
	{
		Ray_t3785851493  L_13 = V_0;
		return L_13;
	}
}
// System.Void UnityEngine.RectTransformUtility::FlipLayoutOnAxis(UnityEngine.RectTransform,System.Int32,System.Boolean,System.Boolean)
extern "C"  void RectTransformUtility_FlipLayoutOnAxis_m144305054 (Il2CppObject * __this /* static, unused */, RectTransform_t3704657025 * ___rect0, int32_t ___axis1, bool ___keepPositioning2, bool ___recursive3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (RectTransformUtility_FlipLayoutOnAxis_m144305054_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	RectTransform_t3704657025 * V_1 = NULL;
	Vector2_t2156229523  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Vector2_t2156229523  V_3;
	memset(&V_3, 0, sizeof(V_3));
	Vector2_t2156229523  V_4;
	memset(&V_4, 0, sizeof(V_4));
	Vector2_t2156229523  V_5;
	memset(&V_5, 0, sizeof(V_5));
	float V_6 = 0.0f;
	{
		RectTransform_t3704657025 * L_0 = ___rect0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_m1454075600(NULL /*static, unused*/, L_0, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0012;
		}
	}
	{
		goto IL_00f3;
	}

IL_0012:
	{
		bool L_2 = ___recursive3;
		if (!L_2)
		{
			goto IL_0055;
		}
	}
	{
		V_0 = 0;
		goto IL_0048;
	}

IL_0020:
	{
		RectTransform_t3704657025 * L_3 = ___rect0;
		int32_t L_4 = V_0;
		NullCheck(L_3);
		Transform_t3600365921 * L_5 = Transform_GetChild_m3541171965(L_3, L_4, /*hidden argument*/NULL);
		V_1 = ((RectTransform_t3704657025 *)IsInstSealed(L_5, RectTransform_t3704657025_il2cpp_TypeInfo_var));
		RectTransform_t3704657025 * L_6 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_7 = Object_op_Inequality_m1920811489(NULL /*static, unused*/, L_6, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0043;
		}
	}
	{
		RectTransform_t3704657025 * L_8 = V_1;
		int32_t L_9 = ___axis1;
		IL2CPP_RUNTIME_CLASS_INIT(RectTransformUtility_t1743242446_il2cpp_TypeInfo_var);
		RectTransformUtility_FlipLayoutOnAxis_m144305054(NULL /*static, unused*/, L_8, L_9, (bool)0, (bool)1, /*hidden argument*/NULL);
	}

IL_0043:
	{
		int32_t L_10 = V_0;
		V_0 = ((int32_t)((int32_t)L_10+(int32_t)1));
	}

IL_0048:
	{
		int32_t L_11 = V_0;
		RectTransform_t3704657025 * L_12 = ___rect0;
		NullCheck(L_12);
		int32_t L_13 = Transform_get_childCount_m4033131441(L_12, /*hidden argument*/NULL);
		if ((((int32_t)L_11) < ((int32_t)L_13)))
		{
			goto IL_0020;
		}
	}
	{
	}

IL_0055:
	{
		RectTransform_t3704657025 * L_14 = ___rect0;
		NullCheck(L_14);
		Vector2_t2156229523  L_15 = RectTransform_get_pivot_m1676241928(L_14, /*hidden argument*/NULL);
		V_2 = L_15;
		int32_t L_16 = ___axis1;
		int32_t L_17 = ___axis1;
		float L_18 = Vector2_get_Item_m3129197029((&V_2), L_17, /*hidden argument*/NULL);
		Vector2_set_Item_m1664083694((&V_2), L_16, ((float)((float)(1.0f)-(float)L_18)), /*hidden argument*/NULL);
		RectTransform_t3704657025 * L_19 = ___rect0;
		Vector2_t2156229523  L_20 = V_2;
		NullCheck(L_19);
		RectTransform_set_pivot_m3678254456(L_19, L_20, /*hidden argument*/NULL);
		bool L_21 = ___keepPositioning2;
		if (!L_21)
		{
			goto IL_0084;
		}
	}
	{
		goto IL_00f3;
	}

IL_0084:
	{
		RectTransform_t3704657025 * L_22 = ___rect0;
		NullCheck(L_22);
		Vector2_t2156229523  L_23 = RectTransform_get_anchoredPosition_m287009682(L_22, /*hidden argument*/NULL);
		V_3 = L_23;
		int32_t L_24 = ___axis1;
		int32_t L_25 = ___axis1;
		float L_26 = Vector2_get_Item_m3129197029((&V_3), L_25, /*hidden argument*/NULL);
		Vector2_set_Item_m1664083694((&V_3), L_24, ((-L_26)), /*hidden argument*/NULL);
		RectTransform_t3704657025 * L_27 = ___rect0;
		Vector2_t2156229523  L_28 = V_3;
		NullCheck(L_27);
		RectTransform_set_anchoredPosition_m1454079598(L_27, L_28, /*hidden argument*/NULL);
		RectTransform_t3704657025 * L_29 = ___rect0;
		NullCheck(L_29);
		Vector2_t2156229523  L_30 = RectTransform_get_anchorMin_m1342910522(L_29, /*hidden argument*/NULL);
		V_4 = L_30;
		RectTransform_t3704657025 * L_31 = ___rect0;
		NullCheck(L_31);
		Vector2_t2156229523  L_32 = RectTransform_get_anchorMax_m1075940194(L_31, /*hidden argument*/NULL);
		V_5 = L_32;
		int32_t L_33 = ___axis1;
		float L_34 = Vector2_get_Item_m3129197029((&V_4), L_33, /*hidden argument*/NULL);
		V_6 = L_34;
		int32_t L_35 = ___axis1;
		int32_t L_36 = ___axis1;
		float L_37 = Vector2_get_Item_m3129197029((&V_5), L_36, /*hidden argument*/NULL);
		Vector2_set_Item_m1664083694((&V_4), L_35, ((float)((float)(1.0f)-(float)L_37)), /*hidden argument*/NULL);
		int32_t L_38 = ___axis1;
		float L_39 = V_6;
		Vector2_set_Item_m1664083694((&V_5), L_38, ((float)((float)(1.0f)-(float)L_39)), /*hidden argument*/NULL);
		RectTransform_t3704657025 * L_40 = ___rect0;
		Vector2_t2156229523  L_41 = V_4;
		NullCheck(L_40);
		RectTransform_set_anchorMin_m2068858122(L_40, L_41, /*hidden argument*/NULL);
		RectTransform_t3704657025 * L_42 = ___rect0;
		Vector2_t2156229523  L_43 = V_5;
		NullCheck(L_42);
		RectTransform_set_anchorMax_m2389806509(L_42, L_43, /*hidden argument*/NULL);
	}

IL_00f3:
	{
		return;
	}
}
// System.Void UnityEngine.RectTransformUtility::FlipLayoutAxes(UnityEngine.RectTransform,System.Boolean,System.Boolean)
extern "C"  void RectTransformUtility_FlipLayoutAxes_m3705369832 (Il2CppObject * __this /* static, unused */, RectTransform_t3704657025 * ___rect0, bool ___keepPositioning1, bool ___recursive2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (RectTransformUtility_FlipLayoutAxes_m3705369832_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	RectTransform_t3704657025 * V_1 = NULL;
	{
		RectTransform_t3704657025 * L_0 = ___rect0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_m1454075600(NULL /*static, unused*/, L_0, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0012;
		}
	}
	{
		goto IL_00b4;
	}

IL_0012:
	{
		bool L_2 = ___recursive2;
		if (!L_2)
		{
			goto IL_0054;
		}
	}
	{
		V_0 = 0;
		goto IL_0047;
	}

IL_0020:
	{
		RectTransform_t3704657025 * L_3 = ___rect0;
		int32_t L_4 = V_0;
		NullCheck(L_3);
		Transform_t3600365921 * L_5 = Transform_GetChild_m3541171965(L_3, L_4, /*hidden argument*/NULL);
		V_1 = ((RectTransform_t3704657025 *)IsInstSealed(L_5, RectTransform_t3704657025_il2cpp_TypeInfo_var));
		RectTransform_t3704657025 * L_6 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_7 = Object_op_Inequality_m1920811489(NULL /*static, unused*/, L_6, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0042;
		}
	}
	{
		RectTransform_t3704657025 * L_8 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(RectTransformUtility_t1743242446_il2cpp_TypeInfo_var);
		RectTransformUtility_FlipLayoutAxes_m3705369832(NULL /*static, unused*/, L_8, (bool)0, (bool)1, /*hidden argument*/NULL);
	}

IL_0042:
	{
		int32_t L_9 = V_0;
		V_0 = ((int32_t)((int32_t)L_9+(int32_t)1));
	}

IL_0047:
	{
		int32_t L_10 = V_0;
		RectTransform_t3704657025 * L_11 = ___rect0;
		NullCheck(L_11);
		int32_t L_12 = Transform_get_childCount_m4033131441(L_11, /*hidden argument*/NULL);
		if ((((int32_t)L_10) < ((int32_t)L_12)))
		{
			goto IL_0020;
		}
	}
	{
	}

IL_0054:
	{
		RectTransform_t3704657025 * L_13 = ___rect0;
		RectTransform_t3704657025 * L_14 = ___rect0;
		NullCheck(L_14);
		Vector2_t2156229523  L_15 = RectTransform_get_pivot_m1676241928(L_14, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(RectTransformUtility_t1743242446_il2cpp_TypeInfo_var);
		Vector2_t2156229523  L_16 = RectTransformUtility_GetTransposed_m440700723(NULL /*static, unused*/, L_15, /*hidden argument*/NULL);
		NullCheck(L_13);
		RectTransform_set_pivot_m3678254456(L_13, L_16, /*hidden argument*/NULL);
		RectTransform_t3704657025 * L_17 = ___rect0;
		RectTransform_t3704657025 * L_18 = ___rect0;
		NullCheck(L_18);
		Vector2_t2156229523  L_19 = RectTransform_get_sizeDelta_m2136908840(L_18, /*hidden argument*/NULL);
		Vector2_t2156229523  L_20 = RectTransformUtility_GetTransposed_m440700723(NULL /*static, unused*/, L_19, /*hidden argument*/NULL);
		NullCheck(L_17);
		RectTransform_set_sizeDelta_m344906562(L_17, L_20, /*hidden argument*/NULL);
		bool L_21 = ___keepPositioning1;
		if (!L_21)
		{
			goto IL_0081;
		}
	}
	{
		goto IL_00b4;
	}

IL_0081:
	{
		RectTransform_t3704657025 * L_22 = ___rect0;
		RectTransform_t3704657025 * L_23 = ___rect0;
		NullCheck(L_23);
		Vector2_t2156229523  L_24 = RectTransform_get_anchoredPosition_m287009682(L_23, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(RectTransformUtility_t1743242446_il2cpp_TypeInfo_var);
		Vector2_t2156229523  L_25 = RectTransformUtility_GetTransposed_m440700723(NULL /*static, unused*/, L_24, /*hidden argument*/NULL);
		NullCheck(L_22);
		RectTransform_set_anchoredPosition_m1454079598(L_22, L_25, /*hidden argument*/NULL);
		RectTransform_t3704657025 * L_26 = ___rect0;
		RectTransform_t3704657025 * L_27 = ___rect0;
		NullCheck(L_27);
		Vector2_t2156229523  L_28 = RectTransform_get_anchorMin_m1342910522(L_27, /*hidden argument*/NULL);
		Vector2_t2156229523  L_29 = RectTransformUtility_GetTransposed_m440700723(NULL /*static, unused*/, L_28, /*hidden argument*/NULL);
		NullCheck(L_26);
		RectTransform_set_anchorMin_m2068858122(L_26, L_29, /*hidden argument*/NULL);
		RectTransform_t3704657025 * L_30 = ___rect0;
		RectTransform_t3704657025 * L_31 = ___rect0;
		NullCheck(L_31);
		Vector2_t2156229523  L_32 = RectTransform_get_anchorMax_m1075940194(L_31, /*hidden argument*/NULL);
		Vector2_t2156229523  L_33 = RectTransformUtility_GetTransposed_m440700723(NULL /*static, unused*/, L_32, /*hidden argument*/NULL);
		NullCheck(L_30);
		RectTransform_set_anchorMax_m2389806509(L_30, L_33, /*hidden argument*/NULL);
	}

IL_00b4:
	{
		return;
	}
}
// UnityEngine.Vector2 UnityEngine.RectTransformUtility::GetTransposed(UnityEngine.Vector2)
extern "C"  Vector2_t2156229523  RectTransformUtility_GetTransposed_m440700723 (Il2CppObject * __this /* static, unused */, Vector2_t2156229523  ___input0, const MethodInfo* method)
{
	Vector2_t2156229523  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		float L_0 = (&___input0)->get_y_1();
		float L_1 = (&___input0)->get_x_0();
		Vector2_t2156229523  L_2;
		memset(&L_2, 0, sizeof(L_2));
		Vector2__ctor_m4060800441(&L_2, L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		goto IL_001a;
	}

IL_001a:
	{
		Vector2_t2156229523  L_3 = V_0;
		return L_3;
	}
}
// System.Boolean UnityEngine.RectTransformUtility::RectangleContainsScreenPoint(UnityEngine.RectTransform,UnityEngine.Vector2,UnityEngine.Camera)
extern "C"  bool RectTransformUtility_RectangleContainsScreenPoint_m1731210517 (Il2CppObject * __this /* static, unused */, RectTransform_t3704657025 * ___rect0, Vector2_t2156229523  ___screenPoint1, Camera_t4157153871 * ___cam2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (RectTransformUtility_RectangleContainsScreenPoint_m1731210517_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	{
		RectTransform_t3704657025 * L_0 = ___rect0;
		Camera_t4157153871 * L_1 = ___cam2;
		IL2CPP_RUNTIME_CLASS_INIT(RectTransformUtility_t1743242446_il2cpp_TypeInfo_var);
		bool L_2 = RectTransformUtility_INTERNAL_CALL_RectangleContainsScreenPoint_m522477797(NULL /*static, unused*/, L_0, (&___screenPoint1), L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		goto IL_0010;
	}

IL_0010:
	{
		bool L_3 = V_0;
		return L_3;
	}
}
// System.Boolean UnityEngine.RectTransformUtility::INTERNAL_CALL_RectangleContainsScreenPoint(UnityEngine.RectTransform,UnityEngine.Vector2&,UnityEngine.Camera)
extern "C"  bool RectTransformUtility_INTERNAL_CALL_RectangleContainsScreenPoint_m522477797 (Il2CppObject * __this /* static, unused */, RectTransform_t3704657025 * ___rect0, Vector2_t2156229523 * ___screenPoint1, Camera_t4157153871 * ___cam2, const MethodInfo* method)
{
	typedef bool (*RectTransformUtility_INTERNAL_CALL_RectangleContainsScreenPoint_m522477797_ftn) (RectTransform_t3704657025 *, Vector2_t2156229523 *, Camera_t4157153871 *);
	static RectTransformUtility_INTERNAL_CALL_RectangleContainsScreenPoint_m522477797_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectTransformUtility_INTERNAL_CALL_RectangleContainsScreenPoint_m522477797_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectTransformUtility::INTERNAL_CALL_RectangleContainsScreenPoint(UnityEngine.RectTransform,UnityEngine.Vector2&,UnityEngine.Camera)");
	return _il2cpp_icall_func(___rect0, ___screenPoint1, ___cam2);
}
// UnityEngine.Vector2 UnityEngine.RectTransformUtility::PixelAdjustPoint(UnityEngine.Vector2,UnityEngine.Transform,UnityEngine.Canvas)
extern "C"  Vector2_t2156229523  RectTransformUtility_PixelAdjustPoint_m2058277111 (Il2CppObject * __this /* static, unused */, Vector2_t2156229523  ___point0, Transform_t3600365921 * ___elementTransform1, Canvas_t3310196443 * ___canvas2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (RectTransformUtility_PixelAdjustPoint_m2058277111_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector2_t2156229523  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector2_t2156229523  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		Transform_t3600365921 * L_0 = ___elementTransform1;
		Canvas_t3310196443 * L_1 = ___canvas2;
		IL2CPP_RUNTIME_CLASS_INIT(RectTransformUtility_t1743242446_il2cpp_TypeInfo_var);
		RectTransformUtility_INTERNAL_CALL_PixelAdjustPoint_m2292821422(NULL /*static, unused*/, (&___point0), L_0, L_1, (&V_0), /*hidden argument*/NULL);
		Vector2_t2156229523  L_2 = V_0;
		V_1 = L_2;
		goto IL_0013;
	}

IL_0013:
	{
		Vector2_t2156229523  L_3 = V_1;
		return L_3;
	}
}
// System.Void UnityEngine.RectTransformUtility::INTERNAL_CALL_PixelAdjustPoint(UnityEngine.Vector2&,UnityEngine.Transform,UnityEngine.Canvas,UnityEngine.Vector2&)
extern "C"  void RectTransformUtility_INTERNAL_CALL_PixelAdjustPoint_m2292821422 (Il2CppObject * __this /* static, unused */, Vector2_t2156229523 * ___point0, Transform_t3600365921 * ___elementTransform1, Canvas_t3310196443 * ___canvas2, Vector2_t2156229523 * ___value3, const MethodInfo* method)
{
	typedef void (*RectTransformUtility_INTERNAL_CALL_PixelAdjustPoint_m2292821422_ftn) (Vector2_t2156229523 *, Transform_t3600365921 *, Canvas_t3310196443 *, Vector2_t2156229523 *);
	static RectTransformUtility_INTERNAL_CALL_PixelAdjustPoint_m2292821422_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectTransformUtility_INTERNAL_CALL_PixelAdjustPoint_m2292821422_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectTransformUtility::INTERNAL_CALL_PixelAdjustPoint(UnityEngine.Vector2&,UnityEngine.Transform,UnityEngine.Canvas,UnityEngine.Vector2&)");
	_il2cpp_icall_func(___point0, ___elementTransform1, ___canvas2, ___value3);
}
// UnityEngine.Rect UnityEngine.RectTransformUtility::PixelAdjustRect(UnityEngine.RectTransform,UnityEngine.Canvas)
extern "C"  Rect_t2360479859  RectTransformUtility_PixelAdjustRect_m1219181130 (Il2CppObject * __this /* static, unused */, RectTransform_t3704657025 * ___rectTransform0, Canvas_t3310196443 * ___canvas1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (RectTransformUtility_PixelAdjustRect_m1219181130_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Rect_t2360479859  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Rect_t2360479859  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		RectTransform_t3704657025 * L_0 = ___rectTransform0;
		Canvas_t3310196443 * L_1 = ___canvas1;
		IL2CPP_RUNTIME_CLASS_INIT(RectTransformUtility_t1743242446_il2cpp_TypeInfo_var);
		RectTransformUtility_INTERNAL_CALL_PixelAdjustRect_m1199355208(NULL /*static, unused*/, L_0, L_1, (&V_0), /*hidden argument*/NULL);
		Rect_t2360479859  L_2 = V_0;
		V_1 = L_2;
		goto IL_0011;
	}

IL_0011:
	{
		Rect_t2360479859  L_3 = V_1;
		return L_3;
	}
}
// System.Void UnityEngine.RectTransformUtility::INTERNAL_CALL_PixelAdjustRect(UnityEngine.RectTransform,UnityEngine.Canvas,UnityEngine.Rect&)
extern "C"  void RectTransformUtility_INTERNAL_CALL_PixelAdjustRect_m1199355208 (Il2CppObject * __this /* static, unused */, RectTransform_t3704657025 * ___rectTransform0, Canvas_t3310196443 * ___canvas1, Rect_t2360479859 * ___value2, const MethodInfo* method)
{
	typedef void (*RectTransformUtility_INTERNAL_CALL_PixelAdjustRect_m1199355208_ftn) (RectTransform_t3704657025 *, Canvas_t3310196443 *, Rect_t2360479859 *);
	static RectTransformUtility_INTERNAL_CALL_PixelAdjustRect_m1199355208_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectTransformUtility_INTERNAL_CALL_PixelAdjustRect_m1199355208_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectTransformUtility::INTERNAL_CALL_PixelAdjustRect(UnityEngine.RectTransform,UnityEngine.Canvas,UnityEngine.Rect&)");
	_il2cpp_icall_func(___rectTransform0, ___canvas1, ___value2);
}
// System.Void UnityEngine.RectTransformUtility::.cctor()
extern "C"  void RectTransformUtility__cctor_m1465726289 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (RectTransformUtility__cctor_m1465726289_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		((RectTransformUtility_t1743242446_StaticFields*)RectTransformUtility_t1743242446_il2cpp_TypeInfo_var->static_fields)->set_s_Corners_0(((Vector3U5BU5D_t1718750761*)SZArrayNew(Vector3U5BU5D_t1718750761_il2cpp_TypeInfo_var, (uint32_t)4)));
		return;
	}
}
// System.Void UnityEngine.RemoteSettings::CallOnUpdate()
extern "C"  void RemoteSettings_CallOnUpdate_m237135148 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (RemoteSettings_CallOnUpdate_m237135148_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	UpdatedEventHandler_t1027848393 * V_0 = NULL;
	{
		UpdatedEventHandler_t1027848393 * L_0 = ((RemoteSettings_t1718627291_StaticFields*)RemoteSettings_t1718627291_il2cpp_TypeInfo_var->static_fields)->get_Updated_0();
		V_0 = L_0;
		UpdatedEventHandler_t1027848393 * L_1 = V_0;
		if (!L_1)
		{
			goto IL_0013;
		}
	}
	{
		UpdatedEventHandler_t1027848393 * L_2 = V_0;
		NullCheck(L_2);
		UpdatedEventHandler_Invoke_m631751593(L_2, /*hidden argument*/NULL);
	}

IL_0013:
	{
		return;
	}
}
extern "C"  void DelegatePInvokeWrapper_UpdatedEventHandler_t1027848393 (UpdatedEventHandler_t1027848393 * __this, const MethodInfo* method)
{
	typedef void (STDCALL *PInvokeFunc)();
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(((Il2CppDelegate*)__this)->method->methodPointer);

	// Native function invocation
	il2cppPInvokeFunc();

}
// System.Void UnityEngine.RemoteSettings/UpdatedEventHandler::.ctor(System.Object,System.IntPtr)
extern "C"  void UpdatedEventHandler__ctor_m1354828285 (UpdatedEventHandler_t1027848393 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void UnityEngine.RemoteSettings/UpdatedEventHandler::Invoke()
extern "C"  void UpdatedEventHandler_Invoke_m631751593 (UpdatedEventHandler_t1027848393 * __this, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		UpdatedEventHandler_Invoke_m631751593((UpdatedEventHandler_t1027848393 *)__this->get_prev_9(), method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if ((__this->get_m_target_2() != NULL || MethodHasParameters((MethodInfo*)(__this->get_method_3().get_m_value_0()))) && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult UnityEngine.RemoteSettings/UpdatedEventHandler::BeginInvoke(System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * UpdatedEventHandler_BeginInvoke_m364031389 (UpdatedEventHandler_t1027848393 * __this, AsyncCallback_t3962456242 * ___callback0, Il2CppObject * ___object1, const MethodInfo* method)
{
	void *__d_args[1] = {0};
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback0, (Il2CppObject*)___object1);
}
// System.Void UnityEngine.RemoteSettings/UpdatedEventHandler::EndInvoke(System.IAsyncResult)
extern "C"  void UpdatedEventHandler_EndInvoke_m1619252114 (UpdatedEventHandler_t1027848393 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void UnityEngine.Renderer::set_enabled(System.Boolean)
extern "C"  void Renderer_set_enabled_m2710705614 (Renderer_t2627027031 * __this, bool ___value0, const MethodInfo* method)
{
	typedef void (*Renderer_set_enabled_m2710705614_ftn) (Renderer_t2627027031 *, bool);
	static Renderer_set_enabled_m2710705614_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Renderer_set_enabled_m2710705614_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Renderer::set_enabled(System.Boolean)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.Renderer::set_shadowCastingMode(UnityEngine.Rendering.ShadowCastingMode)
extern "C"  void Renderer_set_shadowCastingMode_m465575092 (Renderer_t2627027031 * __this, int32_t ___value0, const MethodInfo* method)
{
	typedef void (*Renderer_set_shadowCastingMode_m465575092_ftn) (Renderer_t2627027031 *, int32_t);
	static Renderer_set_shadowCastingMode_m465575092_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Renderer_set_shadowCastingMode_m465575092_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Renderer::set_shadowCastingMode(UnityEngine.Rendering.ShadowCastingMode)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.Renderer::set_receiveShadows(System.Boolean)
extern "C"  void Renderer_set_receiveShadows_m1065778853 (Renderer_t2627027031 * __this, bool ___value0, const MethodInfo* method)
{
	typedef void (*Renderer_set_receiveShadows_m1065778853_ftn) (Renderer_t2627027031 *, bool);
	static Renderer_set_receiveShadows_m1065778853_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Renderer_set_receiveShadows_m1065778853_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Renderer::set_receiveShadows(System.Boolean)");
	_il2cpp_icall_func(__this, ___value0);
}
// UnityEngine.Material UnityEngine.Renderer::get_material()
extern "C"  Material_t340375123 * Renderer_get_material_m909181218 (Renderer_t2627027031 * __this, const MethodInfo* method)
{
	typedef Material_t340375123 * (*Renderer_get_material_m909181218_ftn) (Renderer_t2627027031 *);
	static Renderer_get_material_m909181218_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Renderer_get_material_m909181218_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Renderer::get_material()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Renderer::set_material(UnityEngine.Material)
extern "C"  void Renderer_set_material_m3719049989 (Renderer_t2627027031 * __this, Material_t340375123 * ___value0, const MethodInfo* method)
{
	typedef void (*Renderer_set_material_m3719049989_ftn) (Renderer_t2627027031 *, Material_t340375123 *);
	static Renderer_set_material_m3719049989_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Renderer_set_material_m3719049989_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Renderer::set_material(UnityEngine.Material)");
	_il2cpp_icall_func(__this, ___value0);
}
// UnityEngine.Material UnityEngine.Renderer::get_sharedMaterial()
extern "C"  Material_t340375123 * Renderer_get_sharedMaterial_m3081157466 (Renderer_t2627027031 * __this, const MethodInfo* method)
{
	typedef Material_t340375123 * (*Renderer_get_sharedMaterial_m3081157466_ftn) (Renderer_t2627027031 *);
	static Renderer_get_sharedMaterial_m3081157466_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Renderer_get_sharedMaterial_m3081157466_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Renderer::get_sharedMaterial()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Renderer::set_sharedMaterial(UnityEngine.Material)
extern "C"  void Renderer_set_sharedMaterial_m1856652926 (Renderer_t2627027031 * __this, Material_t340375123 * ___value0, const MethodInfo* method)
{
	typedef void (*Renderer_set_sharedMaterial_m1856652926_ftn) (Renderer_t2627027031 *, Material_t340375123 *);
	static Renderer_set_sharedMaterial_m1856652926_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Renderer_set_sharedMaterial_m1856652926_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Renderer::set_sharedMaterial(UnityEngine.Material)");
	_il2cpp_icall_func(__this, ___value0);
}
// UnityEngine.Material[] UnityEngine.Renderer::get_materials()
extern "C"  MaterialU5BU5D_t561872642* Renderer_get_materials_m2026938772 (Renderer_t2627027031 * __this, const MethodInfo* method)
{
	typedef MaterialU5BU5D_t561872642* (*Renderer_get_materials_m2026938772_ftn) (Renderer_t2627027031 *);
	static Renderer_get_materials_m2026938772_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Renderer_get_materials_m2026938772_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Renderer::get_materials()");
	return _il2cpp_icall_func(__this);
}
// UnityEngine.Material[] UnityEngine.Renderer::get_sharedMaterials()
extern "C"  MaterialU5BU5D_t561872642* Renderer_get_sharedMaterials_m3320735454 (Renderer_t2627027031 * __this, const MethodInfo* method)
{
	typedef MaterialU5BU5D_t561872642* (*Renderer_get_sharedMaterials_m3320735454_ftn) (Renderer_t2627027031 *);
	static Renderer_get_sharedMaterials_m3320735454_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Renderer_get_sharedMaterials_m3320735454_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Renderer::get_sharedMaterials()");
	return _il2cpp_icall_func(__this);
}
// UnityEngine.Bounds UnityEngine.Renderer::get_bounds()
extern "C"  Bounds_t2266837910  Renderer_get_bounds_m2197226282 (Renderer_t2627027031 * __this, const MethodInfo* method)
{
	Bounds_t2266837910  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Bounds_t2266837910  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		Renderer_INTERNAL_get_bounds_m2341376052(__this, (&V_0), /*hidden argument*/NULL);
		Bounds_t2266837910  L_0 = V_0;
		V_1 = L_0;
		goto IL_0010;
	}

IL_0010:
	{
		Bounds_t2266837910  L_1 = V_1;
		return L_1;
	}
}
// System.Void UnityEngine.Renderer::INTERNAL_get_bounds(UnityEngine.Bounds&)
extern "C"  void Renderer_INTERNAL_get_bounds_m2341376052 (Renderer_t2627027031 * __this, Bounds_t2266837910 * ___value0, const MethodInfo* method)
{
	typedef void (*Renderer_INTERNAL_get_bounds_m2341376052_ftn) (Renderer_t2627027031 *, Bounds_t2266837910 *);
	static Renderer_INTERNAL_get_bounds_m2341376052_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Renderer_INTERNAL_get_bounds_m2341376052_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Renderer::INTERNAL_get_bounds(UnityEngine.Bounds&)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Int32 UnityEngine.Renderer::get_lightmapIndex()
extern "C"  int32_t Renderer_get_lightmapIndex_m2832528251 (Renderer_t2627027031 * __this, const MethodInfo* method)
{
	typedef int32_t (*Renderer_get_lightmapIndex_m2832528251_ftn) (Renderer_t2627027031 *);
	static Renderer_get_lightmapIndex_m2832528251_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Renderer_get_lightmapIndex_m2832528251_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Renderer::get_lightmapIndex()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Renderer::set_lightmapIndex(System.Int32)
extern "C"  void Renderer_set_lightmapIndex_m674169733 (Renderer_t2627027031 * __this, int32_t ___value0, const MethodInfo* method)
{
	typedef void (*Renderer_set_lightmapIndex_m674169733_ftn) (Renderer_t2627027031 *, int32_t);
	static Renderer_set_lightmapIndex_m674169733_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Renderer_set_lightmapIndex_m674169733_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Renderer::set_lightmapIndex(System.Int32)");
	_il2cpp_icall_func(__this, ___value0);
}
// UnityEngine.Vector4 UnityEngine.Renderer::get_lightmapScaleOffset()
extern "C"  Vector4_t3319028937  Renderer_get_lightmapScaleOffset_m3248126575 (Renderer_t2627027031 * __this, const MethodInfo* method)
{
	Vector4_t3319028937  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector4_t3319028937  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		Renderer_INTERNAL_get_lightmapScaleOffset_m1480402966(__this, (&V_0), /*hidden argument*/NULL);
		Vector4_t3319028937  L_0 = V_0;
		V_1 = L_0;
		goto IL_0010;
	}

IL_0010:
	{
		Vector4_t3319028937  L_1 = V_1;
		return L_1;
	}
}
// System.Void UnityEngine.Renderer::set_lightmapScaleOffset(UnityEngine.Vector4)
extern "C"  void Renderer_set_lightmapScaleOffset_m2366227484 (Renderer_t2627027031 * __this, Vector4_t3319028937  ___value0, const MethodInfo* method)
{
	{
		Renderer_INTERNAL_set_lightmapScaleOffset_m2266823654(__this, (&___value0), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Renderer::INTERNAL_get_lightmapScaleOffset(UnityEngine.Vector4&)
extern "C"  void Renderer_INTERNAL_get_lightmapScaleOffset_m1480402966 (Renderer_t2627027031 * __this, Vector4_t3319028937 * ___value0, const MethodInfo* method)
{
	typedef void (*Renderer_INTERNAL_get_lightmapScaleOffset_m1480402966_ftn) (Renderer_t2627027031 *, Vector4_t3319028937 *);
	static Renderer_INTERNAL_get_lightmapScaleOffset_m1480402966_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Renderer_INTERNAL_get_lightmapScaleOffset_m1480402966_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Renderer::INTERNAL_get_lightmapScaleOffset(UnityEngine.Vector4&)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.Renderer::INTERNAL_set_lightmapScaleOffset(UnityEngine.Vector4&)
extern "C"  void Renderer_INTERNAL_set_lightmapScaleOffset_m2266823654 (Renderer_t2627027031 * __this, Vector4_t3319028937 * ___value0, const MethodInfo* method)
{
	typedef void (*Renderer_INTERNAL_set_lightmapScaleOffset_m2266823654_ftn) (Renderer_t2627027031 *, Vector4_t3319028937 *);
	static Renderer_INTERNAL_set_lightmapScaleOffset_m2266823654_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Renderer_INTERNAL_set_lightmapScaleOffset_m2266823654_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Renderer::INTERNAL_set_lightmapScaleOffset(UnityEngine.Vector4&)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Int32 UnityEngine.Renderer::get_sortingLayerID()
extern "C"  int32_t Renderer_get_sortingLayerID_m612188142 (Renderer_t2627027031 * __this, const MethodInfo* method)
{
	typedef int32_t (*Renderer_get_sortingLayerID_m612188142_ftn) (Renderer_t2627027031 *);
	static Renderer_get_sortingLayerID_m612188142_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Renderer_get_sortingLayerID_m612188142_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Renderer::get_sortingLayerID()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Renderer::set_sortingLayerID(System.Int32)
extern "C"  void Renderer_set_sortingLayerID_m607199763 (Renderer_t2627027031 * __this, int32_t ___value0, const MethodInfo* method)
{
	typedef void (*Renderer_set_sortingLayerID_m607199763_ftn) (Renderer_t2627027031 *, int32_t);
	static Renderer_set_sortingLayerID_m607199763_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Renderer_set_sortingLayerID_m607199763_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Renderer::set_sortingLayerID(System.Int32)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Int32 UnityEngine.Renderer::get_sortingOrder()
extern "C"  int32_t Renderer_get_sortingOrder_m3189497003 (Renderer_t2627027031 * __this, const MethodInfo* method)
{
	typedef int32_t (*Renderer_get_sortingOrder_m3189497003_ftn) (Renderer_t2627027031 *);
	static Renderer_get_sortingOrder_m3189497003_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Renderer_get_sortingOrder_m3189497003_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Renderer::get_sortingOrder()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Renderer::set_sortingOrder(System.Int32)
extern "C"  void Renderer_set_sortingOrder_m3306159660 (Renderer_t2627027031 * __this, int32_t ___value0, const MethodInfo* method)
{
	typedef void (*Renderer_set_sortingOrder_m3306159660_ftn) (Renderer_t2627027031 *, int32_t);
	static Renderer_set_sortingOrder_m3306159660_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Renderer_set_sortingOrder_m3306159660_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Renderer::set_sortingOrder(System.Int32)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.Rendering.CommandBuffer::.ctor()
extern "C"  void CommandBuffer__ctor_m1834978680 (CommandBuffer_t2206337031 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CommandBuffer__ctor_m1834978680_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Object__ctor_m297566312(__this, /*hidden argument*/NULL);
		IntPtr_t L_0 = ((IntPtr_t_StaticFields*)IntPtr_t_il2cpp_TypeInfo_var->static_fields)->get_Zero_1();
		__this->set_m_Ptr_0(L_0);
		CommandBuffer_InitBuffer_m1799175099(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Rendering.CommandBuffer::Finalize()
extern "C"  void CommandBuffer_Finalize_m3874119776 (CommandBuffer_t2206337031 * __this, const MethodInfo* method)
{
	Exception_t1436737249 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1436737249 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
	}

IL_0001:
	try
	{ // begin try (depth: 1)
		CommandBuffer_Dispose_m811468491(__this, (bool)0, /*hidden argument*/NULL);
		IL2CPP_LEAVE(0x14, FINALLY_000d);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1436737249 *)e.ex;
		goto FINALLY_000d;
	}

FINALLY_000d:
	{ // begin finally (depth: 1)
		Object_Finalize_m3076187857(__this, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(13)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(13)
	{
		IL2CPP_JUMP_TBL(0x14, IL_0014)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1436737249 *)
	}

IL_0014:
	{
		return;
	}
}
// System.Void UnityEngine.Rendering.CommandBuffer::Dispose()
extern "C"  void CommandBuffer_Dispose_m305441489 (CommandBuffer_t2206337031 * __this, const MethodInfo* method)
{
	{
		CommandBuffer_Dispose_m811468491(__this, (bool)1, /*hidden argument*/NULL);
		GC_SuppressFinalize_m1177400158(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Rendering.CommandBuffer::Dispose(System.Boolean)
extern "C"  void CommandBuffer_Dispose_m811468491 (CommandBuffer_t2206337031 * __this, bool ___disposing0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CommandBuffer_Dispose_m811468491_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		CommandBuffer_ReleaseBuffer_m2596961287(__this, /*hidden argument*/NULL);
		IntPtr_t L_0 = ((IntPtr_t_StaticFields*)IntPtr_t_il2cpp_TypeInfo_var->static_fields)->get_Zero_1();
		__this->set_m_Ptr_0(L_0);
		return;
	}
}
// System.Void UnityEngine.Rendering.CommandBuffer::InitBuffer(UnityEngine.Rendering.CommandBuffer)
extern "C"  void CommandBuffer_InitBuffer_m1799175099 (Il2CppObject * __this /* static, unused */, CommandBuffer_t2206337031 * ___buf0, const MethodInfo* method)
{
	typedef void (*CommandBuffer_InitBuffer_m1799175099_ftn) (CommandBuffer_t2206337031 *);
	static CommandBuffer_InitBuffer_m1799175099_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (CommandBuffer_InitBuffer_m1799175099_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rendering.CommandBuffer::InitBuffer(UnityEngine.Rendering.CommandBuffer)");
	_il2cpp_icall_func(___buf0);
}
// System.Void UnityEngine.Rendering.CommandBuffer::ReleaseBuffer()
extern "C"  void CommandBuffer_ReleaseBuffer_m2596961287 (CommandBuffer_t2206337031 * __this, const MethodInfo* method)
{
	typedef void (*CommandBuffer_ReleaseBuffer_m2596961287_ftn) (CommandBuffer_t2206337031 *);
	static CommandBuffer_ReleaseBuffer_m2596961287_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (CommandBuffer_ReleaseBuffer_m2596961287_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rendering.CommandBuffer::ReleaseBuffer()");
	_il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Rendering.CommandBuffer::set_name(System.String)
extern "C"  void CommandBuffer_set_name_m2615257852 (CommandBuffer_t2206337031 * __this, String_t* ___value0, const MethodInfo* method)
{
	typedef void (*CommandBuffer_set_name_m2615257852_ftn) (CommandBuffer_t2206337031 *, String_t*);
	static CommandBuffer_set_name_m2615257852_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (CommandBuffer_set_name_m2615257852_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rendering.CommandBuffer::set_name(System.String)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.Rendering.CommandBuffer::Clear()
extern "C"  void CommandBuffer_Clear_m1778659033 (CommandBuffer_t2206337031 * __this, const MethodInfo* method)
{
	typedef void (*CommandBuffer_Clear_m1778659033_ftn) (CommandBuffer_t2206337031 *);
	static CommandBuffer_Clear_m1778659033_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (CommandBuffer_Clear_m1778659033_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rendering.CommandBuffer::Clear()");
	_il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Rendering.CommandBuffer::DrawMesh(UnityEngine.Mesh,UnityEngine.Matrix4x4,UnityEngine.Material)
extern "C"  void CommandBuffer_DrawMesh_m1084383657 (CommandBuffer_t2206337031 * __this, Mesh_t3648964284 * ___mesh0, Matrix4x4_t1817901843  ___matrix1, Material_t340375123 * ___material2, const MethodInfo* method)
{
	MaterialPropertyBlock_t3213117958 * V_0 = NULL;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	{
		V_0 = (MaterialPropertyBlock_t3213117958 *)NULL;
		V_1 = (-1);
		V_2 = 0;
		Mesh_t3648964284 * L_0 = ___mesh0;
		Matrix4x4_t1817901843  L_1 = ___matrix1;
		Material_t340375123 * L_2 = ___material2;
		int32_t L_3 = V_2;
		int32_t L_4 = V_1;
		MaterialPropertyBlock_t3213117958 * L_5 = V_0;
		CommandBuffer_DrawMesh_m1771992862(__this, L_0, L_1, L_2, L_3, L_4, L_5, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Rendering.CommandBuffer::DrawMesh(UnityEngine.Mesh,UnityEngine.Matrix4x4,UnityEngine.Material,System.Int32,System.Int32,UnityEngine.MaterialPropertyBlock)
extern "C"  void CommandBuffer_DrawMesh_m1771992862 (CommandBuffer_t2206337031 * __this, Mesh_t3648964284 * ___mesh0, Matrix4x4_t1817901843  ___matrix1, Material_t340375123 * ___material2, int32_t ___submeshIndex3, int32_t ___shaderPass4, MaterialPropertyBlock_t3213117958 * ___properties5, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CommandBuffer_DrawMesh_m1771992862_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Mesh_t3648964284 * L_0 = ___mesh0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_m1454075600(NULL /*static, unused*/, L_0, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0018;
		}
	}
	{
		ArgumentNullException_t1615371798 * L_2 = (ArgumentNullException_t1615371798 *)il2cpp_codegen_object_new(ArgumentNullException_t1615371798_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m1170824041(L_2, _stringLiteral3176107630, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0018:
	{
		int32_t L_3 = ___submeshIndex3;
		if ((((int32_t)L_3) < ((int32_t)0)))
		{
			goto IL_002d;
		}
	}
	{
		int32_t L_4 = ___submeshIndex3;
		Mesh_t3648964284 * L_5 = ___mesh0;
		NullCheck(L_5);
		int32_t L_6 = Mesh_get_subMeshCount_m652342060(L_5, /*hidden argument*/NULL);
		if ((((int32_t)L_4) < ((int32_t)L_6)))
		{
			goto IL_0057;
		}
	}

IL_002d:
	{
		int32_t L_7 = ___submeshIndex3;
		Mesh_t3648964284 * L_8 = ___mesh0;
		NullCheck(L_8);
		int32_t L_9 = Mesh_get_subMeshCount_m652342060(L_8, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t3464937446_il2cpp_TypeInfo_var);
		int32_t L_10 = Mathf_Clamp_m2702811178(NULL /*static, unused*/, L_7, 0, ((int32_t)((int32_t)L_9-(int32_t)1)), /*hidden argument*/NULL);
		___submeshIndex3 = L_10;
		int32_t L_11 = ___submeshIndex3;
		int32_t L_12 = L_11;
		Il2CppObject * L_13 = Box(Int32_t2950945753_il2cpp_TypeInfo_var, &L_12);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_14 = String_Format_m2844511972(NULL /*static, unused*/, _stringLiteral4003009997, L_13, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3317548046_il2cpp_TypeInfo_var);
		Debug_LogWarning_m3661709751(NULL /*static, unused*/, L_14, /*hidden argument*/NULL);
	}

IL_0057:
	{
		Material_t340375123 * L_15 = ___material2;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_16 = Object_op_Equality_m1454075600(NULL /*static, unused*/, L_15, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_16)
		{
			goto IL_006e;
		}
	}
	{
		ArgumentNullException_t1615371798 * L_17 = (ArgumentNullException_t1615371798 *)il2cpp_codegen_object_new(ArgumentNullException_t1615371798_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m1170824041(L_17, _stringLiteral2281755610, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_17);
	}

IL_006e:
	{
		Mesh_t3648964284 * L_18 = ___mesh0;
		Matrix4x4_t1817901843  L_19 = ___matrix1;
		Material_t340375123 * L_20 = ___material2;
		int32_t L_21 = ___submeshIndex3;
		int32_t L_22 = ___shaderPass4;
		MaterialPropertyBlock_t3213117958 * L_23 = ___properties5;
		CommandBuffer_Internal_DrawMesh_m3767491291(__this, L_18, L_19, L_20, L_21, L_22, L_23, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Rendering.CommandBuffer::Internal_DrawMesh(UnityEngine.Mesh,UnityEngine.Matrix4x4,UnityEngine.Material,System.Int32,System.Int32,UnityEngine.MaterialPropertyBlock)
extern "C"  void CommandBuffer_Internal_DrawMesh_m3767491291 (CommandBuffer_t2206337031 * __this, Mesh_t3648964284 * ___mesh0, Matrix4x4_t1817901843  ___matrix1, Material_t340375123 * ___material2, int32_t ___submeshIndex3, int32_t ___shaderPass4, MaterialPropertyBlock_t3213117958 * ___properties5, const MethodInfo* method)
{
	{
		Mesh_t3648964284 * L_0 = ___mesh0;
		Material_t340375123 * L_1 = ___material2;
		int32_t L_2 = ___submeshIndex3;
		int32_t L_3 = ___shaderPass4;
		MaterialPropertyBlock_t3213117958 * L_4 = ___properties5;
		CommandBuffer_INTERNAL_CALL_Internal_DrawMesh_m3335436528(NULL /*static, unused*/, __this, L_0, (&___matrix1), L_1, L_2, L_3, L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Rendering.CommandBuffer::INTERNAL_CALL_Internal_DrawMesh(UnityEngine.Rendering.CommandBuffer,UnityEngine.Mesh,UnityEngine.Matrix4x4&,UnityEngine.Material,System.Int32,System.Int32,UnityEngine.MaterialPropertyBlock)
extern "C"  void CommandBuffer_INTERNAL_CALL_Internal_DrawMesh_m3335436528 (Il2CppObject * __this /* static, unused */, CommandBuffer_t2206337031 * ___self0, Mesh_t3648964284 * ___mesh1, Matrix4x4_t1817901843 * ___matrix2, Material_t340375123 * ___material3, int32_t ___submeshIndex4, int32_t ___shaderPass5, MaterialPropertyBlock_t3213117958 * ___properties6, const MethodInfo* method)
{
	typedef void (*CommandBuffer_INTERNAL_CALL_Internal_DrawMesh_m3335436528_ftn) (CommandBuffer_t2206337031 *, Mesh_t3648964284 *, Matrix4x4_t1817901843 *, Material_t340375123 *, int32_t, int32_t, MaterialPropertyBlock_t3213117958 *);
	static CommandBuffer_INTERNAL_CALL_Internal_DrawMesh_m3335436528_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (CommandBuffer_INTERNAL_CALL_Internal_DrawMesh_m3335436528_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rendering.CommandBuffer::INTERNAL_CALL_Internal_DrawMesh(UnityEngine.Rendering.CommandBuffer,UnityEngine.Mesh,UnityEngine.Matrix4x4&,UnityEngine.Material,System.Int32,System.Int32,UnityEngine.MaterialPropertyBlock)");
	_il2cpp_icall_func(___self0, ___mesh1, ___matrix2, ___material3, ___submeshIndex4, ___shaderPass5, ___properties6);
}
// System.Void UnityEngine.Rendering.CommandBuffer::DrawRenderer(UnityEngine.Renderer,UnityEngine.Material,System.Int32)
extern "C"  void CommandBuffer_DrawRenderer_m3319371712 (CommandBuffer_t2206337031 * __this, Renderer_t2627027031 * ___renderer0, Material_t340375123 * ___material1, int32_t ___submeshIndex2, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		V_0 = (-1);
		Renderer_t2627027031 * L_0 = ___renderer0;
		Material_t340375123 * L_1 = ___material1;
		int32_t L_2 = ___submeshIndex2;
		int32_t L_3 = V_0;
		CommandBuffer_DrawRenderer_m3940925401(__this, L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Rendering.CommandBuffer::DrawRenderer(UnityEngine.Renderer,UnityEngine.Material,System.Int32,System.Int32)
extern "C"  void CommandBuffer_DrawRenderer_m3940925401 (CommandBuffer_t2206337031 * __this, Renderer_t2627027031 * ___renderer0, Material_t340375123 * ___material1, int32_t ___submeshIndex2, int32_t ___shaderPass3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CommandBuffer_DrawRenderer_m3940925401_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Renderer_t2627027031 * L_0 = ___renderer0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_m1454075600(NULL /*static, unused*/, L_0, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0018;
		}
	}
	{
		ArgumentNullException_t1615371798 * L_2 = (ArgumentNullException_t1615371798 *)il2cpp_codegen_object_new(ArgumentNullException_t1615371798_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m1170824041(L_2, _stringLiteral3876461936, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0018:
	{
		int32_t L_3 = ___submeshIndex2;
		if ((((int32_t)L_3) >= ((int32_t)0)))
		{
			goto IL_003f;
		}
	}
	{
		int32_t L_4 = ___submeshIndex2;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t3464937446_il2cpp_TypeInfo_var);
		int32_t L_5 = Mathf_Max_m2931752728(NULL /*static, unused*/, L_4, 0, /*hidden argument*/NULL);
		___submeshIndex2 = L_5;
		int32_t L_6 = ___submeshIndex2;
		int32_t L_7 = L_6;
		Il2CppObject * L_8 = Box(Int32_t2950945753_il2cpp_TypeInfo_var, &L_7);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_9 = String_Format_m2844511972(NULL /*static, unused*/, _stringLiteral4003009997, L_8, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3317548046_il2cpp_TypeInfo_var);
		Debug_LogWarning_m3661709751(NULL /*static, unused*/, L_9, /*hidden argument*/NULL);
	}

IL_003f:
	{
		Material_t340375123 * L_10 = ___material1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_11 = Object_op_Equality_m1454075600(NULL /*static, unused*/, L_10, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_11)
		{
			goto IL_0056;
		}
	}
	{
		ArgumentNullException_t1615371798 * L_12 = (ArgumentNullException_t1615371798 *)il2cpp_codegen_object_new(ArgumentNullException_t1615371798_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m1170824041(L_12, _stringLiteral2281755610, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_12);
	}

IL_0056:
	{
		Renderer_t2627027031 * L_13 = ___renderer0;
		Material_t340375123 * L_14 = ___material1;
		int32_t L_15 = ___submeshIndex2;
		int32_t L_16 = ___shaderPass3;
		CommandBuffer_Internal_DrawRenderer_m3421431924(__this, L_13, L_14, L_15, L_16, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Rendering.CommandBuffer::Internal_DrawRenderer(UnityEngine.Renderer,UnityEngine.Material,System.Int32,System.Int32)
extern "C"  void CommandBuffer_Internal_DrawRenderer_m3421431924 (CommandBuffer_t2206337031 * __this, Renderer_t2627027031 * ___renderer0, Material_t340375123 * ___material1, int32_t ___submeshIndex2, int32_t ___shaderPass3, const MethodInfo* method)
{
	typedef void (*CommandBuffer_Internal_DrawRenderer_m3421431924_ftn) (CommandBuffer_t2206337031 *, Renderer_t2627027031 *, Material_t340375123 *, int32_t, int32_t);
	static CommandBuffer_Internal_DrawRenderer_m3421431924_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (CommandBuffer_Internal_DrawRenderer_m3421431924_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rendering.CommandBuffer::Internal_DrawRenderer(UnityEngine.Renderer,UnityEngine.Material,System.Int32,System.Int32)");
	_il2cpp_icall_func(__this, ___renderer0, ___material1, ___submeshIndex2, ___shaderPass3);
}
// System.Void UnityEngine.Rendering.CommandBuffer::SetRenderTarget(UnityEngine.Rendering.RenderTargetIdentifier)
extern "C"  void CommandBuffer_SetRenderTarget_m1177017440 (CommandBuffer_t2206337031 * __this, RenderTargetIdentifier_t2079184500  ___rt0, const MethodInfo* method)
{
	{
		CommandBuffer_SetRenderTarget_Single_m963511059(__this, (&___rt0), 0, (-1), 0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Rendering.CommandBuffer::SetRenderTarget_Single(UnityEngine.Rendering.RenderTargetIdentifier&,System.Int32,UnityEngine.CubemapFace,System.Int32)
extern "C"  void CommandBuffer_SetRenderTarget_Single_m963511059 (CommandBuffer_t2206337031 * __this, RenderTargetIdentifier_t2079184500 * ___rt0, int32_t ___mipLevel1, int32_t ___cubemapFace2, int32_t ___depthSlice3, const MethodInfo* method)
{
	typedef void (*CommandBuffer_SetRenderTarget_Single_m963511059_ftn) (CommandBuffer_t2206337031 *, RenderTargetIdentifier_t2079184500 *, int32_t, int32_t, int32_t);
	static CommandBuffer_SetRenderTarget_Single_m963511059_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (CommandBuffer_SetRenderTarget_Single_m963511059_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rendering.CommandBuffer::SetRenderTarget_Single(UnityEngine.Rendering.RenderTargetIdentifier&,System.Int32,UnityEngine.CubemapFace,System.Int32)");
	_il2cpp_icall_func(__this, ___rt0, ___mipLevel1, ___cubemapFace2, ___depthSlice3);
}
// System.Void UnityEngine.Rendering.CommandBuffer::Blit(UnityEngine.Texture,UnityEngine.Rendering.RenderTargetIdentifier,UnityEngine.Material)
extern "C"  void CommandBuffer_Blit_m3303448169 (CommandBuffer_t2206337031 * __this, Texture_t3661962703 * ___source0, RenderTargetIdentifier_t2079184500  ___dest1, Material_t340375123 * ___mat2, const MethodInfo* method)
{
	{
		Texture_t3661962703 * L_0 = ___source0;
		Material_t340375123 * L_1 = ___mat2;
		CommandBuffer_Blit_Texture_m3866661524(__this, L_0, (&___dest1), L_1, (-1), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Rendering.CommandBuffer::Blit_Texture(UnityEngine.Texture,UnityEngine.Rendering.RenderTargetIdentifier&,UnityEngine.Material,System.Int32)
extern "C"  void CommandBuffer_Blit_Texture_m3866661524 (CommandBuffer_t2206337031 * __this, Texture_t3661962703 * ___source0, RenderTargetIdentifier_t2079184500 * ___dest1, Material_t340375123 * ___mat2, int32_t ___pass3, const MethodInfo* method)
{
	typedef void (*CommandBuffer_Blit_Texture_m3866661524_ftn) (CommandBuffer_t2206337031 *, Texture_t3661962703 *, RenderTargetIdentifier_t2079184500 *, Material_t340375123 *, int32_t);
	static CommandBuffer_Blit_Texture_m3866661524_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (CommandBuffer_Blit_Texture_m3866661524_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rendering.CommandBuffer::Blit_Texture(UnityEngine.Texture,UnityEngine.Rendering.RenderTargetIdentifier&,UnityEngine.Material,System.Int32)");
	_il2cpp_icall_func(__this, ___source0, ___dest1, ___mat2, ___pass3);
}
// System.Void UnityEngine.Rendering.CommandBuffer::Blit(UnityEngine.Rendering.RenderTargetIdentifier,UnityEngine.Rendering.RenderTargetIdentifier)
extern "C"  void CommandBuffer_Blit_m2968869325 (CommandBuffer_t2206337031 * __this, RenderTargetIdentifier_t2079184500  ___source0, RenderTargetIdentifier_t2079184500  ___dest1, const MethodInfo* method)
{
	{
		CommandBuffer_Blit_Identifier_m669956952(__this, (&___source0), (&___dest1), (Material_t340375123 *)NULL, (-1), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Rendering.CommandBuffer::Blit(UnityEngine.Rendering.RenderTargetIdentifier,UnityEngine.Rendering.RenderTargetIdentifier,UnityEngine.Material)
extern "C"  void CommandBuffer_Blit_m1969698715 (CommandBuffer_t2206337031 * __this, RenderTargetIdentifier_t2079184500  ___source0, RenderTargetIdentifier_t2079184500  ___dest1, Material_t340375123 * ___mat2, const MethodInfo* method)
{
	{
		Material_t340375123 * L_0 = ___mat2;
		CommandBuffer_Blit_Identifier_m669956952(__this, (&___source0), (&___dest1), L_0, (-1), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Rendering.CommandBuffer::Blit_Identifier(UnityEngine.Rendering.RenderTargetIdentifier&,UnityEngine.Rendering.RenderTargetIdentifier&,UnityEngine.Material,System.Int32)
extern "C"  void CommandBuffer_Blit_Identifier_m669956952 (CommandBuffer_t2206337031 * __this, RenderTargetIdentifier_t2079184500 * ___source0, RenderTargetIdentifier_t2079184500 * ___dest1, Material_t340375123 * ___mat2, int32_t ___pass3, const MethodInfo* method)
{
	typedef void (*CommandBuffer_Blit_Identifier_m669956952_ftn) (CommandBuffer_t2206337031 *, RenderTargetIdentifier_t2079184500 *, RenderTargetIdentifier_t2079184500 *, Material_t340375123 *, int32_t);
	static CommandBuffer_Blit_Identifier_m669956952_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (CommandBuffer_Blit_Identifier_m669956952_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rendering.CommandBuffer::Blit_Identifier(UnityEngine.Rendering.RenderTargetIdentifier&,UnityEngine.Rendering.RenderTargetIdentifier&,UnityEngine.Material,System.Int32)");
	_il2cpp_icall_func(__this, ___source0, ___dest1, ___mat2, ___pass3);
}
// System.Void UnityEngine.Rendering.CommandBuffer::GetTemporaryRT(System.Int32,System.Int32,System.Int32,System.Int32,UnityEngine.FilterMode,UnityEngine.RenderTextureFormat,UnityEngine.RenderTextureReadWrite,System.Int32,System.Boolean)
extern "C"  void CommandBuffer_GetTemporaryRT_m2964136030 (CommandBuffer_t2206337031 * __this, int32_t ___nameID0, int32_t ___width1, int32_t ___height2, int32_t ___depthBuffer3, int32_t ___filter4, int32_t ___format5, int32_t ___readWrite6, int32_t ___antiAliasing7, bool ___enableRandomWrite8, const MethodInfo* method)
{
	typedef void (*CommandBuffer_GetTemporaryRT_m2964136030_ftn) (CommandBuffer_t2206337031 *, int32_t, int32_t, int32_t, int32_t, int32_t, int32_t, int32_t, int32_t, bool);
	static CommandBuffer_GetTemporaryRT_m2964136030_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (CommandBuffer_GetTemporaryRT_m2964136030_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rendering.CommandBuffer::GetTemporaryRT(System.Int32,System.Int32,System.Int32,System.Int32,UnityEngine.FilterMode,UnityEngine.RenderTextureFormat,UnityEngine.RenderTextureReadWrite,System.Int32,System.Boolean)");
	_il2cpp_icall_func(__this, ___nameID0, ___width1, ___height2, ___depthBuffer3, ___filter4, ___format5, ___readWrite6, ___antiAliasing7, ___enableRandomWrite8);
}
// System.Void UnityEngine.Rendering.CommandBuffer::GetTemporaryRT(System.Int32,System.Int32,System.Int32,System.Int32,UnityEngine.FilterMode,UnityEngine.RenderTextureFormat,UnityEngine.RenderTextureReadWrite)
extern "C"  void CommandBuffer_GetTemporaryRT_m124211467 (CommandBuffer_t2206337031 * __this, int32_t ___nameID0, int32_t ___width1, int32_t ___height2, int32_t ___depthBuffer3, int32_t ___filter4, int32_t ___format5, int32_t ___readWrite6, const MethodInfo* method)
{
	bool V_0 = false;
	int32_t V_1 = 0;
	{
		V_0 = (bool)0;
		V_1 = 1;
		int32_t L_0 = ___nameID0;
		int32_t L_1 = ___width1;
		int32_t L_2 = ___height2;
		int32_t L_3 = ___depthBuffer3;
		int32_t L_4 = ___filter4;
		int32_t L_5 = ___format5;
		int32_t L_6 = ___readWrite6;
		int32_t L_7 = V_1;
		bool L_8 = V_0;
		CommandBuffer_GetTemporaryRT_m2964136030(__this, L_0, L_1, L_2, L_3, L_4, L_5, L_6, L_7, L_8, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Rendering.CommandBuffer::ReleaseTemporaryRT(System.Int32)
extern "C"  void CommandBuffer_ReleaseTemporaryRT_m1302150554 (CommandBuffer_t2206337031 * __this, int32_t ___nameID0, const MethodInfo* method)
{
	typedef void (*CommandBuffer_ReleaseTemporaryRT_m1302150554_ftn) (CommandBuffer_t2206337031 *, int32_t);
	static CommandBuffer_ReleaseTemporaryRT_m1302150554_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (CommandBuffer_ReleaseTemporaryRT_m1302150554_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rendering.CommandBuffer::ReleaseTemporaryRT(System.Int32)");
	_il2cpp_icall_func(__this, ___nameID0);
}
// System.Void UnityEngine.Rendering.CommandBuffer::ClearRenderTarget(System.Boolean,System.Boolean,UnityEngine.Color)
extern "C"  void CommandBuffer_ClearRenderTarget_m1113458470 (CommandBuffer_t2206337031 * __this, bool ___clearDepth0, bool ___clearColor1, Color_t2555686324  ___backgroundColor2, const MethodInfo* method)
{
	float V_0 = 0.0f;
	{
		V_0 = (1.0f);
		bool L_0 = ___clearDepth0;
		bool L_1 = ___clearColor1;
		float L_2 = V_0;
		CommandBuffer_INTERNAL_CALL_ClearRenderTarget_m4109146477(NULL /*static, unused*/, __this, L_0, L_1, (&___backgroundColor2), L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Rendering.CommandBuffer::INTERNAL_CALL_ClearRenderTarget(UnityEngine.Rendering.CommandBuffer,System.Boolean,System.Boolean,UnityEngine.Color&,System.Single)
extern "C"  void CommandBuffer_INTERNAL_CALL_ClearRenderTarget_m4109146477 (Il2CppObject * __this /* static, unused */, CommandBuffer_t2206337031 * ___self0, bool ___clearDepth1, bool ___clearColor2, Color_t2555686324 * ___backgroundColor3, float ___depth4, const MethodInfo* method)
{
	typedef void (*CommandBuffer_INTERNAL_CALL_ClearRenderTarget_m4109146477_ftn) (CommandBuffer_t2206337031 *, bool, bool, Color_t2555686324 *, float);
	static CommandBuffer_INTERNAL_CALL_ClearRenderTarget_m4109146477_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (CommandBuffer_INTERNAL_CALL_ClearRenderTarget_m4109146477_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rendering.CommandBuffer::INTERNAL_CALL_ClearRenderTarget(UnityEngine.Rendering.CommandBuffer,System.Boolean,System.Boolean,UnityEngine.Color&,System.Single)");
	_il2cpp_icall_func(___self0, ___clearDepth1, ___clearColor2, ___backgroundColor3, ___depth4);
}
// System.Void UnityEngine.Rendering.CommandBuffer::SetGlobalFloat(System.Int32,System.Single)
extern "C"  void CommandBuffer_SetGlobalFloat_m2392201481 (CommandBuffer_t2206337031 * __this, int32_t ___nameID0, float ___value1, const MethodInfo* method)
{
	typedef void (*CommandBuffer_SetGlobalFloat_m2392201481_ftn) (CommandBuffer_t2206337031 *, int32_t, float);
	static CommandBuffer_SetGlobalFloat_m2392201481_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (CommandBuffer_SetGlobalFloat_m2392201481_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rendering.CommandBuffer::SetGlobalFloat(System.Int32,System.Single)");
	_il2cpp_icall_func(__this, ___nameID0, ___value1);
}
// System.Void UnityEngine.Rendering.RenderTargetIdentifier::.ctor(UnityEngine.Rendering.BuiltinRenderTextureType)
extern "C"  void RenderTargetIdentifier__ctor_m59141446 (RenderTargetIdentifier_t2079184500 * __this, int32_t ___type0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___type0;
		__this->set_m_Type_0(L_0);
		__this->set_m_NameID_1((-1));
		__this->set_m_InstanceID_2(0);
		return;
	}
}
extern "C"  void RenderTargetIdentifier__ctor_m59141446_AdjustorThunk (Il2CppObject * __this, int32_t ___type0, const MethodInfo* method)
{
	RenderTargetIdentifier_t2079184500 * _thisAdjusted = reinterpret_cast<RenderTargetIdentifier_t2079184500 *>(__this + 1);
	RenderTargetIdentifier__ctor_m59141446(_thisAdjusted, ___type0, method);
}
// System.Void UnityEngine.Rendering.RenderTargetIdentifier::.ctor(System.Int32)
extern "C"  void RenderTargetIdentifier__ctor_m1871346085 (RenderTargetIdentifier_t2079184500 * __this, int32_t ___nameID0, const MethodInfo* method)
{
	{
		__this->set_m_Type_0(0);
		int32_t L_0 = ___nameID0;
		__this->set_m_NameID_1(L_0);
		__this->set_m_InstanceID_2(0);
		return;
	}
}
extern "C"  void RenderTargetIdentifier__ctor_m1871346085_AdjustorThunk (Il2CppObject * __this, int32_t ___nameID0, const MethodInfo* method)
{
	RenderTargetIdentifier_t2079184500 * _thisAdjusted = reinterpret_cast<RenderTargetIdentifier_t2079184500 *>(__this + 1);
	RenderTargetIdentifier__ctor_m1871346085(_thisAdjusted, ___nameID0, method);
}
// System.Void UnityEngine.Rendering.RenderTargetIdentifier::.ctor(UnityEngine.Texture)
extern "C"  void RenderTargetIdentifier__ctor_m2131406890 (RenderTargetIdentifier_t2079184500 * __this, Texture_t3661962703 * ___tex0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (RenderTargetIdentifier__ctor_m2131406890_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RenderTargetIdentifier_t2079184500 * G_B2_0 = NULL;
	RenderTargetIdentifier_t2079184500 * G_B1_0 = NULL;
	RenderTargetIdentifier_t2079184500 * G_B3_0 = NULL;
	int32_t G_B4_0 = 0;
	RenderTargetIdentifier_t2079184500 * G_B4_1 = NULL;
	RenderTargetIdentifier_t2079184500 * G_B6_0 = NULL;
	RenderTargetIdentifier_t2079184500 * G_B5_0 = NULL;
	int32_t G_B7_0 = 0;
	RenderTargetIdentifier_t2079184500 * G_B7_1 = NULL;
	{
		Texture_t3661962703 * L_0 = ___tex0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_m1454075600(NULL /*static, unused*/, L_0, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		G_B1_0 = __this;
		if (L_1)
		{
			G_B2_0 = __this;
			goto IL_0019;
		}
	}
	{
		Texture_t3661962703 * L_2 = ___tex0;
		G_B2_0 = G_B1_0;
		if (!((RenderTexture_t2108887433 *)IsInstClass(L_2, RenderTexture_t2108887433_il2cpp_TypeInfo_var)))
		{
			G_B3_0 = G_B1_0;
			goto IL_001f;
		}
	}

IL_0019:
	{
		G_B4_0 = 0;
		G_B4_1 = G_B2_0;
		goto IL_0020;
	}

IL_001f:
	{
		G_B4_0 = (-1);
		G_B4_1 = G_B3_0;
	}

IL_0020:
	{
		G_B4_1->set_m_Type_0(G_B4_0);
		__this->set_m_NameID_1((-1));
		Texture_t3661962703 * L_3 = ___tex0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_4 = Object_op_Implicit_m487959476(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		G_B5_0 = __this;
		if (!L_4)
		{
			G_B6_0 = __this;
			goto IL_0043;
		}
	}
	{
		Texture_t3661962703 * L_5 = ___tex0;
		NullCheck(L_5);
		int32_t L_6 = Object_GetInstanceID_m3267067959(L_5, /*hidden argument*/NULL);
		G_B7_0 = L_6;
		G_B7_1 = G_B5_0;
		goto IL_0044;
	}

IL_0043:
	{
		G_B7_0 = 0;
		G_B7_1 = G_B6_0;
	}

IL_0044:
	{
		G_B7_1->set_m_InstanceID_2(G_B7_0);
		return;
	}
}
extern "C"  void RenderTargetIdentifier__ctor_m2131406890_AdjustorThunk (Il2CppObject * __this, Texture_t3661962703 * ___tex0, const MethodInfo* method)
{
	RenderTargetIdentifier_t2079184500 * _thisAdjusted = reinterpret_cast<RenderTargetIdentifier_t2079184500 *>(__this + 1);
	RenderTargetIdentifier__ctor_m2131406890(_thisAdjusted, ___tex0, method);
}
// UnityEngine.Rendering.RenderTargetIdentifier UnityEngine.Rendering.RenderTargetIdentifier::op_Implicit(UnityEngine.Rendering.BuiltinRenderTextureType)
extern "C"  RenderTargetIdentifier_t2079184500  RenderTargetIdentifier_op_Implicit_m4290377132 (Il2CppObject * __this /* static, unused */, int32_t ___type0, const MethodInfo* method)
{
	RenderTargetIdentifier_t2079184500  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___type0;
		RenderTargetIdentifier_t2079184500  L_1;
		memset(&L_1, 0, sizeof(L_1));
		RenderTargetIdentifier__ctor_m59141446(&L_1, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_000d;
	}

IL_000d:
	{
		RenderTargetIdentifier_t2079184500  L_2 = V_0;
		return L_2;
	}
}
// System.String UnityEngine.Rendering.RenderTargetIdentifier::ToString()
extern "C"  String_t* RenderTargetIdentifier_ToString_m1876804468 (RenderTargetIdentifier_t2079184500 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (RenderTargetIdentifier_ToString_m1876804468_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	{
		ObjectU5BU5D_t2843939325* L_0 = ((ObjectU5BU5D_t2843939325*)SZArrayNew(ObjectU5BU5D_t2843939325_il2cpp_TypeInfo_var, (uint32_t)3));
		int32_t L_1 = __this->get_m_Type_0();
		int32_t L_2 = L_1;
		Il2CppObject * L_3 = Box(BuiltinRenderTextureType_t2399837169_il2cpp_TypeInfo_var, &L_2);
		NullCheck(L_0);
		ArrayElementTypeCheck (L_0, L_3);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_3);
		ObjectU5BU5D_t2843939325* L_4 = L_0;
		int32_t L_5 = __this->get_m_NameID_1();
		int32_t L_6 = L_5;
		Il2CppObject * L_7 = Box(Int32_t2950945753_il2cpp_TypeInfo_var, &L_6);
		NullCheck(L_4);
		ArrayElementTypeCheck (L_4, L_7);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_7);
		ObjectU5BU5D_t2843939325* L_8 = L_4;
		int32_t L_9 = __this->get_m_InstanceID_2();
		int32_t L_10 = L_9;
		Il2CppObject * L_11 = Box(Int32_t2950945753_il2cpp_TypeInfo_var, &L_10);
		NullCheck(L_8);
		ArrayElementTypeCheck (L_8, L_11);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)L_11);
		String_t* L_12 = UnityString_Format_m3741272017(NULL /*static, unused*/, _stringLiteral2730748001, L_8, /*hidden argument*/NULL);
		V_0 = L_12;
		goto IL_0041;
	}

IL_0041:
	{
		String_t* L_13 = V_0;
		return L_13;
	}
}
extern "C"  String_t* RenderTargetIdentifier_ToString_m1876804468_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	RenderTargetIdentifier_t2079184500 * _thisAdjusted = reinterpret_cast<RenderTargetIdentifier_t2079184500 *>(__this + 1);
	return RenderTargetIdentifier_ToString_m1876804468(_thisAdjusted, method);
}
// System.Int32 UnityEngine.Rendering.RenderTargetIdentifier::GetHashCode()
extern "C"  int32_t RenderTargetIdentifier_GetHashCode_m1613881668 (RenderTargetIdentifier_t2079184500 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (RenderTargetIdentifier_GetHashCode_m1613881668_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		int32_t* L_0 = __this->get_address_of_m_Type_0();
		Il2CppObject * L_1 = Box(BuiltinRenderTextureType_t2399837169_il2cpp_TypeInfo_var, L_0);
		NullCheck(L_1);
		int32_t L_2 = VirtFuncInvoker0< int32_t >::Invoke(2 /* System.Int32 System.Object::GetHashCode() */, L_1);
		int32_t* L_3 = __this->get_address_of_m_NameID_1();
		int32_t L_4 = Int32_GetHashCode_m1876651407(L_3, /*hidden argument*/NULL);
		int32_t* L_5 = __this->get_address_of_m_InstanceID_2();
		int32_t L_6 = Int32_GetHashCode_m1876651407(L_5, /*hidden argument*/NULL);
		V_0 = ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_2*(int32_t)((int32_t)23)))+(int32_t)L_4))*(int32_t)((int32_t)23)))+(int32_t)L_6));
		goto IL_0042;
	}

IL_0042:
	{
		int32_t L_7 = V_0;
		return L_7;
	}
}
extern "C"  int32_t RenderTargetIdentifier_GetHashCode_m1613881668_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	RenderTargetIdentifier_t2079184500 * _thisAdjusted = reinterpret_cast<RenderTargetIdentifier_t2079184500 *>(__this + 1);
	return RenderTargetIdentifier_GetHashCode_m1613881668(_thisAdjusted, method);
}
// System.Boolean UnityEngine.Rendering.RenderTargetIdentifier::Equals(System.Object)
extern "C"  bool RenderTargetIdentifier_Equals_m2411882201 (RenderTargetIdentifier_t2079184500 * __this, Il2CppObject * ___obj0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (RenderTargetIdentifier_Equals_m2411882201_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	RenderTargetIdentifier_t2079184500  V_1;
	memset(&V_1, 0, sizeof(V_1));
	int32_t G_B6_0 = 0;
	{
		Il2CppObject * L_0 = ___obj0;
		if (((Il2CppObject *)IsInstSealed(L_0, RenderTargetIdentifier_t2079184500_il2cpp_TypeInfo_var)))
		{
			goto IL_0013;
		}
	}
	{
		V_0 = (bool)0;
		goto IL_0056;
	}

IL_0013:
	{
		Il2CppObject * L_1 = ___obj0;
		V_1 = ((*(RenderTargetIdentifier_t2079184500 *)((RenderTargetIdentifier_t2079184500 *)UnBox(L_1, RenderTargetIdentifier_t2079184500_il2cpp_TypeInfo_var))));
		int32_t L_2 = __this->get_m_Type_0();
		int32_t L_3 = (&V_1)->get_m_Type_0();
		if ((!(((uint32_t)L_2) == ((uint32_t)L_3))))
		{
			goto IL_004f;
		}
	}
	{
		int32_t L_4 = __this->get_m_NameID_1();
		int32_t L_5 = (&V_1)->get_m_NameID_1();
		if ((!(((uint32_t)L_4) == ((uint32_t)L_5))))
		{
			goto IL_004f;
		}
	}
	{
		int32_t L_6 = __this->get_m_InstanceID_2();
		int32_t L_7 = (&V_1)->get_m_InstanceID_2();
		G_B6_0 = ((((int32_t)L_6) == ((int32_t)L_7))? 1 : 0);
		goto IL_0050;
	}

IL_004f:
	{
		G_B6_0 = 0;
	}

IL_0050:
	{
		V_0 = (bool)G_B6_0;
		goto IL_0056;
	}

IL_0056:
	{
		bool L_8 = V_0;
		return L_8;
	}
}
extern "C"  bool RenderTargetIdentifier_Equals_m2411882201_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___obj0, const MethodInfo* method)
{
	RenderTargetIdentifier_t2079184500 * _thisAdjusted = reinterpret_cast<RenderTargetIdentifier_t2079184500 *>(__this + 1);
	return RenderTargetIdentifier_Equals_m2411882201(_thisAdjusted, ___obj0, method);
}
// System.Void UnityEngine.RenderTexture::.ctor(System.Int32,System.Int32,System.Int32,UnityEngine.RenderTextureFormat,UnityEngine.RenderTextureReadWrite)
extern "C"  void RenderTexture__ctor_m3189586567 (RenderTexture_t2108887433 * __this, int32_t ___width0, int32_t ___height1, int32_t ___depth2, int32_t ___format3, int32_t ___readWrite4, const MethodInfo* method)
{
	bool V_0 = false;
	{
		Texture__ctor_m3534016699(__this, /*hidden argument*/NULL);
		RenderTexture_Internal_CreateRenderTexture_m2106023999(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		int32_t L_0 = ___width0;
		VirtActionInvoker1< int32_t >::Invoke(5 /* System.Void UnityEngine.Texture::set_width(System.Int32) */, __this, L_0);
		int32_t L_1 = ___height1;
		VirtActionInvoker1< int32_t >::Invoke(7 /* System.Void UnityEngine.Texture::set_height(System.Int32) */, __this, L_1);
		int32_t L_2 = ___depth2;
		RenderTexture_set_depth_m1608130061(__this, L_2, /*hidden argument*/NULL);
		int32_t L_3 = ___format3;
		RenderTexture_set_format_m1605175643(__this, L_3, /*hidden argument*/NULL);
		int32_t L_4 = ___readWrite4;
		V_0 = (bool)((((int32_t)L_4) == ((int32_t)2))? 1 : 0);
		int32_t L_5 = ___readWrite4;
		if (L_5)
		{
			goto IL_0042;
		}
	}
	{
		int32_t L_6 = QualitySettings_get_activeColorSpace_m1763800780(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = (bool)((((int32_t)L_6) == ((int32_t)1))? 1 : 0);
	}

IL_0042:
	{
		bool L_7 = V_0;
		RenderTexture_Internal_SetSRGBReadWrite_m4197499185(NULL /*static, unused*/, __this, L_7, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.RenderTexture::Internal_CreateRenderTexture(UnityEngine.RenderTexture)
extern "C"  void RenderTexture_Internal_CreateRenderTexture_m2106023999 (Il2CppObject * __this /* static, unused */, RenderTexture_t2108887433 * ___rt0, const MethodInfo* method)
{
	typedef void (*RenderTexture_Internal_CreateRenderTexture_m2106023999_ftn) (RenderTexture_t2108887433 *);
	static RenderTexture_Internal_CreateRenderTexture_m2106023999_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RenderTexture_Internal_CreateRenderTexture_m2106023999_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RenderTexture::Internal_CreateRenderTexture(UnityEngine.RenderTexture)");
	_il2cpp_icall_func(___rt0);
}
// System.Int32 UnityEngine.RenderTexture::Internal_GetWidth(UnityEngine.RenderTexture)
extern "C"  int32_t RenderTexture_Internal_GetWidth_m3496340582 (Il2CppObject * __this /* static, unused */, RenderTexture_t2108887433 * ___mono0, const MethodInfo* method)
{
	typedef int32_t (*RenderTexture_Internal_GetWidth_m3496340582_ftn) (RenderTexture_t2108887433 *);
	static RenderTexture_Internal_GetWidth_m3496340582_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RenderTexture_Internal_GetWidth_m3496340582_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RenderTexture::Internal_GetWidth(UnityEngine.RenderTexture)");
	return _il2cpp_icall_func(___mono0);
}
// System.Void UnityEngine.RenderTexture::Internal_SetWidth(UnityEngine.RenderTexture,System.Int32)
extern "C"  void RenderTexture_Internal_SetWidth_m4105368391 (Il2CppObject * __this /* static, unused */, RenderTexture_t2108887433 * ___mono0, int32_t ___width1, const MethodInfo* method)
{
	typedef void (*RenderTexture_Internal_SetWidth_m4105368391_ftn) (RenderTexture_t2108887433 *, int32_t);
	static RenderTexture_Internal_SetWidth_m4105368391_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RenderTexture_Internal_SetWidth_m4105368391_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RenderTexture::Internal_SetWidth(UnityEngine.RenderTexture,System.Int32)");
	_il2cpp_icall_func(___mono0, ___width1);
}
// System.Int32 UnityEngine.RenderTexture::Internal_GetHeight(UnityEngine.RenderTexture)
extern "C"  int32_t RenderTexture_Internal_GetHeight_m3135665775 (Il2CppObject * __this /* static, unused */, RenderTexture_t2108887433 * ___mono0, const MethodInfo* method)
{
	typedef int32_t (*RenderTexture_Internal_GetHeight_m3135665775_ftn) (RenderTexture_t2108887433 *);
	static RenderTexture_Internal_GetHeight_m3135665775_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RenderTexture_Internal_GetHeight_m3135665775_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RenderTexture::Internal_GetHeight(UnityEngine.RenderTexture)");
	return _il2cpp_icall_func(___mono0);
}
// System.Void UnityEngine.RenderTexture::Internal_SetHeight(UnityEngine.RenderTexture,System.Int32)
extern "C"  void RenderTexture_Internal_SetHeight_m4049466681 (Il2CppObject * __this /* static, unused */, RenderTexture_t2108887433 * ___mono0, int32_t ___width1, const MethodInfo* method)
{
	typedef void (*RenderTexture_Internal_SetHeight_m4049466681_ftn) (RenderTexture_t2108887433 *, int32_t);
	static RenderTexture_Internal_SetHeight_m4049466681_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RenderTexture_Internal_SetHeight_m4049466681_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RenderTexture::Internal_SetHeight(UnityEngine.RenderTexture,System.Int32)");
	_il2cpp_icall_func(___mono0, ___width1);
}
// System.Void UnityEngine.RenderTexture::Internal_SetSRGBReadWrite(UnityEngine.RenderTexture,System.Boolean)
extern "C"  void RenderTexture_Internal_SetSRGBReadWrite_m4197499185 (Il2CppObject * __this /* static, unused */, RenderTexture_t2108887433 * ___mono0, bool ___sRGB1, const MethodInfo* method)
{
	typedef void (*RenderTexture_Internal_SetSRGBReadWrite_m4197499185_ftn) (RenderTexture_t2108887433 *, bool);
	static RenderTexture_Internal_SetSRGBReadWrite_m4197499185_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RenderTexture_Internal_SetSRGBReadWrite_m4197499185_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RenderTexture::Internal_SetSRGBReadWrite(UnityEngine.RenderTexture,System.Boolean)");
	_il2cpp_icall_func(___mono0, ___sRGB1);
}
// System.Int32 UnityEngine.RenderTexture::get_width()
extern "C"  int32_t RenderTexture_get_width_m3536199159 (RenderTexture_t2108887433 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = RenderTexture_Internal_GetWidth_m3496340582(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		int32_t L_1 = V_0;
		return L_1;
	}
}
// System.Void UnityEngine.RenderTexture::set_width(System.Int32)
extern "C"  void RenderTexture_set_width_m511906021 (RenderTexture_t2108887433 * __this, int32_t ___value0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value0;
		RenderTexture_Internal_SetWidth_m4105368391(NULL /*static, unused*/, __this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Int32 UnityEngine.RenderTexture::get_height()
extern "C"  int32_t RenderTexture_get_height_m4079688824 (RenderTexture_t2108887433 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = RenderTexture_Internal_GetHeight_m3135665775(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		int32_t L_1 = V_0;
		return L_1;
	}
}
// System.Void UnityEngine.RenderTexture::set_height(System.Int32)
extern "C"  void RenderTexture_set_height_m1103570672 (RenderTexture_t2108887433 * __this, int32_t ___value0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value0;
		RenderTexture_Internal_SetHeight_m4049466681(NULL /*static, unused*/, __this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.RenderTexture::set_depth(System.Int32)
extern "C"  void RenderTexture_set_depth_m1608130061 (RenderTexture_t2108887433 * __this, int32_t ___value0, const MethodInfo* method)
{
	typedef void (*RenderTexture_set_depth_m1608130061_ftn) (RenderTexture_t2108887433 *, int32_t);
	static RenderTexture_set_depth_m1608130061_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RenderTexture_set_depth_m1608130061_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RenderTexture::set_depth(System.Int32)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.RenderTexture::set_format(UnityEngine.RenderTextureFormat)
extern "C"  void RenderTexture_set_format_m1605175643 (RenderTexture_t2108887433 * __this, int32_t ___value0, const MethodInfo* method)
{
	typedef void (*RenderTexture_set_format_m1605175643_ftn) (RenderTexture_t2108887433 *, int32_t);
	static RenderTexture_set_format_m1605175643_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RenderTexture_set_format_m1605175643_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RenderTexture::set_format(UnityEngine.RenderTextureFormat)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.RenderTexture::set_useMipMap(System.Boolean)
extern "C"  void RenderTexture_set_useMipMap_m2666985786 (RenderTexture_t2108887433 * __this, bool ___value0, const MethodInfo* method)
{
	typedef void (*RenderTexture_set_useMipMap_m2666985786_ftn) (RenderTexture_t2108887433 *, bool);
	static RenderTexture_set_useMipMap_m2666985786_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RenderTexture_set_useMipMap_m2666985786_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RenderTexture::set_useMipMap(System.Boolean)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.RenderTexture::set_antiAliasing(System.Int32)
extern "C"  void RenderTexture_set_antiAliasing_m568154309 (RenderTexture_t2108887433 * __this, int32_t ___value0, const MethodInfo* method)
{
	typedef void (*RenderTexture_set_antiAliasing_m568154309_ftn) (RenderTexture_t2108887433 *, int32_t);
	static RenderTexture_set_antiAliasing_m568154309_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RenderTexture_set_antiAliasing_m568154309_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RenderTexture::set_antiAliasing(System.Int32)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Boolean UnityEngine.RenderTexture::Create()
extern "C"  bool RenderTexture_Create_m837409768 (RenderTexture_t2108887433 * __this, const MethodInfo* method)
{
	bool V_0 = false;
	{
		bool L_0 = RenderTexture_INTERNAL_CALL_Create_m3116277818(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		bool L_1 = V_0;
		return L_1;
	}
}
// System.Boolean UnityEngine.RenderTexture::INTERNAL_CALL_Create(UnityEngine.RenderTexture)
extern "C"  bool RenderTexture_INTERNAL_CALL_Create_m3116277818 (Il2CppObject * __this /* static, unused */, RenderTexture_t2108887433 * ___self0, const MethodInfo* method)
{
	typedef bool (*RenderTexture_INTERNAL_CALL_Create_m3116277818_ftn) (RenderTexture_t2108887433 *);
	static RenderTexture_INTERNAL_CALL_Create_m3116277818_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RenderTexture_INTERNAL_CALL_Create_m3116277818_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RenderTexture::INTERNAL_CALL_Create(UnityEngine.RenderTexture)");
	return _il2cpp_icall_func(___self0);
}
// System.Void UnityEngine.RenderTexture::Release()
extern "C"  void RenderTexture_Release_m1154710812 (RenderTexture_t2108887433 * __this, const MethodInfo* method)
{
	{
		RenderTexture_INTERNAL_CALL_Release_m2580591883(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.RenderTexture::INTERNAL_CALL_Release(UnityEngine.RenderTexture)
extern "C"  void RenderTexture_INTERNAL_CALL_Release_m2580591883 (Il2CppObject * __this /* static, unused */, RenderTexture_t2108887433 * ___self0, const MethodInfo* method)
{
	typedef void (*RenderTexture_INTERNAL_CALL_Release_m2580591883_ftn) (RenderTexture_t2108887433 *);
	static RenderTexture_INTERNAL_CALL_Release_m2580591883_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RenderTexture_INTERNAL_CALL_Release_m2580591883_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RenderTexture::INTERNAL_CALL_Release(UnityEngine.RenderTexture)");
	_il2cpp_icall_func(___self0);
}
// System.Boolean UnityEngine.RenderTexture::IsCreated()
extern "C"  bool RenderTexture_IsCreated_m1114243524 (RenderTexture_t2108887433 * __this, const MethodInfo* method)
{
	bool V_0 = false;
	{
		bool L_0 = RenderTexture_INTERNAL_CALL_IsCreated_m2038887254(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		bool L_1 = V_0;
		return L_1;
	}
}
// System.Boolean UnityEngine.RenderTexture::INTERNAL_CALL_IsCreated(UnityEngine.RenderTexture)
extern "C"  bool RenderTexture_INTERNAL_CALL_IsCreated_m2038887254 (Il2CppObject * __this /* static, unused */, RenderTexture_t2108887433 * ___self0, const MethodInfo* method)
{
	typedef bool (*RenderTexture_INTERNAL_CALL_IsCreated_m2038887254_ftn) (RenderTexture_t2108887433 *);
	static RenderTexture_INTERNAL_CALL_IsCreated_m2038887254_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RenderTexture_INTERNAL_CALL_IsCreated_m2038887254_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RenderTexture::INTERNAL_CALL_IsCreated(UnityEngine.RenderTexture)");
	return _il2cpp_icall_func(___self0);
}
// System.Void UnityEngine.RequireComponent::.ctor(System.Type)
extern "C"  void RequireComponent__ctor_m876143304 (RequireComponent_t3490506609 * __this, Type_t * ___requiredComponent0, const MethodInfo* method)
{
	{
		Attribute__ctor_m1529526131(__this, /*hidden argument*/NULL);
		Type_t * L_0 = ___requiredComponent0;
		__this->set_m_Type0_0(L_0);
		return;
	}
}
// System.Void UnityEngine.RequireComponent::.ctor(System.Type,System.Type)
extern "C"  void RequireComponent__ctor_m201962841 (RequireComponent_t3490506609 * __this, Type_t * ___requiredComponent0, Type_t * ___requiredComponent21, const MethodInfo* method)
{
	{
		Attribute__ctor_m1529526131(__this, /*hidden argument*/NULL);
		Type_t * L_0 = ___requiredComponent0;
		__this->set_m_Type0_0(L_0);
		Type_t * L_1 = ___requiredComponent21;
		__this->set_m_Type1_1(L_1);
		return;
	}
}
// System.Int32 UnityEngine.Resolution::get_width()
extern "C"  int32_t Resolution_get_width_m1405037768 (Resolution_t2487619763 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_m_Width_0();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		int32_t L_1 = V_0;
		return L_1;
	}
}
extern "C"  int32_t Resolution_get_width_m1405037768_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Resolution_t2487619763 * _thisAdjusted = reinterpret_cast<Resolution_t2487619763 *>(__this + 1);
	return Resolution_get_width_m1405037768(_thisAdjusted, method);
}
// System.Int32 UnityEngine.Resolution::get_height()
extern "C"  int32_t Resolution_get_height_m1134023428 (Resolution_t2487619763 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_m_Height_1();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		int32_t L_1 = V_0;
		return L_1;
	}
}
extern "C"  int32_t Resolution_get_height_m1134023428_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Resolution_t2487619763 * _thisAdjusted = reinterpret_cast<Resolution_t2487619763 *>(__this + 1);
	return Resolution_get_height_m1134023428(_thisAdjusted, method);
}
// System.String UnityEngine.Resolution::ToString()
extern "C"  String_t* Resolution_ToString_m1856579011 (Resolution_t2487619763 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Resolution_ToString_m1856579011_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	{
		ObjectU5BU5D_t2843939325* L_0 = ((ObjectU5BU5D_t2843939325*)SZArrayNew(ObjectU5BU5D_t2843939325_il2cpp_TypeInfo_var, (uint32_t)3));
		int32_t L_1 = __this->get_m_Width_0();
		int32_t L_2 = L_1;
		Il2CppObject * L_3 = Box(Int32_t2950945753_il2cpp_TypeInfo_var, &L_2);
		NullCheck(L_0);
		ArrayElementTypeCheck (L_0, L_3);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_3);
		ObjectU5BU5D_t2843939325* L_4 = L_0;
		int32_t L_5 = __this->get_m_Height_1();
		int32_t L_6 = L_5;
		Il2CppObject * L_7 = Box(Int32_t2950945753_il2cpp_TypeInfo_var, &L_6);
		NullCheck(L_4);
		ArrayElementTypeCheck (L_4, L_7);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_7);
		ObjectU5BU5D_t2843939325* L_8 = L_4;
		int32_t L_9 = __this->get_m_RefreshRate_2();
		int32_t L_10 = L_9;
		Il2CppObject * L_11 = Box(Int32_t2950945753_il2cpp_TypeInfo_var, &L_10);
		NullCheck(L_8);
		ArrayElementTypeCheck (L_8, L_11);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)L_11);
		String_t* L_12 = UnityString_Format_m3741272017(NULL /*static, unused*/, _stringLiteral158126097, L_8, /*hidden argument*/NULL);
		V_0 = L_12;
		goto IL_0041;
	}

IL_0041:
	{
		String_t* L_13 = V_0;
		return L_13;
	}
}
extern "C"  String_t* Resolution_ToString_m1856579011_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Resolution_t2487619763 * _thisAdjusted = reinterpret_cast<Resolution_t2487619763 *>(__this + 1);
	return Resolution_ToString_m1856579011(_thisAdjusted, method);
}
// Conversion methods for marshalling of: UnityEngine.ResourceRequest
extern "C" void ResourceRequest_t3109103591_marshal_pinvoke(const ResourceRequest_t3109103591& unmarshaled, ResourceRequest_t3109103591_marshaled_pinvoke& marshaled)
{
	Il2CppCodeGenException* ___m_Type_2Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_Type' of type 'ResourceRequest': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_Type_2Exception);
}
extern "C" void ResourceRequest_t3109103591_marshal_pinvoke_back(const ResourceRequest_t3109103591_marshaled_pinvoke& marshaled, ResourceRequest_t3109103591& unmarshaled)
{
	Il2CppCodeGenException* ___m_Type_2Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_Type' of type 'ResourceRequest': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_Type_2Exception);
}
// Conversion method for clean up from marshalling of: UnityEngine.ResourceRequest
extern "C" void ResourceRequest_t3109103591_marshal_pinvoke_cleanup(ResourceRequest_t3109103591_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.ResourceRequest
extern "C" void ResourceRequest_t3109103591_marshal_com(const ResourceRequest_t3109103591& unmarshaled, ResourceRequest_t3109103591_marshaled_com& marshaled)
{
	Il2CppCodeGenException* ___m_Type_2Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_Type' of type 'ResourceRequest': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_Type_2Exception);
}
extern "C" void ResourceRequest_t3109103591_marshal_com_back(const ResourceRequest_t3109103591_marshaled_com& marshaled, ResourceRequest_t3109103591& unmarshaled)
{
	Il2CppCodeGenException* ___m_Type_2Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_Type' of type 'ResourceRequest': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_Type_2Exception);
}
// Conversion method for clean up from marshalling of: UnityEngine.ResourceRequest
extern "C" void ResourceRequest_t3109103591_marshal_com_cleanup(ResourceRequest_t3109103591_marshaled_com& marshaled)
{
}
// System.Void UnityEngine.ResourceRequest::.ctor()
extern "C"  void ResourceRequest__ctor_m3286674656 (ResourceRequest_t3109103591 * __this, const MethodInfo* method)
{
	{
		AsyncOperation__ctor_m2683541089(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Object UnityEngine.ResourceRequest::get_asset()
extern "C"  Object_t631007953 * ResourceRequest_get_asset_m2462824237 (ResourceRequest_t3109103591 * __this, const MethodInfo* method)
{
	Object_t631007953 * V_0 = NULL;
	{
		String_t* L_0 = __this->get_m_Path_1();
		Type_t * L_1 = __this->get_m_Type_2();
		Object_t631007953 * L_2 = Resources_Load_m4202687077(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		goto IL_0018;
	}

IL_0018:
	{
		Object_t631007953 * L_3 = V_0;
		return L_3;
	}
}
// UnityEngine.Object UnityEngine.Resources::Load(System.String)
extern "C"  Object_t631007953 * Resources_Load_m4211401679 (Il2CppObject * __this /* static, unused */, String_t* ___path0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Resources_Load_m4211401679_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Object_t631007953 * V_0 = NULL;
	{
		String_t* L_0 = ___path0;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_1 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, LoadTypeToken(Object_t631007953_0_0_0_var), /*hidden argument*/NULL);
		Object_t631007953 * L_2 = Resources_Load_m4202687077(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		goto IL_0017;
	}

IL_0017:
	{
		Object_t631007953 * L_3 = V_0;
		return L_3;
	}
}
// UnityEngine.Object UnityEngine.Resources::Load(System.String,System.Type)
extern "C"  Object_t631007953 * Resources_Load_m4202687077 (Il2CppObject * __this /* static, unused */, String_t* ___path0, Type_t * ___systemTypeInstance1, const MethodInfo* method)
{
	typedef Object_t631007953 * (*Resources_Load_m4202687077_ftn) (String_t*, Type_t *);
	static Resources_Load_m4202687077_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Resources_Load_m4202687077_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Resources::Load(System.String,System.Type)");
	return _il2cpp_icall_func(___path0, ___systemTypeInstance1);
}
// UnityEngine.Object UnityEngine.Resources::GetBuiltinResource(System.Type,System.String)
extern "C"  Object_t631007953 * Resources_GetBuiltinResource_m629864230 (Il2CppObject * __this /* static, unused */, Type_t * ___type0, String_t* ___path1, const MethodInfo* method)
{
	typedef Object_t631007953 * (*Resources_GetBuiltinResource_m629864230_ftn) (Type_t *, String_t*);
	static Resources_GetBuiltinResource_m629864230_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Resources_GetBuiltinResource_m629864230_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Resources::GetBuiltinResource(System.Type,System.String)");
	return _il2cpp_icall_func(___type0, ___path1);
}
// System.Void UnityEngine.Rigidbody::set_velocity(UnityEngine.Vector3)
extern "C"  void Rigidbody_set_velocity_m3256028887 (Rigidbody_t3916780224 * __this, Vector3_t3722313464  ___value0, const MethodInfo* method)
{
	{
		Rigidbody_INTERNAL_set_velocity_m3212559553(__this, (&___value0), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Rigidbody::INTERNAL_set_velocity(UnityEngine.Vector3&)
extern "C"  void Rigidbody_INTERNAL_set_velocity_m3212559553 (Rigidbody_t3916780224 * __this, Vector3_t3722313464 * ___value0, const MethodInfo* method)
{
	typedef void (*Rigidbody_INTERNAL_set_velocity_m3212559553_ftn) (Rigidbody_t3916780224 *, Vector3_t3722313464 *);
	static Rigidbody_INTERNAL_set_velocity_m3212559553_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody_INTERNAL_set_velocity_m3212559553_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody::INTERNAL_set_velocity(UnityEngine.Vector3&)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.Rigidbody::set_freezeRotation(System.Boolean)
extern "C"  void Rigidbody_set_freezeRotation_m178172768 (Rigidbody_t3916780224 * __this, bool ___value0, const MethodInfo* method)
{
	typedef void (*Rigidbody_set_freezeRotation_m178172768_ftn) (Rigidbody_t3916780224 *, bool);
	static Rigidbody_set_freezeRotation_m178172768_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody_set_freezeRotation_m178172768_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody::set_freezeRotation(System.Boolean)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.Rigidbody::AddForce(UnityEngine.Vector3,UnityEngine.ForceMode)
extern "C"  void Rigidbody_AddForce_m1699695356 (Rigidbody_t3916780224 * __this, Vector3_t3722313464  ___force0, int32_t ___mode1, const MethodInfo* method)
{
	{
		int32_t L_0 = ___mode1;
		Rigidbody_INTERNAL_CALL_AddForce_m2680516995(NULL /*static, unused*/, __this, (&___force0), L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Rigidbody::AddForce(UnityEngine.Vector3)
extern "C"  void Rigidbody_AddForce_m2190711883 (Rigidbody_t3916780224 * __this, Vector3_t3722313464  ___force0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		V_0 = 0;
		int32_t L_0 = V_0;
		Rigidbody_INTERNAL_CALL_AddForce_m2680516995(NULL /*static, unused*/, __this, (&___force0), L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Rigidbody::INTERNAL_CALL_AddForce(UnityEngine.Rigidbody,UnityEngine.Vector3&,UnityEngine.ForceMode)
extern "C"  void Rigidbody_INTERNAL_CALL_AddForce_m2680516995 (Il2CppObject * __this /* static, unused */, Rigidbody_t3916780224 * ___self0, Vector3_t3722313464 * ___force1, int32_t ___mode2, const MethodInfo* method)
{
	typedef void (*Rigidbody_INTERNAL_CALL_AddForce_m2680516995_ftn) (Rigidbody_t3916780224 *, Vector3_t3722313464 *, int32_t);
	static Rigidbody_INTERNAL_CALL_AddForce_m2680516995_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody_INTERNAL_CALL_AddForce_m2680516995_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody::INTERNAL_CALL_AddForce(UnityEngine.Rigidbody,UnityEngine.Vector3&,UnityEngine.ForceMode)");
	_il2cpp_icall_func(___self0, ___force1, ___mode2);
}
// System.Void UnityEngine.Rigidbody::MovePosition(UnityEngine.Vector3)
extern "C"  void Rigidbody_MovePosition_m3995401060 (Rigidbody_t3916780224 * __this, Vector3_t3722313464  ___position0, const MethodInfo* method)
{
	{
		Rigidbody_INTERNAL_CALL_MovePosition_m1701944164(NULL /*static, unused*/, __this, (&___position0), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Rigidbody::INTERNAL_CALL_MovePosition(UnityEngine.Rigidbody,UnityEngine.Vector3&)
extern "C"  void Rigidbody_INTERNAL_CALL_MovePosition_m1701944164 (Il2CppObject * __this /* static, unused */, Rigidbody_t3916780224 * ___self0, Vector3_t3722313464 * ___position1, const MethodInfo* method)
{
	typedef void (*Rigidbody_INTERNAL_CALL_MovePosition_m1701944164_ftn) (Rigidbody_t3916780224 *, Vector3_t3722313464 *);
	static Rigidbody_INTERNAL_CALL_MovePosition_m1701944164_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody_INTERNAL_CALL_MovePosition_m1701944164_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody::INTERNAL_CALL_MovePosition(UnityEngine.Rigidbody,UnityEngine.Vector3&)");
	_il2cpp_icall_func(___self0, ___position1);
}
// System.Void UnityEngine.Rigidbody::MoveRotation(UnityEngine.Quaternion)
extern "C"  void Rigidbody_MoveRotation_m900562931 (Rigidbody_t3916780224 * __this, Quaternion_t2301928331  ___rot0, const MethodInfo* method)
{
	{
		Rigidbody_INTERNAL_CALL_MoveRotation_m2836158393(NULL /*static, unused*/, __this, (&___rot0), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Rigidbody::INTERNAL_CALL_MoveRotation(UnityEngine.Rigidbody,UnityEngine.Quaternion&)
extern "C"  void Rigidbody_INTERNAL_CALL_MoveRotation_m2836158393 (Il2CppObject * __this /* static, unused */, Rigidbody_t3916780224 * ___self0, Quaternion_t2301928331 * ___rot1, const MethodInfo* method)
{
	typedef void (*Rigidbody_INTERNAL_CALL_MoveRotation_m2836158393_ftn) (Rigidbody_t3916780224 *, Quaternion_t2301928331 *);
	static Rigidbody_INTERNAL_CALL_MoveRotation_m2836158393_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody_INTERNAL_CALL_MoveRotation_m2836158393_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody::INTERNAL_CALL_MoveRotation(UnityEngine.Rigidbody,UnityEngine.Quaternion&)");
	_il2cpp_icall_func(___self0, ___rot1);
}
// System.Void UnityEngine.Rigidbody2D::set_velocity(UnityEngine.Vector2)
extern "C"  void Rigidbody2D_set_velocity_m1582823277 (Rigidbody2D_t939494601 * __this, Vector2_t2156229523  ___value0, const MethodInfo* method)
{
	{
		Rigidbody2D_INTERNAL_set_velocity_m3111408962(__this, (&___value0), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Rigidbody2D::INTERNAL_set_velocity(UnityEngine.Vector2&)
extern "C"  void Rigidbody2D_INTERNAL_set_velocity_m3111408962 (Rigidbody2D_t939494601 * __this, Vector2_t2156229523 * ___value0, const MethodInfo* method)
{
	typedef void (*Rigidbody2D_INTERNAL_set_velocity_m3111408962_ftn) (Rigidbody2D_t939494601 *, Vector2_t2156229523 *);
	static Rigidbody2D_INTERNAL_set_velocity_m3111408962_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody2D_INTERNAL_set_velocity_m3111408962_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody2D::INTERNAL_set_velocity(UnityEngine.Vector2&)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.Rigidbody2D::AddForce(UnityEngine.Vector2,UnityEngine.ForceMode2D)
extern "C"  void Rigidbody2D_AddForce_m1913769784 (Rigidbody2D_t939494601 * __this, Vector2_t2156229523  ___force0, int32_t ___mode1, const MethodInfo* method)
{
	{
		int32_t L_0 = ___mode1;
		Rigidbody2D_INTERNAL_CALL_AddForce_m3533004671(NULL /*static, unused*/, __this, (&___force0), L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Rigidbody2D::AddForce(UnityEngine.Vector2)
extern "C"  void Rigidbody2D_AddForce_m4053660933 (Rigidbody2D_t939494601 * __this, Vector2_t2156229523  ___force0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		V_0 = 0;
		int32_t L_0 = V_0;
		Rigidbody2D_INTERNAL_CALL_AddForce_m3533004671(NULL /*static, unused*/, __this, (&___force0), L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Rigidbody2D::INTERNAL_CALL_AddForce(UnityEngine.Rigidbody2D,UnityEngine.Vector2&,UnityEngine.ForceMode2D)
extern "C"  void Rigidbody2D_INTERNAL_CALL_AddForce_m3533004671 (Il2CppObject * __this /* static, unused */, Rigidbody2D_t939494601 * ___self0, Vector2_t2156229523 * ___force1, int32_t ___mode2, const MethodInfo* method)
{
	typedef void (*Rigidbody2D_INTERNAL_CALL_AddForce_m3533004671_ftn) (Rigidbody2D_t939494601 *, Vector2_t2156229523 *, int32_t);
	static Rigidbody2D_INTERNAL_CALL_AddForce_m3533004671_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody2D_INTERNAL_CALL_AddForce_m3533004671_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody2D::INTERNAL_CALL_AddForce(UnityEngine.Rigidbody2D,UnityEngine.Vector2&,UnityEngine.ForceMode2D)");
	_il2cpp_icall_func(___self0, ___force1, ___mode2);
}
// System.Void UnityEngine.RuntimeInitializeOnLoadMethodAttribute::.ctor(UnityEngine.RuntimeInitializeLoadType)
extern "C"  void RuntimeInitializeOnLoadMethodAttribute__ctor_m3105394426 (RuntimeInitializeOnLoadMethodAttribute_t3192313494 * __this, int32_t ___loadType0, const MethodInfo* method)
{
	{
		PreserveAttribute__ctor_m728193880(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___loadType0;
		RuntimeInitializeOnLoadMethodAttribute_set_loadType_m227377850(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.RuntimeInitializeOnLoadMethodAttribute::set_loadType(UnityEngine.RuntimeInitializeLoadType)
extern "C"  void RuntimeInitializeOnLoadMethodAttribute_set_loadType_m227377850 (RuntimeInitializeOnLoadMethodAttribute_t3192313494 * __this, int32_t ___value0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_U3CloadTypeU3Ek__BackingField_0(L_0);
		return;
	}
}
// System.Int32 UnityEngine.SceneManagement.Scene::get_handle()
extern "C"  int32_t Scene_get_handle_m1480030146 (Scene_t2348375561 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_m_Handle_0();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		int32_t L_1 = V_0;
		return L_1;
	}
}
extern "C"  int32_t Scene_get_handle_m1480030146_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Scene_t2348375561 * _thisAdjusted = reinterpret_cast<Scene_t2348375561 *>(__this + 1);
	return Scene_get_handle_m1480030146(_thisAdjusted, method);
}
// System.String UnityEngine.SceneManagement.Scene::get_name()
extern "C"  String_t* Scene_get_name_m2213167406 (Scene_t2348375561 * __this, const MethodInfo* method)
{
	String_t* V_0 = NULL;
	{
		int32_t L_0 = Scene_get_handle_m1480030146(__this, /*hidden argument*/NULL);
		String_t* L_1 = Scene_GetNameInternal_m3643126100(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_0012;
	}

IL_0012:
	{
		String_t* L_2 = V_0;
		return L_2;
	}
}
extern "C"  String_t* Scene_get_name_m2213167406_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Scene_t2348375561 * _thisAdjusted = reinterpret_cast<Scene_t2348375561 *>(__this + 1);
	return Scene_get_name_m2213167406(_thisAdjusted, method);
}
// System.Int32 UnityEngine.SceneManagement.Scene::GetHashCode()
extern "C"  int32_t Scene_GetHashCode_m42646907 (Scene_t2348375561 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_m_Handle_0();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		int32_t L_1 = V_0;
		return L_1;
	}
}
extern "C"  int32_t Scene_GetHashCode_m42646907_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Scene_t2348375561 * _thisAdjusted = reinterpret_cast<Scene_t2348375561 *>(__this + 1);
	return Scene_GetHashCode_m42646907(_thisAdjusted, method);
}
// System.Boolean UnityEngine.SceneManagement.Scene::Equals(System.Object)
extern "C"  bool Scene_Equals_m3811894426 (Scene_t2348375561 * __this, Il2CppObject * ___other0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Scene_Equals_m3811894426_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	Scene_t2348375561  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		Il2CppObject * L_0 = ___other0;
		if (((Il2CppObject *)IsInstSealed(L_0, Scene_t2348375561_il2cpp_TypeInfo_var)))
		{
			goto IL_0013;
		}
	}
	{
		V_0 = (bool)0;
		goto IL_002f;
	}

IL_0013:
	{
		Il2CppObject * L_1 = ___other0;
		V_1 = ((*(Scene_t2348375561 *)((Scene_t2348375561 *)UnBox(L_1, Scene_t2348375561_il2cpp_TypeInfo_var))));
		int32_t L_2 = Scene_get_handle_m1480030146(__this, /*hidden argument*/NULL);
		int32_t L_3 = Scene_get_handle_m1480030146((&V_1), /*hidden argument*/NULL);
		V_0 = (bool)((((int32_t)L_2) == ((int32_t)L_3))? 1 : 0);
		goto IL_002f;
	}

IL_002f:
	{
		bool L_4 = V_0;
		return L_4;
	}
}
extern "C"  bool Scene_Equals_m3811894426_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___other0, const MethodInfo* method)
{
	Scene_t2348375561 * _thisAdjusted = reinterpret_cast<Scene_t2348375561 *>(__this + 1);
	return Scene_Equals_m3811894426(_thisAdjusted, ___other0, method);
}
// System.String UnityEngine.SceneManagement.Scene::GetNameInternal(System.Int32)
extern "C"  String_t* Scene_GetNameInternal_m3643126100 (Il2CppObject * __this /* static, unused */, int32_t ___sceneHandle0, const MethodInfo* method)
{
	typedef String_t* (*Scene_GetNameInternal_m3643126100_ftn) (int32_t);
	static Scene_GetNameInternal_m3643126100_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Scene_GetNameInternal_m3643126100_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SceneManagement.Scene::GetNameInternal(System.Int32)");
	return _il2cpp_icall_func(___sceneHandle0);
}
// UnityEngine.SceneManagement.Scene UnityEngine.SceneManagement.SceneManager::GetActiveScene()
extern "C"  Scene_t2348375561  SceneManager_GetActiveScene_m2055923422 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	Scene_t2348375561  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Scene_t2348375561  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		SceneManager_INTERNAL_CALL_GetActiveScene_m756000170(NULL /*static, unused*/, (&V_0), /*hidden argument*/NULL);
		Scene_t2348375561  L_0 = V_0;
		V_1 = L_0;
		goto IL_000f;
	}

IL_000f:
	{
		Scene_t2348375561  L_1 = V_1;
		return L_1;
	}
}
// System.Void UnityEngine.SceneManagement.SceneManager::INTERNAL_CALL_GetActiveScene(UnityEngine.SceneManagement.Scene&)
extern "C"  void SceneManager_INTERNAL_CALL_GetActiveScene_m756000170 (Il2CppObject * __this /* static, unused */, Scene_t2348375561 * ___value0, const MethodInfo* method)
{
	typedef void (*SceneManager_INTERNAL_CALL_GetActiveScene_m756000170_ftn) (Scene_t2348375561 *);
	static SceneManager_INTERNAL_CALL_GetActiveScene_m756000170_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (SceneManager_INTERNAL_CALL_GetActiveScene_m756000170_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SceneManagement.SceneManager::INTERNAL_CALL_GetActiveScene(UnityEngine.SceneManagement.Scene&)");
	_il2cpp_icall_func(___value0);
}
// System.Boolean UnityEngine.SceneManagement.SceneManager::SetActiveScene(UnityEngine.SceneManagement.Scene)
extern "C"  bool SceneManager_SetActiveScene_m1643521145 (Il2CppObject * __this /* static, unused */, Scene_t2348375561  ___scene0, const MethodInfo* method)
{
	bool V_0 = false;
	{
		bool L_0 = SceneManager_INTERNAL_CALL_SetActiveScene_m4174233036(NULL /*static, unused*/, (&___scene0), /*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_000e;
	}

IL_000e:
	{
		bool L_1 = V_0;
		return L_1;
	}
}
// System.Boolean UnityEngine.SceneManagement.SceneManager::INTERNAL_CALL_SetActiveScene(UnityEngine.SceneManagement.Scene&)
extern "C"  bool SceneManager_INTERNAL_CALL_SetActiveScene_m4174233036 (Il2CppObject * __this /* static, unused */, Scene_t2348375561 * ___scene0, const MethodInfo* method)
{
	typedef bool (*SceneManager_INTERNAL_CALL_SetActiveScene_m4174233036_ftn) (Scene_t2348375561 *);
	static SceneManager_INTERNAL_CALL_SetActiveScene_m4174233036_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (SceneManager_INTERNAL_CALL_SetActiveScene_m4174233036_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SceneManagement.SceneManager::INTERNAL_CALL_SetActiveScene(UnityEngine.SceneManagement.Scene&)");
	return _il2cpp_icall_func(___scene0);
}
// UnityEngine.SceneManagement.Scene UnityEngine.SceneManagement.SceneManager::GetSceneByName(System.String)
extern "C"  Scene_t2348375561  SceneManager_GetSceneByName_m429203158 (Il2CppObject * __this /* static, unused */, String_t* ___name0, const MethodInfo* method)
{
	Scene_t2348375561  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Scene_t2348375561  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		String_t* L_0 = ___name0;
		SceneManager_INTERNAL_CALL_GetSceneByName_m4198796804(NULL /*static, unused*/, L_0, (&V_0), /*hidden argument*/NULL);
		Scene_t2348375561  L_1 = V_0;
		V_1 = L_1;
		goto IL_0010;
	}

IL_0010:
	{
		Scene_t2348375561  L_2 = V_1;
		return L_2;
	}
}
// System.Void UnityEngine.SceneManagement.SceneManager::INTERNAL_CALL_GetSceneByName(System.String,UnityEngine.SceneManagement.Scene&)
extern "C"  void SceneManager_INTERNAL_CALL_GetSceneByName_m4198796804 (Il2CppObject * __this /* static, unused */, String_t* ___name0, Scene_t2348375561 * ___value1, const MethodInfo* method)
{
	typedef void (*SceneManager_INTERNAL_CALL_GetSceneByName_m4198796804_ftn) (String_t*, Scene_t2348375561 *);
	static SceneManager_INTERNAL_CALL_GetSceneByName_m4198796804_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (SceneManager_INTERNAL_CALL_GetSceneByName_m4198796804_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SceneManagement.SceneManager::INTERNAL_CALL_GetSceneByName(System.String,UnityEngine.SceneManagement.Scene&)");
	_il2cpp_icall_func(___name0, ___value1);
}
// System.Void UnityEngine.SceneManagement.SceneManager::LoadScene(System.String)
extern "C"  void SceneManager_LoadScene_m3660838673 (Il2CppObject * __this /* static, unused */, String_t* ___sceneName0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		V_0 = 0;
		String_t* L_0 = ___sceneName0;
		int32_t L_1 = V_0;
		SceneManager_LoadScene_m752772171(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.SceneManagement.SceneManager::LoadScene(System.String,UnityEngine.SceneManagement.LoadSceneMode)
extern "C"  void SceneManager_LoadScene_m752772171 (Il2CppObject * __this /* static, unused */, String_t* ___sceneName0, int32_t ___mode1, const MethodInfo* method)
{
	int32_t G_B2_0 = 0;
	String_t* G_B2_1 = NULL;
	int32_t G_B1_0 = 0;
	String_t* G_B1_1 = NULL;
	int32_t G_B3_0 = 0;
	int32_t G_B3_1 = 0;
	String_t* G_B3_2 = NULL;
	{
		String_t* L_0 = ___sceneName0;
		int32_t L_1 = ___mode1;
		G_B1_0 = (-1);
		G_B1_1 = L_0;
		if ((!(((uint32_t)L_1) == ((uint32_t)1))))
		{
			G_B2_0 = (-1);
			G_B2_1 = L_0;
			goto IL_0010;
		}
	}
	{
		G_B3_0 = 1;
		G_B3_1 = G_B1_0;
		G_B3_2 = G_B1_1;
		goto IL_0011;
	}

IL_0010:
	{
		G_B3_0 = 0;
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
	}

IL_0011:
	{
		SceneManager_LoadSceneAsyncNameIndexInternal_m756833036(NULL /*static, unused*/, G_B3_2, G_B3_1, (bool)G_B3_0, (bool)1, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.AsyncOperation UnityEngine.SceneManagement.SceneManager::LoadSceneAsync(System.String)
extern "C"  AsyncOperation_t1445031843 * SceneManager_LoadSceneAsync_m3551625386 (Il2CppObject * __this /* static, unused */, String_t* ___sceneName0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	AsyncOperation_t1445031843 * V_1 = NULL;
	{
		V_0 = 0;
		String_t* L_0 = ___sceneName0;
		int32_t L_1 = V_0;
		AsyncOperation_t1445031843 * L_2 = SceneManager_LoadSceneAsync_m2750151360(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		V_1 = L_2;
		goto IL_0010;
	}

IL_0010:
	{
		AsyncOperation_t1445031843 * L_3 = V_1;
		return L_3;
	}
}
// UnityEngine.AsyncOperation UnityEngine.SceneManagement.SceneManager::LoadSceneAsync(System.String,UnityEngine.SceneManagement.LoadSceneMode)
extern "C"  AsyncOperation_t1445031843 * SceneManager_LoadSceneAsync_m2750151360 (Il2CppObject * __this /* static, unused */, String_t* ___sceneName0, int32_t ___mode1, const MethodInfo* method)
{
	AsyncOperation_t1445031843 * V_0 = NULL;
	int32_t G_B2_0 = 0;
	String_t* G_B2_1 = NULL;
	int32_t G_B1_0 = 0;
	String_t* G_B1_1 = NULL;
	int32_t G_B3_0 = 0;
	int32_t G_B3_1 = 0;
	String_t* G_B3_2 = NULL;
	{
		String_t* L_0 = ___sceneName0;
		int32_t L_1 = ___mode1;
		G_B1_0 = (-1);
		G_B1_1 = L_0;
		if ((!(((uint32_t)L_1) == ((uint32_t)1))))
		{
			G_B2_0 = (-1);
			G_B2_1 = L_0;
			goto IL_0010;
		}
	}
	{
		G_B3_0 = 1;
		G_B3_1 = G_B1_0;
		G_B3_2 = G_B1_1;
		goto IL_0011;
	}

IL_0010:
	{
		G_B3_0 = 0;
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
	}

IL_0011:
	{
		AsyncOperation_t1445031843 * L_2 = SceneManager_LoadSceneAsyncNameIndexInternal_m756833036(NULL /*static, unused*/, G_B3_2, G_B3_1, (bool)G_B3_0, (bool)0, /*hidden argument*/NULL);
		V_0 = L_2;
		goto IL_001d;
	}

IL_001d:
	{
		AsyncOperation_t1445031843 * L_3 = V_0;
		return L_3;
	}
}
// UnityEngine.AsyncOperation UnityEngine.SceneManagement.SceneManager::LoadSceneAsyncNameIndexInternal(System.String,System.Int32,System.Boolean,System.Boolean)
extern "C"  AsyncOperation_t1445031843 * SceneManager_LoadSceneAsyncNameIndexInternal_m756833036 (Il2CppObject * __this /* static, unused */, String_t* ___sceneName0, int32_t ___sceneBuildIndex1, bool ___isAdditive2, bool ___mustCompleteNextFrame3, const MethodInfo* method)
{
	typedef AsyncOperation_t1445031843 * (*SceneManager_LoadSceneAsyncNameIndexInternal_m756833036_ftn) (String_t*, int32_t, bool, bool);
	static SceneManager_LoadSceneAsyncNameIndexInternal_m756833036_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (SceneManager_LoadSceneAsyncNameIndexInternal_m756833036_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SceneManagement.SceneManager::LoadSceneAsyncNameIndexInternal(System.String,System.Int32,System.Boolean,System.Boolean)");
	return _il2cpp_icall_func(___sceneName0, ___sceneBuildIndex1, ___isAdditive2, ___mustCompleteNextFrame3);
}
// System.Void UnityEngine.SceneManagement.SceneManager::Internal_SceneLoaded(UnityEngine.SceneManagement.Scene,UnityEngine.SceneManagement.LoadSceneMode)
extern "C"  void SceneManager_Internal_SceneLoaded_m4023393011 (Il2CppObject * __this /* static, unused */, Scene_t2348375561  ___scene0, int32_t ___mode1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SceneManager_Internal_SceneLoaded_m4023393011_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		UnityAction_2_t2165061829 * L_0 = ((SceneManager_t2787271929_StaticFields*)SceneManager_t2787271929_il2cpp_TypeInfo_var->static_fields)->get_sceneLoaded_0();
		if (!L_0)
		{
			goto IL_0019;
		}
	}
	{
		UnityAction_2_t2165061829 * L_1 = ((SceneManager_t2787271929_StaticFields*)SceneManager_t2787271929_il2cpp_TypeInfo_var->static_fields)->get_sceneLoaded_0();
		Scene_t2348375561  L_2 = ___scene0;
		int32_t L_3 = ___mode1;
		NullCheck(L_1);
		UnityAction_2_Invoke_m1297655681(L_1, L_2, L_3, /*hidden argument*/UnityAction_2_Invoke_m1297655681_MethodInfo_var);
	}

IL_0019:
	{
		return;
	}
}
// System.Void UnityEngine.SceneManagement.SceneManager::Internal_SceneUnloaded(UnityEngine.SceneManagement.Scene)
extern "C"  void SceneManager_Internal_SceneUnloaded_m2496246859 (Il2CppObject * __this /* static, unused */, Scene_t2348375561  ___scene0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SceneManager_Internal_SceneUnloaded_m2496246859_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		UnityAction_1_t2933211702 * L_0 = ((SceneManager_t2787271929_StaticFields*)SceneManager_t2787271929_il2cpp_TypeInfo_var->static_fields)->get_sceneUnloaded_1();
		if (!L_0)
		{
			goto IL_0018;
		}
	}
	{
		UnityAction_1_t2933211702 * L_1 = ((SceneManager_t2787271929_StaticFields*)SceneManager_t2787271929_il2cpp_TypeInfo_var->static_fields)->get_sceneUnloaded_1();
		Scene_t2348375561  L_2 = ___scene0;
		NullCheck(L_1);
		UnityAction_1_Invoke_m3689685399(L_1, L_2, /*hidden argument*/UnityAction_1_Invoke_m3689685399_MethodInfo_var);
	}

IL_0018:
	{
		return;
	}
}
// System.Void UnityEngine.SceneManagement.SceneManager::Internal_ActiveSceneChanged(UnityEngine.SceneManagement.Scene,UnityEngine.SceneManagement.Scene)
extern "C"  void SceneManager_Internal_ActiveSceneChanged_m1220784726 (Il2CppObject * __this /* static, unused */, Scene_t2348375561  ___previousActiveScene0, Scene_t2348375561  ___newActiveScene1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SceneManager_Internal_ActiveSceneChanged_m1220784726_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		UnityAction_2_t1262235195 * L_0 = ((SceneManager_t2787271929_StaticFields*)SceneManager_t2787271929_il2cpp_TypeInfo_var->static_fields)->get_activeSceneChanged_2();
		if (!L_0)
		{
			goto IL_0019;
		}
	}
	{
		UnityAction_2_t1262235195 * L_1 = ((SceneManager_t2787271929_StaticFields*)SceneManager_t2787271929_il2cpp_TypeInfo_var->static_fields)->get_activeSceneChanged_2();
		Scene_t2348375561  L_2 = ___previousActiveScene0;
		Scene_t2348375561  L_3 = ___newActiveScene1;
		NullCheck(L_1);
		UnityAction_2_Invoke_m770027984(L_1, L_2, L_3, /*hidden argument*/UnityAction_2_Invoke_m770027984_MethodInfo_var);
	}

IL_0019:
	{
		return;
	}
}
// UnityEngine.Resolution UnityEngine.Screen::get_currentResolution()
extern "C"  Resolution_t2487619763  Screen_get_currentResolution_m321766258 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef Resolution_t2487619763  (*Screen_get_currentResolution_m321766258_ftn) ();
	static Screen_get_currentResolution_m321766258_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Screen_get_currentResolution_m321766258_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Screen::get_currentResolution()");
	return _il2cpp_icall_func();
}
// System.Int32 UnityEngine.Screen::get_width()
extern "C"  int32_t Screen_get_width_m2061181276 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef int32_t (*Screen_get_width_m2061181276_ftn) ();
	static Screen_get_width_m2061181276_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Screen_get_width_m2061181276_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Screen::get_width()");
	return _il2cpp_icall_func();
}
// System.Int32 UnityEngine.Screen::get_height()
extern "C"  int32_t Screen_get_height_m1569321028 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef int32_t (*Screen_get_height_m1569321028_ftn) ();
	static Screen_get_height_m1569321028_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Screen_get_height_m1569321028_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Screen::get_height()");
	return _il2cpp_icall_func();
}
// System.Single UnityEngine.Screen::get_dpi()
extern "C"  float Screen_get_dpi_m1361565183 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef float (*Screen_get_dpi_m1361565183_ftn) ();
	static Screen_get_dpi_m1361565183_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Screen_get_dpi_m1361565183_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Screen::get_dpi()");
	return _il2cpp_icall_func();
}
// UnityEngine.ScreenOrientation UnityEngine.Screen::get_orientation()
extern "C"  int32_t Screen_get_orientation_m29745804 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef int32_t (*Screen_get_orientation_m29745804_ftn) ();
	static Screen_get_orientation_m29745804_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Screen_get_orientation_m29745804_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Screen::get_orientation()");
	return _il2cpp_icall_func();
}
// System.Void UnityEngine.Screen::set_orientation(UnityEngine.ScreenOrientation)
extern "C"  void Screen_set_orientation_m238726225 (Il2CppObject * __this /* static, unused */, int32_t ___value0, const MethodInfo* method)
{
	typedef void (*Screen_set_orientation_m238726225_ftn) (int32_t);
	static Screen_set_orientation_m238726225_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Screen_set_orientation_m238726225_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Screen::set_orientation(UnityEngine.ScreenOrientation)");
	_il2cpp_icall_func(___value0);
}
// Conversion methods for marshalling of: UnityEngine.ScriptableObject
extern "C" void ScriptableObject_t2528358522_marshal_pinvoke(const ScriptableObject_t2528358522& unmarshaled, ScriptableObject_t2528358522_marshaled_pinvoke& marshaled)
{
	marshaled.___m_CachedPtr_0 = reinterpret_cast<intptr_t>((unmarshaled.get_m_CachedPtr_0()).get_m_value_0());
}
extern "C" void ScriptableObject_t2528358522_marshal_pinvoke_back(const ScriptableObject_t2528358522_marshaled_pinvoke& marshaled, ScriptableObject_t2528358522& unmarshaled)
{
	IntPtr_t unmarshaled_m_CachedPtr_temp_0;
	memset(&unmarshaled_m_CachedPtr_temp_0, 0, sizeof(unmarshaled_m_CachedPtr_temp_0));
	IntPtr_t unmarshaled_m_CachedPtr_temp_0_temp;
	unmarshaled_m_CachedPtr_temp_0_temp.set_m_value_0(reinterpret_cast<void*>((intptr_t)(marshaled.___m_CachedPtr_0)));
	unmarshaled_m_CachedPtr_temp_0 = unmarshaled_m_CachedPtr_temp_0_temp;
	unmarshaled.set_m_CachedPtr_0(unmarshaled_m_CachedPtr_temp_0);
}
// Conversion method for clean up from marshalling of: UnityEngine.ScriptableObject
extern "C" void ScriptableObject_t2528358522_marshal_pinvoke_cleanup(ScriptableObject_t2528358522_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.ScriptableObject
extern "C" void ScriptableObject_t2528358522_marshal_com(const ScriptableObject_t2528358522& unmarshaled, ScriptableObject_t2528358522_marshaled_com& marshaled)
{
	marshaled.___m_CachedPtr_0 = reinterpret_cast<intptr_t>((unmarshaled.get_m_CachedPtr_0()).get_m_value_0());
}
extern "C" void ScriptableObject_t2528358522_marshal_com_back(const ScriptableObject_t2528358522_marshaled_com& marshaled, ScriptableObject_t2528358522& unmarshaled)
{
	IntPtr_t unmarshaled_m_CachedPtr_temp_0;
	memset(&unmarshaled_m_CachedPtr_temp_0, 0, sizeof(unmarshaled_m_CachedPtr_temp_0));
	IntPtr_t unmarshaled_m_CachedPtr_temp_0_temp;
	unmarshaled_m_CachedPtr_temp_0_temp.set_m_value_0(reinterpret_cast<void*>((intptr_t)(marshaled.___m_CachedPtr_0)));
	unmarshaled_m_CachedPtr_temp_0 = unmarshaled_m_CachedPtr_temp_0_temp;
	unmarshaled.set_m_CachedPtr_0(unmarshaled_m_CachedPtr_temp_0);
}
// Conversion method for clean up from marshalling of: UnityEngine.ScriptableObject
extern "C" void ScriptableObject_t2528358522_marshal_com_cleanup(ScriptableObject_t2528358522_marshaled_com& marshaled)
{
}
// System.Void UnityEngine.ScriptableObject::.ctor()
extern "C"  void ScriptableObject__ctor_m4119605938 (ScriptableObject_t2528358522 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ScriptableObject__ctor_m4119605938_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		Object__ctor_m1560822313(__this, /*hidden argument*/NULL);
		ScriptableObject_Internal_CreateScriptableObject_m163809621(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.ScriptableObject::Internal_CreateScriptableObject(UnityEngine.ScriptableObject)
extern "C"  void ScriptableObject_Internal_CreateScriptableObject_m163809621 (Il2CppObject * __this /* static, unused */, ScriptableObject_t2528358522 * ___self0, const MethodInfo* method)
{
	typedef void (*ScriptableObject_Internal_CreateScriptableObject_m163809621_ftn) (ScriptableObject_t2528358522 *);
	static ScriptableObject_Internal_CreateScriptableObject_m163809621_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (ScriptableObject_Internal_CreateScriptableObject_m163809621_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ScriptableObject::Internal_CreateScriptableObject(UnityEngine.ScriptableObject)");
	_il2cpp_icall_func(___self0);
}
// UnityEngine.ScriptableObject UnityEngine.ScriptableObject::CreateInstance(System.String)
extern "C"  ScriptableObject_t2528358522 * ScriptableObject_CreateInstance_m1668078119 (Il2CppObject * __this /* static, unused */, String_t* ___className0, const MethodInfo* method)
{
	typedef ScriptableObject_t2528358522 * (*ScriptableObject_CreateInstance_m1668078119_ftn) (String_t*);
	static ScriptableObject_CreateInstance_m1668078119_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (ScriptableObject_CreateInstance_m1668078119_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ScriptableObject::CreateInstance(System.String)");
	return _il2cpp_icall_func(___className0);
}
// UnityEngine.ScriptableObject UnityEngine.ScriptableObject::CreateInstance(System.Type)
extern "C"  ScriptableObject_t2528358522 * ScriptableObject_CreateInstance_m2508048690 (Il2CppObject * __this /* static, unused */, Type_t * ___type0, const MethodInfo* method)
{
	ScriptableObject_t2528358522 * V_0 = NULL;
	{
		Type_t * L_0 = ___type0;
		ScriptableObject_t2528358522 * L_1 = ScriptableObject_CreateInstanceFromType_m3616970571(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_000d;
	}

IL_000d:
	{
		ScriptableObject_t2528358522 * L_2 = V_0;
		return L_2;
	}
}
// UnityEngine.ScriptableObject UnityEngine.ScriptableObject::CreateInstanceFromType(System.Type)
extern "C"  ScriptableObject_t2528358522 * ScriptableObject_CreateInstanceFromType_m3616970571 (Il2CppObject * __this /* static, unused */, Type_t * ___type0, const MethodInfo* method)
{
	typedef ScriptableObject_t2528358522 * (*ScriptableObject_CreateInstanceFromType_m3616970571_ftn) (Type_t *);
	static ScriptableObject_CreateInstanceFromType_m3616970571_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (ScriptableObject_CreateInstanceFromType_m3616970571_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.ScriptableObject::CreateInstanceFromType(System.Type)");
	return _il2cpp_icall_func(___type0);
}
// System.Void UnityEngine.Scripting.APIUpdating.MovedFromAttribute::.ctor(System.String)
extern "C"  void MovedFromAttribute__ctor_m2951223501 (MovedFromAttribute_t481952341 * __this, String_t* ___sourceNamespace0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___sourceNamespace0;
		MovedFromAttribute__ctor_m1576224919(__this, L_0, (bool)0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Scripting.APIUpdating.MovedFromAttribute::.ctor(System.String,System.Boolean)
extern "C"  void MovedFromAttribute__ctor_m1576224919 (MovedFromAttribute_t481952341 * __this, String_t* ___sourceNamespace0, bool ___isInDifferentAssembly1, const MethodInfo* method)
{
	{
		Attribute__ctor_m1529526131(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___sourceNamespace0;
		MovedFromAttribute_set_Namespace_m2813913251(__this, L_0, /*hidden argument*/NULL);
		bool L_1 = ___isInDifferentAssembly1;
		MovedFromAttribute_set_IsInDifferentAssembly_m1665805492(__this, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Scripting.APIUpdating.MovedFromAttribute::set_Namespace(System.String)
extern "C"  void MovedFromAttribute_set_Namespace_m2813913251 (MovedFromAttribute_t481952341 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CNamespaceU3Ek__BackingField_0(L_0);
		return;
	}
}
// System.Void UnityEngine.Scripting.APIUpdating.MovedFromAttribute::set_IsInDifferentAssembly(System.Boolean)
extern "C"  void MovedFromAttribute_set_IsInDifferentAssembly_m1665805492 (MovedFromAttribute_t481952341 * __this, bool ___value0, const MethodInfo* method)
{
	{
		bool L_0 = ___value0;
		__this->set_U3CIsInDifferentAssemblyU3Ek__BackingField_1(L_0);
		return;
	}
}
// System.Void UnityEngine.Scripting.GeneratedByOldBindingsGeneratorAttribute::.ctor()
extern "C"  void GeneratedByOldBindingsGeneratorAttribute__ctor_m2250379507 (GeneratedByOldBindingsGeneratorAttribute_t433318409 * __this, const MethodInfo* method)
{
	{
		Attribute__ctor_m1529526131(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Scripting.PreserveAttribute::.ctor()
extern "C"  void PreserveAttribute__ctor_m728193880 (PreserveAttribute_t1583619344 * __this, const MethodInfo* method)
{
	{
		Attribute__ctor_m1529526131(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Scripting.RequiredByNativeCodeAttribute::.ctor()
extern "C"  void RequiredByNativeCodeAttribute__ctor_m198258531 (RequiredByNativeCodeAttribute_t4130846357 * __this, const MethodInfo* method)
{
	{
		Attribute__ctor_m1529526131(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Scripting.RequiredByNativeCodeAttribute::.ctor(System.String)
extern "C"  void RequiredByNativeCodeAttribute__ctor_m3697446351 (RequiredByNativeCodeAttribute_t4130846357 * __this, String_t* ___name0, const MethodInfo* method)
{
	{
		Attribute__ctor_m1529526131(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___name0;
		RequiredByNativeCodeAttribute_set_Name_m3281191310(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Scripting.RequiredByNativeCodeAttribute::set_Name(System.String)
extern "C"  void RequiredByNativeCodeAttribute_set_Name_m3281191310 (RequiredByNativeCodeAttribute_t4130846357 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CNameU3Ek__BackingField_0(L_0);
		return;
	}
}
// System.Void UnityEngine.Scripting.RequiredByNativeCodeAttribute::set_Optional(System.Boolean)
extern "C"  void RequiredByNativeCodeAttribute_set_Optional_m1840840280 (RequiredByNativeCodeAttribute_t4130846357 * __this, bool ___value0, const MethodInfo* method)
{
	{
		bool L_0 = ___value0;
		__this->set_U3COptionalU3Ek__BackingField_1(L_0);
		return;
	}
}
// System.Void UnityEngine.Scripting.UsedByNativeCodeAttribute::.ctor()
extern "C"  void UsedByNativeCodeAttribute__ctor_m2648028619 (UsedByNativeCodeAttribute_t1703770351 * __this, const MethodInfo* method)
{
	{
		Attribute__ctor_m1529526131(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.ScrollViewState::.ctor()
extern "C"  void ScrollViewState__ctor_m831262005 (ScrollViewState_t3797911395 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m297566312(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.SelectionBaseAttribute::.ctor()
extern "C"  void SelectionBaseAttribute__ctor_m2511653421 (SelectionBaseAttribute_t3493465804 * __this, const MethodInfo* method)
{
	{
		Attribute__ctor_m1529526131(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
