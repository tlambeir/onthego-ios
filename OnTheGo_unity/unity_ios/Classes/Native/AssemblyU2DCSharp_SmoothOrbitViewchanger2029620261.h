﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour3962482529.h"
#include "UnityEngine_UnityEngine_Vector33722313464.h"
#include "UnityEngine_UnityEngine_Quaternion2301928331.h"
#include "UnityEngine_UnityEngine_Vector22156229523.h"

// SmoothOrbitCam
struct SmoothOrbitCam_t3313789169;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SmoothOrbitViewchanger
struct  SmoothOrbitViewchanger_t2029620261  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Vector3 SmoothOrbitViewchanger::Rotation
	Vector3_t3722313464  ___Rotation_2;
	// UnityEngine.Quaternion SmoothOrbitViewchanger::RotaQuat
	Quaternion_t2301928331  ___RotaQuat_3;
	// System.Single SmoothOrbitViewchanger::Distance
	float ___Distance_4;
	// UnityEngine.Vector2 SmoothOrbitViewchanger::PanValues
	Vector2_t2156229523  ___PanValues_5;
	// System.Single SmoothOrbitViewchanger::speed
	float ___speed_6;
	// SmoothOrbitCam SmoothOrbitViewchanger::smoothOrbitCam
	SmoothOrbitCam_t3313789169 * ___smoothOrbitCam_7;
	// System.Boolean SmoothOrbitViewchanger::moving
	bool ___moving_8;

public:
	inline static int32_t get_offset_of_Rotation_2() { return static_cast<int32_t>(offsetof(SmoothOrbitViewchanger_t2029620261, ___Rotation_2)); }
	inline Vector3_t3722313464  get_Rotation_2() const { return ___Rotation_2; }
	inline Vector3_t3722313464 * get_address_of_Rotation_2() { return &___Rotation_2; }
	inline void set_Rotation_2(Vector3_t3722313464  value)
	{
		___Rotation_2 = value;
	}

	inline static int32_t get_offset_of_RotaQuat_3() { return static_cast<int32_t>(offsetof(SmoothOrbitViewchanger_t2029620261, ___RotaQuat_3)); }
	inline Quaternion_t2301928331  get_RotaQuat_3() const { return ___RotaQuat_3; }
	inline Quaternion_t2301928331 * get_address_of_RotaQuat_3() { return &___RotaQuat_3; }
	inline void set_RotaQuat_3(Quaternion_t2301928331  value)
	{
		___RotaQuat_3 = value;
	}

	inline static int32_t get_offset_of_Distance_4() { return static_cast<int32_t>(offsetof(SmoothOrbitViewchanger_t2029620261, ___Distance_4)); }
	inline float get_Distance_4() const { return ___Distance_4; }
	inline float* get_address_of_Distance_4() { return &___Distance_4; }
	inline void set_Distance_4(float value)
	{
		___Distance_4 = value;
	}

	inline static int32_t get_offset_of_PanValues_5() { return static_cast<int32_t>(offsetof(SmoothOrbitViewchanger_t2029620261, ___PanValues_5)); }
	inline Vector2_t2156229523  get_PanValues_5() const { return ___PanValues_5; }
	inline Vector2_t2156229523 * get_address_of_PanValues_5() { return &___PanValues_5; }
	inline void set_PanValues_5(Vector2_t2156229523  value)
	{
		___PanValues_5 = value;
	}

	inline static int32_t get_offset_of_speed_6() { return static_cast<int32_t>(offsetof(SmoothOrbitViewchanger_t2029620261, ___speed_6)); }
	inline float get_speed_6() const { return ___speed_6; }
	inline float* get_address_of_speed_6() { return &___speed_6; }
	inline void set_speed_6(float value)
	{
		___speed_6 = value;
	}

	inline static int32_t get_offset_of_smoothOrbitCam_7() { return static_cast<int32_t>(offsetof(SmoothOrbitViewchanger_t2029620261, ___smoothOrbitCam_7)); }
	inline SmoothOrbitCam_t3313789169 * get_smoothOrbitCam_7() const { return ___smoothOrbitCam_7; }
	inline SmoothOrbitCam_t3313789169 ** get_address_of_smoothOrbitCam_7() { return &___smoothOrbitCam_7; }
	inline void set_smoothOrbitCam_7(SmoothOrbitCam_t3313789169 * value)
	{
		___smoothOrbitCam_7 = value;
		Il2CppCodeGenWriteBarrier(&___smoothOrbitCam_7, value);
	}

	inline static int32_t get_offset_of_moving_8() { return static_cast<int32_t>(offsetof(SmoothOrbitViewchanger_t2029620261, ___moving_8)); }
	inline bool get_moving_8() const { return ___moving_8; }
	inline bool* get_address_of_moving_8() { return &___moving_8; }
	inline void set_moving_8(bool value)
	{
		___moving_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
