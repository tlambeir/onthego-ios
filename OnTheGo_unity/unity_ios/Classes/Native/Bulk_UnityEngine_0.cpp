﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array3549286319.h"
#include "UnityEngine_U3CModuleU3E692745525.h"
#include "UnityEngine_UnityEngine_AddComponentMenu415040132.h"
#include "mscorlib_System_String1847450689.h"
#include "mscorlib_System_Void1185182177.h"
#include "mscorlib_System_Attribute861562559.h"
#include "mscorlib_System_Int322950945753.h"
#include "UnityEngine_UnityEngine_AdditionalCanvasShaderChan2298426082.h"
#include "UnityEngine_UnityEngine_AI_NavMesh1865600375.h"
#include "UnityEngine_UnityEngine_AI_NavMesh_OnNavMeshPreUpd1580782682.h"
#include "mscorlib_System_Object3080106164.h"
#include "mscorlib_System_IntPtr840150181.h"
#include "mscorlib_System_AsyncCallback3962456242.h"
#include "UnityEngine_UnityEngine_AnimationClip2318505987.h"
#include "UnityEngine_UnityEngine_AnimationCurve3046754366.h"
#include "UnityEngine_UnityEngine_Keyframe4206410242.h"
#include "mscorlib_System_Single1397266774.h"
#include "UnityEngine_UnityEngine_WrapMode730450702.h"
#include "UnityEngine_UnityEngine_AnimationEvent1536042487.h"
#include "UnityEngine_UnityEngine_Object631007953.h"
#include "UnityEngine_UnityEngine_AnimationEventSource3045280095.h"
#include "UnityEngine_UnityEngine_AnimationState1108360407.h"
#include "UnityEngine_UnityEngine_AnimatorStateInfo509032636.h"
#include "UnityEngine_UnityEngine_AnimatorClipInfo3156717155.h"
#include "UnityEngine_UnityEngine_SendMessageOptions3580193095.h"
#include "mscorlib_System_Boolean97287965.h"
#include "UnityEngine_UnityEngine_Animator434523843.h"
#include "UnityEngine_UnityEngine_AnimatorControllerParamete1758260042.h"
#include "UnityEngine_UnityEngine_AnimatorControllerParamete3317225440.h"
#include "UnityEngine_UnityEngine_AnimatorTransitionInfo2534804151.h"
#include "UnityEngine_UnityEngine_Application1852185770.h"
#include "UnityEngine_UnityEngine_Application_LowMemoryCallb4104246196.h"
#include "UnityEngine_UnityEngine_RuntimePlatform4159857903.h"
#include "UnityEngine_UnityEngine_LogType73765434.h"
#include "UnityEngine_UnityEngine_Application_LogCallback3588208630.h"
#include "UnityEngine_UnityEngine_AssemblyIsEditorAssembly3442416807.h"
#include "UnityEngine_UnityEngine_AssetBundle1153907252.h"
#include "mscorlib_System_Type2483944760.h"
#include "UnityEngine_UnityEngine_AssetBundleRequest699759206.h"
#include "mscorlib_System_NullReferenceException1023182353.h"
#include "UnityEngine_UnityEngine_AssetBundleCreateRequest3119663542.h"
#include "UnityEngine_UnityEngine_AsyncOperation1445031843.h"
#include "UnityEngine_UnityEngine_YieldInstruction403091072.h"
#include "UnityEngine_UnityEngine_AttributeHelperEngine2735742303.h"
#include "mscorlib_System_RuntimeTypeHandle3027515415.h"
#include "System_System_Collections_Generic_Stack_1_gen3327334215.h"
#include "mscorlib_System_Reflection_MemberInfo3380001741.h"
#include "mscorlib_System_Collections_Generic_List_1_gen3956019502.h"
#include "UnityEngine_UnityEngine_RequireComponent3490506609.h"
#include "UnityEngine_UnityEngine_DefaultExecutionOrder3059642329.h"
#include "UnityEngine_UnityEngine_DisallowMultipleComponent1422053217.h"
#include "UnityEngine_UnityEngine_ExecuteInEditMode3727731349.h"
#include "UnityEngine_UnityEngine_AudioClip3680889665.h"
#include "UnityEngine_UnityEngine_AudioClip_PCMReaderCallbac1677636661.h"
#include "UnityEngine_UnityEngine_AudioClip_PCMSetPositionCa1059417452.h"
#include "UnityEngine_UnityEngine_AudioSettings3587374600.h"
#include "UnityEngine_UnityEngine_AudioSettings_AudioConfigu2089929874.h"
#include "UnityEngine_UnityEngine_AudioSource3935305588.h"
#include "mscorlib_System_UInt644134040092.h"
#include "UnityEngine_UnityEngine_AudioType3170162477.h"
#include "UnityEngine_UnityEngine_Behaviour1437897464.h"
#include "UnityEngine_UnityEngine_Component1923634451.h"
#include "UnityEngine_UnityEngine_Bounds2266837910.h"
#include "UnityEngine_UnityEngine_Vector33722313464.h"
#include "UnityEngine_UnityEngine_BoxCollider1640800422.h"
#include "UnityEngine_UnityEngine_Camera4157153871.h"
#include "UnityEngine_UnityEngine_RenderingPath883966888.h"
#include "UnityEngine_UnityEngine_Rect2360479859.h"
#include "UnityEngine_UnityEngine_RenderTexture2108887433.h"
#include "UnityEngine_UnityEngine_Matrix4x41817901843.h"
#include "UnityEngine_UnityEngine_CameraClearFlags2362496923.h"
#include "UnityEngine_UnityEngine_Ray3785851493.h"
#include "UnityEngine_UnityEngine_Camera_CameraCallback190067161.h"
#include "UnityEngine_UnityEngine_DepthTextureMode4161834719.h"
#include "UnityEngine_UnityEngine_Rendering_CameraEvent2033959522.h"
#include "UnityEngine_UnityEngine_Rendering_CommandBuffer2206337031.h"
#include "UnityEngine_UnityEngine_GameObject1113636619.h"
#include "UnityEngine_UnityEngine_Canvas3310196443.h"
#include "UnityEngine_UnityEngine_RenderMode4077056833.h"
#include "UnityEngine_UnityEngine_Material340375123.h"
#include "UnityEngine_UnityEngine_Canvas_WillRenderCanvases3309123499.h"
#include "mscorlib_System_Delegate1188392813.h"
#include "UnityEngine_UnityEngine_CanvasGroup4083511760.h"
#include "UnityEngine_UnityEngine_Vector22156229523.h"
#include "UnityEngine_UnityEngine_CanvasRenderer2598313366.h"
#include "UnityEngine_UnityEngine_Color2555686324.h"
#include "UnityEngine_UnityEngine_UIVertex4057497605.h"
#include "UnityEngine_UnityEngine_Color322600501292.h"
#include "UnityEngine_UnityEngine_Vector43319028937.h"
#include "UnityEngine_UnityEngine_MeshTopology838400051.h"
#include "UnityEngine_UnityEngine_Mesh3648964284.h"
#include "mscorlib_System_Collections_Generic_List_1_gen899420910.h"
#include "mscorlib_System_Collections_Generic_List_1_gen4072576034.h"
#include "mscorlib_System_Collections_Generic_List_1_gen3628304265.h"
#include "mscorlib_System_Collections_Generic_List_1_gen496136383.h"
#include "mscorlib_System_Collections_Generic_List_1_gen128053199.h"
#include "UnityEngine_UnityEngine_Texture3661962703.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1234605051.h"
#include "UnityEngine_UnityEngine_CharacterController1138636865.h"
#include "UnityEngine_UnityEngine_CharacterInfo1228754872.h"
#include "UnityEngine_UnityEngine_FontStyle82229486.h"
#include "UnityEngine_UnityEngine_Collections_DeallocateOnJo3131681843.h"
#include "UnityEngine_UnityEngine_Collections_NativeContaine2600515814.h"
#include "UnityEngine_UnityEngine_Collections_NativeContaine3790689680.h"
#include "UnityEngine_UnityEngine_Collections_NativeContaine1586929818.h"
#include "UnityEngine_UnityEngine_Collections_ReadOnlyAttrib2029203740.h"
#include "UnityEngine_UnityEngine_Collections_ReadWriteAttrib306517538.h"
#include "UnityEngine_UnityEngine_Collections_WriteOnlyAttrib595109273.h"
#include "UnityEngine_UnityEngine_Collider1773347010.h"
#include "UnityEngine_UnityEngine_Rigidbody3916780224.h"
#include "UnityEngine_UnityEngine_Collider2D2806799626.h"
#include "UnityEngine_UnityEngine_Rigidbody2D939494601.h"
#include "UnityEngine_UnityEngine_Collision4262080450.h"
#include "UnityEngine_UnityEngine_ContactPoint3758755253.h"
#include "UnityEngine_UnityEngine_Transform3600365921.h"
#include "UnityEngine_UnityEngine_Collision2D2842956331.h"
#include "UnityEngine_UnityEngine_ContactPoint2D3390240644.h"
#include "mscorlib_System_Byte1134296376.h"
#include "UnityEngine_UnityEngine_ColorSpace3453996949.h"
#include "mscorlib_System_Collections_Generic_List_1_gen3395709193.h"
#include "UnityEngine_UnityEngine_ContactFilter2D3805203441.h"
#include "UnityEngine_UnityEngine_LayerMask3493934918.h"
#include "UnityEngine_UnityEngine_ContextMenu1295656858.h"
#include "UnityEngine_UnityEngine_ControllerColliderHit240592346.h"
#include "UnityEngine_UnityEngine_Coroutine3829159415.h"
#include "UnityEngine_UnityEngine_CubemapFace1358225318.h"
#include "UnityEngine_UnityEngine_CullingGroup2096318768.h"
#include "UnityEngine_UnityEngine_CullingGroup_StateChanged2136737110.h"
#include "UnityEngine_UnityEngine_CullingGroupEvent1722745023.h"
#include "UnityEngine_UnityEngine_Cursor1422555833.h"
#include "UnityEngine_UnityEngine_CursorLockMode2840764040.h"
#include "UnityEngine_UnityEngine_CustomYieldInstruction1895667560.h"
#include "UnityEngine_UnityEngine_Debug3317548046.h"
#include "mscorlib_System_Exception1436737249.h"
#include "UnityEngine_UnityEngine_DebugLogHandler826086171.h"
#include "UnityEngine_UnityEngine_Logger274032455.h"
#include "UnityEngine_UnityEngine_Display1387065949.h"
#include "UnityEngine_UnityEngine_Display_DisplaysUpdatedDeleg51287044.h"
#include "UnityEngine_UnityEngine_DrivenRectTransformTracker2562230146.h"
#include "UnityEngine_UnityEngine_RectTransform3704657025.h"
#include "UnityEngine_UnityEngine_DrivenTransformProperties3813433528.h"
#include "UnityEngine_UnityEngine_Event2956885303.h"
#include "UnityEngine_UnityEngine_EventType3528516131.h"
#include "UnityEngine_UnityEngine_KeyCode2599294277.h"
#include "UnityEngine_UnityEngine_EventModifiers2016417398.h"
#include "mscorlib_System_Char3634460470.h"
#include "UnityEngine_UnityEngine_Events_ArgumentCache2187958399.h"
#include "UnityEngine_UnityEngine_Events_BaseInvokableCall2703961024.h"
#include "mscorlib_System_Reflection_MethodInfo1877626248.h"
#include "mscorlib_System_ArgumentNullException1615371798.h"
#include "UnityEngine_UnityEngine_Events_InvokableCall832123510.h"
#include "UnityEngine_UnityEngine_Events_UnityAction3245792599.h"
#include "UnityEngine_UnityEngine_Events_InvokableCallList2498835369.h"
#include "mscorlib_System_Collections_Generic_List_1_gen4176035766.h"
#include "mscorlib_System_Predicate_1_gen3529255148.h"
#include "UnityEngine_UnityEngine_Events_PersistentCall3407714124.h"
#include "UnityEngine_UnityEngine_Events_PersistentListenerMo232255230.h"
#include "UnityEngine_UnityEngine_Events_UnityEventCallState3448586328.h"
#include "UnityEngine_UnityEngine_Events_UnityEventBase3960448221.h"
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall3723462114.h"
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall_982173797.h"
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall4173646029.h"
#include "UnityEngine_UnityEngine_Events_CachedInvokableCall2423483305.h"
#include "mscorlib_System_Reflection_ConstructorInfo5769829.h"
#include "UnityEngine_UnityEngine_Events_PersistentCallGroup3050769227.h"
#include "mscorlib_System_Collections_Generic_List_1_gen584821570.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera2474065447.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera2146457487.h"
#include "UnityEngine_UnityEngine_Events_UnityEvent2581268647.h"
#include "mscorlib_System_Reflection_BindingFlags2721792723.h"
#include "mscorlib_System_Reflection_Binder2999457153.h"
#include "mscorlib_System_Reflection_ParameterInfo1861056598.h"
#include "mscorlib_System_Reflection_ParameterModifier1461694466.h"
#include "mscorlib_System_Reflection_MethodBase609368412.h"
#include "UnityEngine_UnityEngine_ExitGUIException133215258.h"
#include "UnityEngine_UnityEngine_Experimental_Director_Anim2591841208.h"
#include "UnityEngine_UnityEngine_Experimental_Director_Anim2436992687.h"
#include "UnityEngine_UnityEngine_Experimental_Director_Playa882318307.h"
#include "UnityEngine_UnityEngine_Experimental_Director_Play3858037524.h"
#include "UnityEngine_UnityEngine_Experimental_Director_Anim1193230317.h"
#include "UnityEngine_UnityEngine_Experimental_Director_Anim2837525843.h"

// UnityEngine.AddComponentMenu
struct AddComponentMenu_t415040132;
// System.String
struct String_t;
// System.Attribute
struct Attribute_t861562559;
// UnityEngine.AI.NavMesh/OnNavMeshPreUpdate
struct OnNavMeshPreUpdate_t1580782682;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t767004451;
// System.AsyncCallback
struct AsyncCallback_t3962456242;
// UnityEngine.AnimationCurve
struct AnimationCurve_t3046754366;
// UnityEngine.Keyframe[]
struct KeyframeU5BU5D_t1068524471;
// UnityEngine.Object
struct Object_t631007953;
// UnityEngine.AnimationEvent
struct AnimationEvent_t1536042487;
// UnityEngine.AnimationState
struct AnimationState_t1108360407;
// UnityEngine.Animator
struct Animator_t434523843;
// UnityEngine.AnimatorControllerParameter
struct AnimatorControllerParameter_t1758260042;
// UnityEngine.Application/LowMemoryCallback
struct LowMemoryCallback_t4104246196;
// UnityEngine.Application/LogCallback
struct LogCallback_t3588208630;
// UnityEngine.AssemblyIsEditorAssembly
struct AssemblyIsEditorAssembly_t3442416807;
// UnityEngine.AssetBundle
struct AssetBundle_t1153907252;
// UnityEngine.Object[]
struct ObjectU5BU5D_t1417781964;
// System.Type
struct Type_t;
// UnityEngine.AssetBundleRequest
struct AssetBundleRequest_t699759206;
// System.NullReferenceException
struct NullReferenceException_t1023182353;
// UnityEngine.AssetBundleCreateRequest
struct AssetBundleCreateRequest_t3119663542;
// UnityEngine.AsyncOperation
struct AsyncOperation_t1445031843;
// UnityEngine.YieldInstruction
struct YieldInstruction_t403091072;
// System.Collections.Generic.Stack`1<System.Type>
struct Stack_1_t3327334215;
// System.Collections.Generic.Stack`1<System.Object>
struct Stack_1_t3923495619;
// System.Type[]
struct TypeU5BU5D_t3940880105;
// System.Collections.Generic.List`1<System.Type>
struct List_1_t3956019502;
// System.Collections.Generic.List`1<System.Object>
struct List_1_t257213610;
// System.Object[]
struct ObjectU5BU5D_t2843939325;
// UnityEngine.RequireComponent
struct RequireComponent_t3490506609;
// UnityEngine.DefaultExecutionOrder
struct DefaultExecutionOrder_t3059642329;
// UnityEngine.DisallowMultipleComponent
struct DisallowMultipleComponent_t1422053217;
// UnityEngine.ExecuteInEditMode
struct ExecuteInEditMode_t3727731349;
// UnityEngine.AudioClip
struct AudioClip_t3680889665;
// System.Single[]
struct SingleU5BU5D_t1444911251;
// UnityEngine.AudioClip/PCMReaderCallback
struct PCMReaderCallback_t1677636661;
// UnityEngine.AudioClip/PCMSetPositionCallback
struct PCMSetPositionCallback_t1059417452;
// UnityEngine.AudioSettings/AudioConfigurationChangeHandler
struct AudioConfigurationChangeHandler_t2089929874;
// UnityEngine.AudioSource
struct AudioSource_t3935305588;
// UnityEngine.Behaviour
struct Behaviour_t1437897464;
// UnityEngine.Component
struct Component_t1923634451;
// UnityEngine.BoxCollider
struct BoxCollider_t1640800422;
// UnityEngine.Camera
struct Camera_t4157153871;
// UnityEngine.RenderTexture
struct RenderTexture_t2108887433;
// UnityEngine.Camera[]
struct CameraU5BU5D_t1709987734;
// UnityEngine.Camera/CameraCallback
struct CameraCallback_t190067161;
// UnityEngine.Rendering.CommandBuffer
struct CommandBuffer_t2206337031;
// UnityEngine.GameObject
struct GameObject_t1113636619;
// UnityEngine.Canvas
struct Canvas_t3310196443;
// UnityEngine.Material
struct Material_t340375123;
// UnityEngine.Canvas/WillRenderCanvases
struct WillRenderCanvases_t3309123499;
// System.Delegate
struct Delegate_t1188392813;
// UnityEngine.CanvasGroup
struct CanvasGroup_t4083511760;
// UnityEngine.CanvasRenderer
struct CanvasRenderer_t2598313366;
// UnityEngine.UIVertex[]
struct UIVertexU5BU5D_t1981460040;
// UnityEngine.Mesh
struct Mesh_t3648964284;
// System.Collections.Generic.List`1<UnityEngine.Vector3>
struct List_1_t899420910;
// System.Collections.Generic.List`1<UnityEngine.Color32>
struct List_1_t4072576034;
// System.Collections.Generic.List`1<UnityEngine.Vector2>
struct List_1_t3628304265;
// System.Collections.Generic.List`1<UnityEngine.Vector4>
struct List_1_t496136383;
// System.Collections.Generic.List`1<System.Int32>
struct List_1_t128053199;
// System.Int32[]
struct Int32U5BU5D_t385246372;
// UnityEngine.Texture
struct Texture_t3661962703;
// System.Collections.Generic.List`1<UnityEngine.UIVertex>
struct List_1_t1234605051;
// UnityEngine.Collections.DeallocateOnJobCompletionAttribute
struct DeallocateOnJobCompletionAttribute_t3131681843;
// UnityEngine.Collections.NativeContainerAttribute
struct NativeContainerAttribute_t2600515814;
// UnityEngine.Collections.NativeContainerSupportsAtomicWriteAttribute
struct NativeContainerSupportsAtomicWriteAttribute_t3790689680;
// UnityEngine.Collections.NativeContainerSupportsMinMaxWriteRestrictionAttribute
struct NativeContainerSupportsMinMaxWriteRestrictionAttribute_t1586929818;
// UnityEngine.Collections.ReadOnlyAttribute
struct ReadOnlyAttribute_t2029203740;
// UnityEngine.Collections.ReadWriteAttribute
struct ReadWriteAttribute_t306517538;
// UnityEngine.Collections.WriteOnlyAttribute
struct WriteOnlyAttribute_t595109273;
// UnityEngine.Collider
struct Collider_t1773347010;
// UnityEngine.Rigidbody
struct Rigidbody_t3916780224;
// UnityEngine.Collider2D
struct Collider2D_t2806799626;
// UnityEngine.Rigidbody2D
struct Rigidbody2D_t939494601;
// UnityEngine.Collision
struct Collision_t4262080450;
// UnityEngine.Transform
struct Transform_t3600365921;
// UnityEngine.ContactPoint[]
struct ContactPointU5BU5D_t872956888;
// System.Collections.IEnumerator
struct IEnumerator_t1853284238;
// System.Array
struct Il2CppArray;
// UnityEngine.Collision2D
struct Collision2D_t2842956331;
// UnityEngine.ContactPoint2D[]
struct ContactPoint2DU5BU5D_t96683501;
// UnityEngine.Component[]
struct ComponentU5BU5D_t3294940482;
// System.Collections.Generic.List`1<UnityEngine.Component>
struct List_1_t3395709193;
// UnityEngine.ContextMenu
struct ContextMenu_t1295656858;
// UnityEngine.ControllerColliderHit
struct ControllerColliderHit_t240592346;
// UnityEngine.CharacterController
struct CharacterController_t1138636865;
// UnityEngine.Coroutine
struct Coroutine_t3829159415;
// UnityEngine.CullingGroup
struct CullingGroup_t2096318768;
// UnityEngine.CullingGroup/StateChanged
struct StateChanged_t2136737110;
// UnityEngine.CustomYieldInstruction
struct CustomYieldInstruction_t1895667560;
// UnityEngine.ILogger
struct ILogger_t2607134790;
// System.Exception
struct Exception_t1436737249;
// UnityEngine.DebugLogHandler
struct DebugLogHandler_t826086171;
// UnityEngine.Logger
struct Logger_t274032455;
// UnityEngine.ILogHandler
struct ILogHandler_t2464711877;
// UnityEngine.Display
struct Display_t1387065949;
// System.IntPtr[]
struct IntPtrU5BU5D_t4013366056;
// UnityEngine.Display/DisplaysUpdatedDelegate
struct DisplaysUpdatedDelegate_t51287044;
// UnityEngine.RectTransform
struct RectTransform_t3704657025;
// UnityEngine.Event
struct Event_t2956885303;
// UnityEngine.Events.ArgumentCache
struct ArgumentCache_t2187958399;
// UnityEngine.Events.BaseInvokableCall
struct BaseInvokableCall_t2703961024;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.ArgumentNullException
struct ArgumentNullException_t1615371798;
// UnityEngine.Events.InvokableCall
struct InvokableCall_t832123510;
// UnityEngine.Events.UnityAction
struct UnityAction_t3245792599;
// UnityEngine.Events.InvokableCallList
struct InvokableCallList_t2498835369;
// System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall>
struct List_1_t4176035766;
// System.Predicate`1<UnityEngine.Events.BaseInvokableCall>
struct Predicate_1_t3529255148;
// System.Predicate`1<System.Object>
struct Predicate_1_t3905400288;
// System.Collections.Generic.IEnumerable`1<UnityEngine.Events.BaseInvokableCall>
struct IEnumerable_1_t1683813913;
// System.Collections.Generic.IEnumerable`1<System.Object>
struct IEnumerable_1_t2059959053;
// UnityEngine.Events.PersistentCall
struct PersistentCall_t3407714124;
// UnityEngine.Events.UnityEventBase
struct UnityEventBase_t3960448221;
// UnityEngine.Events.CachedInvokableCall`1<System.Single>
struct CachedInvokableCall_1_t3723462114;
// UnityEngine.Events.CachedInvokableCall`1<System.Int32>
struct CachedInvokableCall_1_t982173797;
// UnityEngine.Events.CachedInvokableCall`1<System.String>
struct CachedInvokableCall_1_t4173646029;
// UnityEngine.Events.CachedInvokableCall`1<System.Object>
struct CachedInvokableCall_1_t1111334208;
// UnityEngine.Events.CachedInvokableCall`1<System.Boolean>
struct CachedInvokableCall_1_t2423483305;
// System.Reflection.ConstructorInfo
struct ConstructorInfo_t5769829;
// UnityEngine.Events.PersistentCallGroup
struct PersistentCallGroup_t3050769227;
// System.Collections.Generic.List`1<UnityEngine.Events.PersistentCall>
struct List_1_t584821570;
// UnityEngine.Events.UnityEvent
struct UnityEvent_t2581268647;
// System.Reflection.Binder
struct Binder_t2999457153;
// System.Reflection.ParameterModifier[]
struct ParameterModifierU5BU5D_t2943407543;
// System.Reflection.ParameterInfo
struct ParameterInfo_t1861056598;
// UnityEngine.Experimental.Director.AnimationClipPlayable
struct AnimationClipPlayable_t2591841208;
// UnityEngine.Experimental.Director.AnimationPlayable
struct AnimationPlayable_t2436992687;
// UnityEngine.AnimationClip
struct AnimationClip_t2318505987;
// UnityEngine.Experimental.Director.AnimationLayerMixerPlayable
struct AnimationLayerMixerPlayable_t1193230317;
// UnityEngine.Experimental.Director.AnimationMixerPlayable
struct AnimationMixerPlayable_t2837525843;
extern Il2CppClass* NavMesh_t1865600375_il2cpp_TypeInfo_var;
extern const uint32_t NavMesh_Internal_CallOnNavMeshPreUpdate_m3207184993_MetadataUsageId;
struct Object_t631007953_marshaled_pinvoke;
struct Object_t631007953;;
struct Object_t631007953_marshaled_pinvoke;;
struct Object_t631007953_marshaled_com;
struct Object_t631007953_marshaled_com;;
extern Il2CppCodeGenString* _stringLiteral757602046;
extern const uint32_t AnimationEvent__ctor_m989348042_MetadataUsageId;
extern Il2CppClass* Debug_t3317548046_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1009479862;
extern const uint32_t AnimationEvent_get_animationState_m2229874910_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral3962017886;
extern const uint32_t AnimationEvent_get_animatorStateInfo_m3909257444_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral3070799046;
extern const uint32_t AnimationEvent_get_animatorClipInfo_m2784760084_MetadataUsageId;
extern Il2CppClass* AnimatorControllerParameter_t1758260042_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t AnimatorControllerParameter_Equals_m1144065427_MetadataUsageId;
extern Il2CppClass* Application_t1852185770_il2cpp_TypeInfo_var;
extern const uint32_t Application_CallLowMemory_m2493184849_MetadataUsageId;
extern const uint32_t Application_CallLogCallback_m3663471574_MetadataUsageId;
extern Il2CppClass* LogType_t73765434_il2cpp_TypeInfo_var;
extern const uint32_t LogCallback_BeginInvoke_m3624267429_MetadataUsageId;
extern Il2CppClass* NullReferenceException_t1023182353_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1402148072;
extern const uint32_t AssetBundle_LoadAllAssets_m3416039552_MetadataUsageId;
extern const uint32_t AssetBundle_LoadAllAssetsAsync_m609312961_MetadataUsageId;
extern const Il2CppType* MonoBehaviour_t3962482529_0_0_0_var;
extern const Il2CppType* DisallowMultipleComponent_t1422053217_0_0_0_var;
extern Il2CppClass* Stack_1_t3327334215_il2cpp_TypeInfo_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const MethodInfo* Stack_1__ctor_m926334451_MethodInfo_var;
extern const MethodInfo* Stack_1_Push_m4005337547_MethodInfo_var;
extern const MethodInfo* Stack_1_Pop_m4032335803_MethodInfo_var;
extern const MethodInfo* Stack_1_get_Count_m95885128_MethodInfo_var;
extern const uint32_t AttributeHelperEngine_GetParentTypeDisallowingMultipleInclusion_m3140293344_MetadataUsageId;
extern const Il2CppType* RequireComponent_t3490506609_0_0_0_var;
extern Il2CppClass* RequireComponentU5BU5D_t2245623724_il2cpp_TypeInfo_var;
extern Il2CppClass* TypeU5BU5D_t3940880105_il2cpp_TypeInfo_var;
extern Il2CppClass* List_1_t3956019502_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m696099607_MethodInfo_var;
extern const MethodInfo* List_1_Add_m209710667_MethodInfo_var;
extern const MethodInfo* List_1_ToArray_m1314877377_MethodInfo_var;
extern const uint32_t AttributeHelperEngine_GetRequiredComponents_m3351952333_MetadataUsageId;
extern const Il2CppType* ExecuteInEditMode_t3727731349_0_0_0_var;
extern const uint32_t AttributeHelperEngine_CheckIsEditorScript_m2101700955_MetadataUsageId;
extern Il2CppClass* AttributeHelperEngine_t2735742303_il2cpp_TypeInfo_var;
extern const MethodInfo* AttributeHelperEngine_GetCustomAttributeOfType_TisDefaultExecutionOrder_t3059642329_m1616528635_MethodInfo_var;
extern const uint32_t AttributeHelperEngine_GetDefaultExecutionOrderFor_m118922855_MetadataUsageId;
extern Il2CppClass* DisallowMultipleComponentU5BU5D_t3936143868_il2cpp_TypeInfo_var;
extern Il2CppClass* ExecuteInEditModeU5BU5D_t3239458680_il2cpp_TypeInfo_var;
extern const uint32_t AttributeHelperEngine__cctor_m2980610902_MetadataUsageId;
extern Il2CppClass* Int32_t2950945753_il2cpp_TypeInfo_var;
extern const uint32_t PCMSetPositionCallback_BeginInvoke_m2733210754_MetadataUsageId;
extern Il2CppClass* AudioSettings_t3587374600_il2cpp_TypeInfo_var;
extern const uint32_t AudioSettings_InvokeOnAudioConfigurationChanged_m2341783670_MetadataUsageId;
extern Il2CppClass* Boolean_t97287965_il2cpp_TypeInfo_var;
extern const uint32_t AudioConfigurationChangeHandler_BeginInvoke_m2001196844_MetadataUsageId;
extern Il2CppClass* Bounds_t2266837910_il2cpp_TypeInfo_var;
extern Il2CppClass* Vector3_t3722313464_il2cpp_TypeInfo_var;
extern const uint32_t Bounds_Equals_m1462810812_MetadataUsageId;
extern Il2CppClass* ObjectU5BU5D_t2843939325_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral940982150;
extern const uint32_t Bounds_ToString_m72361594_MetadataUsageId;
extern Il2CppClass* Camera_t4157153871_il2cpp_TypeInfo_var;
extern const uint32_t Camera_FireOnPreCull_m2863664483_MetadataUsageId;
extern const uint32_t Camera_FireOnPreRender_m475210052_MetadataUsageId;
extern const uint32_t Camera_FireOnPostRender_m1633769721_MetadataUsageId;
extern Il2CppClass* Canvas_t3310196443_il2cpp_TypeInfo_var;
extern Il2CppClass* WillRenderCanvases_t3309123499_il2cpp_TypeInfo_var;
extern const uint32_t Canvas_add_willRenderCanvases_m334285881_MetadataUsageId;
extern const uint32_t Canvas_remove_willRenderCanvases_m2711004648_MetadataUsageId;
extern const uint32_t Canvas_SendWillRenderCanvases_m2809604487_MetadataUsageId;
extern Il2CppClass* Mesh_t3648964284_il2cpp_TypeInfo_var;
extern Il2CppClass* List_1_t899420910_il2cpp_TypeInfo_var;
extern Il2CppClass* List_1_t4072576034_il2cpp_TypeInfo_var;
extern Il2CppClass* List_1_t3628304265_il2cpp_TypeInfo_var;
extern Il2CppClass* List_1_t496136383_il2cpp_TypeInfo_var;
extern Il2CppClass* List_1_t128053199_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t631007953_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m4218989665_MethodInfo_var;
extern const MethodInfo* List_1__ctor_m2033536304_MethodInfo_var;
extern const MethodInfo* List_1__ctor_m4218988514_MethodInfo_var;
extern const MethodInfo* List_1__ctor_m4218986464_MethodInfo_var;
extern const MethodInfo* List_1__ctor_m4278196146_MethodInfo_var;
extern const MethodInfo* List_1_Add_m4259514621_MethodInfo_var;
extern const MethodInfo* List_1_Add_m3372602044_MethodInfo_var;
extern const MethodInfo* List_1_Add_m4259515774_MethodInfo_var;
extern const MethodInfo* List_1_Add_m4259509372_MethodInfo_var;
extern const MethodInfo* List_1_Add_m4025721294_MethodInfo_var;
extern const MethodInfo* List_1_ToArray_m789548171_MethodInfo_var;
extern const uint32_t CanvasRenderer_SetVertices_m229633174_MetadataUsageId;
extern const uint32_t Collision_get_transform_m1665182495_MetadataUsageId;
extern const uint32_t Collision_get_gameObject_m1670699900_MetadataUsageId;
extern const uint32_t Collision_get_other_m1003040551_MetadataUsageId;
extern Il2CppClass* Physics2D_t1528932956_il2cpp_TypeInfo_var;
extern const uint32_t Collision2D_get_collider_m3539891938_MetadataUsageId;
extern const uint32_t Collision2D_get_otherCollider_m3332082749_MetadataUsageId;
extern const uint32_t Collision2D_get_rigidbody_m3300210124_MetadataUsageId;
extern const uint32_t Collision2D_get_otherRigidbody_m3881049350_MetadataUsageId;
extern const uint32_t Collision2D_get_transform_m1797657422_MetadataUsageId;
extern const uint32_t Collision2D_get_gameObject_m73511728_MetadataUsageId;
extern Il2CppClass* Single_t1397266774_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1121701886;
extern const uint32_t Color_ToString_m3373877040_MetadataUsageId;
extern Il2CppClass* Color_t2555686324_il2cpp_TypeInfo_var;
extern const uint32_t Color_Equals_m325780748_MetadataUsageId;
extern Il2CppClass* Mathf_t3464937446_il2cpp_TypeInfo_var;
extern const uint32_t Color_Lerp_m3788050327_MetadataUsageId;
extern const uint32_t Color32_op_Implicit_m2622936441_MetadataUsageId;
extern Il2CppClass* Byte_t1134296376_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3218509134;
extern const uint32_t Color32_ToString_m908866355_MetadataUsageId;
extern const uint32_t Component__ctor_m3533368306_MetadataUsageId;
extern const uint32_t ContactFilter2D_CheckConsistency_m1930104761_MetadataUsageId;
extern Il2CppClass* ContactFilter2D_t3805203441_il2cpp_TypeInfo_var;
extern const uint32_t ContactFilter2D_CreateLegacyFilter_m949420789_MetadataUsageId;
extern Il2CppClass* StateChanged_t2136737110_il2cpp_TypeInfo_var;
extern const uint32_t CullingGroup_t2096318768_pinvoke_FromNativeMethodDefinition_MetadataUsageId;
extern const uint32_t CullingGroup_t2096318768_com_FromNativeMethodDefinition_MetadataUsageId;
extern Il2CppClass* IntPtr_t_il2cpp_TypeInfo_var;
extern const uint32_t CullingGroup_Finalize_m3771988052_MetadataUsageId;
extern Il2CppClass* CullingGroupEvent_t1722745023_il2cpp_TypeInfo_var;
extern const uint32_t CullingGroup_SendEvents_m435733983_MetadataUsageId;
extern const uint32_t StateChanged_BeginInvoke_m1110082402_MetadataUsageId;
extern const uint32_t Debug_get_logger_m1300688786_MetadataUsageId;
extern Il2CppClass* ILogger_t2607134790_il2cpp_TypeInfo_var;
extern const uint32_t Debug_Log_m1780991845_MetadataUsageId;
extern const uint32_t Debug_LogError_m2059623341_MetadataUsageId;
extern const uint32_t Debug_LogError_m3356949141_MetadataUsageId;
extern Il2CppClass* ILogHandler_t2464711877_il2cpp_TypeInfo_var;
extern const uint32_t Debug_LogErrorFormat_m4099340697_MetadataUsageId;
extern const uint32_t Debug_LogException_m3660909244_MetadataUsageId;
extern const uint32_t Debug_LogException_m2155052615_MetadataUsageId;
extern const uint32_t Debug_LogWarning_m3661709751_MetadataUsageId;
extern const uint32_t Debug_LogWarning_m218105588_MetadataUsageId;
extern const uint32_t Debug_LogWarningFormat_m3804720959_MetadataUsageId;
extern Il2CppClass* DebugLogHandler_t826086171_il2cpp_TypeInfo_var;
extern Il2CppClass* Logger_t274032455_il2cpp_TypeInfo_var;
extern const uint32_t Debug__cctor_m343362545_MetadataUsageId;
extern const uint32_t DebugLogHandler_LogFormat_m1163074747_MetadataUsageId;
extern Il2CppClass* Display_t1387065949_il2cpp_TypeInfo_var;
extern const uint32_t Display_get_renderingWidth_m2897119351_MetadataUsageId;
extern const uint32_t Display_get_renderingHeight_m4183585850_MetadataUsageId;
extern const uint32_t Display_get_systemWidth_m2297141419_MetadataUsageId;
extern const uint32_t Display_get_systemHeight_m2647631588_MetadataUsageId;
extern const uint32_t Display_RelativeMouseAt_m3342735020_MetadataUsageId;
extern Il2CppClass* DisplayU5BU5D_t103034768_il2cpp_TypeInfo_var;
extern const uint32_t Display_RecreateDisplayList_m48792512_MetadataUsageId;
extern const uint32_t Display_FireDisplaysUpdated_m1504381846_MetadataUsageId;
extern const uint32_t Display__cctor_m625480064_MetadataUsageId;
extern Il2CppClass* Event_t2956885303_il2cpp_TypeInfo_var;
extern const uint32_t Event_get_current_m357379773_MetadataUsageId;
extern const uint32_t Event_Internal_MakeMasterEventCurrent_m1629006028_MetadataUsageId;
extern const uint32_t Event_Equals_m1159919013_MetadataUsageId;
extern Il2CppClass* EventType_t3528516131_il2cpp_TypeInfo_var;
extern Il2CppClass* EventModifiers_t2016417398_il2cpp_TypeInfo_var;
extern Il2CppClass* KeyCode_t2599294277_il2cpp_TypeInfo_var;
extern Il2CppClass* Vector2_t2156229523_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2886375297;
extern Il2CppCodeGenString* _stringLiteral2737274358;
extern Il2CppCodeGenString* _stringLiteral1262317653;
extern Il2CppCodeGenString* _stringLiteral4223655990;
extern Il2CppCodeGenString* _stringLiteral1420157070;
extern Il2CppCodeGenString* _stringLiteral4199752235;
extern Il2CppCodeGenString* _stringLiteral495249995;
extern const uint32_t Event_ToString_m961638257_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral155902455;
extern Il2CppCodeGenString* _stringLiteral1973799399;
extern Il2CppCodeGenString* _stringLiteral1736874801;
extern const uint32_t ArgumentCache_TidyAssemblyTypeName_m1203759352_MetadataUsageId;
extern Il2CppClass* ArgumentNullException_t1615371798_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2833503317;
extern Il2CppCodeGenString* _stringLiteral3941509395;
extern const uint32_t BaseInvokableCall__ctor_m1036035291_MetadataUsageId;
extern const uint32_t BaseInvokableCall_AllowInvoke_m1250801794_MetadataUsageId;
extern const Il2CppType* UnityAction_t3245792599_0_0_0_var;
extern Il2CppClass* UnityAction_t3245792599_il2cpp_TypeInfo_var;
extern const uint32_t InvokableCall__ctor_m2864671469_MetadataUsageId;
extern const uint32_t InvokableCall_add_Delegate_m2530991106_MetadataUsageId;
extern const uint32_t InvokableCall_remove_Delegate_m1019375674_MetadataUsageId;
extern Il2CppClass* List_1_t4176035766_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m3206896509_MethodInfo_var;
extern const uint32_t InvokableCallList__ctor_m2867488712_MetadataUsageId;
extern const MethodInfo* List_1_Add_m19707193_MethodInfo_var;
extern const uint32_t InvokableCallList_AddPersistentInvokableCall_m4020414894_MetadataUsageId;
extern const uint32_t InvokableCallList_AddListener_m4292769145_MetadataUsageId;
extern Il2CppClass* Predicate_1_t3529255148_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_get_Item_m2469781675_MethodInfo_var;
extern const MethodInfo* List_1_get_Count_m743730018_MethodInfo_var;
extern const MethodInfo* List_1_Contains_m152128373_MethodInfo_var;
extern const MethodInfo* Predicate_1__ctor_m3247287534_MethodInfo_var;
extern const MethodInfo* List_1_RemoveAll_m1762023412_MethodInfo_var;
extern const uint32_t InvokableCallList_RemoveListener_m1974000505_MetadataUsageId;
extern const MethodInfo* List_1_Clear_m3668089392_MethodInfo_var;
extern const uint32_t InvokableCallList_ClearPersistent_m3446223403_MetadataUsageId;
extern const MethodInfo* List_1_AddRange_m976883154_MethodInfo_var;
extern const uint32_t InvokableCallList_Invoke_m2795703874_MetadataUsageId;
extern Il2CppClass* ArgumentCache_t2187958399_il2cpp_TypeInfo_var;
extern const uint32_t PersistentCall__ctor_m1104049729_MetadataUsageId;
extern const uint32_t PersistentCall_IsValid_m3557513049_MetadataUsageId;
extern Il2CppClass* CachedInvokableCall_1_t3723462114_il2cpp_TypeInfo_var;
extern Il2CppClass* CachedInvokableCall_1_t982173797_il2cpp_TypeInfo_var;
extern Il2CppClass* CachedInvokableCall_1_t4173646029_il2cpp_TypeInfo_var;
extern Il2CppClass* CachedInvokableCall_1_t2423483305_il2cpp_TypeInfo_var;
extern Il2CppClass* InvokableCall_t832123510_il2cpp_TypeInfo_var;
extern const MethodInfo* CachedInvokableCall_1__ctor_m2985117283_MethodInfo_var;
extern const MethodInfo* CachedInvokableCall_1__ctor_m3353156630_MethodInfo_var;
extern const MethodInfo* CachedInvokableCall_1__ctor_m2815878172_MethodInfo_var;
extern const MethodInfo* CachedInvokableCall_1__ctor_m3594541075_MethodInfo_var;
extern const uint32_t PersistentCall_GetRuntimeCall_m606139440_MetadataUsageId;
extern const Il2CppType* Object_t631007953_0_0_0_var;
extern const Il2CppType* CachedInvokableCall_1_t3153979999_0_0_0_var;
extern const Il2CppType* MethodInfo_t_0_0_0_var;
extern Il2CppClass* BaseInvokableCall_t2703961024_il2cpp_TypeInfo_var;
extern const uint32_t PersistentCall_GetObjectCall_m861446582_MetadataUsageId;
extern Il2CppClass* List_1_t584821570_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m943763978_MethodInfo_var;
extern const uint32_t PersistentCallGroup__ctor_m1755284370_MetadataUsageId;
extern const MethodInfo* List_1_GetEnumerator_m398210679_MethodInfo_var;
extern const MethodInfo* Enumerator_get_Current_m481949260_MethodInfo_var;
extern const MethodInfo* Enumerator_MoveNext_m1429503053_MethodInfo_var;
extern const MethodInfo* Enumerator_Dispose_m2540888223_MethodInfo_var;
extern const uint32_t PersistentCallGroup_Initialize_m2614950721_MetadataUsageId;
extern const uint32_t UnityEvent__ctor_m974839563_MetadataUsageId;
extern const uint32_t UnityEvent_FindMethod_Impl_m3520635000_MetadataUsageId;
extern const uint32_t UnityEvent_GetDelegate_m3509379365_MetadataUsageId;
extern const uint32_t UnityEvent_GetDelegate_m3910545044_MetadataUsageId;
extern Il2CppClass* InvokableCallList_t2498835369_il2cpp_TypeInfo_var;
extern Il2CppClass* PersistentCallGroup_t3050769227_il2cpp_TypeInfo_var;
extern const uint32_t UnityEventBase__ctor_m1087366165_MetadataUsageId;
extern const uint32_t UnityEventBase_FindMethod_m1055565284_MetadataUsageId;
extern const Il2CppType* Single_t1397266774_0_0_0_var;
extern const Il2CppType* Int32_t2950945753_0_0_0_var;
extern const Il2CppType* Boolean_t97287965_0_0_0_var;
extern const Il2CppType* String_t_0_0_0_var;
extern const uint32_t UnityEventBase_FindMethod_m995544650_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral3452614528;
extern const uint32_t UnityEventBase_ToString_m3800562853_MetadataUsageId;
extern const Il2CppType* Il2CppObject_0_0_0_var;
extern const uint32_t UnityEventBase_GetValidMethodInfo_m1761163145_MetadataUsageId;

// UnityEngine.Keyframe[]
struct KeyframeU5BU5D_t1068524471  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Keyframe_t4206410242  m_Items[1];

public:
	inline Keyframe_t4206410242  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Keyframe_t4206410242 * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Keyframe_t4206410242  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline Keyframe_t4206410242  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Keyframe_t4206410242 * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Keyframe_t4206410242  value)
	{
		m_Items[index] = value;
	}
};
// UnityEngine.Object[]
struct ObjectU5BU5D_t1417781964  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Object_t631007953 * m_Items[1];

public:
	inline Object_t631007953 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Object_t631007953 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Object_t631007953 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline Object_t631007953 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Object_t631007953 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Object_t631007953 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Object[]
struct ObjectU5BU5D_t2843939325  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Il2CppObject * m_Items[1];

public:
	inline Il2CppObject * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Il2CppObject ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Il2CppObject * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline Il2CppObject * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Il2CppObject ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Il2CppObject * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Type[]
struct TypeU5BU5D_t3940880105  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Type_t * m_Items[1];

public:
	inline Type_t * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Type_t ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Type_t * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline Type_t * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Type_t ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Type_t * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UnityEngine.RequireComponent[]
struct RequireComponentU5BU5D_t2245623724  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) RequireComponent_t3490506609 * m_Items[1];

public:
	inline RequireComponent_t3490506609 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline RequireComponent_t3490506609 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, RequireComponent_t3490506609 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline RequireComponent_t3490506609 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline RequireComponent_t3490506609 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, RequireComponent_t3490506609 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UnityEngine.DisallowMultipleComponent[]
struct DisallowMultipleComponentU5BU5D_t3936143868  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) DisallowMultipleComponent_t1422053217 * m_Items[1];

public:
	inline DisallowMultipleComponent_t1422053217 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline DisallowMultipleComponent_t1422053217 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, DisallowMultipleComponent_t1422053217 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline DisallowMultipleComponent_t1422053217 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline DisallowMultipleComponent_t1422053217 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, DisallowMultipleComponent_t1422053217 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UnityEngine.ExecuteInEditMode[]
struct ExecuteInEditModeU5BU5D_t3239458680  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) ExecuteInEditMode_t3727731349 * m_Items[1];

public:
	inline ExecuteInEditMode_t3727731349 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline ExecuteInEditMode_t3727731349 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, ExecuteInEditMode_t3727731349 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline ExecuteInEditMode_t3727731349 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline ExecuteInEditMode_t3727731349 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, ExecuteInEditMode_t3727731349 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Single[]
struct SingleU5BU5D_t1444911251  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) float m_Items[1];

public:
	inline float GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline float* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, float value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline float GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline float* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, float value)
	{
		m_Items[index] = value;
	}
};
// UnityEngine.Camera[]
struct CameraU5BU5D_t1709987734  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Camera_t4157153871 * m_Items[1];

public:
	inline Camera_t4157153871 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Camera_t4157153871 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Camera_t4157153871 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline Camera_t4157153871 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Camera_t4157153871 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Camera_t4157153871 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UnityEngine.UIVertex[]
struct UIVertexU5BU5D_t1981460040  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) UIVertex_t4057497605  m_Items[1];

public:
	inline UIVertex_t4057497605  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline UIVertex_t4057497605 * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, UIVertex_t4057497605  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline UIVertex_t4057497605  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline UIVertex_t4057497605 * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, UIVertex_t4057497605  value)
	{
		m_Items[index] = value;
	}
};
// System.Int32[]
struct Int32U5BU5D_t385246372  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) int32_t m_Items[1];

public:
	inline int32_t GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline int32_t* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, int32_t value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline int32_t GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline int32_t* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, int32_t value)
	{
		m_Items[index] = value;
	}
};
// UnityEngine.ContactPoint[]
struct ContactPointU5BU5D_t872956888  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) ContactPoint_t3758755253  m_Items[1];

public:
	inline ContactPoint_t3758755253  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline ContactPoint_t3758755253 * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, ContactPoint_t3758755253  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline ContactPoint_t3758755253  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline ContactPoint_t3758755253 * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, ContactPoint_t3758755253  value)
	{
		m_Items[index] = value;
	}
};
// UnityEngine.ContactPoint2D[]
struct ContactPoint2DU5BU5D_t96683501  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) ContactPoint2D_t3390240644  m_Items[1];

public:
	inline ContactPoint2D_t3390240644  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline ContactPoint2D_t3390240644 * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, ContactPoint2D_t3390240644  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline ContactPoint2D_t3390240644  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline ContactPoint2D_t3390240644 * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, ContactPoint2D_t3390240644  value)
	{
		m_Items[index] = value;
	}
};
// UnityEngine.Component[]
struct ComponentU5BU5D_t3294940482  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Component_t1923634451 * m_Items[1];

public:
	inline Component_t1923634451 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Component_t1923634451 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Component_t1923634451 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline Component_t1923634451 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Component_t1923634451 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Component_t1923634451 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.IntPtr[]
struct IntPtrU5BU5D_t4013366056  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) IntPtr_t m_Items[1];

public:
	inline IntPtr_t GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline IntPtr_t* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, IntPtr_t value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline IntPtr_t GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline IntPtr_t* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, IntPtr_t value)
	{
		m_Items[index] = value;
	}
};
// UnityEngine.Display[]
struct DisplayU5BU5D_t103034768  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Display_t1387065949 * m_Items[1];

public:
	inline Display_t1387065949 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Display_t1387065949 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Display_t1387065949 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline Display_t1387065949 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Display_t1387065949 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Display_t1387065949 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Reflection.ParameterInfo[]
struct ParameterInfoU5BU5D_t390618515  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) ParameterInfo_t1861056598 * m_Items[1];

public:
	inline ParameterInfo_t1861056598 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline ParameterInfo_t1861056598 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, ParameterInfo_t1861056598 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline ParameterInfo_t1861056598 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline ParameterInfo_t1861056598 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, ParameterInfo_t1861056598 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Reflection.ParameterModifier[]
struct ParameterModifierU5BU5D_t2943407543  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) ParameterModifier_t1461694466  m_Items[1];

public:
	inline ParameterModifier_t1461694466  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline ParameterModifier_t1461694466 * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, ParameterModifier_t1461694466  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline ParameterModifier_t1461694466  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline ParameterModifier_t1461694466 * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, ParameterModifier_t1461694466  value)
	{
		m_Items[index] = value;
	}
};

extern "C" void Object_t631007953_marshal_pinvoke(const Object_t631007953& unmarshaled, Object_t631007953_marshaled_pinvoke& marshaled);
extern "C" void Object_t631007953_marshal_pinvoke_back(const Object_t631007953_marshaled_pinvoke& marshaled, Object_t631007953& unmarshaled);
extern "C" void Object_t631007953_marshal_pinvoke_cleanup(Object_t631007953_marshaled_pinvoke& marshaled);
extern "C" void Object_t631007953_marshal_com(const Object_t631007953& unmarshaled, Object_t631007953_marshaled_com& marshaled);
extern "C" void Object_t631007953_marshal_com_back(const Object_t631007953_marshaled_com& marshaled, Object_t631007953& unmarshaled);
extern "C" void Object_t631007953_marshal_com_cleanup(Object_t631007953_marshaled_com& marshaled);

// System.Void System.Collections.Generic.Stack`1<System.Object>::.ctor()
extern "C"  void Stack_1__ctor_m3164958980_gshared (Stack_1_t3923495619 * __this, const MethodInfo* method);
// System.Void System.Collections.Generic.Stack`1<System.Object>::Push(!0)
extern "C"  void Stack_1_Push_m1669856732_gshared (Stack_1_t3923495619 * __this, Il2CppObject * p0, const MethodInfo* method);
// !0 System.Collections.Generic.Stack`1<System.Object>::Pop()
extern "C"  Il2CppObject * Stack_1_Pop_m756553478_gshared (Stack_1_t3923495619 * __this, const MethodInfo* method);
// System.Int32 System.Collections.Generic.Stack`1<System.Object>::get_Count()
extern "C"  int32_t Stack_1_get_Count_m1599740434_gshared (Stack_1_t3923495619 * __this, const MethodInfo* method);
// System.Void System.Collections.Generic.List`1<System.Object>::.ctor()
extern "C"  void List_1__ctor_m2321703786_gshared (List_1_t257213610 * __this, const MethodInfo* method);
// System.Void System.Collections.Generic.List`1<System.Object>::Add(!0)
extern "C"  void List_1_Add_m3338814081_gshared (List_1_t257213610 * __this, Il2CppObject * p0, const MethodInfo* method);
// !0[] System.Collections.Generic.List`1<System.Object>::ToArray()
extern "C"  ObjectU5BU5D_t2843939325* List_1_ToArray_m4168020446_gshared (List_1_t257213610 * __this, const MethodInfo* method);
// T UnityEngine.AttributeHelperEngine::GetCustomAttributeOfType<System.Object>(System.Type)
extern "C"  Il2CppObject * AttributeHelperEngine_GetCustomAttributeOfType_TisIl2CppObject_m2601070632_gshared (Il2CppObject * __this /* static, unused */, Type_t * ___klass0, const MethodInfo* method);
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::.ctor()
extern "C"  void List_1__ctor_m4218989665_gshared (List_1_t899420910 * __this, const MethodInfo* method);
// System.Void System.Collections.Generic.List`1<UnityEngine.Color32>::.ctor()
extern "C"  void List_1__ctor_m2033536304_gshared (List_1_t4072576034 * __this, const MethodInfo* method);
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector2>::.ctor()
extern "C"  void List_1__ctor_m4218988514_gshared (List_1_t3628304265 * __this, const MethodInfo* method);
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector4>::.ctor()
extern "C"  void List_1__ctor_m4218986464_gshared (List_1_t496136383 * __this, const MethodInfo* method);
// System.Void System.Collections.Generic.List`1<System.Int32>::.ctor()
extern "C"  void List_1__ctor_m4278196146_gshared (List_1_t128053199 * __this, const MethodInfo* method);
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::Add(!0)
extern "C"  void List_1_Add_m4259514621_gshared (List_1_t899420910 * __this, Vector3_t3722313464  p0, const MethodInfo* method);
// System.Void System.Collections.Generic.List`1<UnityEngine.Color32>::Add(!0)
extern "C"  void List_1_Add_m3372602044_gshared (List_1_t4072576034 * __this, Color32_t2600501292  p0, const MethodInfo* method);
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector2>::Add(!0)
extern "C"  void List_1_Add_m4259515774_gshared (List_1_t3628304265 * __this, Vector2_t2156229523  p0, const MethodInfo* method);
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector4>::Add(!0)
extern "C"  void List_1_Add_m4259509372_gshared (List_1_t496136383 * __this, Vector4_t3319028937  p0, const MethodInfo* method);
// System.Void System.Collections.Generic.List`1<System.Int32>::Add(!0)
extern "C"  void List_1_Add_m4025721294_gshared (List_1_t128053199 * __this, int32_t p0, const MethodInfo* method);
// !0[] System.Collections.Generic.List`1<System.Int32>::ToArray()
extern "C"  Int32U5BU5D_t385246372* List_1_ToArray_m789548171_gshared (List_1_t128053199 * __this, const MethodInfo* method);
// !0 System.Collections.Generic.List`1<System.Object>::get_Item(System.Int32)
extern "C"  Il2CppObject * List_1_get_Item_m2287542950_gshared (List_1_t257213610 * __this, int32_t p0, const MethodInfo* method);
// System.Int32 System.Collections.Generic.List`1<System.Object>::get_Count()
extern "C"  int32_t List_1_get_Count_m2934127733_gshared (List_1_t257213610 * __this, const MethodInfo* method);
// System.Void System.Predicate`1<System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void Predicate_1__ctor_m327447107_gshared (Predicate_1_t3905400288 * __this, Il2CppObject * p0, IntPtr_t p1, const MethodInfo* method);
// System.Int32 System.Collections.Generic.List`1<System.Object>::RemoveAll(System.Predicate`1<!0>)
extern "C"  int32_t List_1_RemoveAll_m4292035398_gshared (List_1_t257213610 * __this, Predicate_1_t3905400288 * p0, const MethodInfo* method);
// System.Void System.Collections.Generic.List`1<System.Object>::Clear()
extern "C"  void List_1_Clear_m3697625829_gshared (List_1_t257213610 * __this, const MethodInfo* method);
// System.Void System.Collections.Generic.List`1<System.Object>::AddRange(System.Collections.Generic.IEnumerable`1<!0>)
extern "C"  void List_1_AddRange_m3709462088_gshared (List_1_t257213610 * __this, Il2CppObject* p0, const MethodInfo* method);
// System.Void UnityEngine.Events.CachedInvokableCall`1<System.Single>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
extern "C"  void CachedInvokableCall_1__ctor_m2985117283_gshared (CachedInvokableCall_1_t3723462114 * __this, Object_t631007953 * p0, MethodInfo_t * p1, float p2, const MethodInfo* method);
// System.Void UnityEngine.Events.CachedInvokableCall`1<System.Int32>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
extern "C"  void CachedInvokableCall_1__ctor_m3353156630_gshared (CachedInvokableCall_1_t982173797 * __this, Object_t631007953 * p0, MethodInfo_t * p1, int32_t p2, const MethodInfo* method);
// System.Void UnityEngine.Events.CachedInvokableCall`1<System.Object>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
extern "C"  void CachedInvokableCall_1__ctor_m2686176129_gshared (CachedInvokableCall_1_t1111334208 * __this, Object_t631007953 * p0, MethodInfo_t * p1, Il2CppObject * p2, const MethodInfo* method);
// System.Void UnityEngine.Events.CachedInvokableCall`1<System.Boolean>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
extern "C"  void CachedInvokableCall_1__ctor_m3594541075_gshared (CachedInvokableCall_1_t2423483305 * __this, Object_t631007953 * p0, MethodInfo_t * p1, bool p2, const MethodInfo* method);
// System.Collections.Generic.List`1/Enumerator<!0> System.Collections.Generic.List`1<System.Object>::GetEnumerator()
extern "C"  Enumerator_t2146457487  List_1_GetEnumerator_m2930774921_gshared (List_1_t257213610 * __this, const MethodInfo* method);
// !0 System.Collections.Generic.List`1/Enumerator<System.Object>::get_Current()
extern "C"  Il2CppObject * Enumerator_get_Current_m470245444_gshared (Enumerator_t2146457487 * __this, const MethodInfo* method);
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.Object>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m2142368520_gshared (Enumerator_t2146457487 * __this, const MethodInfo* method);
// System.Void System.Collections.Generic.List`1/Enumerator<System.Object>::Dispose()
extern "C"  void Enumerator_Dispose_m3007748546_gshared (Enumerator_t2146457487 * __this, const MethodInfo* method);

// System.Void System.Attribute::.ctor()
extern "C"  void Attribute__ctor_m1529526131 (Attribute_t861562559 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AI.NavMesh/OnNavMeshPreUpdate::Invoke()
extern "C"  void OnNavMeshPreUpdate_Invoke_m2083125449 (OnNavMeshPreUpdate_t1580782682 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Object::.ctor()
extern "C"  void Object__ctor_m297566312 (Il2CppObject * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AnimationCurve::Init(UnityEngine.Keyframe[])
extern "C"  void AnimationCurve_Init_m1115968024 (AnimationCurve_t3046754366 * __this, KeyframeU5BU5D_t1068524471* ___keys0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AnimationCurve::Cleanup()
extern "C"  void AnimationCurve_Cleanup_m3505736496 (AnimationCurve_t3046754366 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Object::Finalize()
extern "C"  void Object_Finalize_m3076187857 (Il2CppObject * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Keyframe[] UnityEngine.AnimationCurve::GetKeys()
extern "C"  KeyframeU5BU5D_t1068524471* AnimationCurve_GetKeys_m2150394447 (AnimationCurve_t3046754366 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AnimationCurve::SetKeys(UnityEngine.Keyframe[])
extern "C"  void AnimationCurve_SetKeys_m121116590 (AnimationCurve_t3046754366 * __this, KeyframeU5BU5D_t1068524471* ___keys0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.AnimationCurve::AddKey_Internal(UnityEngine.Keyframe)
extern "C"  int32_t AnimationCurve_AddKey_Internal_m2661999051 (AnimationCurve_t3046754366 * __this, Keyframe_t4206410242  ___key0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.AnimationCurve::INTERNAL_CALL_AddKey_Internal(UnityEngine.AnimationCurve,UnityEngine.Keyframe&)
extern "C"  int32_t AnimationCurve_INTERNAL_CALL_AddKey_Internal_m4197796724 (Il2CppObject * __this /* static, unused */, AnimationCurve_t3046754366 * ___self0, Keyframe_t4206410242 * ___key1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.AnimationEvent::get_isFiredByLegacy()
extern "C"  bool AnimationEvent_get_isFiredByLegacy_m2397681361 (AnimationEvent_t1536042487 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Debug::LogError(System.Object)
extern "C"  void Debug_LogError_m2059623341 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.AnimationEvent::get_isFiredByAnimator()
extern "C"  bool AnimationEvent_get_isFiredByAnimator_m2545534689 (AnimationEvent_t1536042487 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.AnimationEvent::get_functionName()
extern "C"  String_t* AnimationEvent_get_functionName_m1502361540 (AnimationEvent_t1536042487 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.AnimationEvent::get_time()
extern "C"  float AnimationEvent_get_time_m3455244934 (AnimationEvent_t1536042487 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Single::GetHashCode()
extern "C"  int32_t Single_GetHashCode_m1558506138 (float* __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Animator::GetFloatString(System.String)
extern "C"  float Animator_GetFloatString_m293525848 (Animator_t434523843 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animator::SetFloatString(System.String,System.Single)
extern "C"  void Animator_SetFloatString_m3317565049 (Animator_t434523843 * __this, String_t* ___name0, float ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Animator::GetBoolString(System.String)
extern "C"  bool Animator_GetBoolString_m1559437030 (Animator_t434523843 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animator::SetBoolString(System.String,System.Boolean)
extern "C"  void Animator_SetBoolString_m2747575116 (Animator_t434523843 * __this, String_t* ___name0, bool ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animator::SetTriggerString(System.String)
extern "C"  void Animator_SetTriggerString_m2642918599 (Animator_t434523843 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animator::ResetTriggerString(System.String)
extern "C"  void Animator_ResetTriggerString_m1083629800 (Animator_t434523843 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animator::Play(System.String,System.Int32,System.Single)
extern "C"  void Animator_Play_m2010935922 (Animator_t434523843 * __this, String_t* ___stateName0, int32_t ___layer1, float ___normalizedTime2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Animator::StringToHash(System.String)
extern "C"  int32_t Animator_StringToHash_m2188957499 (Il2CppObject * __this /* static, unused */, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Animator::Play(System.Int32,System.Int32,System.Single)
extern "C"  void Animator_Play_m431000399 (Animator_t434523843 * __this, int32_t ___stateNameHash0, int32_t ___layer1, float ___normalizedTime2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.String::op_Equality(System.String,System.String)
extern "C"  bool String_op_Equality_m920492651 (Il2CppObject * __this /* static, unused */, String_t* p0, String_t* p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.AnimatorControllerParameter::get_name()
extern "C"  String_t* AnimatorControllerParameter_get_name_m4038051952 (AnimatorControllerParameter_t1758260042 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.AnimatorStateInfo::IsName(System.String)
extern "C"  bool AnimatorStateInfo_IsName_m1584047739 (AnimatorStateInfo_t509032636 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.AnimatorStateInfo::get_fullPathHash()
extern "C"  int32_t AnimatorStateInfo_get_fullPathHash_m2240461550 (AnimatorStateInfo_t509032636 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.AnimatorStateInfo::get_nameHash()
extern "C"  int32_t AnimatorStateInfo_get_nameHash_m3988777370 (AnimatorStateInfo_t509032636 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.AnimatorStateInfo::get_shortNameHash()
extern "C"  int32_t AnimatorStateInfo_get_shortNameHash_m119881483 (AnimatorStateInfo_t509032636 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.AnimatorStateInfo::get_normalizedTime()
extern "C"  float AnimatorStateInfo_get_normalizedTime_m2807918906 (AnimatorStateInfo_t509032636 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.AnimatorStateInfo::get_length()
extern "C"  float AnimatorStateInfo_get_length_m3907061235 (AnimatorStateInfo_t509032636 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.AnimatorStateInfo::get_speed()
extern "C"  float AnimatorStateInfo_get_speed_m1229611452 (AnimatorStateInfo_t509032636 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.AnimatorStateInfo::get_speedMultiplier()
extern "C"  float AnimatorStateInfo_get_speedMultiplier_m1871749593 (AnimatorStateInfo_t509032636 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.AnimatorStateInfo::get_tagHash()
extern "C"  int32_t AnimatorStateInfo_get_tagHash_m3281207203 (AnimatorStateInfo_t509032636 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.AnimatorStateInfo::IsTag(System.String)
extern "C"  bool AnimatorStateInfo_IsTag_m4246855624 (AnimatorStateInfo_t509032636 * __this, String_t* ___tag0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.AnimatorStateInfo::get_loop()
extern "C"  bool AnimatorStateInfo_get_loop_m1486238854 (AnimatorStateInfo_t509032636 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.AnimatorTransitionInfo::IsName(System.String)
extern "C"  bool AnimatorTransitionInfo_IsName_m3060761961 (AnimatorTransitionInfo_t2534804151 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.AnimatorTransitionInfo::IsUserName(System.String)
extern "C"  bool AnimatorTransitionInfo_IsUserName_m2064408313 (AnimatorTransitionInfo_t2534804151 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.AnimatorTransitionInfo::get_fullPathHash()
extern "C"  int32_t AnimatorTransitionInfo_get_fullPathHash_m2773536167 (AnimatorTransitionInfo_t2534804151 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.AnimatorTransitionInfo::get_nameHash()
extern "C"  int32_t AnimatorTransitionInfo_get_nameHash_m2906244497 (AnimatorTransitionInfo_t2534804151 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.AnimatorTransitionInfo::get_userNameHash()
extern "C"  int32_t AnimatorTransitionInfo_get_userNameHash_m3118405726 (AnimatorTransitionInfo_t2534804151 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.AnimatorTransitionInfo::get_normalizedTime()
extern "C"  float AnimatorTransitionInfo_get_normalizedTime_m3104123786 (AnimatorTransitionInfo_t2534804151 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.AnimatorTransitionInfo::get_anyState()
extern "C"  bool AnimatorTransitionInfo_get_anyState_m4275202666 (AnimatorTransitionInfo_t2534804151 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.AnimatorTransitionInfo::get_entry()
extern "C"  bool AnimatorTransitionInfo_get_entry_m3079357495 (AnimatorTransitionInfo_t2534804151 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.AnimatorTransitionInfo::get_exit()
extern "C"  bool AnimatorTransitionInfo_get_exit_m3346751000 (AnimatorTransitionInfo_t2534804151 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Application/LowMemoryCallback::Invoke()
extern "C"  void LowMemoryCallback_Invoke_m3804674182 (LowMemoryCallback_t4104246196 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Application/LogCallback::Invoke(System.String,System.String,UnityEngine.LogType)
extern "C"  void LogCallback_Invoke_m3527969146 (LogCallback_t3588208630 * __this, String_t* ___condition0, String_t* ___stackTrace1, int32_t ___type2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.NullReferenceException::.ctor(System.String)
extern "C"  void NullReferenceException__ctor_m3076065613 (NullReferenceException_t1023182353 * __this, String_t* p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Object[] UnityEngine.AssetBundle::LoadAssetWithSubAssets_Internal(System.String,System.Type)
extern "C"  ObjectU5BU5D_t1417781964* AssetBundle_LoadAssetWithSubAssets_Internal_m3736220809 (AssetBundle_t1153907252 * __this, String_t* ___name0, Type_t * ___type1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.AssetBundleRequest UnityEngine.AssetBundle::LoadAssetWithSubAssetsAsync_Internal(System.String,System.Type)
extern "C"  AssetBundleRequest_t699759206 * AssetBundle_LoadAssetWithSubAssetsAsync_Internal_m753058609 (AssetBundle_t1153907252 * __this, String_t* ___name0, Type_t * ___type1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AsyncOperation::.ctor()
extern "C"  void AsyncOperation__ctor_m2683541089 (AsyncOperation_t1445031843 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.YieldInstruction::.ctor()
extern "C"  void YieldInstruction__ctor_m3893172416 (YieldInstruction_t403091072 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AsyncOperation::InternalDestroy()
extern "C"  void AsyncOperation_InternalDestroy_m2288495658 (AsyncOperation_t1445031843 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.Stack`1<System.Type>::.ctor()
#define Stack_1__ctor_m926334451(__this, method) ((  void (*) (Stack_1_t3327334215 *, const MethodInfo*))Stack_1__ctor_m3164958980_gshared)(__this, method)
// System.Void System.Collections.Generic.Stack`1<System.Type>::Push(!0)
#define Stack_1_Push_m4005337547(__this, p0, method) ((  void (*) (Stack_1_t3327334215 *, Type_t *, const MethodInfo*))Stack_1_Push_m1669856732_gshared)(__this, p0, method)
// System.Type System.Type::GetTypeFromHandle(System.RuntimeTypeHandle)
extern "C"  Type_t * Type_GetTypeFromHandle_m1620074514 (Il2CppObject * __this /* static, unused */, RuntimeTypeHandle_t3027515415  p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// !0 System.Collections.Generic.Stack`1<System.Type>::Pop()
#define Stack_1_Pop_m4032335803(__this, method) ((  Type_t * (*) (Stack_1_t3327334215 *, const MethodInfo*))Stack_1_Pop_m756553478_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Stack`1<System.Type>::get_Count()
#define Stack_1_get_Count_m95885128(__this, method) ((  int32_t (*) (Stack_1_t3327334215 *, const MethodInfo*))Stack_1_get_Count_m1599740434_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Type>::.ctor()
#define List_1__ctor_m696099607(__this, method) ((  void (*) (List_1_t3956019502 *, const MethodInfo*))List_1__ctor_m2321703786_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Type>::Add(!0)
#define List_1_Add_m209710667(__this, p0, method) ((  void (*) (List_1_t3956019502 *, Type_t *, const MethodInfo*))List_1_Add_m3338814081_gshared)(__this, p0, method)
// !0[] System.Collections.Generic.List`1<System.Type>::ToArray()
#define List_1_ToArray_m1314877377(__this, method) ((  TypeU5BU5D_t3940880105* (*) (List_1_t3956019502 *, const MethodInfo*))List_1_ToArray_m4168020446_gshared)(__this, method)
// T UnityEngine.AttributeHelperEngine::GetCustomAttributeOfType<UnityEngine.DefaultExecutionOrder>(System.Type)
#define AttributeHelperEngine_GetCustomAttributeOfType_TisDefaultExecutionOrder_t3059642329_m1616528635(__this /* static, unused */, ___klass0, method) ((  DefaultExecutionOrder_t3059642329 * (*) (Il2CppObject * /* static, unused */, Type_t *, const MethodInfo*))AttributeHelperEngine_GetCustomAttributeOfType_TisIl2CppObject_m2601070632_gshared)(__this /* static, unused */, ___klass0, method)
// System.Int32 UnityEngine.DefaultExecutionOrder::get_order()
extern "C"  int32_t DefaultExecutionOrder_get_order_m2144583152 (DefaultExecutionOrder_t3059642329 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AudioClip/PCMReaderCallback::Invoke(System.Single[])
extern "C"  void PCMReaderCallback_Invoke_m650805914 (PCMReaderCallback_t1677636661 * __this, SingleU5BU5D_t1444911251* ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AudioClip/PCMSetPositionCallback::Invoke(System.Int32)
extern "C"  void PCMSetPositionCallback_Invoke_m3905727039 (PCMSetPositionCallback_t1059417452 * __this, int32_t ___position0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AudioSettings/AudioConfigurationChangeHandler::Invoke(System.Boolean)
extern "C"  void AudioConfigurationChangeHandler_Invoke_m2130183669 (AudioConfigurationChangeHandler_t2089929874 * __this, bool ___deviceWasChanged0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AudioSource::Play(System.UInt64)
extern "C"  void AudioSource_Play_m3647871236 (AudioSource_t3935305588 * __this, uint64_t ___delay0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AudioSource::INTERNAL_CALL_Pause(UnityEngine.AudioSource)
extern "C"  void AudioSource_INTERNAL_CALL_Pause_m1192371133 (Il2CppObject * __this /* static, unused */, AudioSource_t3935305588 * ___self0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AudioSource::PlayOneShot(UnityEngine.AudioClip,System.Single)
extern "C"  void AudioSource_PlayOneShot_m1563957832 (AudioSource_t3935305588 * __this, AudioClip_t3680889665 * ___clip0, float ___volumeScale1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Component::.ctor()
extern "C"  void Component__ctor_m3533368306 (Component_t1923634451 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Vector3::op_Multiply(UnityEngine.Vector3,System.Single)
extern "C"  Vector3_t3722313464  Vector3_op_Multiply_m3506743150 (Il2CppObject * __this /* static, unused */, Vector3_t3722313464  ___a0, float ___d1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Bounds::.ctor(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  void Bounds__ctor_m131864078 (Bounds_t2266837910 * __this, Vector3_t3722313464  ___center0, Vector3_t3722313464  ___size1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Bounds::get_center()
extern "C"  Vector3_t3722313464  Bounds_get_center_m1997663455 (Bounds_t2266837910 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Vector3::GetHashCode()
extern "C"  int32_t Vector3_GetHashCode_m4064605418 (Vector3_t3722313464 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Bounds::get_extents()
extern "C"  Vector3_t3722313464  Bounds_get_extents_m3683565517 (Bounds_t2266837910 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Bounds::GetHashCode()
extern "C"  int32_t Bounds_GetHashCode_m1195828975 (Bounds_t2266837910 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Vector3::Equals(System.Object)
extern "C"  bool Vector3_Equals_m496167795 (Vector3_t3722313464 * __this, Il2CppObject * ___other0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Bounds::Equals(System.Object)
extern "C"  bool Bounds_Equals_m1462810812 (Bounds_t2266837910 * __this, Il2CppObject * ___other0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Bounds::set_center(UnityEngine.Vector3)
extern "C"  void Bounds_set_center_m1602292844 (Bounds_t2266837910 * __this, Vector3_t3722313464  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Bounds::get_size()
extern "C"  Vector3_t3722313464  Bounds_get_size_m1171376090 (Bounds_t2266837910 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Bounds::set_size(UnityEngine.Vector3)
extern "C"  void Bounds_set_size_m1025767497 (Bounds_t2266837910 * __this, Vector3_t3722313464  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Bounds::set_extents(UnityEngine.Vector3)
extern "C"  void Bounds_set_extents_m248192017 (Bounds_t2266837910 * __this, Vector3_t3722313464  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Vector3::op_Subtraction(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  Vector3_t3722313464  Vector3_op_Subtraction_m2566684344 (Il2CppObject * __this /* static, unused */, Vector3_t3722313464  ___a0, Vector3_t3722313464  ___b1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Bounds::get_min()
extern "C"  Vector3_t3722313464  Bounds_get_min_m1736091801 (Bounds_t2266837910 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Vector3::op_Addition(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  Vector3_t3722313464  Vector3_op_Addition_m1781942663 (Il2CppObject * __this /* static, unused */, Vector3_t3722313464  ___a0, Vector3_t3722313464  ___b1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Bounds::get_max()
extern "C"  Vector3_t3722313464  Bounds_get_max_m1659284971 (Bounds_t2266837910 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Vector3::op_Equality(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  bool Vector3_op_Equality_m2672007619 (Il2CppObject * __this /* static, unused */, Vector3_t3722313464  ___lhs0, Vector3_t3722313464  ___rhs1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Bounds::op_Equality(UnityEngine.Bounds,UnityEngine.Bounds)
extern "C"  bool Bounds_op_Equality_m3976187280 (Il2CppObject * __this /* static, unused */, Bounds_t2266837910  ___lhs0, Bounds_t2266837910  ___rhs1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Bounds::SetMinMax(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  void Bounds_SetMinMax_m1012362698 (Bounds_t2266837910 * __this, Vector3_t3722313464  ___min0, Vector3_t3722313464  ___max1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Vector3::Min(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  Vector3_t3722313464  Vector3_Min_m4041074433 (Il2CppObject * __this /* static, unused */, Vector3_t3722313464  ___lhs0, Vector3_t3722313464  ___rhs1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Vector3::Max(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  Vector3_t3722313464  Vector3_Max_m821979329 (Il2CppObject * __this /* static, unused */, Vector3_t3722313464  ___lhs0, Vector3_t3722313464  ___rhs1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Bounds::Encapsulate(UnityEngine.Vector3)
extern "C"  void Bounds_Encapsulate_m3482076963 (Bounds_t2266837910 * __this, Vector3_t3722313464  ___point0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.UnityString::Format(System.String,System.Object[])
extern "C"  String_t* UnityString_Format_m3741272017 (Il2CppObject * __this /* static, unused */, String_t* ___fmt0, ObjectU5BU5D_t2843939325* ___args1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.Bounds::ToString()
extern "C"  String_t* Bounds_ToString_m72361594 (Bounds_t2266837910 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.BoxCollider::INTERNAL_set_center(UnityEngine.Vector3&)
extern "C"  void BoxCollider_INTERNAL_set_center_m3088077495 (BoxCollider_t1640800422 * __this, Vector3_t3722313464 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.BoxCollider::INTERNAL_set_size(UnityEngine.Vector3&)
extern "C"  void BoxCollider_INTERNAL_set_size_m950353937 (BoxCollider_t1640800422 * __this, Vector3_t3722313464 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Camera::INTERNAL_get_pixelRect(UnityEngine.Rect&)
extern "C"  void Camera_INTERNAL_get_pixelRect_m1806464323 (Camera_t4157153871 * __this, Rect_t2360479859 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Camera::INTERNAL_get_projectionMatrix(UnityEngine.Matrix4x4&)
extern "C"  void Camera_INTERNAL_get_projectionMatrix_m3112156927 (Camera_t4157153871 * __this, Matrix4x4_t1817901843 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Camera::INTERNAL_set_projectionMatrix(UnityEngine.Matrix4x4&)
extern "C"  void Camera_INTERNAL_set_projectionMatrix_m1700405219 (Camera_t4157153871 * __this, Matrix4x4_t1817901843 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Camera::INTERNAL_CALL_ResetProjectionMatrix(UnityEngine.Camera)
extern "C"  void Camera_INTERNAL_CALL_ResetProjectionMatrix_m2949107079 (Il2CppObject * __this /* static, unused */, Camera_t4157153871 * ___self0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Camera::INTERNAL_CALL_WorldToScreenPoint(UnityEngine.Camera,UnityEngine.Vector3&,UnityEngine.Vector3&)
extern "C"  void Camera_INTERNAL_CALL_WorldToScreenPoint_m2343250406 (Il2CppObject * __this /* static, unused */, Camera_t4157153871 * ___self0, Vector3_t3722313464 * ___position1, Vector3_t3722313464 * ___value2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Camera::INTERNAL_CALL_ViewportToWorldPoint(UnityEngine.Camera,UnityEngine.Vector3&,UnityEngine.Vector3&)
extern "C"  void Camera_INTERNAL_CALL_ViewportToWorldPoint_m2395836354 (Il2CppObject * __this /* static, unused */, Camera_t4157153871 * ___self0, Vector3_t3722313464 * ___position1, Vector3_t3722313464 * ___value2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Camera::INTERNAL_CALL_ScreenToWorldPoint(UnityEngine.Camera,UnityEngine.Vector3&,UnityEngine.Vector3&)
extern "C"  void Camera_INTERNAL_CALL_ScreenToWorldPoint_m2680061077 (Il2CppObject * __this /* static, unused */, Camera_t4157153871 * ___self0, Vector3_t3722313464 * ___position1, Vector3_t3722313464 * ___value2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Camera::INTERNAL_CALL_ScreenToViewportPoint(UnityEngine.Camera,UnityEngine.Vector3&,UnityEngine.Vector3&)
extern "C"  void Camera_INTERNAL_CALL_ScreenToViewportPoint_m3505215880 (Il2CppObject * __this /* static, unused */, Camera_t4157153871 * ___self0, Vector3_t3722313464 * ___position1, Vector3_t3722313464 * ___value2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Camera::INTERNAL_CALL_ScreenPointToRay(UnityEngine.Camera,UnityEngine.Vector3&,UnityEngine.Ray&)
extern "C"  void Camera_INTERNAL_CALL_ScreenPointToRay_m3272660008 (Il2CppObject * __this /* static, unused */, Camera_t4157153871 * ___self0, Vector3_t3722313464 * ___position1, Ray_t3785851493 * ___value2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Camera/CameraCallback::Invoke(UnityEngine.Camera)
extern "C"  void CameraCallback_Invoke_m2206506905 (CameraCallback_t190067161 * __this, Camera_t4157153871 * ___cam0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject UnityEngine.Camera::INTERNAL_CALL_RaycastTry(UnityEngine.Camera,UnityEngine.Ray&,System.Single,System.Int32)
extern "C"  GameObject_t1113636619 * Camera_INTERNAL_CALL_RaycastTry_m1525247362 (Il2CppObject * __this /* static, unused */, Camera_t4157153871 * ___self0, Ray_t3785851493 * ___ray1, float ___distance2, int32_t ___layerMask3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject UnityEngine.Camera::INTERNAL_CALL_RaycastTry2D(UnityEngine.Camera,UnityEngine.Ray&,System.Single,System.Int32)
extern "C"  GameObject_t1113636619 * Camera_INTERNAL_CALL_RaycastTry2D_m4005245494 (Il2CppObject * __this /* static, unused */, Camera_t4157153871 * ___self0, Ray_t3785851493 * ___ray1, float ___distance2, int32_t ___layerMask3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Delegate System.Delegate::Combine(System.Delegate,System.Delegate)
extern "C"  Delegate_t1188392813 * Delegate_Combine_m1859655160 (Il2CppObject * __this /* static, unused */, Delegate_t1188392813 * p0, Delegate_t1188392813 * p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Delegate System.Delegate::Remove(System.Delegate,System.Delegate)
extern "C"  Delegate_t1188392813 * Delegate_Remove_m334097152 (Il2CppObject * __this /* static, unused */, Delegate_t1188392813 * p0, Delegate_t1188392813 * p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Canvas/WillRenderCanvases::Invoke()
extern "C"  void WillRenderCanvases_Invoke_m352835184 (WillRenderCanvases_t3309123499 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Canvas::SendWillRenderCanvases()
extern "C"  void Canvas_SendWillRenderCanvases_m2809604487 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.CanvasGroup::get_blocksRaycasts()
extern "C"  bool CanvasGroup_get_blocksRaycasts_m3740270137 (CanvasGroup_t4083511760 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.CanvasRenderer::INTERNAL_CALL_SetColor(UnityEngine.CanvasRenderer,UnityEngine.Color&)
extern "C"  void CanvasRenderer_INTERNAL_CALL_SetColor_m975881866 (Il2CppObject * __this /* static, unused */, CanvasRenderer_t2598313366 * ___self0, Color_t2555686324 * ___color1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.CanvasRenderer::INTERNAL_CALL_GetColor(UnityEngine.CanvasRenderer,UnityEngine.Color&)
extern "C"  void CanvasRenderer_INTERNAL_CALL_GetColor_m21875977 (Il2CppObject * __this /* static, unused */, CanvasRenderer_t2598313366 * ___self0, Color_t2555686324 * ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Mesh::.ctor()
extern "C"  void Mesh__ctor_m2962122670 (Mesh_t3648964284 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::.ctor()
#define List_1__ctor_m4218989665(__this, method) ((  void (*) (List_1_t899420910 *, const MethodInfo*))List_1__ctor_m4218989665_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Color32>::.ctor()
#define List_1__ctor_m2033536304(__this, method) ((  void (*) (List_1_t4072576034 *, const MethodInfo*))List_1__ctor_m2033536304_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector2>::.ctor()
#define List_1__ctor_m4218988514(__this, method) ((  void (*) (List_1_t3628304265 *, const MethodInfo*))List_1__ctor_m4218988514_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector4>::.ctor()
#define List_1__ctor_m4218986464(__this, method) ((  void (*) (List_1_t496136383 *, const MethodInfo*))List_1__ctor_m4218986464_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Int32>::.ctor()
#define List_1__ctor_m4278196146(__this, method) ((  void (*) (List_1_t128053199 *, const MethodInfo*))List_1__ctor_m4278196146_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::Add(!0)
#define List_1_Add_m4259514621(__this, p0, method) ((  void (*) (List_1_t899420910 *, Vector3_t3722313464 , const MethodInfo*))List_1_Add_m4259514621_gshared)(__this, p0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Color32>::Add(!0)
#define List_1_Add_m3372602044(__this, p0, method) ((  void (*) (List_1_t4072576034 *, Color32_t2600501292 , const MethodInfo*))List_1_Add_m3372602044_gshared)(__this, p0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector2>::Add(!0)
#define List_1_Add_m4259515774(__this, p0, method) ((  void (*) (List_1_t3628304265 *, Vector2_t2156229523 , const MethodInfo*))List_1_Add_m4259515774_gshared)(__this, p0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector4>::Add(!0)
#define List_1_Add_m4259509372(__this, p0, method) ((  void (*) (List_1_t496136383 *, Vector4_t3319028937 , const MethodInfo*))List_1_Add_m4259509372_gshared)(__this, p0, method)
// System.Void System.Collections.Generic.List`1<System.Int32>::Add(!0)
#define List_1_Add_m4025721294(__this, p0, method) ((  void (*) (List_1_t128053199 *, int32_t, const MethodInfo*))List_1_Add_m4025721294_gshared)(__this, p0, method)
// System.Void UnityEngine.Mesh::SetVertices(System.Collections.Generic.List`1<UnityEngine.Vector3>)
extern "C"  void Mesh_SetVertices_m2641879696 (Mesh_t3648964284 * __this, List_1_t899420910 * ___inVertices0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Mesh::SetColors(System.Collections.Generic.List`1<UnityEngine.Color32>)
extern "C"  void Mesh_SetColors_m3397914746 (Mesh_t3648964284 * __this, List_1_t4072576034 * ___inColors0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Mesh::SetNormals(System.Collections.Generic.List`1<UnityEngine.Vector3>)
extern "C"  void Mesh_SetNormals_m404565226 (Mesh_t3648964284 * __this, List_1_t899420910 * ___inNormals0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Mesh::SetTangents(System.Collections.Generic.List`1<UnityEngine.Vector4>)
extern "C"  void Mesh_SetTangents_m2687625516 (Mesh_t3648964284 * __this, List_1_t496136383 * ___inTangents0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Mesh::SetUVs(System.Int32,System.Collections.Generic.List`1<UnityEngine.Vector2>)
extern "C"  void Mesh_SetUVs_m1321350398 (Mesh_t3648964284 * __this, int32_t ___channel0, List_1_t3628304265 * ___uvs1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// !0[] System.Collections.Generic.List`1<System.Int32>::ToArray()
#define List_1_ToArray_m789548171(__this, method) ((  Int32U5BU5D_t385246372* (*) (List_1_t128053199 *, const MethodInfo*))List_1_ToArray_m789548171_gshared)(__this, method)
// System.Void UnityEngine.Mesh::SetIndices(System.Int32[],UnityEngine.MeshTopology,System.Int32)
extern "C"  void Mesh_SetIndices_m3944365893 (Mesh_t3648964284 * __this, Int32U5BU5D_t385246372* ___indices0, int32_t ___topology1, int32_t ___submesh2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.CanvasRenderer::SetMesh(UnityEngine.Mesh)
extern "C"  void CanvasRenderer_SetMesh_m3875887985 (CanvasRenderer_t2598313366 * __this, Mesh_t3648964284 * ___mesh0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Object::DestroyImmediate(UnityEngine.Object)
extern "C"  void Object_DestroyImmediate_m1556866283 (Il2CppObject * __this /* static, unused */, Object_t631007953 * ___obj0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.CanvasRenderer::INTERNAL_CALL_EnableRectClipping(UnityEngine.CanvasRenderer,UnityEngine.Rect&)
extern "C"  void CanvasRenderer_INTERNAL_CALL_EnableRectClipping_m1902636664 (Il2CppObject * __this /* static, unused */, CanvasRenderer_t2598313366 * ___self0, Rect_t2360479859 * ___rect1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.CanvasRenderer::get_materialCount()
extern "C"  int32_t CanvasRenderer_get_materialCount_m3538244728 (CanvasRenderer_t2598313366 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Math::Max(System.Int32,System.Int32)
extern "C"  int32_t Math_Max_m1873195862 (Il2CppObject * __this /* static, unused */, int32_t p0, int32_t p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.CanvasRenderer::set_materialCount(System.Int32)
extern "C"  void CanvasRenderer_set_materialCount_m3744103248 (CanvasRenderer_t2598313366 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.CanvasRenderer::SetMaterial(UnityEngine.Material,System.Int32)
extern "C"  void CanvasRenderer_SetMaterial_m856908584 (CanvasRenderer_t2598313366 * __this, Material_t340375123 * ___material0, int32_t ___index1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.CanvasRenderer::SetTexture(UnityEngine.Texture)
extern "C"  void CanvasRenderer_SetTexture_m685945407 (CanvasRenderer_t2598313366 * __this, Texture_t3661962703 * ___texture0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material UnityEngine.CanvasRenderer::GetMaterial(System.Int32)
extern "C"  Material_t340375123 * CanvasRenderer_GetMaterial_m221169768 (CanvasRenderer_t2598313366 * __this, int32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.CanvasRenderer::SplitUIVertexStreamsInternal(System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object)
extern "C"  void CanvasRenderer_SplitUIVertexStreamsInternal_m2784275415 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___verts0, Il2CppObject * ___positions1, Il2CppObject * ___colors2, Il2CppObject * ___uv0S3, Il2CppObject * ___uv1S4, Il2CppObject * ___normals5, Il2CppObject * ___tangents6, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.CanvasRenderer::SplitIndiciesStreamsInternal(System.Object,System.Object)
extern "C"  void CanvasRenderer_SplitIndiciesStreamsInternal_m2583413468 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___verts0, Il2CppObject * ___indicies1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.CanvasRenderer::CreateUIVertexStreamInternal(System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object)
extern "C"  void CanvasRenderer_CreateUIVertexStreamInternal_m338533840 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___verts0, Il2CppObject * ___positions1, Il2CppObject * ___colors2, Il2CppObject * ___uv0S3, Il2CppObject * ___uv1S4, Il2CppObject * ___normals5, Il2CppObject * ___tangents6, Il2CppObject * ___indicies7, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Rigidbody UnityEngine.Collision::get_rigidbody()
extern "C"  Rigidbody_t3916780224 * Collision_get_rigidbody_m452774238 (Collision_t4262080450 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Object::op_Inequality(UnityEngine.Object,UnityEngine.Object)
extern "C"  bool Object_op_Inequality_m1920811489 (Il2CppObject * __this /* static, unused */, Object_t631007953 * ___x0, Object_t631007953 * ___y1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform UnityEngine.Component::get_transform()
extern "C"  Transform_t3600365921 * Component_get_transform_m2921103810 (Component_t1923634451 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Collider UnityEngine.Collision::get_collider()
extern "C"  Collider_t1773347010 * Collision_get_collider_m4225178406 (Collision_t4262080450 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject UnityEngine.Component::get_gameObject()
extern "C"  GameObject_t1113636619 * Component_get_gameObject_m2648350745 (Component_t1923634451 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.ContactPoint[] UnityEngine.Collision::get_contacts()
extern "C"  ContactPointU5BU5D_t872956888* Collision_get_contacts_m3545214732 (Collision_t4262080450 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator System.Array::GetEnumerator()
extern "C"  Il2CppObject * Array_GetEnumerator_m4277730612 (Il2CppArray * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Collision::get_relativeVelocity()
extern "C"  Vector3_t3722313464  Collision_get_relativeVelocity_m1615412340 (Collision_t4262080450 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Vector3::get_zero()
extern "C"  Vector3_t3722313464  Vector3_get_zero_m1640475482 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Collider2D UnityEngine.Physics2D::GetColliderFromInstanceID(System.Int32)
extern "C"  Collider2D_t2806799626 * Physics2D_GetColliderFromInstanceID_m2016026012 (Il2CppObject * __this /* static, unused */, int32_t ___instanceID0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Rigidbody2D UnityEngine.Physics2D::GetRigidbodyFromInstanceID(System.Int32)
extern "C"  Rigidbody2D_t939494601 * Physics2D_GetRigidbodyFromInstanceID_m3174084804 (Il2CppObject * __this /* static, unused */, int32_t ___instanceID0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Rigidbody2D UnityEngine.Collision2D::get_rigidbody()
extern "C"  Rigidbody2D_t939494601 * Collision2D_get_rigidbody_m3300210124 (Collision2D_t2842956331 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Collider2D UnityEngine.Collision2D::get_collider()
extern "C"  Collider2D_t2806799626 * Collision2D_get_collider_m3539891938 (Collision2D_t2842956331 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Color::.ctor(System.Single,System.Single,System.Single,System.Single)
extern "C"  void Color__ctor_m3057542999 (Color_t2555686324 * __this, float ___r0, float ___g1, float ___b2, float ___a3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Color::.ctor(System.Single,System.Single,System.Single)
extern "C"  void Color__ctor_m1673432820 (Color_t2555686324 * __this, float ___r0, float ___g1, float ___b2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.Color::ToString()
extern "C"  String_t* Color_ToString_m3373877040 (Color_t2555686324 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector4 UnityEngine.Color::op_Implicit(UnityEngine.Color)
extern "C"  Vector4_t3319028937  Color_op_Implicit_m1316618015 (Il2CppObject * __this /* static, unused */, Color_t2555686324  ___c0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Vector4::GetHashCode()
extern "C"  int32_t Vector4_GetHashCode_m2582833348 (Vector4_t3319028937 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Color::GetHashCode()
extern "C"  int32_t Color_GetHashCode_m3613701643 (Color_t2555686324 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Single::Equals(System.Single)
extern "C"  bool Single_Equals_m1601893879 (float* __this, float p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Color::Equals(System.Object)
extern "C"  bool Color_Equals_m325780748 (Color_t2555686324 * __this, Il2CppObject * ___other0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Vector4::op_Equality(UnityEngine.Vector4,UnityEngine.Vector4)
extern "C"  bool Vector4_op_Equality_m2731225992 (Il2CppObject * __this /* static, unused */, Vector4_t3319028937  ___lhs0, Vector4_t3319028937  ___rhs1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Mathf::Clamp01(System.Single)
extern "C"  float Mathf_Clamp01_m4133291925 (Il2CppObject * __this /* static, unused */, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Vector4::.ctor(System.Single,System.Single,System.Single,System.Single)
extern "C"  void Vector4__ctor_m1702261405 (Vector4_t3319028937 * __this, float ___x0, float ___y1, float ___z2, float ___w3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Color32::.ctor(System.Byte,System.Byte,System.Byte,System.Byte)
extern "C"  void Color32__ctor_m655413850 (Color32_t2600501292 * __this, uint8_t ___r0, uint8_t ___g1, uint8_t ___b2, uint8_t ___a3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.Color32::ToString()
extern "C"  String_t* Color32_ToString_m908866355 (Color32_t2600501292 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Object::.ctor()
extern "C"  void Object__ctor_m1560822313 (Object_t631007953 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Component UnityEngine.GameObject::GetComponent(System.Type)
extern "C"  Component_t1923634451 * GameObject_GetComponent_m1928525411 (GameObject_t1113636619 * __this, Type_t * ___type0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Component UnityEngine.GameObject::GetComponentInChildren(System.Type,System.Boolean)
extern "C"  Component_t1923634451 * GameObject_GetComponentInChildren_m230563287 (GameObject_t1113636619 * __this, Type_t * ___type0, bool ___includeInactive1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Component UnityEngine.Component::GetComponentInChildren(System.Type,System.Boolean)
extern "C"  Component_t1923634451 * Component_GetComponentInChildren_m2139176229 (Component_t1923634451 * __this, Type_t * ___t0, bool ___includeInactive1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Component[] UnityEngine.Component::GetComponentsInChildren(System.Type,System.Boolean)
extern "C"  ComponentU5BU5D_t3294940482* Component_GetComponentsInChildren_m1870572916 (Component_t1923634451 * __this, Type_t * ___t0, bool ___includeInactive1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Component[] UnityEngine.GameObject::GetComponentsInChildren(System.Type,System.Boolean)
extern "C"  ComponentU5BU5D_t3294940482* GameObject_GetComponentsInChildren_m479703998 (GameObject_t1113636619 * __this, Type_t * ___type0, bool ___includeInactive1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Component UnityEngine.GameObject::GetComponentInParent(System.Type)
extern "C"  Component_t1923634451 * GameObject_GetComponentInParent_m2794983654 (GameObject_t1113636619 * __this, Type_t * ___type0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Component[] UnityEngine.Component::GetComponentsInParent(System.Type,System.Boolean)
extern "C"  ComponentU5BU5D_t3294940482* Component_GetComponentsInParent_m2629981626 (Component_t1923634451 * __this, Type_t * ___t0, bool ___includeInactive1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Component[] UnityEngine.GameObject::GetComponentsInParent(System.Type,System.Boolean)
extern "C"  ComponentU5BU5D_t3294940482* GameObject_GetComponentsInParent_m4080244100 (GameObject_t1113636619 * __this, Type_t * ___type0, bool ___includeInactive1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Component[] UnityEngine.GameObject::GetComponents(System.Type)
extern "C"  ComponentU5BU5D_t3294940482* GameObject_GetComponents_m3948232783 (GameObject_t1113636619 * __this, Type_t * ___type0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Component::GetComponentsForListInternal(System.Type,System.Object)
extern "C"  void Component_GetComponentsForListInternal_m1978301288 (Component_t1923634451 * __this, Type_t * ___searchType0, Il2CppObject * ___resultList1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.GameObject::get_tag()
extern "C"  String_t* GameObject_get_tag_m778614508 (GameObject_t1113636619 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GameObject::set_tag(System.String)
extern "C"  void GameObject_set_tag_m551092519 (GameObject_t1113636619 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Component::SendMessageUpwards(System.String,System.Object,UnityEngine.SendMessageOptions)
extern "C"  void Component_SendMessageUpwards_m3320529579 (Component_t1923634451 * __this, String_t* ___methodName0, Il2CppObject * ___value1, int32_t ___options2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Component::SendMessage(System.String,System.Object,UnityEngine.SendMessageOptions)
extern "C"  void Component_SendMessage_m2000862420 (Component_t1923634451 * __this, String_t* ___methodName0, Il2CppObject * ___value1, int32_t ___options2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Component::BroadcastMessage(System.String,System.Object,UnityEngine.SendMessageOptions)
extern "C"  void Component_BroadcastMessage_m3615836039 (Component_t1923634451 * __this, String_t* ___methodName0, Il2CppObject * ___parameter1, int32_t ___options2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Single::IsNaN(System.Single)
extern "C"  bool Single_IsNaN_m4024467661 (Il2CppObject * __this /* static, unused */, float p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Mathf::Clamp(System.Single,System.Single,System.Single)
extern "C"  float Mathf_Clamp_m500974738 (Il2CppObject * __this /* static, unused */, float ___value0, float ___min1, float ___max2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ContactFilter2D::CheckConsistency()
extern "C"  void ContactFilter2D_CheckConsistency_m1930104761 (ContactFilter2D_t3805203441 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ContactFilter2D::SetLayerMask(UnityEngine.LayerMask)
extern "C"  void ContactFilter2D_SetLayerMask_m3854128592 (ContactFilter2D_t3805203441 * __this, LayerMask_t3493934918  ___layerMask0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ContactFilter2D::SetDepth(System.Single,System.Single)
extern "C"  void ContactFilter2D_SetDepth_m956990602 (ContactFilter2D_t3805203441 * __this, float ___minDepth0, float ___maxDepth1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Physics2D::get_queriesHitTriggers()
extern "C"  bool Physics2D_get_queriesHitTriggers_m3312014212 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.LayerMask UnityEngine.LayerMask::op_Implicit(System.Int32)
extern "C"  LayerMask_t3493934918  LayerMask_op_Implicit_m1727876484 (Il2CppObject * __this /* static, unused */, int32_t ___intVal0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ContextMenu::.ctor(System.String,System.Boolean)
extern "C"  void ContextMenu__ctor_m4148345090 (ContextMenu_t1295656858 * __this, String_t* ___itemName0, bool ___isValidateFunction1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ContextMenu::.ctor(System.String,System.Boolean,System.Int32)
extern "C"  void ContextMenu__ctor_m906931878 (ContextMenu_t1295656858 * __this, String_t* ___itemName0, bool ___isValidateFunction1, int32_t ___priority2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Rigidbody UnityEngine.Collider::get_attachedRigidbody()
extern "C"  Rigidbody_t3916780224 * Collider_get_attachedRigidbody_m4203494256 (Collider_t1773347010 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Coroutine::ReleaseCoroutine()
extern "C"  void Coroutine_ReleaseCoroutine_m1341336641 (Coroutine_t3829159415 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.IntPtr::op_Inequality(System.IntPtr,System.IntPtr)
extern "C"  bool IntPtr_op_Inequality_m3063970704 (Il2CppObject * __this /* static, unused */, IntPtr_t p0, IntPtr_t p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.CullingGroup::FinalizerFailure()
extern "C"  void CullingGroup_FinalizerFailure_m1971354910 (CullingGroup_t2096318768 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void* System.IntPtr::ToPointer()
extern "C"  void* IntPtr_ToPointer_m4157623054 (IntPtr_t* __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.CullingGroup/StateChanged::Invoke(UnityEngine.CullingGroupEvent)
extern "C"  void StateChanged_Invoke_m899175134 (StateChanged_t2136737110 * __this, CullingGroupEvent_t1722745023  ___sphere0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.ILogger UnityEngine.Debug::get_logger()
extern "C"  Il2CppObject * Debug_get_logger_m1300688786 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.DebugLogHandler::.ctor()
extern "C"  void DebugLogHandler__ctor_m1011039465 (DebugLogHandler_t826086171 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Logger::.ctor(UnityEngine.ILogHandler)
extern "C"  void Logger__ctor_m2177534208 (Logger_t274032455 * __this, Il2CppObject * ___logHandler0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.String::Format(System.String,System.Object[])
extern "C"  String_t* String_Format_m630303134 (Il2CppObject * __this /* static, unused */, String_t* p0, ObjectU5BU5D_t2843939325* p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.DebugLogHandler::Internal_Log(UnityEngine.LogType,System.String,UnityEngine.Object)
extern "C"  void DebugLogHandler_Internal_Log_m1402568625 (Il2CppObject * __this /* static, unused */, int32_t ___level0, String_t* ___msg1, Object_t631007953 * ___obj2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.DebugLogHandler::Internal_LogException(System.Exception,UnityEngine.Object)
extern "C"  void DebugLogHandler_Internal_LogException_m2644579063 (Il2CppObject * __this /* static, unused */, Exception_t1436737249 * ___exception0, Object_t631007953 * ___obj1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.IntPtr::.ctor(System.Int32)
extern "C"  void IntPtr__ctor_m987082960 (IntPtr_t* __this, int32_t p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Display::GetRenderingExtImpl(System.IntPtr,System.Int32&,System.Int32&)
extern "C"  void Display_GetRenderingExtImpl_m545713866 (Il2CppObject * __this /* static, unused */, IntPtr_t ___nativeDisplay0, int32_t* ___w1, int32_t* ___h2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Display::GetSystemExtImpl(System.IntPtr,System.Int32&,System.Int32&)
extern "C"  void Display_GetSystemExtImpl_m1142438950 (Il2CppObject * __this /* static, unused */, IntPtr_t ___nativeDisplay0, int32_t* ___w1, int32_t* ___h2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Display::RelativeMouseAtImpl(System.Int32,System.Int32,System.Int32&,System.Int32&)
extern "C"  int32_t Display_RelativeMouseAtImpl_m375710204 (Il2CppObject * __this /* static, unused */, int32_t ___x0, int32_t ___y1, int32_t* ___rx2, int32_t* ___ry3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Display::.ctor(System.IntPtr)
extern "C"  void Display__ctor_m2273944945 (Display_t1387065949 * __this, IntPtr_t ___nativeDisplay0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Display/DisplaysUpdatedDelegate::Invoke()
extern "C"  void DisplaysUpdatedDelegate_Invoke_m3670286313 (DisplaysUpdatedDelegate_t51287044 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Display::.ctor()
extern "C"  void Display__ctor_m2645739024 (Display_t1387065949 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.DrivenRectTransformTracker::Add(UnityEngine.Object,UnityEngine.RectTransform,UnityEngine.DrivenTransformProperties)
extern "C"  void DrivenRectTransformTracker_Add_m4180584832 (DrivenRectTransformTracker_t2562230146 * __this, Object_t631007953 * ___driver0, RectTransform_t3704657025 * ___rectTransform1, int32_t ___drivenProperties2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.DrivenRectTransformTracker::Clear()
extern "C"  void DrivenRectTransformTracker_Clear_m905140393 (DrivenRectTransformTracker_t2562230146 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Event::Init(System.Int32)
extern "C"  void Event_Init_m3976072405 (Event_t2956885303 * __this, int32_t ___displayIndex0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Event::Cleanup()
extern "C"  void Event_Cleanup_m1044305745 (Event_t2956885303 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Event::Internal_GetMousePosition(UnityEngine.Vector2&)
extern "C"  void Event_Internal_GetMousePosition_m2915150106 (Event_t2956885303 * __this, Vector2_t2156229523 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Event::.ctor(System.Int32)
extern "C"  void Event__ctor_m4084563805 (Event_t2956885303 * __this, int32_t ___displayIndex0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Event::set_displayIndex(System.Int32)
extern "C"  void Event_set_displayIndex_m1870302504 (Event_t2956885303 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Event::Internal_SetNativeEvent(System.IntPtr)
extern "C"  void Event_Internal_SetNativeEvent_m4190336570 (Il2CppObject * __this /* static, unused */, IntPtr_t ___ptr0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.EventType UnityEngine.Event::get_type()
extern "C"  int32_t Event_get_type_m1523589438 (Event_t2956885303 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Event::get_isKey()
extern "C"  bool Event_get_isKey_m2009856615 (Event_t2956885303 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.KeyCode UnityEngine.Event::get_keyCode()
extern "C"  int32_t Event_get_keyCode_m3804184495 (Event_t2956885303 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Event::get_isMouse()
extern "C"  bool Event_get_isMouse_m598780014 (Event_t2956885303 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.Event::get_mousePosition()
extern "C"  Vector2_t2156229523  Event_get_mousePosition_m860577707 (Event_t2956885303 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Vector2::GetHashCode()
extern "C"  int32_t Vector2_GetHashCode_m2690926524 (Vector2_t2156229523 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.EventModifiers UnityEngine.Event::get_modifiers()
extern "C"  int32_t Event_get_modifiers_m2201630428 (Event_t2956885303 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Object::ReferenceEquals(System.Object,System.Object)
extern "C"  bool Object_ReferenceEquals_m610702577 (Il2CppObject * __this /* static, unused */, Il2CppObject * p0, Il2CppObject * p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type System.Object::GetType()
extern "C"  Type_t * Object_GetType_m88164663 (Il2CppObject * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Vector2::op_Equality(UnityEngine.Vector2,UnityEngine.Vector2)
extern "C"  bool Vector2_op_Equality_m3476359499 (Il2CppObject * __this /* static, unused */, Vector2_t2156229523  ___lhs0, Vector2_t2156229523  ___rhs1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Char UnityEngine.Event::get_character()
extern "C"  Il2CppChar Event_get_character_m2243045319 (Event_t2956885303 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.String::Concat(System.Object[])
extern "C"  String_t* String_Concat_m2971454694 (Il2CppObject * __this /* static, unused */, ObjectU5BU5D_t2843939325* p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.Event::get_commandName()
extern "C"  String_t* Event_get_commandName_m3342234398 (Event_t2956885303 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.String::Concat(System.Object,System.Object)
extern "C"  String_t* String_Concat_m904156431 (Il2CppObject * __this /* static, unused */, Il2CppObject * p0, Il2CppObject * p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.String::IsNullOrEmpty(System.String)
extern "C"  bool String_IsNullOrEmpty_m2969720369 (Il2CppObject * __this /* static, unused */, String_t* p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.String::IndexOf(System.String)
extern "C"  int32_t String_IndexOf_m1977622757 (String_t* __this, String_t* p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Math::Min(System.Int32,System.Int32)
extern "C"  int32_t Math_Min_m3468062251 (Il2CppObject * __this /* static, unused */, int32_t p0, int32_t p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.String::Substring(System.Int32,System.Int32)
extern "C"  String_t* String_Substring_m1610150815 (String_t* __this, int32_t p0, int32_t p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Events.ArgumentCache::TidyAssemblyTypeName()
extern "C"  void ArgumentCache_TidyAssemblyTypeName_m1203759352 (ArgumentCache_t2187958399 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.ArgumentNullException::.ctor(System.String)
extern "C"  void ArgumentNullException__ctor_m1170824041 (ArgumentNullException_t1615371798 * __this, String_t* p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Delegate::get_Target()
extern "C"  Il2CppObject * Delegate_get_Target_m2361978888 (Delegate_t1188392813 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Events.BaseInvokableCall::.ctor(System.Object,System.Reflection.MethodInfo)
extern "C"  void BaseInvokableCall__ctor_m1036035291 (BaseInvokableCall_t2703961024 * __this, Il2CppObject * ___target0, MethodInfo_t * ___function1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Delegate UnityEngineInternal.NetFxCoreExtensions::CreateDelegate(System.Reflection.MethodInfo,System.Type,System.Object)
extern "C"  Delegate_t1188392813 * NetFxCoreExtensions_CreateDelegate_m4283065679 (Il2CppObject * __this /* static, unused */, MethodInfo_t * ___self0, Type_t * ___delegateType1, Il2CppObject * ___target2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Events.InvokableCall::add_Delegate(UnityEngine.Events.UnityAction)
extern "C"  void InvokableCall_add_Delegate_m2530991106 (InvokableCall_t832123510 * __this, UnityAction_t3245792599 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Events.BaseInvokableCall::.ctor()
extern "C"  void BaseInvokableCall__ctor_m3648827834 (BaseInvokableCall_t2703961024 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Events.BaseInvokableCall::AllowInvoke(System.Delegate)
extern "C"  bool BaseInvokableCall_AllowInvoke_m1250801794 (Il2CppObject * __this /* static, unused */, Delegate_t1188392813 * ___delegate0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Events.UnityAction::Invoke()
extern "C"  void UnityAction_Invoke_m804868541 (UnityAction_t3245792599 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.MethodInfo UnityEngineInternal.NetFxCoreExtensions::GetMethodInfo(System.Delegate)
extern "C"  MethodInfo_t * NetFxCoreExtensions_GetMethodInfo_m3154548066 (Il2CppObject * __this /* static, unused */, Delegate_t1188392813 * ___self0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall>::.ctor()
#define List_1__ctor_m3206896509(__this, method) ((  void (*) (List_1_t4176035766 *, const MethodInfo*))List_1__ctor_m2321703786_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall>::Add(!0)
#define List_1_Add_m19707193(__this, p0, method) ((  void (*) (List_1_t4176035766 *, BaseInvokableCall_t2703961024 *, const MethodInfo*))List_1_Add_m3338814081_gshared)(__this, p0, method)
// !0 System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall>::get_Item(System.Int32)
#define List_1_get_Item_m2469781675(__this, p0, method) ((  BaseInvokableCall_t2703961024 * (*) (List_1_t4176035766 *, int32_t, const MethodInfo*))List_1_get_Item_m2287542950_gshared)(__this, p0, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall>::get_Count()
#define List_1_get_Count_m743730018(__this, method) ((  int32_t (*) (List_1_t4176035766 *, const MethodInfo*))List_1_get_Count_m2934127733_gshared)(__this, method)
// System.Void System.Predicate`1<UnityEngine.Events.BaseInvokableCall>::.ctor(System.Object,System.IntPtr)
#define Predicate_1__ctor_m3247287534(__this, p0, p1, method) ((  void (*) (Predicate_1_t3529255148 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Predicate_1__ctor_m327447107_gshared)(__this, p0, p1, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall>::RemoveAll(System.Predicate`1<!0>)
#define List_1_RemoveAll_m1762023412(__this, p0, method) ((  int32_t (*) (List_1_t4176035766 *, Predicate_1_t3529255148 *, const MethodInfo*))List_1_RemoveAll_m4292035398_gshared)(__this, p0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall>::Clear()
#define List_1_Clear_m3668089392(__this, method) ((  void (*) (List_1_t4176035766 *, const MethodInfo*))List_1_Clear_m3697625829_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall>::AddRange(System.Collections.Generic.IEnumerable`1<!0>)
#define List_1_AddRange_m976883154(__this, p0, method) ((  void (*) (List_1_t4176035766 *, Il2CppObject*, const MethodInfo*))List_1_AddRange_m3709462088_gshared)(__this, p0, method)
// System.Void UnityEngine.Events.ArgumentCache::.ctor()
extern "C"  void ArgumentCache__ctor_m1913080877 (ArgumentCache_t2187958399 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Object UnityEngine.Events.PersistentCall::get_target()
extern "C"  Object_t631007953 * PersistentCall_get_target_m2795613068 (PersistentCall_t3407714124 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.Events.PersistentCall::get_methodName()
extern "C"  String_t* PersistentCall_get_methodName_m2181722332 (PersistentCall_t3407714124 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.MethodInfo UnityEngine.Events.UnityEventBase::FindMethod(UnityEngine.Events.PersistentCall)
extern "C"  MethodInfo_t * UnityEventBase_FindMethod_m1055565284 (UnityEventBase_t3960448221 * __this, PersistentCall_t3407714124 * ___call0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Events.BaseInvokableCall UnityEngine.Events.PersistentCall::GetObjectCall(UnityEngine.Object,System.Reflection.MethodInfo,UnityEngine.Events.ArgumentCache)
extern "C"  BaseInvokableCall_t2703961024 * PersistentCall_GetObjectCall_m861446582 (Il2CppObject * __this /* static, unused */, Object_t631007953 * ___target0, MethodInfo_t * ___method1, ArgumentCache_t2187958399 * ___arguments2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Events.ArgumentCache::get_floatArgument()
extern "C"  float ArgumentCache_get_floatArgument_m2801316765 (ArgumentCache_t2187958399 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Events.CachedInvokableCall`1<System.Single>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
#define CachedInvokableCall_1__ctor_m2985117283(__this, p0, p1, p2, method) ((  void (*) (CachedInvokableCall_1_t3723462114 *, Object_t631007953 *, MethodInfo_t *, float, const MethodInfo*))CachedInvokableCall_1__ctor_m2985117283_gshared)(__this, p0, p1, p2, method)
// System.Int32 UnityEngine.Events.ArgumentCache::get_intArgument()
extern "C"  int32_t ArgumentCache_get_intArgument_m3815654797 (ArgumentCache_t2187958399 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Events.CachedInvokableCall`1<System.Int32>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
#define CachedInvokableCall_1__ctor_m3353156630(__this, p0, p1, p2, method) ((  void (*) (CachedInvokableCall_1_t982173797 *, Object_t631007953 *, MethodInfo_t *, int32_t, const MethodInfo*))CachedInvokableCall_1__ctor_m3353156630_gshared)(__this, p0, p1, p2, method)
// System.String UnityEngine.Events.ArgumentCache::get_stringArgument()
extern "C"  String_t* ArgumentCache_get_stringArgument_m581215363 (ArgumentCache_t2187958399 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Events.CachedInvokableCall`1<System.String>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
#define CachedInvokableCall_1__ctor_m2815878172(__this, p0, p1, p2, method) ((  void (*) (CachedInvokableCall_1_t4173646029 *, Object_t631007953 *, MethodInfo_t *, String_t*, const MethodInfo*))CachedInvokableCall_1__ctor_m2686176129_gshared)(__this, p0, p1, p2, method)
// System.Boolean UnityEngine.Events.ArgumentCache::get_boolArgument()
extern "C"  bool ArgumentCache_get_boolArgument_m4017632913 (ArgumentCache_t2187958399 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Events.CachedInvokableCall`1<System.Boolean>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
#define CachedInvokableCall_1__ctor_m3594541075(__this, p0, p1, p2, method) ((  void (*) (CachedInvokableCall_1_t2423483305 *, Object_t631007953 *, MethodInfo_t *, bool, const MethodInfo*))CachedInvokableCall_1__ctor_m3594541075_gshared)(__this, p0, p1, p2, method)
// System.Void UnityEngine.Events.InvokableCall::.ctor(System.Object,System.Reflection.MethodInfo)
extern "C"  void InvokableCall__ctor_m2864671469 (InvokableCall_t832123510 * __this, Il2CppObject * ___target0, MethodInfo_t * ___theFunction1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.Events.ArgumentCache::get_unityObjectArgumentAssemblyTypeName()
extern "C"  String_t* ArgumentCache_get_unityObjectArgumentAssemblyTypeName_m3286213661 (ArgumentCache_t2187958399 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type System.Type::GetType(System.String,System.Boolean)
extern "C"  Type_t * Type_GetType_m3605423543 (Il2CppObject * __this /* static, unused */, String_t* p0, bool p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.ConstructorInfo System.Type::GetConstructor(System.Type[])
extern "C"  ConstructorInfo_t5769829 * Type_GetConstructor_m2219014380 (Type_t * __this, TypeU5BU5D_t3940880105* p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Object UnityEngine.Events.ArgumentCache::get_unityObjectArgument()
extern "C"  Object_t631007953 * ArgumentCache_get_unityObjectArgument_m164429709 (ArgumentCache_t2187958399 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Reflection.ConstructorInfo::Invoke(System.Object[])
extern "C"  Il2CppObject * ConstructorInfo_Invoke_m4089836896 (ConstructorInfo_t5769829 * __this, ObjectU5BU5D_t2843939325* p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.List`1<UnityEngine.Events.PersistentCall>::.ctor()
#define List_1__ctor_m943763978(__this, method) ((  void (*) (List_1_t584821570 *, const MethodInfo*))List_1__ctor_m2321703786_gshared)(__this, method)
// System.Collections.Generic.List`1/Enumerator<!0> System.Collections.Generic.List`1<UnityEngine.Events.PersistentCall>::GetEnumerator()
#define List_1_GetEnumerator_m398210679(__this, method) ((  Enumerator_t2474065447  (*) (List_1_t584821570 *, const MethodInfo*))List_1_GetEnumerator_m2930774921_gshared)(__this, method)
// !0 System.Collections.Generic.List`1/Enumerator<UnityEngine.Events.PersistentCall>::get_Current()
#define Enumerator_get_Current_m481949260(__this, method) ((  PersistentCall_t3407714124 * (*) (Enumerator_t2474065447 *, const MethodInfo*))Enumerator_get_Current_m470245444_gshared)(__this, method)
// System.Boolean UnityEngine.Events.PersistentCall::IsValid()
extern "C"  bool PersistentCall_IsValid_m3557513049 (PersistentCall_t3407714124 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Events.BaseInvokableCall UnityEngine.Events.PersistentCall::GetRuntimeCall(UnityEngine.Events.UnityEventBase)
extern "C"  BaseInvokableCall_t2703961024 * PersistentCall_GetRuntimeCall_m606139440 (PersistentCall_t3407714124 * __this, UnityEventBase_t3960448221 * ___theEvent0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Events.InvokableCallList::AddPersistentInvokableCall(UnityEngine.Events.BaseInvokableCall)
extern "C"  void InvokableCallList_AddPersistentInvokableCall_m4020414894 (InvokableCallList_t2498835369 * __this, BaseInvokableCall_t2703961024 * ___call0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.Events.PersistentCall>::MoveNext()
#define Enumerator_MoveNext_m1429503053(__this, method) ((  bool (*) (Enumerator_t2474065447 *, const MethodInfo*))Enumerator_MoveNext_m2142368520_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Events.PersistentCall>::Dispose()
#define Enumerator_Dispose_m2540888223(__this, method) ((  void (*) (Enumerator_t2474065447 *, const MethodInfo*))Enumerator_Dispose_m3007748546_gshared)(__this, method)
// System.Void UnityEngine.Events.UnityEventBase::.ctor()
extern "C"  void UnityEventBase__ctor_m1087366165 (UnityEventBase_t3960448221 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Events.BaseInvokableCall UnityEngine.Events.UnityEvent::GetDelegate(UnityEngine.Events.UnityAction)
extern "C"  BaseInvokableCall_t2703961024 * UnityEvent_GetDelegate_m3910545044 (Il2CppObject * __this /* static, unused */, UnityAction_t3245792599 * ___action0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Events.UnityEventBase::AddCall(UnityEngine.Events.BaseInvokableCall)
extern "C"  void UnityEventBase_AddCall_m3632990696 (UnityEventBase_t3960448221 * __this, BaseInvokableCall_t2703961024 * ___call0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Events.UnityEventBase::RemoveListener(System.Object,System.Reflection.MethodInfo)
extern "C"  void UnityEventBase_RemoveListener_m748247232 (UnityEventBase_t3960448221 * __this, Il2CppObject * ___targetObj0, MethodInfo_t * ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.MethodInfo UnityEngine.Events.UnityEventBase::GetValidMethodInfo(System.Object,System.String,System.Type[])
extern "C"  MethodInfo_t * UnityEventBase_GetValidMethodInfo_m1761163145 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___obj0, String_t* ___functionName1, TypeU5BU5D_t3940880105* ___argumentTypes2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Events.InvokableCall::.ctor(UnityEngine.Events.UnityAction)
extern "C"  void InvokableCall__ctor_m616326848 (InvokableCall_t832123510 * __this, UnityAction_t3245792599 * ___action0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Events.UnityEventBase::Invoke(System.Object[])
extern "C"  void UnityEventBase_Invoke_m3022009851 (UnityEventBase_t3960448221 * __this, ObjectU5BU5D_t2843939325* ___parameters0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Events.InvokableCallList::.ctor()
extern "C"  void InvokableCallList__ctor_m2867488712 (InvokableCallList_t2498835369 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Events.PersistentCallGroup::.ctor()
extern "C"  void PersistentCallGroup__ctor_m1755284370 (PersistentCallGroup_t3050769227 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Events.UnityEventBase::DirtyPersistentCalls()
extern "C"  void UnityEventBase_DirtyPersistentCalls_m3787457104 (UnityEventBase_t3960448221 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Events.ArgumentCache UnityEngine.Events.PersistentCall::get_arguments()
extern "C"  ArgumentCache_t2187958399 * PersistentCall_get_arguments_m1263638726 (PersistentCall_t3407714124 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Events.PersistentListenerMode UnityEngine.Events.PersistentCall::get_mode()
extern "C"  int32_t PersistentCall_get_mode_m1368131393 (PersistentCall_t3407714124 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.MethodInfo UnityEngine.Events.UnityEventBase::FindMethod(System.String,System.Object,UnityEngine.Events.PersistentListenerMode,System.Type)
extern "C"  MethodInfo_t * UnityEventBase_FindMethod_m995544650 (UnityEventBase_t3960448221 * __this, String_t* ___name0, Il2CppObject * ___listener1, int32_t ___mode2, Type_t * ___argumentType3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Events.InvokableCallList::ClearPersistent()
extern "C"  void InvokableCallList_ClearPersistent_m3446223403 (InvokableCallList_t2498835369 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Events.PersistentCallGroup::Initialize(UnityEngine.Events.InvokableCallList,UnityEngine.Events.UnityEventBase)
extern "C"  void PersistentCallGroup_Initialize_m2614950721 (PersistentCallGroup_t3050769227 * __this, InvokableCallList_t2498835369 * ___invokableList0, UnityEventBase_t3960448221 * ___unityEventBase1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Events.InvokableCallList::AddListener(UnityEngine.Events.BaseInvokableCall)
extern "C"  void InvokableCallList_AddListener_m4292769145 (InvokableCallList_t2498835369 * __this, BaseInvokableCall_t2703961024 * ___call0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Events.InvokableCallList::RemoveListener(System.Object,System.Reflection.MethodInfo)
extern "C"  void InvokableCallList_RemoveListener_m1974000505 (InvokableCallList_t2498835369 * __this, Il2CppObject * ___targetObj0, MethodInfo_t * ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Events.UnityEventBase::RebuildPersistentCallsIfNeeded()
extern "C"  void UnityEventBase_RebuildPersistentCallsIfNeeded_m1560510243 (UnityEventBase_t3960448221 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Events.InvokableCallList::Invoke(System.Object[])
extern "C"  void InvokableCallList_Invoke_m2795703874 (InvokableCallList_t2498835369 * __this, ObjectU5BU5D_t2843939325* ___parameters0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Object::ToString()
extern "C"  String_t* Object_ToString_m1740002499 (Il2CppObject * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.String::Concat(System.String,System.String,System.String)
extern "C"  String_t* String_Concat_m3755062657 (Il2CppObject * __this /* static, unused */, String_t* p0, String_t* p1, String_t* p2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.MethodInfo System.Type::GetMethod(System.String,System.Reflection.BindingFlags,System.Reflection.Binder,System.Type[],System.Reflection.ParameterModifier[])
extern "C"  MethodInfo_t * Type_GetMethod_m637078096 (Type_t * __this, String_t* p0, int32_t p1, Binder_t2999457153 * p2, TypeU5BU5D_t3940880105* p3, ParameterModifierU5BU5D_t2943407543* p4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Type::get_IsPrimitive()
extern "C"  bool Type_get_IsPrimitive_m1114712797 (Type_t * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Experimental.Director.AnimationPlayable::.ctor()
extern "C"  void AnimationPlayable__ctor_m1573048877 (AnimationPlayable_t2436992687 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.AnimationClip UnityEngine.Experimental.Director.AnimationClipPlayable::GetAnimationClip(UnityEngine.Experimental.Director.PlayableHandle&)
extern "C"  AnimationClip_t2318505987 * AnimationClipPlayable_GetAnimationClip_m3645124204 (Il2CppObject * __this /* static, unused */, PlayableHandle_t882318307 * ___handle0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Experimental.Director.AnimationClipPlayable::GetSpeed(UnityEngine.Experimental.Director.PlayableHandle&)
extern "C"  float AnimationClipPlayable_GetSpeed_m2357489435 (Il2CppObject * __this /* static, unused */, PlayableHandle_t882318307 * ___handle0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Experimental.Director.AnimationClipPlayable::SetSpeed(UnityEngine.Experimental.Director.PlayableHandle&,System.Single)
extern "C"  void AnimationClipPlayable_SetSpeed_m403221215 (Il2CppObject * __this /* static, unused */, PlayableHandle_t882318307 * ___handle0, float ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Experimental.Director.AnimationClipPlayable::GetApplyFootIK(UnityEngine.Experimental.Director.PlayableHandle&)
extern "C"  bool AnimationClipPlayable_GetApplyFootIK_m2625007350 (Il2CppObject * __this /* static, unused */, PlayableHandle_t882318307 * ___handle0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Experimental.Director.AnimationClipPlayable::SetApplyFootIK(UnityEngine.Experimental.Director.PlayableHandle&,System.Boolean)
extern "C"  void AnimationClipPlayable_SetApplyFootIK_m3447148980 (Il2CppObject * __this /* static, unused */, PlayableHandle_t882318307 * ___handle0, bool ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Experimental.Director.AnimationClipPlayable::GetRemoveStartOffset(UnityEngine.Experimental.Director.PlayableHandle&)
extern "C"  bool AnimationClipPlayable_GetRemoveStartOffset_m212565688 (Il2CppObject * __this /* static, unused */, PlayableHandle_t882318307 * ___handle0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Experimental.Director.AnimationClipPlayable::SetRemoveStartOffset(UnityEngine.Experimental.Director.PlayableHandle&,System.Boolean)
extern "C"  void AnimationClipPlayable_SetRemoveStartOffset_m806931619 (Il2CppObject * __this /* static, unused */, PlayableHandle_t882318307 * ___handle0, bool ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.AnimationClip UnityEngine.Experimental.Director.AnimationClipPlayable::INTERNAL_CALL_GetAnimationClip(UnityEngine.Experimental.Director.PlayableHandle&)
extern "C"  AnimationClip_t2318505987 * AnimationClipPlayable_INTERNAL_CALL_GetAnimationClip_m2280195531 (Il2CppObject * __this /* static, unused */, PlayableHandle_t882318307 * ___handle0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Experimental.Director.AnimationClipPlayable::INTERNAL_CALL_GetSpeed(UnityEngine.Experimental.Director.PlayableHandle&)
extern "C"  float AnimationClipPlayable_INTERNAL_CALL_GetSpeed_m2906631008 (Il2CppObject * __this /* static, unused */, PlayableHandle_t882318307 * ___handle0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Experimental.Director.AnimationClipPlayable::INTERNAL_CALL_SetSpeed(UnityEngine.Experimental.Director.PlayableHandle&,System.Single)
extern "C"  void AnimationClipPlayable_INTERNAL_CALL_SetSpeed_m1493580406 (Il2CppObject * __this /* static, unused */, PlayableHandle_t882318307 * ___handle0, float ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Experimental.Director.AnimationClipPlayable::INTERNAL_CALL_GetApplyFootIK(UnityEngine.Experimental.Director.PlayableHandle&)
extern "C"  bool AnimationClipPlayable_INTERNAL_CALL_GetApplyFootIK_m1802268754 (Il2CppObject * __this /* static, unused */, PlayableHandle_t882318307 * ___handle0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Experimental.Director.AnimationClipPlayable::INTERNAL_CALL_SetApplyFootIK(UnityEngine.Experimental.Director.PlayableHandle&,System.Boolean)
extern "C"  void AnimationClipPlayable_INTERNAL_CALL_SetApplyFootIK_m2254986524 (Il2CppObject * __this /* static, unused */, PlayableHandle_t882318307 * ___handle0, bool ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Experimental.Director.AnimationClipPlayable::INTERNAL_CALL_GetRemoveStartOffset(UnityEngine.Experimental.Director.PlayableHandle&)
extern "C"  bool AnimationClipPlayable_INTERNAL_CALL_GetRemoveStartOffset_m1179097678 (Il2CppObject * __this /* static, unused */, PlayableHandle_t882318307 * ___handle0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Experimental.Director.AnimationClipPlayable::INTERNAL_CALL_SetRemoveStartOffset(UnityEngine.Experimental.Director.PlayableHandle&,System.Boolean)
extern "C"  void AnimationClipPlayable_INTERNAL_CALL_SetRemoveStartOffset_m3644572681 (Il2CppObject * __this /* static, unused */, PlayableHandle_t882318307 * ___handle0, bool ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UnityEngine.AddComponentMenu::.ctor(System.String)
extern "C"  void AddComponentMenu__ctor_m3560455134 (AddComponentMenu_t415040132 * __this, String_t* ___menuName0, const MethodInfo* method)
{
	{
		Attribute__ctor_m1529526131(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___menuName0;
		__this->set_m_AddComponentMenu_0(L_0);
		__this->set_m_Ordering_1(0);
		return;
	}
}
// System.Void UnityEngine.AddComponentMenu::.ctor(System.String,System.Int32)
extern "C"  void AddComponentMenu__ctor_m2783488291 (AddComponentMenu_t415040132 * __this, String_t* ___menuName0, int32_t ___order1, const MethodInfo* method)
{
	{
		Attribute__ctor_m1529526131(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___menuName0;
		__this->set_m_AddComponentMenu_0(L_0);
		int32_t L_1 = ___order1;
		__this->set_m_Ordering_1(L_1);
		return;
	}
}
// System.Void UnityEngine.AI.NavMesh::Internal_CallOnNavMeshPreUpdate()
extern "C"  void NavMesh_Internal_CallOnNavMeshPreUpdate_m3207184993 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (NavMesh_Internal_CallOnNavMeshPreUpdate_m3207184993_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		OnNavMeshPreUpdate_t1580782682 * L_0 = ((NavMesh_t1865600375_StaticFields*)NavMesh_t1865600375_il2cpp_TypeInfo_var->static_fields)->get_onPreUpdate_0();
		if (!L_0)
		{
			goto IL_0015;
		}
	}
	{
		OnNavMeshPreUpdate_t1580782682 * L_1 = ((NavMesh_t1865600375_StaticFields*)NavMesh_t1865600375_il2cpp_TypeInfo_var->static_fields)->get_onPreUpdate_0();
		NullCheck(L_1);
		OnNavMeshPreUpdate_Invoke_m2083125449(L_1, /*hidden argument*/NULL);
	}

IL_0015:
	{
		return;
	}
}
extern "C"  void DelegatePInvokeWrapper_OnNavMeshPreUpdate_t1580782682 (OnNavMeshPreUpdate_t1580782682 * __this, const MethodInfo* method)
{
	typedef void (STDCALL *PInvokeFunc)();
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(((Il2CppDelegate*)__this)->method->methodPointer);

	// Native function invocation
	il2cppPInvokeFunc();

}
// System.Void UnityEngine.AI.NavMesh/OnNavMeshPreUpdate::.ctor(System.Object,System.IntPtr)
extern "C"  void OnNavMeshPreUpdate__ctor_m1734452209 (OnNavMeshPreUpdate_t1580782682 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void UnityEngine.AI.NavMesh/OnNavMeshPreUpdate::Invoke()
extern "C"  void OnNavMeshPreUpdate_Invoke_m2083125449 (OnNavMeshPreUpdate_t1580782682 * __this, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		OnNavMeshPreUpdate_Invoke_m2083125449((OnNavMeshPreUpdate_t1580782682 *)__this->get_prev_9(), method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if ((__this->get_m_target_2() != NULL || MethodHasParameters((MethodInfo*)(__this->get_method_3().get_m_value_0()))) && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult UnityEngine.AI.NavMesh/OnNavMeshPreUpdate::BeginInvoke(System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * OnNavMeshPreUpdate_BeginInvoke_m1924280924 (OnNavMeshPreUpdate_t1580782682 * __this, AsyncCallback_t3962456242 * ___callback0, Il2CppObject * ___object1, const MethodInfo* method)
{
	void *__d_args[1] = {0};
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback0, (Il2CppObject*)___object1);
}
// System.Void UnityEngine.AI.NavMesh/OnNavMeshPreUpdate::EndInvoke(System.IAsyncResult)
extern "C"  void OnNavMeshPreUpdate_EndInvoke_m3032730014 (OnNavMeshPreUpdate_t1580782682 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// Conversion methods for marshalling of: UnityEngine.AnimationCurve
extern "C" void AnimationCurve_t3046754366_marshal_pinvoke(const AnimationCurve_t3046754366& unmarshaled, AnimationCurve_t3046754366_marshaled_pinvoke& marshaled)
{
	marshaled.___m_Ptr_0 = reinterpret_cast<intptr_t>((unmarshaled.get_m_Ptr_0()).get_m_value_0());
}
extern "C" void AnimationCurve_t3046754366_marshal_pinvoke_back(const AnimationCurve_t3046754366_marshaled_pinvoke& marshaled, AnimationCurve_t3046754366& unmarshaled)
{
	IntPtr_t unmarshaled_m_Ptr_temp_0;
	memset(&unmarshaled_m_Ptr_temp_0, 0, sizeof(unmarshaled_m_Ptr_temp_0));
	IntPtr_t unmarshaled_m_Ptr_temp_0_temp;
	unmarshaled_m_Ptr_temp_0_temp.set_m_value_0(reinterpret_cast<void*>((intptr_t)(marshaled.___m_Ptr_0)));
	unmarshaled_m_Ptr_temp_0 = unmarshaled_m_Ptr_temp_0_temp;
	unmarshaled.set_m_Ptr_0(unmarshaled_m_Ptr_temp_0);
}
// Conversion method for clean up from marshalling of: UnityEngine.AnimationCurve
extern "C" void AnimationCurve_t3046754366_marshal_pinvoke_cleanup(AnimationCurve_t3046754366_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.AnimationCurve
extern "C" void AnimationCurve_t3046754366_marshal_com(const AnimationCurve_t3046754366& unmarshaled, AnimationCurve_t3046754366_marshaled_com& marshaled)
{
	marshaled.___m_Ptr_0 = reinterpret_cast<intptr_t>((unmarshaled.get_m_Ptr_0()).get_m_value_0());
}
extern "C" void AnimationCurve_t3046754366_marshal_com_back(const AnimationCurve_t3046754366_marshaled_com& marshaled, AnimationCurve_t3046754366& unmarshaled)
{
	IntPtr_t unmarshaled_m_Ptr_temp_0;
	memset(&unmarshaled_m_Ptr_temp_0, 0, sizeof(unmarshaled_m_Ptr_temp_0));
	IntPtr_t unmarshaled_m_Ptr_temp_0_temp;
	unmarshaled_m_Ptr_temp_0_temp.set_m_value_0(reinterpret_cast<void*>((intptr_t)(marshaled.___m_Ptr_0)));
	unmarshaled_m_Ptr_temp_0 = unmarshaled_m_Ptr_temp_0_temp;
	unmarshaled.set_m_Ptr_0(unmarshaled_m_Ptr_temp_0);
}
// Conversion method for clean up from marshalling of: UnityEngine.AnimationCurve
extern "C" void AnimationCurve_t3046754366_marshal_com_cleanup(AnimationCurve_t3046754366_marshaled_com& marshaled)
{
}
// System.Void UnityEngine.AnimationCurve::.ctor(UnityEngine.Keyframe[])
extern "C"  void AnimationCurve__ctor_m1726807957 (AnimationCurve_t3046754366 * __this, KeyframeU5BU5D_t1068524471* ___keys0, const MethodInfo* method)
{
	{
		Object__ctor_m297566312(__this, /*hidden argument*/NULL);
		KeyframeU5BU5D_t1068524471* L_0 = ___keys0;
		AnimationCurve_Init_m1115968024(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.AnimationCurve::.ctor()
extern "C"  void AnimationCurve__ctor_m1499663178 (AnimationCurve_t3046754366 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m297566312(__this, /*hidden argument*/NULL);
		AnimationCurve_Init_m1115968024(__this, (KeyframeU5BU5D_t1068524471*)(KeyframeU5BU5D_t1068524471*)NULL, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.AnimationCurve::Cleanup()
extern "C"  void AnimationCurve_Cleanup_m3505736496 (AnimationCurve_t3046754366 * __this, const MethodInfo* method)
{
	typedef void (*AnimationCurve_Cleanup_m3505736496_ftn) (AnimationCurve_t3046754366 *);
	static AnimationCurve_Cleanup_m3505736496_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AnimationCurve_Cleanup_m3505736496_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AnimationCurve::Cleanup()");
	_il2cpp_icall_func(__this);
}
// System.Void UnityEngine.AnimationCurve::Finalize()
extern "C"  void AnimationCurve_Finalize_m719965173 (AnimationCurve_t3046754366 * __this, const MethodInfo* method)
{
	Exception_t1436737249 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1436737249 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
	}

IL_0001:
	try
	{ // begin try (depth: 1)
		AnimationCurve_Cleanup_m3505736496(__this, /*hidden argument*/NULL);
		IL2CPP_LEAVE(0x13, FINALLY_000c);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1436737249 *)e.ex;
		goto FINALLY_000c;
	}

FINALLY_000c:
	{ // begin finally (depth: 1)
		Object_Finalize_m3076187857(__this, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(12)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(12)
	{
		IL2CPP_JUMP_TBL(0x13, IL_0013)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1436737249 *)
	}

IL_0013:
	{
		return;
	}
}
// System.Single UnityEngine.AnimationCurve::Evaluate(System.Single)
extern "C"  float AnimationCurve_Evaluate_m2577673347 (AnimationCurve_t3046754366 * __this, float ___time0, const MethodInfo* method)
{
	typedef float (*AnimationCurve_Evaluate_m2577673347_ftn) (AnimationCurve_t3046754366 *, float);
	static AnimationCurve_Evaluate_m2577673347_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AnimationCurve_Evaluate_m2577673347_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AnimationCurve::Evaluate(System.Single)");
	return _il2cpp_icall_func(__this, ___time0);
}
// UnityEngine.Keyframe[] UnityEngine.AnimationCurve::get_keys()
extern "C"  KeyframeU5BU5D_t1068524471* AnimationCurve_get_keys_m3622248237 (AnimationCurve_t3046754366 * __this, const MethodInfo* method)
{
	KeyframeU5BU5D_t1068524471* V_0 = NULL;
	{
		KeyframeU5BU5D_t1068524471* L_0 = AnimationCurve_GetKeys_m2150394447(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		KeyframeU5BU5D_t1068524471* L_1 = V_0;
		return L_1;
	}
}
// System.Void UnityEngine.AnimationCurve::set_keys(UnityEngine.Keyframe[])
extern "C"  void AnimationCurve_set_keys_m3462789338 (AnimationCurve_t3046754366 * __this, KeyframeU5BU5D_t1068524471* ___value0, const MethodInfo* method)
{
	{
		KeyframeU5BU5D_t1068524471* L_0 = ___value0;
		AnimationCurve_SetKeys_m121116590(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Int32 UnityEngine.AnimationCurve::AddKey(UnityEngine.Keyframe)
extern "C"  int32_t AnimationCurve_AddKey_m4050017270 (AnimationCurve_t3046754366 * __this, Keyframe_t4206410242  ___key0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		Keyframe_t4206410242  L_0 = ___key0;
		int32_t L_1 = AnimationCurve_AddKey_Internal_m2661999051(__this, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_000e;
	}

IL_000e:
	{
		int32_t L_2 = V_0;
		return L_2;
	}
}
// System.Int32 UnityEngine.AnimationCurve::AddKey_Internal(UnityEngine.Keyframe)
extern "C"  int32_t AnimationCurve_AddKey_Internal_m2661999051 (AnimationCurve_t3046754366 * __this, Keyframe_t4206410242  ___key0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = AnimationCurve_INTERNAL_CALL_AddKey_Internal_m4197796724(NULL /*static, unused*/, __this, (&___key0), /*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_000f;
	}

IL_000f:
	{
		int32_t L_1 = V_0;
		return L_1;
	}
}
// System.Int32 UnityEngine.AnimationCurve::INTERNAL_CALL_AddKey_Internal(UnityEngine.AnimationCurve,UnityEngine.Keyframe&)
extern "C"  int32_t AnimationCurve_INTERNAL_CALL_AddKey_Internal_m4197796724 (Il2CppObject * __this /* static, unused */, AnimationCurve_t3046754366 * ___self0, Keyframe_t4206410242 * ___key1, const MethodInfo* method)
{
	typedef int32_t (*AnimationCurve_INTERNAL_CALL_AddKey_Internal_m4197796724_ftn) (AnimationCurve_t3046754366 *, Keyframe_t4206410242 *);
	static AnimationCurve_INTERNAL_CALL_AddKey_Internal_m4197796724_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AnimationCurve_INTERNAL_CALL_AddKey_Internal_m4197796724_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AnimationCurve::INTERNAL_CALL_AddKey_Internal(UnityEngine.AnimationCurve,UnityEngine.Keyframe&)");
	return _il2cpp_icall_func(___self0, ___key1);
}
// System.Void UnityEngine.AnimationCurve::SetKeys(UnityEngine.Keyframe[])
extern "C"  void AnimationCurve_SetKeys_m121116590 (AnimationCurve_t3046754366 * __this, KeyframeU5BU5D_t1068524471* ___keys0, const MethodInfo* method)
{
	typedef void (*AnimationCurve_SetKeys_m121116590_ftn) (AnimationCurve_t3046754366 *, KeyframeU5BU5D_t1068524471*);
	static AnimationCurve_SetKeys_m121116590_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AnimationCurve_SetKeys_m121116590_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AnimationCurve::SetKeys(UnityEngine.Keyframe[])");
	_il2cpp_icall_func(__this, ___keys0);
}
// UnityEngine.Keyframe[] UnityEngine.AnimationCurve::GetKeys()
extern "C"  KeyframeU5BU5D_t1068524471* AnimationCurve_GetKeys_m2150394447 (AnimationCurve_t3046754366 * __this, const MethodInfo* method)
{
	typedef KeyframeU5BU5D_t1068524471* (*AnimationCurve_GetKeys_m2150394447_ftn) (AnimationCurve_t3046754366 *);
	static AnimationCurve_GetKeys_m2150394447_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AnimationCurve_GetKeys_m2150394447_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AnimationCurve::GetKeys()");
	return _il2cpp_icall_func(__this);
}
// UnityEngine.WrapMode UnityEngine.AnimationCurve::get_preWrapMode()
extern "C"  int32_t AnimationCurve_get_preWrapMode_m3510315011 (AnimationCurve_t3046754366 * __this, const MethodInfo* method)
{
	typedef int32_t (*AnimationCurve_get_preWrapMode_m3510315011_ftn) (AnimationCurve_t3046754366 *);
	static AnimationCurve_get_preWrapMode_m3510315011_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AnimationCurve_get_preWrapMode_m3510315011_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AnimationCurve::get_preWrapMode()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.AnimationCurve::set_preWrapMode(UnityEngine.WrapMode)
extern "C"  void AnimationCurve_set_preWrapMode_m2589217844 (AnimationCurve_t3046754366 * __this, int32_t ___value0, const MethodInfo* method)
{
	typedef void (*AnimationCurve_set_preWrapMode_m2589217844_ftn) (AnimationCurve_t3046754366 *, int32_t);
	static AnimationCurve_set_preWrapMode_m2589217844_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AnimationCurve_set_preWrapMode_m2589217844_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AnimationCurve::set_preWrapMode(UnityEngine.WrapMode)");
	_il2cpp_icall_func(__this, ___value0);
}
// UnityEngine.WrapMode UnityEngine.AnimationCurve::get_postWrapMode()
extern "C"  int32_t AnimationCurve_get_postWrapMode_m948083379 (AnimationCurve_t3046754366 * __this, const MethodInfo* method)
{
	typedef int32_t (*AnimationCurve_get_postWrapMode_m948083379_ftn) (AnimationCurve_t3046754366 *);
	static AnimationCurve_get_postWrapMode_m948083379_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AnimationCurve_get_postWrapMode_m948083379_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AnimationCurve::get_postWrapMode()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.AnimationCurve::set_postWrapMode(UnityEngine.WrapMode)
extern "C"  void AnimationCurve_set_postWrapMode_m1388181002 (AnimationCurve_t3046754366 * __this, int32_t ___value0, const MethodInfo* method)
{
	typedef void (*AnimationCurve_set_postWrapMode_m1388181002_ftn) (AnimationCurve_t3046754366 *, int32_t);
	static AnimationCurve_set_postWrapMode_m1388181002_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AnimationCurve_set_postWrapMode_m1388181002_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AnimationCurve::set_postWrapMode(UnityEngine.WrapMode)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.AnimationCurve::Init(UnityEngine.Keyframe[])
extern "C"  void AnimationCurve_Init_m1115968024 (AnimationCurve_t3046754366 * __this, KeyframeU5BU5D_t1068524471* ___keys0, const MethodInfo* method)
{
	typedef void (*AnimationCurve_Init_m1115968024_ftn) (AnimationCurve_t3046754366 *, KeyframeU5BU5D_t1068524471*);
	static AnimationCurve_Init_m1115968024_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AnimationCurve_Init_m1115968024_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AnimationCurve::Init(UnityEngine.Keyframe[])");
	_il2cpp_icall_func(__this, ___keys0);
}


// Conversion methods for marshalling of: UnityEngine.AnimationEvent
extern "C" void AnimationEvent_t1536042487_marshal_pinvoke(const AnimationEvent_t1536042487& unmarshaled, AnimationEvent_t1536042487_marshaled_pinvoke& marshaled)
{
	Il2CppCodeGenException* ___m_StateSender_8Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_StateSender' of type 'AnimationEvent': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_StateSender_8Exception);
}
extern "C" void AnimationEvent_t1536042487_marshal_pinvoke_back(const AnimationEvent_t1536042487_marshaled_pinvoke& marshaled, AnimationEvent_t1536042487& unmarshaled)
{
	Il2CppCodeGenException* ___m_StateSender_8Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_StateSender' of type 'AnimationEvent': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_StateSender_8Exception);
}
// Conversion method for clean up from marshalling of: UnityEngine.AnimationEvent
extern "C" void AnimationEvent_t1536042487_marshal_pinvoke_cleanup(AnimationEvent_t1536042487_marshaled_pinvoke& marshaled)
{
}


// Conversion methods for marshalling of: UnityEngine.AnimationEvent
extern "C" void AnimationEvent_t1536042487_marshal_com(const AnimationEvent_t1536042487& unmarshaled, AnimationEvent_t1536042487_marshaled_com& marshaled)
{
	Il2CppCodeGenException* ___m_StateSender_8Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_StateSender' of type 'AnimationEvent': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_StateSender_8Exception);
}
extern "C" void AnimationEvent_t1536042487_marshal_com_back(const AnimationEvent_t1536042487_marshaled_com& marshaled, AnimationEvent_t1536042487& unmarshaled)
{
	Il2CppCodeGenException* ___m_StateSender_8Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_StateSender' of type 'AnimationEvent': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_StateSender_8Exception);
}
// Conversion method for clean up from marshalling of: UnityEngine.AnimationEvent
extern "C" void AnimationEvent_t1536042487_marshal_com_cleanup(AnimationEvent_t1536042487_marshaled_com& marshaled)
{
}
// System.Void UnityEngine.AnimationEvent::.ctor()
extern "C"  void AnimationEvent__ctor_m989348042 (AnimationEvent_t1536042487 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AnimationEvent__ctor_m989348042_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Object__ctor_m297566312(__this, /*hidden argument*/NULL);
		__this->set_m_Time_0((0.0f));
		__this->set_m_FunctionName_1(_stringLiteral757602046);
		__this->set_m_StringParameter_2(_stringLiteral757602046);
		__this->set_m_ObjectReferenceParameter_3((Object_t631007953 *)NULL);
		__this->set_m_FloatParameter_4((0.0f));
		__this->set_m_IntParameter_5(0);
		__this->set_m_MessageOptions_6(0);
		__this->set_m_Source_7(0);
		__this->set_m_StateSender_8((AnimationState_t1108360407 *)NULL);
		return;
	}
}
// System.String UnityEngine.AnimationEvent::get_data()
extern "C"  String_t* AnimationEvent_get_data_m1681300901 (AnimationEvent_t1536042487 * __this, const MethodInfo* method)
{
	String_t* V_0 = NULL;
	{
		String_t* L_0 = __this->get_m_StringParameter_2();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		String_t* L_1 = V_0;
		return L_1;
	}
}
// System.Void UnityEngine.AnimationEvent::set_data(System.String)
extern "C"  void AnimationEvent_set_data_m368348476 (AnimationEvent_t1536042487 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_m_StringParameter_2(L_0);
		return;
	}
}
// System.String UnityEngine.AnimationEvent::get_stringParameter()
extern "C"  String_t* AnimationEvent_get_stringParameter_m77381198 (AnimationEvent_t1536042487 * __this, const MethodInfo* method)
{
	String_t* V_0 = NULL;
	{
		String_t* L_0 = __this->get_m_StringParameter_2();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		String_t* L_1 = V_0;
		return L_1;
	}
}
// System.Void UnityEngine.AnimationEvent::set_stringParameter(System.String)
extern "C"  void AnimationEvent_set_stringParameter_m45927508 (AnimationEvent_t1536042487 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_m_StringParameter_2(L_0);
		return;
	}
}
// System.Single UnityEngine.AnimationEvent::get_floatParameter()
extern "C"  float AnimationEvent_get_floatParameter_m1745425807 (AnimationEvent_t1536042487 * __this, const MethodInfo* method)
{
	float V_0 = 0.0f;
	{
		float L_0 = __this->get_m_FloatParameter_4();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		float L_1 = V_0;
		return L_1;
	}
}
// System.Void UnityEngine.AnimationEvent::set_floatParameter(System.Single)
extern "C"  void AnimationEvent_set_floatParameter_m642408167 (AnimationEvent_t1536042487 * __this, float ___value0, const MethodInfo* method)
{
	{
		float L_0 = ___value0;
		__this->set_m_FloatParameter_4(L_0);
		return;
	}
}
// System.Int32 UnityEngine.AnimationEvent::get_intParameter()
extern "C"  int32_t AnimationEvent_get_intParameter_m2847673300 (AnimationEvent_t1536042487 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_m_IntParameter_5();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		int32_t L_1 = V_0;
		return L_1;
	}
}
// System.Void UnityEngine.AnimationEvent::set_intParameter(System.Int32)
extern "C"  void AnimationEvent_set_intParameter_m3678717901 (AnimationEvent_t1536042487 * __this, int32_t ___value0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_m_IntParameter_5(L_0);
		return;
	}
}
// UnityEngine.Object UnityEngine.AnimationEvent::get_objectReferenceParameter()
extern "C"  Object_t631007953 * AnimationEvent_get_objectReferenceParameter_m3615964334 (AnimationEvent_t1536042487 * __this, const MethodInfo* method)
{
	Object_t631007953 * V_0 = NULL;
	{
		Object_t631007953 * L_0 = __this->get_m_ObjectReferenceParameter_3();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		Object_t631007953 * L_1 = V_0;
		return L_1;
	}
}
// System.Void UnityEngine.AnimationEvent::set_objectReferenceParameter(UnityEngine.Object)
extern "C"  void AnimationEvent_set_objectReferenceParameter_m1772927931 (AnimationEvent_t1536042487 * __this, Object_t631007953 * ___value0, const MethodInfo* method)
{
	{
		Object_t631007953 * L_0 = ___value0;
		__this->set_m_ObjectReferenceParameter_3(L_0);
		return;
	}
}
// System.String UnityEngine.AnimationEvent::get_functionName()
extern "C"  String_t* AnimationEvent_get_functionName_m1502361540 (AnimationEvent_t1536042487 * __this, const MethodInfo* method)
{
	String_t* V_0 = NULL;
	{
		String_t* L_0 = __this->get_m_FunctionName_1();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		String_t* L_1 = V_0;
		return L_1;
	}
}
// System.Void UnityEngine.AnimationEvent::set_functionName(System.String)
extern "C"  void AnimationEvent_set_functionName_m932580127 (AnimationEvent_t1536042487 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_m_FunctionName_1(L_0);
		return;
	}
}
// System.Single UnityEngine.AnimationEvent::get_time()
extern "C"  float AnimationEvent_get_time_m3455244934 (AnimationEvent_t1536042487 * __this, const MethodInfo* method)
{
	float V_0 = 0.0f;
	{
		float L_0 = __this->get_m_Time_0();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		float L_1 = V_0;
		return L_1;
	}
}
// System.Void UnityEngine.AnimationEvent::set_time(System.Single)
extern "C"  void AnimationEvent_set_time_m1426994918 (AnimationEvent_t1536042487 * __this, float ___value0, const MethodInfo* method)
{
	{
		float L_0 = ___value0;
		__this->set_m_Time_0(L_0);
		return;
	}
}
// UnityEngine.SendMessageOptions UnityEngine.AnimationEvent::get_messageOptions()
extern "C"  int32_t AnimationEvent_get_messageOptions_m3657540398 (AnimationEvent_t1536042487 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_m_MessageOptions_6();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		int32_t L_1 = V_0;
		return L_1;
	}
}
// System.Void UnityEngine.AnimationEvent::set_messageOptions(UnityEngine.SendMessageOptions)
extern "C"  void AnimationEvent_set_messageOptions_m3131518327 (AnimationEvent_t1536042487 * __this, int32_t ___value0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_m_MessageOptions_6(L_0);
		return;
	}
}
// System.Boolean UnityEngine.AnimationEvent::get_isFiredByLegacy()
extern "C"  bool AnimationEvent_get_isFiredByLegacy_m2397681361 (AnimationEvent_t1536042487 * __this, const MethodInfo* method)
{
	bool V_0 = false;
	{
		int32_t L_0 = __this->get_m_Source_7();
		V_0 = (bool)((((int32_t)L_0) == ((int32_t)1))? 1 : 0);
		goto IL_0010;
	}

IL_0010:
	{
		bool L_1 = V_0;
		return L_1;
	}
}
// System.Boolean UnityEngine.AnimationEvent::get_isFiredByAnimator()
extern "C"  bool AnimationEvent_get_isFiredByAnimator_m2545534689 (AnimationEvent_t1536042487 * __this, const MethodInfo* method)
{
	bool V_0 = false;
	{
		int32_t L_0 = __this->get_m_Source_7();
		V_0 = (bool)((((int32_t)L_0) == ((int32_t)2))? 1 : 0);
		goto IL_0010;
	}

IL_0010:
	{
		bool L_1 = V_0;
		return L_1;
	}
}
// UnityEngine.AnimationState UnityEngine.AnimationEvent::get_animationState()
extern "C"  AnimationState_t1108360407 * AnimationEvent_get_animationState_m2229874910 (AnimationEvent_t1536042487 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AnimationEvent_get_animationState_m2229874910_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	AnimationState_t1108360407 * V_0 = NULL;
	{
		bool L_0 = AnimationEvent_get_isFiredByLegacy_m2397681361(__this, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_0016;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3317548046_il2cpp_TypeInfo_var);
		Debug_LogError_m2059623341(NULL /*static, unused*/, _stringLiteral1009479862, /*hidden argument*/NULL);
	}

IL_0016:
	{
		AnimationState_t1108360407 * L_1 = __this->get_m_StateSender_8();
		V_0 = L_1;
		goto IL_0022;
	}

IL_0022:
	{
		AnimationState_t1108360407 * L_2 = V_0;
		return L_2;
	}
}
// UnityEngine.AnimatorStateInfo UnityEngine.AnimationEvent::get_animatorStateInfo()
extern "C"  AnimatorStateInfo_t509032636  AnimationEvent_get_animatorStateInfo_m3909257444 (AnimationEvent_t1536042487 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AnimationEvent_get_animatorStateInfo_m3909257444_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	AnimatorStateInfo_t509032636  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		bool L_0 = AnimationEvent_get_isFiredByAnimator_m2545534689(__this, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_0016;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3317548046_il2cpp_TypeInfo_var);
		Debug_LogError_m2059623341(NULL /*static, unused*/, _stringLiteral3962017886, /*hidden argument*/NULL);
	}

IL_0016:
	{
		AnimatorStateInfo_t509032636  L_1 = __this->get_m_AnimatorStateInfo_9();
		V_0 = L_1;
		goto IL_0022;
	}

IL_0022:
	{
		AnimatorStateInfo_t509032636  L_2 = V_0;
		return L_2;
	}
}
// UnityEngine.AnimatorClipInfo UnityEngine.AnimationEvent::get_animatorClipInfo()
extern "C"  AnimatorClipInfo_t3156717155  AnimationEvent_get_animatorClipInfo_m2784760084 (AnimationEvent_t1536042487 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AnimationEvent_get_animatorClipInfo_m2784760084_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	AnimatorClipInfo_t3156717155  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		bool L_0 = AnimationEvent_get_isFiredByAnimator_m2545534689(__this, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_0016;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3317548046_il2cpp_TypeInfo_var);
		Debug_LogError_m2059623341(NULL /*static, unused*/, _stringLiteral3070799046, /*hidden argument*/NULL);
	}

IL_0016:
	{
		AnimatorClipInfo_t3156717155  L_1 = __this->get_m_AnimatorClipInfo_10();
		V_0 = L_1;
		goto IL_0022;
	}

IL_0022:
	{
		AnimatorClipInfo_t3156717155  L_2 = V_0;
		return L_2;
	}
}
// System.Int32 UnityEngine.AnimationEvent::GetHash()
extern "C"  int32_t AnimationEvent_GetHash_m1768228440 (AnimationEvent_t1536042487 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	float V_1 = 0.0f;
	int32_t V_2 = 0;
	{
		V_0 = 0;
		String_t* L_0 = AnimationEvent_get_functionName_m1502361540(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		int32_t L_1 = VirtFuncInvoker0< int32_t >::Invoke(2 /* System.Int32 System.Object::GetHashCode() */, L_0);
		V_0 = L_1;
		int32_t L_2 = V_0;
		float L_3 = AnimationEvent_get_time_m3455244934(__this, /*hidden argument*/NULL);
		V_1 = L_3;
		int32_t L_4 = Single_GetHashCode_m1558506138((&V_1), /*hidden argument*/NULL);
		V_0 = ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)33)*(int32_t)L_2))+(int32_t)L_4));
		int32_t L_5 = V_0;
		V_2 = L_5;
		goto IL_0031;
	}

IL_0031:
	{
		int32_t L_6 = V_2;
		return L_6;
	}
}
// System.Single UnityEngine.Animator::GetFloat(System.String)
extern "C"  float Animator_GetFloat_m1706581283 (Animator_t434523843 * __this, String_t* ___name0, const MethodInfo* method)
{
	float V_0 = 0.0f;
	{
		String_t* L_0 = ___name0;
		float L_1 = Animator_GetFloatString_m293525848(__this, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_000e;
	}

IL_000e:
	{
		float L_2 = V_0;
		return L_2;
	}
}
// System.Void UnityEngine.Animator::SetFloat(System.String,System.Single)
extern "C"  void Animator_SetFloat_m3017254632 (Animator_t434523843 * __this, String_t* ___name0, float ___value1, const MethodInfo* method)
{
	{
		String_t* L_0 = ___name0;
		float L_1 = ___value1;
		Animator_SetFloatString_m3317565049(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean UnityEngine.Animator::GetBool(System.String)
extern "C"  bool Animator_GetBool_m4254455114 (Animator_t434523843 * __this, String_t* ___name0, const MethodInfo* method)
{
	bool V_0 = false;
	{
		String_t* L_0 = ___name0;
		bool L_1 = Animator_GetBoolString_m1559437030(__this, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_000e;
	}

IL_000e:
	{
		bool L_2 = V_0;
		return L_2;
	}
}
// System.Void UnityEngine.Animator::SetBool(System.String,System.Boolean)
extern "C"  void Animator_SetBool_m1964387883 (Animator_t434523843 * __this, String_t* ___name0, bool ___value1, const MethodInfo* method)
{
	{
		String_t* L_0 = ___name0;
		bool L_1 = ___value1;
		Animator_SetBoolString_m2747575116(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Animator::SetTrigger(System.String)
extern "C"  void Animator_SetTrigger_m2194443996 (Animator_t434523843 * __this, String_t* ___name0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___name0;
		Animator_SetTriggerString_m2642918599(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Animator::ResetTrigger(System.String)
extern "C"  void Animator_ResetTrigger_m2500195305 (Animator_t434523843 * __this, String_t* ___name0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___name0;
		Animator_ResetTriggerString_m1083629800(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Animator::set_applyRootMotion(System.Boolean)
extern "C"  void Animator_set_applyRootMotion_m1776456186 (Animator_t434523843 * __this, bool ___value0, const MethodInfo* method)
{
	typedef void (*Animator_set_applyRootMotion_m1776456186_ftn) (Animator_t434523843 *, bool);
	static Animator_set_applyRootMotion_m1776456186_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animator_set_applyRootMotion_m1776456186_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animator::set_applyRootMotion(System.Boolean)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.Animator::Play(System.String)
extern "C"  void Animator_Play_m3209247432 (Animator_t434523843 * __this, String_t* ___stateName0, const MethodInfo* method)
{
	float V_0 = 0.0f;
	int32_t V_1 = 0;
	{
		V_0 = (-std::numeric_limits<float>::infinity());
		V_1 = (-1);
		String_t* L_0 = ___stateName0;
		int32_t L_1 = V_1;
		float L_2 = V_0;
		Animator_Play_m2010935922(__this, L_0, L_1, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Animator::Play(System.String,System.Int32,System.Single)
extern "C"  void Animator_Play_m2010935922 (Animator_t434523843 * __this, String_t* ___stateName0, int32_t ___layer1, float ___normalizedTime2, const MethodInfo* method)
{
	{
		String_t* L_0 = ___stateName0;
		int32_t L_1 = Animator_StringToHash_m2188957499(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		int32_t L_2 = ___layer1;
		float L_3 = ___normalizedTime2;
		Animator_Play_m431000399(__this, L_1, L_2, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Animator::Play(System.Int32,System.Int32,System.Single)
extern "C"  void Animator_Play_m431000399 (Animator_t434523843 * __this, int32_t ___stateNameHash0, int32_t ___layer1, float ___normalizedTime2, const MethodInfo* method)
{
	typedef void (*Animator_Play_m431000399_ftn) (Animator_t434523843 *, int32_t, int32_t, float);
	static Animator_Play_m431000399_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animator_Play_m431000399_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animator::Play(System.Int32,System.Int32,System.Single)");
	_il2cpp_icall_func(__this, ___stateNameHash0, ___layer1, ___normalizedTime2);
}
// System.Boolean UnityEngine.Animator::get_hasBoundPlayables()
extern "C"  bool Animator_get_hasBoundPlayables_m117367483 (Animator_t434523843 * __this, const MethodInfo* method)
{
	typedef bool (*Animator_get_hasBoundPlayables_m117367483_ftn) (Animator_t434523843 *);
	static Animator_get_hasBoundPlayables_m117367483_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animator_get_hasBoundPlayables_m117367483_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animator::get_hasBoundPlayables()");
	return _il2cpp_icall_func(__this);
}
// System.Int32 UnityEngine.Animator::StringToHash(System.String)
extern "C"  int32_t Animator_StringToHash_m2188957499 (Il2CppObject * __this /* static, unused */, String_t* ___name0, const MethodInfo* method)
{
	typedef int32_t (*Animator_StringToHash_m2188957499_ftn) (String_t*);
	static Animator_StringToHash_m2188957499_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animator_StringToHash_m2188957499_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animator::StringToHash(System.String)");
	return _il2cpp_icall_func(___name0);
}
// System.Void UnityEngine.Animator::SetFloatString(System.String,System.Single)
extern "C"  void Animator_SetFloatString_m3317565049 (Animator_t434523843 * __this, String_t* ___name0, float ___value1, const MethodInfo* method)
{
	typedef void (*Animator_SetFloatString_m3317565049_ftn) (Animator_t434523843 *, String_t*, float);
	static Animator_SetFloatString_m3317565049_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animator_SetFloatString_m3317565049_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animator::SetFloatString(System.String,System.Single)");
	_il2cpp_icall_func(__this, ___name0, ___value1);
}
// System.Single UnityEngine.Animator::GetFloatString(System.String)
extern "C"  float Animator_GetFloatString_m293525848 (Animator_t434523843 * __this, String_t* ___name0, const MethodInfo* method)
{
	typedef float (*Animator_GetFloatString_m293525848_ftn) (Animator_t434523843 *, String_t*);
	static Animator_GetFloatString_m293525848_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animator_GetFloatString_m293525848_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animator::GetFloatString(System.String)");
	return _il2cpp_icall_func(__this, ___name0);
}
// System.Void UnityEngine.Animator::SetBoolString(System.String,System.Boolean)
extern "C"  void Animator_SetBoolString_m2747575116 (Animator_t434523843 * __this, String_t* ___name0, bool ___value1, const MethodInfo* method)
{
	typedef void (*Animator_SetBoolString_m2747575116_ftn) (Animator_t434523843 *, String_t*, bool);
	static Animator_SetBoolString_m2747575116_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animator_SetBoolString_m2747575116_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animator::SetBoolString(System.String,System.Boolean)");
	_il2cpp_icall_func(__this, ___name0, ___value1);
}
// System.Boolean UnityEngine.Animator::GetBoolString(System.String)
extern "C"  bool Animator_GetBoolString_m1559437030 (Animator_t434523843 * __this, String_t* ___name0, const MethodInfo* method)
{
	typedef bool (*Animator_GetBoolString_m1559437030_ftn) (Animator_t434523843 *, String_t*);
	static Animator_GetBoolString_m1559437030_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animator_GetBoolString_m1559437030_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animator::GetBoolString(System.String)");
	return _il2cpp_icall_func(__this, ___name0);
}
// System.Void UnityEngine.Animator::SetTriggerString(System.String)
extern "C"  void Animator_SetTriggerString_m2642918599 (Animator_t434523843 * __this, String_t* ___name0, const MethodInfo* method)
{
	typedef void (*Animator_SetTriggerString_m2642918599_ftn) (Animator_t434523843 *, String_t*);
	static Animator_SetTriggerString_m2642918599_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animator_SetTriggerString_m2642918599_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animator::SetTriggerString(System.String)");
	_il2cpp_icall_func(__this, ___name0);
}
// System.Void UnityEngine.Animator::ResetTriggerString(System.String)
extern "C"  void Animator_ResetTriggerString_m1083629800 (Animator_t434523843 * __this, String_t* ___name0, const MethodInfo* method)
{
	typedef void (*Animator_ResetTriggerString_m1083629800_ftn) (Animator_t434523843 *, String_t*);
	static Animator_ResetTriggerString_m1083629800_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animator_ResetTriggerString_m1083629800_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animator::ResetTriggerString(System.String)");
	_il2cpp_icall_func(__this, ___name0);
}
// System.String UnityEngine.AnimatorControllerParameter::get_name()
extern "C"  String_t* AnimatorControllerParameter_get_name_m4038051952 (AnimatorControllerParameter_t1758260042 * __this, const MethodInfo* method)
{
	String_t* V_0 = NULL;
	{
		String_t* L_0 = __this->get_m_Name_0();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		String_t* L_1 = V_0;
		return L_1;
	}
}
// System.Boolean UnityEngine.AnimatorControllerParameter::Equals(System.Object)
extern "C"  bool AnimatorControllerParameter_Equals_m1144065427 (AnimatorControllerParameter_t1758260042 * __this, Il2CppObject * ___o0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AnimatorControllerParameter_Equals_m1144065427_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	AnimatorControllerParameter_t1758260042 * V_0 = NULL;
	bool V_1 = false;
	int32_t G_B7_0 = 0;
	{
		Il2CppObject * L_0 = ___o0;
		V_0 = ((AnimatorControllerParameter_t1758260042 *)IsInstSealed(L_0, AnimatorControllerParameter_t1758260042_il2cpp_TypeInfo_var));
		AnimatorControllerParameter_t1758260042 * L_1 = V_0;
		if (!L_1)
		{
			goto IL_0067;
		}
	}
	{
		String_t* L_2 = __this->get_m_Name_0();
		AnimatorControllerParameter_t1758260042 * L_3 = V_0;
		NullCheck(L_3);
		String_t* L_4 = L_3->get_m_Name_0();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_5 = String_op_Equality_m920492651(NULL /*static, unused*/, L_2, L_4, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_0067;
		}
	}
	{
		int32_t L_6 = __this->get_m_Type_1();
		AnimatorControllerParameter_t1758260042 * L_7 = V_0;
		NullCheck(L_7);
		int32_t L_8 = L_7->get_m_Type_1();
		if ((!(((uint32_t)L_6) == ((uint32_t)L_8))))
		{
			goto IL_0067;
		}
	}
	{
		float L_9 = __this->get_m_DefaultFloat_2();
		AnimatorControllerParameter_t1758260042 * L_10 = V_0;
		NullCheck(L_10);
		float L_11 = L_10->get_m_DefaultFloat_2();
		if ((!(((float)L_9) == ((float)L_11))))
		{
			goto IL_0067;
		}
	}
	{
		int32_t L_12 = __this->get_m_DefaultInt_3();
		AnimatorControllerParameter_t1758260042 * L_13 = V_0;
		NullCheck(L_13);
		int32_t L_14 = L_13->get_m_DefaultInt_3();
		if ((!(((uint32_t)L_12) == ((uint32_t)L_14))))
		{
			goto IL_0067;
		}
	}
	{
		bool L_15 = __this->get_m_DefaultBool_4();
		AnimatorControllerParameter_t1758260042 * L_16 = V_0;
		NullCheck(L_16);
		bool L_17 = L_16->get_m_DefaultBool_4();
		G_B7_0 = ((((int32_t)L_15) == ((int32_t)L_17))? 1 : 0);
		goto IL_0068;
	}

IL_0067:
	{
		G_B7_0 = 0;
	}

IL_0068:
	{
		V_1 = (bool)G_B7_0;
		goto IL_006e;
	}

IL_006e:
	{
		bool L_18 = V_1;
		return L_18;
	}
}
// System.Int32 UnityEngine.AnimatorControllerParameter::GetHashCode()
extern "C"  int32_t AnimatorControllerParameter_GetHashCode_m2306059248 (AnimatorControllerParameter_t1758260042 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		String_t* L_0 = AnimatorControllerParameter_get_name_m4038051952(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		int32_t L_1 = VirtFuncInvoker0< int32_t >::Invoke(2 /* System.Int32 System.Object::GetHashCode() */, L_0);
		V_0 = L_1;
		goto IL_0012;
	}

IL_0012:
	{
		int32_t L_2 = V_0;
		return L_2;
	}
}
// System.Boolean UnityEngine.AnimatorStateInfo::IsName(System.String)
extern "C"  bool AnimatorStateInfo_IsName_m1584047739 (AnimatorStateInfo_t509032636 * __this, String_t* ___name0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	bool V_1 = false;
	int32_t G_B4_0 = 0;
	{
		String_t* L_0 = ___name0;
		int32_t L_1 = Animator_StringToHash_m2188957499(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		int32_t L_2 = V_0;
		int32_t L_3 = __this->get_m_FullPath_2();
		if ((((int32_t)L_2) == ((int32_t)L_3)))
		{
			goto IL_002b;
		}
	}
	{
		int32_t L_4 = V_0;
		int32_t L_5 = __this->get_m_Name_0();
		if ((((int32_t)L_4) == ((int32_t)L_5)))
		{
			goto IL_002b;
		}
	}
	{
		int32_t L_6 = V_0;
		int32_t L_7 = __this->get_m_Path_1();
		G_B4_0 = ((((int32_t)L_6) == ((int32_t)L_7))? 1 : 0);
		goto IL_002c;
	}

IL_002b:
	{
		G_B4_0 = 1;
	}

IL_002c:
	{
		V_1 = (bool)G_B4_0;
		goto IL_0032;
	}

IL_0032:
	{
		bool L_8 = V_1;
		return L_8;
	}
}
extern "C"  bool AnimatorStateInfo_IsName_m1584047739_AdjustorThunk (Il2CppObject * __this, String_t* ___name0, const MethodInfo* method)
{
	AnimatorStateInfo_t509032636 * _thisAdjusted = reinterpret_cast<AnimatorStateInfo_t509032636 *>(__this + 1);
	return AnimatorStateInfo_IsName_m1584047739(_thisAdjusted, ___name0, method);
}
// System.Int32 UnityEngine.AnimatorStateInfo::get_fullPathHash()
extern "C"  int32_t AnimatorStateInfo_get_fullPathHash_m2240461550 (AnimatorStateInfo_t509032636 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_m_FullPath_2();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		int32_t L_1 = V_0;
		return L_1;
	}
}
extern "C"  int32_t AnimatorStateInfo_get_fullPathHash_m2240461550_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	AnimatorStateInfo_t509032636 * _thisAdjusted = reinterpret_cast<AnimatorStateInfo_t509032636 *>(__this + 1);
	return AnimatorStateInfo_get_fullPathHash_m2240461550(_thisAdjusted, method);
}
// System.Int32 UnityEngine.AnimatorStateInfo::get_nameHash()
extern "C"  int32_t AnimatorStateInfo_get_nameHash_m3988777370 (AnimatorStateInfo_t509032636 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_m_Path_1();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		int32_t L_1 = V_0;
		return L_1;
	}
}
extern "C"  int32_t AnimatorStateInfo_get_nameHash_m3988777370_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	AnimatorStateInfo_t509032636 * _thisAdjusted = reinterpret_cast<AnimatorStateInfo_t509032636 *>(__this + 1);
	return AnimatorStateInfo_get_nameHash_m3988777370(_thisAdjusted, method);
}
// System.Int32 UnityEngine.AnimatorStateInfo::get_shortNameHash()
extern "C"  int32_t AnimatorStateInfo_get_shortNameHash_m119881483 (AnimatorStateInfo_t509032636 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_m_Name_0();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		int32_t L_1 = V_0;
		return L_1;
	}
}
extern "C"  int32_t AnimatorStateInfo_get_shortNameHash_m119881483_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	AnimatorStateInfo_t509032636 * _thisAdjusted = reinterpret_cast<AnimatorStateInfo_t509032636 *>(__this + 1);
	return AnimatorStateInfo_get_shortNameHash_m119881483(_thisAdjusted, method);
}
// System.Single UnityEngine.AnimatorStateInfo::get_normalizedTime()
extern "C"  float AnimatorStateInfo_get_normalizedTime_m2807918906 (AnimatorStateInfo_t509032636 * __this, const MethodInfo* method)
{
	float V_0 = 0.0f;
	{
		float L_0 = __this->get_m_NormalizedTime_3();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		float L_1 = V_0;
		return L_1;
	}
}
extern "C"  float AnimatorStateInfo_get_normalizedTime_m2807918906_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	AnimatorStateInfo_t509032636 * _thisAdjusted = reinterpret_cast<AnimatorStateInfo_t509032636 *>(__this + 1);
	return AnimatorStateInfo_get_normalizedTime_m2807918906(_thisAdjusted, method);
}
// System.Single UnityEngine.AnimatorStateInfo::get_length()
extern "C"  float AnimatorStateInfo_get_length_m3907061235 (AnimatorStateInfo_t509032636 * __this, const MethodInfo* method)
{
	float V_0 = 0.0f;
	{
		float L_0 = __this->get_m_Length_4();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		float L_1 = V_0;
		return L_1;
	}
}
extern "C"  float AnimatorStateInfo_get_length_m3907061235_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	AnimatorStateInfo_t509032636 * _thisAdjusted = reinterpret_cast<AnimatorStateInfo_t509032636 *>(__this + 1);
	return AnimatorStateInfo_get_length_m3907061235(_thisAdjusted, method);
}
// System.Single UnityEngine.AnimatorStateInfo::get_speed()
extern "C"  float AnimatorStateInfo_get_speed_m1229611452 (AnimatorStateInfo_t509032636 * __this, const MethodInfo* method)
{
	float V_0 = 0.0f;
	{
		float L_0 = __this->get_m_Speed_5();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		float L_1 = V_0;
		return L_1;
	}
}
extern "C"  float AnimatorStateInfo_get_speed_m1229611452_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	AnimatorStateInfo_t509032636 * _thisAdjusted = reinterpret_cast<AnimatorStateInfo_t509032636 *>(__this + 1);
	return AnimatorStateInfo_get_speed_m1229611452(_thisAdjusted, method);
}
// System.Single UnityEngine.AnimatorStateInfo::get_speedMultiplier()
extern "C"  float AnimatorStateInfo_get_speedMultiplier_m1871749593 (AnimatorStateInfo_t509032636 * __this, const MethodInfo* method)
{
	float V_0 = 0.0f;
	{
		float L_0 = __this->get_m_SpeedMultiplier_6();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		float L_1 = V_0;
		return L_1;
	}
}
extern "C"  float AnimatorStateInfo_get_speedMultiplier_m1871749593_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	AnimatorStateInfo_t509032636 * _thisAdjusted = reinterpret_cast<AnimatorStateInfo_t509032636 *>(__this + 1);
	return AnimatorStateInfo_get_speedMultiplier_m1871749593(_thisAdjusted, method);
}
// System.Int32 UnityEngine.AnimatorStateInfo::get_tagHash()
extern "C"  int32_t AnimatorStateInfo_get_tagHash_m3281207203 (AnimatorStateInfo_t509032636 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_m_Tag_7();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		int32_t L_1 = V_0;
		return L_1;
	}
}
extern "C"  int32_t AnimatorStateInfo_get_tagHash_m3281207203_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	AnimatorStateInfo_t509032636 * _thisAdjusted = reinterpret_cast<AnimatorStateInfo_t509032636 *>(__this + 1);
	return AnimatorStateInfo_get_tagHash_m3281207203(_thisAdjusted, method);
}
// System.Boolean UnityEngine.AnimatorStateInfo::IsTag(System.String)
extern "C"  bool AnimatorStateInfo_IsTag_m4246855624 (AnimatorStateInfo_t509032636 * __this, String_t* ___tag0, const MethodInfo* method)
{
	bool V_0 = false;
	{
		String_t* L_0 = ___tag0;
		int32_t L_1 = Animator_StringToHash_m2188957499(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		int32_t L_2 = __this->get_m_Tag_7();
		V_0 = (bool)((((int32_t)L_1) == ((int32_t)L_2))? 1 : 0);
		goto IL_0015;
	}

IL_0015:
	{
		bool L_3 = V_0;
		return L_3;
	}
}
extern "C"  bool AnimatorStateInfo_IsTag_m4246855624_AdjustorThunk (Il2CppObject * __this, String_t* ___tag0, const MethodInfo* method)
{
	AnimatorStateInfo_t509032636 * _thisAdjusted = reinterpret_cast<AnimatorStateInfo_t509032636 *>(__this + 1);
	return AnimatorStateInfo_IsTag_m4246855624(_thisAdjusted, ___tag0, method);
}
// System.Boolean UnityEngine.AnimatorStateInfo::get_loop()
extern "C"  bool AnimatorStateInfo_get_loop_m1486238854 (AnimatorStateInfo_t509032636 * __this, const MethodInfo* method)
{
	bool V_0 = false;
	{
		int32_t L_0 = __this->get_m_Loop_8();
		V_0 = (bool)((((int32_t)((((int32_t)L_0) == ((int32_t)0))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0013;
	}

IL_0013:
	{
		bool L_1 = V_0;
		return L_1;
	}
}
extern "C"  bool AnimatorStateInfo_get_loop_m1486238854_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	AnimatorStateInfo_t509032636 * _thisAdjusted = reinterpret_cast<AnimatorStateInfo_t509032636 *>(__this + 1);
	return AnimatorStateInfo_get_loop_m1486238854(_thisAdjusted, method);
}
// Conversion methods for marshalling of: UnityEngine.AnimatorTransitionInfo
extern "C" void AnimatorTransitionInfo_t2534804151_marshal_pinvoke(const AnimatorTransitionInfo_t2534804151& unmarshaled, AnimatorTransitionInfo_t2534804151_marshaled_pinvoke& marshaled)
{
	marshaled.___m_FullPath_0 = unmarshaled.get_m_FullPath_0();
	marshaled.___m_UserName_1 = unmarshaled.get_m_UserName_1();
	marshaled.___m_Name_2 = unmarshaled.get_m_Name_2();
	marshaled.___m_NormalizedTime_3 = unmarshaled.get_m_NormalizedTime_3();
	marshaled.___m_AnyState_4 = static_cast<int32_t>(unmarshaled.get_m_AnyState_4());
	marshaled.___m_TransitionType_5 = unmarshaled.get_m_TransitionType_5();
}
extern "C" void AnimatorTransitionInfo_t2534804151_marshal_pinvoke_back(const AnimatorTransitionInfo_t2534804151_marshaled_pinvoke& marshaled, AnimatorTransitionInfo_t2534804151& unmarshaled)
{
	int32_t unmarshaled_m_FullPath_temp_0 = 0;
	unmarshaled_m_FullPath_temp_0 = marshaled.___m_FullPath_0;
	unmarshaled.set_m_FullPath_0(unmarshaled_m_FullPath_temp_0);
	int32_t unmarshaled_m_UserName_temp_1 = 0;
	unmarshaled_m_UserName_temp_1 = marshaled.___m_UserName_1;
	unmarshaled.set_m_UserName_1(unmarshaled_m_UserName_temp_1);
	int32_t unmarshaled_m_Name_temp_2 = 0;
	unmarshaled_m_Name_temp_2 = marshaled.___m_Name_2;
	unmarshaled.set_m_Name_2(unmarshaled_m_Name_temp_2);
	float unmarshaled_m_NormalizedTime_temp_3 = 0.0f;
	unmarshaled_m_NormalizedTime_temp_3 = marshaled.___m_NormalizedTime_3;
	unmarshaled.set_m_NormalizedTime_3(unmarshaled_m_NormalizedTime_temp_3);
	bool unmarshaled_m_AnyState_temp_4 = false;
	unmarshaled_m_AnyState_temp_4 = static_cast<bool>(marshaled.___m_AnyState_4);
	unmarshaled.set_m_AnyState_4(unmarshaled_m_AnyState_temp_4);
	int32_t unmarshaled_m_TransitionType_temp_5 = 0;
	unmarshaled_m_TransitionType_temp_5 = marshaled.___m_TransitionType_5;
	unmarshaled.set_m_TransitionType_5(unmarshaled_m_TransitionType_temp_5);
}
// Conversion method for clean up from marshalling of: UnityEngine.AnimatorTransitionInfo
extern "C" void AnimatorTransitionInfo_t2534804151_marshal_pinvoke_cleanup(AnimatorTransitionInfo_t2534804151_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.AnimatorTransitionInfo
extern "C" void AnimatorTransitionInfo_t2534804151_marshal_com(const AnimatorTransitionInfo_t2534804151& unmarshaled, AnimatorTransitionInfo_t2534804151_marshaled_com& marshaled)
{
	marshaled.___m_FullPath_0 = unmarshaled.get_m_FullPath_0();
	marshaled.___m_UserName_1 = unmarshaled.get_m_UserName_1();
	marshaled.___m_Name_2 = unmarshaled.get_m_Name_2();
	marshaled.___m_NormalizedTime_3 = unmarshaled.get_m_NormalizedTime_3();
	marshaled.___m_AnyState_4 = static_cast<int32_t>(unmarshaled.get_m_AnyState_4());
	marshaled.___m_TransitionType_5 = unmarshaled.get_m_TransitionType_5();
}
extern "C" void AnimatorTransitionInfo_t2534804151_marshal_com_back(const AnimatorTransitionInfo_t2534804151_marshaled_com& marshaled, AnimatorTransitionInfo_t2534804151& unmarshaled)
{
	int32_t unmarshaled_m_FullPath_temp_0 = 0;
	unmarshaled_m_FullPath_temp_0 = marshaled.___m_FullPath_0;
	unmarshaled.set_m_FullPath_0(unmarshaled_m_FullPath_temp_0);
	int32_t unmarshaled_m_UserName_temp_1 = 0;
	unmarshaled_m_UserName_temp_1 = marshaled.___m_UserName_1;
	unmarshaled.set_m_UserName_1(unmarshaled_m_UserName_temp_1);
	int32_t unmarshaled_m_Name_temp_2 = 0;
	unmarshaled_m_Name_temp_2 = marshaled.___m_Name_2;
	unmarshaled.set_m_Name_2(unmarshaled_m_Name_temp_2);
	float unmarshaled_m_NormalizedTime_temp_3 = 0.0f;
	unmarshaled_m_NormalizedTime_temp_3 = marshaled.___m_NormalizedTime_3;
	unmarshaled.set_m_NormalizedTime_3(unmarshaled_m_NormalizedTime_temp_3);
	bool unmarshaled_m_AnyState_temp_4 = false;
	unmarshaled_m_AnyState_temp_4 = static_cast<bool>(marshaled.___m_AnyState_4);
	unmarshaled.set_m_AnyState_4(unmarshaled_m_AnyState_temp_4);
	int32_t unmarshaled_m_TransitionType_temp_5 = 0;
	unmarshaled_m_TransitionType_temp_5 = marshaled.___m_TransitionType_5;
	unmarshaled.set_m_TransitionType_5(unmarshaled_m_TransitionType_temp_5);
}
// Conversion method for clean up from marshalling of: UnityEngine.AnimatorTransitionInfo
extern "C" void AnimatorTransitionInfo_t2534804151_marshal_com_cleanup(AnimatorTransitionInfo_t2534804151_marshaled_com& marshaled)
{
}
// System.Boolean UnityEngine.AnimatorTransitionInfo::IsName(System.String)
extern "C"  bool AnimatorTransitionInfo_IsName_m3060761961 (AnimatorTransitionInfo_t2534804151 * __this, String_t* ___name0, const MethodInfo* method)
{
	bool V_0 = false;
	int32_t G_B3_0 = 0;
	{
		String_t* L_0 = ___name0;
		int32_t L_1 = Animator_StringToHash_m2188957499(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		int32_t L_2 = __this->get_m_Name_2();
		if ((((int32_t)L_1) == ((int32_t)L_2)))
		{
			goto IL_0022;
		}
	}
	{
		String_t* L_3 = ___name0;
		int32_t L_4 = Animator_StringToHash_m2188957499(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		int32_t L_5 = __this->get_m_FullPath_0();
		G_B3_0 = ((((int32_t)L_4) == ((int32_t)L_5))? 1 : 0);
		goto IL_0023;
	}

IL_0022:
	{
		G_B3_0 = 1;
	}

IL_0023:
	{
		V_0 = (bool)G_B3_0;
		goto IL_0029;
	}

IL_0029:
	{
		bool L_6 = V_0;
		return L_6;
	}
}
extern "C"  bool AnimatorTransitionInfo_IsName_m3060761961_AdjustorThunk (Il2CppObject * __this, String_t* ___name0, const MethodInfo* method)
{
	AnimatorTransitionInfo_t2534804151 * _thisAdjusted = reinterpret_cast<AnimatorTransitionInfo_t2534804151 *>(__this + 1);
	return AnimatorTransitionInfo_IsName_m3060761961(_thisAdjusted, ___name0, method);
}
// System.Boolean UnityEngine.AnimatorTransitionInfo::IsUserName(System.String)
extern "C"  bool AnimatorTransitionInfo_IsUserName_m2064408313 (AnimatorTransitionInfo_t2534804151 * __this, String_t* ___name0, const MethodInfo* method)
{
	bool V_0 = false;
	{
		String_t* L_0 = ___name0;
		int32_t L_1 = Animator_StringToHash_m2188957499(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		int32_t L_2 = __this->get_m_UserName_1();
		V_0 = (bool)((((int32_t)L_1) == ((int32_t)L_2))? 1 : 0);
		goto IL_0015;
	}

IL_0015:
	{
		bool L_3 = V_0;
		return L_3;
	}
}
extern "C"  bool AnimatorTransitionInfo_IsUserName_m2064408313_AdjustorThunk (Il2CppObject * __this, String_t* ___name0, const MethodInfo* method)
{
	AnimatorTransitionInfo_t2534804151 * _thisAdjusted = reinterpret_cast<AnimatorTransitionInfo_t2534804151 *>(__this + 1);
	return AnimatorTransitionInfo_IsUserName_m2064408313(_thisAdjusted, ___name0, method);
}
// System.Int32 UnityEngine.AnimatorTransitionInfo::get_fullPathHash()
extern "C"  int32_t AnimatorTransitionInfo_get_fullPathHash_m2773536167 (AnimatorTransitionInfo_t2534804151 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_m_FullPath_0();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		int32_t L_1 = V_0;
		return L_1;
	}
}
extern "C"  int32_t AnimatorTransitionInfo_get_fullPathHash_m2773536167_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	AnimatorTransitionInfo_t2534804151 * _thisAdjusted = reinterpret_cast<AnimatorTransitionInfo_t2534804151 *>(__this + 1);
	return AnimatorTransitionInfo_get_fullPathHash_m2773536167(_thisAdjusted, method);
}
// System.Int32 UnityEngine.AnimatorTransitionInfo::get_nameHash()
extern "C"  int32_t AnimatorTransitionInfo_get_nameHash_m2906244497 (AnimatorTransitionInfo_t2534804151 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_m_Name_2();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		int32_t L_1 = V_0;
		return L_1;
	}
}
extern "C"  int32_t AnimatorTransitionInfo_get_nameHash_m2906244497_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	AnimatorTransitionInfo_t2534804151 * _thisAdjusted = reinterpret_cast<AnimatorTransitionInfo_t2534804151 *>(__this + 1);
	return AnimatorTransitionInfo_get_nameHash_m2906244497(_thisAdjusted, method);
}
// System.Int32 UnityEngine.AnimatorTransitionInfo::get_userNameHash()
extern "C"  int32_t AnimatorTransitionInfo_get_userNameHash_m3118405726 (AnimatorTransitionInfo_t2534804151 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_m_UserName_1();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		int32_t L_1 = V_0;
		return L_1;
	}
}
extern "C"  int32_t AnimatorTransitionInfo_get_userNameHash_m3118405726_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	AnimatorTransitionInfo_t2534804151 * _thisAdjusted = reinterpret_cast<AnimatorTransitionInfo_t2534804151 *>(__this + 1);
	return AnimatorTransitionInfo_get_userNameHash_m3118405726(_thisAdjusted, method);
}
// System.Single UnityEngine.AnimatorTransitionInfo::get_normalizedTime()
extern "C"  float AnimatorTransitionInfo_get_normalizedTime_m3104123786 (AnimatorTransitionInfo_t2534804151 * __this, const MethodInfo* method)
{
	float V_0 = 0.0f;
	{
		float L_0 = __this->get_m_NormalizedTime_3();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		float L_1 = V_0;
		return L_1;
	}
}
extern "C"  float AnimatorTransitionInfo_get_normalizedTime_m3104123786_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	AnimatorTransitionInfo_t2534804151 * _thisAdjusted = reinterpret_cast<AnimatorTransitionInfo_t2534804151 *>(__this + 1);
	return AnimatorTransitionInfo_get_normalizedTime_m3104123786(_thisAdjusted, method);
}
// System.Boolean UnityEngine.AnimatorTransitionInfo::get_anyState()
extern "C"  bool AnimatorTransitionInfo_get_anyState_m4275202666 (AnimatorTransitionInfo_t2534804151 * __this, const MethodInfo* method)
{
	bool V_0 = false;
	{
		bool L_0 = __this->get_m_AnyState_4();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		bool L_1 = V_0;
		return L_1;
	}
}
extern "C"  bool AnimatorTransitionInfo_get_anyState_m4275202666_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	AnimatorTransitionInfo_t2534804151 * _thisAdjusted = reinterpret_cast<AnimatorTransitionInfo_t2534804151 *>(__this + 1);
	return AnimatorTransitionInfo_get_anyState_m4275202666(_thisAdjusted, method);
}
// System.Boolean UnityEngine.AnimatorTransitionInfo::get_entry()
extern "C"  bool AnimatorTransitionInfo_get_entry_m3079357495 (AnimatorTransitionInfo_t2534804151 * __this, const MethodInfo* method)
{
	bool V_0 = false;
	{
		int32_t L_0 = __this->get_m_TransitionType_5();
		V_0 = (bool)((((int32_t)((((int32_t)((int32_t)((int32_t)L_0&(int32_t)2))) == ((int32_t)0))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0015;
	}

IL_0015:
	{
		bool L_1 = V_0;
		return L_1;
	}
}
extern "C"  bool AnimatorTransitionInfo_get_entry_m3079357495_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	AnimatorTransitionInfo_t2534804151 * _thisAdjusted = reinterpret_cast<AnimatorTransitionInfo_t2534804151 *>(__this + 1);
	return AnimatorTransitionInfo_get_entry_m3079357495(_thisAdjusted, method);
}
// System.Boolean UnityEngine.AnimatorTransitionInfo::get_exit()
extern "C"  bool AnimatorTransitionInfo_get_exit_m3346751000 (AnimatorTransitionInfo_t2534804151 * __this, const MethodInfo* method)
{
	bool V_0 = false;
	{
		int32_t L_0 = __this->get_m_TransitionType_5();
		V_0 = (bool)((((int32_t)((((int32_t)((int32_t)((int32_t)L_0&(int32_t)4))) == ((int32_t)0))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0015;
	}

IL_0015:
	{
		bool L_1 = V_0;
		return L_1;
	}
}
extern "C"  bool AnimatorTransitionInfo_get_exit_m3346751000_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	AnimatorTransitionInfo_t2534804151 * _thisAdjusted = reinterpret_cast<AnimatorTransitionInfo_t2534804151 *>(__this + 1);
	return AnimatorTransitionInfo_get_exit_m3346751000(_thisAdjusted, method);
}
// System.Void UnityEngine.Application::CallLowMemory()
extern "C"  void Application_CallLowMemory_m2493184849 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Application_CallLowMemory_m2493184849_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	LowMemoryCallback_t4104246196 * V_0 = NULL;
	{
		LowMemoryCallback_t4104246196 * L_0 = ((Application_t1852185770_StaticFields*)Application_t1852185770_il2cpp_TypeInfo_var->static_fields)->get_lowMemory_0();
		V_0 = L_0;
		LowMemoryCallback_t4104246196 * L_1 = V_0;
		if (!L_1)
		{
			goto IL_0013;
		}
	}
	{
		LowMemoryCallback_t4104246196 * L_2 = V_0;
		NullCheck(L_2);
		LowMemoryCallback_Invoke_m3804674182(L_2, /*hidden argument*/NULL);
	}

IL_0013:
	{
		return;
	}
}
// System.Void UnityEngine.Application::Quit()
extern "C"  void Application_Quit_m1672997886 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef void (*Application_Quit_m1672997886_ftn) ();
	static Application_Quit_m1672997886_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Application_Quit_m1672997886_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Application::Quit()");
	_il2cpp_icall_func();
}
// System.Boolean UnityEngine.Application::get_isPlaying()
extern "C"  bool Application_get_isPlaying_m2058545906 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef bool (*Application_get_isPlaying_m2058545906_ftn) ();
	static Application_get_isPlaying_m2058545906_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Application_get_isPlaying_m2058545906_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Application::get_isPlaying()");
	return _il2cpp_icall_func();
}
// System.Boolean UnityEngine.Application::get_isEditor()
extern "C"  bool Application_get_isEditor_m3427485980 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef bool (*Application_get_isEditor_m3427485980_ftn) ();
	static Application_get_isEditor_m3427485980_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Application_get_isEditor_m3427485980_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Application::get_isEditor()");
	return _il2cpp_icall_func();
}
// UnityEngine.RuntimePlatform UnityEngine.Application::get_platform()
extern "C"  int32_t Application_get_platform_m3724116297 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef int32_t (*Application_get_platform_m3724116297_ftn) ();
	static Application_get_platform_m3724116297_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Application_get_platform_m3724116297_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Application::get_platform()");
	return _il2cpp_icall_func();
}
// System.Void UnityEngine.Application::set_runInBackground(System.Boolean)
extern "C"  void Application_set_runInBackground_m1039003250 (Il2CppObject * __this /* static, unused */, bool ___value0, const MethodInfo* method)
{
	typedef void (*Application_set_runInBackground_m1039003250_ftn) (bool);
	static Application_set_runInBackground_m1039003250_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Application_set_runInBackground_m1039003250_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Application::set_runInBackground(System.Boolean)");
	_il2cpp_icall_func(___value0);
}
// System.String UnityEngine.Application::get_dataPath()
extern "C"  String_t* Application_get_dataPath_m518352432 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef String_t* (*Application_get_dataPath_m518352432_ftn) ();
	static Application_get_dataPath_m518352432_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Application_get_dataPath_m518352432_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Application::get_dataPath()");
	return _il2cpp_icall_func();
}
// System.String UnityEngine.Application::get_streamingAssetsPath()
extern "C"  String_t* Application_get_streamingAssetsPath_m3555335307 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef String_t* (*Application_get_streamingAssetsPath_m3555335307_ftn) ();
	static Application_get_streamingAssetsPath_m3555335307_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Application_get_streamingAssetsPath_m3555335307_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Application::get_streamingAssetsPath()");
	return _il2cpp_icall_func();
}
// System.String UnityEngine.Application::get_persistentDataPath()
extern "C"  String_t* Application_get_persistentDataPath_m1664523141 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef String_t* (*Application_get_persistentDataPath_m1664523141_ftn) ();
	static Application_get_persistentDataPath_m1664523141_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Application_get_persistentDataPath_m1664523141_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Application::get_persistentDataPath()");
	return _il2cpp_icall_func();
}
// System.String UnityEngine.Application::get_unityVersion()
extern "C"  String_t* Application_get_unityVersion_m1905323324 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef String_t* (*Application_get_unityVersion_m1905323324_ftn) ();
	static Application_get_unityVersion_m1905323324_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Application_get_unityVersion_m1905323324_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Application::get_unityVersion()");
	return _il2cpp_icall_func();
}
// System.Void UnityEngine.Application::OpenURL(System.String)
extern "C"  void Application_OpenURL_m1638057963 (Il2CppObject * __this /* static, unused */, String_t* ___url0, const MethodInfo* method)
{
	typedef void (*Application_OpenURL_m1638057963_ftn) (String_t*);
	static Application_OpenURL_m1638057963_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Application_OpenURL_m1638057963_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Application::OpenURL(System.String)");
	_il2cpp_icall_func(___url0);
}
// System.Int32 UnityEngine.Application::get_targetFrameRate()
extern "C"  int32_t Application_get_targetFrameRate_m490886681 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef int32_t (*Application_get_targetFrameRate_m490886681_ftn) ();
	static Application_get_targetFrameRate_m490886681_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Application_get_targetFrameRate_m490886681_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Application::get_targetFrameRate()");
	return _il2cpp_icall_func();
}
// System.Void UnityEngine.Application::set_targetFrameRate(System.Int32)
extern "C"  void Application_set_targetFrameRate_m3311620469 (Il2CppObject * __this /* static, unused */, int32_t ___value0, const MethodInfo* method)
{
	typedef void (*Application_set_targetFrameRate_m3311620469_ftn) (int32_t);
	static Application_set_targetFrameRate_m3311620469_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Application_set_targetFrameRate_m3311620469_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Application::set_targetFrameRate(System.Int32)");
	_il2cpp_icall_func(___value0);
}
// System.Void UnityEngine.Application::CallLogCallback(System.String,System.String,UnityEngine.LogType,System.Boolean)
extern "C"  void Application_CallLogCallback_m3663471574 (Il2CppObject * __this /* static, unused */, String_t* ___logString0, String_t* ___stackTrace1, int32_t ___type2, bool ___invokedOnMainThread3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Application_CallLogCallback_m3663471574_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	LogCallback_t3588208630 * V_0 = NULL;
	LogCallback_t3588208630 * V_1 = NULL;
	{
		bool L_0 = ___invokedOnMainThread3;
		if (!L_0)
		{
			goto IL_001e;
		}
	}
	{
		LogCallback_t3588208630 * L_1 = ((Application_t1852185770_StaticFields*)Application_t1852185770_il2cpp_TypeInfo_var->static_fields)->get_s_LogCallbackHandler_1();
		V_0 = L_1;
		LogCallback_t3588208630 * L_2 = V_0;
		if (!L_2)
		{
			goto IL_001d;
		}
	}
	{
		LogCallback_t3588208630 * L_3 = V_0;
		String_t* L_4 = ___logString0;
		String_t* L_5 = ___stackTrace1;
		int32_t L_6 = ___type2;
		NullCheck(L_3);
		LogCallback_Invoke_m3527969146(L_3, L_4, L_5, L_6, /*hidden argument*/NULL);
	}

IL_001d:
	{
	}

IL_001e:
	{
		LogCallback_t3588208630 * L_7 = ((Application_t1852185770_StaticFields*)Application_t1852185770_il2cpp_TypeInfo_var->static_fields)->get_s_LogCallbackHandlerThreaded_2();
		V_1 = L_7;
		LogCallback_t3588208630 * L_8 = V_1;
		if (!L_8)
		{
			goto IL_0033;
		}
	}
	{
		LogCallback_t3588208630 * L_9 = V_1;
		String_t* L_10 = ___logString0;
		String_t* L_11 = ___stackTrace1;
		int32_t L_12 = ___type2;
		NullCheck(L_9);
		LogCallback_Invoke_m3527969146(L_9, L_10, L_11, L_12, /*hidden argument*/NULL);
	}

IL_0033:
	{
		return;
	}
}
extern "C"  void DelegatePInvokeWrapper_LogCallback_t3588208630 (LogCallback_t3588208630 * __this, String_t* ___condition0, String_t* ___stackTrace1, int32_t ___type2, const MethodInfo* method)
{
	typedef void (STDCALL *PInvokeFunc)(char*, char*, int32_t);
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(((Il2CppDelegate*)__this)->method->methodPointer);

	// Marshaling of parameter '___condition0' to native representation
	char* ____condition0_marshaled = NULL;
	____condition0_marshaled = il2cpp_codegen_marshal_string(___condition0);

	// Marshaling of parameter '___stackTrace1' to native representation
	char* ____stackTrace1_marshaled = NULL;
	____stackTrace1_marshaled = il2cpp_codegen_marshal_string(___stackTrace1);

	// Native function invocation
	il2cppPInvokeFunc(____condition0_marshaled, ____stackTrace1_marshaled, ___type2);

	// Marshaling cleanup of parameter '___condition0' native representation
	il2cpp_codegen_marshal_free(____condition0_marshaled);
	____condition0_marshaled = NULL;

	// Marshaling cleanup of parameter '___stackTrace1' native representation
	il2cpp_codegen_marshal_free(____stackTrace1_marshaled);
	____stackTrace1_marshaled = NULL;

}
// System.Void UnityEngine.Application/LogCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void LogCallback__ctor_m1768990581 (LogCallback_t3588208630 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void UnityEngine.Application/LogCallback::Invoke(System.String,System.String,UnityEngine.LogType)
extern "C"  void LogCallback_Invoke_m3527969146 (LogCallback_t3588208630 * __this, String_t* ___condition0, String_t* ___stackTrace1, int32_t ___type2, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		LogCallback_Invoke_m3527969146((LogCallback_t3588208630 *)__this->get_prev_9(),___condition0, ___stackTrace1, ___type2, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, String_t* ___condition0, String_t* ___stackTrace1, int32_t ___type2, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___condition0, ___stackTrace1, ___type2,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (void* __this, String_t* ___condition0, String_t* ___stackTrace1, int32_t ___type2, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___condition0, ___stackTrace1, ___type2,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, String_t* ___stackTrace1, int32_t ___type2, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(___condition0, ___stackTrace1, ___type2,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult UnityEngine.Application/LogCallback::BeginInvoke(System.String,System.String,UnityEngine.LogType,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * LogCallback_BeginInvoke_m3624267429 (LogCallback_t3588208630 * __this, String_t* ___condition0, String_t* ___stackTrace1, int32_t ___type2, AsyncCallback_t3962456242 * ___callback3, Il2CppObject * ___object4, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (LogCallback_BeginInvoke_m3624267429_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[4] = {0};
	__d_args[0] = ___condition0;
	__d_args[1] = ___stackTrace1;
	__d_args[2] = Box(LogType_t73765434_il2cpp_TypeInfo_var, &___type2);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback3, (Il2CppObject*)___object4);
}
// System.Void UnityEngine.Application/LogCallback::EndInvoke(System.IAsyncResult)
extern "C"  void LogCallback_EndInvoke_m146286088 (LogCallback_t3588208630 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
extern "C"  void DelegatePInvokeWrapper_LowMemoryCallback_t4104246196 (LowMemoryCallback_t4104246196 * __this, const MethodInfo* method)
{
	typedef void (STDCALL *PInvokeFunc)();
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(((Il2CppDelegate*)__this)->method->methodPointer);

	// Native function invocation
	il2cppPInvokeFunc();

}
// System.Void UnityEngine.Application/LowMemoryCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void LowMemoryCallback__ctor_m383620046 (LowMemoryCallback_t4104246196 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void UnityEngine.Application/LowMemoryCallback::Invoke()
extern "C"  void LowMemoryCallback_Invoke_m3804674182 (LowMemoryCallback_t4104246196 * __this, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		LowMemoryCallback_Invoke_m3804674182((LowMemoryCallback_t4104246196 *)__this->get_prev_9(), method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if ((__this->get_m_target_2() != NULL || MethodHasParameters((MethodInfo*)(__this->get_method_3().get_m_value_0()))) && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult UnityEngine.Application/LowMemoryCallback::BeginInvoke(System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * LowMemoryCallback_BeginInvoke_m3257921260 (LowMemoryCallback_t4104246196 * __this, AsyncCallback_t3962456242 * ___callback0, Il2CppObject * ___object1, const MethodInfo* method)
{
	void *__d_args[1] = {0};
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback0, (Il2CppObject*)___object1);
}
// System.Void UnityEngine.Application/LowMemoryCallback::EndInvoke(System.IAsyncResult)
extern "C"  void LowMemoryCallback_EndInvoke_m3679420672 (LowMemoryCallback_t4104246196 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void UnityEngine.AssemblyIsEditorAssembly::.ctor()
extern "C"  void AssemblyIsEditorAssembly__ctor_m1417313067 (AssemblyIsEditorAssembly_t3442416807 * __this, const MethodInfo* method)
{
	{
		Attribute__ctor_m1529526131(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Object[] UnityEngine.AssetBundle::LoadAssetWithSubAssets_Internal(System.String,System.Type)
extern "C"  ObjectU5BU5D_t1417781964* AssetBundle_LoadAssetWithSubAssets_Internal_m3736220809 (AssetBundle_t1153907252 * __this, String_t* ___name0, Type_t * ___type1, const MethodInfo* method)
{
	typedef ObjectU5BU5D_t1417781964* (*AssetBundle_LoadAssetWithSubAssets_Internal_m3736220809_ftn) (AssetBundle_t1153907252 *, String_t*, Type_t *);
	static AssetBundle_LoadAssetWithSubAssets_Internal_m3736220809_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AssetBundle_LoadAssetWithSubAssets_Internal_m3736220809_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AssetBundle::LoadAssetWithSubAssets_Internal(System.String,System.Type)");
	return _il2cpp_icall_func(__this, ___name0, ___type1);
}
// UnityEngine.AssetBundleRequest UnityEngine.AssetBundle::LoadAssetWithSubAssetsAsync_Internal(System.String,System.Type)
extern "C"  AssetBundleRequest_t699759206 * AssetBundle_LoadAssetWithSubAssetsAsync_Internal_m753058609 (AssetBundle_t1153907252 * __this, String_t* ___name0, Type_t * ___type1, const MethodInfo* method)
{
	typedef AssetBundleRequest_t699759206 * (*AssetBundle_LoadAssetWithSubAssetsAsync_Internal_m753058609_ftn) (AssetBundle_t1153907252 *, String_t*, Type_t *);
	static AssetBundle_LoadAssetWithSubAssetsAsync_Internal_m753058609_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AssetBundle_LoadAssetWithSubAssetsAsync_Internal_m753058609_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AssetBundle::LoadAssetWithSubAssetsAsync_Internal(System.String,System.Type)");
	return _il2cpp_icall_func(__this, ___name0, ___type1);
}
// UnityEngine.Object[] UnityEngine.AssetBundle::LoadAllAssets(System.Type)
extern "C"  ObjectU5BU5D_t1417781964* AssetBundle_LoadAllAssets_m3416039552 (AssetBundle_t1153907252 * __this, Type_t * ___type0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AssetBundle_LoadAllAssets_m3416039552_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ObjectU5BU5D_t1417781964* V_0 = NULL;
	{
		Type_t * L_0 = ___type0;
		if (L_0)
		{
			goto IL_0013;
		}
	}
	{
		NullReferenceException_t1023182353 * L_1 = (NullReferenceException_t1023182353 *)il2cpp_codegen_object_new(NullReferenceException_t1023182353_il2cpp_TypeInfo_var);
		NullReferenceException__ctor_m3076065613(L_1, _stringLiteral1402148072, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0013:
	{
		Type_t * L_2 = ___type0;
		ObjectU5BU5D_t1417781964* L_3 = AssetBundle_LoadAssetWithSubAssets_Internal_m3736220809(__this, _stringLiteral757602046, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		goto IL_0025;
	}

IL_0025:
	{
		ObjectU5BU5D_t1417781964* L_4 = V_0;
		return L_4;
	}
}
// UnityEngine.AssetBundleRequest UnityEngine.AssetBundle::LoadAllAssetsAsync(System.Type)
extern "C"  AssetBundleRequest_t699759206 * AssetBundle_LoadAllAssetsAsync_m609312961 (AssetBundle_t1153907252 * __this, Type_t * ___type0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AssetBundle_LoadAllAssetsAsync_m609312961_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	AssetBundleRequest_t699759206 * V_0 = NULL;
	{
		Type_t * L_0 = ___type0;
		if (L_0)
		{
			goto IL_0013;
		}
	}
	{
		NullReferenceException_t1023182353 * L_1 = (NullReferenceException_t1023182353 *)il2cpp_codegen_object_new(NullReferenceException_t1023182353_il2cpp_TypeInfo_var);
		NullReferenceException__ctor_m3076065613(L_1, _stringLiteral1402148072, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0013:
	{
		Type_t * L_2 = ___type0;
		AssetBundleRequest_t699759206 * L_3 = AssetBundle_LoadAssetWithSubAssetsAsync_Internal_m753058609(__this, _stringLiteral757602046, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		goto IL_0025;
	}

IL_0025:
	{
		AssetBundleRequest_t699759206 * L_4 = V_0;
		return L_4;
	}
}
// System.Void UnityEngine.AssetBundle::Unload(System.Boolean)
extern "C"  void AssetBundle_Unload_m2439560418 (AssetBundle_t1153907252 * __this, bool ___unloadAllLoadedObjects0, const MethodInfo* method)
{
	typedef void (*AssetBundle_Unload_m2439560418_ftn) (AssetBundle_t1153907252 *, bool);
	static AssetBundle_Unload_m2439560418_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AssetBundle_Unload_m2439560418_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AssetBundle::Unload(System.Boolean)");
	_il2cpp_icall_func(__this, ___unloadAllLoadedObjects0);
}
// System.Void UnityEngine.AssetBundleCreateRequest::.ctor()
extern "C"  void AssetBundleCreateRequest__ctor_m172787386 (AssetBundleCreateRequest_t3119663542 * __this, const MethodInfo* method)
{
	{
		AsyncOperation__ctor_m2683541089(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.AssetBundle UnityEngine.AssetBundleCreateRequest::get_assetBundle()
extern "C"  AssetBundle_t1153907252 * AssetBundleCreateRequest_get_assetBundle_m3071837008 (AssetBundleCreateRequest_t3119663542 * __this, const MethodInfo* method)
{
	typedef AssetBundle_t1153907252 * (*AssetBundleCreateRequest_get_assetBundle_m3071837008_ftn) (AssetBundleCreateRequest_t3119663542 *);
	static AssetBundleCreateRequest_get_assetBundle_m3071837008_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AssetBundleCreateRequest_get_assetBundle_m3071837008_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AssetBundleCreateRequest::get_assetBundle()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.AssetBundleCreateRequest::DisableCompatibilityChecks()
extern "C"  void AssetBundleCreateRequest_DisableCompatibilityChecks_m1783260612 (AssetBundleCreateRequest_t3119663542 * __this, const MethodInfo* method)
{
	typedef void (*AssetBundleCreateRequest_DisableCompatibilityChecks_m1783260612_ftn) (AssetBundleCreateRequest_t3119663542 *);
	static AssetBundleCreateRequest_DisableCompatibilityChecks_m1783260612_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AssetBundleCreateRequest_DisableCompatibilityChecks_m1783260612_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AssetBundleCreateRequest::DisableCompatibilityChecks()");
	_il2cpp_icall_func(__this);
}
// Conversion methods for marshalling of: UnityEngine.AssetBundleRequest
extern "C" void AssetBundleRequest_t699759206_marshal_pinvoke(const AssetBundleRequest_t699759206& unmarshaled, AssetBundleRequest_t699759206_marshaled_pinvoke& marshaled)
{
	marshaled.___m_Ptr_0 = reinterpret_cast<intptr_t>((unmarshaled.get_m_Ptr_0()).get_m_value_0());
}
extern "C" void AssetBundleRequest_t699759206_marshal_pinvoke_back(const AssetBundleRequest_t699759206_marshaled_pinvoke& marshaled, AssetBundleRequest_t699759206& unmarshaled)
{
	IntPtr_t unmarshaled_m_Ptr_temp_0;
	memset(&unmarshaled_m_Ptr_temp_0, 0, sizeof(unmarshaled_m_Ptr_temp_0));
	IntPtr_t unmarshaled_m_Ptr_temp_0_temp;
	unmarshaled_m_Ptr_temp_0_temp.set_m_value_0(reinterpret_cast<void*>((intptr_t)(marshaled.___m_Ptr_0)));
	unmarshaled_m_Ptr_temp_0 = unmarshaled_m_Ptr_temp_0_temp;
	unmarshaled.set_m_Ptr_0(unmarshaled_m_Ptr_temp_0);
}
// Conversion method for clean up from marshalling of: UnityEngine.AssetBundleRequest
extern "C" void AssetBundleRequest_t699759206_marshal_pinvoke_cleanup(AssetBundleRequest_t699759206_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.AssetBundleRequest
extern "C" void AssetBundleRequest_t699759206_marshal_com(const AssetBundleRequest_t699759206& unmarshaled, AssetBundleRequest_t699759206_marshaled_com& marshaled)
{
	marshaled.___m_Ptr_0 = reinterpret_cast<intptr_t>((unmarshaled.get_m_Ptr_0()).get_m_value_0());
}
extern "C" void AssetBundleRequest_t699759206_marshal_com_back(const AssetBundleRequest_t699759206_marshaled_com& marshaled, AssetBundleRequest_t699759206& unmarshaled)
{
	IntPtr_t unmarshaled_m_Ptr_temp_0;
	memset(&unmarshaled_m_Ptr_temp_0, 0, sizeof(unmarshaled_m_Ptr_temp_0));
	IntPtr_t unmarshaled_m_Ptr_temp_0_temp;
	unmarshaled_m_Ptr_temp_0_temp.set_m_value_0(reinterpret_cast<void*>((intptr_t)(marshaled.___m_Ptr_0)));
	unmarshaled_m_Ptr_temp_0 = unmarshaled_m_Ptr_temp_0_temp;
	unmarshaled.set_m_Ptr_0(unmarshaled_m_Ptr_temp_0);
}
// Conversion method for clean up from marshalling of: UnityEngine.AssetBundleRequest
extern "C" void AssetBundleRequest_t699759206_marshal_com_cleanup(AssetBundleRequest_t699759206_marshaled_com& marshaled)
{
}
// System.Void UnityEngine.AssetBundleRequest::.ctor()
extern "C"  void AssetBundleRequest__ctor_m1029262113 (AssetBundleRequest_t699759206 * __this, const MethodInfo* method)
{
	{
		AsyncOperation__ctor_m2683541089(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Object UnityEngine.AssetBundleRequest::get_asset()
extern "C"  Object_t631007953 * AssetBundleRequest_get_asset_m1682200277 (AssetBundleRequest_t699759206 * __this, const MethodInfo* method)
{
	typedef Object_t631007953 * (*AssetBundleRequest_get_asset_m1682200277_ftn) (AssetBundleRequest_t699759206 *);
	static AssetBundleRequest_get_asset_m1682200277_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AssetBundleRequest_get_asset_m1682200277_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AssetBundleRequest::get_asset()");
	return _il2cpp_icall_func(__this);
}
// UnityEngine.Object[] UnityEngine.AssetBundleRequest::get_allAssets()
extern "C"  ObjectU5BU5D_t1417781964* AssetBundleRequest_get_allAssets_m4044557684 (AssetBundleRequest_t699759206 * __this, const MethodInfo* method)
{
	typedef ObjectU5BU5D_t1417781964* (*AssetBundleRequest_get_allAssets_m4044557684_ftn) (AssetBundleRequest_t699759206 *);
	static AssetBundleRequest_get_allAssets_m4044557684_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AssetBundleRequest_get_allAssets_m4044557684_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AssetBundleRequest::get_allAssets()");
	return _il2cpp_icall_func(__this);
}
// Conversion methods for marshalling of: UnityEngine.AsyncOperation
extern "C" void AsyncOperation_t1445031843_marshal_pinvoke(const AsyncOperation_t1445031843& unmarshaled, AsyncOperation_t1445031843_marshaled_pinvoke& marshaled)
{
	marshaled.___m_Ptr_0 = reinterpret_cast<intptr_t>((unmarshaled.get_m_Ptr_0()).get_m_value_0());
}
extern "C" void AsyncOperation_t1445031843_marshal_pinvoke_back(const AsyncOperation_t1445031843_marshaled_pinvoke& marshaled, AsyncOperation_t1445031843& unmarshaled)
{
	IntPtr_t unmarshaled_m_Ptr_temp_0;
	memset(&unmarshaled_m_Ptr_temp_0, 0, sizeof(unmarshaled_m_Ptr_temp_0));
	IntPtr_t unmarshaled_m_Ptr_temp_0_temp;
	unmarshaled_m_Ptr_temp_0_temp.set_m_value_0(reinterpret_cast<void*>((intptr_t)(marshaled.___m_Ptr_0)));
	unmarshaled_m_Ptr_temp_0 = unmarshaled_m_Ptr_temp_0_temp;
	unmarshaled.set_m_Ptr_0(unmarshaled_m_Ptr_temp_0);
}
// Conversion method for clean up from marshalling of: UnityEngine.AsyncOperation
extern "C" void AsyncOperation_t1445031843_marshal_pinvoke_cleanup(AsyncOperation_t1445031843_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.AsyncOperation
extern "C" void AsyncOperation_t1445031843_marshal_com(const AsyncOperation_t1445031843& unmarshaled, AsyncOperation_t1445031843_marshaled_com& marshaled)
{
	marshaled.___m_Ptr_0 = reinterpret_cast<intptr_t>((unmarshaled.get_m_Ptr_0()).get_m_value_0());
}
extern "C" void AsyncOperation_t1445031843_marshal_com_back(const AsyncOperation_t1445031843_marshaled_com& marshaled, AsyncOperation_t1445031843& unmarshaled)
{
	IntPtr_t unmarshaled_m_Ptr_temp_0;
	memset(&unmarshaled_m_Ptr_temp_0, 0, sizeof(unmarshaled_m_Ptr_temp_0));
	IntPtr_t unmarshaled_m_Ptr_temp_0_temp;
	unmarshaled_m_Ptr_temp_0_temp.set_m_value_0(reinterpret_cast<void*>((intptr_t)(marshaled.___m_Ptr_0)));
	unmarshaled_m_Ptr_temp_0 = unmarshaled_m_Ptr_temp_0_temp;
	unmarshaled.set_m_Ptr_0(unmarshaled_m_Ptr_temp_0);
}
// Conversion method for clean up from marshalling of: UnityEngine.AsyncOperation
extern "C" void AsyncOperation_t1445031843_marshal_com_cleanup(AsyncOperation_t1445031843_marshaled_com& marshaled)
{
}
// System.Void UnityEngine.AsyncOperation::.ctor()
extern "C"  void AsyncOperation__ctor_m2683541089 (AsyncOperation_t1445031843 * __this, const MethodInfo* method)
{
	{
		YieldInstruction__ctor_m3893172416(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.AsyncOperation::InternalDestroy()
extern "C"  void AsyncOperation_InternalDestroy_m2288495658 (AsyncOperation_t1445031843 * __this, const MethodInfo* method)
{
	typedef void (*AsyncOperation_InternalDestroy_m2288495658_ftn) (AsyncOperation_t1445031843 *);
	static AsyncOperation_InternalDestroy_m2288495658_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AsyncOperation_InternalDestroy_m2288495658_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AsyncOperation::InternalDestroy()");
	_il2cpp_icall_func(__this);
}
// System.Void UnityEngine.AsyncOperation::Finalize()
extern "C"  void AsyncOperation_Finalize_m1338204656 (AsyncOperation_t1445031843 * __this, const MethodInfo* method)
{
	Exception_t1436737249 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1436737249 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
	}

IL_0001:
	try
	{ // begin try (depth: 1)
		AsyncOperation_InternalDestroy_m2288495658(__this, /*hidden argument*/NULL);
		IL2CPP_LEAVE(0x13, FINALLY_000c);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1436737249 *)e.ex;
		goto FINALLY_000c;
	}

FINALLY_000c:
	{ // begin finally (depth: 1)
		Object_Finalize_m3076187857(__this, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(12)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(12)
	{
		IL2CPP_JUMP_TBL(0x13, IL_0013)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1436737249 *)
	}

IL_0013:
	{
		return;
	}
}
// System.Boolean UnityEngine.AsyncOperation::get_isDone()
extern "C"  bool AsyncOperation_get_isDone_m2309912918 (AsyncOperation_t1445031843 * __this, const MethodInfo* method)
{
	typedef bool (*AsyncOperation_get_isDone_m2309912918_ftn) (AsyncOperation_t1445031843 *);
	static AsyncOperation_get_isDone_m2309912918_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AsyncOperation_get_isDone_m2309912918_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AsyncOperation::get_isDone()");
	return _il2cpp_icall_func(__this);
}
// System.Single UnityEngine.AsyncOperation::get_progress()
extern "C"  float AsyncOperation_get_progress_m2298969202 (AsyncOperation_t1445031843 * __this, const MethodInfo* method)
{
	typedef float (*AsyncOperation_get_progress_m2298969202_ftn) (AsyncOperation_t1445031843 *);
	static AsyncOperation_get_progress_m2298969202_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AsyncOperation_get_progress_m2298969202_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AsyncOperation::get_progress()");
	return _il2cpp_icall_func(__this);
}
// System.Int32 UnityEngine.AsyncOperation::get_priority()
extern "C"  int32_t AsyncOperation_get_priority_m4186284848 (AsyncOperation_t1445031843 * __this, const MethodInfo* method)
{
	typedef int32_t (*AsyncOperation_get_priority_m4186284848_ftn) (AsyncOperation_t1445031843 *);
	static AsyncOperation_get_priority_m4186284848_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AsyncOperation_get_priority_m4186284848_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AsyncOperation::get_priority()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.AsyncOperation::set_priority(System.Int32)
extern "C"  void AsyncOperation_set_priority_m1451852981 (AsyncOperation_t1445031843 * __this, int32_t ___value0, const MethodInfo* method)
{
	typedef void (*AsyncOperation_set_priority_m1451852981_ftn) (AsyncOperation_t1445031843 *, int32_t);
	static AsyncOperation_set_priority_m1451852981_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AsyncOperation_set_priority_m1451852981_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AsyncOperation::set_priority(System.Int32)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Boolean UnityEngine.AsyncOperation::get_allowSceneActivation()
extern "C"  bool AsyncOperation_get_allowSceneActivation_m2258427089 (AsyncOperation_t1445031843 * __this, const MethodInfo* method)
{
	typedef bool (*AsyncOperation_get_allowSceneActivation_m2258427089_ftn) (AsyncOperation_t1445031843 *);
	static AsyncOperation_get_allowSceneActivation_m2258427089_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AsyncOperation_get_allowSceneActivation_m2258427089_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AsyncOperation::get_allowSceneActivation()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.AsyncOperation::set_allowSceneActivation(System.Boolean)
extern "C"  void AsyncOperation_set_allowSceneActivation_m1319035743 (AsyncOperation_t1445031843 * __this, bool ___value0, const MethodInfo* method)
{
	typedef void (*AsyncOperation_set_allowSceneActivation_m1319035743_ftn) (AsyncOperation_t1445031843 *, bool);
	static AsyncOperation_set_allowSceneActivation_m1319035743_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AsyncOperation_set_allowSceneActivation_m1319035743_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AsyncOperation::set_allowSceneActivation(System.Boolean)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Type UnityEngine.AttributeHelperEngine::GetParentTypeDisallowingMultipleInclusion(System.Type)
extern "C"  Type_t * AttributeHelperEngine_GetParentTypeDisallowingMultipleInclusion_m3140293344 (Il2CppObject * __this /* static, unused */, Type_t * ___type0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AttributeHelperEngine_GetParentTypeDisallowingMultipleInclusion_m3140293344_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Stack_1_t3327334215 * V_0 = NULL;
	Type_t * V_1 = NULL;
	ObjectU5BU5D_t2843939325* V_2 = NULL;
	int32_t V_3 = 0;
	Type_t * V_4 = NULL;
	{
		Stack_1_t3327334215 * L_0 = (Stack_1_t3327334215 *)il2cpp_codegen_object_new(Stack_1_t3327334215_il2cpp_TypeInfo_var);
		Stack_1__ctor_m926334451(L_0, /*hidden argument*/Stack_1__ctor_m926334451_MethodInfo_var);
		V_0 = L_0;
		goto IL_001d;
	}

IL_000c:
	{
		Stack_1_t3327334215 * L_1 = V_0;
		Type_t * L_2 = ___type0;
		NullCheck(L_1);
		Stack_1_Push_m4005337547(L_1, L_2, /*hidden argument*/Stack_1_Push_m4005337547_MethodInfo_var);
		Type_t * L_3 = ___type0;
		NullCheck(L_3);
		Type_t * L_4 = VirtFuncInvoker0< Type_t * >::Invoke(17 /* System.Type System.Type::get_BaseType() */, L_3);
		___type0 = L_4;
	}

IL_001d:
	{
		Type_t * L_5 = ___type0;
		if (!L_5)
		{
			goto IL_0033;
		}
	}
	{
		Type_t * L_6 = ___type0;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_7 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, LoadTypeToken(MonoBehaviour_t3962482529_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Il2CppObject*)(Type_t *)L_6) == ((Il2CppObject*)(Type_t *)L_7))))
		{
			goto IL_000c;
		}
	}

IL_0033:
	{
		V_1 = (Type_t *)NULL;
		goto IL_0067;
	}

IL_003a:
	{
		Stack_1_t3327334215 * L_8 = V_0;
		NullCheck(L_8);
		Type_t * L_9 = Stack_1_Pop_m4032335803(L_8, /*hidden argument*/Stack_1_Pop_m4032335803_MethodInfo_var);
		V_1 = L_9;
		Type_t * L_10 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_11 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, LoadTypeToken(DisallowMultipleComponent_t1422053217_0_0_0_var), /*hidden argument*/NULL);
		NullCheck(L_10);
		ObjectU5BU5D_t2843939325* L_12 = VirtFuncInvoker2< ObjectU5BU5D_t2843939325*, Type_t *, bool >::Invoke(13 /* System.Object[] System.Reflection.MemberInfo::GetCustomAttributes(System.Type,System.Boolean) */, L_10, L_11, (bool)0);
		V_2 = L_12;
		ObjectU5BU5D_t2843939325* L_13 = V_2;
		NullCheck(L_13);
		V_3 = (((int32_t)((int32_t)(((Il2CppArray *)L_13)->max_length))));
		int32_t L_14 = V_3;
		if (!L_14)
		{
			goto IL_0066;
		}
	}
	{
		Type_t * L_15 = V_1;
		V_4 = L_15;
		goto IL_007b;
	}

IL_0066:
	{
	}

IL_0067:
	{
		Stack_1_t3327334215 * L_16 = V_0;
		NullCheck(L_16);
		int32_t L_17 = Stack_1_get_Count_m95885128(L_16, /*hidden argument*/Stack_1_get_Count_m95885128_MethodInfo_var);
		if ((((int32_t)L_17) > ((int32_t)0)))
		{
			goto IL_003a;
		}
	}
	{
		V_4 = (Type_t *)NULL;
		goto IL_007b;
	}

IL_007b:
	{
		Type_t * L_18 = V_4;
		return L_18;
	}
}
// System.Type[] UnityEngine.AttributeHelperEngine::GetRequiredComponents(System.Type)
extern "C"  TypeU5BU5D_t3940880105* AttributeHelperEngine_GetRequiredComponents_m3351952333 (Il2CppObject * __this /* static, unused */, Type_t * ___klass0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AttributeHelperEngine_GetRequiredComponents_m3351952333_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	List_1_t3956019502 * V_0 = NULL;
	RequireComponentU5BU5D_t2245623724* V_1 = NULL;
	Type_t * V_2 = NULL;
	RequireComponent_t3490506609 * V_3 = NULL;
	RequireComponentU5BU5D_t2245623724* V_4 = NULL;
	int32_t V_5 = 0;
	TypeU5BU5D_t3940880105* V_6 = NULL;
	TypeU5BU5D_t3940880105* V_7 = NULL;
	{
		V_0 = (List_1_t3956019502 *)NULL;
		goto IL_00ef;
	}

IL_0008:
	{
		Type_t * L_0 = ___klass0;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_1 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, LoadTypeToken(RequireComponent_t3490506609_0_0_0_var), /*hidden argument*/NULL);
		NullCheck(L_0);
		ObjectU5BU5D_t2843939325* L_2 = VirtFuncInvoker2< ObjectU5BU5D_t2843939325*, Type_t *, bool >::Invoke(13 /* System.Object[] System.Reflection.MemberInfo::GetCustomAttributes(System.Type,System.Boolean) */, L_0, L_1, (bool)0);
		V_1 = ((RequireComponentU5BU5D_t2245623724*)Castclass(L_2, RequireComponentU5BU5D_t2245623724_il2cpp_TypeInfo_var));
		Type_t * L_3 = ___klass0;
		NullCheck(L_3);
		Type_t * L_4 = VirtFuncInvoker0< Type_t * >::Invoke(17 /* System.Type System.Type::get_BaseType() */, L_3);
		V_2 = L_4;
		RequireComponentU5BU5D_t2245623724* L_5 = V_1;
		V_4 = L_5;
		V_5 = 0;
		goto IL_00e0;
	}

IL_0033:
	{
		RequireComponentU5BU5D_t2245623724* L_6 = V_4;
		int32_t L_7 = V_5;
		NullCheck(L_6);
		int32_t L_8 = L_7;
		RequireComponent_t3490506609 * L_9 = (L_6)->GetAt(static_cast<il2cpp_array_size_t>(L_8));
		V_3 = L_9;
		List_1_t3956019502 * L_10 = V_0;
		if (L_10)
		{
			goto IL_0086;
		}
	}
	{
		RequireComponentU5BU5D_t2245623724* L_11 = V_1;
		NullCheck(L_11);
		if ((!(((uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_11)->max_length))))) == ((uint32_t)1))))
		{
			goto IL_0086;
		}
	}
	{
		Type_t * L_12 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_13 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, LoadTypeToken(MonoBehaviour_t3962482529_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Il2CppObject*)(Type_t *)L_12) == ((Il2CppObject*)(Type_t *)L_13))))
		{
			goto IL_0086;
		}
	}
	{
		TypeU5BU5D_t3940880105* L_14 = ((TypeU5BU5D_t3940880105*)SZArrayNew(TypeU5BU5D_t3940880105_il2cpp_TypeInfo_var, (uint32_t)3));
		RequireComponent_t3490506609 * L_15 = V_3;
		NullCheck(L_15);
		Type_t * L_16 = L_15->get_m_Type0_0();
		NullCheck(L_14);
		ArrayElementTypeCheck (L_14, L_16);
		(L_14)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_16);
		TypeU5BU5D_t3940880105* L_17 = L_14;
		RequireComponent_t3490506609 * L_18 = V_3;
		NullCheck(L_18);
		Type_t * L_19 = L_18->get_m_Type1_1();
		NullCheck(L_17);
		ArrayElementTypeCheck (L_17, L_19);
		(L_17)->SetAt(static_cast<il2cpp_array_size_t>(1), (Type_t *)L_19);
		TypeU5BU5D_t3940880105* L_20 = L_17;
		RequireComponent_t3490506609 * L_21 = V_3;
		NullCheck(L_21);
		Type_t * L_22 = L_21->get_m_Type2_2();
		NullCheck(L_20);
		ArrayElementTypeCheck (L_20, L_22);
		(L_20)->SetAt(static_cast<il2cpp_array_size_t>(2), (Type_t *)L_22);
		V_6 = L_20;
		TypeU5BU5D_t3940880105* L_23 = V_6;
		V_7 = L_23;
		goto IL_0120;
	}

IL_0086:
	{
		List_1_t3956019502 * L_24 = V_0;
		if (L_24)
		{
			goto IL_0093;
		}
	}
	{
		List_1_t3956019502 * L_25 = (List_1_t3956019502 *)il2cpp_codegen_object_new(List_1_t3956019502_il2cpp_TypeInfo_var);
		List_1__ctor_m696099607(L_25, /*hidden argument*/List_1__ctor_m696099607_MethodInfo_var);
		V_0 = L_25;
	}

IL_0093:
	{
		RequireComponent_t3490506609 * L_26 = V_3;
		NullCheck(L_26);
		Type_t * L_27 = L_26->get_m_Type0_0();
		if (!L_27)
		{
			goto IL_00aa;
		}
	}
	{
		List_1_t3956019502 * L_28 = V_0;
		RequireComponent_t3490506609 * L_29 = V_3;
		NullCheck(L_29);
		Type_t * L_30 = L_29->get_m_Type0_0();
		NullCheck(L_28);
		List_1_Add_m209710667(L_28, L_30, /*hidden argument*/List_1_Add_m209710667_MethodInfo_var);
	}

IL_00aa:
	{
		RequireComponent_t3490506609 * L_31 = V_3;
		NullCheck(L_31);
		Type_t * L_32 = L_31->get_m_Type1_1();
		if (!L_32)
		{
			goto IL_00c1;
		}
	}
	{
		List_1_t3956019502 * L_33 = V_0;
		RequireComponent_t3490506609 * L_34 = V_3;
		NullCheck(L_34);
		Type_t * L_35 = L_34->get_m_Type1_1();
		NullCheck(L_33);
		List_1_Add_m209710667(L_33, L_35, /*hidden argument*/List_1_Add_m209710667_MethodInfo_var);
	}

IL_00c1:
	{
		RequireComponent_t3490506609 * L_36 = V_3;
		NullCheck(L_36);
		Type_t * L_37 = L_36->get_m_Type2_2();
		if (!L_37)
		{
			goto IL_00d8;
		}
	}
	{
		List_1_t3956019502 * L_38 = V_0;
		RequireComponent_t3490506609 * L_39 = V_3;
		NullCheck(L_39);
		Type_t * L_40 = L_39->get_m_Type2_2();
		NullCheck(L_38);
		List_1_Add_m209710667(L_38, L_40, /*hidden argument*/List_1_Add_m209710667_MethodInfo_var);
	}

IL_00d8:
	{
		int32_t L_41 = V_5;
		V_5 = ((int32_t)((int32_t)L_41+(int32_t)1));
	}

IL_00e0:
	{
		int32_t L_42 = V_5;
		RequireComponentU5BU5D_t2245623724* L_43 = V_4;
		NullCheck(L_43);
		if ((((int32_t)L_42) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_43)->max_length)))))))
		{
			goto IL_0033;
		}
	}
	{
		Type_t * L_44 = V_2;
		___klass0 = L_44;
	}

IL_00ef:
	{
		Type_t * L_45 = ___klass0;
		if (!L_45)
		{
			goto IL_0105;
		}
	}
	{
		Type_t * L_46 = ___klass0;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_47 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, LoadTypeToken(MonoBehaviour_t3962482529_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Il2CppObject*)(Type_t *)L_46) == ((Il2CppObject*)(Type_t *)L_47))))
		{
			goto IL_0008;
		}
	}

IL_0105:
	{
		List_1_t3956019502 * L_48 = V_0;
		if (L_48)
		{
			goto IL_0113;
		}
	}
	{
		V_7 = (TypeU5BU5D_t3940880105*)NULL;
		goto IL_0120;
	}

IL_0113:
	{
		List_1_t3956019502 * L_49 = V_0;
		NullCheck(L_49);
		TypeU5BU5D_t3940880105* L_50 = List_1_ToArray_m1314877377(L_49, /*hidden argument*/List_1_ToArray_m1314877377_MethodInfo_var);
		V_7 = L_50;
		goto IL_0120;
	}

IL_0120:
	{
		TypeU5BU5D_t3940880105* L_51 = V_7;
		return L_51;
	}
}
// System.Boolean UnityEngine.AttributeHelperEngine::CheckIsEditorScript(System.Type)
extern "C"  bool AttributeHelperEngine_CheckIsEditorScript_m2101700955 (Il2CppObject * __this /* static, unused */, Type_t * ___klass0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AttributeHelperEngine_CheckIsEditorScript_m2101700955_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ObjectU5BU5D_t2843939325* V_0 = NULL;
	int32_t V_1 = 0;
	bool V_2 = false;
	{
		goto IL_0033;
	}

IL_0006:
	{
		Type_t * L_0 = ___klass0;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_1 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, LoadTypeToken(ExecuteInEditMode_t3727731349_0_0_0_var), /*hidden argument*/NULL);
		NullCheck(L_0);
		ObjectU5BU5D_t2843939325* L_2 = VirtFuncInvoker2< ObjectU5BU5D_t2843939325*, Type_t *, bool >::Invoke(13 /* System.Object[] System.Reflection.MemberInfo::GetCustomAttributes(System.Type,System.Boolean) */, L_0, L_1, (bool)0);
		V_0 = L_2;
		ObjectU5BU5D_t2843939325* L_3 = V_0;
		NullCheck(L_3);
		V_1 = (((int32_t)((int32_t)(((Il2CppArray *)L_3)->max_length))));
		int32_t L_4 = V_1;
		if (!L_4)
		{
			goto IL_002a;
		}
	}
	{
		V_2 = (bool)1;
		goto IL_0050;
	}

IL_002a:
	{
		Type_t * L_5 = ___klass0;
		NullCheck(L_5);
		Type_t * L_6 = VirtFuncInvoker0< Type_t * >::Invoke(17 /* System.Type System.Type::get_BaseType() */, L_5);
		___klass0 = L_6;
	}

IL_0033:
	{
		Type_t * L_7 = ___klass0;
		if (!L_7)
		{
			goto IL_0049;
		}
	}
	{
		Type_t * L_8 = ___klass0;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_9 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, LoadTypeToken(MonoBehaviour_t3962482529_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Il2CppObject*)(Type_t *)L_8) == ((Il2CppObject*)(Type_t *)L_9))))
		{
			goto IL_0006;
		}
	}

IL_0049:
	{
		V_2 = (bool)0;
		goto IL_0050;
	}

IL_0050:
	{
		bool L_10 = V_2;
		return L_10;
	}
}
// System.Int32 UnityEngine.AttributeHelperEngine::GetDefaultExecutionOrderFor(System.Type)
extern "C"  int32_t AttributeHelperEngine_GetDefaultExecutionOrderFor_m118922855 (Il2CppObject * __this /* static, unused */, Type_t * ___klass0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AttributeHelperEngine_GetDefaultExecutionOrderFor_m118922855_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	DefaultExecutionOrder_t3059642329 * V_0 = NULL;
	int32_t V_1 = 0;
	{
		Type_t * L_0 = ___klass0;
		IL2CPP_RUNTIME_CLASS_INIT(AttributeHelperEngine_t2735742303_il2cpp_TypeInfo_var);
		DefaultExecutionOrder_t3059642329 * L_1 = AttributeHelperEngine_GetCustomAttributeOfType_TisDefaultExecutionOrder_t3059642329_m1616528635(NULL /*static, unused*/, L_0, /*hidden argument*/AttributeHelperEngine_GetCustomAttributeOfType_TisDefaultExecutionOrder_t3059642329_m1616528635_MethodInfo_var);
		V_0 = L_1;
		DefaultExecutionOrder_t3059642329 * L_2 = V_0;
		if (L_2)
		{
			goto IL_0015;
		}
	}
	{
		V_1 = 0;
		goto IL_0021;
	}

IL_0015:
	{
		DefaultExecutionOrder_t3059642329 * L_3 = V_0;
		NullCheck(L_3);
		int32_t L_4 = DefaultExecutionOrder_get_order_m2144583152(L_3, /*hidden argument*/NULL);
		V_1 = L_4;
		goto IL_0021;
	}

IL_0021:
	{
		int32_t L_5 = V_1;
		return L_5;
	}
}
// System.Void UnityEngine.AttributeHelperEngine::.cctor()
extern "C"  void AttributeHelperEngine__cctor_m2980610902 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AttributeHelperEngine__cctor_m2980610902_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		((AttributeHelperEngine_t2735742303_StaticFields*)AttributeHelperEngine_t2735742303_il2cpp_TypeInfo_var->static_fields)->set__disallowMultipleComponentArray_0(((DisallowMultipleComponentU5BU5D_t3936143868*)SZArrayNew(DisallowMultipleComponentU5BU5D_t3936143868_il2cpp_TypeInfo_var, (uint32_t)1)));
		((AttributeHelperEngine_t2735742303_StaticFields*)AttributeHelperEngine_t2735742303_il2cpp_TypeInfo_var->static_fields)->set__executeInEditModeArray_1(((ExecuteInEditModeU5BU5D_t3239458680*)SZArrayNew(ExecuteInEditModeU5BU5D_t3239458680_il2cpp_TypeInfo_var, (uint32_t)1)));
		((AttributeHelperEngine_t2735742303_StaticFields*)AttributeHelperEngine_t2735742303_il2cpp_TypeInfo_var->static_fields)->set__requireComponentArray_2(((RequireComponentU5BU5D_t2245623724*)SZArrayNew(RequireComponentU5BU5D_t2245623724_il2cpp_TypeInfo_var, (uint32_t)1)));
		return;
	}
}
// System.Single UnityEngine.AudioClip::get_length()
extern "C"  float AudioClip_get_length_m1592775809 (AudioClip_t3680889665 * __this, const MethodInfo* method)
{
	typedef float (*AudioClip_get_length_m1592775809_ftn) (AudioClip_t3680889665 *);
	static AudioClip_get_length_m1592775809_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AudioClip_get_length_m1592775809_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AudioClip::get_length()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.AudioClip::InvokePCMReaderCallback_Internal(System.Single[])
extern "C"  void AudioClip_InvokePCMReaderCallback_Internal_m3092652056 (AudioClip_t3680889665 * __this, SingleU5BU5D_t1444911251* ___data0, const MethodInfo* method)
{
	{
		PCMReaderCallback_t1677636661 * L_0 = __this->get_m_PCMReaderCallback_2();
		if (!L_0)
		{
			goto IL_0018;
		}
	}
	{
		PCMReaderCallback_t1677636661 * L_1 = __this->get_m_PCMReaderCallback_2();
		SingleU5BU5D_t1444911251* L_2 = ___data0;
		NullCheck(L_1);
		PCMReaderCallback_Invoke_m650805914(L_1, L_2, /*hidden argument*/NULL);
	}

IL_0018:
	{
		return;
	}
}
// System.Void UnityEngine.AudioClip::InvokePCMSetPositionCallback_Internal(System.Int32)
extern "C"  void AudioClip_InvokePCMSetPositionCallback_Internal_m1983196106 (AudioClip_t3680889665 * __this, int32_t ___position0, const MethodInfo* method)
{
	{
		PCMSetPositionCallback_t1059417452 * L_0 = __this->get_m_PCMSetPositionCallback_3();
		if (!L_0)
		{
			goto IL_0018;
		}
	}
	{
		PCMSetPositionCallback_t1059417452 * L_1 = __this->get_m_PCMSetPositionCallback_3();
		int32_t L_2 = ___position0;
		NullCheck(L_1);
		PCMSetPositionCallback_Invoke_m3905727039(L_1, L_2, /*hidden argument*/NULL);
	}

IL_0018:
	{
		return;
	}
}
extern "C"  void DelegatePInvokeWrapper_PCMReaderCallback_t1677636661 (PCMReaderCallback_t1677636661 * __this, SingleU5BU5D_t1444911251* ___data0, const MethodInfo* method)
{
	typedef void (STDCALL *PInvokeFunc)(float*);
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(((Il2CppDelegate*)__this)->method->methodPointer);

	// Marshaling of parameter '___data0' to native representation
	float* ____data0_marshaled = NULL;
	if (___data0 != NULL)
	{
		____data0_marshaled = reinterpret_cast<float*>((___data0)->GetAddressAtUnchecked(0));
	}

	// Native function invocation
	il2cppPInvokeFunc(____data0_marshaled);

}
// System.Void UnityEngine.AudioClip/PCMReaderCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void PCMReaderCallback__ctor_m1375193681 (PCMReaderCallback_t1677636661 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void UnityEngine.AudioClip/PCMReaderCallback::Invoke(System.Single[])
extern "C"  void PCMReaderCallback_Invoke_m650805914 (PCMReaderCallback_t1677636661 * __this, SingleU5BU5D_t1444911251* ___data0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		PCMReaderCallback_Invoke_m650805914((PCMReaderCallback_t1677636661 *)__this->get_prev_9(),___data0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, SingleU5BU5D_t1444911251* ___data0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___data0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (void* __this, SingleU5BU5D_t1444911251* ___data0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___data0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(___data0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult UnityEngine.AudioClip/PCMReaderCallback::BeginInvoke(System.Single[],System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * PCMReaderCallback_BeginInvoke_m3066497696 (PCMReaderCallback_t1677636661 * __this, SingleU5BU5D_t1444911251* ___data0, AsyncCallback_t3962456242 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	void *__d_args[2] = {0};
	__d_args[0] = ___data0;
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void UnityEngine.AudioClip/PCMReaderCallback::EndInvoke(System.IAsyncResult)
extern "C"  void PCMReaderCallback_EndInvoke_m2252620959 (PCMReaderCallback_t1677636661 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
extern "C"  void DelegatePInvokeWrapper_PCMSetPositionCallback_t1059417452 (PCMSetPositionCallback_t1059417452 * __this, int32_t ___position0, const MethodInfo* method)
{
	typedef void (STDCALL *PInvokeFunc)(int32_t);
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(((Il2CppDelegate*)__this)->method->methodPointer);

	// Native function invocation
	il2cppPInvokeFunc(___position0);

}
// System.Void UnityEngine.AudioClip/PCMSetPositionCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void PCMSetPositionCallback__ctor_m1815642656 (PCMSetPositionCallback_t1059417452 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void UnityEngine.AudioClip/PCMSetPositionCallback::Invoke(System.Int32)
extern "C"  void PCMSetPositionCallback_Invoke_m3905727039 (PCMSetPositionCallback_t1059417452 * __this, int32_t ___position0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		PCMSetPositionCallback_Invoke_m3905727039((PCMSetPositionCallback_t1059417452 *)__this->get_prev_9(),___position0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___position0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___position0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, int32_t ___position0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___position0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult UnityEngine.AudioClip/PCMSetPositionCallback::BeginInvoke(System.Int32,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * PCMSetPositionCallback_BeginInvoke_m2733210754 (PCMSetPositionCallback_t1059417452 * __this, int32_t ___position0, AsyncCallback_t3962456242 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PCMSetPositionCallback_BeginInvoke_m2733210754_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Int32_t2950945753_il2cpp_TypeInfo_var, &___position0);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void UnityEngine.AudioClip/PCMSetPositionCallback::EndInvoke(System.IAsyncResult)
extern "C"  void PCMSetPositionCallback_EndInvoke_m1524761131 (PCMSetPositionCallback_t1059417452 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void UnityEngine.AudioSettings::InvokeOnAudioConfigurationChanged(System.Boolean)
extern "C"  void AudioSettings_InvokeOnAudioConfigurationChanged_m2341783670 (Il2CppObject * __this /* static, unused */, bool ___deviceWasChanged0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AudioSettings_InvokeOnAudioConfigurationChanged_m2341783670_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		AudioConfigurationChangeHandler_t2089929874 * L_0 = ((AudioSettings_t3587374600_StaticFields*)AudioSettings_t3587374600_il2cpp_TypeInfo_var->static_fields)->get_OnAudioConfigurationChanged_0();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		AudioConfigurationChangeHandler_t2089929874 * L_1 = ((AudioSettings_t3587374600_StaticFields*)AudioSettings_t3587374600_il2cpp_TypeInfo_var->static_fields)->get_OnAudioConfigurationChanged_0();
		bool L_2 = ___deviceWasChanged0;
		NullCheck(L_1);
		AudioConfigurationChangeHandler_Invoke_m2130183669(L_1, L_2, /*hidden argument*/NULL);
	}

IL_0016:
	{
		return;
	}
}
extern "C"  void DelegatePInvokeWrapper_AudioConfigurationChangeHandler_t2089929874 (AudioConfigurationChangeHandler_t2089929874 * __this, bool ___deviceWasChanged0, const MethodInfo* method)
{
	typedef void (STDCALL *PInvokeFunc)(int32_t);
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(((Il2CppDelegate*)__this)->method->methodPointer);

	// Native function invocation
	il2cppPInvokeFunc(static_cast<int32_t>(___deviceWasChanged0));

}
// System.Void UnityEngine.AudioSettings/AudioConfigurationChangeHandler::.ctor(System.Object,System.IntPtr)
extern "C"  void AudioConfigurationChangeHandler__ctor_m1431302861 (AudioConfigurationChangeHandler_t2089929874 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void UnityEngine.AudioSettings/AudioConfigurationChangeHandler::Invoke(System.Boolean)
extern "C"  void AudioConfigurationChangeHandler_Invoke_m2130183669 (AudioConfigurationChangeHandler_t2089929874 * __this, bool ___deviceWasChanged0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		AudioConfigurationChangeHandler_Invoke_m2130183669((AudioConfigurationChangeHandler_t2089929874 *)__this->get_prev_9(),___deviceWasChanged0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, bool ___deviceWasChanged0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___deviceWasChanged0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, bool ___deviceWasChanged0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___deviceWasChanged0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult UnityEngine.AudioSettings/AudioConfigurationChangeHandler::BeginInvoke(System.Boolean,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * AudioConfigurationChangeHandler_BeginInvoke_m2001196844 (AudioConfigurationChangeHandler_t2089929874 * __this, bool ___deviceWasChanged0, AsyncCallback_t3962456242 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AudioConfigurationChangeHandler_BeginInvoke_m2001196844_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Boolean_t97287965_il2cpp_TypeInfo_var, &___deviceWasChanged0);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void UnityEngine.AudioSettings/AudioConfigurationChangeHandler::EndInvoke(System.IAsyncResult)
extern "C"  void AudioConfigurationChangeHandler_EndInvoke_m1720774368 (AudioConfigurationChangeHandler_t2089929874 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Single UnityEngine.AudioSource::get_volume()
extern "C"  float AudioSource_get_volume_m1877674779 (AudioSource_t3935305588 * __this, const MethodInfo* method)
{
	typedef float (*AudioSource_get_volume_m1877674779_ftn) (AudioSource_t3935305588 *);
	static AudioSource_get_volume_m1877674779_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AudioSource_get_volume_m1877674779_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AudioSource::get_volume()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.AudioSource::set_volume(System.Single)
extern "C"  void AudioSource_set_volume_m1363209434 (AudioSource_t3935305588 * __this, float ___value0, const MethodInfo* method)
{
	typedef void (*AudioSource_set_volume_m1363209434_ftn) (AudioSource_t3935305588 *, float);
	static AudioSource_set_volume_m1363209434_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AudioSource_set_volume_m1363209434_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AudioSource::set_volume(System.Single)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Single UnityEngine.AudioSource::get_pitch()
extern "C"  float AudioSource_get_pitch_m2380693810 (AudioSource_t3935305588 * __this, const MethodInfo* method)
{
	typedef float (*AudioSource_get_pitch_m2380693810_ftn) (AudioSource_t3935305588 *);
	static AudioSource_get_pitch_m2380693810_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AudioSource_get_pitch_m2380693810_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AudioSource::get_pitch()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.AudioSource::set_pitch(System.Single)
extern "C"  void AudioSource_set_pitch_m580115289 (AudioSource_t3935305588 * __this, float ___value0, const MethodInfo* method)
{
	typedef void (*AudioSource_set_pitch_m580115289_ftn) (AudioSource_t3935305588 *, float);
	static AudioSource_set_pitch_m580115289_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AudioSource_set_pitch_m580115289_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AudioSource::set_pitch(System.Single)");
	_il2cpp_icall_func(__this, ___value0);
}
// UnityEngine.AudioClip UnityEngine.AudioSource::get_clip()
extern "C"  AudioClip_t3680889665 * AudioSource_get_clip_m2677147368 (AudioSource_t3935305588 * __this, const MethodInfo* method)
{
	typedef AudioClip_t3680889665 * (*AudioSource_get_clip_m2677147368_ftn) (AudioSource_t3935305588 *);
	static AudioSource_get_clip_m2677147368_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AudioSource_get_clip_m2677147368_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AudioSource::get_clip()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.AudioSource::set_clip(UnityEngine.AudioClip)
extern "C"  void AudioSource_set_clip_m1255174161 (AudioSource_t3935305588 * __this, AudioClip_t3680889665 * ___value0, const MethodInfo* method)
{
	typedef void (*AudioSource_set_clip_m1255174161_ftn) (AudioSource_t3935305588 *, AudioClip_t3680889665 *);
	static AudioSource_set_clip_m1255174161_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AudioSource_set_clip_m1255174161_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AudioSource::set_clip(UnityEngine.AudioClip)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.AudioSource::Play(System.UInt64)
extern "C"  void AudioSource_Play_m3647871236 (AudioSource_t3935305588 * __this, uint64_t ___delay0, const MethodInfo* method)
{
	typedef void (*AudioSource_Play_m3647871236_ftn) (AudioSource_t3935305588 *, uint64_t);
	static AudioSource_Play_m3647871236_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AudioSource_Play_m3647871236_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AudioSource::Play(System.UInt64)");
	_il2cpp_icall_func(__this, ___delay0);
}
// System.Void UnityEngine.AudioSource::Play()
extern "C"  void AudioSource_Play_m2606248505 (AudioSource_t3935305588 * __this, const MethodInfo* method)
{
	uint64_t V_0 = 0;
	{
		V_0 = (((int64_t)((int64_t)0)));
		uint64_t L_0 = V_0;
		AudioSource_Play_m3647871236(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.AudioSource::Pause()
extern "C"  void AudioSource_Pause_m2161006631 (AudioSource_t3935305588 * __this, const MethodInfo* method)
{
	{
		AudioSource_INTERNAL_CALL_Pause_m1192371133(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.AudioSource::INTERNAL_CALL_Pause(UnityEngine.AudioSource)
extern "C"  void AudioSource_INTERNAL_CALL_Pause_m1192371133 (Il2CppObject * __this /* static, unused */, AudioSource_t3935305588 * ___self0, const MethodInfo* method)
{
	typedef void (*AudioSource_INTERNAL_CALL_Pause_m1192371133_ftn) (AudioSource_t3935305588 *);
	static AudioSource_INTERNAL_CALL_Pause_m1192371133_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AudioSource_INTERNAL_CALL_Pause_m1192371133_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AudioSource::INTERNAL_CALL_Pause(UnityEngine.AudioSource)");
	_il2cpp_icall_func(___self0);
}
// System.Void UnityEngine.AudioSource::PlayOneShot(UnityEngine.AudioClip,System.Single)
extern "C"  void AudioSource_PlayOneShot_m1563957832 (AudioSource_t3935305588 * __this, AudioClip_t3680889665 * ___clip0, float ___volumeScale1, const MethodInfo* method)
{
	typedef void (*AudioSource_PlayOneShot_m1563957832_ftn) (AudioSource_t3935305588 *, AudioClip_t3680889665 *, float);
	static AudioSource_PlayOneShot_m1563957832_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AudioSource_PlayOneShot_m1563957832_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AudioSource::PlayOneShot(UnityEngine.AudioClip,System.Single)");
	_il2cpp_icall_func(__this, ___clip0, ___volumeScale1);
}
// System.Void UnityEngine.AudioSource::PlayOneShot(UnityEngine.AudioClip)
extern "C"  void AudioSource_PlayOneShot_m3527126420 (AudioSource_t3935305588 * __this, AudioClip_t3680889665 * ___clip0, const MethodInfo* method)
{
	float V_0 = 0.0f;
	{
		V_0 = (1.0f);
		AudioClip_t3680889665 * L_0 = ___clip0;
		float L_1 = V_0;
		AudioSource_PlayOneShot_m1563957832(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.AudioSource::set_playOnAwake(System.Boolean)
extern "C"  void AudioSource_set_playOnAwake_m3967847264 (AudioSource_t3935305588 * __this, bool ___value0, const MethodInfo* method)
{
	typedef void (*AudioSource_set_playOnAwake_m3967847264_ftn) (AudioSource_t3935305588 *, bool);
	static AudioSource_set_playOnAwake_m3967847264_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AudioSource_set_playOnAwake_m3967847264_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AudioSource::set_playOnAwake(System.Boolean)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.Behaviour::.ctor()
extern "C"  void Behaviour__ctor_m3596636260 (Behaviour_t1437897464 * __this, const MethodInfo* method)
{
	{
		Component__ctor_m3533368306(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean UnityEngine.Behaviour::get_enabled()
extern "C"  bool Behaviour_get_enabled_m3113984196 (Behaviour_t1437897464 * __this, const MethodInfo* method)
{
	typedef bool (*Behaviour_get_enabled_m3113984196_ftn) (Behaviour_t1437897464 *);
	static Behaviour_get_enabled_m3113984196_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Behaviour_get_enabled_m3113984196_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Behaviour::get_enabled()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Behaviour::set_enabled(System.Boolean)
extern "C"  void Behaviour_set_enabled_m3107225489 (Behaviour_t1437897464 * __this, bool ___value0, const MethodInfo* method)
{
	typedef void (*Behaviour_set_enabled_m3107225489_ftn) (Behaviour_t1437897464 *, bool);
	static Behaviour_set_enabled_m3107225489_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Behaviour_set_enabled_m3107225489_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Behaviour::set_enabled(System.Boolean)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Boolean UnityEngine.Behaviour::get_isActiveAndEnabled()
extern "C"  bool Behaviour_get_isActiveAndEnabled_m3013577336 (Behaviour_t1437897464 * __this, const MethodInfo* method)
{
	typedef bool (*Behaviour_get_isActiveAndEnabled_m3013577336_ftn) (Behaviour_t1437897464 *);
	static Behaviour_get_isActiveAndEnabled_m3013577336_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Behaviour_get_isActiveAndEnabled_m3013577336_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Behaviour::get_isActiveAndEnabled()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Bounds::.ctor(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  void Bounds__ctor_m131864078 (Bounds_t2266837910 * __this, Vector3_t3722313464  ___center0, Vector3_t3722313464  ___size1, const MethodInfo* method)
{
	{
		Vector3_t3722313464  L_0 = ___center0;
		__this->set_m_Center_0(L_0);
		Vector3_t3722313464  L_1 = ___size1;
		Vector3_t3722313464  L_2 = Vector3_op_Multiply_m3506743150(NULL /*static, unused*/, L_1, (0.5f), /*hidden argument*/NULL);
		__this->set_m_Extents_1(L_2);
		return;
	}
}
extern "C"  void Bounds__ctor_m131864078_AdjustorThunk (Il2CppObject * __this, Vector3_t3722313464  ___center0, Vector3_t3722313464  ___size1, const MethodInfo* method)
{
	Bounds_t2266837910 * _thisAdjusted = reinterpret_cast<Bounds_t2266837910 *>(__this + 1);
	Bounds__ctor_m131864078(_thisAdjusted, ___center0, ___size1, method);
}
// System.Int32 UnityEngine.Bounds::GetHashCode()
extern "C"  int32_t Bounds_GetHashCode_m1195828975 (Bounds_t2266837910 * __this, const MethodInfo* method)
{
	Vector3_t3722313464  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector3_t3722313464  V_1;
	memset(&V_1, 0, sizeof(V_1));
	int32_t V_2 = 0;
	{
		Vector3_t3722313464  L_0 = Bounds_get_center_m1997663455(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		int32_t L_1 = Vector3_GetHashCode_m4064605418((&V_0), /*hidden argument*/NULL);
		Vector3_t3722313464  L_2 = Bounds_get_extents_m3683565517(__this, /*hidden argument*/NULL);
		V_1 = L_2;
		int32_t L_3 = Vector3_GetHashCode_m4064605418((&V_1), /*hidden argument*/NULL);
		V_2 = ((int32_t)((int32_t)L_1^(int32_t)((int32_t)((int32_t)L_3<<(int32_t)2))));
		goto IL_0032;
	}

IL_0032:
	{
		int32_t L_4 = V_2;
		return L_4;
	}
}
extern "C"  int32_t Bounds_GetHashCode_m1195828975_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Bounds_t2266837910 * _thisAdjusted = reinterpret_cast<Bounds_t2266837910 *>(__this + 1);
	return Bounds_GetHashCode_m1195828975(_thisAdjusted, method);
}
// System.Boolean UnityEngine.Bounds::Equals(System.Object)
extern "C"  bool Bounds_Equals_m1462810812 (Bounds_t2266837910 * __this, Il2CppObject * ___other0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Bounds_Equals_m1462810812_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	Bounds_t2266837910  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Vector3_t3722313464  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Vector3_t3722313464  V_3;
	memset(&V_3, 0, sizeof(V_3));
	int32_t G_B5_0 = 0;
	{
		Il2CppObject * L_0 = ___other0;
		if (((Il2CppObject *)IsInstSealed(L_0, Bounds_t2266837910_il2cpp_TypeInfo_var)))
		{
			goto IL_0013;
		}
	}
	{
		V_0 = (bool)0;
		goto IL_0068;
	}

IL_0013:
	{
		Il2CppObject * L_1 = ___other0;
		V_1 = ((*(Bounds_t2266837910 *)((Bounds_t2266837910 *)UnBox(L_1, Bounds_t2266837910_il2cpp_TypeInfo_var))));
		Vector3_t3722313464  L_2 = Bounds_get_center_m1997663455(__this, /*hidden argument*/NULL);
		V_2 = L_2;
		Vector3_t3722313464  L_3 = Bounds_get_center_m1997663455((&V_1), /*hidden argument*/NULL);
		Vector3_t3722313464  L_4 = L_3;
		Il2CppObject * L_5 = Box(Vector3_t3722313464_il2cpp_TypeInfo_var, &L_4);
		bool L_6 = Vector3_Equals_m496167795((&V_2), L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0061;
		}
	}
	{
		Vector3_t3722313464  L_7 = Bounds_get_extents_m3683565517(__this, /*hidden argument*/NULL);
		V_3 = L_7;
		Vector3_t3722313464  L_8 = Bounds_get_extents_m3683565517((&V_1), /*hidden argument*/NULL);
		Vector3_t3722313464  L_9 = L_8;
		Il2CppObject * L_10 = Box(Vector3_t3722313464_il2cpp_TypeInfo_var, &L_9);
		bool L_11 = Vector3_Equals_m496167795((&V_3), L_10, /*hidden argument*/NULL);
		G_B5_0 = ((int32_t)(L_11));
		goto IL_0062;
	}

IL_0061:
	{
		G_B5_0 = 0;
	}

IL_0062:
	{
		V_0 = (bool)G_B5_0;
		goto IL_0068;
	}

IL_0068:
	{
		bool L_12 = V_0;
		return L_12;
	}
}
extern "C"  bool Bounds_Equals_m1462810812_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___other0, const MethodInfo* method)
{
	Bounds_t2266837910 * _thisAdjusted = reinterpret_cast<Bounds_t2266837910 *>(__this + 1);
	return Bounds_Equals_m1462810812(_thisAdjusted, ___other0, method);
}
// UnityEngine.Vector3 UnityEngine.Bounds::get_center()
extern "C"  Vector3_t3722313464  Bounds_get_center_m1997663455 (Bounds_t2266837910 * __this, const MethodInfo* method)
{
	Vector3_t3722313464  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Vector3_t3722313464  L_0 = __this->get_m_Center_0();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		Vector3_t3722313464  L_1 = V_0;
		return L_1;
	}
}
extern "C"  Vector3_t3722313464  Bounds_get_center_m1997663455_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Bounds_t2266837910 * _thisAdjusted = reinterpret_cast<Bounds_t2266837910 *>(__this + 1);
	return Bounds_get_center_m1997663455(_thisAdjusted, method);
}
// System.Void UnityEngine.Bounds::set_center(UnityEngine.Vector3)
extern "C"  void Bounds_set_center_m1602292844 (Bounds_t2266837910 * __this, Vector3_t3722313464  ___value0, const MethodInfo* method)
{
	{
		Vector3_t3722313464  L_0 = ___value0;
		__this->set_m_Center_0(L_0);
		return;
	}
}
extern "C"  void Bounds_set_center_m1602292844_AdjustorThunk (Il2CppObject * __this, Vector3_t3722313464  ___value0, const MethodInfo* method)
{
	Bounds_t2266837910 * _thisAdjusted = reinterpret_cast<Bounds_t2266837910 *>(__this + 1);
	Bounds_set_center_m1602292844(_thisAdjusted, ___value0, method);
}
// UnityEngine.Vector3 UnityEngine.Bounds::get_size()
extern "C"  Vector3_t3722313464  Bounds_get_size_m1171376090 (Bounds_t2266837910 * __this, const MethodInfo* method)
{
	Vector3_t3722313464  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Vector3_t3722313464  L_0 = __this->get_m_Extents_1();
		Vector3_t3722313464  L_1 = Vector3_op_Multiply_m3506743150(NULL /*static, unused*/, L_0, (2.0f), /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_0017;
	}

IL_0017:
	{
		Vector3_t3722313464  L_2 = V_0;
		return L_2;
	}
}
extern "C"  Vector3_t3722313464  Bounds_get_size_m1171376090_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Bounds_t2266837910 * _thisAdjusted = reinterpret_cast<Bounds_t2266837910 *>(__this + 1);
	return Bounds_get_size_m1171376090(_thisAdjusted, method);
}
// System.Void UnityEngine.Bounds::set_size(UnityEngine.Vector3)
extern "C"  void Bounds_set_size_m1025767497 (Bounds_t2266837910 * __this, Vector3_t3722313464  ___value0, const MethodInfo* method)
{
	{
		Vector3_t3722313464  L_0 = ___value0;
		Vector3_t3722313464  L_1 = Vector3_op_Multiply_m3506743150(NULL /*static, unused*/, L_0, (0.5f), /*hidden argument*/NULL);
		__this->set_m_Extents_1(L_1);
		return;
	}
}
extern "C"  void Bounds_set_size_m1025767497_AdjustorThunk (Il2CppObject * __this, Vector3_t3722313464  ___value0, const MethodInfo* method)
{
	Bounds_t2266837910 * _thisAdjusted = reinterpret_cast<Bounds_t2266837910 *>(__this + 1);
	Bounds_set_size_m1025767497(_thisAdjusted, ___value0, method);
}
// UnityEngine.Vector3 UnityEngine.Bounds::get_extents()
extern "C"  Vector3_t3722313464  Bounds_get_extents_m3683565517 (Bounds_t2266837910 * __this, const MethodInfo* method)
{
	Vector3_t3722313464  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Vector3_t3722313464  L_0 = __this->get_m_Extents_1();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		Vector3_t3722313464  L_1 = V_0;
		return L_1;
	}
}
extern "C"  Vector3_t3722313464  Bounds_get_extents_m3683565517_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Bounds_t2266837910 * _thisAdjusted = reinterpret_cast<Bounds_t2266837910 *>(__this + 1);
	return Bounds_get_extents_m3683565517(_thisAdjusted, method);
}
// System.Void UnityEngine.Bounds::set_extents(UnityEngine.Vector3)
extern "C"  void Bounds_set_extents_m248192017 (Bounds_t2266837910 * __this, Vector3_t3722313464  ___value0, const MethodInfo* method)
{
	{
		Vector3_t3722313464  L_0 = ___value0;
		__this->set_m_Extents_1(L_0);
		return;
	}
}
extern "C"  void Bounds_set_extents_m248192017_AdjustorThunk (Il2CppObject * __this, Vector3_t3722313464  ___value0, const MethodInfo* method)
{
	Bounds_t2266837910 * _thisAdjusted = reinterpret_cast<Bounds_t2266837910 *>(__this + 1);
	Bounds_set_extents_m248192017(_thisAdjusted, ___value0, method);
}
// UnityEngine.Vector3 UnityEngine.Bounds::get_min()
extern "C"  Vector3_t3722313464  Bounds_get_min_m1736091801 (Bounds_t2266837910 * __this, const MethodInfo* method)
{
	Vector3_t3722313464  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Vector3_t3722313464  L_0 = Bounds_get_center_m1997663455(__this, /*hidden argument*/NULL);
		Vector3_t3722313464  L_1 = Bounds_get_extents_m3683565517(__this, /*hidden argument*/NULL);
		Vector3_t3722313464  L_2 = Vector3_op_Subtraction_m2566684344(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		goto IL_0018;
	}

IL_0018:
	{
		Vector3_t3722313464  L_3 = V_0;
		return L_3;
	}
}
extern "C"  Vector3_t3722313464  Bounds_get_min_m1736091801_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Bounds_t2266837910 * _thisAdjusted = reinterpret_cast<Bounds_t2266837910 *>(__this + 1);
	return Bounds_get_min_m1736091801(_thisAdjusted, method);
}
// UnityEngine.Vector3 UnityEngine.Bounds::get_max()
extern "C"  Vector3_t3722313464  Bounds_get_max_m1659284971 (Bounds_t2266837910 * __this, const MethodInfo* method)
{
	Vector3_t3722313464  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Vector3_t3722313464  L_0 = Bounds_get_center_m1997663455(__this, /*hidden argument*/NULL);
		Vector3_t3722313464  L_1 = Bounds_get_extents_m3683565517(__this, /*hidden argument*/NULL);
		Vector3_t3722313464  L_2 = Vector3_op_Addition_m1781942663(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		goto IL_0018;
	}

IL_0018:
	{
		Vector3_t3722313464  L_3 = V_0;
		return L_3;
	}
}
extern "C"  Vector3_t3722313464  Bounds_get_max_m1659284971_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Bounds_t2266837910 * _thisAdjusted = reinterpret_cast<Bounds_t2266837910 *>(__this + 1);
	return Bounds_get_max_m1659284971(_thisAdjusted, method);
}
// System.Boolean UnityEngine.Bounds::op_Equality(UnityEngine.Bounds,UnityEngine.Bounds)
extern "C"  bool Bounds_op_Equality_m3976187280 (Il2CppObject * __this /* static, unused */, Bounds_t2266837910  ___lhs0, Bounds_t2266837910  ___rhs1, const MethodInfo* method)
{
	bool V_0 = false;
	int32_t G_B3_0 = 0;
	{
		Vector3_t3722313464  L_0 = Bounds_get_center_m1997663455((&___lhs0), /*hidden argument*/NULL);
		Vector3_t3722313464  L_1 = Bounds_get_center_m1997663455((&___rhs1), /*hidden argument*/NULL);
		bool L_2 = Vector3_op_Equality_m2672007619(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_002e;
		}
	}
	{
		Vector3_t3722313464  L_3 = Bounds_get_extents_m3683565517((&___lhs0), /*hidden argument*/NULL);
		Vector3_t3722313464  L_4 = Bounds_get_extents_m3683565517((&___rhs1), /*hidden argument*/NULL);
		bool L_5 = Vector3_op_Equality_m2672007619(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		G_B3_0 = ((int32_t)(L_5));
		goto IL_002f;
	}

IL_002e:
	{
		G_B3_0 = 0;
	}

IL_002f:
	{
		V_0 = (bool)G_B3_0;
		goto IL_0035;
	}

IL_0035:
	{
		bool L_6 = V_0;
		return L_6;
	}
}
// System.Boolean UnityEngine.Bounds::op_Inequality(UnityEngine.Bounds,UnityEngine.Bounds)
extern "C"  bool Bounds_op_Inequality_m4034488270 (Il2CppObject * __this /* static, unused */, Bounds_t2266837910  ___lhs0, Bounds_t2266837910  ___rhs1, const MethodInfo* method)
{
	bool V_0 = false;
	{
		Bounds_t2266837910  L_0 = ___lhs0;
		Bounds_t2266837910  L_1 = ___rhs1;
		bool L_2 = Bounds_op_Equality_m3976187280(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		V_0 = (bool)((((int32_t)L_2) == ((int32_t)0))? 1 : 0);
		goto IL_0011;
	}

IL_0011:
	{
		bool L_3 = V_0;
		return L_3;
	}
}
// System.Void UnityEngine.Bounds::SetMinMax(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  void Bounds_SetMinMax_m1012362698 (Bounds_t2266837910 * __this, Vector3_t3722313464  ___min0, Vector3_t3722313464  ___max1, const MethodInfo* method)
{
	{
		Vector3_t3722313464  L_0 = ___max1;
		Vector3_t3722313464  L_1 = ___min0;
		Vector3_t3722313464  L_2 = Vector3_op_Subtraction_m2566684344(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		Vector3_t3722313464  L_3 = Vector3_op_Multiply_m3506743150(NULL /*static, unused*/, L_2, (0.5f), /*hidden argument*/NULL);
		Bounds_set_extents_m248192017(__this, L_3, /*hidden argument*/NULL);
		Vector3_t3722313464  L_4 = ___min0;
		Vector3_t3722313464  L_5 = Bounds_get_extents_m3683565517(__this, /*hidden argument*/NULL);
		Vector3_t3722313464  L_6 = Vector3_op_Addition_m1781942663(NULL /*static, unused*/, L_4, L_5, /*hidden argument*/NULL);
		Bounds_set_center_m1602292844(__this, L_6, /*hidden argument*/NULL);
		return;
	}
}
extern "C"  void Bounds_SetMinMax_m1012362698_AdjustorThunk (Il2CppObject * __this, Vector3_t3722313464  ___min0, Vector3_t3722313464  ___max1, const MethodInfo* method)
{
	Bounds_t2266837910 * _thisAdjusted = reinterpret_cast<Bounds_t2266837910 *>(__this + 1);
	Bounds_SetMinMax_m1012362698(_thisAdjusted, ___min0, ___max1, method);
}
// System.Void UnityEngine.Bounds::Encapsulate(UnityEngine.Vector3)
extern "C"  void Bounds_Encapsulate_m3482076963 (Bounds_t2266837910 * __this, Vector3_t3722313464  ___point0, const MethodInfo* method)
{
	{
		Vector3_t3722313464  L_0 = Bounds_get_min_m1736091801(__this, /*hidden argument*/NULL);
		Vector3_t3722313464  L_1 = ___point0;
		Vector3_t3722313464  L_2 = Vector3_Min_m4041074433(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		Vector3_t3722313464  L_3 = Bounds_get_max_m1659284971(__this, /*hidden argument*/NULL);
		Vector3_t3722313464  L_4 = ___point0;
		Vector3_t3722313464  L_5 = Vector3_Max_m821979329(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		Bounds_SetMinMax_m1012362698(__this, L_2, L_5, /*hidden argument*/NULL);
		return;
	}
}
extern "C"  void Bounds_Encapsulate_m3482076963_AdjustorThunk (Il2CppObject * __this, Vector3_t3722313464  ___point0, const MethodInfo* method)
{
	Bounds_t2266837910 * _thisAdjusted = reinterpret_cast<Bounds_t2266837910 *>(__this + 1);
	Bounds_Encapsulate_m3482076963(_thisAdjusted, ___point0, method);
}
// System.String UnityEngine.Bounds::ToString()
extern "C"  String_t* Bounds_ToString_m72361594 (Bounds_t2266837910 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Bounds_ToString_m72361594_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	{
		ObjectU5BU5D_t2843939325* L_0 = ((ObjectU5BU5D_t2843939325*)SZArrayNew(ObjectU5BU5D_t2843939325_il2cpp_TypeInfo_var, (uint32_t)2));
		Vector3_t3722313464  L_1 = __this->get_m_Center_0();
		Vector3_t3722313464  L_2 = L_1;
		Il2CppObject * L_3 = Box(Vector3_t3722313464_il2cpp_TypeInfo_var, &L_2);
		NullCheck(L_0);
		ArrayElementTypeCheck (L_0, L_3);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_3);
		ObjectU5BU5D_t2843939325* L_4 = L_0;
		Vector3_t3722313464  L_5 = __this->get_m_Extents_1();
		Vector3_t3722313464  L_6 = L_5;
		Il2CppObject * L_7 = Box(Vector3_t3722313464_il2cpp_TypeInfo_var, &L_6);
		NullCheck(L_4);
		ArrayElementTypeCheck (L_4, L_7);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_7);
		String_t* L_8 = UnityString_Format_m3741272017(NULL /*static, unused*/, _stringLiteral940982150, L_4, /*hidden argument*/NULL);
		V_0 = L_8;
		goto IL_0033;
	}

IL_0033:
	{
		String_t* L_9 = V_0;
		return L_9;
	}
}
extern "C"  String_t* Bounds_ToString_m72361594_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Bounds_t2266837910 * _thisAdjusted = reinterpret_cast<Bounds_t2266837910 *>(__this + 1);
	return Bounds_ToString_m72361594(_thisAdjusted, method);
}
// System.Void UnityEngine.BoxCollider::set_center(UnityEngine.Vector3)
extern "C"  void BoxCollider_set_center_m2729241340 (BoxCollider_t1640800422 * __this, Vector3_t3722313464  ___value0, const MethodInfo* method)
{
	{
		BoxCollider_INTERNAL_set_center_m3088077495(__this, (&___value0), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.BoxCollider::INTERNAL_set_center(UnityEngine.Vector3&)
extern "C"  void BoxCollider_INTERNAL_set_center_m3088077495 (BoxCollider_t1640800422 * __this, Vector3_t3722313464 * ___value0, const MethodInfo* method)
{
	typedef void (*BoxCollider_INTERNAL_set_center_m3088077495_ftn) (BoxCollider_t1640800422 *, Vector3_t3722313464 *);
	static BoxCollider_INTERNAL_set_center_m3088077495_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (BoxCollider_INTERNAL_set_center_m3088077495_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.BoxCollider::INTERNAL_set_center(UnityEngine.Vector3&)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.BoxCollider::set_size(UnityEngine.Vector3)
extern "C"  void BoxCollider_set_size_m2543427819 (BoxCollider_t1640800422 * __this, Vector3_t3722313464  ___value0, const MethodInfo* method)
{
	{
		BoxCollider_INTERNAL_set_size_m950353937(__this, (&___value0), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.BoxCollider::INTERNAL_set_size(UnityEngine.Vector3&)
extern "C"  void BoxCollider_INTERNAL_set_size_m950353937 (BoxCollider_t1640800422 * __this, Vector3_t3722313464 * ___value0, const MethodInfo* method)
{
	typedef void (*BoxCollider_INTERNAL_set_size_m950353937_ftn) (BoxCollider_t1640800422 *, Vector3_t3722313464 *);
	static BoxCollider_INTERNAL_set_size_m950353937_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (BoxCollider_INTERNAL_set_size_m950353937_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.BoxCollider::INTERNAL_set_size(UnityEngine.Vector3&)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Single UnityEngine.Camera::get_fieldOfView()
extern "C"  float Camera_get_fieldOfView_m1973900042 (Camera_t4157153871 * __this, const MethodInfo* method)
{
	typedef float (*Camera_get_fieldOfView_m1973900042_ftn) (Camera_t4157153871 *);
	static Camera_get_fieldOfView_m1973900042_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_get_fieldOfView_m1973900042_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::get_fieldOfView()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Camera::set_fieldOfView(System.Single)
extern "C"  void Camera_set_fieldOfView_m4094496100 (Camera_t4157153871 * __this, float ___value0, const MethodInfo* method)
{
	typedef void (*Camera_set_fieldOfView_m4094496100_ftn) (Camera_t4157153871 *, float);
	static Camera_set_fieldOfView_m4094496100_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_set_fieldOfView_m4094496100_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::set_fieldOfView(System.Single)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Single UnityEngine.Camera::get_nearClipPlane()
extern "C"  float Camera_get_nearClipPlane_m1518978844 (Camera_t4157153871 * __this, const MethodInfo* method)
{
	typedef float (*Camera_get_nearClipPlane_m1518978844_ftn) (Camera_t4157153871 *);
	static Camera_get_nearClipPlane_m1518978844_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_get_nearClipPlane_m1518978844_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::get_nearClipPlane()");
	return _il2cpp_icall_func(__this);
}
// System.Single UnityEngine.Camera::get_farClipPlane()
extern "C"  float Camera_get_farClipPlane_m1946573527 (Camera_t4157153871 * __this, const MethodInfo* method)
{
	typedef float (*Camera_get_farClipPlane_m1946573527_ftn) (Camera_t4157153871 *);
	static Camera_get_farClipPlane_m1946573527_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_get_farClipPlane_m1946573527_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::get_farClipPlane()");
	return _il2cpp_icall_func(__this);
}
// UnityEngine.RenderingPath UnityEngine.Camera::get_actualRenderingPath()
extern "C"  int32_t Camera_get_actualRenderingPath_m3740236745 (Camera_t4157153871 * __this, const MethodInfo* method)
{
	typedef int32_t (*Camera_get_actualRenderingPath_m3740236745_ftn) (Camera_t4157153871 *);
	static Camera_get_actualRenderingPath_m3740236745_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_get_actualRenderingPath_m3740236745_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::get_actualRenderingPath()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Camera::set_orthographicSize(System.Single)
extern "C"  void Camera_set_orthographicSize_m752464754 (Camera_t4157153871 * __this, float ___value0, const MethodInfo* method)
{
	typedef void (*Camera_set_orthographicSize_m752464754_ftn) (Camera_t4157153871 *, float);
	static Camera_set_orthographicSize_m752464754_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_set_orthographicSize_m752464754_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::set_orthographicSize(System.Single)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Boolean UnityEngine.Camera::get_orthographic()
extern "C"  bool Camera_get_orthographic_m3877638061 (Camera_t4157153871 * __this, const MethodInfo* method)
{
	typedef bool (*Camera_get_orthographic_m3877638061_ftn) (Camera_t4157153871 *);
	static Camera_get_orthographic_m3877638061_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_get_orthographic_m3877638061_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::get_orthographic()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Camera::set_orthographic(System.Boolean)
extern "C"  void Camera_set_orthographic_m4270673596 (Camera_t4157153871 * __this, bool ___value0, const MethodInfo* method)
{
	typedef void (*Camera_set_orthographic_m4270673596_ftn) (Camera_t4157153871 *, bool);
	static Camera_set_orthographic_m4270673596_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_set_orthographic_m4270673596_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::set_orthographic(System.Boolean)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Single UnityEngine.Camera::get_depth()
extern "C"  float Camera_get_depth_m642735266 (Camera_t4157153871 * __this, const MethodInfo* method)
{
	typedef float (*Camera_get_depth_m642735266_ftn) (Camera_t4157153871 *);
	static Camera_get_depth_m642735266_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_get_depth_m642735266_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::get_depth()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Camera::set_depth(System.Single)
extern "C"  void Camera_set_depth_m2145294311 (Camera_t4157153871 * __this, float ___value0, const MethodInfo* method)
{
	typedef void (*Camera_set_depth_m2145294311_ftn) (Camera_t4157153871 *, float);
	static Camera_set_depth_m2145294311_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_set_depth_m2145294311_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::set_depth(System.Single)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Single UnityEngine.Camera::get_aspect()
extern "C"  float Camera_get_aspect_m128343589 (Camera_t4157153871 * __this, const MethodInfo* method)
{
	typedef float (*Camera_get_aspect_m128343589_ftn) (Camera_t4157153871 *);
	static Camera_get_aspect_m128343589_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_get_aspect_m128343589_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::get_aspect()");
	return _il2cpp_icall_func(__this);
}
// System.Int32 UnityEngine.Camera::get_cullingMask()
extern "C"  int32_t Camera_get_cullingMask_m3552550664 (Camera_t4157153871 * __this, const MethodInfo* method)
{
	typedef int32_t (*Camera_get_cullingMask_m3552550664_ftn) (Camera_t4157153871 *);
	static Camera_get_cullingMask_m3552550664_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_get_cullingMask_m3552550664_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::get_cullingMask()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Camera::set_cullingMask(System.Int32)
extern "C"  void Camera_set_cullingMask_m54832321 (Camera_t4157153871 * __this, int32_t ___value0, const MethodInfo* method)
{
	typedef void (*Camera_set_cullingMask_m54832321_ftn) (Camera_t4157153871 *, int32_t);
	static Camera_set_cullingMask_m54832321_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_set_cullingMask_m54832321_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::set_cullingMask(System.Int32)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Int32 UnityEngine.Camera::get_eventMask()
extern "C"  int32_t Camera_get_eventMask_m588573340 (Camera_t4157153871 * __this, const MethodInfo* method)
{
	typedef int32_t (*Camera_get_eventMask_m588573340_ftn) (Camera_t4157153871 *);
	static Camera_get_eventMask_m588573340_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_get_eventMask_m588573340_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::get_eventMask()");
	return _il2cpp_icall_func(__this);
}
// UnityEngine.Rect UnityEngine.Camera::get_pixelRect()
extern "C"  Rect_t2360479859  Camera_get_pixelRect_m1345608344 (Camera_t4157153871 * __this, const MethodInfo* method)
{
	Rect_t2360479859  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Rect_t2360479859  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		Camera_INTERNAL_get_pixelRect_m1806464323(__this, (&V_0), /*hidden argument*/NULL);
		Rect_t2360479859  L_0 = V_0;
		V_1 = L_0;
		goto IL_0010;
	}

IL_0010:
	{
		Rect_t2360479859  L_1 = V_1;
		return L_1;
	}
}
// System.Void UnityEngine.Camera::INTERNAL_get_pixelRect(UnityEngine.Rect&)
extern "C"  void Camera_INTERNAL_get_pixelRect_m1806464323 (Camera_t4157153871 * __this, Rect_t2360479859 * ___value0, const MethodInfo* method)
{
	typedef void (*Camera_INTERNAL_get_pixelRect_m1806464323_ftn) (Camera_t4157153871 *, Rect_t2360479859 *);
	static Camera_INTERNAL_get_pixelRect_m1806464323_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_INTERNAL_get_pixelRect_m1806464323_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::INTERNAL_get_pixelRect(UnityEngine.Rect&)");
	_il2cpp_icall_func(__this, ___value0);
}
// UnityEngine.RenderTexture UnityEngine.Camera::get_targetTexture()
extern "C"  RenderTexture_t2108887433 * Camera_get_targetTexture_m2110259960 (Camera_t4157153871 * __this, const MethodInfo* method)
{
	typedef RenderTexture_t2108887433 * (*Camera_get_targetTexture_m2110259960_ftn) (Camera_t4157153871 *);
	static Camera_get_targetTexture_m2110259960_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_get_targetTexture_m2110259960_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::get_targetTexture()");
	return _il2cpp_icall_func(__this);
}
// System.Int32 UnityEngine.Camera::get_pixelWidth()
extern "C"  int32_t Camera_get_pixelWidth_m2128258779 (Camera_t4157153871 * __this, const MethodInfo* method)
{
	typedef int32_t (*Camera_get_pixelWidth_m2128258779_ftn) (Camera_t4157153871 *);
	static Camera_get_pixelWidth_m2128258779_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_get_pixelWidth_m2128258779_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::get_pixelWidth()");
	return _il2cpp_icall_func(__this);
}
// System.Int32 UnityEngine.Camera::get_pixelHeight()
extern "C"  int32_t Camera_get_pixelHeight_m1203084778 (Camera_t4157153871 * __this, const MethodInfo* method)
{
	typedef int32_t (*Camera_get_pixelHeight_m1203084778_ftn) (Camera_t4157153871 *);
	static Camera_get_pixelHeight_m1203084778_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_get_pixelHeight_m1203084778_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::get_pixelHeight()");
	return _il2cpp_icall_func(__this);
}
// UnityEngine.Matrix4x4 UnityEngine.Camera::get_projectionMatrix()
extern "C"  Matrix4x4_t1817901843  Camera_get_projectionMatrix_m1418724237 (Camera_t4157153871 * __this, const MethodInfo* method)
{
	Matrix4x4_t1817901843  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Matrix4x4_t1817901843  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		Camera_INTERNAL_get_projectionMatrix_m3112156927(__this, (&V_0), /*hidden argument*/NULL);
		Matrix4x4_t1817901843  L_0 = V_0;
		V_1 = L_0;
		goto IL_0010;
	}

IL_0010:
	{
		Matrix4x4_t1817901843  L_1 = V_1;
		return L_1;
	}
}
// System.Void UnityEngine.Camera::set_projectionMatrix(UnityEngine.Matrix4x4)
extern "C"  void Camera_set_projectionMatrix_m1779670536 (Camera_t4157153871 * __this, Matrix4x4_t1817901843  ___value0, const MethodInfo* method)
{
	{
		Camera_INTERNAL_set_projectionMatrix_m1700405219(__this, (&___value0), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Camera::INTERNAL_get_projectionMatrix(UnityEngine.Matrix4x4&)
extern "C"  void Camera_INTERNAL_get_projectionMatrix_m3112156927 (Camera_t4157153871 * __this, Matrix4x4_t1817901843 * ___value0, const MethodInfo* method)
{
	typedef void (*Camera_INTERNAL_get_projectionMatrix_m3112156927_ftn) (Camera_t4157153871 *, Matrix4x4_t1817901843 *);
	static Camera_INTERNAL_get_projectionMatrix_m3112156927_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_INTERNAL_get_projectionMatrix_m3112156927_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::INTERNAL_get_projectionMatrix(UnityEngine.Matrix4x4&)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.Camera::INTERNAL_set_projectionMatrix(UnityEngine.Matrix4x4&)
extern "C"  void Camera_INTERNAL_set_projectionMatrix_m1700405219 (Camera_t4157153871 * __this, Matrix4x4_t1817901843 * ___value0, const MethodInfo* method)
{
	typedef void (*Camera_INTERNAL_set_projectionMatrix_m1700405219_ftn) (Camera_t4157153871 *, Matrix4x4_t1817901843 *);
	static Camera_INTERNAL_set_projectionMatrix_m1700405219_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_INTERNAL_set_projectionMatrix_m1700405219_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::INTERNAL_set_projectionMatrix(UnityEngine.Matrix4x4&)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.Camera::ResetProjectionMatrix()
extern "C"  void Camera_ResetProjectionMatrix_m725811250 (Camera_t4157153871 * __this, const MethodInfo* method)
{
	{
		Camera_INTERNAL_CALL_ResetProjectionMatrix_m2949107079(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Camera::INTERNAL_CALL_ResetProjectionMatrix(UnityEngine.Camera)
extern "C"  void Camera_INTERNAL_CALL_ResetProjectionMatrix_m2949107079 (Il2CppObject * __this /* static, unused */, Camera_t4157153871 * ___self0, const MethodInfo* method)
{
	typedef void (*Camera_INTERNAL_CALL_ResetProjectionMatrix_m2949107079_ftn) (Camera_t4157153871 *);
	static Camera_INTERNAL_CALL_ResetProjectionMatrix_m2949107079_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_INTERNAL_CALL_ResetProjectionMatrix_m2949107079_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::INTERNAL_CALL_ResetProjectionMatrix(UnityEngine.Camera)");
	_il2cpp_icall_func(___self0);
}
// UnityEngine.CameraClearFlags UnityEngine.Camera::get_clearFlags()
extern "C"  int32_t Camera_get_clearFlags_m869917516 (Camera_t4157153871 * __this, const MethodInfo* method)
{
	typedef int32_t (*Camera_get_clearFlags_m869917516_ftn) (Camera_t4157153871 *);
	static Camera_get_clearFlags_m869917516_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_get_clearFlags_m869917516_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::get_clearFlags()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Camera::set_clearFlags(UnityEngine.CameraClearFlags)
extern "C"  void Camera_set_clearFlags_m3521320377 (Camera_t4157153871 * __this, int32_t ___value0, const MethodInfo* method)
{
	typedef void (*Camera_set_clearFlags_m3521320377_ftn) (Camera_t4157153871 *, int32_t);
	static Camera_set_clearFlags_m3521320377_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_set_clearFlags_m3521320377_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::set_clearFlags(UnityEngine.CameraClearFlags)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Int32 UnityEngine.Camera::get_targetDisplay()
extern "C"  int32_t Camera_get_targetDisplay_m1728727618 (Camera_t4157153871 * __this, const MethodInfo* method)
{
	typedef int32_t (*Camera_get_targetDisplay_m1728727618_ftn) (Camera_t4157153871 *);
	static Camera_get_targetDisplay_m1728727618_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_get_targetDisplay_m1728727618_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::get_targetDisplay()");
	return _il2cpp_icall_func(__this);
}
// UnityEngine.Vector3 UnityEngine.Camera::WorldToScreenPoint(UnityEngine.Vector3)
extern "C"  Vector3_t3722313464  Camera_WorldToScreenPoint_m491883527 (Camera_t4157153871 * __this, Vector3_t3722313464  ___position0, const MethodInfo* method)
{
	Vector3_t3722313464  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector3_t3722313464  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		Camera_INTERNAL_CALL_WorldToScreenPoint_m2343250406(NULL /*static, unused*/, __this, (&___position0), (&V_0), /*hidden argument*/NULL);
		Vector3_t3722313464  L_0 = V_0;
		V_1 = L_0;
		goto IL_0012;
	}

IL_0012:
	{
		Vector3_t3722313464  L_1 = V_1;
		return L_1;
	}
}
// System.Void UnityEngine.Camera::INTERNAL_CALL_WorldToScreenPoint(UnityEngine.Camera,UnityEngine.Vector3&,UnityEngine.Vector3&)
extern "C"  void Camera_INTERNAL_CALL_WorldToScreenPoint_m2343250406 (Il2CppObject * __this /* static, unused */, Camera_t4157153871 * ___self0, Vector3_t3722313464 * ___position1, Vector3_t3722313464 * ___value2, const MethodInfo* method)
{
	typedef void (*Camera_INTERNAL_CALL_WorldToScreenPoint_m2343250406_ftn) (Camera_t4157153871 *, Vector3_t3722313464 *, Vector3_t3722313464 *);
	static Camera_INTERNAL_CALL_WorldToScreenPoint_m2343250406_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_INTERNAL_CALL_WorldToScreenPoint_m2343250406_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::INTERNAL_CALL_WorldToScreenPoint(UnityEngine.Camera,UnityEngine.Vector3&,UnityEngine.Vector3&)");
	_il2cpp_icall_func(___self0, ___position1, ___value2);
}
// UnityEngine.Vector3 UnityEngine.Camera::ViewportToWorldPoint(UnityEngine.Vector3)
extern "C"  Vector3_t3722313464  Camera_ViewportToWorldPoint_m3480887959 (Camera_t4157153871 * __this, Vector3_t3722313464  ___position0, const MethodInfo* method)
{
	Vector3_t3722313464  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector3_t3722313464  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		Camera_INTERNAL_CALL_ViewportToWorldPoint_m2395836354(NULL /*static, unused*/, __this, (&___position0), (&V_0), /*hidden argument*/NULL);
		Vector3_t3722313464  L_0 = V_0;
		V_1 = L_0;
		goto IL_0012;
	}

IL_0012:
	{
		Vector3_t3722313464  L_1 = V_1;
		return L_1;
	}
}
// System.Void UnityEngine.Camera::INTERNAL_CALL_ViewportToWorldPoint(UnityEngine.Camera,UnityEngine.Vector3&,UnityEngine.Vector3&)
extern "C"  void Camera_INTERNAL_CALL_ViewportToWorldPoint_m2395836354 (Il2CppObject * __this /* static, unused */, Camera_t4157153871 * ___self0, Vector3_t3722313464 * ___position1, Vector3_t3722313464 * ___value2, const MethodInfo* method)
{
	typedef void (*Camera_INTERNAL_CALL_ViewportToWorldPoint_m2395836354_ftn) (Camera_t4157153871 *, Vector3_t3722313464 *, Vector3_t3722313464 *);
	static Camera_INTERNAL_CALL_ViewportToWorldPoint_m2395836354_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_INTERNAL_CALL_ViewportToWorldPoint_m2395836354_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::INTERNAL_CALL_ViewportToWorldPoint(UnityEngine.Camera,UnityEngine.Vector3&,UnityEngine.Vector3&)");
	_il2cpp_icall_func(___self0, ___position1, ___value2);
}
// UnityEngine.Vector3 UnityEngine.Camera::ScreenToWorldPoint(UnityEngine.Vector3)
extern "C"  Vector3_t3722313464  Camera_ScreenToWorldPoint_m1238306073 (Camera_t4157153871 * __this, Vector3_t3722313464  ___position0, const MethodInfo* method)
{
	Vector3_t3722313464  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector3_t3722313464  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		Camera_INTERNAL_CALL_ScreenToWorldPoint_m2680061077(NULL /*static, unused*/, __this, (&___position0), (&V_0), /*hidden argument*/NULL);
		Vector3_t3722313464  L_0 = V_0;
		V_1 = L_0;
		goto IL_0012;
	}

IL_0012:
	{
		Vector3_t3722313464  L_1 = V_1;
		return L_1;
	}
}
// System.Void UnityEngine.Camera::INTERNAL_CALL_ScreenToWorldPoint(UnityEngine.Camera,UnityEngine.Vector3&,UnityEngine.Vector3&)
extern "C"  void Camera_INTERNAL_CALL_ScreenToWorldPoint_m2680061077 (Il2CppObject * __this /* static, unused */, Camera_t4157153871 * ___self0, Vector3_t3722313464 * ___position1, Vector3_t3722313464 * ___value2, const MethodInfo* method)
{
	typedef void (*Camera_INTERNAL_CALL_ScreenToWorldPoint_m2680061077_ftn) (Camera_t4157153871 *, Vector3_t3722313464 *, Vector3_t3722313464 *);
	static Camera_INTERNAL_CALL_ScreenToWorldPoint_m2680061077_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_INTERNAL_CALL_ScreenToWorldPoint_m2680061077_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::INTERNAL_CALL_ScreenToWorldPoint(UnityEngine.Camera,UnityEngine.Vector3&,UnityEngine.Vector3&)");
	_il2cpp_icall_func(___self0, ___position1, ___value2);
}
// UnityEngine.Vector3 UnityEngine.Camera::ScreenToViewportPoint(UnityEngine.Vector3)
extern "C"  Vector3_t3722313464  Camera_ScreenToViewportPoint_m1855721282 (Camera_t4157153871 * __this, Vector3_t3722313464  ___position0, const MethodInfo* method)
{
	Vector3_t3722313464  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector3_t3722313464  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		Camera_INTERNAL_CALL_ScreenToViewportPoint_m3505215880(NULL /*static, unused*/, __this, (&___position0), (&V_0), /*hidden argument*/NULL);
		Vector3_t3722313464  L_0 = V_0;
		V_1 = L_0;
		goto IL_0012;
	}

IL_0012:
	{
		Vector3_t3722313464  L_1 = V_1;
		return L_1;
	}
}
// System.Void UnityEngine.Camera::INTERNAL_CALL_ScreenToViewportPoint(UnityEngine.Camera,UnityEngine.Vector3&,UnityEngine.Vector3&)
extern "C"  void Camera_INTERNAL_CALL_ScreenToViewportPoint_m3505215880 (Il2CppObject * __this /* static, unused */, Camera_t4157153871 * ___self0, Vector3_t3722313464 * ___position1, Vector3_t3722313464 * ___value2, const MethodInfo* method)
{
	typedef void (*Camera_INTERNAL_CALL_ScreenToViewportPoint_m3505215880_ftn) (Camera_t4157153871 *, Vector3_t3722313464 *, Vector3_t3722313464 *);
	static Camera_INTERNAL_CALL_ScreenToViewportPoint_m3505215880_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_INTERNAL_CALL_ScreenToViewportPoint_m3505215880_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::INTERNAL_CALL_ScreenToViewportPoint(UnityEngine.Camera,UnityEngine.Vector3&,UnityEngine.Vector3&)");
	_il2cpp_icall_func(___self0, ___position1, ___value2);
}
// UnityEngine.Ray UnityEngine.Camera::ScreenPointToRay(UnityEngine.Vector3)
extern "C"  Ray_t3785851493  Camera_ScreenPointToRay_m1522780915 (Camera_t4157153871 * __this, Vector3_t3722313464  ___position0, const MethodInfo* method)
{
	Ray_t3785851493  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Ray_t3785851493  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		Camera_INTERNAL_CALL_ScreenPointToRay_m3272660008(NULL /*static, unused*/, __this, (&___position0), (&V_0), /*hidden argument*/NULL);
		Ray_t3785851493  L_0 = V_0;
		V_1 = L_0;
		goto IL_0012;
	}

IL_0012:
	{
		Ray_t3785851493  L_1 = V_1;
		return L_1;
	}
}
// System.Void UnityEngine.Camera::INTERNAL_CALL_ScreenPointToRay(UnityEngine.Camera,UnityEngine.Vector3&,UnityEngine.Ray&)
extern "C"  void Camera_INTERNAL_CALL_ScreenPointToRay_m3272660008 (Il2CppObject * __this /* static, unused */, Camera_t4157153871 * ___self0, Vector3_t3722313464 * ___position1, Ray_t3785851493 * ___value2, const MethodInfo* method)
{
	typedef void (*Camera_INTERNAL_CALL_ScreenPointToRay_m3272660008_ftn) (Camera_t4157153871 *, Vector3_t3722313464 *, Ray_t3785851493 *);
	static Camera_INTERNAL_CALL_ScreenPointToRay_m3272660008_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_INTERNAL_CALL_ScreenPointToRay_m3272660008_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::INTERNAL_CALL_ScreenPointToRay(UnityEngine.Camera,UnityEngine.Vector3&,UnityEngine.Ray&)");
	_il2cpp_icall_func(___self0, ___position1, ___value2);
}
// UnityEngine.Camera UnityEngine.Camera::get_main()
extern "C"  Camera_t4157153871 * Camera_get_main_m882983993 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef Camera_t4157153871 * (*Camera_get_main_m882983993_ftn) ();
	static Camera_get_main_m882983993_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_get_main_m882983993_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::get_main()");
	return _il2cpp_icall_func();
}
// UnityEngine.Camera UnityEngine.Camera::get_current()
extern "C"  Camera_t4157153871 * Camera_get_current_m783629169 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef Camera_t4157153871 * (*Camera_get_current_m783629169_ftn) ();
	static Camera_get_current_m783629169_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_get_current_m783629169_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::get_current()");
	return _il2cpp_icall_func();
}
// UnityEngine.Camera[] UnityEngine.Camera::get_allCameras()
extern "C"  CameraU5BU5D_t1709987734* Camera_get_allCameras_m1547499779 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef CameraU5BU5D_t1709987734* (*Camera_get_allCameras_m1547499779_ftn) ();
	static Camera_get_allCameras_m1547499779_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_get_allCameras_m1547499779_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::get_allCameras()");
	return _il2cpp_icall_func();
}
// System.Int32 UnityEngine.Camera::get_allCamerasCount()
extern "C"  int32_t Camera_get_allCamerasCount_m1052940671 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef int32_t (*Camera_get_allCamerasCount_m1052940671_ftn) ();
	static Camera_get_allCamerasCount_m1052940671_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_get_allCamerasCount_m1052940671_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::get_allCamerasCount()");
	return _il2cpp_icall_func();
}
// System.Int32 UnityEngine.Camera::GetAllCameras(UnityEngine.Camera[])
extern "C"  int32_t Camera_GetAllCameras_m3836434108 (Il2CppObject * __this /* static, unused */, CameraU5BU5D_t1709987734* ___cameras0, const MethodInfo* method)
{
	typedef int32_t (*Camera_GetAllCameras_m3836434108_ftn) (CameraU5BU5D_t1709987734*);
	static Camera_GetAllCameras_m3836434108_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_GetAllCameras_m3836434108_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::GetAllCameras(UnityEngine.Camera[])");
	return _il2cpp_icall_func(___cameras0);
}
// System.Void UnityEngine.Camera::FireOnPreCull(UnityEngine.Camera)
extern "C"  void Camera_FireOnPreCull_m2863664483 (Il2CppObject * __this /* static, unused */, Camera_t4157153871 * ___cam0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Camera_FireOnPreCull_m2863664483_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		CameraCallback_t190067161 * L_0 = ((Camera_t4157153871_StaticFields*)Camera_t4157153871_il2cpp_TypeInfo_var->static_fields)->get_onPreCull_2();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		CameraCallback_t190067161 * L_1 = ((Camera_t4157153871_StaticFields*)Camera_t4157153871_il2cpp_TypeInfo_var->static_fields)->get_onPreCull_2();
		Camera_t4157153871 * L_2 = ___cam0;
		NullCheck(L_1);
		CameraCallback_Invoke_m2206506905(L_1, L_2, /*hidden argument*/NULL);
	}

IL_0016:
	{
		return;
	}
}
// System.Void UnityEngine.Camera::FireOnPreRender(UnityEngine.Camera)
extern "C"  void Camera_FireOnPreRender_m475210052 (Il2CppObject * __this /* static, unused */, Camera_t4157153871 * ___cam0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Camera_FireOnPreRender_m475210052_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		CameraCallback_t190067161 * L_0 = ((Camera_t4157153871_StaticFields*)Camera_t4157153871_il2cpp_TypeInfo_var->static_fields)->get_onPreRender_3();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		CameraCallback_t190067161 * L_1 = ((Camera_t4157153871_StaticFields*)Camera_t4157153871_il2cpp_TypeInfo_var->static_fields)->get_onPreRender_3();
		Camera_t4157153871 * L_2 = ___cam0;
		NullCheck(L_1);
		CameraCallback_Invoke_m2206506905(L_1, L_2, /*hidden argument*/NULL);
	}

IL_0016:
	{
		return;
	}
}
// System.Void UnityEngine.Camera::FireOnPostRender(UnityEngine.Camera)
extern "C"  void Camera_FireOnPostRender_m1633769721 (Il2CppObject * __this /* static, unused */, Camera_t4157153871 * ___cam0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Camera_FireOnPostRender_m1633769721_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		CameraCallback_t190067161 * L_0 = ((Camera_t4157153871_StaticFields*)Camera_t4157153871_il2cpp_TypeInfo_var->static_fields)->get_onPostRender_4();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		CameraCallback_t190067161 * L_1 = ((Camera_t4157153871_StaticFields*)Camera_t4157153871_il2cpp_TypeInfo_var->static_fields)->get_onPostRender_4();
		Camera_t4157153871 * L_2 = ___cam0;
		NullCheck(L_1);
		CameraCallback_Invoke_m2206506905(L_1, L_2, /*hidden argument*/NULL);
	}

IL_0016:
	{
		return;
	}
}
// UnityEngine.DepthTextureMode UnityEngine.Camera::get_depthTextureMode()
extern "C"  int32_t Camera_get_depthTextureMode_m671431136 (Camera_t4157153871 * __this, const MethodInfo* method)
{
	typedef int32_t (*Camera_get_depthTextureMode_m671431136_ftn) (Camera_t4157153871 *);
	static Camera_get_depthTextureMode_m671431136_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_get_depthTextureMode_m671431136_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::get_depthTextureMode()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Camera::set_depthTextureMode(UnityEngine.DepthTextureMode)
extern "C"  void Camera_set_depthTextureMode_m2892547683 (Camera_t4157153871 * __this, int32_t ___value0, const MethodInfo* method)
{
	typedef void (*Camera_set_depthTextureMode_m2892547683_ftn) (Camera_t4157153871 *, int32_t);
	static Camera_set_depthTextureMode_m2892547683_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_set_depthTextureMode_m2892547683_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::set_depthTextureMode(UnityEngine.DepthTextureMode)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.Camera::AddCommandBuffer(UnityEngine.Rendering.CameraEvent,UnityEngine.Rendering.CommandBuffer)
extern "C"  void Camera_AddCommandBuffer_m992019465 (Camera_t4157153871 * __this, int32_t ___evt0, CommandBuffer_t2206337031 * ___buffer1, const MethodInfo* method)
{
	typedef void (*Camera_AddCommandBuffer_m992019465_ftn) (Camera_t4157153871 *, int32_t, CommandBuffer_t2206337031 *);
	static Camera_AddCommandBuffer_m992019465_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_AddCommandBuffer_m992019465_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::AddCommandBuffer(UnityEngine.Rendering.CameraEvent,UnityEngine.Rendering.CommandBuffer)");
	_il2cpp_icall_func(__this, ___evt0, ___buffer1);
}
// System.Void UnityEngine.Camera::RemoveCommandBuffer(UnityEngine.Rendering.CameraEvent,UnityEngine.Rendering.CommandBuffer)
extern "C"  void Camera_RemoveCommandBuffer_m1920284267 (Camera_t4157153871 * __this, int32_t ___evt0, CommandBuffer_t2206337031 * ___buffer1, const MethodInfo* method)
{
	typedef void (*Camera_RemoveCommandBuffer_m1920284267_ftn) (Camera_t4157153871 *, int32_t, CommandBuffer_t2206337031 *);
	static Camera_RemoveCommandBuffer_m1920284267_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_RemoveCommandBuffer_m1920284267_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::RemoveCommandBuffer(UnityEngine.Rendering.CameraEvent,UnityEngine.Rendering.CommandBuffer)");
	_il2cpp_icall_func(__this, ___evt0, ___buffer1);
}
// System.Void UnityEngine.Camera::RemoveAllCommandBuffers()
extern "C"  void Camera_RemoveAllCommandBuffers_m1281959227 (Camera_t4157153871 * __this, const MethodInfo* method)
{
	typedef void (*Camera_RemoveAllCommandBuffers_m1281959227_ftn) (Camera_t4157153871 *);
	static Camera_RemoveAllCommandBuffers_m1281959227_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_RemoveAllCommandBuffers_m1281959227_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::RemoveAllCommandBuffers()");
	_il2cpp_icall_func(__this);
}
// UnityEngine.GameObject UnityEngine.Camera::RaycastTry(UnityEngine.Ray,System.Single,System.Int32)
extern "C"  GameObject_t1113636619 * Camera_RaycastTry_m1524854543 (Camera_t4157153871 * __this, Ray_t3785851493  ___ray0, float ___distance1, int32_t ___layerMask2, const MethodInfo* method)
{
	GameObject_t1113636619 * V_0 = NULL;
	{
		float L_0 = ___distance1;
		int32_t L_1 = ___layerMask2;
		GameObject_t1113636619 * L_2 = Camera_INTERNAL_CALL_RaycastTry_m1525247362(NULL /*static, unused*/, __this, (&___ray0), L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		goto IL_0011;
	}

IL_0011:
	{
		GameObject_t1113636619 * L_3 = V_0;
		return L_3;
	}
}
// UnityEngine.GameObject UnityEngine.Camera::INTERNAL_CALL_RaycastTry(UnityEngine.Camera,UnityEngine.Ray&,System.Single,System.Int32)
extern "C"  GameObject_t1113636619 * Camera_INTERNAL_CALL_RaycastTry_m1525247362 (Il2CppObject * __this /* static, unused */, Camera_t4157153871 * ___self0, Ray_t3785851493 * ___ray1, float ___distance2, int32_t ___layerMask3, const MethodInfo* method)
{
	typedef GameObject_t1113636619 * (*Camera_INTERNAL_CALL_RaycastTry_m1525247362_ftn) (Camera_t4157153871 *, Ray_t3785851493 *, float, int32_t);
	static Camera_INTERNAL_CALL_RaycastTry_m1525247362_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_INTERNAL_CALL_RaycastTry_m1525247362_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::INTERNAL_CALL_RaycastTry(UnityEngine.Camera,UnityEngine.Ray&,System.Single,System.Int32)");
	return _il2cpp_icall_func(___self0, ___ray1, ___distance2, ___layerMask3);
}
// UnityEngine.GameObject UnityEngine.Camera::RaycastTry2D(UnityEngine.Ray,System.Single,System.Int32)
extern "C"  GameObject_t1113636619 * Camera_RaycastTry2D_m937727458 (Camera_t4157153871 * __this, Ray_t3785851493  ___ray0, float ___distance1, int32_t ___layerMask2, const MethodInfo* method)
{
	GameObject_t1113636619 * V_0 = NULL;
	{
		float L_0 = ___distance1;
		int32_t L_1 = ___layerMask2;
		GameObject_t1113636619 * L_2 = Camera_INTERNAL_CALL_RaycastTry2D_m4005245494(NULL /*static, unused*/, __this, (&___ray0), L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		goto IL_0011;
	}

IL_0011:
	{
		GameObject_t1113636619 * L_3 = V_0;
		return L_3;
	}
}
// UnityEngine.GameObject UnityEngine.Camera::INTERNAL_CALL_RaycastTry2D(UnityEngine.Camera,UnityEngine.Ray&,System.Single,System.Int32)
extern "C"  GameObject_t1113636619 * Camera_INTERNAL_CALL_RaycastTry2D_m4005245494 (Il2CppObject * __this /* static, unused */, Camera_t4157153871 * ___self0, Ray_t3785851493 * ___ray1, float ___distance2, int32_t ___layerMask3, const MethodInfo* method)
{
	typedef GameObject_t1113636619 * (*Camera_INTERNAL_CALL_RaycastTry2D_m4005245494_ftn) (Camera_t4157153871 *, Ray_t3785851493 *, float, int32_t);
	static Camera_INTERNAL_CALL_RaycastTry2D_m4005245494_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Camera_INTERNAL_CALL_RaycastTry2D_m4005245494_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Camera::INTERNAL_CALL_RaycastTry2D(UnityEngine.Camera,UnityEngine.Ray&,System.Single,System.Int32)");
	return _il2cpp_icall_func(___self0, ___ray1, ___distance2, ___layerMask3);
}
// System.Void UnityEngine.Camera/CameraCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void CameraCallback__ctor_m1067541952 (CameraCallback_t190067161 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void UnityEngine.Camera/CameraCallback::Invoke(UnityEngine.Camera)
extern "C"  void CameraCallback_Invoke_m2206506905 (CameraCallback_t190067161 * __this, Camera_t4157153871 * ___cam0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		CameraCallback_Invoke_m2206506905((CameraCallback_t190067161 *)__this->get_prev_9(),___cam0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, Camera_t4157153871 * ___cam0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___cam0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (void* __this, Camera_t4157153871 * ___cam0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___cam0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(___cam0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult UnityEngine.Camera/CameraCallback::BeginInvoke(UnityEngine.Camera,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * CameraCallback_BeginInvoke_m870667444 (CameraCallback_t190067161 * __this, Camera_t4157153871 * ___cam0, AsyncCallback_t3962456242 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	void *__d_args[2] = {0};
	__d_args[0] = ___cam0;
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void UnityEngine.Camera/CameraCallback::EndInvoke(System.IAsyncResult)
extern "C"  void CameraCallback_EndInvoke_m606855993 (CameraCallback_t190067161 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// UnityEngine.RenderMode UnityEngine.Canvas::get_renderMode()
extern "C"  int32_t Canvas_get_renderMode_m449291776 (Canvas_t3310196443 * __this, const MethodInfo* method)
{
	typedef int32_t (*Canvas_get_renderMode_m449291776_ftn) (Canvas_t3310196443 *);
	static Canvas_get_renderMode_m449291776_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Canvas_get_renderMode_m449291776_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Canvas::get_renderMode()");
	return _il2cpp_icall_func(__this);
}
// System.Boolean UnityEngine.Canvas::get_isRootCanvas()
extern "C"  bool Canvas_get_isRootCanvas_m3048564350 (Canvas_t3310196443 * __this, const MethodInfo* method)
{
	typedef bool (*Canvas_get_isRootCanvas_m3048564350_ftn) (Canvas_t3310196443 *);
	static Canvas_get_isRootCanvas_m3048564350_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Canvas_get_isRootCanvas_m3048564350_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Canvas::get_isRootCanvas()");
	return _il2cpp_icall_func(__this);
}
// UnityEngine.Camera UnityEngine.Canvas::get_worldCamera()
extern "C"  Camera_t4157153871 * Canvas_get_worldCamera_m2289553767 (Canvas_t3310196443 * __this, const MethodInfo* method)
{
	typedef Camera_t4157153871 * (*Canvas_get_worldCamera_m2289553767_ftn) (Canvas_t3310196443 *);
	static Canvas_get_worldCamera_m2289553767_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Canvas_get_worldCamera_m2289553767_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Canvas::get_worldCamera()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Canvas::set_worldCamera(UnityEngine.Camera)
extern "C"  void Canvas_set_worldCamera_m3721138503 (Canvas_t3310196443 * __this, Camera_t4157153871 * ___value0, const MethodInfo* method)
{
	typedef void (*Canvas_set_worldCamera_m3721138503_ftn) (Canvas_t3310196443 *, Camera_t4157153871 *);
	static Canvas_set_worldCamera_m3721138503_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Canvas_set_worldCamera_m3721138503_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Canvas::set_worldCamera(UnityEngine.Camera)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Single UnityEngine.Canvas::get_scaleFactor()
extern "C"  float Canvas_get_scaleFactor_m2033584752 (Canvas_t3310196443 * __this, const MethodInfo* method)
{
	typedef float (*Canvas_get_scaleFactor_m2033584752_ftn) (Canvas_t3310196443 *);
	static Canvas_get_scaleFactor_m2033584752_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Canvas_get_scaleFactor_m2033584752_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Canvas::get_scaleFactor()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Canvas::set_scaleFactor(System.Single)
extern "C"  void Canvas_set_scaleFactor_m2249445925 (Canvas_t3310196443 * __this, float ___value0, const MethodInfo* method)
{
	typedef void (*Canvas_set_scaleFactor_m2249445925_ftn) (Canvas_t3310196443 *, float);
	static Canvas_set_scaleFactor_m2249445925_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Canvas_set_scaleFactor_m2249445925_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Canvas::set_scaleFactor(System.Single)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Single UnityEngine.Canvas::get_referencePixelsPerUnit()
extern "C"  float Canvas_get_referencePixelsPerUnit_m4240209832 (Canvas_t3310196443 * __this, const MethodInfo* method)
{
	typedef float (*Canvas_get_referencePixelsPerUnit_m4240209832_ftn) (Canvas_t3310196443 *);
	static Canvas_get_referencePixelsPerUnit_m4240209832_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Canvas_get_referencePixelsPerUnit_m4240209832_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Canvas::get_referencePixelsPerUnit()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Canvas::set_referencePixelsPerUnit(System.Single)
extern "C"  void Canvas_set_referencePixelsPerUnit_m1895686772 (Canvas_t3310196443 * __this, float ___value0, const MethodInfo* method)
{
	typedef void (*Canvas_set_referencePixelsPerUnit_m1895686772_ftn) (Canvas_t3310196443 *, float);
	static Canvas_set_referencePixelsPerUnit_m1895686772_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Canvas_set_referencePixelsPerUnit_m1895686772_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Canvas::set_referencePixelsPerUnit(System.Single)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Boolean UnityEngine.Canvas::get_pixelPerfect()
extern "C"  bool Canvas_get_pixelPerfect_m551317767 (Canvas_t3310196443 * __this, const MethodInfo* method)
{
	typedef bool (*Canvas_get_pixelPerfect_m551317767_ftn) (Canvas_t3310196443 *);
	static Canvas_get_pixelPerfect_m551317767_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Canvas_get_pixelPerfect_m551317767_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Canvas::get_pixelPerfect()");
	return _il2cpp_icall_func(__this);
}
// System.Int32 UnityEngine.Canvas::get_renderOrder()
extern "C"  int32_t Canvas_get_renderOrder_m3299857801 (Canvas_t3310196443 * __this, const MethodInfo* method)
{
	typedef int32_t (*Canvas_get_renderOrder_m3299857801_ftn) (Canvas_t3310196443 *);
	static Canvas_get_renderOrder_m3299857801_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Canvas_get_renderOrder_m3299857801_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Canvas::get_renderOrder()");
	return _il2cpp_icall_func(__this);
}
// System.Boolean UnityEngine.Canvas::get_overrideSorting()
extern "C"  bool Canvas_get_overrideSorting_m1424123543 (Canvas_t3310196443 * __this, const MethodInfo* method)
{
	typedef bool (*Canvas_get_overrideSorting_m1424123543_ftn) (Canvas_t3310196443 *);
	static Canvas_get_overrideSorting_m1424123543_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Canvas_get_overrideSorting_m1424123543_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Canvas::get_overrideSorting()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Canvas::set_overrideSorting(System.Boolean)
extern "C"  void Canvas_set_overrideSorting_m509353260 (Canvas_t3310196443 * __this, bool ___value0, const MethodInfo* method)
{
	typedef void (*Canvas_set_overrideSorting_m509353260_ftn) (Canvas_t3310196443 *, bool);
	static Canvas_set_overrideSorting_m509353260_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Canvas_set_overrideSorting_m509353260_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Canvas::set_overrideSorting(System.Boolean)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Int32 UnityEngine.Canvas::get_sortingOrder()
extern "C"  int32_t Canvas_get_sortingOrder_m424384399 (Canvas_t3310196443 * __this, const MethodInfo* method)
{
	typedef int32_t (*Canvas_get_sortingOrder_m424384399_ftn) (Canvas_t3310196443 *);
	static Canvas_get_sortingOrder_m424384399_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Canvas_get_sortingOrder_m424384399_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Canvas::get_sortingOrder()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Canvas::set_sortingOrder(System.Int32)
extern "C"  void Canvas_set_sortingOrder_m2825109295 (Canvas_t3310196443 * __this, int32_t ___value0, const MethodInfo* method)
{
	typedef void (*Canvas_set_sortingOrder_m2825109295_ftn) (Canvas_t3310196443 *, int32_t);
	static Canvas_set_sortingOrder_m2825109295_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Canvas_set_sortingOrder_m2825109295_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Canvas::set_sortingOrder(System.Int32)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Int32 UnityEngine.Canvas::get_targetDisplay()
extern "C"  int32_t Canvas_get_targetDisplay_m486586852 (Canvas_t3310196443 * __this, const MethodInfo* method)
{
	typedef int32_t (*Canvas_get_targetDisplay_m486586852_ftn) (Canvas_t3310196443 *);
	static Canvas_get_targetDisplay_m486586852_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Canvas_get_targetDisplay_m486586852_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Canvas::get_targetDisplay()");
	return _il2cpp_icall_func(__this);
}
// System.Int32 UnityEngine.Canvas::get_sortingLayerID()
extern "C"  int32_t Canvas_get_sortingLayerID_m1242506597 (Canvas_t3310196443 * __this, const MethodInfo* method)
{
	typedef int32_t (*Canvas_get_sortingLayerID_m1242506597_ftn) (Canvas_t3310196443 *);
	static Canvas_get_sortingLayerID_m1242506597_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Canvas_get_sortingLayerID_m1242506597_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Canvas::get_sortingLayerID()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Canvas::set_sortingLayerID(System.Int32)
extern "C"  void Canvas_set_sortingLayerID_m3423762275 (Canvas_t3310196443 * __this, int32_t ___value0, const MethodInfo* method)
{
	typedef void (*Canvas_set_sortingLayerID_m3423762275_ftn) (Canvas_t3310196443 *, int32_t);
	static Canvas_set_sortingLayerID_m3423762275_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Canvas_set_sortingLayerID_m3423762275_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Canvas::set_sortingLayerID(System.Int32)");
	_il2cpp_icall_func(__this, ___value0);
}
// UnityEngine.AdditionalCanvasShaderChannels UnityEngine.Canvas::get_additionalShaderChannels()
extern "C"  int32_t Canvas_get_additionalShaderChannels_m256878886 (Canvas_t3310196443 * __this, const MethodInfo* method)
{
	typedef int32_t (*Canvas_get_additionalShaderChannels_m256878886_ftn) (Canvas_t3310196443 *);
	static Canvas_get_additionalShaderChannels_m256878886_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Canvas_get_additionalShaderChannels_m256878886_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Canvas::get_additionalShaderChannels()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Canvas::set_additionalShaderChannels(UnityEngine.AdditionalCanvasShaderChannels)
extern "C"  void Canvas_set_additionalShaderChannels_m4232358317 (Canvas_t3310196443 * __this, int32_t ___value0, const MethodInfo* method)
{
	typedef void (*Canvas_set_additionalShaderChannels_m4232358317_ftn) (Canvas_t3310196443 *, int32_t);
	static Canvas_set_additionalShaderChannels_m4232358317_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Canvas_set_additionalShaderChannels_m4232358317_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Canvas::set_additionalShaderChannels(UnityEngine.AdditionalCanvasShaderChannels)");
	_il2cpp_icall_func(__this, ___value0);
}
// UnityEngine.Canvas UnityEngine.Canvas::get_rootCanvas()
extern "C"  Canvas_t3310196443 * Canvas_get_rootCanvas_m753521845 (Canvas_t3310196443 * __this, const MethodInfo* method)
{
	typedef Canvas_t3310196443 * (*Canvas_get_rootCanvas_m753521845_ftn) (Canvas_t3310196443 *);
	static Canvas_get_rootCanvas_m753521845_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Canvas_get_rootCanvas_m753521845_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Canvas::get_rootCanvas()");
	return _il2cpp_icall_func(__this);
}
// UnityEngine.Material UnityEngine.Canvas::GetDefaultCanvasMaterial()
extern "C"  Material_t340375123 * Canvas_GetDefaultCanvasMaterial_m2548556249 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef Material_t340375123 * (*Canvas_GetDefaultCanvasMaterial_m2548556249_ftn) ();
	static Canvas_GetDefaultCanvasMaterial_m2548556249_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Canvas_GetDefaultCanvasMaterial_m2548556249_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Canvas::GetDefaultCanvasMaterial()");
	return _il2cpp_icall_func();
}
// UnityEngine.Material UnityEngine.Canvas::GetETC1SupportedCanvasMaterial()
extern "C"  Material_t340375123 * Canvas_GetETC1SupportedCanvasMaterial_m2276645594 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef Material_t340375123 * (*Canvas_GetETC1SupportedCanvasMaterial_m2276645594_ftn) ();
	static Canvas_GetETC1SupportedCanvasMaterial_m2276645594_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Canvas_GetETC1SupportedCanvasMaterial_m2276645594_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Canvas::GetETC1SupportedCanvasMaterial()");
	return _il2cpp_icall_func();
}
// System.Void UnityEngine.Canvas::add_willRenderCanvases(UnityEngine.Canvas/WillRenderCanvases)
extern "C"  void Canvas_add_willRenderCanvases_m334285881 (Il2CppObject * __this /* static, unused */, WillRenderCanvases_t3309123499 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Canvas_add_willRenderCanvases_m334285881_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	WillRenderCanvases_t3309123499 * V_0 = NULL;
	WillRenderCanvases_t3309123499 * V_1 = NULL;
	{
		WillRenderCanvases_t3309123499 * L_0 = ((Canvas_t3310196443_StaticFields*)Canvas_t3310196443_il2cpp_TypeInfo_var->static_fields)->get_willRenderCanvases_2();
		V_0 = L_0;
	}

IL_0006:
	{
		WillRenderCanvases_t3309123499 * L_1 = V_0;
		V_1 = L_1;
		WillRenderCanvases_t3309123499 * L_2 = V_1;
		WillRenderCanvases_t3309123499 * L_3 = ___value0;
		Delegate_t1188392813 * L_4 = Delegate_Combine_m1859655160(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		WillRenderCanvases_t3309123499 * L_5 = V_0;
		WillRenderCanvases_t3309123499 * L_6 = InterlockedCompareExchangeImpl<WillRenderCanvases_t3309123499 *>((((Canvas_t3310196443_StaticFields*)Canvas_t3310196443_il2cpp_TypeInfo_var->static_fields)->get_address_of_willRenderCanvases_2()), ((WillRenderCanvases_t3309123499 *)CastclassSealed(L_4, WillRenderCanvases_t3309123499_il2cpp_TypeInfo_var)), L_5);
		V_0 = L_6;
		WillRenderCanvases_t3309123499 * L_7 = V_0;
		WillRenderCanvases_t3309123499 * L_8 = V_1;
		if ((!(((Il2CppObject*)(WillRenderCanvases_t3309123499 *)L_7) == ((Il2CppObject*)(WillRenderCanvases_t3309123499 *)L_8))))
		{
			goto IL_0006;
		}
	}
	{
		return;
	}
}
// System.Void UnityEngine.Canvas::remove_willRenderCanvases(UnityEngine.Canvas/WillRenderCanvases)
extern "C"  void Canvas_remove_willRenderCanvases_m2711004648 (Il2CppObject * __this /* static, unused */, WillRenderCanvases_t3309123499 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Canvas_remove_willRenderCanvases_m2711004648_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	WillRenderCanvases_t3309123499 * V_0 = NULL;
	WillRenderCanvases_t3309123499 * V_1 = NULL;
	{
		WillRenderCanvases_t3309123499 * L_0 = ((Canvas_t3310196443_StaticFields*)Canvas_t3310196443_il2cpp_TypeInfo_var->static_fields)->get_willRenderCanvases_2();
		V_0 = L_0;
	}

IL_0006:
	{
		WillRenderCanvases_t3309123499 * L_1 = V_0;
		V_1 = L_1;
		WillRenderCanvases_t3309123499 * L_2 = V_1;
		WillRenderCanvases_t3309123499 * L_3 = ___value0;
		Delegate_t1188392813 * L_4 = Delegate_Remove_m334097152(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		WillRenderCanvases_t3309123499 * L_5 = V_0;
		WillRenderCanvases_t3309123499 * L_6 = InterlockedCompareExchangeImpl<WillRenderCanvases_t3309123499 *>((((Canvas_t3310196443_StaticFields*)Canvas_t3310196443_il2cpp_TypeInfo_var->static_fields)->get_address_of_willRenderCanvases_2()), ((WillRenderCanvases_t3309123499 *)CastclassSealed(L_4, WillRenderCanvases_t3309123499_il2cpp_TypeInfo_var)), L_5);
		V_0 = L_6;
		WillRenderCanvases_t3309123499 * L_7 = V_0;
		WillRenderCanvases_t3309123499 * L_8 = V_1;
		if ((!(((Il2CppObject*)(WillRenderCanvases_t3309123499 *)L_7) == ((Il2CppObject*)(WillRenderCanvases_t3309123499 *)L_8))))
		{
			goto IL_0006;
		}
	}
	{
		return;
	}
}
// System.Void UnityEngine.Canvas::SendWillRenderCanvases()
extern "C"  void Canvas_SendWillRenderCanvases_m2809604487 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Canvas_SendWillRenderCanvases_m2809604487_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		WillRenderCanvases_t3309123499 * L_0 = ((Canvas_t3310196443_StaticFields*)Canvas_t3310196443_il2cpp_TypeInfo_var->static_fields)->get_willRenderCanvases_2();
		if (!L_0)
		{
			goto IL_0015;
		}
	}
	{
		WillRenderCanvases_t3309123499 * L_1 = ((Canvas_t3310196443_StaticFields*)Canvas_t3310196443_il2cpp_TypeInfo_var->static_fields)->get_willRenderCanvases_2();
		NullCheck(L_1);
		WillRenderCanvases_Invoke_m352835184(L_1, /*hidden argument*/NULL);
	}

IL_0015:
	{
		return;
	}
}
// System.Void UnityEngine.Canvas::ForceUpdateCanvases()
extern "C"  void Canvas_ForceUpdateCanvases_m1969676771 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		Canvas_SendWillRenderCanvases_m2809604487(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
extern "C"  void DelegatePInvokeWrapper_WillRenderCanvases_t3309123499 (WillRenderCanvases_t3309123499 * __this, const MethodInfo* method)
{
	typedef void (STDCALL *PInvokeFunc)();
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(((Il2CppDelegate*)__this)->method->methodPointer);

	// Native function invocation
	il2cppPInvokeFunc();

}
// System.Void UnityEngine.Canvas/WillRenderCanvases::.ctor(System.Object,System.IntPtr)
extern "C"  void WillRenderCanvases__ctor_m1674343690 (WillRenderCanvases_t3309123499 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void UnityEngine.Canvas/WillRenderCanvases::Invoke()
extern "C"  void WillRenderCanvases_Invoke_m352835184 (WillRenderCanvases_t3309123499 * __this, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		WillRenderCanvases_Invoke_m352835184((WillRenderCanvases_t3309123499 *)__this->get_prev_9(), method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if ((__this->get_m_target_2() != NULL || MethodHasParameters((MethodInfo*)(__this->get_method_3().get_m_value_0()))) && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult UnityEngine.Canvas/WillRenderCanvases::BeginInvoke(System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * WillRenderCanvases_BeginInvoke_m366478605 (WillRenderCanvases_t3309123499 * __this, AsyncCallback_t3962456242 * ___callback0, Il2CppObject * ___object1, const MethodInfo* method)
{
	void *__d_args[1] = {0};
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback0, (Il2CppObject*)___object1);
}
// System.Void UnityEngine.Canvas/WillRenderCanvases::EndInvoke(System.IAsyncResult)
extern "C"  void WillRenderCanvases_EndInvoke_m1999799517 (WillRenderCanvases_t3309123499 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Single UnityEngine.CanvasGroup::get_alpha()
extern "C"  float CanvasGroup_get_alpha_m913685212 (CanvasGroup_t4083511760 * __this, const MethodInfo* method)
{
	typedef float (*CanvasGroup_get_alpha_m913685212_ftn) (CanvasGroup_t4083511760 *);
	static CanvasGroup_get_alpha_m913685212_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (CanvasGroup_get_alpha_m913685212_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.CanvasGroup::get_alpha()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.CanvasGroup::set_alpha(System.Single)
extern "C"  void CanvasGroup_set_alpha_m900432846 (CanvasGroup_t4083511760 * __this, float ___value0, const MethodInfo* method)
{
	typedef void (*CanvasGroup_set_alpha_m900432846_ftn) (CanvasGroup_t4083511760 *, float);
	static CanvasGroup_set_alpha_m900432846_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (CanvasGroup_set_alpha_m900432846_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.CanvasGroup::set_alpha(System.Single)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Boolean UnityEngine.CanvasGroup::get_interactable()
extern "C"  bool CanvasGroup_get_interactable_m176056025 (CanvasGroup_t4083511760 * __this, const MethodInfo* method)
{
	typedef bool (*CanvasGroup_get_interactable_m176056025_ftn) (CanvasGroup_t4083511760 *);
	static CanvasGroup_get_interactable_m176056025_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (CanvasGroup_get_interactable_m176056025_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.CanvasGroup::get_interactable()");
	return _il2cpp_icall_func(__this);
}
// System.Boolean UnityEngine.CanvasGroup::get_blocksRaycasts()
extern "C"  bool CanvasGroup_get_blocksRaycasts_m3740270137 (CanvasGroup_t4083511760 * __this, const MethodInfo* method)
{
	typedef bool (*CanvasGroup_get_blocksRaycasts_m3740270137_ftn) (CanvasGroup_t4083511760 *);
	static CanvasGroup_get_blocksRaycasts_m3740270137_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (CanvasGroup_get_blocksRaycasts_m3740270137_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.CanvasGroup::get_blocksRaycasts()");
	return _il2cpp_icall_func(__this);
}
// System.Boolean UnityEngine.CanvasGroup::get_ignoreParentGroups()
extern "C"  bool CanvasGroup_get_ignoreParentGroups_m551440072 (CanvasGroup_t4083511760 * __this, const MethodInfo* method)
{
	typedef bool (*CanvasGroup_get_ignoreParentGroups_m551440072_ftn) (CanvasGroup_t4083511760 *);
	static CanvasGroup_get_ignoreParentGroups_m551440072_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (CanvasGroup_get_ignoreParentGroups_m551440072_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.CanvasGroup::get_ignoreParentGroups()");
	return _il2cpp_icall_func(__this);
}
// System.Boolean UnityEngine.CanvasGroup::IsRaycastLocationValid(UnityEngine.Vector2,UnityEngine.Camera)
extern "C"  bool CanvasGroup_IsRaycastLocationValid_m4078037026 (CanvasGroup_t4083511760 * __this, Vector2_t2156229523  ___sp0, Camera_t4157153871 * ___eventCamera1, const MethodInfo* method)
{
	bool V_0 = false;
	{
		bool L_0 = CanvasGroup_get_blocksRaycasts_m3740270137(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		bool L_1 = V_0;
		return L_1;
	}
}
// System.Void UnityEngine.CanvasRenderer::SetColor(UnityEngine.Color)
extern "C"  void CanvasRenderer_SetColor_m3481008010 (CanvasRenderer_t2598313366 * __this, Color_t2555686324  ___color0, const MethodInfo* method)
{
	{
		CanvasRenderer_INTERNAL_CALL_SetColor_m975881866(NULL /*static, unused*/, __this, (&___color0), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.CanvasRenderer::INTERNAL_CALL_SetColor(UnityEngine.CanvasRenderer,UnityEngine.Color&)
extern "C"  void CanvasRenderer_INTERNAL_CALL_SetColor_m975881866 (Il2CppObject * __this /* static, unused */, CanvasRenderer_t2598313366 * ___self0, Color_t2555686324 * ___color1, const MethodInfo* method)
{
	typedef void (*CanvasRenderer_INTERNAL_CALL_SetColor_m975881866_ftn) (CanvasRenderer_t2598313366 *, Color_t2555686324 *);
	static CanvasRenderer_INTERNAL_CALL_SetColor_m975881866_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (CanvasRenderer_INTERNAL_CALL_SetColor_m975881866_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.CanvasRenderer::INTERNAL_CALL_SetColor(UnityEngine.CanvasRenderer,UnityEngine.Color&)");
	_il2cpp_icall_func(___self0, ___color1);
}
// UnityEngine.Color UnityEngine.CanvasRenderer::GetColor()
extern "C"  Color_t2555686324  CanvasRenderer_GetColor_m4231047588 (CanvasRenderer_t2598313366 * __this, const MethodInfo* method)
{
	Color_t2555686324  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Color_t2555686324  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		CanvasRenderer_INTERNAL_CALL_GetColor_m21875977(NULL /*static, unused*/, __this, (&V_0), /*hidden argument*/NULL);
		Color_t2555686324  L_0 = V_0;
		V_1 = L_0;
		goto IL_0010;
	}

IL_0010:
	{
		Color_t2555686324  L_1 = V_1;
		return L_1;
	}
}
// System.Void UnityEngine.CanvasRenderer::INTERNAL_CALL_GetColor(UnityEngine.CanvasRenderer,UnityEngine.Color&)
extern "C"  void CanvasRenderer_INTERNAL_CALL_GetColor_m21875977 (Il2CppObject * __this /* static, unused */, CanvasRenderer_t2598313366 * ___self0, Color_t2555686324 * ___value1, const MethodInfo* method)
{
	typedef void (*CanvasRenderer_INTERNAL_CALL_GetColor_m21875977_ftn) (CanvasRenderer_t2598313366 *, Color_t2555686324 *);
	static CanvasRenderer_INTERNAL_CALL_GetColor_m21875977_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (CanvasRenderer_INTERNAL_CALL_GetColor_m21875977_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.CanvasRenderer::INTERNAL_CALL_GetColor(UnityEngine.CanvasRenderer,UnityEngine.Color&)");
	_il2cpp_icall_func(___self0, ___value1);
}
// System.Void UnityEngine.CanvasRenderer::SetVertices(UnityEngine.UIVertex[],System.Int32)
extern "C"  void CanvasRenderer_SetVertices_m229633174 (CanvasRenderer_t2598313366 * __this, UIVertexU5BU5D_t1981460040* ___vertices0, int32_t ___size1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CanvasRenderer_SetVertices_m229633174_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Mesh_t3648964284 * V_0 = NULL;
	List_1_t899420910 * V_1 = NULL;
	List_1_t4072576034 * V_2 = NULL;
	List_1_t3628304265 * V_3 = NULL;
	List_1_t3628304265 * V_4 = NULL;
	List_1_t899420910 * V_5 = NULL;
	List_1_t496136383 * V_6 = NULL;
	List_1_t128053199 * V_7 = NULL;
	int32_t V_8 = 0;
	int32_t V_9 = 0;
	{
		Mesh_t3648964284 * L_0 = (Mesh_t3648964284 *)il2cpp_codegen_object_new(Mesh_t3648964284_il2cpp_TypeInfo_var);
		Mesh__ctor_m2962122670(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		List_1_t899420910 * L_1 = (List_1_t899420910 *)il2cpp_codegen_object_new(List_1_t899420910_il2cpp_TypeInfo_var);
		List_1__ctor_m4218989665(L_1, /*hidden argument*/List_1__ctor_m4218989665_MethodInfo_var);
		V_1 = L_1;
		List_1_t4072576034 * L_2 = (List_1_t4072576034 *)il2cpp_codegen_object_new(List_1_t4072576034_il2cpp_TypeInfo_var);
		List_1__ctor_m2033536304(L_2, /*hidden argument*/List_1__ctor_m2033536304_MethodInfo_var);
		V_2 = L_2;
		List_1_t3628304265 * L_3 = (List_1_t3628304265 *)il2cpp_codegen_object_new(List_1_t3628304265_il2cpp_TypeInfo_var);
		List_1__ctor_m4218988514(L_3, /*hidden argument*/List_1__ctor_m4218988514_MethodInfo_var);
		V_3 = L_3;
		List_1_t3628304265 * L_4 = (List_1_t3628304265 *)il2cpp_codegen_object_new(List_1_t3628304265_il2cpp_TypeInfo_var);
		List_1__ctor_m4218988514(L_4, /*hidden argument*/List_1__ctor_m4218988514_MethodInfo_var);
		V_4 = L_4;
		List_1_t899420910 * L_5 = (List_1_t899420910 *)il2cpp_codegen_object_new(List_1_t899420910_il2cpp_TypeInfo_var);
		List_1__ctor_m4218989665(L_5, /*hidden argument*/List_1__ctor_m4218989665_MethodInfo_var);
		V_5 = L_5;
		List_1_t496136383 * L_6 = (List_1_t496136383 *)il2cpp_codegen_object_new(List_1_t496136383_il2cpp_TypeInfo_var);
		List_1__ctor_m4218986464(L_6, /*hidden argument*/List_1__ctor_m4218986464_MethodInfo_var);
		V_6 = L_6;
		List_1_t128053199 * L_7 = (List_1_t128053199 *)il2cpp_codegen_object_new(List_1_t128053199_il2cpp_TypeInfo_var);
		List_1__ctor_m4278196146(L_7, /*hidden argument*/List_1__ctor_m4278196146_MethodInfo_var);
		V_7 = L_7;
		V_8 = 0;
		goto IL_0122;
	}

IL_003d:
	{
		V_9 = 0;
		goto IL_00d5;
	}

IL_0046:
	{
		List_1_t899420910 * L_8 = V_1;
		UIVertexU5BU5D_t1981460040* L_9 = ___vertices0;
		int32_t L_10 = V_8;
		int32_t L_11 = V_9;
		NullCheck(L_9);
		Vector3_t3722313464  L_12 = ((L_9)->GetAddressAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)L_10+(int32_t)L_11)))))->get_position_0();
		NullCheck(L_8);
		List_1_Add_m4259514621(L_8, L_12, /*hidden argument*/List_1_Add_m4259514621_MethodInfo_var);
		List_1_t4072576034 * L_13 = V_2;
		UIVertexU5BU5D_t1981460040* L_14 = ___vertices0;
		int32_t L_15 = V_8;
		int32_t L_16 = V_9;
		NullCheck(L_14);
		Color32_t2600501292  L_17 = ((L_14)->GetAddressAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)L_15+(int32_t)L_16)))))->get_color_2();
		NullCheck(L_13);
		List_1_Add_m3372602044(L_13, L_17, /*hidden argument*/List_1_Add_m3372602044_MethodInfo_var);
		List_1_t3628304265 * L_18 = V_3;
		UIVertexU5BU5D_t1981460040* L_19 = ___vertices0;
		int32_t L_20 = V_8;
		int32_t L_21 = V_9;
		NullCheck(L_19);
		Vector2_t2156229523  L_22 = ((L_19)->GetAddressAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)L_20+(int32_t)L_21)))))->get_uv0_3();
		NullCheck(L_18);
		List_1_Add_m4259515774(L_18, L_22, /*hidden argument*/List_1_Add_m4259515774_MethodInfo_var);
		List_1_t3628304265 * L_23 = V_4;
		UIVertexU5BU5D_t1981460040* L_24 = ___vertices0;
		int32_t L_25 = V_8;
		int32_t L_26 = V_9;
		NullCheck(L_24);
		Vector2_t2156229523  L_27 = ((L_24)->GetAddressAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)L_25+(int32_t)L_26)))))->get_uv1_4();
		NullCheck(L_23);
		List_1_Add_m4259515774(L_23, L_27, /*hidden argument*/List_1_Add_m4259515774_MethodInfo_var);
		List_1_t899420910 * L_28 = V_5;
		UIVertexU5BU5D_t1981460040* L_29 = ___vertices0;
		int32_t L_30 = V_8;
		int32_t L_31 = V_9;
		NullCheck(L_29);
		Vector3_t3722313464  L_32 = ((L_29)->GetAddressAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)L_30+(int32_t)L_31)))))->get_normal_1();
		NullCheck(L_28);
		List_1_Add_m4259514621(L_28, L_32, /*hidden argument*/List_1_Add_m4259514621_MethodInfo_var);
		List_1_t496136383 * L_33 = V_6;
		UIVertexU5BU5D_t1981460040* L_34 = ___vertices0;
		int32_t L_35 = V_8;
		int32_t L_36 = V_9;
		NullCheck(L_34);
		Vector4_t3319028937  L_37 = ((L_34)->GetAddressAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)L_35+(int32_t)L_36)))))->get_tangent_7();
		NullCheck(L_33);
		List_1_Add_m4259509372(L_33, L_37, /*hidden argument*/List_1_Add_m4259509372_MethodInfo_var);
		int32_t L_38 = V_9;
		V_9 = ((int32_t)((int32_t)L_38+(int32_t)1));
	}

IL_00d5:
	{
		int32_t L_39 = V_9;
		if ((((int32_t)L_39) < ((int32_t)4)))
		{
			goto IL_0046;
		}
	}
	{
		List_1_t128053199 * L_40 = V_7;
		int32_t L_41 = V_8;
		NullCheck(L_40);
		List_1_Add_m4025721294(L_40, L_41, /*hidden argument*/List_1_Add_m4025721294_MethodInfo_var);
		List_1_t128053199 * L_42 = V_7;
		int32_t L_43 = V_8;
		NullCheck(L_42);
		List_1_Add_m4025721294(L_42, ((int32_t)((int32_t)L_43+(int32_t)1)), /*hidden argument*/List_1_Add_m4025721294_MethodInfo_var);
		List_1_t128053199 * L_44 = V_7;
		int32_t L_45 = V_8;
		NullCheck(L_44);
		List_1_Add_m4025721294(L_44, ((int32_t)((int32_t)L_45+(int32_t)2)), /*hidden argument*/List_1_Add_m4025721294_MethodInfo_var);
		List_1_t128053199 * L_46 = V_7;
		int32_t L_47 = V_8;
		NullCheck(L_46);
		List_1_Add_m4025721294(L_46, ((int32_t)((int32_t)L_47+(int32_t)2)), /*hidden argument*/List_1_Add_m4025721294_MethodInfo_var);
		List_1_t128053199 * L_48 = V_7;
		int32_t L_49 = V_8;
		NullCheck(L_48);
		List_1_Add_m4025721294(L_48, ((int32_t)((int32_t)L_49+(int32_t)3)), /*hidden argument*/List_1_Add_m4025721294_MethodInfo_var);
		List_1_t128053199 * L_50 = V_7;
		int32_t L_51 = V_8;
		NullCheck(L_50);
		List_1_Add_m4025721294(L_50, L_51, /*hidden argument*/List_1_Add_m4025721294_MethodInfo_var);
		int32_t L_52 = V_8;
		V_8 = ((int32_t)((int32_t)L_52+(int32_t)4));
	}

IL_0122:
	{
		int32_t L_53 = V_8;
		int32_t L_54 = ___size1;
		if ((((int32_t)L_53) < ((int32_t)L_54)))
		{
			goto IL_003d;
		}
	}
	{
		Mesh_t3648964284 * L_55 = V_0;
		List_1_t899420910 * L_56 = V_1;
		NullCheck(L_55);
		Mesh_SetVertices_m2641879696(L_55, L_56, /*hidden argument*/NULL);
		Mesh_t3648964284 * L_57 = V_0;
		List_1_t4072576034 * L_58 = V_2;
		NullCheck(L_57);
		Mesh_SetColors_m3397914746(L_57, L_58, /*hidden argument*/NULL);
		Mesh_t3648964284 * L_59 = V_0;
		List_1_t899420910 * L_60 = V_5;
		NullCheck(L_59);
		Mesh_SetNormals_m404565226(L_59, L_60, /*hidden argument*/NULL);
		Mesh_t3648964284 * L_61 = V_0;
		List_1_t496136383 * L_62 = V_6;
		NullCheck(L_61);
		Mesh_SetTangents_m2687625516(L_61, L_62, /*hidden argument*/NULL);
		Mesh_t3648964284 * L_63 = V_0;
		List_1_t3628304265 * L_64 = V_3;
		NullCheck(L_63);
		Mesh_SetUVs_m1321350398(L_63, 0, L_64, /*hidden argument*/NULL);
		Mesh_t3648964284 * L_65 = V_0;
		List_1_t3628304265 * L_66 = V_4;
		NullCheck(L_65);
		Mesh_SetUVs_m1321350398(L_65, 1, L_66, /*hidden argument*/NULL);
		Mesh_t3648964284 * L_67 = V_0;
		List_1_t128053199 * L_68 = V_7;
		NullCheck(L_68);
		Int32U5BU5D_t385246372* L_69 = List_1_ToArray_m789548171(L_68, /*hidden argument*/List_1_ToArray_m789548171_MethodInfo_var);
		NullCheck(L_67);
		Mesh_SetIndices_m3944365893(L_67, L_69, 0, 0, /*hidden argument*/NULL);
		Mesh_t3648964284 * L_70 = V_0;
		CanvasRenderer_SetMesh_m3875887985(__this, L_70, /*hidden argument*/NULL);
		Mesh_t3648964284 * L_71 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		Object_DestroyImmediate_m1556866283(NULL /*static, unused*/, L_71, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.CanvasRenderer::EnableRectClipping(UnityEngine.Rect)
extern "C"  void CanvasRenderer_EnableRectClipping_m2541364234 (CanvasRenderer_t2598313366 * __this, Rect_t2360479859  ___rect0, const MethodInfo* method)
{
	{
		CanvasRenderer_INTERNAL_CALL_EnableRectClipping_m1902636664(NULL /*static, unused*/, __this, (&___rect0), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.CanvasRenderer::INTERNAL_CALL_EnableRectClipping(UnityEngine.CanvasRenderer,UnityEngine.Rect&)
extern "C"  void CanvasRenderer_INTERNAL_CALL_EnableRectClipping_m1902636664 (Il2CppObject * __this /* static, unused */, CanvasRenderer_t2598313366 * ___self0, Rect_t2360479859 * ___rect1, const MethodInfo* method)
{
	typedef void (*CanvasRenderer_INTERNAL_CALL_EnableRectClipping_m1902636664_ftn) (CanvasRenderer_t2598313366 *, Rect_t2360479859 *);
	static CanvasRenderer_INTERNAL_CALL_EnableRectClipping_m1902636664_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (CanvasRenderer_INTERNAL_CALL_EnableRectClipping_m1902636664_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.CanvasRenderer::INTERNAL_CALL_EnableRectClipping(UnityEngine.CanvasRenderer,UnityEngine.Rect&)");
	_il2cpp_icall_func(___self0, ___rect1);
}
// System.Void UnityEngine.CanvasRenderer::DisableRectClipping()
extern "C"  void CanvasRenderer_DisableRectClipping_m4197748798 (CanvasRenderer_t2598313366 * __this, const MethodInfo* method)
{
	typedef void (*CanvasRenderer_DisableRectClipping_m4197748798_ftn) (CanvasRenderer_t2598313366 *);
	static CanvasRenderer_DisableRectClipping_m4197748798_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (CanvasRenderer_DisableRectClipping_m4197748798_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.CanvasRenderer::DisableRectClipping()");
	_il2cpp_icall_func(__this);
}
// System.Void UnityEngine.CanvasRenderer::set_hasPopInstruction(System.Boolean)
extern "C"  void CanvasRenderer_set_hasPopInstruction_m4160815714 (CanvasRenderer_t2598313366 * __this, bool ___value0, const MethodInfo* method)
{
	typedef void (*CanvasRenderer_set_hasPopInstruction_m4160815714_ftn) (CanvasRenderer_t2598313366 *, bool);
	static CanvasRenderer_set_hasPopInstruction_m4160815714_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (CanvasRenderer_set_hasPopInstruction_m4160815714_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.CanvasRenderer::set_hasPopInstruction(System.Boolean)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Int32 UnityEngine.CanvasRenderer::get_materialCount()
extern "C"  int32_t CanvasRenderer_get_materialCount_m3538244728 (CanvasRenderer_t2598313366 * __this, const MethodInfo* method)
{
	typedef int32_t (*CanvasRenderer_get_materialCount_m3538244728_ftn) (CanvasRenderer_t2598313366 *);
	static CanvasRenderer_get_materialCount_m3538244728_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (CanvasRenderer_get_materialCount_m3538244728_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.CanvasRenderer::get_materialCount()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.CanvasRenderer::set_materialCount(System.Int32)
extern "C"  void CanvasRenderer_set_materialCount_m3744103248 (CanvasRenderer_t2598313366 * __this, int32_t ___value0, const MethodInfo* method)
{
	typedef void (*CanvasRenderer_set_materialCount_m3744103248_ftn) (CanvasRenderer_t2598313366 *, int32_t);
	static CanvasRenderer_set_materialCount_m3744103248_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (CanvasRenderer_set_materialCount_m3744103248_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.CanvasRenderer::set_materialCount(System.Int32)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.CanvasRenderer::SetMaterial(UnityEngine.Material,System.Int32)
extern "C"  void CanvasRenderer_SetMaterial_m856908584 (CanvasRenderer_t2598313366 * __this, Material_t340375123 * ___material0, int32_t ___index1, const MethodInfo* method)
{
	typedef void (*CanvasRenderer_SetMaterial_m856908584_ftn) (CanvasRenderer_t2598313366 *, Material_t340375123 *, int32_t);
	static CanvasRenderer_SetMaterial_m856908584_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (CanvasRenderer_SetMaterial_m856908584_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.CanvasRenderer::SetMaterial(UnityEngine.Material,System.Int32)");
	_il2cpp_icall_func(__this, ___material0, ___index1);
}
// System.Void UnityEngine.CanvasRenderer::SetMaterial(UnityEngine.Material,UnityEngine.Texture)
extern "C"  void CanvasRenderer_SetMaterial_m572416531 (CanvasRenderer_t2598313366 * __this, Material_t340375123 * ___material0, Texture_t3661962703 * ___texture1, const MethodInfo* method)
{
	{
		int32_t L_0 = CanvasRenderer_get_materialCount_m3538244728(__this, /*hidden argument*/NULL);
		int32_t L_1 = Math_Max_m1873195862(NULL /*static, unused*/, 1, L_0, /*hidden argument*/NULL);
		CanvasRenderer_set_materialCount_m3744103248(__this, L_1, /*hidden argument*/NULL);
		Material_t340375123 * L_2 = ___material0;
		CanvasRenderer_SetMaterial_m856908584(__this, L_2, 0, /*hidden argument*/NULL);
		Texture_t3661962703 * L_3 = ___texture1;
		CanvasRenderer_SetTexture_m685945407(__this, L_3, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Material UnityEngine.CanvasRenderer::GetMaterial()
extern "C"  Material_t340375123 * CanvasRenderer_GetMaterial_m1804472845 (CanvasRenderer_t2598313366 * __this, const MethodInfo* method)
{
	Material_t340375123 * V_0 = NULL;
	{
		Material_t340375123 * L_0 = CanvasRenderer_GetMaterial_m221169768(__this, 0, /*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_000e;
	}

IL_000e:
	{
		Material_t340375123 * L_1 = V_0;
		return L_1;
	}
}
// UnityEngine.Material UnityEngine.CanvasRenderer::GetMaterial(System.Int32)
extern "C"  Material_t340375123 * CanvasRenderer_GetMaterial_m221169768 (CanvasRenderer_t2598313366 * __this, int32_t ___index0, const MethodInfo* method)
{
	typedef Material_t340375123 * (*CanvasRenderer_GetMaterial_m221169768_ftn) (CanvasRenderer_t2598313366 *, int32_t);
	static CanvasRenderer_GetMaterial_m221169768_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (CanvasRenderer_GetMaterial_m221169768_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.CanvasRenderer::GetMaterial(System.Int32)");
	return _il2cpp_icall_func(__this, ___index0);
}
// System.Void UnityEngine.CanvasRenderer::set_popMaterialCount(System.Int32)
extern "C"  void CanvasRenderer_set_popMaterialCount_m3278092344 (CanvasRenderer_t2598313366 * __this, int32_t ___value0, const MethodInfo* method)
{
	typedef void (*CanvasRenderer_set_popMaterialCount_m3278092344_ftn) (CanvasRenderer_t2598313366 *, int32_t);
	static CanvasRenderer_set_popMaterialCount_m3278092344_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (CanvasRenderer_set_popMaterialCount_m3278092344_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.CanvasRenderer::set_popMaterialCount(System.Int32)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.CanvasRenderer::SetPopMaterial(UnityEngine.Material,System.Int32)
extern "C"  void CanvasRenderer_SetPopMaterial_m2102730815 (CanvasRenderer_t2598313366 * __this, Material_t340375123 * ___material0, int32_t ___index1, const MethodInfo* method)
{
	typedef void (*CanvasRenderer_SetPopMaterial_m2102730815_ftn) (CanvasRenderer_t2598313366 *, Material_t340375123 *, int32_t);
	static CanvasRenderer_SetPopMaterial_m2102730815_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (CanvasRenderer_SetPopMaterial_m2102730815_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.CanvasRenderer::SetPopMaterial(UnityEngine.Material,System.Int32)");
	_il2cpp_icall_func(__this, ___material0, ___index1);
}
// System.Void UnityEngine.CanvasRenderer::SetTexture(UnityEngine.Texture)
extern "C"  void CanvasRenderer_SetTexture_m685945407 (CanvasRenderer_t2598313366 * __this, Texture_t3661962703 * ___texture0, const MethodInfo* method)
{
	typedef void (*CanvasRenderer_SetTexture_m685945407_ftn) (CanvasRenderer_t2598313366 *, Texture_t3661962703 *);
	static CanvasRenderer_SetTexture_m685945407_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (CanvasRenderer_SetTexture_m685945407_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.CanvasRenderer::SetTexture(UnityEngine.Texture)");
	_il2cpp_icall_func(__this, ___texture0);
}
// System.Void UnityEngine.CanvasRenderer::SetAlphaTexture(UnityEngine.Texture)
extern "C"  void CanvasRenderer_SetAlphaTexture_m2691085254 (CanvasRenderer_t2598313366 * __this, Texture_t3661962703 * ___texture0, const MethodInfo* method)
{
	typedef void (*CanvasRenderer_SetAlphaTexture_m2691085254_ftn) (CanvasRenderer_t2598313366 *, Texture_t3661962703 *);
	static CanvasRenderer_SetAlphaTexture_m2691085254_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (CanvasRenderer_SetAlphaTexture_m2691085254_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.CanvasRenderer::SetAlphaTexture(UnityEngine.Texture)");
	_il2cpp_icall_func(__this, ___texture0);
}
// System.Void UnityEngine.CanvasRenderer::SetMesh(UnityEngine.Mesh)
extern "C"  void CanvasRenderer_SetMesh_m3875887985 (CanvasRenderer_t2598313366 * __this, Mesh_t3648964284 * ___mesh0, const MethodInfo* method)
{
	typedef void (*CanvasRenderer_SetMesh_m3875887985_ftn) (CanvasRenderer_t2598313366 *, Mesh_t3648964284 *);
	static CanvasRenderer_SetMesh_m3875887985_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (CanvasRenderer_SetMesh_m3875887985_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.CanvasRenderer::SetMesh(UnityEngine.Mesh)");
	_il2cpp_icall_func(__this, ___mesh0);
}
// System.Void UnityEngine.CanvasRenderer::Clear()
extern "C"  void CanvasRenderer_Clear_m2758300145 (CanvasRenderer_t2598313366 * __this, const MethodInfo* method)
{
	typedef void (*CanvasRenderer_Clear_m2758300145_ftn) (CanvasRenderer_t2598313366 *);
	static CanvasRenderer_Clear_m2758300145_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (CanvasRenderer_Clear_m2758300145_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.CanvasRenderer::Clear()");
	_il2cpp_icall_func(__this);
}
// System.Void UnityEngine.CanvasRenderer::SplitUIVertexStreams(System.Collections.Generic.List`1<UnityEngine.UIVertex>,System.Collections.Generic.List`1<UnityEngine.Vector3>,System.Collections.Generic.List`1<UnityEngine.Color32>,System.Collections.Generic.List`1<UnityEngine.Vector2>,System.Collections.Generic.List`1<UnityEngine.Vector2>,System.Collections.Generic.List`1<UnityEngine.Vector3>,System.Collections.Generic.List`1<UnityEngine.Vector4>,System.Collections.Generic.List`1<System.Int32>)
extern "C"  void CanvasRenderer_SplitUIVertexStreams_m557540500 (Il2CppObject * __this /* static, unused */, List_1_t1234605051 * ___verts0, List_1_t899420910 * ___positions1, List_1_t4072576034 * ___colors2, List_1_t3628304265 * ___uv0S3, List_1_t3628304265 * ___uv1S4, List_1_t899420910 * ___normals5, List_1_t496136383 * ___tangents6, List_1_t128053199 * ___indicies7, const MethodInfo* method)
{
	{
		List_1_t1234605051 * L_0 = ___verts0;
		List_1_t899420910 * L_1 = ___positions1;
		List_1_t4072576034 * L_2 = ___colors2;
		List_1_t3628304265 * L_3 = ___uv0S3;
		List_1_t3628304265 * L_4 = ___uv1S4;
		List_1_t899420910 * L_5 = ___normals5;
		List_1_t496136383 * L_6 = ___tangents6;
		CanvasRenderer_SplitUIVertexStreamsInternal_m2784275415(NULL /*static, unused*/, L_0, L_1, L_2, L_3, L_4, L_5, L_6, /*hidden argument*/NULL);
		List_1_t1234605051 * L_7 = ___verts0;
		List_1_t128053199 * L_8 = ___indicies7;
		CanvasRenderer_SplitIndiciesStreamsInternal_m2583413468(NULL /*static, unused*/, L_7, L_8, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.CanvasRenderer::SplitUIVertexStreamsInternal(System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object)
extern "C"  void CanvasRenderer_SplitUIVertexStreamsInternal_m2784275415 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___verts0, Il2CppObject * ___positions1, Il2CppObject * ___colors2, Il2CppObject * ___uv0S3, Il2CppObject * ___uv1S4, Il2CppObject * ___normals5, Il2CppObject * ___tangents6, const MethodInfo* method)
{
	typedef void (*CanvasRenderer_SplitUIVertexStreamsInternal_m2784275415_ftn) (Il2CppObject *, Il2CppObject *, Il2CppObject *, Il2CppObject *, Il2CppObject *, Il2CppObject *, Il2CppObject *);
	static CanvasRenderer_SplitUIVertexStreamsInternal_m2784275415_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (CanvasRenderer_SplitUIVertexStreamsInternal_m2784275415_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.CanvasRenderer::SplitUIVertexStreamsInternal(System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object)");
	_il2cpp_icall_func(___verts0, ___positions1, ___colors2, ___uv0S3, ___uv1S4, ___normals5, ___tangents6);
}
// System.Void UnityEngine.CanvasRenderer::SplitIndiciesStreamsInternal(System.Object,System.Object)
extern "C"  void CanvasRenderer_SplitIndiciesStreamsInternal_m2583413468 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___verts0, Il2CppObject * ___indicies1, const MethodInfo* method)
{
	typedef void (*CanvasRenderer_SplitIndiciesStreamsInternal_m2583413468_ftn) (Il2CppObject *, Il2CppObject *);
	static CanvasRenderer_SplitIndiciesStreamsInternal_m2583413468_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (CanvasRenderer_SplitIndiciesStreamsInternal_m2583413468_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.CanvasRenderer::SplitIndiciesStreamsInternal(System.Object,System.Object)");
	_il2cpp_icall_func(___verts0, ___indicies1);
}
// System.Void UnityEngine.CanvasRenderer::CreateUIVertexStream(System.Collections.Generic.List`1<UnityEngine.UIVertex>,System.Collections.Generic.List`1<UnityEngine.Vector3>,System.Collections.Generic.List`1<UnityEngine.Color32>,System.Collections.Generic.List`1<UnityEngine.Vector2>,System.Collections.Generic.List`1<UnityEngine.Vector2>,System.Collections.Generic.List`1<UnityEngine.Vector3>,System.Collections.Generic.List`1<UnityEngine.Vector4>,System.Collections.Generic.List`1<System.Int32>)
extern "C"  void CanvasRenderer_CreateUIVertexStream_m1315334716 (Il2CppObject * __this /* static, unused */, List_1_t1234605051 * ___verts0, List_1_t899420910 * ___positions1, List_1_t4072576034 * ___colors2, List_1_t3628304265 * ___uv0S3, List_1_t3628304265 * ___uv1S4, List_1_t899420910 * ___normals5, List_1_t496136383 * ___tangents6, List_1_t128053199 * ___indicies7, const MethodInfo* method)
{
	{
		List_1_t1234605051 * L_0 = ___verts0;
		List_1_t899420910 * L_1 = ___positions1;
		List_1_t4072576034 * L_2 = ___colors2;
		List_1_t3628304265 * L_3 = ___uv0S3;
		List_1_t3628304265 * L_4 = ___uv1S4;
		List_1_t899420910 * L_5 = ___normals5;
		List_1_t496136383 * L_6 = ___tangents6;
		List_1_t128053199 * L_7 = ___indicies7;
		CanvasRenderer_CreateUIVertexStreamInternal_m338533840(NULL /*static, unused*/, L_0, L_1, L_2, L_3, L_4, L_5, L_6, L_7, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.CanvasRenderer::CreateUIVertexStreamInternal(System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object)
extern "C"  void CanvasRenderer_CreateUIVertexStreamInternal_m338533840 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___verts0, Il2CppObject * ___positions1, Il2CppObject * ___colors2, Il2CppObject * ___uv0S3, Il2CppObject * ___uv1S4, Il2CppObject * ___normals5, Il2CppObject * ___tangents6, Il2CppObject * ___indicies7, const MethodInfo* method)
{
	typedef void (*CanvasRenderer_CreateUIVertexStreamInternal_m338533840_ftn) (Il2CppObject *, Il2CppObject *, Il2CppObject *, Il2CppObject *, Il2CppObject *, Il2CppObject *, Il2CppObject *, Il2CppObject *);
	static CanvasRenderer_CreateUIVertexStreamInternal_m338533840_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (CanvasRenderer_CreateUIVertexStreamInternal_m338533840_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.CanvasRenderer::CreateUIVertexStreamInternal(System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object,System.Object)");
	_il2cpp_icall_func(___verts0, ___positions1, ___colors2, ___uv0S3, ___uv1S4, ___normals5, ___tangents6, ___indicies7);
}
// System.Void UnityEngine.CanvasRenderer::AddUIVertexStream(System.Collections.Generic.List`1<UnityEngine.UIVertex>,System.Collections.Generic.List`1<UnityEngine.Vector3>,System.Collections.Generic.List`1<UnityEngine.Color32>,System.Collections.Generic.List`1<UnityEngine.Vector2>,System.Collections.Generic.List`1<UnityEngine.Vector2>,System.Collections.Generic.List`1<UnityEngine.Vector3>,System.Collections.Generic.List`1<UnityEngine.Vector4>)
extern "C"  void CanvasRenderer_AddUIVertexStream_m681118477 (Il2CppObject * __this /* static, unused */, List_1_t1234605051 * ___verts0, List_1_t899420910 * ___positions1, List_1_t4072576034 * ___colors2, List_1_t3628304265 * ___uv0S3, List_1_t3628304265 * ___uv1S4, List_1_t899420910 * ___normals5, List_1_t496136383 * ___tangents6, const MethodInfo* method)
{
	{
		List_1_t1234605051 * L_0 = ___verts0;
		List_1_t899420910 * L_1 = ___positions1;
		List_1_t4072576034 * L_2 = ___colors2;
		List_1_t3628304265 * L_3 = ___uv0S3;
		List_1_t3628304265 * L_4 = ___uv1S4;
		List_1_t899420910 * L_5 = ___normals5;
		List_1_t496136383 * L_6 = ___tangents6;
		CanvasRenderer_SplitUIVertexStreamsInternal_m2784275415(NULL /*static, unused*/, L_0, L_1, L_2, L_3, L_4, L_5, L_6, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean UnityEngine.CanvasRenderer::get_cull()
extern "C"  bool CanvasRenderer_get_cull_m2277997101 (CanvasRenderer_t2598313366 * __this, const MethodInfo* method)
{
	typedef bool (*CanvasRenderer_get_cull_m2277997101_ftn) (CanvasRenderer_t2598313366 *);
	static CanvasRenderer_get_cull_m2277997101_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (CanvasRenderer_get_cull_m2277997101_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.CanvasRenderer::get_cull()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.CanvasRenderer::set_cull(System.Boolean)
extern "C"  void CanvasRenderer_set_cull_m2086579420 (CanvasRenderer_t2598313366 * __this, bool ___value0, const MethodInfo* method)
{
	typedef void (*CanvasRenderer_set_cull_m2086579420_ftn) (CanvasRenderer_t2598313366 *, bool);
	static CanvasRenderer_set_cull_m2086579420_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (CanvasRenderer_set_cull_m2086579420_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.CanvasRenderer::set_cull(System.Boolean)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Int32 UnityEngine.CanvasRenderer::get_absoluteDepth()
extern "C"  int32_t CanvasRenderer_get_absoluteDepth_m3121429127 (CanvasRenderer_t2598313366 * __this, const MethodInfo* method)
{
	typedef int32_t (*CanvasRenderer_get_absoluteDepth_m3121429127_ftn) (CanvasRenderer_t2598313366 *);
	static CanvasRenderer_get_absoluteDepth_m3121429127_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (CanvasRenderer_get_absoluteDepth_m3121429127_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.CanvasRenderer::get_absoluteDepth()");
	return _il2cpp_icall_func(__this);
}
// System.Boolean UnityEngine.CanvasRenderer::get_hasMoved()
extern "C"  bool CanvasRenderer_get_hasMoved_m1853367979 (CanvasRenderer_t2598313366 * __this, const MethodInfo* method)
{
	typedef bool (*CanvasRenderer_get_hasMoved_m1853367979_ftn) (CanvasRenderer_t2598313366 *);
	static CanvasRenderer_get_hasMoved_m1853367979_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (CanvasRenderer_get_hasMoved_m1853367979_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.CanvasRenderer::get_hasMoved()");
	return _il2cpp_icall_func(__this);
}
// Conversion methods for marshalling of: UnityEngine.CharacterInfo
extern "C" void CharacterInfo_t1228754872_marshal_pinvoke(const CharacterInfo_t1228754872& unmarshaled, CharacterInfo_t1228754872_marshaled_pinvoke& marshaled)
{
	marshaled.___index_0 = unmarshaled.get_index_0();
	marshaled.___uv_1 = unmarshaled.get_uv_1();
	marshaled.___vert_2 = unmarshaled.get_vert_2();
	marshaled.___width_3 = unmarshaled.get_width_3();
	marshaled.___size_4 = unmarshaled.get_size_4();
	marshaled.___style_5 = unmarshaled.get_style_5();
	marshaled.___flipped_6 = static_cast<int32_t>(unmarshaled.get_flipped_6());
}
extern "C" void CharacterInfo_t1228754872_marshal_pinvoke_back(const CharacterInfo_t1228754872_marshaled_pinvoke& marshaled, CharacterInfo_t1228754872& unmarshaled)
{
	int32_t unmarshaled_index_temp_0 = 0;
	unmarshaled_index_temp_0 = marshaled.___index_0;
	unmarshaled.set_index_0(unmarshaled_index_temp_0);
	Rect_t2360479859  unmarshaled_uv_temp_1;
	memset(&unmarshaled_uv_temp_1, 0, sizeof(unmarshaled_uv_temp_1));
	unmarshaled_uv_temp_1 = marshaled.___uv_1;
	unmarshaled.set_uv_1(unmarshaled_uv_temp_1);
	Rect_t2360479859  unmarshaled_vert_temp_2;
	memset(&unmarshaled_vert_temp_2, 0, sizeof(unmarshaled_vert_temp_2));
	unmarshaled_vert_temp_2 = marshaled.___vert_2;
	unmarshaled.set_vert_2(unmarshaled_vert_temp_2);
	float unmarshaled_width_temp_3 = 0.0f;
	unmarshaled_width_temp_3 = marshaled.___width_3;
	unmarshaled.set_width_3(unmarshaled_width_temp_3);
	int32_t unmarshaled_size_temp_4 = 0;
	unmarshaled_size_temp_4 = marshaled.___size_4;
	unmarshaled.set_size_4(unmarshaled_size_temp_4);
	int32_t unmarshaled_style_temp_5 = 0;
	unmarshaled_style_temp_5 = marshaled.___style_5;
	unmarshaled.set_style_5(unmarshaled_style_temp_5);
	bool unmarshaled_flipped_temp_6 = false;
	unmarshaled_flipped_temp_6 = static_cast<bool>(marshaled.___flipped_6);
	unmarshaled.set_flipped_6(unmarshaled_flipped_temp_6);
}
// Conversion method for clean up from marshalling of: UnityEngine.CharacterInfo
extern "C" void CharacterInfo_t1228754872_marshal_pinvoke_cleanup(CharacterInfo_t1228754872_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.CharacterInfo
extern "C" void CharacterInfo_t1228754872_marshal_com(const CharacterInfo_t1228754872& unmarshaled, CharacterInfo_t1228754872_marshaled_com& marshaled)
{
	marshaled.___index_0 = unmarshaled.get_index_0();
	marshaled.___uv_1 = unmarshaled.get_uv_1();
	marshaled.___vert_2 = unmarshaled.get_vert_2();
	marshaled.___width_3 = unmarshaled.get_width_3();
	marshaled.___size_4 = unmarshaled.get_size_4();
	marshaled.___style_5 = unmarshaled.get_style_5();
	marshaled.___flipped_6 = static_cast<int32_t>(unmarshaled.get_flipped_6());
}
extern "C" void CharacterInfo_t1228754872_marshal_com_back(const CharacterInfo_t1228754872_marshaled_com& marshaled, CharacterInfo_t1228754872& unmarshaled)
{
	int32_t unmarshaled_index_temp_0 = 0;
	unmarshaled_index_temp_0 = marshaled.___index_0;
	unmarshaled.set_index_0(unmarshaled_index_temp_0);
	Rect_t2360479859  unmarshaled_uv_temp_1;
	memset(&unmarshaled_uv_temp_1, 0, sizeof(unmarshaled_uv_temp_1));
	unmarshaled_uv_temp_1 = marshaled.___uv_1;
	unmarshaled.set_uv_1(unmarshaled_uv_temp_1);
	Rect_t2360479859  unmarshaled_vert_temp_2;
	memset(&unmarshaled_vert_temp_2, 0, sizeof(unmarshaled_vert_temp_2));
	unmarshaled_vert_temp_2 = marshaled.___vert_2;
	unmarshaled.set_vert_2(unmarshaled_vert_temp_2);
	float unmarshaled_width_temp_3 = 0.0f;
	unmarshaled_width_temp_3 = marshaled.___width_3;
	unmarshaled.set_width_3(unmarshaled_width_temp_3);
	int32_t unmarshaled_size_temp_4 = 0;
	unmarshaled_size_temp_4 = marshaled.___size_4;
	unmarshaled.set_size_4(unmarshaled_size_temp_4);
	int32_t unmarshaled_style_temp_5 = 0;
	unmarshaled_style_temp_5 = marshaled.___style_5;
	unmarshaled.set_style_5(unmarshaled_style_temp_5);
	bool unmarshaled_flipped_temp_6 = false;
	unmarshaled_flipped_temp_6 = static_cast<bool>(marshaled.___flipped_6);
	unmarshaled.set_flipped_6(unmarshaled_flipped_temp_6);
}
// Conversion method for clean up from marshalling of: UnityEngine.CharacterInfo
extern "C" void CharacterInfo_t1228754872_marshal_com_cleanup(CharacterInfo_t1228754872_marshaled_com& marshaled)
{
}
// System.Void UnityEngine.Collections.DeallocateOnJobCompletionAttribute::.ctor()
extern "C"  void DeallocateOnJobCompletionAttribute__ctor_m2063146001 (DeallocateOnJobCompletionAttribute_t3131681843 * __this, const MethodInfo* method)
{
	{
		Attribute__ctor_m1529526131(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Collections.NativeContainerAttribute::.ctor()
extern "C"  void NativeContainerAttribute__ctor_m2973434004 (NativeContainerAttribute_t2600515814 * __this, const MethodInfo* method)
{
	{
		Attribute__ctor_m1529526131(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Collections.NativeContainerSupportsAtomicWriteAttribute::.ctor()
extern "C"  void NativeContainerSupportsAtomicWriteAttribute__ctor_m635891873 (NativeContainerSupportsAtomicWriteAttribute_t3790689680 * __this, const MethodInfo* method)
{
	{
		Attribute__ctor_m1529526131(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Collections.NativeContainerSupportsMinMaxWriteRestrictionAttribute::.ctor()
extern "C"  void NativeContainerSupportsMinMaxWriteRestrictionAttribute__ctor_m1121644465 (NativeContainerSupportsMinMaxWriteRestrictionAttribute_t1586929818 * __this, const MethodInfo* method)
{
	{
		Attribute__ctor_m1529526131(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Collections.ReadOnlyAttribute::.ctor()
extern "C"  void ReadOnlyAttribute__ctor_m1808549735 (ReadOnlyAttribute_t2029203740 * __this, const MethodInfo* method)
{
	{
		Attribute__ctor_m1529526131(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Collections.ReadWriteAttribute::.ctor()
extern "C"  void ReadWriteAttribute__ctor_m253610106 (ReadWriteAttribute_t306517538 * __this, const MethodInfo* method)
{
	{
		Attribute__ctor_m1529526131(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Collections.WriteOnlyAttribute::.ctor()
extern "C"  void WriteOnlyAttribute__ctor_m4139616771 (WriteOnlyAttribute_t595109273 * __this, const MethodInfo* method)
{
	{
		Attribute__ctor_m1529526131(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Rigidbody UnityEngine.Collider::get_attachedRigidbody()
extern "C"  Rigidbody_t3916780224 * Collider_get_attachedRigidbody_m4203494256 (Collider_t1773347010 * __this, const MethodInfo* method)
{
	typedef Rigidbody_t3916780224 * (*Collider_get_attachedRigidbody_m4203494256_ftn) (Collider_t1773347010 *);
	static Collider_get_attachedRigidbody_m4203494256_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Collider_get_attachedRigidbody_m4203494256_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Collider::get_attachedRigidbody()");
	return _il2cpp_icall_func(__this);
}
// UnityEngine.Rigidbody2D UnityEngine.Collider2D::get_attachedRigidbody()
extern "C"  Rigidbody2D_t939494601 * Collider2D_get_attachedRigidbody_m2346847020 (Collider2D_t2806799626 * __this, const MethodInfo* method)
{
	typedef Rigidbody2D_t939494601 * (*Collider2D_get_attachedRigidbody_m2346847020_ftn) (Collider2D_t2806799626 *);
	static Collider2D_get_attachedRigidbody_m2346847020_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Collider2D_get_attachedRigidbody_m2346847020_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Collider2D::get_attachedRigidbody()");
	return _il2cpp_icall_func(__this);
}
// Conversion methods for marshalling of: UnityEngine.Collision
extern "C" void Collision_t4262080450_marshal_pinvoke(const Collision_t4262080450& unmarshaled, Collision_t4262080450_marshaled_pinvoke& marshaled)
{
	Il2CppCodeGenException* ___m_Rigidbody_2Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_Rigidbody' of type 'Collision': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_Rigidbody_2Exception);
}
extern "C" void Collision_t4262080450_marshal_pinvoke_back(const Collision_t4262080450_marshaled_pinvoke& marshaled, Collision_t4262080450& unmarshaled)
{
	Il2CppCodeGenException* ___m_Rigidbody_2Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_Rigidbody' of type 'Collision': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_Rigidbody_2Exception);
}
// Conversion method for clean up from marshalling of: UnityEngine.Collision
extern "C" void Collision_t4262080450_marshal_pinvoke_cleanup(Collision_t4262080450_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.Collision
extern "C" void Collision_t4262080450_marshal_com(const Collision_t4262080450& unmarshaled, Collision_t4262080450_marshaled_com& marshaled)
{
	Il2CppCodeGenException* ___m_Rigidbody_2Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_Rigidbody' of type 'Collision': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_Rigidbody_2Exception);
}
extern "C" void Collision_t4262080450_marshal_com_back(const Collision_t4262080450_marshaled_com& marshaled, Collision_t4262080450& unmarshaled)
{
	Il2CppCodeGenException* ___m_Rigidbody_2Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_Rigidbody' of type 'Collision': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_Rigidbody_2Exception);
}
// Conversion method for clean up from marshalling of: UnityEngine.Collision
extern "C" void Collision_t4262080450_marshal_com_cleanup(Collision_t4262080450_marshaled_com& marshaled)
{
}
// System.Void UnityEngine.Collision::.ctor()
extern "C"  void Collision__ctor_m4097559813 (Collision_t4262080450 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m297566312(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Vector3 UnityEngine.Collision::get_relativeVelocity()
extern "C"  Vector3_t3722313464  Collision_get_relativeVelocity_m1615412340 (Collision_t4262080450 * __this, const MethodInfo* method)
{
	Vector3_t3722313464  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Vector3_t3722313464  L_0 = __this->get_m_RelativeVelocity_1();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		Vector3_t3722313464  L_1 = V_0;
		return L_1;
	}
}
// UnityEngine.Rigidbody UnityEngine.Collision::get_rigidbody()
extern "C"  Rigidbody_t3916780224 * Collision_get_rigidbody_m452774238 (Collision_t4262080450 * __this, const MethodInfo* method)
{
	Rigidbody_t3916780224 * V_0 = NULL;
	{
		Rigidbody_t3916780224 * L_0 = __this->get_m_Rigidbody_2();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		Rigidbody_t3916780224 * L_1 = V_0;
		return L_1;
	}
}
// UnityEngine.Collider UnityEngine.Collision::get_collider()
extern "C"  Collider_t1773347010 * Collision_get_collider_m4225178406 (Collision_t4262080450 * __this, const MethodInfo* method)
{
	Collider_t1773347010 * V_0 = NULL;
	{
		Collider_t1773347010 * L_0 = __this->get_m_Collider_3();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		Collider_t1773347010 * L_1 = V_0;
		return L_1;
	}
}
// UnityEngine.Transform UnityEngine.Collision::get_transform()
extern "C"  Transform_t3600365921 * Collision_get_transform_m1665182495 (Collision_t4262080450 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Collision_get_transform_m1665182495_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Transform_t3600365921 * V_0 = NULL;
	Transform_t3600365921 * G_B3_0 = NULL;
	{
		Rigidbody_t3916780224 * L_0 = Collision_get_rigidbody_m452774238(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m1920811489(NULL /*static, unused*/, L_0, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0022;
		}
	}
	{
		Rigidbody_t3916780224 * L_2 = Collision_get_rigidbody_m452774238(__this, /*hidden argument*/NULL);
		NullCheck(L_2);
		Transform_t3600365921 * L_3 = Component_get_transform_m2921103810(L_2, /*hidden argument*/NULL);
		G_B3_0 = L_3;
		goto IL_002d;
	}

IL_0022:
	{
		Collider_t1773347010 * L_4 = Collision_get_collider_m4225178406(__this, /*hidden argument*/NULL);
		NullCheck(L_4);
		Transform_t3600365921 * L_5 = Component_get_transform_m2921103810(L_4, /*hidden argument*/NULL);
		G_B3_0 = L_5;
	}

IL_002d:
	{
		V_0 = G_B3_0;
		goto IL_0033;
	}

IL_0033:
	{
		Transform_t3600365921 * L_6 = V_0;
		return L_6;
	}
}
// UnityEngine.GameObject UnityEngine.Collision::get_gameObject()
extern "C"  GameObject_t1113636619 * Collision_get_gameObject_m1670699900 (Collision_t4262080450 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Collision_get_gameObject_m1670699900_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	GameObject_t1113636619 * V_0 = NULL;
	GameObject_t1113636619 * G_B3_0 = NULL;
	{
		Rigidbody_t3916780224 * L_0 = __this->get_m_Rigidbody_2();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m1920811489(NULL /*static, unused*/, L_0, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0022;
		}
	}
	{
		Rigidbody_t3916780224 * L_2 = __this->get_m_Rigidbody_2();
		NullCheck(L_2);
		GameObject_t1113636619 * L_3 = Component_get_gameObject_m2648350745(L_2, /*hidden argument*/NULL);
		G_B3_0 = L_3;
		goto IL_002d;
	}

IL_0022:
	{
		Collider_t1773347010 * L_4 = __this->get_m_Collider_3();
		NullCheck(L_4);
		GameObject_t1113636619 * L_5 = Component_get_gameObject_m2648350745(L_4, /*hidden argument*/NULL);
		G_B3_0 = L_5;
	}

IL_002d:
	{
		V_0 = G_B3_0;
		goto IL_0033;
	}

IL_0033:
	{
		GameObject_t1113636619 * L_6 = V_0;
		return L_6;
	}
}
// UnityEngine.ContactPoint[] UnityEngine.Collision::get_contacts()
extern "C"  ContactPointU5BU5D_t872956888* Collision_get_contacts_m3545214732 (Collision_t4262080450 * __this, const MethodInfo* method)
{
	ContactPointU5BU5D_t872956888* V_0 = NULL;
	{
		ContactPointU5BU5D_t872956888* L_0 = __this->get_m_Contacts_4();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		ContactPointU5BU5D_t872956888* L_1 = V_0;
		return L_1;
	}
}
// System.Collections.IEnumerator UnityEngine.Collision::GetEnumerator()
extern "C"  Il2CppObject * Collision_GetEnumerator_m1912447147 (Collision_t4262080450 * __this, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	{
		ContactPointU5BU5D_t872956888* L_0 = Collision_get_contacts_m3545214732(__this, /*hidden argument*/NULL);
		NullCheck((Il2CppArray *)(Il2CppArray *)L_0);
		Il2CppObject * L_1 = Array_GetEnumerator_m4277730612((Il2CppArray *)(Il2CppArray *)L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_0012;
	}

IL_0012:
	{
		Il2CppObject * L_2 = V_0;
		return L_2;
	}
}
// UnityEngine.Vector3 UnityEngine.Collision::get_impulse()
extern "C"  Vector3_t3722313464  Collision_get_impulse_m1433629766 (Collision_t4262080450 * __this, const MethodInfo* method)
{
	Vector3_t3722313464  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Vector3_t3722313464  L_0 = __this->get_m_Impulse_0();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		Vector3_t3722313464  L_1 = V_0;
		return L_1;
	}
}
// UnityEngine.Vector3 UnityEngine.Collision::get_impactForceSum()
extern "C"  Vector3_t3722313464  Collision_get_impactForceSum_m2704959762 (Collision_t4262080450 * __this, const MethodInfo* method)
{
	Vector3_t3722313464  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Vector3_t3722313464  L_0 = Collision_get_relativeVelocity_m1615412340(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		Vector3_t3722313464  L_1 = V_0;
		return L_1;
	}
}
// UnityEngine.Vector3 UnityEngine.Collision::get_frictionForceSum()
extern "C"  Vector3_t3722313464  Collision_get_frictionForceSum_m4001667003 (Collision_t4262080450 * __this, const MethodInfo* method)
{
	Vector3_t3722313464  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Vector3_t3722313464  L_0 = Vector3_get_zero_m1640475482(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_000c;
	}

IL_000c:
	{
		Vector3_t3722313464  L_1 = V_0;
		return L_1;
	}
}
// UnityEngine.Component UnityEngine.Collision::get_other()
extern "C"  Component_t1923634451 * Collision_get_other_m1003040551 (Collision_t4262080450 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Collision_get_other_m1003040551_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Component_t1923634451 * V_0 = NULL;
	Rigidbody_t3916780224 * G_B3_0 = NULL;
	{
		Rigidbody_t3916780224 * L_0 = __this->get_m_Rigidbody_2();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m1920811489(NULL /*static, unused*/, L_0, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001d;
		}
	}
	{
		Rigidbody_t3916780224 * L_2 = __this->get_m_Rigidbody_2();
		G_B3_0 = L_2;
		goto IL_0023;
	}

IL_001d:
	{
		Collider_t1773347010 * L_3 = __this->get_m_Collider_3();
		G_B3_0 = ((Rigidbody_t3916780224 *)(L_3));
	}

IL_0023:
	{
		V_0 = G_B3_0;
		goto IL_0029;
	}

IL_0029:
	{
		Component_t1923634451 * L_4 = V_0;
		return L_4;
	}
}
// Conversion methods for marshalling of: UnityEngine.Collision2D
extern "C" void Collision2D_t2842956331_marshal_pinvoke(const Collision2D_t2842956331& unmarshaled, Collision2D_t2842956331_marshaled_pinvoke& marshaled)
{
	Il2CppCodeGenException* ___m_Contacts_4Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_Contacts' of type 'Collision2D'.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_Contacts_4Exception);
}
extern "C" void Collision2D_t2842956331_marshal_pinvoke_back(const Collision2D_t2842956331_marshaled_pinvoke& marshaled, Collision2D_t2842956331& unmarshaled)
{
	Il2CppCodeGenException* ___m_Contacts_4Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_Contacts' of type 'Collision2D'.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_Contacts_4Exception);
}
// Conversion method for clean up from marshalling of: UnityEngine.Collision2D
extern "C" void Collision2D_t2842956331_marshal_pinvoke_cleanup(Collision2D_t2842956331_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.Collision2D
extern "C" void Collision2D_t2842956331_marshal_com(const Collision2D_t2842956331& unmarshaled, Collision2D_t2842956331_marshaled_com& marshaled)
{
	Il2CppCodeGenException* ___m_Contacts_4Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_Contacts' of type 'Collision2D'.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_Contacts_4Exception);
}
extern "C" void Collision2D_t2842956331_marshal_com_back(const Collision2D_t2842956331_marshaled_com& marshaled, Collision2D_t2842956331& unmarshaled)
{
	Il2CppCodeGenException* ___m_Contacts_4Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_Contacts' of type 'Collision2D'.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_Contacts_4Exception);
}
// Conversion method for clean up from marshalling of: UnityEngine.Collision2D
extern "C" void Collision2D_t2842956331_marshal_com_cleanup(Collision2D_t2842956331_marshaled_com& marshaled)
{
}
// System.Void UnityEngine.Collision2D::.ctor()
extern "C"  void Collision2D__ctor_m3402915587 (Collision2D_t2842956331 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m297566312(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Collider2D UnityEngine.Collision2D::get_collider()
extern "C"  Collider2D_t2806799626 * Collision2D_get_collider_m3539891938 (Collision2D_t2842956331 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Collision2D_get_collider_m3539891938_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Collider2D_t2806799626 * V_0 = NULL;
	{
		int32_t L_0 = __this->get_m_Collider_0();
		IL2CPP_RUNTIME_CLASS_INIT(Physics2D_t1528932956_il2cpp_TypeInfo_var);
		Collider2D_t2806799626 * L_1 = Physics2D_GetColliderFromInstanceID_m2016026012(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_0012;
	}

IL_0012:
	{
		Collider2D_t2806799626 * L_2 = V_0;
		return L_2;
	}
}
// UnityEngine.Collider2D UnityEngine.Collision2D::get_otherCollider()
extern "C"  Collider2D_t2806799626 * Collision2D_get_otherCollider_m3332082749 (Collision2D_t2842956331 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Collision2D_get_otherCollider_m3332082749_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Collider2D_t2806799626 * V_0 = NULL;
	{
		int32_t L_0 = __this->get_m_OtherCollider_1();
		IL2CPP_RUNTIME_CLASS_INIT(Physics2D_t1528932956_il2cpp_TypeInfo_var);
		Collider2D_t2806799626 * L_1 = Physics2D_GetColliderFromInstanceID_m2016026012(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_0012;
	}

IL_0012:
	{
		Collider2D_t2806799626 * L_2 = V_0;
		return L_2;
	}
}
// UnityEngine.Rigidbody2D UnityEngine.Collision2D::get_rigidbody()
extern "C"  Rigidbody2D_t939494601 * Collision2D_get_rigidbody_m3300210124 (Collision2D_t2842956331 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Collision2D_get_rigidbody_m3300210124_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Rigidbody2D_t939494601 * V_0 = NULL;
	{
		int32_t L_0 = __this->get_m_Rigidbody_2();
		IL2CPP_RUNTIME_CLASS_INIT(Physics2D_t1528932956_il2cpp_TypeInfo_var);
		Rigidbody2D_t939494601 * L_1 = Physics2D_GetRigidbodyFromInstanceID_m3174084804(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_0012;
	}

IL_0012:
	{
		Rigidbody2D_t939494601 * L_2 = V_0;
		return L_2;
	}
}
// UnityEngine.Rigidbody2D UnityEngine.Collision2D::get_otherRigidbody()
extern "C"  Rigidbody2D_t939494601 * Collision2D_get_otherRigidbody_m3881049350 (Collision2D_t2842956331 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Collision2D_get_otherRigidbody_m3881049350_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Rigidbody2D_t939494601 * V_0 = NULL;
	{
		int32_t L_0 = __this->get_m_OtherRigidbody_3();
		IL2CPP_RUNTIME_CLASS_INIT(Physics2D_t1528932956_il2cpp_TypeInfo_var);
		Rigidbody2D_t939494601 * L_1 = Physics2D_GetRigidbodyFromInstanceID_m3174084804(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_0012;
	}

IL_0012:
	{
		Rigidbody2D_t939494601 * L_2 = V_0;
		return L_2;
	}
}
// UnityEngine.Transform UnityEngine.Collision2D::get_transform()
extern "C"  Transform_t3600365921 * Collision2D_get_transform_m1797657422 (Collision2D_t2842956331 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Collision2D_get_transform_m1797657422_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Transform_t3600365921 * V_0 = NULL;
	Transform_t3600365921 * G_B3_0 = NULL;
	{
		Rigidbody2D_t939494601 * L_0 = Collision2D_get_rigidbody_m3300210124(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m1920811489(NULL /*static, unused*/, L_0, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0022;
		}
	}
	{
		Rigidbody2D_t939494601 * L_2 = Collision2D_get_rigidbody_m3300210124(__this, /*hidden argument*/NULL);
		NullCheck(L_2);
		Transform_t3600365921 * L_3 = Component_get_transform_m2921103810(L_2, /*hidden argument*/NULL);
		G_B3_0 = L_3;
		goto IL_002d;
	}

IL_0022:
	{
		Collider2D_t2806799626 * L_4 = Collision2D_get_collider_m3539891938(__this, /*hidden argument*/NULL);
		NullCheck(L_4);
		Transform_t3600365921 * L_5 = Component_get_transform_m2921103810(L_4, /*hidden argument*/NULL);
		G_B3_0 = L_5;
	}

IL_002d:
	{
		V_0 = G_B3_0;
		goto IL_0033;
	}

IL_0033:
	{
		Transform_t3600365921 * L_6 = V_0;
		return L_6;
	}
}
// UnityEngine.GameObject UnityEngine.Collision2D::get_gameObject()
extern "C"  GameObject_t1113636619 * Collision2D_get_gameObject_m73511728 (Collision2D_t2842956331 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Collision2D_get_gameObject_m73511728_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	GameObject_t1113636619 * V_0 = NULL;
	GameObject_t1113636619 * G_B3_0 = NULL;
	{
		Rigidbody2D_t939494601 * L_0 = Collision2D_get_rigidbody_m3300210124(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m1920811489(NULL /*static, unused*/, L_0, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0022;
		}
	}
	{
		Rigidbody2D_t939494601 * L_2 = Collision2D_get_rigidbody_m3300210124(__this, /*hidden argument*/NULL);
		NullCheck(L_2);
		GameObject_t1113636619 * L_3 = Component_get_gameObject_m2648350745(L_2, /*hidden argument*/NULL);
		G_B3_0 = L_3;
		goto IL_002d;
	}

IL_0022:
	{
		Collider2D_t2806799626 * L_4 = Collision2D_get_collider_m3539891938(__this, /*hidden argument*/NULL);
		NullCheck(L_4);
		GameObject_t1113636619 * L_5 = Component_get_gameObject_m2648350745(L_4, /*hidden argument*/NULL);
		G_B3_0 = L_5;
	}

IL_002d:
	{
		V_0 = G_B3_0;
		goto IL_0033;
	}

IL_0033:
	{
		GameObject_t1113636619 * L_6 = V_0;
		return L_6;
	}
}
// UnityEngine.ContactPoint2D[] UnityEngine.Collision2D::get_contacts()
extern "C"  ContactPoint2DU5BU5D_t96683501* Collision2D_get_contacts_m2794527467 (Collision2D_t2842956331 * __this, const MethodInfo* method)
{
	ContactPoint2DU5BU5D_t96683501* V_0 = NULL;
	{
		ContactPoint2DU5BU5D_t96683501* L_0 = __this->get_m_Contacts_4();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		ContactPoint2DU5BU5D_t96683501* L_1 = V_0;
		return L_1;
	}
}
// UnityEngine.Vector2 UnityEngine.Collision2D::get_relativeVelocity()
extern "C"  Vector2_t2156229523  Collision2D_get_relativeVelocity_m360685393 (Collision2D_t2842956331 * __this, const MethodInfo* method)
{
	Vector2_t2156229523  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Vector2_t2156229523  L_0 = __this->get_m_RelativeVelocity_5();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		Vector2_t2156229523  L_1 = V_0;
		return L_1;
	}
}
// System.Boolean UnityEngine.Collision2D::get_enabled()
extern "C"  bool Collision2D_get_enabled_m154834155 (Collision2D_t2842956331 * __this, const MethodInfo* method)
{
	bool V_0 = false;
	{
		int32_t L_0 = __this->get_m_Enabled_6();
		V_0 = (bool)((((int32_t)L_0) == ((int32_t)1))? 1 : 0);
		goto IL_0010;
	}

IL_0010:
	{
		bool L_1 = V_0;
		return L_1;
	}
}
// System.Void UnityEngine.Color::.ctor(System.Single,System.Single,System.Single,System.Single)
extern "C"  void Color__ctor_m3057542999 (Color_t2555686324 * __this, float ___r0, float ___g1, float ___b2, float ___a3, const MethodInfo* method)
{
	{
		float L_0 = ___r0;
		__this->set_r_0(L_0);
		float L_1 = ___g1;
		__this->set_g_1(L_1);
		float L_2 = ___b2;
		__this->set_b_2(L_2);
		float L_3 = ___a3;
		__this->set_a_3(L_3);
		return;
	}
}
extern "C"  void Color__ctor_m3057542999_AdjustorThunk (Il2CppObject * __this, float ___r0, float ___g1, float ___b2, float ___a3, const MethodInfo* method)
{
	Color_t2555686324 * _thisAdjusted = reinterpret_cast<Color_t2555686324 *>(__this + 1);
	Color__ctor_m3057542999(_thisAdjusted, ___r0, ___g1, ___b2, ___a3, method);
}
// System.Void UnityEngine.Color::.ctor(System.Single,System.Single,System.Single)
extern "C"  void Color__ctor_m1673432820 (Color_t2555686324 * __this, float ___r0, float ___g1, float ___b2, const MethodInfo* method)
{
	{
		float L_0 = ___r0;
		__this->set_r_0(L_0);
		float L_1 = ___g1;
		__this->set_g_1(L_1);
		float L_2 = ___b2;
		__this->set_b_2(L_2);
		__this->set_a_3((1.0f));
		return;
	}
}
extern "C"  void Color__ctor_m1673432820_AdjustorThunk (Il2CppObject * __this, float ___r0, float ___g1, float ___b2, const MethodInfo* method)
{
	Color_t2555686324 * _thisAdjusted = reinterpret_cast<Color_t2555686324 *>(__this + 1);
	Color__ctor_m1673432820(_thisAdjusted, ___r0, ___g1, ___b2, method);
}
// System.String UnityEngine.Color::ToString()
extern "C"  String_t* Color_ToString_m3373877040 (Color_t2555686324 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Color_ToString_m3373877040_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	{
		ObjectU5BU5D_t2843939325* L_0 = ((ObjectU5BU5D_t2843939325*)SZArrayNew(ObjectU5BU5D_t2843939325_il2cpp_TypeInfo_var, (uint32_t)4));
		float L_1 = __this->get_r_0();
		float L_2 = L_1;
		Il2CppObject * L_3 = Box(Single_t1397266774_il2cpp_TypeInfo_var, &L_2);
		NullCheck(L_0);
		ArrayElementTypeCheck (L_0, L_3);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_3);
		ObjectU5BU5D_t2843939325* L_4 = L_0;
		float L_5 = __this->get_g_1();
		float L_6 = L_5;
		Il2CppObject * L_7 = Box(Single_t1397266774_il2cpp_TypeInfo_var, &L_6);
		NullCheck(L_4);
		ArrayElementTypeCheck (L_4, L_7);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_7);
		ObjectU5BU5D_t2843939325* L_8 = L_4;
		float L_9 = __this->get_b_2();
		float L_10 = L_9;
		Il2CppObject * L_11 = Box(Single_t1397266774_il2cpp_TypeInfo_var, &L_10);
		NullCheck(L_8);
		ArrayElementTypeCheck (L_8, L_11);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)L_11);
		ObjectU5BU5D_t2843939325* L_12 = L_8;
		float L_13 = __this->get_a_3();
		float L_14 = L_13;
		Il2CppObject * L_15 = Box(Single_t1397266774_il2cpp_TypeInfo_var, &L_14);
		NullCheck(L_12);
		ArrayElementTypeCheck (L_12, L_15);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_15);
		String_t* L_16 = UnityString_Format_m3741272017(NULL /*static, unused*/, _stringLiteral1121701886, L_12, /*hidden argument*/NULL);
		V_0 = L_16;
		goto IL_004f;
	}

IL_004f:
	{
		String_t* L_17 = V_0;
		return L_17;
	}
}
extern "C"  String_t* Color_ToString_m3373877040_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Color_t2555686324 * _thisAdjusted = reinterpret_cast<Color_t2555686324 *>(__this + 1);
	return Color_ToString_m3373877040(_thisAdjusted, method);
}
// System.Int32 UnityEngine.Color::GetHashCode()
extern "C"  int32_t Color_GetHashCode_m3613701643 (Color_t2555686324 * __this, const MethodInfo* method)
{
	Vector4_t3319028937  V_0;
	memset(&V_0, 0, sizeof(V_0));
	int32_t V_1 = 0;
	{
		Vector4_t3319028937  L_0 = Color_op_Implicit_m1316618015(NULL /*static, unused*/, (*(Color_t2555686324 *)__this), /*hidden argument*/NULL);
		V_0 = L_0;
		int32_t L_1 = Vector4_GetHashCode_m2582833348((&V_0), /*hidden argument*/NULL);
		V_1 = L_1;
		goto IL_0020;
	}

IL_0020:
	{
		int32_t L_2 = V_1;
		return L_2;
	}
}
extern "C"  int32_t Color_GetHashCode_m3613701643_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Color_t2555686324 * _thisAdjusted = reinterpret_cast<Color_t2555686324 *>(__this + 1);
	return Color_GetHashCode_m3613701643(_thisAdjusted, method);
}
// System.Boolean UnityEngine.Color::Equals(System.Object)
extern "C"  bool Color_Equals_m325780748 (Color_t2555686324 * __this, Il2CppObject * ___other0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Color_Equals_m325780748_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	Color_t2555686324  V_1;
	memset(&V_1, 0, sizeof(V_1));
	int32_t G_B7_0 = 0;
	{
		Il2CppObject * L_0 = ___other0;
		if (((Il2CppObject *)IsInstSealed(L_0, Color_t2555686324_il2cpp_TypeInfo_var)))
		{
			goto IL_0013;
		}
	}
	{
		V_0 = (bool)0;
		goto IL_007a;
	}

IL_0013:
	{
		Il2CppObject * L_1 = ___other0;
		V_1 = ((*(Color_t2555686324 *)((Color_t2555686324 *)UnBox(L_1, Color_t2555686324_il2cpp_TypeInfo_var))));
		float* L_2 = __this->get_address_of_r_0();
		float L_3 = (&V_1)->get_r_0();
		bool L_4 = Single_Equals_m1601893879(L_2, L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0073;
		}
	}
	{
		float* L_5 = __this->get_address_of_g_1();
		float L_6 = (&V_1)->get_g_1();
		bool L_7 = Single_Equals_m1601893879(L_5, L_6, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0073;
		}
	}
	{
		float* L_8 = __this->get_address_of_b_2();
		float L_9 = (&V_1)->get_b_2();
		bool L_10 = Single_Equals_m1601893879(L_8, L_9, /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_0073;
		}
	}
	{
		float* L_11 = __this->get_address_of_a_3();
		float L_12 = (&V_1)->get_a_3();
		bool L_13 = Single_Equals_m1601893879(L_11, L_12, /*hidden argument*/NULL);
		G_B7_0 = ((int32_t)(L_13));
		goto IL_0074;
	}

IL_0073:
	{
		G_B7_0 = 0;
	}

IL_0074:
	{
		V_0 = (bool)G_B7_0;
		goto IL_007a;
	}

IL_007a:
	{
		bool L_14 = V_0;
		return L_14;
	}
}
extern "C"  bool Color_Equals_m325780748_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___other0, const MethodInfo* method)
{
	Color_t2555686324 * _thisAdjusted = reinterpret_cast<Color_t2555686324 *>(__this + 1);
	return Color_Equals_m325780748(_thisAdjusted, ___other0, method);
}
// UnityEngine.Color UnityEngine.Color::op_Multiply(UnityEngine.Color,UnityEngine.Color)
extern "C"  Color_t2555686324  Color_op_Multiply_m611273242 (Il2CppObject * __this /* static, unused */, Color_t2555686324  ___a0, Color_t2555686324  ___b1, const MethodInfo* method)
{
	Color_t2555686324  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		float L_0 = (&___a0)->get_r_0();
		float L_1 = (&___b1)->get_r_0();
		float L_2 = (&___a0)->get_g_1();
		float L_3 = (&___b1)->get_g_1();
		float L_4 = (&___a0)->get_b_2();
		float L_5 = (&___b1)->get_b_2();
		float L_6 = (&___a0)->get_a_3();
		float L_7 = (&___b1)->get_a_3();
		Color_t2555686324  L_8;
		memset(&L_8, 0, sizeof(L_8));
		Color__ctor_m3057542999(&L_8, ((float)((float)L_0*(float)L_1)), ((float)((float)L_2*(float)L_3)), ((float)((float)L_4*(float)L_5)), ((float)((float)L_6*(float)L_7)), /*hidden argument*/NULL);
		V_0 = L_8;
		goto IL_0048;
	}

IL_0048:
	{
		Color_t2555686324  L_9 = V_0;
		return L_9;
	}
}
// UnityEngine.Color UnityEngine.Color::op_Multiply(UnityEngine.Color,System.Single)
extern "C"  Color_t2555686324  Color_op_Multiply_m69884213 (Il2CppObject * __this /* static, unused */, Color_t2555686324  ___a0, float ___b1, const MethodInfo* method)
{
	Color_t2555686324  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		float L_0 = (&___a0)->get_r_0();
		float L_1 = ___b1;
		float L_2 = (&___a0)->get_g_1();
		float L_3 = ___b1;
		float L_4 = (&___a0)->get_b_2();
		float L_5 = ___b1;
		float L_6 = (&___a0)->get_a_3();
		float L_7 = ___b1;
		Color_t2555686324  L_8;
		memset(&L_8, 0, sizeof(L_8));
		Color__ctor_m3057542999(&L_8, ((float)((float)L_0*(float)L_1)), ((float)((float)L_2*(float)L_3)), ((float)((float)L_4*(float)L_5)), ((float)((float)L_6*(float)L_7)), /*hidden argument*/NULL);
		V_0 = L_8;
		goto IL_0030;
	}

IL_0030:
	{
		Color_t2555686324  L_9 = V_0;
		return L_9;
	}
}
// System.Boolean UnityEngine.Color::op_Equality(UnityEngine.Color,UnityEngine.Color)
extern "C"  bool Color_op_Equality_m3666991687 (Il2CppObject * __this /* static, unused */, Color_t2555686324  ___lhs0, Color_t2555686324  ___rhs1, const MethodInfo* method)
{
	bool V_0 = false;
	{
		Color_t2555686324  L_0 = ___lhs0;
		Vector4_t3319028937  L_1 = Color_op_Implicit_m1316618015(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		Color_t2555686324  L_2 = ___rhs1;
		Vector4_t3319028937  L_3 = Color_op_Implicit_m1316618015(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		bool L_4 = Vector4_op_Equality_m2731225992(NULL /*static, unused*/, L_1, L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		goto IL_0018;
	}

IL_0018:
	{
		bool L_5 = V_0;
		return L_5;
	}
}
// UnityEngine.Color UnityEngine.Color::Lerp(UnityEngine.Color,UnityEngine.Color,System.Single)
extern "C"  Color_t2555686324  Color_Lerp_m3788050327 (Il2CppObject * __this /* static, unused */, Color_t2555686324  ___a0, Color_t2555686324  ___b1, float ___t2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Color_Lerp_m3788050327_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Color_t2555686324  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		float L_0 = ___t2;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t3464937446_il2cpp_TypeInfo_var);
		float L_1 = Mathf_Clamp01_m4133291925(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		___t2 = L_1;
		float L_2 = (&___a0)->get_r_0();
		float L_3 = (&___b1)->get_r_0();
		float L_4 = (&___a0)->get_r_0();
		float L_5 = ___t2;
		float L_6 = (&___a0)->get_g_1();
		float L_7 = (&___b1)->get_g_1();
		float L_8 = (&___a0)->get_g_1();
		float L_9 = ___t2;
		float L_10 = (&___a0)->get_b_2();
		float L_11 = (&___b1)->get_b_2();
		float L_12 = (&___a0)->get_b_2();
		float L_13 = ___t2;
		float L_14 = (&___a0)->get_a_3();
		float L_15 = (&___b1)->get_a_3();
		float L_16 = (&___a0)->get_a_3();
		float L_17 = ___t2;
		Color_t2555686324  L_18;
		memset(&L_18, 0, sizeof(L_18));
		Color__ctor_m3057542999(&L_18, ((float)((float)L_2+(float)((float)((float)((float)((float)L_3-(float)L_4))*(float)L_5)))), ((float)((float)L_6+(float)((float)((float)((float)((float)L_7-(float)L_8))*(float)L_9)))), ((float)((float)L_10+(float)((float)((float)((float)((float)L_11-(float)L_12))*(float)L_13)))), ((float)((float)L_14+(float)((float)((float)((float)((float)L_15-(float)L_16))*(float)L_17)))), /*hidden argument*/NULL);
		V_0 = L_18;
		goto IL_0078;
	}

IL_0078:
	{
		Color_t2555686324  L_19 = V_0;
		return L_19;
	}
}
// UnityEngine.Color UnityEngine.Color::get_red()
extern "C"  Color_t2555686324  Color_get_red_m1573871861 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	Color_t2555686324  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Color_t2555686324  L_0;
		memset(&L_0, 0, sizeof(L_0));
		Color__ctor_m3057542999(&L_0, (1.0f), (0.0f), (0.0f), (1.0f), /*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_0020;
	}

IL_0020:
	{
		Color_t2555686324  L_1 = V_0;
		return L_1;
	}
}
// UnityEngine.Color UnityEngine.Color::get_green()
extern "C"  Color_t2555686324  Color_get_green_m3890106057 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	Color_t2555686324  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Color_t2555686324  L_0;
		memset(&L_0, 0, sizeof(L_0));
		Color__ctor_m3057542999(&L_0, (0.0f), (1.0f), (0.0f), (1.0f), /*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_0020;
	}

IL_0020:
	{
		Color_t2555686324  L_1 = V_0;
		return L_1;
	}
}
// UnityEngine.Color UnityEngine.Color::get_blue()
extern "C"  Color_t2555686324  Color_get_blue_m3245858892 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	Color_t2555686324  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Color_t2555686324  L_0;
		memset(&L_0, 0, sizeof(L_0));
		Color__ctor_m3057542999(&L_0, (0.0f), (0.0f), (1.0f), (1.0f), /*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_0020;
	}

IL_0020:
	{
		Color_t2555686324  L_1 = V_0;
		return L_1;
	}
}
// UnityEngine.Color UnityEngine.Color::get_white()
extern "C"  Color_t2555686324  Color_get_white_m3544547002 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	Color_t2555686324  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Color_t2555686324  L_0;
		memset(&L_0, 0, sizeof(L_0));
		Color__ctor_m3057542999(&L_0, (1.0f), (1.0f), (1.0f), (1.0f), /*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_0020;
	}

IL_0020:
	{
		Color_t2555686324  L_1 = V_0;
		return L_1;
	}
}
// UnityEngine.Color UnityEngine.Color::get_black()
extern "C"  Color_t2555686324  Color_get_black_m650597609 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	Color_t2555686324  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Color_t2555686324  L_0;
		memset(&L_0, 0, sizeof(L_0));
		Color__ctor_m3057542999(&L_0, (0.0f), (0.0f), (0.0f), (1.0f), /*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_0020;
	}

IL_0020:
	{
		Color_t2555686324  L_1 = V_0;
		return L_1;
	}
}
// UnityEngine.Color UnityEngine.Color::get_yellow()
extern "C"  Color_t2555686324  Color_get_yellow_m3352053420 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	Color_t2555686324  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Color_t2555686324  L_0;
		memset(&L_0, 0, sizeof(L_0));
		Color__ctor_m3057542999(&L_0, (1.0f), (0.921568632f), (0.0156862754f), (1.0f), /*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_0020;
	}

IL_0020:
	{
		Color_t2555686324  L_1 = V_0;
		return L_1;
	}
}
// UnityEngine.Color UnityEngine.Color::get_cyan()
extern "C"  Color_t2555686324  Color_get_cyan_m2549433552 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	Color_t2555686324  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Color_t2555686324  L_0;
		memset(&L_0, 0, sizeof(L_0));
		Color__ctor_m3057542999(&L_0, (0.0f), (1.0f), (1.0f), (1.0f), /*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_0020;
	}

IL_0020:
	{
		Color_t2555686324  L_1 = V_0;
		return L_1;
	}
}
// UnityEngine.Color UnityEngine.Color::get_magenta()
extern "C"  Color_t2555686324  Color_get_magenta_m3783166653 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	Color_t2555686324  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Color_t2555686324  L_0;
		memset(&L_0, 0, sizeof(L_0));
		Color__ctor_m3057542999(&L_0, (1.0f), (0.0f), (1.0f), (1.0f), /*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_0020;
	}

IL_0020:
	{
		Color_t2555686324  L_1 = V_0;
		return L_1;
	}
}
// UnityEngine.Color UnityEngine.Color::get_gray()
extern "C"  Color_t2555686324  Color_get_gray_m2675038778 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	Color_t2555686324  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Color_t2555686324  L_0;
		memset(&L_0, 0, sizeof(L_0));
		Color__ctor_m3057542999(&L_0, (0.5f), (0.5f), (0.5f), (1.0f), /*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_0020;
	}

IL_0020:
	{
		Color_t2555686324  L_1 = V_0;
		return L_1;
	}
}
// UnityEngine.Color UnityEngine.Color::get_grey()
extern "C"  Color_t2555686324  Color_get_grey_m3210598970 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	Color_t2555686324  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Color_t2555686324  L_0;
		memset(&L_0, 0, sizeof(L_0));
		Color__ctor_m3057542999(&L_0, (0.5f), (0.5f), (0.5f), (1.0f), /*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_0020;
	}

IL_0020:
	{
		Color_t2555686324  L_1 = V_0;
		return L_1;
	}
}
// UnityEngine.Color UnityEngine.Color::get_clear()
extern "C"  Color_t2555686324  Color_get_clear_m1773884651 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	Color_t2555686324  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Color_t2555686324  L_0;
		memset(&L_0, 0, sizeof(L_0));
		Color__ctor_m3057542999(&L_0, (0.0f), (0.0f), (0.0f), (0.0f), /*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_0020;
	}

IL_0020:
	{
		Color_t2555686324  L_1 = V_0;
		return L_1;
	}
}
// UnityEngine.Vector4 UnityEngine.Color::op_Implicit(UnityEngine.Color)
extern "C"  Vector4_t3319028937  Color_op_Implicit_m1316618015 (Il2CppObject * __this /* static, unused */, Color_t2555686324  ___c0, const MethodInfo* method)
{
	Vector4_t3319028937  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		float L_0 = (&___c0)->get_r_0();
		float L_1 = (&___c0)->get_g_1();
		float L_2 = (&___c0)->get_b_2();
		float L_3 = (&___c0)->get_a_3();
		Vector4_t3319028937  L_4;
		memset(&L_4, 0, sizeof(L_4));
		Vector4__ctor_m1702261405(&L_4, L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		goto IL_0028;
	}

IL_0028:
	{
		Vector4_t3319028937  L_5 = V_0;
		return L_5;
	}
}
// System.Void UnityEngine.Color32::.ctor(System.Byte,System.Byte,System.Byte,System.Byte)
extern "C"  void Color32__ctor_m655413850 (Color32_t2600501292 * __this, uint8_t ___r0, uint8_t ___g1, uint8_t ___b2, uint8_t ___a3, const MethodInfo* method)
{
	{
		uint8_t L_0 = ___r0;
		__this->set_r_0(L_0);
		uint8_t L_1 = ___g1;
		__this->set_g_1(L_1);
		uint8_t L_2 = ___b2;
		__this->set_b_2(L_2);
		uint8_t L_3 = ___a3;
		__this->set_a_3(L_3);
		return;
	}
}
extern "C"  void Color32__ctor_m655413850_AdjustorThunk (Il2CppObject * __this, uint8_t ___r0, uint8_t ___g1, uint8_t ___b2, uint8_t ___a3, const MethodInfo* method)
{
	Color32_t2600501292 * _thisAdjusted = reinterpret_cast<Color32_t2600501292 *>(__this + 1);
	Color32__ctor_m655413850(_thisAdjusted, ___r0, ___g1, ___b2, ___a3, method);
}
// UnityEngine.Color32 UnityEngine.Color32::op_Implicit(UnityEngine.Color)
extern "C"  Color32_t2600501292  Color32_op_Implicit_m2622936441 (Il2CppObject * __this /* static, unused */, Color_t2555686324  ___c0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Color32_op_Implicit_m2622936441_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Color32_t2600501292  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		float L_0 = (&___c0)->get_r_0();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t3464937446_il2cpp_TypeInfo_var);
		float L_1 = Mathf_Clamp01_m4133291925(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		float L_2 = (&___c0)->get_g_1();
		float L_3 = Mathf_Clamp01_m4133291925(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		float L_4 = (&___c0)->get_b_2();
		float L_5 = Mathf_Clamp01_m4133291925(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		float L_6 = (&___c0)->get_a_3();
		float L_7 = Mathf_Clamp01_m4133291925(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		Color32_t2600501292  L_8;
		memset(&L_8, 0, sizeof(L_8));
		Color32__ctor_m655413850(&L_8, (uint8_t)(((int32_t)((uint8_t)((float)((float)L_1*(float)(255.0f)))))), (uint8_t)(((int32_t)((uint8_t)((float)((float)L_3*(float)(255.0f)))))), (uint8_t)(((int32_t)((uint8_t)((float)((float)L_5*(float)(255.0f)))))), (uint8_t)(((int32_t)((uint8_t)((float)((float)L_7*(float)(255.0f)))))), /*hidden argument*/NULL);
		V_0 = L_8;
		goto IL_0058;
	}

IL_0058:
	{
		Color32_t2600501292  L_9 = V_0;
		return L_9;
	}
}
// UnityEngine.Color UnityEngine.Color32::op_Implicit(UnityEngine.Color32)
extern "C"  Color_t2555686324  Color32_op_Implicit_m3049222236 (Il2CppObject * __this /* static, unused */, Color32_t2600501292  ___c0, const MethodInfo* method)
{
	Color_t2555686324  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		uint8_t L_0 = (&___c0)->get_r_0();
		uint8_t L_1 = (&___c0)->get_g_1();
		uint8_t L_2 = (&___c0)->get_b_2();
		uint8_t L_3 = (&___c0)->get_a_3();
		Color_t2555686324  L_4;
		memset(&L_4, 0, sizeof(L_4));
		Color__ctor_m3057542999(&L_4, ((float)((float)(((float)((float)L_0)))/(float)(255.0f))), ((float)((float)(((float)((float)L_1)))/(float)(255.0f))), ((float)((float)(((float)((float)L_2)))/(float)(255.0f))), ((float)((float)(((float)((float)L_3)))/(float)(255.0f))), /*hidden argument*/NULL);
		V_0 = L_4;
		goto IL_0044;
	}

IL_0044:
	{
		Color_t2555686324  L_5 = V_0;
		return L_5;
	}
}
// System.String UnityEngine.Color32::ToString()
extern "C"  String_t* Color32_ToString_m908866355 (Color32_t2600501292 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Color32_ToString_m908866355_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	{
		ObjectU5BU5D_t2843939325* L_0 = ((ObjectU5BU5D_t2843939325*)SZArrayNew(ObjectU5BU5D_t2843939325_il2cpp_TypeInfo_var, (uint32_t)4));
		uint8_t L_1 = __this->get_r_0();
		uint8_t L_2 = L_1;
		Il2CppObject * L_3 = Box(Byte_t1134296376_il2cpp_TypeInfo_var, &L_2);
		NullCheck(L_0);
		ArrayElementTypeCheck (L_0, L_3);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_3);
		ObjectU5BU5D_t2843939325* L_4 = L_0;
		uint8_t L_5 = __this->get_g_1();
		uint8_t L_6 = L_5;
		Il2CppObject * L_7 = Box(Byte_t1134296376_il2cpp_TypeInfo_var, &L_6);
		NullCheck(L_4);
		ArrayElementTypeCheck (L_4, L_7);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_7);
		ObjectU5BU5D_t2843939325* L_8 = L_4;
		uint8_t L_9 = __this->get_b_2();
		uint8_t L_10 = L_9;
		Il2CppObject * L_11 = Box(Byte_t1134296376_il2cpp_TypeInfo_var, &L_10);
		NullCheck(L_8);
		ArrayElementTypeCheck (L_8, L_11);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)L_11);
		ObjectU5BU5D_t2843939325* L_12 = L_8;
		uint8_t L_13 = __this->get_a_3();
		uint8_t L_14 = L_13;
		Il2CppObject * L_15 = Box(Byte_t1134296376_il2cpp_TypeInfo_var, &L_14);
		NullCheck(L_12);
		ArrayElementTypeCheck (L_12, L_15);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_15);
		String_t* L_16 = UnityString_Format_m3741272017(NULL /*static, unused*/, _stringLiteral3218509134, L_12, /*hidden argument*/NULL);
		V_0 = L_16;
		goto IL_004f;
	}

IL_004f:
	{
		String_t* L_17 = V_0;
		return L_17;
	}
}
extern "C"  String_t* Color32_ToString_m908866355_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Color32_t2600501292 * _thisAdjusted = reinterpret_cast<Color32_t2600501292 *>(__this + 1);
	return Color32_ToString_m908866355(_thisAdjusted, method);
}
// System.Void UnityEngine.Component::.ctor()
extern "C"  void Component__ctor_m3533368306 (Component_t1923634451 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Component__ctor_m3533368306_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		Object__ctor_m1560822313(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Transform UnityEngine.Component::get_transform()
extern "C"  Transform_t3600365921 * Component_get_transform_m2921103810 (Component_t1923634451 * __this, const MethodInfo* method)
{
	typedef Transform_t3600365921 * (*Component_get_transform_m2921103810_ftn) (Component_t1923634451 *);
	static Component_get_transform_m2921103810_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Component_get_transform_m2921103810_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Component::get_transform()");
	return _il2cpp_icall_func(__this);
}
// UnityEngine.GameObject UnityEngine.Component::get_gameObject()
extern "C"  GameObject_t1113636619 * Component_get_gameObject_m2648350745 (Component_t1923634451 * __this, const MethodInfo* method)
{
	typedef GameObject_t1113636619 * (*Component_get_gameObject_m2648350745_ftn) (Component_t1923634451 *);
	static Component_get_gameObject_m2648350745_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Component_get_gameObject_m2648350745_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Component::get_gameObject()");
	return _il2cpp_icall_func(__this);
}
// UnityEngine.Component UnityEngine.Component::GetComponent(System.Type)
extern "C"  Component_t1923634451 * Component_GetComponent_m2813676312 (Component_t1923634451 * __this, Type_t * ___type0, const MethodInfo* method)
{
	Component_t1923634451 * V_0 = NULL;
	{
		GameObject_t1113636619 * L_0 = Component_get_gameObject_m2648350745(__this, /*hidden argument*/NULL);
		Type_t * L_1 = ___type0;
		NullCheck(L_0);
		Component_t1923634451 * L_2 = GameObject_GetComponent_m1928525411(L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		goto IL_0013;
	}

IL_0013:
	{
		Component_t1923634451 * L_3 = V_0;
		return L_3;
	}
}
// System.Void UnityEngine.Component::GetComponentFastPath(System.Type,System.IntPtr)
extern "C"  void Component_GetComponentFastPath_m476901382 (Component_t1923634451 * __this, Type_t * ___type0, IntPtr_t ___oneFurtherThanResultValue1, const MethodInfo* method)
{
	typedef void (*Component_GetComponentFastPath_m476901382_ftn) (Component_t1923634451 *, Type_t *, IntPtr_t);
	static Component_GetComponentFastPath_m476901382_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Component_GetComponentFastPath_m476901382_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Component::GetComponentFastPath(System.Type,System.IntPtr)");
	_il2cpp_icall_func(__this, ___type0, ___oneFurtherThanResultValue1);
}
// UnityEngine.Component UnityEngine.Component::GetComponent(System.String)
extern "C"  Component_t1923634451 * Component_GetComponent_m1143395748 (Component_t1923634451 * __this, String_t* ___type0, const MethodInfo* method)
{
	typedef Component_t1923634451 * (*Component_GetComponent_m1143395748_ftn) (Component_t1923634451 *, String_t*);
	static Component_GetComponent_m1143395748_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Component_GetComponent_m1143395748_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Component::GetComponent(System.String)");
	return _il2cpp_icall_func(__this, ___type0);
}
// UnityEngine.Component UnityEngine.Component::GetComponentInChildren(System.Type,System.Boolean)
extern "C"  Component_t1923634451 * Component_GetComponentInChildren_m2139176229 (Component_t1923634451 * __this, Type_t * ___t0, bool ___includeInactive1, const MethodInfo* method)
{
	Component_t1923634451 * V_0 = NULL;
	{
		GameObject_t1113636619 * L_0 = Component_get_gameObject_m2648350745(__this, /*hidden argument*/NULL);
		Type_t * L_1 = ___t0;
		bool L_2 = ___includeInactive1;
		NullCheck(L_0);
		Component_t1923634451 * L_3 = GameObject_GetComponentInChildren_m230563287(L_0, L_1, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		goto IL_0014;
	}

IL_0014:
	{
		Component_t1923634451 * L_4 = V_0;
		return L_4;
	}
}
// UnityEngine.Component UnityEngine.Component::GetComponentInChildren(System.Type)
extern "C"  Component_t1923634451 * Component_GetComponentInChildren_m23539466 (Component_t1923634451 * __this, Type_t * ___t0, const MethodInfo* method)
{
	Component_t1923634451 * V_0 = NULL;
	{
		Type_t * L_0 = ___t0;
		Component_t1923634451 * L_1 = Component_GetComponentInChildren_m2139176229(__this, L_0, (bool)0, /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_000f;
	}

IL_000f:
	{
		Component_t1923634451 * L_2 = V_0;
		return L_2;
	}
}
// UnityEngine.Component[] UnityEngine.Component::GetComponentsInChildren(System.Type)
extern "C"  ComponentU5BU5D_t3294940482* Component_GetComponentsInChildren_m2110248505 (Component_t1923634451 * __this, Type_t * ___t0, const MethodInfo* method)
{
	bool V_0 = false;
	ComponentU5BU5D_t3294940482* V_1 = NULL;
	{
		V_0 = (bool)0;
		Type_t * L_0 = ___t0;
		bool L_1 = V_0;
		ComponentU5BU5D_t3294940482* L_2 = Component_GetComponentsInChildren_m1870572916(__this, L_0, L_1, /*hidden argument*/NULL);
		V_1 = L_2;
		goto IL_0011;
	}

IL_0011:
	{
		ComponentU5BU5D_t3294940482* L_3 = V_1;
		return L_3;
	}
}
// UnityEngine.Component[] UnityEngine.Component::GetComponentsInChildren(System.Type,System.Boolean)
extern "C"  ComponentU5BU5D_t3294940482* Component_GetComponentsInChildren_m1870572916 (Component_t1923634451 * __this, Type_t * ___t0, bool ___includeInactive1, const MethodInfo* method)
{
	ComponentU5BU5D_t3294940482* V_0 = NULL;
	{
		GameObject_t1113636619 * L_0 = Component_get_gameObject_m2648350745(__this, /*hidden argument*/NULL);
		Type_t * L_1 = ___t0;
		bool L_2 = ___includeInactive1;
		NullCheck(L_0);
		ComponentU5BU5D_t3294940482* L_3 = GameObject_GetComponentsInChildren_m479703998(L_0, L_1, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		goto IL_0014;
	}

IL_0014:
	{
		ComponentU5BU5D_t3294940482* L_4 = V_0;
		return L_4;
	}
}
// UnityEngine.Component UnityEngine.Component::GetComponentInParent(System.Type)
extern "C"  Component_t1923634451 * Component_GetComponentInParent_m2023105846 (Component_t1923634451 * __this, Type_t * ___t0, const MethodInfo* method)
{
	Component_t1923634451 * V_0 = NULL;
	{
		GameObject_t1113636619 * L_0 = Component_get_gameObject_m2648350745(__this, /*hidden argument*/NULL);
		Type_t * L_1 = ___t0;
		NullCheck(L_0);
		Component_t1923634451 * L_2 = GameObject_GetComponentInParent_m2794983654(L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		goto IL_0013;
	}

IL_0013:
	{
		Component_t1923634451 * L_3 = V_0;
		return L_3;
	}
}
// UnityEngine.Component[] UnityEngine.Component::GetComponentsInParent(System.Type)
extern "C"  ComponentU5BU5D_t3294940482* Component_GetComponentsInParent_m2020987483 (Component_t1923634451 * __this, Type_t * ___t0, const MethodInfo* method)
{
	bool V_0 = false;
	ComponentU5BU5D_t3294940482* V_1 = NULL;
	{
		V_0 = (bool)0;
		Type_t * L_0 = ___t0;
		bool L_1 = V_0;
		ComponentU5BU5D_t3294940482* L_2 = Component_GetComponentsInParent_m2629981626(__this, L_0, L_1, /*hidden argument*/NULL);
		V_1 = L_2;
		goto IL_0011;
	}

IL_0011:
	{
		ComponentU5BU5D_t3294940482* L_3 = V_1;
		return L_3;
	}
}
// UnityEngine.Component[] UnityEngine.Component::GetComponentsInParent(System.Type,System.Boolean)
extern "C"  ComponentU5BU5D_t3294940482* Component_GetComponentsInParent_m2629981626 (Component_t1923634451 * __this, Type_t * ___t0, bool ___includeInactive1, const MethodInfo* method)
{
	ComponentU5BU5D_t3294940482* V_0 = NULL;
	{
		GameObject_t1113636619 * L_0 = Component_get_gameObject_m2648350745(__this, /*hidden argument*/NULL);
		Type_t * L_1 = ___t0;
		bool L_2 = ___includeInactive1;
		NullCheck(L_0);
		ComponentU5BU5D_t3294940482* L_3 = GameObject_GetComponentsInParent_m4080244100(L_0, L_1, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		goto IL_0014;
	}

IL_0014:
	{
		ComponentU5BU5D_t3294940482* L_4 = V_0;
		return L_4;
	}
}
// UnityEngine.Component[] UnityEngine.Component::GetComponents(System.Type)
extern "C"  ComponentU5BU5D_t3294940482* Component_GetComponents_m3453415141 (Component_t1923634451 * __this, Type_t * ___type0, const MethodInfo* method)
{
	ComponentU5BU5D_t3294940482* V_0 = NULL;
	{
		GameObject_t1113636619 * L_0 = Component_get_gameObject_m2648350745(__this, /*hidden argument*/NULL);
		Type_t * L_1 = ___type0;
		NullCheck(L_0);
		ComponentU5BU5D_t3294940482* L_2 = GameObject_GetComponents_m3948232783(L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		goto IL_0013;
	}

IL_0013:
	{
		ComponentU5BU5D_t3294940482* L_3 = V_0;
		return L_3;
	}
}
// System.Void UnityEngine.Component::GetComponentsForListInternal(System.Type,System.Object)
extern "C"  void Component_GetComponentsForListInternal_m1978301288 (Component_t1923634451 * __this, Type_t * ___searchType0, Il2CppObject * ___resultList1, const MethodInfo* method)
{
	typedef void (*Component_GetComponentsForListInternal_m1978301288_ftn) (Component_t1923634451 *, Type_t *, Il2CppObject *);
	static Component_GetComponentsForListInternal_m1978301288_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Component_GetComponentsForListInternal_m1978301288_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Component::GetComponentsForListInternal(System.Type,System.Object)");
	_il2cpp_icall_func(__this, ___searchType0, ___resultList1);
}
// System.Void UnityEngine.Component::GetComponents(System.Type,System.Collections.Generic.List`1<UnityEngine.Component>)
extern "C"  void Component_GetComponents_m4024149016 (Component_t1923634451 * __this, Type_t * ___type0, List_1_t3395709193 * ___results1, const MethodInfo* method)
{
	{
		Type_t * L_0 = ___type0;
		List_1_t3395709193 * L_1 = ___results1;
		Component_GetComponentsForListInternal_m1978301288(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.String UnityEngine.Component::get_tag()
extern "C"  String_t* Component_get_tag_m3111332171 (Component_t1923634451 * __this, const MethodInfo* method)
{
	String_t* V_0 = NULL;
	{
		GameObject_t1113636619 * L_0 = Component_get_gameObject_m2648350745(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		String_t* L_1 = GameObject_get_tag_m778614508(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_0012;
	}

IL_0012:
	{
		String_t* L_2 = V_0;
		return L_2;
	}
}
// System.Void UnityEngine.Component::set_tag(System.String)
extern "C"  void Component_set_tag_m782554587 (Component_t1923634451 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		GameObject_t1113636619 * L_0 = Component_get_gameObject_m2648350745(__this, /*hidden argument*/NULL);
		String_t* L_1 = ___value0;
		NullCheck(L_0);
		GameObject_set_tag_m551092519(L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean UnityEngine.Component::CompareTag(System.String)
extern "C"  bool Component_CompareTag_m4001538407 (Component_t1923634451 * __this, String_t* ___tag0, const MethodInfo* method)
{
	typedef bool (*Component_CompareTag_m4001538407_ftn) (Component_t1923634451 *, String_t*);
	static Component_CompareTag_m4001538407_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Component_CompareTag_m4001538407_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Component::CompareTag(System.String)");
	return _il2cpp_icall_func(__this, ___tag0);
}
// System.Void UnityEngine.Component::SendMessageUpwards(System.String,System.Object,UnityEngine.SendMessageOptions)
extern "C"  void Component_SendMessageUpwards_m3320529579 (Component_t1923634451 * __this, String_t* ___methodName0, Il2CppObject * ___value1, int32_t ___options2, const MethodInfo* method)
{
	typedef void (*Component_SendMessageUpwards_m3320529579_ftn) (Component_t1923634451 *, String_t*, Il2CppObject *, int32_t);
	static Component_SendMessageUpwards_m3320529579_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Component_SendMessageUpwards_m3320529579_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Component::SendMessageUpwards(System.String,System.Object,UnityEngine.SendMessageOptions)");
	_il2cpp_icall_func(__this, ___methodName0, ___value1, ___options2);
}
// System.Void UnityEngine.Component::SendMessageUpwards(System.String,System.Object)
extern "C"  void Component_SendMessageUpwards_m3484056427 (Component_t1923634451 * __this, String_t* ___methodName0, Il2CppObject * ___value1, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		V_0 = 0;
		String_t* L_0 = ___methodName0;
		Il2CppObject * L_1 = ___value1;
		int32_t L_2 = V_0;
		Component_SendMessageUpwards_m3320529579(__this, L_0, L_1, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Component::SendMessageUpwards(System.String)
extern "C"  void Component_SendMessageUpwards_m3988656259 (Component_t1923634451 * __this, String_t* ___methodName0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	Il2CppObject * V_1 = NULL;
	{
		V_0 = 0;
		V_1 = NULL;
		String_t* L_0 = ___methodName0;
		Il2CppObject * L_1 = V_1;
		int32_t L_2 = V_0;
		Component_SendMessageUpwards_m3320529579(__this, L_0, L_1, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Component::SendMessageUpwards(System.String,UnityEngine.SendMessageOptions)
extern "C"  void Component_SendMessageUpwards_m2724564744 (Component_t1923634451 * __this, String_t* ___methodName0, int32_t ___options1, const MethodInfo* method)
{
	{
		String_t* L_0 = ___methodName0;
		int32_t L_1 = ___options1;
		Component_SendMessageUpwards_m3320529579(__this, L_0, NULL, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Component::SendMessage(System.String,System.Object,UnityEngine.SendMessageOptions)
extern "C"  void Component_SendMessage_m2000862420 (Component_t1923634451 * __this, String_t* ___methodName0, Il2CppObject * ___value1, int32_t ___options2, const MethodInfo* method)
{
	typedef void (*Component_SendMessage_m2000862420_ftn) (Component_t1923634451 *, String_t*, Il2CppObject *, int32_t);
	static Component_SendMessage_m2000862420_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Component_SendMessage_m2000862420_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Component::SendMessage(System.String,System.Object,UnityEngine.SendMessageOptions)");
	_il2cpp_icall_func(__this, ___methodName0, ___value1, ___options2);
}
// System.Void UnityEngine.Component::SendMessage(System.String,System.Object)
extern "C"  void Component_SendMessage_m2383548706 (Component_t1923634451 * __this, String_t* ___methodName0, Il2CppObject * ___value1, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		V_0 = 0;
		String_t* L_0 = ___methodName0;
		Il2CppObject * L_1 = ___value1;
		int32_t L_2 = V_0;
		Component_SendMessage_m2000862420(__this, L_0, L_1, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Component::SendMessage(System.String)
extern "C"  void Component_SendMessage_m2375064349 (Component_t1923634451 * __this, String_t* ___methodName0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	Il2CppObject * V_1 = NULL;
	{
		V_0 = 0;
		V_1 = NULL;
		String_t* L_0 = ___methodName0;
		Il2CppObject * L_1 = V_1;
		int32_t L_2 = V_0;
		Component_SendMessage_m2000862420(__this, L_0, L_1, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Component::SendMessage(System.String,UnityEngine.SendMessageOptions)
extern "C"  void Component_SendMessage_m3887616663 (Component_t1923634451 * __this, String_t* ___methodName0, int32_t ___options1, const MethodInfo* method)
{
	{
		String_t* L_0 = ___methodName0;
		int32_t L_1 = ___options1;
		Component_SendMessage_m2000862420(__this, L_0, NULL, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Component::BroadcastMessage(System.String,System.Object,UnityEngine.SendMessageOptions)
extern "C"  void Component_BroadcastMessage_m3615836039 (Component_t1923634451 * __this, String_t* ___methodName0, Il2CppObject * ___parameter1, int32_t ___options2, const MethodInfo* method)
{
	typedef void (*Component_BroadcastMessage_m3615836039_ftn) (Component_t1923634451 *, String_t*, Il2CppObject *, int32_t);
	static Component_BroadcastMessage_m3615836039_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Component_BroadcastMessage_m3615836039_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Component::BroadcastMessage(System.String,System.Object,UnityEngine.SendMessageOptions)");
	_il2cpp_icall_func(__this, ___methodName0, ___parameter1, ___options2);
}
// System.Void UnityEngine.Component::BroadcastMessage(System.String,System.Object)
extern "C"  void Component_BroadcastMessage_m677337613 (Component_t1923634451 * __this, String_t* ___methodName0, Il2CppObject * ___parameter1, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		V_0 = 0;
		String_t* L_0 = ___methodName0;
		Il2CppObject * L_1 = ___parameter1;
		int32_t L_2 = V_0;
		Component_BroadcastMessage_m3615836039(__this, L_0, L_1, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Component::BroadcastMessage(System.String)
extern "C"  void Component_BroadcastMessage_m1141514818 (Component_t1923634451 * __this, String_t* ___methodName0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	Il2CppObject * V_1 = NULL;
	{
		V_0 = 0;
		V_1 = NULL;
		String_t* L_0 = ___methodName0;
		Il2CppObject * L_1 = V_1;
		int32_t L_2 = V_0;
		Component_BroadcastMessage_m3615836039(__this, L_0, L_1, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Component::BroadcastMessage(System.String,UnityEngine.SendMessageOptions)
extern "C"  void Component_BroadcastMessage_m3581323585 (Component_t1923634451 * __this, String_t* ___methodName0, int32_t ___options1, const MethodInfo* method)
{
	{
		String_t* L_0 = ___methodName0;
		int32_t L_1 = ___options1;
		Component_BroadcastMessage_m3615836039(__this, L_0, NULL, L_1, /*hidden argument*/NULL);
		return;
	}
}
// Conversion methods for marshalling of: UnityEngine.ContactFilter2D
extern "C" void ContactFilter2D_t3805203441_marshal_pinvoke(const ContactFilter2D_t3805203441& unmarshaled, ContactFilter2D_t3805203441_marshaled_pinvoke& marshaled)
{
	marshaled.___useTriggers_0 = static_cast<int32_t>(unmarshaled.get_useTriggers_0());
	marshaled.___useLayerMask_1 = static_cast<int32_t>(unmarshaled.get_useLayerMask_1());
	marshaled.___useDepth_2 = static_cast<int32_t>(unmarshaled.get_useDepth_2());
	marshaled.___useOutsideDepth_3 = static_cast<int32_t>(unmarshaled.get_useOutsideDepth_3());
	marshaled.___useNormalAngle_4 = static_cast<int32_t>(unmarshaled.get_useNormalAngle_4());
	marshaled.___useOutsideNormalAngle_5 = static_cast<int32_t>(unmarshaled.get_useOutsideNormalAngle_5());
	marshaled.___layerMask_6 = unmarshaled.get_layerMask_6();
	marshaled.___minDepth_7 = unmarshaled.get_minDepth_7();
	marshaled.___maxDepth_8 = unmarshaled.get_maxDepth_8();
	marshaled.___minNormalAngle_9 = unmarshaled.get_minNormalAngle_9();
	marshaled.___maxNormalAngle_10 = unmarshaled.get_maxNormalAngle_10();
}
extern "C" void ContactFilter2D_t3805203441_marshal_pinvoke_back(const ContactFilter2D_t3805203441_marshaled_pinvoke& marshaled, ContactFilter2D_t3805203441& unmarshaled)
{
	bool unmarshaled_useTriggers_temp_0 = false;
	unmarshaled_useTriggers_temp_0 = static_cast<bool>(marshaled.___useTriggers_0);
	unmarshaled.set_useTriggers_0(unmarshaled_useTriggers_temp_0);
	bool unmarshaled_useLayerMask_temp_1 = false;
	unmarshaled_useLayerMask_temp_1 = static_cast<bool>(marshaled.___useLayerMask_1);
	unmarshaled.set_useLayerMask_1(unmarshaled_useLayerMask_temp_1);
	bool unmarshaled_useDepth_temp_2 = false;
	unmarshaled_useDepth_temp_2 = static_cast<bool>(marshaled.___useDepth_2);
	unmarshaled.set_useDepth_2(unmarshaled_useDepth_temp_2);
	bool unmarshaled_useOutsideDepth_temp_3 = false;
	unmarshaled_useOutsideDepth_temp_3 = static_cast<bool>(marshaled.___useOutsideDepth_3);
	unmarshaled.set_useOutsideDepth_3(unmarshaled_useOutsideDepth_temp_3);
	bool unmarshaled_useNormalAngle_temp_4 = false;
	unmarshaled_useNormalAngle_temp_4 = static_cast<bool>(marshaled.___useNormalAngle_4);
	unmarshaled.set_useNormalAngle_4(unmarshaled_useNormalAngle_temp_4);
	bool unmarshaled_useOutsideNormalAngle_temp_5 = false;
	unmarshaled_useOutsideNormalAngle_temp_5 = static_cast<bool>(marshaled.___useOutsideNormalAngle_5);
	unmarshaled.set_useOutsideNormalAngle_5(unmarshaled_useOutsideNormalAngle_temp_5);
	LayerMask_t3493934918  unmarshaled_layerMask_temp_6;
	memset(&unmarshaled_layerMask_temp_6, 0, sizeof(unmarshaled_layerMask_temp_6));
	unmarshaled_layerMask_temp_6 = marshaled.___layerMask_6;
	unmarshaled.set_layerMask_6(unmarshaled_layerMask_temp_6);
	float unmarshaled_minDepth_temp_7 = 0.0f;
	unmarshaled_minDepth_temp_7 = marshaled.___minDepth_7;
	unmarshaled.set_minDepth_7(unmarshaled_minDepth_temp_7);
	float unmarshaled_maxDepth_temp_8 = 0.0f;
	unmarshaled_maxDepth_temp_8 = marshaled.___maxDepth_8;
	unmarshaled.set_maxDepth_8(unmarshaled_maxDepth_temp_8);
	float unmarshaled_minNormalAngle_temp_9 = 0.0f;
	unmarshaled_minNormalAngle_temp_9 = marshaled.___minNormalAngle_9;
	unmarshaled.set_minNormalAngle_9(unmarshaled_minNormalAngle_temp_9);
	float unmarshaled_maxNormalAngle_temp_10 = 0.0f;
	unmarshaled_maxNormalAngle_temp_10 = marshaled.___maxNormalAngle_10;
	unmarshaled.set_maxNormalAngle_10(unmarshaled_maxNormalAngle_temp_10);
}
// Conversion method for clean up from marshalling of: UnityEngine.ContactFilter2D
extern "C" void ContactFilter2D_t3805203441_marshal_pinvoke_cleanup(ContactFilter2D_t3805203441_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.ContactFilter2D
extern "C" void ContactFilter2D_t3805203441_marshal_com(const ContactFilter2D_t3805203441& unmarshaled, ContactFilter2D_t3805203441_marshaled_com& marshaled)
{
	marshaled.___useTriggers_0 = static_cast<int32_t>(unmarshaled.get_useTriggers_0());
	marshaled.___useLayerMask_1 = static_cast<int32_t>(unmarshaled.get_useLayerMask_1());
	marshaled.___useDepth_2 = static_cast<int32_t>(unmarshaled.get_useDepth_2());
	marshaled.___useOutsideDepth_3 = static_cast<int32_t>(unmarshaled.get_useOutsideDepth_3());
	marshaled.___useNormalAngle_4 = static_cast<int32_t>(unmarshaled.get_useNormalAngle_4());
	marshaled.___useOutsideNormalAngle_5 = static_cast<int32_t>(unmarshaled.get_useOutsideNormalAngle_5());
	marshaled.___layerMask_6 = unmarshaled.get_layerMask_6();
	marshaled.___minDepth_7 = unmarshaled.get_minDepth_7();
	marshaled.___maxDepth_8 = unmarshaled.get_maxDepth_8();
	marshaled.___minNormalAngle_9 = unmarshaled.get_minNormalAngle_9();
	marshaled.___maxNormalAngle_10 = unmarshaled.get_maxNormalAngle_10();
}
extern "C" void ContactFilter2D_t3805203441_marshal_com_back(const ContactFilter2D_t3805203441_marshaled_com& marshaled, ContactFilter2D_t3805203441& unmarshaled)
{
	bool unmarshaled_useTriggers_temp_0 = false;
	unmarshaled_useTriggers_temp_0 = static_cast<bool>(marshaled.___useTriggers_0);
	unmarshaled.set_useTriggers_0(unmarshaled_useTriggers_temp_0);
	bool unmarshaled_useLayerMask_temp_1 = false;
	unmarshaled_useLayerMask_temp_1 = static_cast<bool>(marshaled.___useLayerMask_1);
	unmarshaled.set_useLayerMask_1(unmarshaled_useLayerMask_temp_1);
	bool unmarshaled_useDepth_temp_2 = false;
	unmarshaled_useDepth_temp_2 = static_cast<bool>(marshaled.___useDepth_2);
	unmarshaled.set_useDepth_2(unmarshaled_useDepth_temp_2);
	bool unmarshaled_useOutsideDepth_temp_3 = false;
	unmarshaled_useOutsideDepth_temp_3 = static_cast<bool>(marshaled.___useOutsideDepth_3);
	unmarshaled.set_useOutsideDepth_3(unmarshaled_useOutsideDepth_temp_3);
	bool unmarshaled_useNormalAngle_temp_4 = false;
	unmarshaled_useNormalAngle_temp_4 = static_cast<bool>(marshaled.___useNormalAngle_4);
	unmarshaled.set_useNormalAngle_4(unmarshaled_useNormalAngle_temp_4);
	bool unmarshaled_useOutsideNormalAngle_temp_5 = false;
	unmarshaled_useOutsideNormalAngle_temp_5 = static_cast<bool>(marshaled.___useOutsideNormalAngle_5);
	unmarshaled.set_useOutsideNormalAngle_5(unmarshaled_useOutsideNormalAngle_temp_5);
	LayerMask_t3493934918  unmarshaled_layerMask_temp_6;
	memset(&unmarshaled_layerMask_temp_6, 0, sizeof(unmarshaled_layerMask_temp_6));
	unmarshaled_layerMask_temp_6 = marshaled.___layerMask_6;
	unmarshaled.set_layerMask_6(unmarshaled_layerMask_temp_6);
	float unmarshaled_minDepth_temp_7 = 0.0f;
	unmarshaled_minDepth_temp_7 = marshaled.___minDepth_7;
	unmarshaled.set_minDepth_7(unmarshaled_minDepth_temp_7);
	float unmarshaled_maxDepth_temp_8 = 0.0f;
	unmarshaled_maxDepth_temp_8 = marshaled.___maxDepth_8;
	unmarshaled.set_maxDepth_8(unmarshaled_maxDepth_temp_8);
	float unmarshaled_minNormalAngle_temp_9 = 0.0f;
	unmarshaled_minNormalAngle_temp_9 = marshaled.___minNormalAngle_9;
	unmarshaled.set_minNormalAngle_9(unmarshaled_minNormalAngle_temp_9);
	float unmarshaled_maxNormalAngle_temp_10 = 0.0f;
	unmarshaled_maxNormalAngle_temp_10 = marshaled.___maxNormalAngle_10;
	unmarshaled.set_maxNormalAngle_10(unmarshaled_maxNormalAngle_temp_10);
}
// Conversion method for clean up from marshalling of: UnityEngine.ContactFilter2D
extern "C" void ContactFilter2D_t3805203441_marshal_com_cleanup(ContactFilter2D_t3805203441_marshaled_com& marshaled)
{
}
// System.Void UnityEngine.ContactFilter2D::CheckConsistency()
extern "C"  void ContactFilter2D_CheckConsistency_m1930104761 (ContactFilter2D_t3805203441 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ContactFilter2D_CheckConsistency_m1930104761_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	ContactFilter2D_t3805203441 * G_B3_0 = NULL;
	ContactFilter2D_t3805203441 * G_B1_0 = NULL;
	ContactFilter2D_t3805203441 * G_B2_0 = NULL;
	ContactFilter2D_t3805203441 * G_B4_0 = NULL;
	float G_B5_0 = 0.0f;
	ContactFilter2D_t3805203441 * G_B5_1 = NULL;
	ContactFilter2D_t3805203441 * G_B8_0 = NULL;
	ContactFilter2D_t3805203441 * G_B6_0 = NULL;
	ContactFilter2D_t3805203441 * G_B7_0 = NULL;
	ContactFilter2D_t3805203441 * G_B9_0 = NULL;
	float G_B10_0 = 0.0f;
	ContactFilter2D_t3805203441 * G_B10_1 = NULL;
	ContactFilter2D_t3805203441 * G_B14_0 = NULL;
	ContactFilter2D_t3805203441 * G_B13_0 = NULL;
	float G_B15_0 = 0.0f;
	ContactFilter2D_t3805203441 * G_B15_1 = NULL;
	ContactFilter2D_t3805203441 * G_B17_0 = NULL;
	ContactFilter2D_t3805203441 * G_B16_0 = NULL;
	float G_B18_0 = 0.0f;
	ContactFilter2D_t3805203441 * G_B18_1 = NULL;
	{
		float L_0 = __this->get_minDepth_7();
		G_B1_0 = __this;
		if ((((float)L_0) == ((float)(-std::numeric_limits<float>::infinity()))))
		{
			G_B3_0 = __this;
			goto IL_0032;
		}
	}
	{
		float L_1 = __this->get_minDepth_7();
		G_B2_0 = G_B1_0;
		if ((((float)L_1) == ((float)(std::numeric_limits<float>::infinity()))))
		{
			G_B3_0 = G_B1_0;
			goto IL_0032;
		}
	}
	{
		float L_2 = __this->get_minDepth_7();
		bool L_3 = Single_IsNaN_m4024467661(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		G_B3_0 = G_B2_0;
		if (!L_3)
		{
			G_B4_0 = G_B2_0;
			goto IL_003c;
		}
	}

IL_0032:
	{
		G_B5_0 = (-std::numeric_limits<float>::max());
		G_B5_1 = G_B3_0;
		goto IL_0042;
	}

IL_003c:
	{
		float L_4 = __this->get_minDepth_7();
		G_B5_0 = L_4;
		G_B5_1 = G_B4_0;
	}

IL_0042:
	{
		G_B5_1->set_minDepth_7(G_B5_0);
		float L_5 = __this->get_maxDepth_8();
		G_B6_0 = __this;
		if ((((float)L_5) == ((float)(-std::numeric_limits<float>::infinity()))))
		{
			G_B8_0 = __this;
			goto IL_0078;
		}
	}
	{
		float L_6 = __this->get_maxDepth_8();
		G_B7_0 = G_B6_0;
		if ((((float)L_6) == ((float)(std::numeric_limits<float>::infinity()))))
		{
			G_B8_0 = G_B6_0;
			goto IL_0078;
		}
	}
	{
		float L_7 = __this->get_maxDepth_8();
		bool L_8 = Single_IsNaN_m4024467661(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
		G_B8_0 = G_B7_0;
		if (!L_8)
		{
			G_B9_0 = G_B7_0;
			goto IL_0082;
		}
	}

IL_0078:
	{
		G_B10_0 = (std::numeric_limits<float>::max());
		G_B10_1 = G_B8_0;
		goto IL_0088;
	}

IL_0082:
	{
		float L_9 = __this->get_maxDepth_8();
		G_B10_0 = L_9;
		G_B10_1 = G_B9_0;
	}

IL_0088:
	{
		G_B10_1->set_maxDepth_8(G_B10_0);
		float L_10 = __this->get_minDepth_7();
		float L_11 = __this->get_maxDepth_8();
		if ((!(((float)L_10) > ((float)L_11))))
		{
			goto IL_00ba;
		}
	}
	{
		float L_12 = __this->get_minDepth_7();
		V_0 = L_12;
		float L_13 = __this->get_maxDepth_8();
		__this->set_minDepth_7(L_13);
		float L_14 = V_0;
		__this->set_maxDepth_8(L_14);
	}

IL_00ba:
	{
		float L_15 = __this->get_minNormalAngle_9();
		bool L_16 = Single_IsNaN_m4024467661(NULL /*static, unused*/, L_15, /*hidden argument*/NULL);
		G_B13_0 = __this;
		if (!L_16)
		{
			G_B14_0 = __this;
			goto IL_00d5;
		}
	}
	{
		G_B15_0 = (0.0f);
		G_B15_1 = G_B13_0;
		goto IL_00ea;
	}

IL_00d5:
	{
		float L_17 = __this->get_minNormalAngle_9();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t3464937446_il2cpp_TypeInfo_var);
		float L_18 = Mathf_Clamp_m500974738(NULL /*static, unused*/, L_17, (0.0f), (359.9999f), /*hidden argument*/NULL);
		G_B15_0 = L_18;
		G_B15_1 = G_B14_0;
	}

IL_00ea:
	{
		G_B15_1->set_minNormalAngle_9(G_B15_0);
		float L_19 = __this->get_maxNormalAngle_10();
		bool L_20 = Single_IsNaN_m4024467661(NULL /*static, unused*/, L_19, /*hidden argument*/NULL);
		G_B16_0 = __this;
		if (!L_20)
		{
			G_B17_0 = __this;
			goto IL_010a;
		}
	}
	{
		G_B18_0 = (359.9999f);
		G_B18_1 = G_B16_0;
		goto IL_011f;
	}

IL_010a:
	{
		float L_21 = __this->get_maxNormalAngle_10();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t3464937446_il2cpp_TypeInfo_var);
		float L_22 = Mathf_Clamp_m500974738(NULL /*static, unused*/, L_21, (0.0f), (359.9999f), /*hidden argument*/NULL);
		G_B18_0 = L_22;
		G_B18_1 = G_B17_0;
	}

IL_011f:
	{
		G_B18_1->set_maxNormalAngle_10(G_B18_0);
		float L_23 = __this->get_minNormalAngle_9();
		float L_24 = __this->get_maxNormalAngle_10();
		if ((!(((float)L_23) > ((float)L_24))))
		{
			goto IL_0151;
		}
	}
	{
		float L_25 = __this->get_minNormalAngle_9();
		V_1 = L_25;
		float L_26 = __this->get_maxNormalAngle_10();
		__this->set_minNormalAngle_9(L_26);
		float L_27 = V_1;
		__this->set_maxNormalAngle_10(L_27);
	}

IL_0151:
	{
		return;
	}
}
extern "C"  void ContactFilter2D_CheckConsistency_m1930104761_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	ContactFilter2D_t3805203441 * _thisAdjusted = reinterpret_cast<ContactFilter2D_t3805203441 *>(__this + 1);
	ContactFilter2D_CheckConsistency_m1930104761(_thisAdjusted, method);
}
// System.Void UnityEngine.ContactFilter2D::SetLayerMask(UnityEngine.LayerMask)
extern "C"  void ContactFilter2D_SetLayerMask_m3854128592 (ContactFilter2D_t3805203441 * __this, LayerMask_t3493934918  ___layerMask0, const MethodInfo* method)
{
	{
		LayerMask_t3493934918  L_0 = ___layerMask0;
		__this->set_layerMask_6(L_0);
		__this->set_useLayerMask_1((bool)1);
		return;
	}
}
extern "C"  void ContactFilter2D_SetLayerMask_m3854128592_AdjustorThunk (Il2CppObject * __this, LayerMask_t3493934918  ___layerMask0, const MethodInfo* method)
{
	ContactFilter2D_t3805203441 * _thisAdjusted = reinterpret_cast<ContactFilter2D_t3805203441 *>(__this + 1);
	ContactFilter2D_SetLayerMask_m3854128592(_thisAdjusted, ___layerMask0, method);
}
// System.Void UnityEngine.ContactFilter2D::SetDepth(System.Single,System.Single)
extern "C"  void ContactFilter2D_SetDepth_m956990602 (ContactFilter2D_t3805203441 * __this, float ___minDepth0, float ___maxDepth1, const MethodInfo* method)
{
	{
		float L_0 = ___minDepth0;
		__this->set_minDepth_7(L_0);
		float L_1 = ___maxDepth1;
		__this->set_maxDepth_8(L_1);
		__this->set_useDepth_2((bool)1);
		ContactFilter2D_CheckConsistency_m1930104761(__this, /*hidden argument*/NULL);
		return;
	}
}
extern "C"  void ContactFilter2D_SetDepth_m956990602_AdjustorThunk (Il2CppObject * __this, float ___minDepth0, float ___maxDepth1, const MethodInfo* method)
{
	ContactFilter2D_t3805203441 * _thisAdjusted = reinterpret_cast<ContactFilter2D_t3805203441 *>(__this + 1);
	ContactFilter2D_SetDepth_m956990602(_thisAdjusted, ___minDepth0, ___maxDepth1, method);
}
// UnityEngine.ContactFilter2D UnityEngine.ContactFilter2D::CreateLegacyFilter(System.Int32,System.Single,System.Single)
extern "C"  ContactFilter2D_t3805203441  ContactFilter2D_CreateLegacyFilter_m949420789 (Il2CppObject * __this /* static, unused */, int32_t ___layerMask0, float ___minDepth1, float ___maxDepth2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ContactFilter2D_CreateLegacyFilter_m949420789_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ContactFilter2D_t3805203441  V_0;
	memset(&V_0, 0, sizeof(V_0));
	ContactFilter2D_t3805203441  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		Initobj (ContactFilter2D_t3805203441_il2cpp_TypeInfo_var, (&V_0));
		IL2CPP_RUNTIME_CLASS_INIT(Physics2D_t1528932956_il2cpp_TypeInfo_var);
		bool L_0 = Physics2D_get_queriesHitTriggers_m3312014212(NULL /*static, unused*/, /*hidden argument*/NULL);
		(&V_0)->set_useTriggers_0(L_0);
		int32_t L_1 = ___layerMask0;
		LayerMask_t3493934918  L_2 = LayerMask_op_Implicit_m1727876484(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		ContactFilter2D_SetLayerMask_m3854128592((&V_0), L_2, /*hidden argument*/NULL);
		float L_3 = ___minDepth1;
		float L_4 = ___maxDepth2;
		ContactFilter2D_SetDepth_m956990602((&V_0), L_3, L_4, /*hidden argument*/NULL);
		ContactFilter2D_t3805203441  L_5 = V_0;
		V_1 = L_5;
		goto IL_0032;
	}

IL_0032:
	{
		ContactFilter2D_t3805203441  L_6 = V_1;
		return L_6;
	}
}
// System.Void UnityEngine.ContextMenu::.ctor(System.String)
extern "C"  void ContextMenu__ctor_m352769692 (ContextMenu_t1295656858 * __this, String_t* ___itemName0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___itemName0;
		ContextMenu__ctor_m4148345090(__this, L_0, (bool)0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.ContextMenu::.ctor(System.String,System.Boolean)
extern "C"  void ContextMenu__ctor_m4148345090 (ContextMenu_t1295656858 * __this, String_t* ___itemName0, bool ___isValidateFunction1, const MethodInfo* method)
{
	{
		String_t* L_0 = ___itemName0;
		bool L_1 = ___isValidateFunction1;
		ContextMenu__ctor_m906931878(__this, L_0, L_1, ((int32_t)1000000), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.ContextMenu::.ctor(System.String,System.Boolean,System.Int32)
extern "C"  void ContextMenu__ctor_m906931878 (ContextMenu_t1295656858 * __this, String_t* ___itemName0, bool ___isValidateFunction1, int32_t ___priority2, const MethodInfo* method)
{
	{
		Attribute__ctor_m1529526131(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___itemName0;
		__this->set_menuItem_0(L_0);
		bool L_1 = ___isValidateFunction1;
		__this->set_validate_1(L_1);
		int32_t L_2 = ___priority2;
		__this->set_priority_2(L_2);
		return;
	}
}
// Conversion methods for marshalling of: UnityEngine.ControllerColliderHit
extern "C" void ControllerColliderHit_t240592346_marshal_pinvoke(const ControllerColliderHit_t240592346& unmarshaled, ControllerColliderHit_t240592346_marshaled_pinvoke& marshaled)
{
	Il2CppCodeGenException* ___m_Controller_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_Controller' of type 'ControllerColliderHit': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_Controller_0Exception);
}
extern "C" void ControllerColliderHit_t240592346_marshal_pinvoke_back(const ControllerColliderHit_t240592346_marshaled_pinvoke& marshaled, ControllerColliderHit_t240592346& unmarshaled)
{
	Il2CppCodeGenException* ___m_Controller_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_Controller' of type 'ControllerColliderHit': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_Controller_0Exception);
}
// Conversion method for clean up from marshalling of: UnityEngine.ControllerColliderHit
extern "C" void ControllerColliderHit_t240592346_marshal_pinvoke_cleanup(ControllerColliderHit_t240592346_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.ControllerColliderHit
extern "C" void ControllerColliderHit_t240592346_marshal_com(const ControllerColliderHit_t240592346& unmarshaled, ControllerColliderHit_t240592346_marshaled_com& marshaled)
{
	Il2CppCodeGenException* ___m_Controller_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_Controller' of type 'ControllerColliderHit': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_Controller_0Exception);
}
extern "C" void ControllerColliderHit_t240592346_marshal_com_back(const ControllerColliderHit_t240592346_marshaled_com& marshaled, ControllerColliderHit_t240592346& unmarshaled)
{
	Il2CppCodeGenException* ___m_Controller_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_Controller' of type 'ControllerColliderHit': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_Controller_0Exception);
}
// Conversion method for clean up from marshalling of: UnityEngine.ControllerColliderHit
extern "C" void ControllerColliderHit_t240592346_marshal_com_cleanup(ControllerColliderHit_t240592346_marshaled_com& marshaled)
{
}
// System.Void UnityEngine.ControllerColliderHit::.ctor()
extern "C"  void ControllerColliderHit__ctor_m3407574866 (ControllerColliderHit_t240592346 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m297566312(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.CharacterController UnityEngine.ControllerColliderHit::get_controller()
extern "C"  CharacterController_t1138636865 * ControllerColliderHit_get_controller_m1977299589 (ControllerColliderHit_t240592346 * __this, const MethodInfo* method)
{
	CharacterController_t1138636865 * V_0 = NULL;
	{
		CharacterController_t1138636865 * L_0 = __this->get_m_Controller_0();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		CharacterController_t1138636865 * L_1 = V_0;
		return L_1;
	}
}
// UnityEngine.Collider UnityEngine.ControllerColliderHit::get_collider()
extern "C"  Collider_t1773347010 * ControllerColliderHit_get_collider_m1464046619 (ControllerColliderHit_t240592346 * __this, const MethodInfo* method)
{
	Collider_t1773347010 * V_0 = NULL;
	{
		Collider_t1773347010 * L_0 = __this->get_m_Collider_1();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		Collider_t1773347010 * L_1 = V_0;
		return L_1;
	}
}
// UnityEngine.Rigidbody UnityEngine.ControllerColliderHit::get_rigidbody()
extern "C"  Rigidbody_t3916780224 * ControllerColliderHit_get_rigidbody_m1414965731 (ControllerColliderHit_t240592346 * __this, const MethodInfo* method)
{
	Rigidbody_t3916780224 * V_0 = NULL;
	{
		Collider_t1773347010 * L_0 = __this->get_m_Collider_1();
		NullCheck(L_0);
		Rigidbody_t3916780224 * L_1 = Collider_get_attachedRigidbody_m4203494256(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_0012;
	}

IL_0012:
	{
		Rigidbody_t3916780224 * L_2 = V_0;
		return L_2;
	}
}
// UnityEngine.GameObject UnityEngine.ControllerColliderHit::get_gameObject()
extern "C"  GameObject_t1113636619 * ControllerColliderHit_get_gameObject_m2778394299 (ControllerColliderHit_t240592346 * __this, const MethodInfo* method)
{
	GameObject_t1113636619 * V_0 = NULL;
	{
		Collider_t1773347010 * L_0 = __this->get_m_Collider_1();
		NullCheck(L_0);
		GameObject_t1113636619 * L_1 = Component_get_gameObject_m2648350745(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_0012;
	}

IL_0012:
	{
		GameObject_t1113636619 * L_2 = V_0;
		return L_2;
	}
}
// UnityEngine.Transform UnityEngine.ControllerColliderHit::get_transform()
extern "C"  Transform_t3600365921 * ControllerColliderHit_get_transform_m2995853850 (ControllerColliderHit_t240592346 * __this, const MethodInfo* method)
{
	Transform_t3600365921 * V_0 = NULL;
	{
		Collider_t1773347010 * L_0 = __this->get_m_Collider_1();
		NullCheck(L_0);
		Transform_t3600365921 * L_1 = Component_get_transform_m2921103810(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_0012;
	}

IL_0012:
	{
		Transform_t3600365921 * L_2 = V_0;
		return L_2;
	}
}
// UnityEngine.Vector3 UnityEngine.ControllerColliderHit::get_point()
extern "C"  Vector3_t3722313464  ControllerColliderHit_get_point_m1370041567 (ControllerColliderHit_t240592346 * __this, const MethodInfo* method)
{
	Vector3_t3722313464  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Vector3_t3722313464  L_0 = __this->get_m_Point_2();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		Vector3_t3722313464  L_1 = V_0;
		return L_1;
	}
}
// UnityEngine.Vector3 UnityEngine.ControllerColliderHit::get_normal()
extern "C"  Vector3_t3722313464  ControllerColliderHit_get_normal_m2666753182 (ControllerColliderHit_t240592346 * __this, const MethodInfo* method)
{
	Vector3_t3722313464  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Vector3_t3722313464  L_0 = __this->get_m_Normal_3();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		Vector3_t3722313464  L_1 = V_0;
		return L_1;
	}
}
// UnityEngine.Vector3 UnityEngine.ControllerColliderHit::get_moveDirection()
extern "C"  Vector3_t3722313464  ControllerColliderHit_get_moveDirection_m1756043951 (ControllerColliderHit_t240592346 * __this, const MethodInfo* method)
{
	Vector3_t3722313464  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Vector3_t3722313464  L_0 = __this->get_m_MoveDirection_4();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		Vector3_t3722313464  L_1 = V_0;
		return L_1;
	}
}
// System.Single UnityEngine.ControllerColliderHit::get_moveLength()
extern "C"  float ControllerColliderHit_get_moveLength_m1588555106 (ControllerColliderHit_t240592346 * __this, const MethodInfo* method)
{
	float V_0 = 0.0f;
	{
		float L_0 = __this->get_m_MoveLength_5();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		float L_1 = V_0;
		return L_1;
	}
}
// System.Boolean UnityEngine.ControllerColliderHit::get_push()
extern "C"  bool ControllerColliderHit_get_push_m1219653818 (ControllerColliderHit_t240592346 * __this, const MethodInfo* method)
{
	bool V_0 = false;
	{
		int32_t L_0 = __this->get_m_Push_6();
		V_0 = (bool)((((int32_t)((((int32_t)L_0) == ((int32_t)0))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0013;
	}

IL_0013:
	{
		bool L_1 = V_0;
		return L_1;
	}
}
// System.Void UnityEngine.ControllerColliderHit::set_push(System.Boolean)
extern "C"  void ControllerColliderHit_set_push_m2495521645 (ControllerColliderHit_t240592346 * __this, bool ___value0, const MethodInfo* method)
{
	ControllerColliderHit_t240592346 * G_B2_0 = NULL;
	ControllerColliderHit_t240592346 * G_B1_0 = NULL;
	int32_t G_B3_0 = 0;
	ControllerColliderHit_t240592346 * G_B3_1 = NULL;
	{
		bool L_0 = ___value0;
		G_B1_0 = __this;
		if (!L_0)
		{
			G_B2_0 = __this;
			goto IL_000e;
		}
	}
	{
		G_B3_0 = 1;
		G_B3_1 = G_B1_0;
		goto IL_000f;
	}

IL_000e:
	{
		G_B3_0 = 0;
		G_B3_1 = G_B2_0;
	}

IL_000f:
	{
		NullCheck(G_B3_1);
		G_B3_1->set_m_Push_6(G_B3_0);
		return;
	}
}
// Conversion methods for marshalling of: UnityEngine.Coroutine
extern "C" void Coroutine_t3829159415_marshal_pinvoke(const Coroutine_t3829159415& unmarshaled, Coroutine_t3829159415_marshaled_pinvoke& marshaled)
{
	marshaled.___m_Ptr_0 = reinterpret_cast<intptr_t>((unmarshaled.get_m_Ptr_0()).get_m_value_0());
}
extern "C" void Coroutine_t3829159415_marshal_pinvoke_back(const Coroutine_t3829159415_marshaled_pinvoke& marshaled, Coroutine_t3829159415& unmarshaled)
{
	IntPtr_t unmarshaled_m_Ptr_temp_0;
	memset(&unmarshaled_m_Ptr_temp_0, 0, sizeof(unmarshaled_m_Ptr_temp_0));
	IntPtr_t unmarshaled_m_Ptr_temp_0_temp;
	unmarshaled_m_Ptr_temp_0_temp.set_m_value_0(reinterpret_cast<void*>((intptr_t)(marshaled.___m_Ptr_0)));
	unmarshaled_m_Ptr_temp_0 = unmarshaled_m_Ptr_temp_0_temp;
	unmarshaled.set_m_Ptr_0(unmarshaled_m_Ptr_temp_0);
}
// Conversion method for clean up from marshalling of: UnityEngine.Coroutine
extern "C" void Coroutine_t3829159415_marshal_pinvoke_cleanup(Coroutine_t3829159415_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.Coroutine
extern "C" void Coroutine_t3829159415_marshal_com(const Coroutine_t3829159415& unmarshaled, Coroutine_t3829159415_marshaled_com& marshaled)
{
	marshaled.___m_Ptr_0 = reinterpret_cast<intptr_t>((unmarshaled.get_m_Ptr_0()).get_m_value_0());
}
extern "C" void Coroutine_t3829159415_marshal_com_back(const Coroutine_t3829159415_marshaled_com& marshaled, Coroutine_t3829159415& unmarshaled)
{
	IntPtr_t unmarshaled_m_Ptr_temp_0;
	memset(&unmarshaled_m_Ptr_temp_0, 0, sizeof(unmarshaled_m_Ptr_temp_0));
	IntPtr_t unmarshaled_m_Ptr_temp_0_temp;
	unmarshaled_m_Ptr_temp_0_temp.set_m_value_0(reinterpret_cast<void*>((intptr_t)(marshaled.___m_Ptr_0)));
	unmarshaled_m_Ptr_temp_0 = unmarshaled_m_Ptr_temp_0_temp;
	unmarshaled.set_m_Ptr_0(unmarshaled_m_Ptr_temp_0);
}
// Conversion method for clean up from marshalling of: UnityEngine.Coroutine
extern "C" void Coroutine_t3829159415_marshal_com_cleanup(Coroutine_t3829159415_marshaled_com& marshaled)
{
}
// System.Void UnityEngine.Coroutine::.ctor()
extern "C"  void Coroutine__ctor_m543355591 (Coroutine_t3829159415 * __this, const MethodInfo* method)
{
	{
		YieldInstruction__ctor_m3893172416(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Coroutine::ReleaseCoroutine()
extern "C"  void Coroutine_ReleaseCoroutine_m1341336641 (Coroutine_t3829159415 * __this, const MethodInfo* method)
{
	typedef void (*Coroutine_ReleaseCoroutine_m1341336641_ftn) (Coroutine_t3829159415 *);
	static Coroutine_ReleaseCoroutine_m1341336641_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Coroutine_ReleaseCoroutine_m1341336641_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Coroutine::ReleaseCoroutine()");
	_il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Coroutine::Finalize()
extern "C"  void Coroutine_Finalize_m1992241042 (Coroutine_t3829159415 * __this, const MethodInfo* method)
{
	Exception_t1436737249 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1436737249 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
	}

IL_0001:
	try
	{ // begin try (depth: 1)
		Coroutine_ReleaseCoroutine_m1341336641(__this, /*hidden argument*/NULL);
		IL2CPP_LEAVE(0x13, FINALLY_000c);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1436737249 *)e.ex;
		goto FINALLY_000c;
	}

FINALLY_000c:
	{ // begin finally (depth: 1)
		Object_Finalize_m3076187857(__this, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(12)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(12)
	{
		IL2CPP_JUMP_TBL(0x13, IL_0013)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1436737249 *)
	}

IL_0013:
	{
		return;
	}
}
// Conversion methods for marshalling of: UnityEngine.CullingGroup
extern "C" void CullingGroup_t2096318768_marshal_pinvoke(const CullingGroup_t2096318768& unmarshaled, CullingGroup_t2096318768_marshaled_pinvoke& marshaled)
{
	marshaled.___m_Ptr_0 = reinterpret_cast<intptr_t>((unmarshaled.get_m_Ptr_0()).get_m_value_0());
	marshaled.___m_OnStateChanged_1 = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(unmarshaled.get_m_OnStateChanged_1()));
}
extern "C" void CullingGroup_t2096318768_marshal_pinvoke_back(const CullingGroup_t2096318768_marshaled_pinvoke& marshaled, CullingGroup_t2096318768& unmarshaled)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CullingGroup_t2096318768_pinvoke_FromNativeMethodDefinition_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	IntPtr_t unmarshaled_m_Ptr_temp_0;
	memset(&unmarshaled_m_Ptr_temp_0, 0, sizeof(unmarshaled_m_Ptr_temp_0));
	IntPtr_t unmarshaled_m_Ptr_temp_0_temp;
	unmarshaled_m_Ptr_temp_0_temp.set_m_value_0(reinterpret_cast<void*>((intptr_t)(marshaled.___m_Ptr_0)));
	unmarshaled_m_Ptr_temp_0 = unmarshaled_m_Ptr_temp_0_temp;
	unmarshaled.set_m_Ptr_0(unmarshaled_m_Ptr_temp_0);
	unmarshaled.set_m_OnStateChanged_1(il2cpp_codegen_marshal_function_ptr_to_delegate<StateChanged_t2136737110>(marshaled.___m_OnStateChanged_1, StateChanged_t2136737110_il2cpp_TypeInfo_var));
}
// Conversion method for clean up from marshalling of: UnityEngine.CullingGroup
extern "C" void CullingGroup_t2096318768_marshal_pinvoke_cleanup(CullingGroup_t2096318768_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.CullingGroup
extern "C" void CullingGroup_t2096318768_marshal_com(const CullingGroup_t2096318768& unmarshaled, CullingGroup_t2096318768_marshaled_com& marshaled)
{
	marshaled.___m_Ptr_0 = reinterpret_cast<intptr_t>((unmarshaled.get_m_Ptr_0()).get_m_value_0());
	marshaled.___m_OnStateChanged_1 = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(unmarshaled.get_m_OnStateChanged_1()));
}
extern "C" void CullingGroup_t2096318768_marshal_com_back(const CullingGroup_t2096318768_marshaled_com& marshaled, CullingGroup_t2096318768& unmarshaled)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CullingGroup_t2096318768_com_FromNativeMethodDefinition_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	IntPtr_t unmarshaled_m_Ptr_temp_0;
	memset(&unmarshaled_m_Ptr_temp_0, 0, sizeof(unmarshaled_m_Ptr_temp_0));
	IntPtr_t unmarshaled_m_Ptr_temp_0_temp;
	unmarshaled_m_Ptr_temp_0_temp.set_m_value_0(reinterpret_cast<void*>((intptr_t)(marshaled.___m_Ptr_0)));
	unmarshaled_m_Ptr_temp_0 = unmarshaled_m_Ptr_temp_0_temp;
	unmarshaled.set_m_Ptr_0(unmarshaled_m_Ptr_temp_0);
	unmarshaled.set_m_OnStateChanged_1(il2cpp_codegen_marshal_function_ptr_to_delegate<StateChanged_t2136737110>(marshaled.___m_OnStateChanged_1, StateChanged_t2136737110_il2cpp_TypeInfo_var));
}
// Conversion method for clean up from marshalling of: UnityEngine.CullingGroup
extern "C" void CullingGroup_t2096318768_marshal_com_cleanup(CullingGroup_t2096318768_marshaled_com& marshaled)
{
}
// System.Void UnityEngine.CullingGroup::Finalize()
extern "C"  void CullingGroup_Finalize_m3771988052 (CullingGroup_t2096318768 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CullingGroup_Finalize_m3771988052_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Exception_t1436737249 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1436737249 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
	}

IL_0001:
	try
	{ // begin try (depth: 1)
		{
			IntPtr_t L_0 = __this->get_m_Ptr_0();
			IntPtr_t L_1 = ((IntPtr_t_StaticFields*)IntPtr_t_il2cpp_TypeInfo_var->static_fields)->get_Zero_1();
			bool L_2 = IntPtr_op_Inequality_m3063970704(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
			if (!L_2)
			{
				goto IL_001e;
			}
		}

IL_0016:
		{
			CullingGroup_FinalizerFailure_m1971354910(__this, /*hidden argument*/NULL);
		}

IL_001e:
		{
			IL2CPP_LEAVE(0x2A, FINALLY_0023);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1436737249 *)e.ex;
		goto FINALLY_0023;
	}

FINALLY_0023:
	{ // begin finally (depth: 1)
		Object_Finalize_m3076187857(__this, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(35)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(35)
	{
		IL2CPP_JUMP_TBL(0x2A, IL_002a)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1436737249 *)
	}

IL_002a:
	{
		return;
	}
}
// System.Void UnityEngine.CullingGroup::Dispose()
extern "C"  void CullingGroup_Dispose_m1918394814 (CullingGroup_t2096318768 * __this, const MethodInfo* method)
{
	typedef void (*CullingGroup_Dispose_m1918394814_ftn) (CullingGroup_t2096318768 *);
	static CullingGroup_Dispose_m1918394814_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (CullingGroup_Dispose_m1918394814_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.CullingGroup::Dispose()");
	_il2cpp_icall_func(__this);
}
// System.Void UnityEngine.CullingGroup::SendEvents(UnityEngine.CullingGroup,System.IntPtr,System.Int32)
extern "C"  void CullingGroup_SendEvents_m435733983 (Il2CppObject * __this /* static, unused */, CullingGroup_t2096318768 * ___cullingGroup0, IntPtr_t ___eventsPtr1, int32_t ___count2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (CullingGroup_SendEvents_m435733983_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	CullingGroupEvent_t1722745023 * V_0 = NULL;
	int32_t V_1 = 0;
	{
		void* L_0 = IntPtr_ToPointer_m4157623054((&___eventsPtr1), /*hidden argument*/NULL);
		V_0 = (CullingGroupEvent_t1722745023 *)L_0;
		CullingGroup_t2096318768 * L_1 = ___cullingGroup0;
		NullCheck(L_1);
		StateChanged_t2136737110 * L_2 = L_1->get_m_OnStateChanged_1();
		if (L_2)
		{
			goto IL_0019;
		}
	}
	{
		goto IL_0046;
	}

IL_0019:
	{
		V_1 = 0;
		goto IL_003f;
	}

IL_0020:
	{
		CullingGroup_t2096318768 * L_3 = ___cullingGroup0;
		NullCheck(L_3);
		StateChanged_t2136737110 * L_4 = L_3->get_m_OnStateChanged_1();
		CullingGroupEvent_t1722745023 * L_5 = V_0;
		int32_t L_6 = V_1;
		uint32_t L_7 = il2cpp_codegen_sizeof(CullingGroupEvent_t1722745023_il2cpp_TypeInfo_var);
		NullCheck(L_4);
		StateChanged_Invoke_m899175134(L_4, (*(CullingGroupEvent_t1722745023 *)((CullingGroupEvent_t1722745023 *)((intptr_t)L_5+(intptr_t)((intptr_t)((intptr_t)(((intptr_t)L_6))*(int32_t)L_7))))), /*hidden argument*/NULL);
		int32_t L_8 = V_1;
		V_1 = ((int32_t)((int32_t)L_8+(int32_t)1));
	}

IL_003f:
	{
		int32_t L_9 = V_1;
		int32_t L_10 = ___count2;
		if ((((int32_t)L_9) < ((int32_t)L_10)))
		{
			goto IL_0020;
		}
	}

IL_0046:
	{
		return;
	}
}
// System.Void UnityEngine.CullingGroup::FinalizerFailure()
extern "C"  void CullingGroup_FinalizerFailure_m1971354910 (CullingGroup_t2096318768 * __this, const MethodInfo* method)
{
	typedef void (*CullingGroup_FinalizerFailure_m1971354910_ftn) (CullingGroup_t2096318768 *);
	static CullingGroup_FinalizerFailure_m1971354910_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (CullingGroup_FinalizerFailure_m1971354910_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.CullingGroup::FinalizerFailure()");
	_il2cpp_icall_func(__this);
}
extern "C"  void DelegatePInvokeWrapper_StateChanged_t2136737110 (StateChanged_t2136737110 * __this, CullingGroupEvent_t1722745023  ___sphere0, const MethodInfo* method)
{
	typedef void (STDCALL *PInvokeFunc)(CullingGroupEvent_t1722745023 );
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(((Il2CppDelegate*)__this)->method->methodPointer);

	// Native function invocation
	il2cppPInvokeFunc(___sphere0);

}
// System.Void UnityEngine.CullingGroup/StateChanged::.ctor(System.Object,System.IntPtr)
extern "C"  void StateChanged__ctor_m1361435477 (StateChanged_t2136737110 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void UnityEngine.CullingGroup/StateChanged::Invoke(UnityEngine.CullingGroupEvent)
extern "C"  void StateChanged_Invoke_m899175134 (StateChanged_t2136737110 * __this, CullingGroupEvent_t1722745023  ___sphere0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		StateChanged_Invoke_m899175134((StateChanged_t2136737110 *)__this->get_prev_9(),___sphere0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, CullingGroupEvent_t1722745023  ___sphere0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___sphere0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, CullingGroupEvent_t1722745023  ___sphere0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___sphere0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult UnityEngine.CullingGroup/StateChanged::BeginInvoke(UnityEngine.CullingGroupEvent,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * StateChanged_BeginInvoke_m1110082402 (StateChanged_t2136737110 * __this, CullingGroupEvent_t1722745023  ___sphere0, AsyncCallback_t3962456242 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (StateChanged_BeginInvoke_m1110082402_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(CullingGroupEvent_t1722745023_il2cpp_TypeInfo_var, &___sphere0);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void UnityEngine.CullingGroup/StateChanged::EndInvoke(System.IAsyncResult)
extern "C"  void StateChanged_EndInvoke_m1411426168 (StateChanged_t2136737110 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// UnityEngine.CursorLockMode UnityEngine.Cursor::get_lockState()
extern "C"  int32_t Cursor_get_lockState_m1991532087 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	typedef int32_t (*Cursor_get_lockState_m1991532087_ftn) ();
	static Cursor_get_lockState_m1991532087_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Cursor_get_lockState_m1991532087_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Cursor::get_lockState()");
	return _il2cpp_icall_func();
}
// System.Void UnityEngine.Cursor::set_lockState(UnityEngine.CursorLockMode)
extern "C"  void Cursor_set_lockState_m3442434705 (Il2CppObject * __this /* static, unused */, int32_t ___value0, const MethodInfo* method)
{
	typedef void (*Cursor_set_lockState_m3442434705_ftn) (int32_t);
	static Cursor_set_lockState_m3442434705_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Cursor_set_lockState_m3442434705_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Cursor::set_lockState(UnityEngine.CursorLockMode)");
	_il2cpp_icall_func(___value0);
}
// System.Void UnityEngine.CustomYieldInstruction::.ctor()
extern "C"  void CustomYieldInstruction__ctor_m4013793876 (CustomYieldInstruction_t1895667560 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m297566312(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object UnityEngine.CustomYieldInstruction::get_Current()
extern "C"  Il2CppObject * CustomYieldInstruction_get_Current_m544197713 (CustomYieldInstruction_t1895667560 * __this, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	{
		V_0 = NULL;
		goto IL_0008;
	}

IL_0008:
	{
		Il2CppObject * L_0 = V_0;
		return L_0;
	}
}
// System.Boolean UnityEngine.CustomYieldInstruction::MoveNext()
extern "C"  bool CustomYieldInstruction_MoveNext_m2784850646 (CustomYieldInstruction_t1895667560 * __this, const MethodInfo* method)
{
	bool V_0 = false;
	{
		bool L_0 = VirtFuncInvoker0< bool >::Invoke(7 /* System.Boolean UnityEngine.CustomYieldInstruction::get_keepWaiting() */, __this);
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		bool L_1 = V_0;
		return L_1;
	}
}
// System.Void UnityEngine.CustomYieldInstruction::Reset()
extern "C"  void CustomYieldInstruction_Reset_m3921443893 (CustomYieldInstruction_t1895667560 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// UnityEngine.ILogger UnityEngine.Debug::get_logger()
extern "C"  Il2CppObject * Debug_get_logger_m1300688786 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Debug_get_logger_m1300688786_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Il2CppObject * V_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3317548046_il2cpp_TypeInfo_var);
		Il2CppObject * L_0 = ((Debug_t3317548046_StaticFields*)Debug_t3317548046_il2cpp_TypeInfo_var->static_fields)->get_s_Logger_0();
		V_0 = L_0;
		goto IL_000c;
	}

IL_000c:
	{
		Il2CppObject * L_1 = V_0;
		return L_1;
	}
}
// System.Void UnityEngine.Debug::Log(System.Object)
extern "C"  void Debug_Log_m1780991845 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___message0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Debug_Log_m1780991845_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3317548046_il2cpp_TypeInfo_var);
		Il2CppObject * L_0 = Debug_get_logger_m1300688786(NULL /*static, unused*/, /*hidden argument*/NULL);
		Il2CppObject * L_1 = ___message0;
		NullCheck(L_0);
		InterfaceActionInvoker2< int32_t, Il2CppObject * >::Invoke(0 /* System.Void UnityEngine.ILogger::Log(UnityEngine.LogType,System.Object) */, ILogger_t2607134790_il2cpp_TypeInfo_var, L_0, 3, L_1);
		return;
	}
}
// System.Void UnityEngine.Debug::LogError(System.Object)
extern "C"  void Debug_LogError_m2059623341 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___message0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Debug_LogError_m2059623341_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3317548046_il2cpp_TypeInfo_var);
		Il2CppObject * L_0 = Debug_get_logger_m1300688786(NULL /*static, unused*/, /*hidden argument*/NULL);
		Il2CppObject * L_1 = ___message0;
		NullCheck(L_0);
		InterfaceActionInvoker2< int32_t, Il2CppObject * >::Invoke(0 /* System.Void UnityEngine.ILogger::Log(UnityEngine.LogType,System.Object) */, ILogger_t2607134790_il2cpp_TypeInfo_var, L_0, 0, L_1);
		return;
	}
}
// System.Void UnityEngine.Debug::LogError(System.Object,UnityEngine.Object)
extern "C"  void Debug_LogError_m3356949141 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___message0, Object_t631007953 * ___context1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Debug_LogError_m3356949141_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3317548046_il2cpp_TypeInfo_var);
		Il2CppObject * L_0 = Debug_get_logger_m1300688786(NULL /*static, unused*/, /*hidden argument*/NULL);
		Il2CppObject * L_1 = ___message0;
		Object_t631007953 * L_2 = ___context1;
		NullCheck(L_0);
		InterfaceActionInvoker3< int32_t, Il2CppObject *, Object_t631007953 * >::Invoke(1 /* System.Void UnityEngine.ILogger::Log(UnityEngine.LogType,System.Object,UnityEngine.Object) */, ILogger_t2607134790_il2cpp_TypeInfo_var, L_0, 0, L_1, L_2);
		return;
	}
}
// System.Void UnityEngine.Debug::LogErrorFormat(UnityEngine.Object,System.String,System.Object[])
extern "C"  void Debug_LogErrorFormat_m4099340697 (Il2CppObject * __this /* static, unused */, Object_t631007953 * ___context0, String_t* ___format1, ObjectU5BU5D_t2843939325* ___args2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Debug_LogErrorFormat_m4099340697_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3317548046_il2cpp_TypeInfo_var);
		Il2CppObject * L_0 = Debug_get_logger_m1300688786(NULL /*static, unused*/, /*hidden argument*/NULL);
		Object_t631007953 * L_1 = ___context0;
		String_t* L_2 = ___format1;
		ObjectU5BU5D_t2843939325* L_3 = ___args2;
		NullCheck(L_0);
		InterfaceActionInvoker4< int32_t, Object_t631007953 *, String_t*, ObjectU5BU5D_t2843939325* >::Invoke(0 /* System.Void UnityEngine.ILogHandler::LogFormat(UnityEngine.LogType,UnityEngine.Object,System.String,System.Object[]) */, ILogHandler_t2464711877_il2cpp_TypeInfo_var, L_0, 0, L_1, L_2, L_3);
		return;
	}
}
// System.Void UnityEngine.Debug::LogException(System.Exception)
extern "C"  void Debug_LogException_m3660909244 (Il2CppObject * __this /* static, unused */, Exception_t1436737249 * ___exception0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Debug_LogException_m3660909244_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3317548046_il2cpp_TypeInfo_var);
		Il2CppObject * L_0 = Debug_get_logger_m1300688786(NULL /*static, unused*/, /*hidden argument*/NULL);
		Exception_t1436737249 * L_1 = ___exception0;
		NullCheck(L_0);
		InterfaceActionInvoker2< Exception_t1436737249 *, Object_t631007953 * >::Invoke(1 /* System.Void UnityEngine.ILogHandler::LogException(System.Exception,UnityEngine.Object) */, ILogHandler_t2464711877_il2cpp_TypeInfo_var, L_0, L_1, (Object_t631007953 *)NULL);
		return;
	}
}
// System.Void UnityEngine.Debug::LogException(System.Exception,UnityEngine.Object)
extern "C"  void Debug_LogException_m2155052615 (Il2CppObject * __this /* static, unused */, Exception_t1436737249 * ___exception0, Object_t631007953 * ___context1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Debug_LogException_m2155052615_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3317548046_il2cpp_TypeInfo_var);
		Il2CppObject * L_0 = Debug_get_logger_m1300688786(NULL /*static, unused*/, /*hidden argument*/NULL);
		Exception_t1436737249 * L_1 = ___exception0;
		Object_t631007953 * L_2 = ___context1;
		NullCheck(L_0);
		InterfaceActionInvoker2< Exception_t1436737249 *, Object_t631007953 * >::Invoke(1 /* System.Void UnityEngine.ILogHandler::LogException(System.Exception,UnityEngine.Object) */, ILogHandler_t2464711877_il2cpp_TypeInfo_var, L_0, L_1, L_2);
		return;
	}
}
// System.Void UnityEngine.Debug::LogWarning(System.Object)
extern "C"  void Debug_LogWarning_m3661709751 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___message0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Debug_LogWarning_m3661709751_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3317548046_il2cpp_TypeInfo_var);
		Il2CppObject * L_0 = Debug_get_logger_m1300688786(NULL /*static, unused*/, /*hidden argument*/NULL);
		Il2CppObject * L_1 = ___message0;
		NullCheck(L_0);
		InterfaceActionInvoker2< int32_t, Il2CppObject * >::Invoke(0 /* System.Void UnityEngine.ILogger::Log(UnityEngine.LogType,System.Object) */, ILogger_t2607134790_il2cpp_TypeInfo_var, L_0, 2, L_1);
		return;
	}
}
// System.Void UnityEngine.Debug::LogWarning(System.Object,UnityEngine.Object)
extern "C"  void Debug_LogWarning_m218105588 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___message0, Object_t631007953 * ___context1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Debug_LogWarning_m218105588_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3317548046_il2cpp_TypeInfo_var);
		Il2CppObject * L_0 = Debug_get_logger_m1300688786(NULL /*static, unused*/, /*hidden argument*/NULL);
		Il2CppObject * L_1 = ___message0;
		Object_t631007953 * L_2 = ___context1;
		NullCheck(L_0);
		InterfaceActionInvoker3< int32_t, Il2CppObject *, Object_t631007953 * >::Invoke(1 /* System.Void UnityEngine.ILogger::Log(UnityEngine.LogType,System.Object,UnityEngine.Object) */, ILogger_t2607134790_il2cpp_TypeInfo_var, L_0, 2, L_1, L_2);
		return;
	}
}
// System.Void UnityEngine.Debug::LogWarningFormat(UnityEngine.Object,System.String,System.Object[])
extern "C"  void Debug_LogWarningFormat_m3804720959 (Il2CppObject * __this /* static, unused */, Object_t631007953 * ___context0, String_t* ___format1, ObjectU5BU5D_t2843939325* ___args2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Debug_LogWarningFormat_m3804720959_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t3317548046_il2cpp_TypeInfo_var);
		Il2CppObject * L_0 = Debug_get_logger_m1300688786(NULL /*static, unused*/, /*hidden argument*/NULL);
		Object_t631007953 * L_1 = ___context0;
		String_t* L_2 = ___format1;
		ObjectU5BU5D_t2843939325* L_3 = ___args2;
		NullCheck(L_0);
		InterfaceActionInvoker4< int32_t, Object_t631007953 *, String_t*, ObjectU5BU5D_t2843939325* >::Invoke(0 /* System.Void UnityEngine.ILogHandler::LogFormat(UnityEngine.LogType,UnityEngine.Object,System.String,System.Object[]) */, ILogHandler_t2464711877_il2cpp_TypeInfo_var, L_0, 2, L_1, L_2, L_3);
		return;
	}
}
// System.Void UnityEngine.Debug::.cctor()
extern "C"  void Debug__cctor_m343362545 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Debug__cctor_m343362545_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		DebugLogHandler_t826086171 * L_0 = (DebugLogHandler_t826086171 *)il2cpp_codegen_object_new(DebugLogHandler_t826086171_il2cpp_TypeInfo_var);
		DebugLogHandler__ctor_m1011039465(L_0, /*hidden argument*/NULL);
		Logger_t274032455 * L_1 = (Logger_t274032455 *)il2cpp_codegen_object_new(Logger_t274032455_il2cpp_TypeInfo_var);
		Logger__ctor_m2177534208(L_1, L_0, /*hidden argument*/NULL);
		((Debug_t3317548046_StaticFields*)Debug_t3317548046_il2cpp_TypeInfo_var->static_fields)->set_s_Logger_0(L_1);
		return;
	}
}
// System.Void UnityEngine.DebugLogHandler::.ctor()
extern "C"  void DebugLogHandler__ctor_m1011039465 (DebugLogHandler_t826086171 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m297566312(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.DebugLogHandler::Internal_Log(UnityEngine.LogType,System.String,UnityEngine.Object)
extern "C"  void DebugLogHandler_Internal_Log_m1402568625 (Il2CppObject * __this /* static, unused */, int32_t ___level0, String_t* ___msg1, Object_t631007953 * ___obj2, const MethodInfo* method)
{
	typedef void (*DebugLogHandler_Internal_Log_m1402568625_ftn) (int32_t, String_t*, Object_t631007953 *);
	static DebugLogHandler_Internal_Log_m1402568625_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (DebugLogHandler_Internal_Log_m1402568625_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.DebugLogHandler::Internal_Log(UnityEngine.LogType,System.String,UnityEngine.Object)");
	_il2cpp_icall_func(___level0, ___msg1, ___obj2);
}
// System.Void UnityEngine.DebugLogHandler::Internal_LogException(System.Exception,UnityEngine.Object)
extern "C"  void DebugLogHandler_Internal_LogException_m2644579063 (Il2CppObject * __this /* static, unused */, Exception_t1436737249 * ___exception0, Object_t631007953 * ___obj1, const MethodInfo* method)
{
	typedef void (*DebugLogHandler_Internal_LogException_m2644579063_ftn) (Exception_t1436737249 *, Object_t631007953 *);
	static DebugLogHandler_Internal_LogException_m2644579063_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (DebugLogHandler_Internal_LogException_m2644579063_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.DebugLogHandler::Internal_LogException(System.Exception,UnityEngine.Object)");
	_il2cpp_icall_func(___exception0, ___obj1);
}
// System.Void UnityEngine.DebugLogHandler::LogFormat(UnityEngine.LogType,UnityEngine.Object,System.String,System.Object[])
extern "C"  void DebugLogHandler_LogFormat_m1163074747 (DebugLogHandler_t826086171 * __this, int32_t ___logType0, Object_t631007953 * ___context1, String_t* ___format2, ObjectU5BU5D_t2843939325* ___args3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DebugLogHandler_LogFormat_m1163074747_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = ___logType0;
		String_t* L_1 = ___format2;
		ObjectU5BU5D_t2843939325* L_2 = ___args3;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_3 = String_Format_m630303134(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		Object_t631007953 * L_4 = ___context1;
		DebugLogHandler_Internal_Log_m1402568625(NULL /*static, unused*/, L_0, L_3, L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.DebugLogHandler::LogException(System.Exception,UnityEngine.Object)
extern "C"  void DebugLogHandler_LogException_m1664185363 (DebugLogHandler_t826086171 * __this, Exception_t1436737249 * ___exception0, Object_t631007953 * ___context1, const MethodInfo* method)
{
	{
		Exception_t1436737249 * L_0 = ___exception0;
		Object_t631007953 * L_1 = ___context1;
		DebugLogHandler_Internal_LogException_m2644579063(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Int32 UnityEngine.DefaultExecutionOrder::get_order()
extern "C"  int32_t DefaultExecutionOrder_get_order_m2144583152 (DefaultExecutionOrder_t3059642329 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_U3CorderU3Ek__BackingField_0();
		V_0 = L_0;
		goto IL_000c;
	}

IL_000c:
	{
		int32_t L_1 = V_0;
		return L_1;
	}
}
// System.Void UnityEngine.DisallowMultipleComponent::.ctor()
extern "C"  void DisallowMultipleComponent__ctor_m1904568243 (DisallowMultipleComponent_t1422053217 * __this, const MethodInfo* method)
{
	{
		Attribute__ctor_m1529526131(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Display::.ctor()
extern "C"  void Display__ctor_m2645739024 (Display_t1387065949 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m297566312(__this, /*hidden argument*/NULL);
		IntPtr_t L_0;
		memset(&L_0, 0, sizeof(L_0));
		IntPtr__ctor_m987082960(&L_0, 0, /*hidden argument*/NULL);
		__this->set_nativeDisplay_0(L_0);
		return;
	}
}
// System.Void UnityEngine.Display::.ctor(System.IntPtr)
extern "C"  void Display__ctor_m2273944945 (Display_t1387065949 * __this, IntPtr_t ___nativeDisplay0, const MethodInfo* method)
{
	{
		Object__ctor_m297566312(__this, /*hidden argument*/NULL);
		IntPtr_t L_0 = ___nativeDisplay0;
		__this->set_nativeDisplay_0(L_0);
		return;
	}
}
// System.Int32 UnityEngine.Display::get_renderingWidth()
extern "C"  int32_t Display_get_renderingWidth_m2897119351 (Display_t1387065949 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Display_get_renderingWidth_m2897119351_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	{
		V_0 = 0;
		V_1 = 0;
		IntPtr_t L_0 = __this->get_nativeDisplay_0();
		IL2CPP_RUNTIME_CLASS_INIT(Display_t1387065949_il2cpp_TypeInfo_var);
		Display_GetRenderingExtImpl_m545713866(NULL /*static, unused*/, L_0, (&V_0), (&V_1), /*hidden argument*/NULL);
		int32_t L_1 = V_0;
		V_2 = L_1;
		goto IL_001b;
	}

IL_001b:
	{
		int32_t L_2 = V_2;
		return L_2;
	}
}
// System.Int32 UnityEngine.Display::get_renderingHeight()
extern "C"  int32_t Display_get_renderingHeight_m4183585850 (Display_t1387065949 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Display_get_renderingHeight_m4183585850_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	{
		V_0 = 0;
		V_1 = 0;
		IntPtr_t L_0 = __this->get_nativeDisplay_0();
		IL2CPP_RUNTIME_CLASS_INIT(Display_t1387065949_il2cpp_TypeInfo_var);
		Display_GetRenderingExtImpl_m545713866(NULL /*static, unused*/, L_0, (&V_0), (&V_1), /*hidden argument*/NULL);
		int32_t L_1 = V_1;
		V_2 = L_1;
		goto IL_001b;
	}

IL_001b:
	{
		int32_t L_2 = V_2;
		return L_2;
	}
}
// System.Int32 UnityEngine.Display::get_systemWidth()
extern "C"  int32_t Display_get_systemWidth_m2297141419 (Display_t1387065949 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Display_get_systemWidth_m2297141419_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	{
		V_0 = 0;
		V_1 = 0;
		IntPtr_t L_0 = __this->get_nativeDisplay_0();
		IL2CPP_RUNTIME_CLASS_INIT(Display_t1387065949_il2cpp_TypeInfo_var);
		Display_GetSystemExtImpl_m1142438950(NULL /*static, unused*/, L_0, (&V_0), (&V_1), /*hidden argument*/NULL);
		int32_t L_1 = V_0;
		V_2 = L_1;
		goto IL_001b;
	}

IL_001b:
	{
		int32_t L_2 = V_2;
		return L_2;
	}
}
// System.Int32 UnityEngine.Display::get_systemHeight()
extern "C"  int32_t Display_get_systemHeight_m2647631588 (Display_t1387065949 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Display_get_systemHeight_m2647631588_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	{
		V_0 = 0;
		V_1 = 0;
		IntPtr_t L_0 = __this->get_nativeDisplay_0();
		IL2CPP_RUNTIME_CLASS_INIT(Display_t1387065949_il2cpp_TypeInfo_var);
		Display_GetSystemExtImpl_m1142438950(NULL /*static, unused*/, L_0, (&V_0), (&V_1), /*hidden argument*/NULL);
		int32_t L_1 = V_1;
		V_2 = L_1;
		goto IL_001b;
	}

IL_001b:
	{
		int32_t L_2 = V_2;
		return L_2;
	}
}
// UnityEngine.Vector3 UnityEngine.Display::RelativeMouseAt(UnityEngine.Vector3)
extern "C"  Vector3_t3722313464  Display_RelativeMouseAt_m3342735020 (Il2CppObject * __this /* static, unused */, Vector3_t3722313464  ___inputMouseCoordinates0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Display_RelativeMouseAt_m3342735020_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t3722313464  V_0;
	memset(&V_0, 0, sizeof(V_0));
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	Vector3_t3722313464  V_5;
	memset(&V_5, 0, sizeof(V_5));
	{
		V_1 = 0;
		V_2 = 0;
		float L_0 = (&___inputMouseCoordinates0)->get_x_1();
		V_3 = (((int32_t)((int32_t)L_0)));
		float L_1 = (&___inputMouseCoordinates0)->get_y_2();
		V_4 = (((int32_t)((int32_t)L_1)));
		int32_t L_2 = V_3;
		int32_t L_3 = V_4;
		IL2CPP_RUNTIME_CLASS_INIT(Display_t1387065949_il2cpp_TypeInfo_var);
		int32_t L_4 = Display_RelativeMouseAtImpl_m375710204(NULL /*static, unused*/, L_2, L_3, (&V_1), (&V_2), /*hidden argument*/NULL);
		(&V_0)->set_z_3((((float)((float)L_4))));
		int32_t L_5 = V_1;
		(&V_0)->set_x_1((((float)((float)L_5))));
		int32_t L_6 = V_2;
		(&V_0)->set_y_2((((float)((float)L_6))));
		Vector3_t3722313464  L_7 = V_0;
		V_5 = L_7;
		goto IL_0046;
	}

IL_0046:
	{
		Vector3_t3722313464  L_8 = V_5;
		return L_8;
	}
}
// System.Void UnityEngine.Display::RecreateDisplayList(System.IntPtr[])
extern "C"  void Display_RecreateDisplayList_m48792512 (Il2CppObject * __this /* static, unused */, IntPtrU5BU5D_t4013366056* ___nativeDisplay0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Display_RecreateDisplayList_m48792512_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		IntPtrU5BU5D_t4013366056* L_0 = ___nativeDisplay0;
		NullCheck(L_0);
		IL2CPP_RUNTIME_CLASS_INIT(Display_t1387065949_il2cpp_TypeInfo_var);
		((Display_t1387065949_StaticFields*)Display_t1387065949_il2cpp_TypeInfo_var->static_fields)->set_displays_1(((DisplayU5BU5D_t103034768*)SZArrayNew(DisplayU5BU5D_t103034768_il2cpp_TypeInfo_var, (uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_0)->max_length)))))));
		V_0 = 0;
		goto IL_0028;
	}

IL_0015:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Display_t1387065949_il2cpp_TypeInfo_var);
		DisplayU5BU5D_t103034768* L_1 = ((Display_t1387065949_StaticFields*)Display_t1387065949_il2cpp_TypeInfo_var->static_fields)->get_displays_1();
		int32_t L_2 = V_0;
		IntPtrU5BU5D_t4013366056* L_3 = ___nativeDisplay0;
		int32_t L_4 = V_0;
		NullCheck(L_3);
		int32_t L_5 = L_4;
		IntPtr_t L_6 = (L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_5));
		Display_t1387065949 * L_7 = (Display_t1387065949 *)il2cpp_codegen_object_new(Display_t1387065949_il2cpp_TypeInfo_var);
		Display__ctor_m2273944945(L_7, L_6, /*hidden argument*/NULL);
		NullCheck(L_1);
		ArrayElementTypeCheck (L_1, L_7);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(L_2), (Display_t1387065949 *)L_7);
		int32_t L_8 = V_0;
		V_0 = ((int32_t)((int32_t)L_8+(int32_t)1));
	}

IL_0028:
	{
		int32_t L_9 = V_0;
		IntPtrU5BU5D_t4013366056* L_10 = ___nativeDisplay0;
		NullCheck(L_10);
		if ((((int32_t)L_9) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_10)->max_length)))))))
		{
			goto IL_0015;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Display_t1387065949_il2cpp_TypeInfo_var);
		DisplayU5BU5D_t103034768* L_11 = ((Display_t1387065949_StaticFields*)Display_t1387065949_il2cpp_TypeInfo_var->static_fields)->get_displays_1();
		NullCheck(L_11);
		int32_t L_12 = 0;
		Display_t1387065949 * L_13 = (L_11)->GetAt(static_cast<il2cpp_array_size_t>(L_12));
		((Display_t1387065949_StaticFields*)Display_t1387065949_il2cpp_TypeInfo_var->static_fields)->set__mainDisplay_2(L_13);
		return;
	}
}
// System.Void UnityEngine.Display::FireDisplaysUpdated()
extern "C"  void Display_FireDisplaysUpdated_m1504381846 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Display_FireDisplaysUpdated_m1504381846_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Display_t1387065949_il2cpp_TypeInfo_var);
		DisplaysUpdatedDelegate_t51287044 * L_0 = ((Display_t1387065949_StaticFields*)Display_t1387065949_il2cpp_TypeInfo_var->static_fields)->get_onDisplaysUpdated_3();
		if (!L_0)
		{
			goto IL_0015;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Display_t1387065949_il2cpp_TypeInfo_var);
		DisplaysUpdatedDelegate_t51287044 * L_1 = ((Display_t1387065949_StaticFields*)Display_t1387065949_il2cpp_TypeInfo_var->static_fields)->get_onDisplaysUpdated_3();
		NullCheck(L_1);
		DisplaysUpdatedDelegate_Invoke_m3670286313(L_1, /*hidden argument*/NULL);
	}

IL_0015:
	{
		return;
	}
}
// System.Void UnityEngine.Display::GetSystemExtImpl(System.IntPtr,System.Int32&,System.Int32&)
extern "C"  void Display_GetSystemExtImpl_m1142438950 (Il2CppObject * __this /* static, unused */, IntPtr_t ___nativeDisplay0, int32_t* ___w1, int32_t* ___h2, const MethodInfo* method)
{
	typedef void (*Display_GetSystemExtImpl_m1142438950_ftn) (IntPtr_t, int32_t*, int32_t*);
	static Display_GetSystemExtImpl_m1142438950_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Display_GetSystemExtImpl_m1142438950_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Display::GetSystemExtImpl(System.IntPtr,System.Int32&,System.Int32&)");
	_il2cpp_icall_func(___nativeDisplay0, ___w1, ___h2);
}
// System.Void UnityEngine.Display::GetRenderingExtImpl(System.IntPtr,System.Int32&,System.Int32&)
extern "C"  void Display_GetRenderingExtImpl_m545713866 (Il2CppObject * __this /* static, unused */, IntPtr_t ___nativeDisplay0, int32_t* ___w1, int32_t* ___h2, const MethodInfo* method)
{
	typedef void (*Display_GetRenderingExtImpl_m545713866_ftn) (IntPtr_t, int32_t*, int32_t*);
	static Display_GetRenderingExtImpl_m545713866_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Display_GetRenderingExtImpl_m545713866_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Display::GetRenderingExtImpl(System.IntPtr,System.Int32&,System.Int32&)");
	_il2cpp_icall_func(___nativeDisplay0, ___w1, ___h2);
}
// System.Int32 UnityEngine.Display::RelativeMouseAtImpl(System.Int32,System.Int32,System.Int32&,System.Int32&)
extern "C"  int32_t Display_RelativeMouseAtImpl_m375710204 (Il2CppObject * __this /* static, unused */, int32_t ___x0, int32_t ___y1, int32_t* ___rx2, int32_t* ___ry3, const MethodInfo* method)
{
	typedef int32_t (*Display_RelativeMouseAtImpl_m375710204_ftn) (int32_t, int32_t, int32_t*, int32_t*);
	static Display_RelativeMouseAtImpl_m375710204_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Display_RelativeMouseAtImpl_m375710204_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Display::RelativeMouseAtImpl(System.Int32,System.Int32,System.Int32&,System.Int32&)");
	return _il2cpp_icall_func(___x0, ___y1, ___rx2, ___ry3);
}
// System.Void UnityEngine.Display::.cctor()
extern "C"  void Display__cctor_m625480064 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Display__cctor_m625480064_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		DisplayU5BU5D_t103034768* L_0 = ((DisplayU5BU5D_t103034768*)SZArrayNew(DisplayU5BU5D_t103034768_il2cpp_TypeInfo_var, (uint32_t)1));
		Display_t1387065949 * L_1 = (Display_t1387065949 *)il2cpp_codegen_object_new(Display_t1387065949_il2cpp_TypeInfo_var);
		Display__ctor_m2645739024(L_1, /*hidden argument*/NULL);
		NullCheck(L_0);
		ArrayElementTypeCheck (L_0, L_1);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (Display_t1387065949 *)L_1);
		((Display_t1387065949_StaticFields*)Display_t1387065949_il2cpp_TypeInfo_var->static_fields)->set_displays_1(L_0);
		DisplayU5BU5D_t103034768* L_2 = ((Display_t1387065949_StaticFields*)Display_t1387065949_il2cpp_TypeInfo_var->static_fields)->get_displays_1();
		NullCheck(L_2);
		int32_t L_3 = 0;
		Display_t1387065949 * L_4 = (L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_3));
		((Display_t1387065949_StaticFields*)Display_t1387065949_il2cpp_TypeInfo_var->static_fields)->set__mainDisplay_2(L_4);
		((Display_t1387065949_StaticFields*)Display_t1387065949_il2cpp_TypeInfo_var->static_fields)->set_onDisplaysUpdated_3((DisplaysUpdatedDelegate_t51287044 *)NULL);
		return;
	}
}
extern "C"  void DelegatePInvokeWrapper_DisplaysUpdatedDelegate_t51287044 (DisplaysUpdatedDelegate_t51287044 * __this, const MethodInfo* method)
{
	typedef void (STDCALL *PInvokeFunc)();
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(((Il2CppDelegate*)__this)->method->methodPointer);

	// Native function invocation
	il2cppPInvokeFunc();

}
// System.Void UnityEngine.Display/DisplaysUpdatedDelegate::.ctor(System.Object,System.IntPtr)
extern "C"  void DisplaysUpdatedDelegate__ctor_m274078413 (DisplaysUpdatedDelegate_t51287044 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void UnityEngine.Display/DisplaysUpdatedDelegate::Invoke()
extern "C"  void DisplaysUpdatedDelegate_Invoke_m3670286313 (DisplaysUpdatedDelegate_t51287044 * __this, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		DisplaysUpdatedDelegate_Invoke_m3670286313((DisplaysUpdatedDelegate_t51287044 *)__this->get_prev_9(), method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if ((__this->get_m_target_2() != NULL || MethodHasParameters((MethodInfo*)(__this->get_method_3().get_m_value_0()))) && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult UnityEngine.Display/DisplaysUpdatedDelegate::BeginInvoke(System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * DisplaysUpdatedDelegate_BeginInvoke_m3645854466 (DisplaysUpdatedDelegate_t51287044 * __this, AsyncCallback_t3962456242 * ___callback0, Il2CppObject * ___object1, const MethodInfo* method)
{
	void *__d_args[1] = {0};
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback0, (Il2CppObject*)___object1);
}
// System.Void UnityEngine.Display/DisplaysUpdatedDelegate::EndInvoke(System.IAsyncResult)
extern "C"  void DisplaysUpdatedDelegate_EndInvoke_m1178788972 (DisplaysUpdatedDelegate_t51287044 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void UnityEngine.DrivenRectTransformTracker::Add(UnityEngine.Object,UnityEngine.RectTransform,UnityEngine.DrivenTransformProperties)
extern "C"  void DrivenRectTransformTracker_Add_m4180584832 (DrivenRectTransformTracker_t2562230146 * __this, Object_t631007953 * ___driver0, RectTransform_t3704657025 * ___rectTransform1, int32_t ___drivenProperties2, const MethodInfo* method)
{
	{
		return;
	}
}
extern "C"  void DrivenRectTransformTracker_Add_m4180584832_AdjustorThunk (Il2CppObject * __this, Object_t631007953 * ___driver0, RectTransform_t3704657025 * ___rectTransform1, int32_t ___drivenProperties2, const MethodInfo* method)
{
	DrivenRectTransformTracker_t2562230146 * _thisAdjusted = reinterpret_cast<DrivenRectTransformTracker_t2562230146 *>(__this + 1);
	DrivenRectTransformTracker_Add_m4180584832(_thisAdjusted, ___driver0, ___rectTransform1, ___drivenProperties2, method);
}
// System.Void UnityEngine.DrivenRectTransformTracker::Clear()
extern "C"  void DrivenRectTransformTracker_Clear_m905140393 (DrivenRectTransformTracker_t2562230146 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
extern "C"  void DrivenRectTransformTracker_Clear_m905140393_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	DrivenRectTransformTracker_t2562230146 * _thisAdjusted = reinterpret_cast<DrivenRectTransformTracker_t2562230146 *>(__this + 1);
	DrivenRectTransformTracker_Clear_m905140393(_thisAdjusted, method);
}
// Conversion methods for marshalling of: UnityEngine.Event
extern "C" void Event_t2956885303_marshal_pinvoke(const Event_t2956885303& unmarshaled, Event_t2956885303_marshaled_pinvoke& marshaled)
{
	marshaled.___m_Ptr_0 = reinterpret_cast<intptr_t>((unmarshaled.get_m_Ptr_0()).get_m_value_0());
}
extern "C" void Event_t2956885303_marshal_pinvoke_back(const Event_t2956885303_marshaled_pinvoke& marshaled, Event_t2956885303& unmarshaled)
{
	IntPtr_t unmarshaled_m_Ptr_temp_0;
	memset(&unmarshaled_m_Ptr_temp_0, 0, sizeof(unmarshaled_m_Ptr_temp_0));
	IntPtr_t unmarshaled_m_Ptr_temp_0_temp;
	unmarshaled_m_Ptr_temp_0_temp.set_m_value_0(reinterpret_cast<void*>((intptr_t)(marshaled.___m_Ptr_0)));
	unmarshaled_m_Ptr_temp_0 = unmarshaled_m_Ptr_temp_0_temp;
	unmarshaled.set_m_Ptr_0(unmarshaled_m_Ptr_temp_0);
}
// Conversion method for clean up from marshalling of: UnityEngine.Event
extern "C" void Event_t2956885303_marshal_pinvoke_cleanup(Event_t2956885303_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.Event
extern "C" void Event_t2956885303_marshal_com(const Event_t2956885303& unmarshaled, Event_t2956885303_marshaled_com& marshaled)
{
	marshaled.___m_Ptr_0 = reinterpret_cast<intptr_t>((unmarshaled.get_m_Ptr_0()).get_m_value_0());
}
extern "C" void Event_t2956885303_marshal_com_back(const Event_t2956885303_marshaled_com& marshaled, Event_t2956885303& unmarshaled)
{
	IntPtr_t unmarshaled_m_Ptr_temp_0;
	memset(&unmarshaled_m_Ptr_temp_0, 0, sizeof(unmarshaled_m_Ptr_temp_0));
	IntPtr_t unmarshaled_m_Ptr_temp_0_temp;
	unmarshaled_m_Ptr_temp_0_temp.set_m_value_0(reinterpret_cast<void*>((intptr_t)(marshaled.___m_Ptr_0)));
	unmarshaled_m_Ptr_temp_0 = unmarshaled_m_Ptr_temp_0_temp;
	unmarshaled.set_m_Ptr_0(unmarshaled_m_Ptr_temp_0);
}
// Conversion method for clean up from marshalling of: UnityEngine.Event
extern "C" void Event_t2956885303_marshal_com_cleanup(Event_t2956885303_marshaled_com& marshaled)
{
}
// System.Void UnityEngine.Event::.ctor()
extern "C"  void Event__ctor_m2602111494 (Event_t2956885303 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m297566312(__this, /*hidden argument*/NULL);
		Event_Init_m3976072405(__this, 0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Event::.ctor(System.Int32)
extern "C"  void Event__ctor_m4084563805 (Event_t2956885303 * __this, int32_t ___displayIndex0, const MethodInfo* method)
{
	{
		Object__ctor_m297566312(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___displayIndex0;
		Event_Init_m3976072405(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Event::Finalize()
extern "C"  void Event_Finalize_m1421083088 (Event_t2956885303 * __this, const MethodInfo* method)
{
	Exception_t1436737249 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1436737249 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
	}

IL_0001:
	try
	{ // begin try (depth: 1)
		Event_Cleanup_m1044305745(__this, /*hidden argument*/NULL);
		IL2CPP_LEAVE(0x13, FINALLY_000c);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1436737249 *)e.ex;
		goto FINALLY_000c;
	}

FINALLY_000c:
	{ // begin finally (depth: 1)
		Object_Finalize_m3076187857(__this, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(12)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(12)
	{
		IL2CPP_JUMP_TBL(0x13, IL_0013)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1436737249 *)
	}

IL_0013:
	{
		return;
	}
}
// UnityEngine.Vector2 UnityEngine.Event::get_mousePosition()
extern "C"  Vector2_t2156229523  Event_get_mousePosition_m860577707 (Event_t2956885303 * __this, const MethodInfo* method)
{
	Vector2_t2156229523  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector2_t2156229523  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		Event_Internal_GetMousePosition_m2915150106(__this, (&V_0), /*hidden argument*/NULL);
		Vector2_t2156229523  L_0 = V_0;
		V_1 = L_0;
		goto IL_0010;
	}

IL_0010:
	{
		Vector2_t2156229523  L_1 = V_1;
		return L_1;
	}
}
// UnityEngine.Event UnityEngine.Event::get_current()
extern "C"  Event_t2956885303 * Event_get_current_m357379773 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Event_get_current_m357379773_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Event_t2956885303 * V_0 = NULL;
	{
		Event_t2956885303 * L_0 = ((Event_t2956885303_StaticFields*)Event_t2956885303_il2cpp_TypeInfo_var->static_fields)->get_s_Current_1();
		V_0 = L_0;
		goto IL_000c;
	}

IL_000c:
	{
		Event_t2956885303 * L_1 = V_0;
		return L_1;
	}
}
// System.Void UnityEngine.Event::Internal_MakeMasterEventCurrent(System.Int32)
extern "C"  void Event_Internal_MakeMasterEventCurrent_m1629006028 (Il2CppObject * __this /* static, unused */, int32_t ___displayIndex0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Event_Internal_MakeMasterEventCurrent_m1629006028_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Event_t2956885303 * L_0 = ((Event_t2956885303_StaticFields*)Event_t2956885303_il2cpp_TypeInfo_var->static_fields)->get_s_MasterEvent_2();
		if (L_0)
		{
			goto IL_0016;
		}
	}
	{
		int32_t L_1 = ___displayIndex0;
		Event_t2956885303 * L_2 = (Event_t2956885303 *)il2cpp_codegen_object_new(Event_t2956885303_il2cpp_TypeInfo_var);
		Event__ctor_m4084563805(L_2, L_1, /*hidden argument*/NULL);
		((Event_t2956885303_StaticFields*)Event_t2956885303_il2cpp_TypeInfo_var->static_fields)->set_s_MasterEvent_2(L_2);
	}

IL_0016:
	{
		Event_t2956885303 * L_3 = ((Event_t2956885303_StaticFields*)Event_t2956885303_il2cpp_TypeInfo_var->static_fields)->get_s_MasterEvent_2();
		int32_t L_4 = ___displayIndex0;
		NullCheck(L_3);
		Event_set_displayIndex_m1870302504(L_3, L_4, /*hidden argument*/NULL);
		Event_t2956885303 * L_5 = ((Event_t2956885303_StaticFields*)Event_t2956885303_il2cpp_TypeInfo_var->static_fields)->get_s_MasterEvent_2();
		((Event_t2956885303_StaticFields*)Event_t2956885303_il2cpp_TypeInfo_var->static_fields)->set_s_Current_1(L_5);
		Event_t2956885303 * L_6 = ((Event_t2956885303_StaticFields*)Event_t2956885303_il2cpp_TypeInfo_var->static_fields)->get_s_MasterEvent_2();
		NullCheck(L_6);
		IntPtr_t L_7 = L_6->get_m_Ptr_0();
		Event_Internal_SetNativeEvent_m4190336570(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean UnityEngine.Event::get_isKey()
extern "C"  bool Event_get_isKey_m2009856615 (Event_t2956885303 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	bool V_1 = false;
	int32_t G_B3_0 = 0;
	{
		int32_t L_0 = Event_get_type_m1523589438(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		int32_t L_1 = V_0;
		if ((((int32_t)L_1) == ((int32_t)4)))
		{
			goto IL_0015;
		}
	}
	{
		int32_t L_2 = V_0;
		G_B3_0 = ((((int32_t)L_2) == ((int32_t)5))? 1 : 0);
		goto IL_0016;
	}

IL_0015:
	{
		G_B3_0 = 1;
	}

IL_0016:
	{
		V_1 = (bool)G_B3_0;
		goto IL_001c;
	}

IL_001c:
	{
		bool L_3 = V_1;
		return L_3;
	}
}
// System.Boolean UnityEngine.Event::get_isMouse()
extern "C"  bool Event_get_isMouse_m598780014 (Event_t2956885303 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	bool V_1 = false;
	int32_t G_B8_0 = 0;
	{
		int32_t L_0 = Event_get_type_m1523589438(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		int32_t L_1 = V_0;
		if ((((int32_t)L_1) == ((int32_t)2)))
		{
			goto IL_003a;
		}
	}
	{
		int32_t L_2 = V_0;
		if (!L_2)
		{
			goto IL_003a;
		}
	}
	{
		int32_t L_3 = V_0;
		if ((((int32_t)L_3) == ((int32_t)1)))
		{
			goto IL_003a;
		}
	}
	{
		int32_t L_4 = V_0;
		if ((((int32_t)L_4) == ((int32_t)3)))
		{
			goto IL_003a;
		}
	}
	{
		int32_t L_5 = V_0;
		if ((((int32_t)L_5) == ((int32_t)((int32_t)16))))
		{
			goto IL_003a;
		}
	}
	{
		int32_t L_6 = V_0;
		if ((((int32_t)L_6) == ((int32_t)((int32_t)20))))
		{
			goto IL_003a;
		}
	}
	{
		int32_t L_7 = V_0;
		G_B8_0 = ((((int32_t)L_7) == ((int32_t)((int32_t)21)))? 1 : 0);
		goto IL_003b;
	}

IL_003a:
	{
		G_B8_0 = 1;
	}

IL_003b:
	{
		V_1 = (bool)G_B8_0;
		goto IL_0041;
	}

IL_0041:
	{
		bool L_8 = V_1;
		return L_8;
	}
}
// System.Int32 UnityEngine.Event::GetHashCode()
extern "C"  int32_t Event_GetHashCode_m2421473880 (Event_t2956885303 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	Vector2_t2156229523  V_1;
	memset(&V_1, 0, sizeof(V_1));
	int32_t V_2 = 0;
	{
		V_0 = 1;
		bool L_0 = Event_get_isKey_m2009856615(__this, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		int32_t L_1 = Event_get_keyCode_m3804184495(__this, /*hidden argument*/NULL);
		V_0 = (((int32_t)((uint16_t)L_1)));
	}

IL_0016:
	{
		bool L_2 = Event_get_isMouse_m598780014(__this, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0036;
		}
	}
	{
		Vector2_t2156229523  L_3 = Event_get_mousePosition_m860577707(__this, /*hidden argument*/NULL);
		V_1 = L_3;
		int32_t L_4 = Vector2_GetHashCode_m2690926524((&V_1), /*hidden argument*/NULL);
		V_0 = L_4;
	}

IL_0036:
	{
		int32_t L_5 = V_0;
		int32_t L_6 = Event_get_modifiers_m2201630428(__this, /*hidden argument*/NULL);
		V_0 = ((int32_t)((int32_t)((int32_t)((int32_t)L_5*(int32_t)((int32_t)37)))|(int32_t)L_6));
		int32_t L_7 = V_0;
		V_2 = L_7;
		goto IL_0049;
	}

IL_0049:
	{
		int32_t L_8 = V_2;
		return L_8;
	}
}
// System.Boolean UnityEngine.Event::Equals(System.Object)
extern "C"  bool Event_Equals_m1159919013 (Event_t2956885303 * __this, Il2CppObject * ___obj0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Event_Equals_m1159919013_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	Event_t2956885303 * V_1 = NULL;
	{
		Il2CppObject * L_0 = ___obj0;
		if (L_0)
		{
			goto IL_000e;
		}
	}
	{
		V_0 = (bool)0;
		goto IL_00b7;
	}

IL_000e:
	{
		Il2CppObject * L_1 = ___obj0;
		bool L_2 = Object_ReferenceEquals_m610702577(NULL /*static, unused*/, __this, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0021;
		}
	}
	{
		V_0 = (bool)1;
		goto IL_00b7;
	}

IL_0021:
	{
		Il2CppObject * L_3 = ___obj0;
		NullCheck(L_3);
		Type_t * L_4 = Object_GetType_m88164663(L_3, /*hidden argument*/NULL);
		Type_t * L_5 = Object_GetType_m88164663(__this, /*hidden argument*/NULL);
		if ((((Il2CppObject*)(Type_t *)L_4) == ((Il2CppObject*)(Type_t *)L_5)))
		{
			goto IL_0039;
		}
	}
	{
		V_0 = (bool)0;
		goto IL_00b7;
	}

IL_0039:
	{
		Il2CppObject * L_6 = ___obj0;
		V_1 = ((Event_t2956885303 *)CastclassSealed(L_6, Event_t2956885303_il2cpp_TypeInfo_var));
		int32_t L_7 = Event_get_type_m1523589438(__this, /*hidden argument*/NULL);
		Event_t2956885303 * L_8 = V_1;
		NullCheck(L_8);
		int32_t L_9 = Event_get_type_m1523589438(L_8, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_7) == ((uint32_t)L_9))))
		{
			goto IL_0068;
		}
	}
	{
		int32_t L_10 = Event_get_modifiers_m2201630428(__this, /*hidden argument*/NULL);
		Event_t2956885303 * L_11 = V_1;
		NullCheck(L_11);
		int32_t L_12 = Event_get_modifiers_m2201630428(L_11, /*hidden argument*/NULL);
		if ((((int32_t)((int32_t)((int32_t)L_10&(int32_t)((int32_t)-33)))) == ((int32_t)((int32_t)((int32_t)L_12&(int32_t)((int32_t)-33))))))
		{
			goto IL_006f;
		}
	}

IL_0068:
	{
		V_0 = (bool)0;
		goto IL_00b7;
	}

IL_006f:
	{
		bool L_13 = Event_get_isKey_m2009856615(__this, /*hidden argument*/NULL);
		if (!L_13)
		{
			goto IL_008e;
		}
	}
	{
		int32_t L_14 = Event_get_keyCode_m3804184495(__this, /*hidden argument*/NULL);
		Event_t2956885303 * L_15 = V_1;
		NullCheck(L_15);
		int32_t L_16 = Event_get_keyCode_m3804184495(L_15, /*hidden argument*/NULL);
		V_0 = (bool)((((int32_t)L_14) == ((int32_t)L_16))? 1 : 0);
		goto IL_00b7;
	}

IL_008e:
	{
		bool L_17 = Event_get_isMouse_m598780014(__this, /*hidden argument*/NULL);
		if (!L_17)
		{
			goto IL_00b0;
		}
	}
	{
		Vector2_t2156229523  L_18 = Event_get_mousePosition_m860577707(__this, /*hidden argument*/NULL);
		Event_t2956885303 * L_19 = V_1;
		NullCheck(L_19);
		Vector2_t2156229523  L_20 = Event_get_mousePosition_m860577707(L_19, /*hidden argument*/NULL);
		bool L_21 = Vector2_op_Equality_m3476359499(NULL /*static, unused*/, L_18, L_20, /*hidden argument*/NULL);
		V_0 = L_21;
		goto IL_00b7;
	}

IL_00b0:
	{
		V_0 = (bool)0;
		goto IL_00b7;
	}

IL_00b7:
	{
		bool L_22 = V_0;
		return L_22;
	}
}
// System.String UnityEngine.Event::ToString()
extern "C"  String_t* Event_ToString_m961638257 (Event_t2956885303 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Event_ToString_m961638257_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	{
		bool L_0 = Event_get_isKey_m2009856615(__this, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_00c1;
		}
	}
	{
		Il2CppChar L_1 = Event_get_character_m2243045319(__this, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_0058;
		}
	}
	{
		ObjectU5BU5D_t2843939325* L_2 = ((ObjectU5BU5D_t2843939325*)SZArrayNew(ObjectU5BU5D_t2843939325_il2cpp_TypeInfo_var, (uint32_t)3));
		int32_t L_3 = Event_get_type_m1523589438(__this, /*hidden argument*/NULL);
		int32_t L_4 = L_3;
		Il2CppObject * L_5 = Box(EventType_t3528516131_il2cpp_TypeInfo_var, &L_4);
		NullCheck(L_2);
		ArrayElementTypeCheck (L_2, L_5);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_5);
		ObjectU5BU5D_t2843939325* L_6 = L_2;
		int32_t L_7 = Event_get_modifiers_m2201630428(__this, /*hidden argument*/NULL);
		int32_t L_8 = L_7;
		Il2CppObject * L_9 = Box(EventModifiers_t2016417398_il2cpp_TypeInfo_var, &L_8);
		NullCheck(L_6);
		ArrayElementTypeCheck (L_6, L_9);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_9);
		ObjectU5BU5D_t2843939325* L_10 = L_6;
		int32_t L_11 = Event_get_keyCode_m3804184495(__this, /*hidden argument*/NULL);
		int32_t L_12 = L_11;
		Il2CppObject * L_13 = Box(KeyCode_t2599294277_il2cpp_TypeInfo_var, &L_12);
		NullCheck(L_10);
		ArrayElementTypeCheck (L_10, L_13);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)L_13);
		String_t* L_14 = UnityString_Format_m3741272017(NULL /*static, unused*/, _stringLiteral2886375297, L_10, /*hidden argument*/NULL);
		V_0 = L_14;
		goto IL_016e;
	}

IL_0058:
	{
		ObjectU5BU5D_t2843939325* L_15 = ((ObjectU5BU5D_t2843939325*)SZArrayNew(ObjectU5BU5D_t2843939325_il2cpp_TypeInfo_var, (uint32_t)8));
		NullCheck(L_15);
		ArrayElementTypeCheck (L_15, _stringLiteral2737274358);
		(L_15)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral2737274358);
		ObjectU5BU5D_t2843939325* L_16 = L_15;
		int32_t L_17 = Event_get_type_m1523589438(__this, /*hidden argument*/NULL);
		int32_t L_18 = L_17;
		Il2CppObject * L_19 = Box(EventType_t3528516131_il2cpp_TypeInfo_var, &L_18);
		NullCheck(L_16);
		ArrayElementTypeCheck (L_16, L_19);
		(L_16)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_19);
		ObjectU5BU5D_t2843939325* L_20 = L_16;
		NullCheck(L_20);
		ArrayElementTypeCheck (L_20, _stringLiteral1262317653);
		(L_20)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral1262317653);
		ObjectU5BU5D_t2843939325* L_21 = L_20;
		Il2CppChar L_22 = Event_get_character_m2243045319(__this, /*hidden argument*/NULL);
		int32_t L_23 = ((int32_t)L_22);
		Il2CppObject * L_24 = Box(Int32_t2950945753_il2cpp_TypeInfo_var, &L_23);
		NullCheck(L_21);
		ArrayElementTypeCheck (L_21, L_24);
		(L_21)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_24);
		ObjectU5BU5D_t2843939325* L_25 = L_21;
		NullCheck(L_25);
		ArrayElementTypeCheck (L_25, _stringLiteral4223655990);
		(L_25)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral4223655990);
		ObjectU5BU5D_t2843939325* L_26 = L_25;
		int32_t L_27 = Event_get_modifiers_m2201630428(__this, /*hidden argument*/NULL);
		int32_t L_28 = L_27;
		Il2CppObject * L_29 = Box(EventModifiers_t2016417398_il2cpp_TypeInfo_var, &L_28);
		NullCheck(L_26);
		ArrayElementTypeCheck (L_26, L_29);
		(L_26)->SetAt(static_cast<il2cpp_array_size_t>(5), (Il2CppObject *)L_29);
		ObjectU5BU5D_t2843939325* L_30 = L_26;
		NullCheck(L_30);
		ArrayElementTypeCheck (L_30, _stringLiteral1420157070);
		(L_30)->SetAt(static_cast<il2cpp_array_size_t>(6), (Il2CppObject *)_stringLiteral1420157070);
		ObjectU5BU5D_t2843939325* L_31 = L_30;
		int32_t L_32 = Event_get_keyCode_m3804184495(__this, /*hidden argument*/NULL);
		int32_t L_33 = L_32;
		Il2CppObject * L_34 = Box(KeyCode_t2599294277_il2cpp_TypeInfo_var, &L_33);
		NullCheck(L_31);
		ArrayElementTypeCheck (L_31, L_34);
		(L_31)->SetAt(static_cast<il2cpp_array_size_t>(7), (Il2CppObject *)L_34);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_35 = String_Concat_m2971454694(NULL /*static, unused*/, L_31, /*hidden argument*/NULL);
		V_0 = L_35;
		goto IL_016e;
	}

IL_00c1:
	{
		bool L_36 = Event_get_isMouse_m598780014(__this, /*hidden argument*/NULL);
		if (!L_36)
		{
			goto IL_010c;
		}
	}
	{
		ObjectU5BU5D_t2843939325* L_37 = ((ObjectU5BU5D_t2843939325*)SZArrayNew(ObjectU5BU5D_t2843939325_il2cpp_TypeInfo_var, (uint32_t)3));
		int32_t L_38 = Event_get_type_m1523589438(__this, /*hidden argument*/NULL);
		int32_t L_39 = L_38;
		Il2CppObject * L_40 = Box(EventType_t3528516131_il2cpp_TypeInfo_var, &L_39);
		NullCheck(L_37);
		ArrayElementTypeCheck (L_37, L_40);
		(L_37)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_40);
		ObjectU5BU5D_t2843939325* L_41 = L_37;
		Vector2_t2156229523  L_42 = Event_get_mousePosition_m860577707(__this, /*hidden argument*/NULL);
		Vector2_t2156229523  L_43 = L_42;
		Il2CppObject * L_44 = Box(Vector2_t2156229523_il2cpp_TypeInfo_var, &L_43);
		NullCheck(L_41);
		ArrayElementTypeCheck (L_41, L_44);
		(L_41)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_44);
		ObjectU5BU5D_t2843939325* L_45 = L_41;
		int32_t L_46 = Event_get_modifiers_m2201630428(__this, /*hidden argument*/NULL);
		int32_t L_47 = L_46;
		Il2CppObject * L_48 = Box(EventModifiers_t2016417398_il2cpp_TypeInfo_var, &L_47);
		NullCheck(L_45);
		ArrayElementTypeCheck (L_45, L_48);
		(L_45)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)L_48);
		String_t* L_49 = UnityString_Format_m3741272017(NULL /*static, unused*/, _stringLiteral4199752235, L_45, /*hidden argument*/NULL);
		V_0 = L_49;
		goto IL_016e;
	}

IL_010c:
	{
		int32_t L_50 = Event_get_type_m1523589438(__this, /*hidden argument*/NULL);
		if ((((int32_t)L_50) == ((int32_t)((int32_t)14))))
		{
			goto IL_0126;
		}
	}
	{
		int32_t L_51 = Event_get_type_m1523589438(__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_51) == ((uint32_t)((int32_t)13)))))
		{
			goto IL_0153;
		}
	}

IL_0126:
	{
		ObjectU5BU5D_t2843939325* L_52 = ((ObjectU5BU5D_t2843939325*)SZArrayNew(ObjectU5BU5D_t2843939325_il2cpp_TypeInfo_var, (uint32_t)2));
		int32_t L_53 = Event_get_type_m1523589438(__this, /*hidden argument*/NULL);
		int32_t L_54 = L_53;
		Il2CppObject * L_55 = Box(EventType_t3528516131_il2cpp_TypeInfo_var, &L_54);
		NullCheck(L_52);
		ArrayElementTypeCheck (L_52, L_55);
		(L_52)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_55);
		ObjectU5BU5D_t2843939325* L_56 = L_52;
		String_t* L_57 = Event_get_commandName_m3342234398(__this, /*hidden argument*/NULL);
		NullCheck(L_56);
		ArrayElementTypeCheck (L_56, L_57);
		(L_56)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_57);
		String_t* L_58 = UnityString_Format_m3741272017(NULL /*static, unused*/, _stringLiteral495249995, L_56, /*hidden argument*/NULL);
		V_0 = L_58;
		goto IL_016e;
	}

IL_0153:
	{
		int32_t L_59 = Event_get_type_m1523589438(__this, /*hidden argument*/NULL);
		int32_t L_60 = L_59;
		Il2CppObject * L_61 = Box(EventType_t3528516131_il2cpp_TypeInfo_var, &L_60);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_62 = String_Concat_m904156431(NULL /*static, unused*/, _stringLiteral757602046, L_61, /*hidden argument*/NULL);
		V_0 = L_62;
		goto IL_016e;
	}

IL_016e:
	{
		String_t* L_63 = V_0;
		return L_63;
	}
}
// System.Void UnityEngine.Event::Init(System.Int32)
extern "C"  void Event_Init_m3976072405 (Event_t2956885303 * __this, int32_t ___displayIndex0, const MethodInfo* method)
{
	typedef void (*Event_Init_m3976072405_ftn) (Event_t2956885303 *, int32_t);
	static Event_Init_m3976072405_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Event_Init_m3976072405_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Event::Init(System.Int32)");
	_il2cpp_icall_func(__this, ___displayIndex0);
}
// System.Void UnityEngine.Event::Cleanup()
extern "C"  void Event_Cleanup_m1044305745 (Event_t2956885303 * __this, const MethodInfo* method)
{
	typedef void (*Event_Cleanup_m1044305745_ftn) (Event_t2956885303 *);
	static Event_Cleanup_m1044305745_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Event_Cleanup_m1044305745_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Event::Cleanup()");
	_il2cpp_icall_func(__this);
}
// UnityEngine.EventType UnityEngine.Event::get_rawType()
extern "C"  int32_t Event_get_rawType_m1266512469 (Event_t2956885303 * __this, const MethodInfo* method)
{
	typedef int32_t (*Event_get_rawType_m1266512469_ftn) (Event_t2956885303 *);
	static Event_get_rawType_m1266512469_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Event_get_rawType_m1266512469_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Event::get_rawType()");
	return _il2cpp_icall_func(__this);
}
// UnityEngine.EventType UnityEngine.Event::get_type()
extern "C"  int32_t Event_get_type_m1523589438 (Event_t2956885303 * __this, const MethodInfo* method)
{
	typedef int32_t (*Event_get_type_m1523589438_ftn) (Event_t2956885303 *);
	static Event_get_type_m1523589438_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Event_get_type_m1523589438_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Event::get_type()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Event::Internal_GetMousePosition(UnityEngine.Vector2&)
extern "C"  void Event_Internal_GetMousePosition_m2915150106 (Event_t2956885303 * __this, Vector2_t2156229523 * ___value0, const MethodInfo* method)
{
	typedef void (*Event_Internal_GetMousePosition_m2915150106_ftn) (Event_t2956885303 *, Vector2_t2156229523 *);
	static Event_Internal_GetMousePosition_m2915150106_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Event_Internal_GetMousePosition_m2915150106_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Event::Internal_GetMousePosition(UnityEngine.Vector2&)");
	_il2cpp_icall_func(__this, ___value0);
}
// UnityEngine.EventModifiers UnityEngine.Event::get_modifiers()
extern "C"  int32_t Event_get_modifiers_m2201630428 (Event_t2956885303 * __this, const MethodInfo* method)
{
	typedef int32_t (*Event_get_modifiers_m2201630428_ftn) (Event_t2956885303 *);
	static Event_get_modifiers_m2201630428_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Event_get_modifiers_m2201630428_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Event::get_modifiers()");
	return _il2cpp_icall_func(__this);
}
// System.Char UnityEngine.Event::get_character()
extern "C"  Il2CppChar Event_get_character_m2243045319 (Event_t2956885303 * __this, const MethodInfo* method)
{
	typedef Il2CppChar (*Event_get_character_m2243045319_ftn) (Event_t2956885303 *);
	static Event_get_character_m2243045319_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Event_get_character_m2243045319_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Event::get_character()");
	return _il2cpp_icall_func(__this);
}
// System.String UnityEngine.Event::get_commandName()
extern "C"  String_t* Event_get_commandName_m3342234398 (Event_t2956885303 * __this, const MethodInfo* method)
{
	typedef String_t* (*Event_get_commandName_m3342234398_ftn) (Event_t2956885303 *);
	static Event_get_commandName_m3342234398_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Event_get_commandName_m3342234398_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Event::get_commandName()");
	return _il2cpp_icall_func(__this);
}
// UnityEngine.KeyCode UnityEngine.Event::get_keyCode()
extern "C"  int32_t Event_get_keyCode_m3804184495 (Event_t2956885303 * __this, const MethodInfo* method)
{
	typedef int32_t (*Event_get_keyCode_m3804184495_ftn) (Event_t2956885303 *);
	static Event_get_keyCode_m3804184495_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Event_get_keyCode_m3804184495_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Event::get_keyCode()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Event::Internal_SetNativeEvent(System.IntPtr)
extern "C"  void Event_Internal_SetNativeEvent_m4190336570 (Il2CppObject * __this /* static, unused */, IntPtr_t ___ptr0, const MethodInfo* method)
{
	typedef void (*Event_Internal_SetNativeEvent_m4190336570_ftn) (IntPtr_t);
	static Event_Internal_SetNativeEvent_m4190336570_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Event_Internal_SetNativeEvent_m4190336570_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Event::Internal_SetNativeEvent(System.IntPtr)");
	_il2cpp_icall_func(___ptr0);
}
// System.Void UnityEngine.Event::set_displayIndex(System.Int32)
extern "C"  void Event_set_displayIndex_m1870302504 (Event_t2956885303 * __this, int32_t ___value0, const MethodInfo* method)
{
	typedef void (*Event_set_displayIndex_m1870302504_ftn) (Event_t2956885303 *, int32_t);
	static Event_set_displayIndex_m1870302504_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Event_set_displayIndex_m1870302504_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Event::set_displayIndex(System.Int32)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Boolean UnityEngine.Event::PopEvent(UnityEngine.Event)
extern "C"  bool Event_PopEvent_m1322279088 (Il2CppObject * __this /* static, unused */, Event_t2956885303 * ___outEvent0, const MethodInfo* method)
{
	typedef bool (*Event_PopEvent_m1322279088_ftn) (Event_t2956885303 *);
	static Event_PopEvent_m1322279088_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Event_PopEvent_m1322279088_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Event::PopEvent(UnityEngine.Event)");
	return _il2cpp_icall_func(___outEvent0);
}
// System.Void UnityEngine.Events.ArgumentCache::.ctor()
extern "C"  void ArgumentCache__ctor_m1913080877 (ArgumentCache_t2187958399 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m297566312(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Object UnityEngine.Events.ArgumentCache::get_unityObjectArgument()
extern "C"  Object_t631007953 * ArgumentCache_get_unityObjectArgument_m164429709 (ArgumentCache_t2187958399 * __this, const MethodInfo* method)
{
	Object_t631007953 * V_0 = NULL;
	{
		Object_t631007953 * L_0 = __this->get_m_ObjectArgument_0();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		Object_t631007953 * L_1 = V_0;
		return L_1;
	}
}
// System.String UnityEngine.Events.ArgumentCache::get_unityObjectArgumentAssemblyTypeName()
extern "C"  String_t* ArgumentCache_get_unityObjectArgumentAssemblyTypeName_m3286213661 (ArgumentCache_t2187958399 * __this, const MethodInfo* method)
{
	String_t* V_0 = NULL;
	{
		String_t* L_0 = __this->get_m_ObjectArgumentAssemblyTypeName_1();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		String_t* L_1 = V_0;
		return L_1;
	}
}
// System.Int32 UnityEngine.Events.ArgumentCache::get_intArgument()
extern "C"  int32_t ArgumentCache_get_intArgument_m3815654797 (ArgumentCache_t2187958399 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_m_IntArgument_2();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		int32_t L_1 = V_0;
		return L_1;
	}
}
// System.Single UnityEngine.Events.ArgumentCache::get_floatArgument()
extern "C"  float ArgumentCache_get_floatArgument_m2801316765 (ArgumentCache_t2187958399 * __this, const MethodInfo* method)
{
	float V_0 = 0.0f;
	{
		float L_0 = __this->get_m_FloatArgument_3();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		float L_1 = V_0;
		return L_1;
	}
}
// System.String UnityEngine.Events.ArgumentCache::get_stringArgument()
extern "C"  String_t* ArgumentCache_get_stringArgument_m581215363 (ArgumentCache_t2187958399 * __this, const MethodInfo* method)
{
	String_t* V_0 = NULL;
	{
		String_t* L_0 = __this->get_m_StringArgument_4();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		String_t* L_1 = V_0;
		return L_1;
	}
}
// System.Boolean UnityEngine.Events.ArgumentCache::get_boolArgument()
extern "C"  bool ArgumentCache_get_boolArgument_m4017632913 (ArgumentCache_t2187958399 * __this, const MethodInfo* method)
{
	bool V_0 = false;
	{
		bool L_0 = __this->get_m_BoolArgument_5();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		bool L_1 = V_0;
		return L_1;
	}
}
// System.Void UnityEngine.Events.ArgumentCache::TidyAssemblyTypeName()
extern "C"  void ArgumentCache_TidyAssemblyTypeName_m1203759352 (ArgumentCache_t2187958399 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ArgumentCache_TidyAssemblyTypeName_m1203759352_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		String_t* L_0 = __this->get_m_ObjectArgumentAssemblyTypeName_1();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_1 = String_IsNullOrEmpty_m2969720369(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0016;
		}
	}
	{
		goto IL_009f;
	}

IL_0016:
	{
		V_0 = ((int32_t)2147483647LL);
		String_t* L_2 = __this->get_m_ObjectArgumentAssemblyTypeName_1();
		NullCheck(L_2);
		int32_t L_3 = String_IndexOf_m1977622757(L_2, _stringLiteral155902455, /*hidden argument*/NULL);
		V_1 = L_3;
		int32_t L_4 = V_1;
		if ((((int32_t)L_4) == ((int32_t)(-1))))
		{
			goto IL_003c;
		}
	}
	{
		int32_t L_5 = V_1;
		int32_t L_6 = V_0;
		int32_t L_7 = Math_Min_m3468062251(NULL /*static, unused*/, L_5, L_6, /*hidden argument*/NULL);
		V_0 = L_7;
	}

IL_003c:
	{
		String_t* L_8 = __this->get_m_ObjectArgumentAssemblyTypeName_1();
		NullCheck(L_8);
		int32_t L_9 = String_IndexOf_m1977622757(L_8, _stringLiteral1973799399, /*hidden argument*/NULL);
		V_1 = L_9;
		int32_t L_10 = V_1;
		if ((((int32_t)L_10) == ((int32_t)(-1))))
		{
			goto IL_005c;
		}
	}
	{
		int32_t L_11 = V_1;
		int32_t L_12 = V_0;
		int32_t L_13 = Math_Min_m3468062251(NULL /*static, unused*/, L_11, L_12, /*hidden argument*/NULL);
		V_0 = L_13;
	}

IL_005c:
	{
		String_t* L_14 = __this->get_m_ObjectArgumentAssemblyTypeName_1();
		NullCheck(L_14);
		int32_t L_15 = String_IndexOf_m1977622757(L_14, _stringLiteral1736874801, /*hidden argument*/NULL);
		V_1 = L_15;
		int32_t L_16 = V_1;
		if ((((int32_t)L_16) == ((int32_t)(-1))))
		{
			goto IL_007c;
		}
	}
	{
		int32_t L_17 = V_1;
		int32_t L_18 = V_0;
		int32_t L_19 = Math_Min_m3468062251(NULL /*static, unused*/, L_17, L_18, /*hidden argument*/NULL);
		V_0 = L_19;
	}

IL_007c:
	{
		int32_t L_20 = V_0;
		if ((!(((uint32_t)L_20) == ((uint32_t)((int32_t)2147483647LL)))))
		{
			goto IL_008c;
		}
	}
	{
		goto IL_009f;
	}

IL_008c:
	{
		String_t* L_21 = __this->get_m_ObjectArgumentAssemblyTypeName_1();
		int32_t L_22 = V_0;
		NullCheck(L_21);
		String_t* L_23 = String_Substring_m1610150815(L_21, 0, L_22, /*hidden argument*/NULL);
		__this->set_m_ObjectArgumentAssemblyTypeName_1(L_23);
	}

IL_009f:
	{
		return;
	}
}
// System.Void UnityEngine.Events.ArgumentCache::OnBeforeSerialize()
extern "C"  void ArgumentCache_OnBeforeSerialize_m1013337285 (ArgumentCache_t2187958399 * __this, const MethodInfo* method)
{
	{
		ArgumentCache_TidyAssemblyTypeName_m1203759352(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Events.ArgumentCache::OnAfterDeserialize()
extern "C"  void ArgumentCache_OnAfterDeserialize_m4176438319 (ArgumentCache_t2187958399 * __this, const MethodInfo* method)
{
	{
		ArgumentCache_TidyAssemblyTypeName_m1203759352(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Events.BaseInvokableCall::.ctor()
extern "C"  void BaseInvokableCall__ctor_m3648827834 (BaseInvokableCall_t2703961024 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m297566312(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Events.BaseInvokableCall::.ctor(System.Object,System.Reflection.MethodInfo)
extern "C"  void BaseInvokableCall__ctor_m1036035291 (BaseInvokableCall_t2703961024 * __this, Il2CppObject * ___target0, MethodInfo_t * ___function1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (BaseInvokableCall__ctor_m1036035291_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Object__ctor_m297566312(__this, /*hidden argument*/NULL);
		Il2CppObject * L_0 = ___target0;
		if (L_0)
		{
			goto IL_0018;
		}
	}
	{
		ArgumentNullException_t1615371798 * L_1 = (ArgumentNullException_t1615371798 *)il2cpp_codegen_object_new(ArgumentNullException_t1615371798_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m1170824041(L_1, _stringLiteral2833503317, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		MethodInfo_t * L_2 = ___function1;
		if (L_2)
		{
			goto IL_0029;
		}
	}
	{
		ArgumentNullException_t1615371798 * L_3 = (ArgumentNullException_t1615371798 *)il2cpp_codegen_object_new(ArgumentNullException_t1615371798_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m1170824041(L_3, _stringLiteral3941509395, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_0029:
	{
		return;
	}
}
// System.Boolean UnityEngine.Events.BaseInvokableCall::AllowInvoke(System.Delegate)
extern "C"  bool BaseInvokableCall_AllowInvoke_m1250801794 (Il2CppObject * __this /* static, unused */, Delegate_t1188392813 * ___delegate0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (BaseInvokableCall_AllowInvoke_m1250801794_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Il2CppObject * V_0 = NULL;
	bool V_1 = false;
	Object_t631007953 * V_2 = NULL;
	{
		Delegate_t1188392813 * L_0 = ___delegate0;
		NullCheck(L_0);
		Il2CppObject * L_1 = Delegate_get_Target_m2361978888(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		Il2CppObject * L_2 = V_0;
		if (L_2)
		{
			goto IL_0015;
		}
	}
	{
		V_1 = (bool)1;
		goto IL_003c;
	}

IL_0015:
	{
		Il2CppObject * L_3 = V_0;
		V_2 = ((Object_t631007953 *)IsInstClass(L_3, Object_t631007953_il2cpp_TypeInfo_var));
		Object_t631007953 * L_4 = V_2;
		bool L_5 = Object_ReferenceEquals_m610702577(NULL /*static, unused*/, L_4, NULL, /*hidden argument*/NULL);
		if (L_5)
		{
			goto IL_0035;
		}
	}
	{
		Object_t631007953 * L_6 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_7 = Object_op_Inequality_m1920811489(NULL /*static, unused*/, L_6, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		V_1 = L_7;
		goto IL_003c;
	}

IL_0035:
	{
		V_1 = (bool)1;
		goto IL_003c;
	}

IL_003c:
	{
		bool L_8 = V_1;
		return L_8;
	}
}
// System.Void UnityEngine.Events.InvokableCall::.ctor(System.Object,System.Reflection.MethodInfo)
extern "C"  void InvokableCall__ctor_m2864671469 (InvokableCall_t832123510 * __this, Il2CppObject * ___target0, MethodInfo_t * ___theFunction1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InvokableCall__ctor_m2864671469_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppObject * L_0 = ___target0;
		MethodInfo_t * L_1 = ___theFunction1;
		BaseInvokableCall__ctor_m1036035291(__this, L_0, L_1, /*hidden argument*/NULL);
		MethodInfo_t * L_2 = ___theFunction1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, LoadTypeToken(UnityAction_t3245792599_0_0_0_var), /*hidden argument*/NULL);
		Il2CppObject * L_4 = ___target0;
		Delegate_t1188392813 * L_5 = NetFxCoreExtensions_CreateDelegate_m4283065679(NULL /*static, unused*/, L_2, L_3, L_4, /*hidden argument*/NULL);
		InvokableCall_add_Delegate_m2530991106(__this, ((UnityAction_t3245792599 *)CastclassSealed(L_5, UnityAction_t3245792599_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCall::.ctor(UnityEngine.Events.UnityAction)
extern "C"  void InvokableCall__ctor_m616326848 (InvokableCall_t832123510 * __this, UnityAction_t3245792599 * ___action0, const MethodInfo* method)
{
	{
		BaseInvokableCall__ctor_m3648827834(__this, /*hidden argument*/NULL);
		UnityAction_t3245792599 * L_0 = ___action0;
		InvokableCall_add_Delegate_m2530991106(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCall::add_Delegate(UnityEngine.Events.UnityAction)
extern "C"  void InvokableCall_add_Delegate_m2530991106 (InvokableCall_t832123510 * __this, UnityAction_t3245792599 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InvokableCall_add_Delegate_m2530991106_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	UnityAction_t3245792599 * V_0 = NULL;
	UnityAction_t3245792599 * V_1 = NULL;
	{
		UnityAction_t3245792599 * L_0 = __this->get_Delegate_0();
		V_0 = L_0;
	}

IL_0007:
	{
		UnityAction_t3245792599 * L_1 = V_0;
		V_1 = L_1;
		UnityAction_t3245792599 ** L_2 = __this->get_address_of_Delegate_0();
		UnityAction_t3245792599 * L_3 = V_1;
		UnityAction_t3245792599 * L_4 = ___value0;
		Delegate_t1188392813 * L_5 = Delegate_Combine_m1859655160(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		UnityAction_t3245792599 * L_6 = V_0;
		UnityAction_t3245792599 * L_7 = InterlockedCompareExchangeImpl<UnityAction_t3245792599 *>(L_2, ((UnityAction_t3245792599 *)CastclassSealed(L_5, UnityAction_t3245792599_il2cpp_TypeInfo_var)), L_6);
		V_0 = L_7;
		UnityAction_t3245792599 * L_8 = V_0;
		UnityAction_t3245792599 * L_9 = V_1;
		if ((!(((Il2CppObject*)(UnityAction_t3245792599 *)L_8) == ((Il2CppObject*)(UnityAction_t3245792599 *)L_9))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCall::remove_Delegate(UnityEngine.Events.UnityAction)
extern "C"  void InvokableCall_remove_Delegate_m1019375674 (InvokableCall_t832123510 * __this, UnityAction_t3245792599 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InvokableCall_remove_Delegate_m1019375674_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	UnityAction_t3245792599 * V_0 = NULL;
	UnityAction_t3245792599 * V_1 = NULL;
	{
		UnityAction_t3245792599 * L_0 = __this->get_Delegate_0();
		V_0 = L_0;
	}

IL_0007:
	{
		UnityAction_t3245792599 * L_1 = V_0;
		V_1 = L_1;
		UnityAction_t3245792599 ** L_2 = __this->get_address_of_Delegate_0();
		UnityAction_t3245792599 * L_3 = V_1;
		UnityAction_t3245792599 * L_4 = ___value0;
		Delegate_t1188392813 * L_5 = Delegate_Remove_m334097152(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		UnityAction_t3245792599 * L_6 = V_0;
		UnityAction_t3245792599 * L_7 = InterlockedCompareExchangeImpl<UnityAction_t3245792599 *>(L_2, ((UnityAction_t3245792599 *)CastclassSealed(L_5, UnityAction_t3245792599_il2cpp_TypeInfo_var)), L_6);
		V_0 = L_7;
		UnityAction_t3245792599 * L_8 = V_0;
		UnityAction_t3245792599 * L_9 = V_1;
		if ((!(((Il2CppObject*)(UnityAction_t3245792599 *)L_8) == ((Il2CppObject*)(UnityAction_t3245792599 *)L_9))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCall::Invoke(System.Object[])
extern "C"  void InvokableCall_Invoke_m3416177260 (InvokableCall_t832123510 * __this, ObjectU5BU5D_t2843939325* ___args0, const MethodInfo* method)
{
	{
		UnityAction_t3245792599 * L_0 = __this->get_Delegate_0();
		bool L_1 = BaseInvokableCall_AllowInvoke_m1250801794(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001c;
		}
	}
	{
		UnityAction_t3245792599 * L_2 = __this->get_Delegate_0();
		NullCheck(L_2);
		UnityAction_Invoke_m804868541(L_2, /*hidden argument*/NULL);
	}

IL_001c:
	{
		return;
	}
}
// System.Boolean UnityEngine.Events.InvokableCall::Find(System.Object,System.Reflection.MethodInfo)
extern "C"  bool InvokableCall_Find_m185494232 (InvokableCall_t832123510 * __this, Il2CppObject * ___targetObj0, MethodInfo_t * ___method1, const MethodInfo* method)
{
	bool V_0 = false;
	int32_t G_B3_0 = 0;
	{
		UnityAction_t3245792599 * L_0 = __this->get_Delegate_0();
		NullCheck(L_0);
		Il2CppObject * L_1 = Delegate_get_Target_m2361978888(L_0, /*hidden argument*/NULL);
		Il2CppObject * L_2 = ___targetObj0;
		if ((!(((Il2CppObject*)(Il2CppObject *)L_1) == ((Il2CppObject*)(Il2CppObject *)L_2))))
		{
			goto IL_0025;
		}
	}
	{
		UnityAction_t3245792599 * L_3 = __this->get_Delegate_0();
		MethodInfo_t * L_4 = NetFxCoreExtensions_GetMethodInfo_m3154548066(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		MethodInfo_t * L_5 = ___method1;
		NullCheck(L_4);
		bool L_6 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, L_4, L_5);
		G_B3_0 = ((int32_t)(L_6));
		goto IL_0026;
	}

IL_0025:
	{
		G_B3_0 = 0;
	}

IL_0026:
	{
		V_0 = (bool)G_B3_0;
		goto IL_002c;
	}

IL_002c:
	{
		bool L_7 = V_0;
		return L_7;
	}
}
// System.Void UnityEngine.Events.InvokableCallList::.ctor()
extern "C"  void InvokableCallList__ctor_m2867488712 (InvokableCallList_t2498835369 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InvokableCallList__ctor_m2867488712_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		List_1_t4176035766 * L_0 = (List_1_t4176035766 *)il2cpp_codegen_object_new(List_1_t4176035766_il2cpp_TypeInfo_var);
		List_1__ctor_m3206896509(L_0, /*hidden argument*/List_1__ctor_m3206896509_MethodInfo_var);
		__this->set_m_PersistentCalls_0(L_0);
		List_1_t4176035766 * L_1 = (List_1_t4176035766 *)il2cpp_codegen_object_new(List_1_t4176035766_il2cpp_TypeInfo_var);
		List_1__ctor_m3206896509(L_1, /*hidden argument*/List_1__ctor_m3206896509_MethodInfo_var);
		__this->set_m_RuntimeCalls_1(L_1);
		List_1_t4176035766 * L_2 = (List_1_t4176035766 *)il2cpp_codegen_object_new(List_1_t4176035766_il2cpp_TypeInfo_var);
		List_1__ctor_m3206896509(L_2, /*hidden argument*/List_1__ctor_m3206896509_MethodInfo_var);
		__this->set_m_ExecutingCalls_2(L_2);
		__this->set_m_NeedsUpdate_3((bool)1);
		Object__ctor_m297566312(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCallList::AddPersistentInvokableCall(UnityEngine.Events.BaseInvokableCall)
extern "C"  void InvokableCallList_AddPersistentInvokableCall_m4020414894 (InvokableCallList_t2498835369 * __this, BaseInvokableCall_t2703961024 * ___call0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InvokableCallList_AddPersistentInvokableCall_m4020414894_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		List_1_t4176035766 * L_0 = __this->get_m_PersistentCalls_0();
		BaseInvokableCall_t2703961024 * L_1 = ___call0;
		NullCheck(L_0);
		List_1_Add_m19707193(L_0, L_1, /*hidden argument*/List_1_Add_m19707193_MethodInfo_var);
		__this->set_m_NeedsUpdate_3((bool)1);
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCallList::AddListener(UnityEngine.Events.BaseInvokableCall)
extern "C"  void InvokableCallList_AddListener_m4292769145 (InvokableCallList_t2498835369 * __this, BaseInvokableCall_t2703961024 * ___call0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InvokableCallList_AddListener_m4292769145_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		List_1_t4176035766 * L_0 = __this->get_m_RuntimeCalls_1();
		BaseInvokableCall_t2703961024 * L_1 = ___call0;
		NullCheck(L_0);
		List_1_Add_m19707193(L_0, L_1, /*hidden argument*/List_1_Add_m19707193_MethodInfo_var);
		__this->set_m_NeedsUpdate_3((bool)1);
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCallList::RemoveListener(System.Object,System.Reflection.MethodInfo)
extern "C"  void InvokableCallList_RemoveListener_m1974000505 (InvokableCallList_t2498835369 * __this, Il2CppObject * ___targetObj0, MethodInfo_t * ___method1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InvokableCallList_RemoveListener_m1974000505_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	List_1_t4176035766 * V_0 = NULL;
	int32_t V_1 = 0;
	{
		List_1_t4176035766 * L_0 = (List_1_t4176035766 *)il2cpp_codegen_object_new(List_1_t4176035766_il2cpp_TypeInfo_var);
		List_1__ctor_m3206896509(L_0, /*hidden argument*/List_1__ctor_m3206896509_MethodInfo_var);
		V_0 = L_0;
		V_1 = 0;
		goto IL_003e;
	}

IL_000e:
	{
		List_1_t4176035766 * L_1 = __this->get_m_RuntimeCalls_1();
		int32_t L_2 = V_1;
		NullCheck(L_1);
		BaseInvokableCall_t2703961024 * L_3 = List_1_get_Item_m2469781675(L_1, L_2, /*hidden argument*/List_1_get_Item_m2469781675_MethodInfo_var);
		Il2CppObject * L_4 = ___targetObj0;
		MethodInfo_t * L_5 = ___method1;
		NullCheck(L_3);
		bool L_6 = VirtFuncInvoker2< bool, Il2CppObject *, MethodInfo_t * >::Invoke(5 /* System.Boolean UnityEngine.Events.BaseInvokableCall::Find(System.Object,System.Reflection.MethodInfo) */, L_3, L_4, L_5);
		if (!L_6)
		{
			goto IL_0039;
		}
	}
	{
		List_1_t4176035766 * L_7 = V_0;
		List_1_t4176035766 * L_8 = __this->get_m_RuntimeCalls_1();
		int32_t L_9 = V_1;
		NullCheck(L_8);
		BaseInvokableCall_t2703961024 * L_10 = List_1_get_Item_m2469781675(L_8, L_9, /*hidden argument*/List_1_get_Item_m2469781675_MethodInfo_var);
		NullCheck(L_7);
		List_1_Add_m19707193(L_7, L_10, /*hidden argument*/List_1_Add_m19707193_MethodInfo_var);
	}

IL_0039:
	{
		int32_t L_11 = V_1;
		V_1 = ((int32_t)((int32_t)L_11+(int32_t)1));
	}

IL_003e:
	{
		int32_t L_12 = V_1;
		List_1_t4176035766 * L_13 = __this->get_m_RuntimeCalls_1();
		NullCheck(L_13);
		int32_t L_14 = List_1_get_Count_m743730018(L_13, /*hidden argument*/List_1_get_Count_m743730018_MethodInfo_var);
		if ((((int32_t)L_12) < ((int32_t)L_14)))
		{
			goto IL_000e;
		}
	}
	{
		List_1_t4176035766 * L_15 = __this->get_m_RuntimeCalls_1();
		List_1_t4176035766 * L_16 = V_0;
		IntPtr_t L_17;
		L_17.set_m_value_0((void*)(void*)List_1_Contains_m152128373_MethodInfo_var);
		Predicate_1_t3529255148 * L_18 = (Predicate_1_t3529255148 *)il2cpp_codegen_object_new(Predicate_1_t3529255148_il2cpp_TypeInfo_var);
		Predicate_1__ctor_m3247287534(L_18, L_16, L_17, /*hidden argument*/Predicate_1__ctor_m3247287534_MethodInfo_var);
		NullCheck(L_15);
		List_1_RemoveAll_m1762023412(L_15, L_18, /*hidden argument*/List_1_RemoveAll_m1762023412_MethodInfo_var);
		__this->set_m_NeedsUpdate_3((bool)1);
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCallList::ClearPersistent()
extern "C"  void InvokableCallList_ClearPersistent_m3446223403 (InvokableCallList_t2498835369 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InvokableCallList_ClearPersistent_m3446223403_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		List_1_t4176035766 * L_0 = __this->get_m_PersistentCalls_0();
		NullCheck(L_0);
		List_1_Clear_m3668089392(L_0, /*hidden argument*/List_1_Clear_m3668089392_MethodInfo_var);
		__this->set_m_NeedsUpdate_3((bool)1);
		return;
	}
}
// System.Void UnityEngine.Events.InvokableCallList::Invoke(System.Object[])
extern "C"  void InvokableCallList_Invoke_m2795703874 (InvokableCallList_t2498835369 * __this, ObjectU5BU5D_t2843939325* ___parameters0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (InvokableCallList_Invoke_m2795703874_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		bool L_0 = __this->get_m_NeedsUpdate_3();
		if (!L_0)
		{
			goto IL_0042;
		}
	}
	{
		List_1_t4176035766 * L_1 = __this->get_m_ExecutingCalls_2();
		NullCheck(L_1);
		List_1_Clear_m3668089392(L_1, /*hidden argument*/List_1_Clear_m3668089392_MethodInfo_var);
		List_1_t4176035766 * L_2 = __this->get_m_ExecutingCalls_2();
		List_1_t4176035766 * L_3 = __this->get_m_PersistentCalls_0();
		NullCheck(L_2);
		List_1_AddRange_m976883154(L_2, L_3, /*hidden argument*/List_1_AddRange_m976883154_MethodInfo_var);
		List_1_t4176035766 * L_4 = __this->get_m_ExecutingCalls_2();
		List_1_t4176035766 * L_5 = __this->get_m_RuntimeCalls_1();
		NullCheck(L_4);
		List_1_AddRange_m976883154(L_4, L_5, /*hidden argument*/List_1_AddRange_m976883154_MethodInfo_var);
		__this->set_m_NeedsUpdate_3((bool)0);
	}

IL_0042:
	{
		V_0 = 0;
		goto IL_005f;
	}

IL_0049:
	{
		List_1_t4176035766 * L_6 = __this->get_m_ExecutingCalls_2();
		int32_t L_7 = V_0;
		NullCheck(L_6);
		BaseInvokableCall_t2703961024 * L_8 = List_1_get_Item_m2469781675(L_6, L_7, /*hidden argument*/List_1_get_Item_m2469781675_MethodInfo_var);
		ObjectU5BU5D_t2843939325* L_9 = ___parameters0;
		NullCheck(L_8);
		VirtActionInvoker1< ObjectU5BU5D_t2843939325* >::Invoke(4 /* System.Void UnityEngine.Events.BaseInvokableCall::Invoke(System.Object[]) */, L_8, L_9);
		int32_t L_10 = V_0;
		V_0 = ((int32_t)((int32_t)L_10+(int32_t)1));
	}

IL_005f:
	{
		int32_t L_11 = V_0;
		List_1_t4176035766 * L_12 = __this->get_m_ExecutingCalls_2();
		NullCheck(L_12);
		int32_t L_13 = List_1_get_Count_m743730018(L_12, /*hidden argument*/List_1_get_Count_m743730018_MethodInfo_var);
		if ((((int32_t)L_11) < ((int32_t)L_13)))
		{
			goto IL_0049;
		}
	}
	{
		return;
	}
}
// System.Void UnityEngine.Events.PersistentCall::.ctor()
extern "C"  void PersistentCall__ctor_m1104049729 (PersistentCall_t3407714124 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PersistentCall__ctor_m1104049729_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		__this->set_m_Mode_2(0);
		ArgumentCache_t2187958399 * L_0 = (ArgumentCache_t2187958399 *)il2cpp_codegen_object_new(ArgumentCache_t2187958399_il2cpp_TypeInfo_var);
		ArgumentCache__ctor_m1913080877(L_0, /*hidden argument*/NULL);
		__this->set_m_Arguments_3(L_0);
		__this->set_m_CallState_4(2);
		Object__ctor_m297566312(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Object UnityEngine.Events.PersistentCall::get_target()
extern "C"  Object_t631007953 * PersistentCall_get_target_m2795613068 (PersistentCall_t3407714124 * __this, const MethodInfo* method)
{
	Object_t631007953 * V_0 = NULL;
	{
		Object_t631007953 * L_0 = __this->get_m_Target_0();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		Object_t631007953 * L_1 = V_0;
		return L_1;
	}
}
// System.String UnityEngine.Events.PersistentCall::get_methodName()
extern "C"  String_t* PersistentCall_get_methodName_m2181722332 (PersistentCall_t3407714124 * __this, const MethodInfo* method)
{
	String_t* V_0 = NULL;
	{
		String_t* L_0 = __this->get_m_MethodName_1();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		String_t* L_1 = V_0;
		return L_1;
	}
}
// UnityEngine.Events.PersistentListenerMode UnityEngine.Events.PersistentCall::get_mode()
extern "C"  int32_t PersistentCall_get_mode_m1368131393 (PersistentCall_t3407714124 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_m_Mode_2();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		int32_t L_1 = V_0;
		return L_1;
	}
}
// UnityEngine.Events.ArgumentCache UnityEngine.Events.PersistentCall::get_arguments()
extern "C"  ArgumentCache_t2187958399 * PersistentCall_get_arguments_m1263638726 (PersistentCall_t3407714124 * __this, const MethodInfo* method)
{
	ArgumentCache_t2187958399 * V_0 = NULL;
	{
		ArgumentCache_t2187958399 * L_0 = __this->get_m_Arguments_3();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		ArgumentCache_t2187958399 * L_1 = V_0;
		return L_1;
	}
}
// System.Boolean UnityEngine.Events.PersistentCall::IsValid()
extern "C"  bool PersistentCall_IsValid_m3557513049 (PersistentCall_t3407714124 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PersistentCall_IsValid_m3557513049_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	int32_t G_B3_0 = 0;
	{
		Object_t631007953 * L_0 = PersistentCall_get_target_m2795613068(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m1920811489(NULL /*static, unused*/, L_0, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0022;
		}
	}
	{
		String_t* L_2 = PersistentCall_get_methodName_m2181722332(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_3 = String_IsNullOrEmpty_m2969720369(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		G_B3_0 = ((((int32_t)L_3) == ((int32_t)0))? 1 : 0);
		goto IL_0023;
	}

IL_0022:
	{
		G_B3_0 = 0;
	}

IL_0023:
	{
		V_0 = (bool)G_B3_0;
		goto IL_0029;
	}

IL_0029:
	{
		bool L_4 = V_0;
		return L_4;
	}
}
// UnityEngine.Events.BaseInvokableCall UnityEngine.Events.PersistentCall::GetRuntimeCall(UnityEngine.Events.UnityEventBase)
extern "C"  BaseInvokableCall_t2703961024 * PersistentCall_GetRuntimeCall_m606139440 (PersistentCall_t3407714124 * __this, UnityEventBase_t3960448221 * ___theEvent0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PersistentCall_GetRuntimeCall_m606139440_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	BaseInvokableCall_t2703961024 * V_0 = NULL;
	MethodInfo_t * V_1 = NULL;
	int32_t V_2 = 0;
	{
		int32_t L_0 = __this->get_m_CallState_4();
		if (!L_0)
		{
			goto IL_0012;
		}
	}
	{
		UnityEventBase_t3960448221 * L_1 = ___theEvent0;
		if (L_1)
		{
			goto IL_0019;
		}
	}

IL_0012:
	{
		V_0 = (BaseInvokableCall_t2703961024 *)NULL;
		goto IL_0114;
	}

IL_0019:
	{
		UnityEventBase_t3960448221 * L_2 = ___theEvent0;
		NullCheck(L_2);
		MethodInfo_t * L_3 = UnityEventBase_FindMethod_m1055565284(L_2, __this, /*hidden argument*/NULL);
		V_1 = L_3;
		MethodInfo_t * L_4 = V_1;
		if (L_4)
		{
			goto IL_002e;
		}
	}
	{
		V_0 = (BaseInvokableCall_t2703961024 *)NULL;
		goto IL_0114;
	}

IL_002e:
	{
		int32_t L_5 = __this->get_m_Mode_2();
		V_2 = L_5;
		int32_t L_6 = V_2;
		switch (L_6)
		{
			case 0:
			{
				goto IL_005c;
			}
			case 1:
			{
				goto IL_00fb;
			}
			case 2:
			{
				goto IL_006f;
			}
			case 3:
			{
				goto IL_00a4;
			}
			case 4:
			{
				goto IL_0087;
			}
			case 5:
			{
				goto IL_00c1;
			}
			case 6:
			{
				goto IL_00de;
			}
		}
	}
	{
		goto IL_010d;
	}

IL_005c:
	{
		UnityEventBase_t3960448221 * L_7 = ___theEvent0;
		Object_t631007953 * L_8 = PersistentCall_get_target_m2795613068(__this, /*hidden argument*/NULL);
		MethodInfo_t * L_9 = V_1;
		NullCheck(L_7);
		BaseInvokableCall_t2703961024 * L_10 = VirtFuncInvoker2< BaseInvokableCall_t2703961024 *, Il2CppObject *, MethodInfo_t * >::Invoke(7 /* UnityEngine.Events.BaseInvokableCall UnityEngine.Events.UnityEventBase::GetDelegate(System.Object,System.Reflection.MethodInfo) */, L_7, L_8, L_9);
		V_0 = L_10;
		goto IL_0114;
	}

IL_006f:
	{
		Object_t631007953 * L_11 = PersistentCall_get_target_m2795613068(__this, /*hidden argument*/NULL);
		MethodInfo_t * L_12 = V_1;
		ArgumentCache_t2187958399 * L_13 = __this->get_m_Arguments_3();
		BaseInvokableCall_t2703961024 * L_14 = PersistentCall_GetObjectCall_m861446582(NULL /*static, unused*/, L_11, L_12, L_13, /*hidden argument*/NULL);
		V_0 = L_14;
		goto IL_0114;
	}

IL_0087:
	{
		Object_t631007953 * L_15 = PersistentCall_get_target_m2795613068(__this, /*hidden argument*/NULL);
		MethodInfo_t * L_16 = V_1;
		ArgumentCache_t2187958399 * L_17 = __this->get_m_Arguments_3();
		NullCheck(L_17);
		float L_18 = ArgumentCache_get_floatArgument_m2801316765(L_17, /*hidden argument*/NULL);
		CachedInvokableCall_1_t3723462114 * L_19 = (CachedInvokableCall_1_t3723462114 *)il2cpp_codegen_object_new(CachedInvokableCall_1_t3723462114_il2cpp_TypeInfo_var);
		CachedInvokableCall_1__ctor_m2985117283(L_19, L_15, L_16, L_18, /*hidden argument*/CachedInvokableCall_1__ctor_m2985117283_MethodInfo_var);
		V_0 = L_19;
		goto IL_0114;
	}

IL_00a4:
	{
		Object_t631007953 * L_20 = PersistentCall_get_target_m2795613068(__this, /*hidden argument*/NULL);
		MethodInfo_t * L_21 = V_1;
		ArgumentCache_t2187958399 * L_22 = __this->get_m_Arguments_3();
		NullCheck(L_22);
		int32_t L_23 = ArgumentCache_get_intArgument_m3815654797(L_22, /*hidden argument*/NULL);
		CachedInvokableCall_1_t982173797 * L_24 = (CachedInvokableCall_1_t982173797 *)il2cpp_codegen_object_new(CachedInvokableCall_1_t982173797_il2cpp_TypeInfo_var);
		CachedInvokableCall_1__ctor_m3353156630(L_24, L_20, L_21, L_23, /*hidden argument*/CachedInvokableCall_1__ctor_m3353156630_MethodInfo_var);
		V_0 = L_24;
		goto IL_0114;
	}

IL_00c1:
	{
		Object_t631007953 * L_25 = PersistentCall_get_target_m2795613068(__this, /*hidden argument*/NULL);
		MethodInfo_t * L_26 = V_1;
		ArgumentCache_t2187958399 * L_27 = __this->get_m_Arguments_3();
		NullCheck(L_27);
		String_t* L_28 = ArgumentCache_get_stringArgument_m581215363(L_27, /*hidden argument*/NULL);
		CachedInvokableCall_1_t4173646029 * L_29 = (CachedInvokableCall_1_t4173646029 *)il2cpp_codegen_object_new(CachedInvokableCall_1_t4173646029_il2cpp_TypeInfo_var);
		CachedInvokableCall_1__ctor_m2815878172(L_29, L_25, L_26, L_28, /*hidden argument*/CachedInvokableCall_1__ctor_m2815878172_MethodInfo_var);
		V_0 = L_29;
		goto IL_0114;
	}

IL_00de:
	{
		Object_t631007953 * L_30 = PersistentCall_get_target_m2795613068(__this, /*hidden argument*/NULL);
		MethodInfo_t * L_31 = V_1;
		ArgumentCache_t2187958399 * L_32 = __this->get_m_Arguments_3();
		NullCheck(L_32);
		bool L_33 = ArgumentCache_get_boolArgument_m4017632913(L_32, /*hidden argument*/NULL);
		CachedInvokableCall_1_t2423483305 * L_34 = (CachedInvokableCall_1_t2423483305 *)il2cpp_codegen_object_new(CachedInvokableCall_1_t2423483305_il2cpp_TypeInfo_var);
		CachedInvokableCall_1__ctor_m3594541075(L_34, L_30, L_31, L_33, /*hidden argument*/CachedInvokableCall_1__ctor_m3594541075_MethodInfo_var);
		V_0 = L_34;
		goto IL_0114;
	}

IL_00fb:
	{
		Object_t631007953 * L_35 = PersistentCall_get_target_m2795613068(__this, /*hidden argument*/NULL);
		MethodInfo_t * L_36 = V_1;
		InvokableCall_t832123510 * L_37 = (InvokableCall_t832123510 *)il2cpp_codegen_object_new(InvokableCall_t832123510_il2cpp_TypeInfo_var);
		InvokableCall__ctor_m2864671469(L_37, L_35, L_36, /*hidden argument*/NULL);
		V_0 = L_37;
		goto IL_0114;
	}

IL_010d:
	{
		V_0 = (BaseInvokableCall_t2703961024 *)NULL;
		goto IL_0114;
	}

IL_0114:
	{
		BaseInvokableCall_t2703961024 * L_38 = V_0;
		return L_38;
	}
}
// UnityEngine.Events.BaseInvokableCall UnityEngine.Events.PersistentCall::GetObjectCall(UnityEngine.Object,System.Reflection.MethodInfo,UnityEngine.Events.ArgumentCache)
extern "C"  BaseInvokableCall_t2703961024 * PersistentCall_GetObjectCall_m861446582 (Il2CppObject * __this /* static, unused */, Object_t631007953 * ___target0, MethodInfo_t * ___method1, ArgumentCache_t2187958399 * ___arguments2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PersistentCall_GetObjectCall_m861446582_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Type_t * V_0 = NULL;
	Type_t * V_1 = NULL;
	Type_t * V_2 = NULL;
	ConstructorInfo_t5769829 * V_3 = NULL;
	Object_t631007953 * V_4 = NULL;
	BaseInvokableCall_t2703961024 * V_5 = NULL;
	Type_t * G_B3_0 = NULL;
	Type_t * G_B2_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, LoadTypeToken(Object_t631007953_0_0_0_var), /*hidden argument*/NULL);
		V_0 = L_0;
		ArgumentCache_t2187958399 * L_1 = ___arguments2;
		NullCheck(L_1);
		String_t* L_2 = ArgumentCache_get_unityObjectArgumentAssemblyTypeName_m3286213661(L_1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_3 = String_IsNullOrEmpty_m2969720369(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		if (L_3)
		{
			goto IL_003a;
		}
	}
	{
		ArgumentCache_t2187958399 * L_4 = ___arguments2;
		NullCheck(L_4);
		String_t* L_5 = ArgumentCache_get_unityObjectArgumentAssemblyTypeName_m3286213661(L_4, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_6 = il2cpp_codegen_get_type((Il2CppMethodPointer)&Type_GetType_m3605423543, L_5, (bool)0, "UnityEngine, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null");
		Type_t * L_7 = L_6;
		G_B2_0 = L_7;
		if (L_7)
		{
			G_B3_0 = L_7;
			goto IL_0039;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_8 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, LoadTypeToken(Object_t631007953_0_0_0_var), /*hidden argument*/NULL);
		G_B3_0 = L_8;
	}

IL_0039:
	{
		V_0 = G_B3_0;
	}

IL_003a:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_9 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, LoadTypeToken(CachedInvokableCall_1_t3153979999_0_0_0_var), /*hidden argument*/NULL);
		V_1 = L_9;
		Type_t * L_10 = V_1;
		TypeU5BU5D_t3940880105* L_11 = ((TypeU5BU5D_t3940880105*)SZArrayNew(TypeU5BU5D_t3940880105_il2cpp_TypeInfo_var, (uint32_t)1));
		Type_t * L_12 = V_0;
		NullCheck(L_11);
		ArrayElementTypeCheck (L_11, L_12);
		(L_11)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_12);
		NullCheck(L_10);
		Type_t * L_13 = VirtFuncInvoker1< Type_t *, TypeU5BU5D_t3940880105* >::Invoke(82 /* System.Type System.Type::MakeGenericType(System.Type[]) */, L_10, L_11);
		V_2 = L_13;
		Type_t * L_14 = V_2;
		TypeU5BU5D_t3940880105* L_15 = ((TypeU5BU5D_t3940880105*)SZArrayNew(TypeU5BU5D_t3940880105_il2cpp_TypeInfo_var, (uint32_t)3));
		Type_t * L_16 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, LoadTypeToken(Object_t631007953_0_0_0_var), /*hidden argument*/NULL);
		NullCheck(L_15);
		ArrayElementTypeCheck (L_15, L_16);
		(L_15)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_16);
		TypeU5BU5D_t3940880105* L_17 = L_15;
		Type_t * L_18 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, LoadTypeToken(MethodInfo_t_0_0_0_var), /*hidden argument*/NULL);
		NullCheck(L_17);
		ArrayElementTypeCheck (L_17, L_18);
		(L_17)->SetAt(static_cast<il2cpp_array_size_t>(1), (Type_t *)L_18);
		TypeU5BU5D_t3940880105* L_19 = L_17;
		Type_t * L_20 = V_0;
		NullCheck(L_19);
		ArrayElementTypeCheck (L_19, L_20);
		(L_19)->SetAt(static_cast<il2cpp_array_size_t>(2), (Type_t *)L_20);
		NullCheck(L_14);
		ConstructorInfo_t5769829 * L_21 = Type_GetConstructor_m2219014380(L_14, L_19, /*hidden argument*/NULL);
		V_3 = L_21;
		ArgumentCache_t2187958399 * L_22 = ___arguments2;
		NullCheck(L_22);
		Object_t631007953 * L_23 = ArgumentCache_get_unityObjectArgument_m164429709(L_22, /*hidden argument*/NULL);
		V_4 = L_23;
		Object_t631007953 * L_24 = V_4;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_25 = Object_op_Inequality_m1920811489(NULL /*static, unused*/, L_24, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_25)
		{
			goto IL_00ab;
		}
	}
	{
		Type_t * L_26 = V_0;
		Object_t631007953 * L_27 = V_4;
		NullCheck(L_27);
		Type_t * L_28 = Object_GetType_m88164663(L_27, /*hidden argument*/NULL);
		NullCheck(L_26);
		bool L_29 = VirtFuncInvoker1< bool, Type_t * >::Invoke(40 /* System.Boolean System.Type::IsAssignableFrom(System.Type) */, L_26, L_28);
		if (L_29)
		{
			goto IL_00ab;
		}
	}
	{
		V_4 = (Object_t631007953 *)NULL;
	}

IL_00ab:
	{
		ConstructorInfo_t5769829 * L_30 = V_3;
		ObjectU5BU5D_t2843939325* L_31 = ((ObjectU5BU5D_t2843939325*)SZArrayNew(ObjectU5BU5D_t2843939325_il2cpp_TypeInfo_var, (uint32_t)3));
		Object_t631007953 * L_32 = ___target0;
		NullCheck(L_31);
		ArrayElementTypeCheck (L_31, L_32);
		(L_31)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_32);
		ObjectU5BU5D_t2843939325* L_33 = L_31;
		MethodInfo_t * L_34 = ___method1;
		NullCheck(L_33);
		ArrayElementTypeCheck (L_33, L_34);
		(L_33)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_34);
		ObjectU5BU5D_t2843939325* L_35 = L_33;
		Object_t631007953 * L_36 = V_4;
		NullCheck(L_35);
		ArrayElementTypeCheck (L_35, L_36);
		(L_35)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)L_36);
		NullCheck(L_30);
		Il2CppObject * L_37 = ConstructorInfo_Invoke_m4089836896(L_30, L_35, /*hidden argument*/NULL);
		V_5 = ((BaseInvokableCall_t2703961024 *)IsInstClass(L_37, BaseInvokableCall_t2703961024_il2cpp_TypeInfo_var));
		goto IL_00d0;
	}

IL_00d0:
	{
		BaseInvokableCall_t2703961024 * L_38 = V_5;
		return L_38;
	}
}
// System.Void UnityEngine.Events.PersistentCallGroup::.ctor()
extern "C"  void PersistentCallGroup__ctor_m1755284370 (PersistentCallGroup_t3050769227 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PersistentCallGroup__ctor_m1755284370_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Object__ctor_m297566312(__this, /*hidden argument*/NULL);
		List_1_t584821570 * L_0 = (List_1_t584821570 *)il2cpp_codegen_object_new(List_1_t584821570_il2cpp_TypeInfo_var);
		List_1__ctor_m943763978(L_0, /*hidden argument*/List_1__ctor_m943763978_MethodInfo_var);
		__this->set_m_Calls_0(L_0);
		return;
	}
}
// System.Void UnityEngine.Events.PersistentCallGroup::Initialize(UnityEngine.Events.InvokableCallList,UnityEngine.Events.UnityEventBase)
extern "C"  void PersistentCallGroup_Initialize_m2614950721 (PersistentCallGroup_t3050769227 * __this, InvokableCallList_t2498835369 * ___invokableList0, UnityEventBase_t3960448221 * ___unityEventBase1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PersistentCallGroup_Initialize_m2614950721_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	PersistentCall_t3407714124 * V_0 = NULL;
	Enumerator_t2474065447  V_1;
	memset(&V_1, 0, sizeof(V_1));
	BaseInvokableCall_t2703961024 * V_2 = NULL;
	Exception_t1436737249 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1436737249 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		List_1_t584821570 * L_0 = __this->get_m_Calls_0();
		NullCheck(L_0);
		Enumerator_t2474065447  L_1 = List_1_GetEnumerator_m398210679(L_0, /*hidden argument*/List_1_GetEnumerator_m398210679_MethodInfo_var);
		V_1 = L_1;
	}

IL_000e:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0042;
		}

IL_0013:
		{
			PersistentCall_t3407714124 * L_2 = Enumerator_get_Current_m481949260((&V_1), /*hidden argument*/Enumerator_get_Current_m481949260_MethodInfo_var);
			V_0 = L_2;
			PersistentCall_t3407714124 * L_3 = V_0;
			NullCheck(L_3);
			bool L_4 = PersistentCall_IsValid_m3557513049(L_3, /*hidden argument*/NULL);
			if (L_4)
			{
				goto IL_002c;
			}
		}

IL_0027:
		{
			goto IL_0042;
		}

IL_002c:
		{
			PersistentCall_t3407714124 * L_5 = V_0;
			UnityEventBase_t3960448221 * L_6 = ___unityEventBase1;
			NullCheck(L_5);
			BaseInvokableCall_t2703961024 * L_7 = PersistentCall_GetRuntimeCall_m606139440(L_5, L_6, /*hidden argument*/NULL);
			V_2 = L_7;
			BaseInvokableCall_t2703961024 * L_8 = V_2;
			if (!L_8)
			{
				goto IL_0041;
			}
		}

IL_003a:
		{
			InvokableCallList_t2498835369 * L_9 = ___invokableList0;
			BaseInvokableCall_t2703961024 * L_10 = V_2;
			NullCheck(L_9);
			InvokableCallList_AddPersistentInvokableCall_m4020414894(L_9, L_10, /*hidden argument*/NULL);
		}

IL_0041:
		{
		}

IL_0042:
		{
			bool L_11 = Enumerator_MoveNext_m1429503053((&V_1), /*hidden argument*/Enumerator_MoveNext_m1429503053_MethodInfo_var);
			if (L_11)
			{
				goto IL_0013;
			}
		}

IL_004e:
		{
			IL2CPP_LEAVE(0x61, FINALLY_0053);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1436737249 *)e.ex;
		goto FINALLY_0053;
	}

FINALLY_0053:
	{ // begin finally (depth: 1)
		Enumerator_Dispose_m2540888223((&V_1), /*hidden argument*/Enumerator_Dispose_m2540888223_MethodInfo_var);
		IL2CPP_END_FINALLY(83)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(83)
	{
		IL2CPP_JUMP_TBL(0x61, IL_0061)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1436737249 *)
	}

IL_0061:
	{
		return;
	}
}
extern "C"  void DelegatePInvokeWrapper_UnityAction_t3245792599 (UnityAction_t3245792599 * __this, const MethodInfo* method)
{
	typedef void (STDCALL *PInvokeFunc)();
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(((Il2CppDelegate*)__this)->method->methodPointer);

	// Native function invocation
	il2cppPInvokeFunc();

}
// System.Void UnityEngine.Events.UnityAction::.ctor(System.Object,System.IntPtr)
extern "C"  void UnityAction__ctor_m2801230893 (UnityAction_t3245792599 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void UnityEngine.Events.UnityAction::Invoke()
extern "C"  void UnityAction_Invoke_m804868541 (UnityAction_t3245792599 * __this, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		UnityAction_Invoke_m804868541((UnityAction_t3245792599 *)__this->get_prev_9(), method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if ((__this->get_m_target_2() != NULL || MethodHasParameters((MethodInfo*)(__this->get_method_3().get_m_value_0()))) && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult UnityEngine.Events.UnityAction::BeginInvoke(System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * UnityAction_BeginInvoke_m3808280724 (UnityAction_t3245792599 * __this, AsyncCallback_t3962456242 * ___callback0, Il2CppObject * ___object1, const MethodInfo* method)
{
	void *__d_args[1] = {0};
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback0, (Il2CppObject*)___object1);
}
// System.Void UnityEngine.Events.UnityAction::EndInvoke(System.IAsyncResult)
extern "C"  void UnityAction_EndInvoke_m6333459 (UnityAction_t3245792599 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void UnityEngine.Events.UnityEvent::.ctor()
extern "C"  void UnityEvent__ctor_m974839563 (UnityEvent_t2581268647 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityEvent__ctor_m974839563_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		__this->set_m_InvokeArray_4(((ObjectU5BU5D_t2843939325*)SZArrayNew(ObjectU5BU5D_t2843939325_il2cpp_TypeInfo_var, (uint32_t)0)));
		UnityEventBase__ctor_m1087366165(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Events.UnityEvent::AddListener(UnityEngine.Events.UnityAction)
extern "C"  void UnityEvent_AddListener_m3274227256 (UnityEvent_t2581268647 * __this, UnityAction_t3245792599 * ___call0, const MethodInfo* method)
{
	{
		UnityAction_t3245792599 * L_0 = ___call0;
		BaseInvokableCall_t2703961024 * L_1 = UnityEvent_GetDelegate_m3910545044(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		UnityEventBase_AddCall_m3632990696(__this, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Events.UnityEvent::RemoveListener(UnityEngine.Events.UnityAction)
extern "C"  void UnityEvent_RemoveListener_m369190218 (UnityEvent_t2581268647 * __this, UnityAction_t3245792599 * ___call0, const MethodInfo* method)
{
	{
		UnityAction_t3245792599 * L_0 = ___call0;
		NullCheck(L_0);
		Il2CppObject * L_1 = Delegate_get_Target_m2361978888(L_0, /*hidden argument*/NULL);
		UnityAction_t3245792599 * L_2 = ___call0;
		MethodInfo_t * L_3 = NetFxCoreExtensions_GetMethodInfo_m3154548066(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		UnityEventBase_RemoveListener_m748247232(__this, L_1, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Reflection.MethodInfo UnityEngine.Events.UnityEvent::FindMethod_Impl(System.String,System.Object)
extern "C"  MethodInfo_t * UnityEvent_FindMethod_Impl_m3520635000 (UnityEvent_t2581268647 * __this, String_t* ___name0, Il2CppObject * ___targetObj1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityEvent_FindMethod_Impl_m3520635000_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	MethodInfo_t * V_0 = NULL;
	{
		Il2CppObject * L_0 = ___targetObj1;
		String_t* L_1 = ___name0;
		MethodInfo_t * L_2 = UnityEventBase_GetValidMethodInfo_m1761163145(NULL /*static, unused*/, L_0, L_1, ((TypeU5BU5D_t3940880105*)SZArrayNew(TypeU5BU5D_t3940880105_il2cpp_TypeInfo_var, (uint32_t)0)), /*hidden argument*/NULL);
		V_0 = L_2;
		goto IL_0014;
	}

IL_0014:
	{
		MethodInfo_t * L_3 = V_0;
		return L_3;
	}
}
// UnityEngine.Events.BaseInvokableCall UnityEngine.Events.UnityEvent::GetDelegate(System.Object,System.Reflection.MethodInfo)
extern "C"  BaseInvokableCall_t2703961024 * UnityEvent_GetDelegate_m3509379365 (UnityEvent_t2581268647 * __this, Il2CppObject * ___target0, MethodInfo_t * ___theFunction1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityEvent_GetDelegate_m3509379365_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	BaseInvokableCall_t2703961024 * V_0 = NULL;
	{
		Il2CppObject * L_0 = ___target0;
		MethodInfo_t * L_1 = ___theFunction1;
		InvokableCall_t832123510 * L_2 = (InvokableCall_t832123510 *)il2cpp_codegen_object_new(InvokableCall_t832123510_il2cpp_TypeInfo_var);
		InvokableCall__ctor_m2864671469(L_2, L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		goto IL_000e;
	}

IL_000e:
	{
		BaseInvokableCall_t2703961024 * L_3 = V_0;
		return L_3;
	}
}
// UnityEngine.Events.BaseInvokableCall UnityEngine.Events.UnityEvent::GetDelegate(UnityEngine.Events.UnityAction)
extern "C"  BaseInvokableCall_t2703961024 * UnityEvent_GetDelegate_m3910545044 (Il2CppObject * __this /* static, unused */, UnityAction_t3245792599 * ___action0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityEvent_GetDelegate_m3910545044_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	BaseInvokableCall_t2703961024 * V_0 = NULL;
	{
		UnityAction_t3245792599 * L_0 = ___action0;
		InvokableCall_t832123510 * L_1 = (InvokableCall_t832123510 *)il2cpp_codegen_object_new(InvokableCall_t832123510_il2cpp_TypeInfo_var);
		InvokableCall__ctor_m616326848(L_1, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_000d;
	}

IL_000d:
	{
		BaseInvokableCall_t2703961024 * L_2 = V_0;
		return L_2;
	}
}
// System.Void UnityEngine.Events.UnityEvent::Invoke()
extern "C"  void UnityEvent_Invoke_m1072399001 (UnityEvent_t2581268647 * __this, const MethodInfo* method)
{
	{
		ObjectU5BU5D_t2843939325* L_0 = __this->get_m_InvokeArray_4();
		UnityEventBase_Invoke_m3022009851(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Events.UnityEventBase::.ctor()
extern "C"  void UnityEventBase__ctor_m1087366165 (UnityEventBase_t3960448221 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityEventBase__ctor_m1087366165_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		__this->set_m_CallsDirty_3((bool)1);
		Object__ctor_m297566312(__this, /*hidden argument*/NULL);
		InvokableCallList_t2498835369 * L_0 = (InvokableCallList_t2498835369 *)il2cpp_codegen_object_new(InvokableCallList_t2498835369_il2cpp_TypeInfo_var);
		InvokableCallList__ctor_m2867488712(L_0, /*hidden argument*/NULL);
		__this->set_m_Calls_0(L_0);
		PersistentCallGroup_t3050769227 * L_1 = (PersistentCallGroup_t3050769227 *)il2cpp_codegen_object_new(PersistentCallGroup_t3050769227_il2cpp_TypeInfo_var);
		PersistentCallGroup__ctor_m1755284370(L_1, /*hidden argument*/NULL);
		__this->set_m_PersistentCalls_1(L_1);
		Type_t * L_2 = Object_GetType_m88164663(__this, /*hidden argument*/NULL);
		NullCheck(L_2);
		String_t* L_3 = VirtFuncInvoker0< String_t* >::Invoke(15 /* System.String System.Type::get_AssemblyQualifiedName() */, L_2);
		__this->set_m_TypeName_2(L_3);
		return;
	}
}
// System.Void UnityEngine.Events.UnityEventBase::UnityEngine.ISerializationCallbackReceiver.OnBeforeSerialize()
extern "C"  void UnityEventBase_UnityEngine_ISerializationCallbackReceiver_OnBeforeSerialize_m772227487 (UnityEventBase_t3960448221 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void UnityEngine.Events.UnityEventBase::UnityEngine.ISerializationCallbackReceiver.OnAfterDeserialize()
extern "C"  void UnityEventBase_UnityEngine_ISerializationCallbackReceiver_OnAfterDeserialize_m901668830 (UnityEventBase_t3960448221 * __this, const MethodInfo* method)
{
	{
		UnityEventBase_DirtyPersistentCalls_m3787457104(__this, /*hidden argument*/NULL);
		Type_t * L_0 = Object_GetType_m88164663(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		String_t* L_1 = VirtFuncInvoker0< String_t* >::Invoke(15 /* System.String System.Type::get_AssemblyQualifiedName() */, L_0);
		__this->set_m_TypeName_2(L_1);
		return;
	}
}
// System.Reflection.MethodInfo UnityEngine.Events.UnityEventBase::FindMethod(UnityEngine.Events.PersistentCall)
extern "C"  MethodInfo_t * UnityEventBase_FindMethod_m1055565284 (UnityEventBase_t3960448221 * __this, PersistentCall_t3407714124 * ___call0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityEventBase_FindMethod_m1055565284_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Type_t * V_0 = NULL;
	MethodInfo_t * V_1 = NULL;
	Type_t * G_B3_0 = NULL;
	Type_t * G_B2_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, LoadTypeToken(Object_t631007953_0_0_0_var), /*hidden argument*/NULL);
		V_0 = L_0;
		PersistentCall_t3407714124 * L_1 = ___call0;
		NullCheck(L_1);
		ArgumentCache_t2187958399 * L_2 = PersistentCall_get_arguments_m1263638726(L_1, /*hidden argument*/NULL);
		NullCheck(L_2);
		String_t* L_3 = ArgumentCache_get_unityObjectArgumentAssemblyTypeName_m3286213661(L_2, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_4 = String_IsNullOrEmpty_m2969720369(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		if (L_4)
		{
			goto IL_0044;
		}
	}
	{
		PersistentCall_t3407714124 * L_5 = ___call0;
		NullCheck(L_5);
		ArgumentCache_t2187958399 * L_6 = PersistentCall_get_arguments_m1263638726(L_5, /*hidden argument*/NULL);
		NullCheck(L_6);
		String_t* L_7 = ArgumentCache_get_unityObjectArgumentAssemblyTypeName_m3286213661(L_6, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_8 = il2cpp_codegen_get_type((Il2CppMethodPointer)&Type_GetType_m3605423543, L_7, (bool)0, "UnityEngine, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null");
		Type_t * L_9 = L_8;
		G_B2_0 = L_9;
		if (L_9)
		{
			G_B3_0 = L_9;
			goto IL_0043;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_10 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, LoadTypeToken(Object_t631007953_0_0_0_var), /*hidden argument*/NULL);
		G_B3_0 = L_10;
	}

IL_0043:
	{
		V_0 = G_B3_0;
	}

IL_0044:
	{
		PersistentCall_t3407714124 * L_11 = ___call0;
		NullCheck(L_11);
		String_t* L_12 = PersistentCall_get_methodName_m2181722332(L_11, /*hidden argument*/NULL);
		PersistentCall_t3407714124 * L_13 = ___call0;
		NullCheck(L_13);
		Object_t631007953 * L_14 = PersistentCall_get_target_m2795613068(L_13, /*hidden argument*/NULL);
		PersistentCall_t3407714124 * L_15 = ___call0;
		NullCheck(L_15);
		int32_t L_16 = PersistentCall_get_mode_m1368131393(L_15, /*hidden argument*/NULL);
		Type_t * L_17 = V_0;
		MethodInfo_t * L_18 = UnityEventBase_FindMethod_m995544650(__this, L_12, L_14, L_16, L_17, /*hidden argument*/NULL);
		V_1 = L_18;
		goto IL_0063;
	}

IL_0063:
	{
		MethodInfo_t * L_19 = V_1;
		return L_19;
	}
}
// System.Reflection.MethodInfo UnityEngine.Events.UnityEventBase::FindMethod(System.String,System.Object,UnityEngine.Events.PersistentListenerMode,System.Type)
extern "C"  MethodInfo_t * UnityEventBase_FindMethod_m995544650 (UnityEventBase_t3960448221 * __this, String_t* ___name0, Il2CppObject * ___listener1, int32_t ___mode2, Type_t * ___argumentType3, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityEventBase_FindMethod_m995544650_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	MethodInfo_t * V_0 = NULL;
	Type_t * G_B10_0 = NULL;
	int32_t G_B10_1 = 0;
	TypeU5BU5D_t3940880105* G_B10_2 = NULL;
	TypeU5BU5D_t3940880105* G_B10_3 = NULL;
	String_t* G_B10_4 = NULL;
	Il2CppObject * G_B10_5 = NULL;
	Type_t * G_B9_0 = NULL;
	int32_t G_B9_1 = 0;
	TypeU5BU5D_t3940880105* G_B9_2 = NULL;
	TypeU5BU5D_t3940880105* G_B9_3 = NULL;
	String_t* G_B9_4 = NULL;
	Il2CppObject * G_B9_5 = NULL;
	{
		int32_t L_0 = ___mode2;
		switch (L_0)
		{
			case 0:
			{
				goto IL_0028;
			}
			case 1:
			{
				goto IL_0036;
			}
			case 2:
			{
				goto IL_00c9;
			}
			case 3:
			{
				goto IL_0069;
			}
			case 4:
			{
				goto IL_0049;
			}
			case 5:
			{
				goto IL_00a9;
			}
			case 6:
			{
				goto IL_0089;
			}
		}
	}
	{
		goto IL_00f2;
	}

IL_0028:
	{
		String_t* L_1 = ___name0;
		Il2CppObject * L_2 = ___listener1;
		MethodInfo_t * L_3 = VirtFuncInvoker2< MethodInfo_t *, String_t*, Il2CppObject * >::Invoke(6 /* System.Reflection.MethodInfo UnityEngine.Events.UnityEventBase::FindMethod_Impl(System.String,System.Object) */, __this, L_1, L_2);
		V_0 = L_3;
		goto IL_00f9;
	}

IL_0036:
	{
		Il2CppObject * L_4 = ___listener1;
		String_t* L_5 = ___name0;
		MethodInfo_t * L_6 = UnityEventBase_GetValidMethodInfo_m1761163145(NULL /*static, unused*/, L_4, L_5, ((TypeU5BU5D_t3940880105*)SZArrayNew(TypeU5BU5D_t3940880105_il2cpp_TypeInfo_var, (uint32_t)0)), /*hidden argument*/NULL);
		V_0 = L_6;
		goto IL_00f9;
	}

IL_0049:
	{
		Il2CppObject * L_7 = ___listener1;
		String_t* L_8 = ___name0;
		TypeU5BU5D_t3940880105* L_9 = ((TypeU5BU5D_t3940880105*)SZArrayNew(TypeU5BU5D_t3940880105_il2cpp_TypeInfo_var, (uint32_t)1));
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_10 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, LoadTypeToken(Single_t1397266774_0_0_0_var), /*hidden argument*/NULL);
		NullCheck(L_9);
		ArrayElementTypeCheck (L_9, L_10);
		(L_9)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_10);
		MethodInfo_t * L_11 = UnityEventBase_GetValidMethodInfo_m1761163145(NULL /*static, unused*/, L_7, L_8, L_9, /*hidden argument*/NULL);
		V_0 = L_11;
		goto IL_00f9;
	}

IL_0069:
	{
		Il2CppObject * L_12 = ___listener1;
		String_t* L_13 = ___name0;
		TypeU5BU5D_t3940880105* L_14 = ((TypeU5BU5D_t3940880105*)SZArrayNew(TypeU5BU5D_t3940880105_il2cpp_TypeInfo_var, (uint32_t)1));
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_15 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, LoadTypeToken(Int32_t2950945753_0_0_0_var), /*hidden argument*/NULL);
		NullCheck(L_14);
		ArrayElementTypeCheck (L_14, L_15);
		(L_14)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_15);
		MethodInfo_t * L_16 = UnityEventBase_GetValidMethodInfo_m1761163145(NULL /*static, unused*/, L_12, L_13, L_14, /*hidden argument*/NULL);
		V_0 = L_16;
		goto IL_00f9;
	}

IL_0089:
	{
		Il2CppObject * L_17 = ___listener1;
		String_t* L_18 = ___name0;
		TypeU5BU5D_t3940880105* L_19 = ((TypeU5BU5D_t3940880105*)SZArrayNew(TypeU5BU5D_t3940880105_il2cpp_TypeInfo_var, (uint32_t)1));
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_20 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, LoadTypeToken(Boolean_t97287965_0_0_0_var), /*hidden argument*/NULL);
		NullCheck(L_19);
		ArrayElementTypeCheck (L_19, L_20);
		(L_19)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_20);
		MethodInfo_t * L_21 = UnityEventBase_GetValidMethodInfo_m1761163145(NULL /*static, unused*/, L_17, L_18, L_19, /*hidden argument*/NULL);
		V_0 = L_21;
		goto IL_00f9;
	}

IL_00a9:
	{
		Il2CppObject * L_22 = ___listener1;
		String_t* L_23 = ___name0;
		TypeU5BU5D_t3940880105* L_24 = ((TypeU5BU5D_t3940880105*)SZArrayNew(TypeU5BU5D_t3940880105_il2cpp_TypeInfo_var, (uint32_t)1));
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_25 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, LoadTypeToken(String_t_0_0_0_var), /*hidden argument*/NULL);
		NullCheck(L_24);
		ArrayElementTypeCheck (L_24, L_25);
		(L_24)->SetAt(static_cast<il2cpp_array_size_t>(0), (Type_t *)L_25);
		MethodInfo_t * L_26 = UnityEventBase_GetValidMethodInfo_m1761163145(NULL /*static, unused*/, L_22, L_23, L_24, /*hidden argument*/NULL);
		V_0 = L_26;
		goto IL_00f9;
	}

IL_00c9:
	{
		Il2CppObject * L_27 = ___listener1;
		String_t* L_28 = ___name0;
		TypeU5BU5D_t3940880105* L_29 = ((TypeU5BU5D_t3940880105*)SZArrayNew(TypeU5BU5D_t3940880105_il2cpp_TypeInfo_var, (uint32_t)1));
		Type_t * L_30 = ___argumentType3;
		Type_t * L_31 = L_30;
		G_B9_0 = L_31;
		G_B9_1 = 0;
		G_B9_2 = L_29;
		G_B9_3 = L_29;
		G_B9_4 = L_28;
		G_B9_5 = L_27;
		if (L_31)
		{
			G_B10_0 = L_31;
			G_B10_1 = 0;
			G_B10_2 = L_29;
			G_B10_3 = L_29;
			G_B10_4 = L_28;
			G_B10_5 = L_27;
			goto IL_00e6;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_32 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, LoadTypeToken(Object_t631007953_0_0_0_var), /*hidden argument*/NULL);
		G_B10_0 = L_32;
		G_B10_1 = G_B9_1;
		G_B10_2 = G_B9_2;
		G_B10_3 = G_B9_3;
		G_B10_4 = G_B9_4;
		G_B10_5 = G_B9_5;
	}

IL_00e6:
	{
		NullCheck(G_B10_2);
		ArrayElementTypeCheck (G_B10_2, G_B10_0);
		(G_B10_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B10_1), (Type_t *)G_B10_0);
		MethodInfo_t * L_33 = UnityEventBase_GetValidMethodInfo_m1761163145(NULL /*static, unused*/, G_B10_5, G_B10_4, G_B10_3, /*hidden argument*/NULL);
		V_0 = L_33;
		goto IL_00f9;
	}

IL_00f2:
	{
		V_0 = (MethodInfo_t *)NULL;
		goto IL_00f9;
	}

IL_00f9:
	{
		MethodInfo_t * L_34 = V_0;
		return L_34;
	}
}
// System.Void UnityEngine.Events.UnityEventBase::DirtyPersistentCalls()
extern "C"  void UnityEventBase_DirtyPersistentCalls_m3787457104 (UnityEventBase_t3960448221 * __this, const MethodInfo* method)
{
	{
		InvokableCallList_t2498835369 * L_0 = __this->get_m_Calls_0();
		NullCheck(L_0);
		InvokableCallList_ClearPersistent_m3446223403(L_0, /*hidden argument*/NULL);
		__this->set_m_CallsDirty_3((bool)1);
		return;
	}
}
// System.Void UnityEngine.Events.UnityEventBase::RebuildPersistentCallsIfNeeded()
extern "C"  void UnityEventBase_RebuildPersistentCallsIfNeeded_m1560510243 (UnityEventBase_t3960448221 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get_m_CallsDirty_3();
		if (!L_0)
		{
			goto IL_0027;
		}
	}
	{
		PersistentCallGroup_t3050769227 * L_1 = __this->get_m_PersistentCalls_1();
		InvokableCallList_t2498835369 * L_2 = __this->get_m_Calls_0();
		NullCheck(L_1);
		PersistentCallGroup_Initialize_m2614950721(L_1, L_2, __this, /*hidden argument*/NULL);
		__this->set_m_CallsDirty_3((bool)0);
	}

IL_0027:
	{
		return;
	}
}
// System.Void UnityEngine.Events.UnityEventBase::AddCall(UnityEngine.Events.BaseInvokableCall)
extern "C"  void UnityEventBase_AddCall_m3632990696 (UnityEventBase_t3960448221 * __this, BaseInvokableCall_t2703961024 * ___call0, const MethodInfo* method)
{
	{
		InvokableCallList_t2498835369 * L_0 = __this->get_m_Calls_0();
		BaseInvokableCall_t2703961024 * L_1 = ___call0;
		NullCheck(L_0);
		InvokableCallList_AddListener_m4292769145(L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Events.UnityEventBase::RemoveListener(System.Object,System.Reflection.MethodInfo)
extern "C"  void UnityEventBase_RemoveListener_m748247232 (UnityEventBase_t3960448221 * __this, Il2CppObject * ___targetObj0, MethodInfo_t * ___method1, const MethodInfo* method)
{
	{
		InvokableCallList_t2498835369 * L_0 = __this->get_m_Calls_0();
		Il2CppObject * L_1 = ___targetObj0;
		MethodInfo_t * L_2 = ___method1;
		NullCheck(L_0);
		InvokableCallList_RemoveListener_m1974000505(L_0, L_1, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Events.UnityEventBase::Invoke(System.Object[])
extern "C"  void UnityEventBase_Invoke_m3022009851 (UnityEventBase_t3960448221 * __this, ObjectU5BU5D_t2843939325* ___parameters0, const MethodInfo* method)
{
	{
		UnityEventBase_RebuildPersistentCallsIfNeeded_m1560510243(__this, /*hidden argument*/NULL);
		InvokableCallList_t2498835369 * L_0 = __this->get_m_Calls_0();
		ObjectU5BU5D_t2843939325* L_1 = ___parameters0;
		NullCheck(L_0);
		InvokableCallList_Invoke_m2795703874(L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.String UnityEngine.Events.UnityEventBase::ToString()
extern "C"  String_t* UnityEventBase_ToString_m3800562853 (UnityEventBase_t3960448221 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityEventBase_ToString_m3800562853_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	{
		String_t* L_0 = Object_ToString_m1740002499(__this, /*hidden argument*/NULL);
		Type_t * L_1 = Object_GetType_m88164663(__this, /*hidden argument*/NULL);
		NullCheck(L_1);
		String_t* L_2 = VirtFuncInvoker0< String_t* >::Invoke(18 /* System.String System.Type::get_FullName() */, L_1);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_3 = String_Concat_m3755062657(NULL /*static, unused*/, L_0, _stringLiteral3452614528, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		goto IL_0022;
	}

IL_0022:
	{
		String_t* L_4 = V_0;
		return L_4;
	}
}
// System.Reflection.MethodInfo UnityEngine.Events.UnityEventBase::GetValidMethodInfo(System.Object,System.String,System.Type[])
extern "C"  MethodInfo_t * UnityEventBase_GetValidMethodInfo_m1761163145 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___obj0, String_t* ___functionName1, TypeU5BU5D_t3940880105* ___argumentTypes2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityEventBase_GetValidMethodInfo_m1761163145_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Type_t * V_0 = NULL;
	MethodInfo_t * V_1 = NULL;
	ParameterInfoU5BU5D_t390618515* V_2 = NULL;
	bool V_3 = false;
	int32_t V_4 = 0;
	ParameterInfo_t1861056598 * V_5 = NULL;
	ParameterInfoU5BU5D_t390618515* V_6 = NULL;
	int32_t V_7 = 0;
	Type_t * V_8 = NULL;
	Type_t * V_9 = NULL;
	MethodInfo_t * V_10 = NULL;
	{
		Il2CppObject * L_0 = ___obj0;
		NullCheck(L_0);
		Type_t * L_1 = Object_GetType_m88164663(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_009c;
	}

IL_000d:
	{
		Type_t * L_2 = V_0;
		String_t* L_3 = ___functionName1;
		TypeU5BU5D_t3940880105* L_4 = ___argumentTypes2;
		NullCheck(L_2);
		MethodInfo_t * L_5 = Type_GetMethod_m637078096(L_2, L_3, ((int32_t)52), (Binder_t2999457153 *)NULL, L_4, (ParameterModifierU5BU5D_t2943407543*)(ParameterModifierU5BU5D_t2943407543*)NULL, /*hidden argument*/NULL);
		V_1 = L_5;
		MethodInfo_t * L_6 = V_1;
		if (!L_6)
		{
			goto IL_0094;
		}
	}
	{
		MethodInfo_t * L_7 = V_1;
		NullCheck(L_7);
		ParameterInfoU5BU5D_t390618515* L_8 = VirtFuncInvoker0< ParameterInfoU5BU5D_t390618515* >::Invoke(14 /* System.Reflection.ParameterInfo[] System.Reflection.MethodBase::GetParameters() */, L_7);
		V_2 = L_8;
		V_3 = (bool)1;
		V_4 = 0;
		ParameterInfoU5BU5D_t390618515* L_9 = V_2;
		V_6 = L_9;
		V_7 = 0;
		goto IL_007a;
	}

IL_003a:
	{
		ParameterInfoU5BU5D_t390618515* L_10 = V_6;
		int32_t L_11 = V_7;
		NullCheck(L_10);
		int32_t L_12 = L_11;
		ParameterInfo_t1861056598 * L_13 = (L_10)->GetAt(static_cast<il2cpp_array_size_t>(L_12));
		V_5 = L_13;
		TypeU5BU5D_t3940880105* L_14 = ___argumentTypes2;
		int32_t L_15 = V_4;
		NullCheck(L_14);
		int32_t L_16 = L_15;
		Type_t * L_17 = (L_14)->GetAt(static_cast<il2cpp_array_size_t>(L_16));
		V_8 = L_17;
		ParameterInfo_t1861056598 * L_18 = V_5;
		NullCheck(L_18);
		Type_t * L_19 = VirtFuncInvoker0< Type_t * >::Invoke(6 /* System.Type System.Reflection.ParameterInfo::get_ParameterType() */, L_18);
		V_9 = L_19;
		Type_t * L_20 = V_8;
		NullCheck(L_20);
		bool L_21 = Type_get_IsPrimitive_m1114712797(L_20, /*hidden argument*/NULL);
		Type_t * L_22 = V_9;
		NullCheck(L_22);
		bool L_23 = Type_get_IsPrimitive_m1114712797(L_22, /*hidden argument*/NULL);
		V_3 = (bool)((((int32_t)L_21) == ((int32_t)L_23))? 1 : 0);
		bool L_24 = V_3;
		if (L_24)
		{
			goto IL_006d;
		}
	}
	{
		goto IL_0085;
	}

IL_006d:
	{
		int32_t L_25 = V_4;
		V_4 = ((int32_t)((int32_t)L_25+(int32_t)1));
		int32_t L_26 = V_7;
		V_7 = ((int32_t)((int32_t)L_26+(int32_t)1));
	}

IL_007a:
	{
		int32_t L_27 = V_7;
		ParameterInfoU5BU5D_t390618515* L_28 = V_6;
		NullCheck(L_28);
		if ((((int32_t)L_27) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_28)->max_length)))))))
		{
			goto IL_003a;
		}
	}

IL_0085:
	{
		bool L_29 = V_3;
		if (!L_29)
		{
			goto IL_0093;
		}
	}
	{
		MethodInfo_t * L_30 = V_1;
		V_10 = L_30;
		goto IL_00ba;
	}

IL_0093:
	{
	}

IL_0094:
	{
		Type_t * L_31 = V_0;
		NullCheck(L_31);
		Type_t * L_32 = VirtFuncInvoker0< Type_t * >::Invoke(17 /* System.Type System.Type::get_BaseType() */, L_31);
		V_0 = L_32;
	}

IL_009c:
	{
		Type_t * L_33 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_34 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, LoadTypeToken(Il2CppObject_0_0_0_var), /*hidden argument*/NULL);
		if ((((Il2CppObject*)(Type_t *)L_33) == ((Il2CppObject*)(Type_t *)L_34)))
		{
			goto IL_00b2;
		}
	}
	{
		Type_t * L_35 = V_0;
		if (L_35)
		{
			goto IL_000d;
		}
	}

IL_00b2:
	{
		V_10 = (MethodInfo_t *)NULL;
		goto IL_00ba;
	}

IL_00ba:
	{
		MethodInfo_t * L_36 = V_10;
		return L_36;
	}
}
// System.Void UnityEngine.ExecuteInEditMode::.ctor()
extern "C"  void ExecuteInEditMode__ctor_m2328450127 (ExecuteInEditMode_t3727731349 * __this, const MethodInfo* method)
{
	{
		Attribute__ctor_m1529526131(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Experimental.Director.AnimationClipPlayable::.ctor()
extern "C"  void AnimationClipPlayable__ctor_m1749856595 (AnimationClipPlayable_t2591841208 * __this, const MethodInfo* method)
{
	{
		AnimationPlayable__ctor_m1573048877(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.AnimationClip UnityEngine.Experimental.Director.AnimationClipPlayable::get_clip()
extern "C"  AnimationClip_t2318505987 * AnimationClipPlayable_get_clip_m3223536140 (AnimationClipPlayable_t2591841208 * __this, const MethodInfo* method)
{
	AnimationClip_t2318505987 * V_0 = NULL;
	{
		PlayableHandle_t882318307 * L_0 = ((Playable_t3858037524 *)__this)->get_address_of_handle_0();
		AnimationClip_t2318505987 * L_1 = AnimationClipPlayable_GetAnimationClip_m3645124204(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_0012;
	}

IL_0012:
	{
		AnimationClip_t2318505987 * L_2 = V_0;
		return L_2;
	}
}
// System.Single UnityEngine.Experimental.Director.AnimationClipPlayable::get_speed()
extern "C"  float AnimationClipPlayable_get_speed_m2278481801 (AnimationClipPlayable_t2591841208 * __this, const MethodInfo* method)
{
	float V_0 = 0.0f;
	{
		PlayableHandle_t882318307 * L_0 = ((Playable_t3858037524 *)__this)->get_address_of_handle_0();
		float L_1 = AnimationClipPlayable_GetSpeed_m2357489435(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_0012;
	}

IL_0012:
	{
		float L_2 = V_0;
		return L_2;
	}
}
// System.Void UnityEngine.Experimental.Director.AnimationClipPlayable::set_speed(System.Single)
extern "C"  void AnimationClipPlayable_set_speed_m3460851161 (AnimationClipPlayable_t2591841208 * __this, float ___value0, const MethodInfo* method)
{
	{
		PlayableHandle_t882318307 * L_0 = ((Playable_t3858037524 *)__this)->get_address_of_handle_0();
		float L_1 = ___value0;
		AnimationClipPlayable_SetSpeed_m403221215(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean UnityEngine.Experimental.Director.AnimationClipPlayable::get_applyFootIK()
extern "C"  bool AnimationClipPlayable_get_applyFootIK_m4158232533 (AnimationClipPlayable_t2591841208 * __this, const MethodInfo* method)
{
	bool V_0 = false;
	{
		PlayableHandle_t882318307 * L_0 = ((Playable_t3858037524 *)__this)->get_address_of_handle_0();
		bool L_1 = AnimationClipPlayable_GetApplyFootIK_m2625007350(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_0012;
	}

IL_0012:
	{
		bool L_2 = V_0;
		return L_2;
	}
}
// System.Void UnityEngine.Experimental.Director.AnimationClipPlayable::set_applyFootIK(System.Boolean)
extern "C"  void AnimationClipPlayable_set_applyFootIK_m2263311083 (AnimationClipPlayable_t2591841208 * __this, bool ___value0, const MethodInfo* method)
{
	{
		PlayableHandle_t882318307 * L_0 = ((Playable_t3858037524 *)__this)->get_address_of_handle_0();
		bool L_1 = ___value0;
		AnimationClipPlayable_SetApplyFootIK_m3447148980(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean UnityEngine.Experimental.Director.AnimationClipPlayable::get_removeStartOffset()
extern "C"  bool AnimationClipPlayable_get_removeStartOffset_m3026807538 (AnimationClipPlayable_t2591841208 * __this, const MethodInfo* method)
{
	bool V_0 = false;
	{
		PlayableHandle_t882318307 * L_0 = ((Playable_t3858037524 *)__this)->get_address_of_handle_0();
		bool L_1 = AnimationClipPlayable_GetRemoveStartOffset_m212565688(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_0012;
	}

IL_0012:
	{
		bool L_2 = V_0;
		return L_2;
	}
}
// System.Void UnityEngine.Experimental.Director.AnimationClipPlayable::set_removeStartOffset(System.Boolean)
extern "C"  void AnimationClipPlayable_set_removeStartOffset_m2747487870 (AnimationClipPlayable_t2591841208 * __this, bool ___value0, const MethodInfo* method)
{
	{
		PlayableHandle_t882318307 * L_0 = ((Playable_t3858037524 *)__this)->get_address_of_handle_0();
		bool L_1 = ___value0;
		AnimationClipPlayable_SetRemoveStartOffset_m806931619(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.AnimationClip UnityEngine.Experimental.Director.AnimationClipPlayable::GetAnimationClip(UnityEngine.Experimental.Director.PlayableHandle&)
extern "C"  AnimationClip_t2318505987 * AnimationClipPlayable_GetAnimationClip_m3645124204 (Il2CppObject * __this /* static, unused */, PlayableHandle_t882318307 * ___handle0, const MethodInfo* method)
{
	AnimationClip_t2318505987 * V_0 = NULL;
	{
		PlayableHandle_t882318307 * L_0 = ___handle0;
		AnimationClip_t2318505987 * L_1 = AnimationClipPlayable_INTERNAL_CALL_GetAnimationClip_m2280195531(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_000d;
	}

IL_000d:
	{
		AnimationClip_t2318505987 * L_2 = V_0;
		return L_2;
	}
}
// UnityEngine.AnimationClip UnityEngine.Experimental.Director.AnimationClipPlayable::INTERNAL_CALL_GetAnimationClip(UnityEngine.Experimental.Director.PlayableHandle&)
extern "C"  AnimationClip_t2318505987 * AnimationClipPlayable_INTERNAL_CALL_GetAnimationClip_m2280195531 (Il2CppObject * __this /* static, unused */, PlayableHandle_t882318307 * ___handle0, const MethodInfo* method)
{
	typedef AnimationClip_t2318505987 * (*AnimationClipPlayable_INTERNAL_CALL_GetAnimationClip_m2280195531_ftn) (PlayableHandle_t882318307 *);
	static AnimationClipPlayable_INTERNAL_CALL_GetAnimationClip_m2280195531_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AnimationClipPlayable_INTERNAL_CALL_GetAnimationClip_m2280195531_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Experimental.Director.AnimationClipPlayable::INTERNAL_CALL_GetAnimationClip(UnityEngine.Experimental.Director.PlayableHandle&)");
	return _il2cpp_icall_func(___handle0);
}
// System.Single UnityEngine.Experimental.Director.AnimationClipPlayable::GetSpeed(UnityEngine.Experimental.Director.PlayableHandle&)
extern "C"  float AnimationClipPlayable_GetSpeed_m2357489435 (Il2CppObject * __this /* static, unused */, PlayableHandle_t882318307 * ___handle0, const MethodInfo* method)
{
	float V_0 = 0.0f;
	{
		PlayableHandle_t882318307 * L_0 = ___handle0;
		float L_1 = AnimationClipPlayable_INTERNAL_CALL_GetSpeed_m2906631008(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_000d;
	}

IL_000d:
	{
		float L_2 = V_0;
		return L_2;
	}
}
// System.Single UnityEngine.Experimental.Director.AnimationClipPlayable::INTERNAL_CALL_GetSpeed(UnityEngine.Experimental.Director.PlayableHandle&)
extern "C"  float AnimationClipPlayable_INTERNAL_CALL_GetSpeed_m2906631008 (Il2CppObject * __this /* static, unused */, PlayableHandle_t882318307 * ___handle0, const MethodInfo* method)
{
	typedef float (*AnimationClipPlayable_INTERNAL_CALL_GetSpeed_m2906631008_ftn) (PlayableHandle_t882318307 *);
	static AnimationClipPlayable_INTERNAL_CALL_GetSpeed_m2906631008_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AnimationClipPlayable_INTERNAL_CALL_GetSpeed_m2906631008_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Experimental.Director.AnimationClipPlayable::INTERNAL_CALL_GetSpeed(UnityEngine.Experimental.Director.PlayableHandle&)");
	return _il2cpp_icall_func(___handle0);
}
// System.Void UnityEngine.Experimental.Director.AnimationClipPlayable::SetSpeed(UnityEngine.Experimental.Director.PlayableHandle&,System.Single)
extern "C"  void AnimationClipPlayable_SetSpeed_m403221215 (Il2CppObject * __this /* static, unused */, PlayableHandle_t882318307 * ___handle0, float ___value1, const MethodInfo* method)
{
	{
		PlayableHandle_t882318307 * L_0 = ___handle0;
		float L_1 = ___value1;
		AnimationClipPlayable_INTERNAL_CALL_SetSpeed_m1493580406(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Experimental.Director.AnimationClipPlayable::INTERNAL_CALL_SetSpeed(UnityEngine.Experimental.Director.PlayableHandle&,System.Single)
extern "C"  void AnimationClipPlayable_INTERNAL_CALL_SetSpeed_m1493580406 (Il2CppObject * __this /* static, unused */, PlayableHandle_t882318307 * ___handle0, float ___value1, const MethodInfo* method)
{
	typedef void (*AnimationClipPlayable_INTERNAL_CALL_SetSpeed_m1493580406_ftn) (PlayableHandle_t882318307 *, float);
	static AnimationClipPlayable_INTERNAL_CALL_SetSpeed_m1493580406_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AnimationClipPlayable_INTERNAL_CALL_SetSpeed_m1493580406_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Experimental.Director.AnimationClipPlayable::INTERNAL_CALL_SetSpeed(UnityEngine.Experimental.Director.PlayableHandle&,System.Single)");
	_il2cpp_icall_func(___handle0, ___value1);
}
// System.Boolean UnityEngine.Experimental.Director.AnimationClipPlayable::GetApplyFootIK(UnityEngine.Experimental.Director.PlayableHandle&)
extern "C"  bool AnimationClipPlayable_GetApplyFootIK_m2625007350 (Il2CppObject * __this /* static, unused */, PlayableHandle_t882318307 * ___handle0, const MethodInfo* method)
{
	bool V_0 = false;
	{
		PlayableHandle_t882318307 * L_0 = ___handle0;
		bool L_1 = AnimationClipPlayable_INTERNAL_CALL_GetApplyFootIK_m1802268754(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_000d;
	}

IL_000d:
	{
		bool L_2 = V_0;
		return L_2;
	}
}
// System.Boolean UnityEngine.Experimental.Director.AnimationClipPlayable::INTERNAL_CALL_GetApplyFootIK(UnityEngine.Experimental.Director.PlayableHandle&)
extern "C"  bool AnimationClipPlayable_INTERNAL_CALL_GetApplyFootIK_m1802268754 (Il2CppObject * __this /* static, unused */, PlayableHandle_t882318307 * ___handle0, const MethodInfo* method)
{
	typedef bool (*AnimationClipPlayable_INTERNAL_CALL_GetApplyFootIK_m1802268754_ftn) (PlayableHandle_t882318307 *);
	static AnimationClipPlayable_INTERNAL_CALL_GetApplyFootIK_m1802268754_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AnimationClipPlayable_INTERNAL_CALL_GetApplyFootIK_m1802268754_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Experimental.Director.AnimationClipPlayable::INTERNAL_CALL_GetApplyFootIK(UnityEngine.Experimental.Director.PlayableHandle&)");
	return _il2cpp_icall_func(___handle0);
}
// System.Void UnityEngine.Experimental.Director.AnimationClipPlayable::SetApplyFootIK(UnityEngine.Experimental.Director.PlayableHandle&,System.Boolean)
extern "C"  void AnimationClipPlayable_SetApplyFootIK_m3447148980 (Il2CppObject * __this /* static, unused */, PlayableHandle_t882318307 * ___handle0, bool ___value1, const MethodInfo* method)
{
	{
		PlayableHandle_t882318307 * L_0 = ___handle0;
		bool L_1 = ___value1;
		AnimationClipPlayable_INTERNAL_CALL_SetApplyFootIK_m2254986524(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Experimental.Director.AnimationClipPlayable::INTERNAL_CALL_SetApplyFootIK(UnityEngine.Experimental.Director.PlayableHandle&,System.Boolean)
extern "C"  void AnimationClipPlayable_INTERNAL_CALL_SetApplyFootIK_m2254986524 (Il2CppObject * __this /* static, unused */, PlayableHandle_t882318307 * ___handle0, bool ___value1, const MethodInfo* method)
{
	typedef void (*AnimationClipPlayable_INTERNAL_CALL_SetApplyFootIK_m2254986524_ftn) (PlayableHandle_t882318307 *, bool);
	static AnimationClipPlayable_INTERNAL_CALL_SetApplyFootIK_m2254986524_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AnimationClipPlayable_INTERNAL_CALL_SetApplyFootIK_m2254986524_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Experimental.Director.AnimationClipPlayable::INTERNAL_CALL_SetApplyFootIK(UnityEngine.Experimental.Director.PlayableHandle&,System.Boolean)");
	_il2cpp_icall_func(___handle0, ___value1);
}
// System.Boolean UnityEngine.Experimental.Director.AnimationClipPlayable::GetRemoveStartOffset(UnityEngine.Experimental.Director.PlayableHandle&)
extern "C"  bool AnimationClipPlayable_GetRemoveStartOffset_m212565688 (Il2CppObject * __this /* static, unused */, PlayableHandle_t882318307 * ___handle0, const MethodInfo* method)
{
	bool V_0 = false;
	{
		PlayableHandle_t882318307 * L_0 = ___handle0;
		bool L_1 = AnimationClipPlayable_INTERNAL_CALL_GetRemoveStartOffset_m1179097678(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_000d;
	}

IL_000d:
	{
		bool L_2 = V_0;
		return L_2;
	}
}
// System.Boolean UnityEngine.Experimental.Director.AnimationClipPlayable::INTERNAL_CALL_GetRemoveStartOffset(UnityEngine.Experimental.Director.PlayableHandle&)
extern "C"  bool AnimationClipPlayable_INTERNAL_CALL_GetRemoveStartOffset_m1179097678 (Il2CppObject * __this /* static, unused */, PlayableHandle_t882318307 * ___handle0, const MethodInfo* method)
{
	typedef bool (*AnimationClipPlayable_INTERNAL_CALL_GetRemoveStartOffset_m1179097678_ftn) (PlayableHandle_t882318307 *);
	static AnimationClipPlayable_INTERNAL_CALL_GetRemoveStartOffset_m1179097678_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AnimationClipPlayable_INTERNAL_CALL_GetRemoveStartOffset_m1179097678_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Experimental.Director.AnimationClipPlayable::INTERNAL_CALL_GetRemoveStartOffset(UnityEngine.Experimental.Director.PlayableHandle&)");
	return _il2cpp_icall_func(___handle0);
}
// System.Void UnityEngine.Experimental.Director.AnimationClipPlayable::SetRemoveStartOffset(UnityEngine.Experimental.Director.PlayableHandle&,System.Boolean)
extern "C"  void AnimationClipPlayable_SetRemoveStartOffset_m806931619 (Il2CppObject * __this /* static, unused */, PlayableHandle_t882318307 * ___handle0, bool ___value1, const MethodInfo* method)
{
	{
		PlayableHandle_t882318307 * L_0 = ___handle0;
		bool L_1 = ___value1;
		AnimationClipPlayable_INTERNAL_CALL_SetRemoveStartOffset_m3644572681(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Experimental.Director.AnimationClipPlayable::INTERNAL_CALL_SetRemoveStartOffset(UnityEngine.Experimental.Director.PlayableHandle&,System.Boolean)
extern "C"  void AnimationClipPlayable_INTERNAL_CALL_SetRemoveStartOffset_m3644572681 (Il2CppObject * __this /* static, unused */, PlayableHandle_t882318307 * ___handle0, bool ___value1, const MethodInfo* method)
{
	typedef void (*AnimationClipPlayable_INTERNAL_CALL_SetRemoveStartOffset_m3644572681_ftn) (PlayableHandle_t882318307 *, bool);
	static AnimationClipPlayable_INTERNAL_CALL_SetRemoveStartOffset_m3644572681_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AnimationClipPlayable_INTERNAL_CALL_SetRemoveStartOffset_m3644572681_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Experimental.Director.AnimationClipPlayable::INTERNAL_CALL_SetRemoveStartOffset(UnityEngine.Experimental.Director.PlayableHandle&,System.Boolean)");
	_il2cpp_icall_func(___handle0, ___value1);
}
// System.Void UnityEngine.Experimental.Director.AnimationLayerMixerPlayable::.ctor()
extern "C"  void AnimationLayerMixerPlayable__ctor_m3757309989 (AnimationLayerMixerPlayable_t1193230317 * __this, const MethodInfo* method)
{
	{
		AnimationPlayable__ctor_m1573048877(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Experimental.Director.AnimationMixerPlayable::.ctor()
extern "C"  void AnimationMixerPlayable__ctor_m1412535250 (AnimationMixerPlayable_t2837525843 * __this, const MethodInfo* method)
{
	{
		AnimationPlayable__ctor_m1573048877(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
