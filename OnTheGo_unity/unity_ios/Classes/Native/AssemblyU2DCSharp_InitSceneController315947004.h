﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour3962482529.h"

// UnityEngine.UI.Image
struct Image_t2670269651;
// UnityEngine.UI.Text
struct Text_t1901882714;
// System.String
struct String_t;
// UnityEngine.WWW
struct WWW_t3688466362;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// InitSceneController
struct  InitSceneController_t315947004  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.UI.Image InitSceneController::imageComp
	Image_t2670269651 * ___imageComp_2;
	// UnityEngine.UI.Text InitSceneController::text
	Text_t1901882714 * ___text_3;
	// System.String InitSceneController::json
	String_t* ___json_4;
	// System.Boolean InitSceneController::readyToLoadScene
	bool ___readyToLoadScene_5;
	// UnityEngine.WWW InitSceneController::www
	WWW_t3688466362 * ___www_6;

public:
	inline static int32_t get_offset_of_imageComp_2() { return static_cast<int32_t>(offsetof(InitSceneController_t315947004, ___imageComp_2)); }
	inline Image_t2670269651 * get_imageComp_2() const { return ___imageComp_2; }
	inline Image_t2670269651 ** get_address_of_imageComp_2() { return &___imageComp_2; }
	inline void set_imageComp_2(Image_t2670269651 * value)
	{
		___imageComp_2 = value;
		Il2CppCodeGenWriteBarrier(&___imageComp_2, value);
	}

	inline static int32_t get_offset_of_text_3() { return static_cast<int32_t>(offsetof(InitSceneController_t315947004, ___text_3)); }
	inline Text_t1901882714 * get_text_3() const { return ___text_3; }
	inline Text_t1901882714 ** get_address_of_text_3() { return &___text_3; }
	inline void set_text_3(Text_t1901882714 * value)
	{
		___text_3 = value;
		Il2CppCodeGenWriteBarrier(&___text_3, value);
	}

	inline static int32_t get_offset_of_json_4() { return static_cast<int32_t>(offsetof(InitSceneController_t315947004, ___json_4)); }
	inline String_t* get_json_4() const { return ___json_4; }
	inline String_t** get_address_of_json_4() { return &___json_4; }
	inline void set_json_4(String_t* value)
	{
		___json_4 = value;
		Il2CppCodeGenWriteBarrier(&___json_4, value);
	}

	inline static int32_t get_offset_of_readyToLoadScene_5() { return static_cast<int32_t>(offsetof(InitSceneController_t315947004, ___readyToLoadScene_5)); }
	inline bool get_readyToLoadScene_5() const { return ___readyToLoadScene_5; }
	inline bool* get_address_of_readyToLoadScene_5() { return &___readyToLoadScene_5; }
	inline void set_readyToLoadScene_5(bool value)
	{
		___readyToLoadScene_5 = value;
	}

	inline static int32_t get_offset_of_www_6() { return static_cast<int32_t>(offsetof(InitSceneController_t315947004, ___www_6)); }
	inline WWW_t3688466362 * get_www_6() const { return ___www_6; }
	inline WWW_t3688466362 ** get_address_of_www_6() { return &___www_6; }
	inline void set_www_6(WWW_t3688466362 * value)
	{
		___www_6 = value;
		Il2CppCodeGenWriteBarrier(&___www_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
