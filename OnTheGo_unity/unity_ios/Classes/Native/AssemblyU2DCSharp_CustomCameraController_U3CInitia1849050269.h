﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// UnityEngine.WebCamDevice[]
struct WebCamDeviceU5BU5D_t4294070825;
// CustomCameraController
struct CustomCameraController_t1440392445;
// System.Object
struct Il2CppObject;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CustomCameraController/<Initialize>c__Iterator0
struct  U3CInitializeU3Ec__Iterator0_t1849050269  : public Il2CppObject
{
public:
	// System.Boolean CustomCameraController/<Initialize>c__Iterator0::firstStart
	bool ___firstStart_0;
	// UnityEngine.WebCamDevice[] CustomCameraController/<Initialize>c__Iterator0::$locvar0
	WebCamDeviceU5BU5D_t4294070825* ___U24locvar0_1;
	// System.Int32 CustomCameraController/<Initialize>c__Iterator0::$locvar1
	int32_t ___U24locvar1_2;
	// CustomCameraController CustomCameraController/<Initialize>c__Iterator0::$this
	CustomCameraController_t1440392445 * ___U24this_3;
	// System.Object CustomCameraController/<Initialize>c__Iterator0::$current
	Il2CppObject * ___U24current_4;
	// System.Boolean CustomCameraController/<Initialize>c__Iterator0::$disposing
	bool ___U24disposing_5;
	// System.Int32 CustomCameraController/<Initialize>c__Iterator0::$PC
	int32_t ___U24PC_6;

public:
	inline static int32_t get_offset_of_firstStart_0() { return static_cast<int32_t>(offsetof(U3CInitializeU3Ec__Iterator0_t1849050269, ___firstStart_0)); }
	inline bool get_firstStart_0() const { return ___firstStart_0; }
	inline bool* get_address_of_firstStart_0() { return &___firstStart_0; }
	inline void set_firstStart_0(bool value)
	{
		___firstStart_0 = value;
	}

	inline static int32_t get_offset_of_U24locvar0_1() { return static_cast<int32_t>(offsetof(U3CInitializeU3Ec__Iterator0_t1849050269, ___U24locvar0_1)); }
	inline WebCamDeviceU5BU5D_t4294070825* get_U24locvar0_1() const { return ___U24locvar0_1; }
	inline WebCamDeviceU5BU5D_t4294070825** get_address_of_U24locvar0_1() { return &___U24locvar0_1; }
	inline void set_U24locvar0_1(WebCamDeviceU5BU5D_t4294070825* value)
	{
		___U24locvar0_1 = value;
		Il2CppCodeGenWriteBarrier(&___U24locvar0_1, value);
	}

	inline static int32_t get_offset_of_U24locvar1_2() { return static_cast<int32_t>(offsetof(U3CInitializeU3Ec__Iterator0_t1849050269, ___U24locvar1_2)); }
	inline int32_t get_U24locvar1_2() const { return ___U24locvar1_2; }
	inline int32_t* get_address_of_U24locvar1_2() { return &___U24locvar1_2; }
	inline void set_U24locvar1_2(int32_t value)
	{
		___U24locvar1_2 = value;
	}

	inline static int32_t get_offset_of_U24this_3() { return static_cast<int32_t>(offsetof(U3CInitializeU3Ec__Iterator0_t1849050269, ___U24this_3)); }
	inline CustomCameraController_t1440392445 * get_U24this_3() const { return ___U24this_3; }
	inline CustomCameraController_t1440392445 ** get_address_of_U24this_3() { return &___U24this_3; }
	inline void set_U24this_3(CustomCameraController_t1440392445 * value)
	{
		___U24this_3 = value;
		Il2CppCodeGenWriteBarrier(&___U24this_3, value);
	}

	inline static int32_t get_offset_of_U24current_4() { return static_cast<int32_t>(offsetof(U3CInitializeU3Ec__Iterator0_t1849050269, ___U24current_4)); }
	inline Il2CppObject * get_U24current_4() const { return ___U24current_4; }
	inline Il2CppObject ** get_address_of_U24current_4() { return &___U24current_4; }
	inline void set_U24current_4(Il2CppObject * value)
	{
		___U24current_4 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_4, value);
	}

	inline static int32_t get_offset_of_U24disposing_5() { return static_cast<int32_t>(offsetof(U3CInitializeU3Ec__Iterator0_t1849050269, ___U24disposing_5)); }
	inline bool get_U24disposing_5() const { return ___U24disposing_5; }
	inline bool* get_address_of_U24disposing_5() { return &___U24disposing_5; }
	inline void set_U24disposing_5(bool value)
	{
		___U24disposing_5 = value;
	}

	inline static int32_t get_offset_of_U24PC_6() { return static_cast<int32_t>(offsetof(U3CInitializeU3Ec__Iterator0_t1849050269, ___U24PC_6)); }
	inline int32_t get_U24PC_6() const { return ___U24PC_6; }
	inline int32_t* get_address_of_U24PC_6() { return &___U24PC_6; }
	inline void set_U24PC_6(int32_t value)
	{
		___U24PC_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
