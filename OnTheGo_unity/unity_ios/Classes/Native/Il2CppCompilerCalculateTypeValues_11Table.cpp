﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "System_System_Text_RegularExpressions_BaseMachine2554639499.h"
#include "System_System_Text_RegularExpressions_Capture2232016050.h"
#include "System_System_Text_RegularExpressions_CaptureColle1760593541.h"
#include "System_System_Text_RegularExpressions_Group2468205786.h"
#include "System_System_Text_RegularExpressions_GroupCollectio69770484.h"
#include "System_System_Text_RegularExpressions_Match3408321083.h"
#include "System_System_Text_RegularExpressions_MatchCollect1395363720.h"
#include "System_System_Text_RegularExpressions_MatchCollect2645102469.h"
#include "System_System_Text_RegularExpressions_Regex3657309853.h"
#include "System_System_Text_RegularExpressions_RegexOptions92845595.h"
#include "System_System_Text_RegularExpressions_OpCode1565867503.h"
#include "System_System_Text_RegularExpressions_OpFlags23120214.h"
#include "System_System_Text_RegularExpressions_Position2536274344.h"
#include "System_System_Text_RegularExpressions_FactoryCache2327118887.h"
#include "System_System_Text_RegularExpressions_FactoryCache2725523001.h"
#include "System_System_Text_RegularExpressions_MRUList4121573800.h"
#include "System_System_Text_RegularExpressions_MRUList_Node2049086415.h"
#include "System_System_Text_RegularExpressions_Category1200126069.h"
#include "System_System_Text_RegularExpressions_CategoryUtil3167997394.h"
#include "System_System_Text_RegularExpressions_LinkRef2971865410.h"
#include "System_System_Text_RegularExpressions_InterpreterFa533216624.h"
#include "System_System_Text_RegularExpressions_PatternCompi4036359803.h"
#include "System_System_Text_RegularExpressions_PatternCompil976787442.h"
#include "System_System_Text_RegularExpressions_PatternCompi3395949159.h"
#include "System_System_Text_RegularExpressions_LinkStack887727776.h"
#include "System_System_Text_RegularExpressions_Mark3471605523.h"
#include "System_System_Text_RegularExpressions_Interpreter582715701.h"
#include "System_System_Text_RegularExpressions_Interpreter_2189327687.h"
#include "System_System_Text_RegularExpressions_Interpreter_1214863076.h"
#include "System_System_Text_RegularExpressions_Interpreter_3692532274.h"
#include "System_System_Text_RegularExpressions_Interval1802865632.h"
#include "System_System_Text_RegularExpressions_IntervalColl2609070824.h"
#include "System_System_Text_RegularExpressions_IntervalColle737725276.h"
#include "System_System_Text_RegularExpressions_IntervalColl1722821004.h"
#include "System_System_Text_RegularExpressions_Syntax_Parse2430509383.h"
#include "System_System_Text_RegularExpressions_QuickSearch2588090110.h"
#include "System_System_Text_RegularExpressions_Syntax_Expre1810289389.h"
#include "System_System_Text_RegularExpressions_Syntax_Expre2722445759.h"
#include "System_System_Text_RegularExpressions_Syntax_Compo1252229802.h"
#include "System_System_Text_RegularExpressions_Syntax_Group1458537008.h"
#include "System_System_Text_RegularExpressions_Syntax_Regul3834220169.h"
#include "System_System_Text_RegularExpressions_Syntax_Captur751358689.h"
#include "System_System_Text_RegularExpressions_Syntax_Balan2395658894.h"
#include "System_System_Text_RegularExpressions_Syntax_NonBa3074098547.h"
#include "System_System_Text_RegularExpressions_Syntax_Repet2393242404.h"
#include "System_System_Text_RegularExpressions_Syntax_Asser3267412828.h"
#include "System_System_Text_RegularExpressions_Syntax_Captu3786084589.h"
#include "System_System_Text_RegularExpressions_Syntax_Expre1861210811.h"
#include "System_System_Text_RegularExpressions_Syntax_Altern625481451.h"
#include "System_System_Text_RegularExpressions_Syntax_Litera434143540.h"
#include "System_System_Text_RegularExpressions_Syntax_Posit3339288061.h"
#include "System_System_Text_RegularExpressions_Syntax_Refer1799410108.h"
#include "System_System_Text_RegularExpressions_Syntax_Backs3656518667.h"
#include "System_System_Text_RegularExpressions_Syntax_Charac839120860.h"
#include "System_System_Text_RegularExpressions_Syntax_Ancho3387011151.h"
#include "System_System_DefaultUriParser95882050.h"
#include "System_System_GenericUriParser1141496137.h"
#include "System_System_Uri100236324.h"
#include "System_System_Uri_UriScheme722425697.h"
#include "System_System_UriFormatException953270471.h"
#include "System_System_UriHostNameType881866241.h"
#include "System_System_UriKind3816567336.h"
#include "System_System_UriParser3890150400.h"
#include "System_System_UriPartial1736313903.h"
#include "System_System_UriTypeConverter3695916615.h"
#include "System_System_Net_Security_RemoteCertificateValida3014364904.h"
#include "System_U3CPrivateImplementationDetailsU3E3057255361.h"
#include "System_U3CPrivateImplementationDetailsU3E_U24Array4289081659.h"
#include "System_U3CPrivateImplementationDetailsU3E_U24Array2490092596.h"
#include "System_Core_U3CModuleU3E692745525.h"
#include "System_Core_System_Runtime_CompilerServices_Extens1723066603.h"
#include "System_Core_Locale4128636107.h"
#include "System_Core_System_MonoTODOAttribute4131080581.h"
#include "System_Core_Mono_Security_Cryptography_KeyBuilder2049230354.h"
#include "System_Core_Mono_Security_Cryptography_SymmetricTr3802591842.h"
#include "System_Core_System_Linq_Check192468399.h"
#include "System_Core_System_Linq_Enumerable538148348.h"
#include "System_Core_System_Linq_SortDirection3222219096.h"
#include "System_Core_System_Security_Cryptography_Aes1218282760.h"
#include "System_Core_System_Security_Cryptography_AesManage1129950597.h"
#include "System_Core_System_Security_Cryptography_AesTransf2957123611.h"
#include "System_Core_System_Action1264377477.h"







#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1100 = { sizeof (BaseMachine_t2554639499), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1100[1] = 
{
	BaseMachine_t2554639499::get_offset_of_needs_groups_or_captures_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1101 = { sizeof (Capture_t2232016050), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1101[3] = 
{
	Capture_t2232016050::get_offset_of_index_0(),
	Capture_t2232016050::get_offset_of_length_1(),
	Capture_t2232016050::get_offset_of_text_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1102 = { sizeof (CaptureCollection_t1760593541), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1102[1] = 
{
	CaptureCollection_t1760593541::get_offset_of_list_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1103 = { sizeof (Group_t2468205786), -1, sizeof(Group_t2468205786_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1103[3] = 
{
	Group_t2468205786_StaticFields::get_offset_of_Fail_3(),
	Group_t2468205786::get_offset_of_success_4(),
	Group_t2468205786::get_offset_of_captures_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1104 = { sizeof (GroupCollection_t69770484), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1104[2] = 
{
	GroupCollection_t69770484::get_offset_of_list_0(),
	GroupCollection_t69770484::get_offset_of_gap_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1105 = { sizeof (Match_t3408321083), -1, sizeof(Match_t3408321083_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1105[5] = 
{
	Match_t3408321083::get_offset_of_regex_6(),
	Match_t3408321083::get_offset_of_machine_7(),
	Match_t3408321083::get_offset_of_text_length_8(),
	Match_t3408321083::get_offset_of_groups_9(),
	Match_t3408321083_StaticFields::get_offset_of_empty_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1106 = { sizeof (MatchCollection_t1395363720), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1106[2] = 
{
	MatchCollection_t1395363720::get_offset_of_current_0(),
	MatchCollection_t1395363720::get_offset_of_list_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1107 = { sizeof (Enumerator_t2645102469), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1107[2] = 
{
	Enumerator_t2645102469::get_offset_of_index_0(),
	Enumerator_t2645102469::get_offset_of_coll_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1108 = { sizeof (Regex_t3657309853), -1, sizeof(Regex_t3657309853_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1108[14] = 
{
	Regex_t3657309853_StaticFields::get_offset_of_cache_0(),
	Regex_t3657309853::get_offset_of_machineFactory_1(),
	Regex_t3657309853::get_offset_of_mapping_2(),
	Regex_t3657309853::get_offset_of_group_count_3(),
	Regex_t3657309853::get_offset_of_gap_4(),
	Regex_t3657309853::get_offset_of_refsInitialized_5(),
	Regex_t3657309853::get_offset_of_group_names_6(),
	Regex_t3657309853::get_offset_of_group_numbers_7(),
	Regex_t3657309853::get_offset_of_pattern_8(),
	Regex_t3657309853::get_offset_of_roptions_9(),
	Regex_t3657309853::get_offset_of_capnames_10(),
	Regex_t3657309853::get_offset_of_caps_11(),
	Regex_t3657309853::get_offset_of_capsize_12(),
	Regex_t3657309853::get_offset_of_capslist_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1109 = { sizeof (RegexOptions_t92845595)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1109[10] = 
{
	RegexOptions_t92845595::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1110 = { sizeof (OpCode_t1565867503)+ sizeof (Il2CppObject), sizeof(uint16_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1110[26] = 
{
	OpCode_t1565867503::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1111 = { sizeof (OpFlags_t23120214)+ sizeof (Il2CppObject), sizeof(uint16_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1111[6] = 
{
	OpFlags_t23120214::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1112 = { sizeof (Position_t2536274344)+ sizeof (Il2CppObject), sizeof(uint16_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1112[11] = 
{
	Position_t2536274344::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1113 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1114 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1115 = { sizeof (FactoryCache_t2327118887), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1115[3] = 
{
	FactoryCache_t2327118887::get_offset_of_capacity_0(),
	FactoryCache_t2327118887::get_offset_of_factories_1(),
	FactoryCache_t2327118887::get_offset_of_mru_list_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1116 = { sizeof (Key_t2725523001), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1116[2] = 
{
	Key_t2725523001::get_offset_of_pattern_0(),
	Key_t2725523001::get_offset_of_options_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1117 = { sizeof (MRUList_t4121573800), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1117[2] = 
{
	MRUList_t4121573800::get_offset_of_head_0(),
	MRUList_t4121573800::get_offset_of_tail_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1118 = { sizeof (Node_t2049086415), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1118[3] = 
{
	Node_t2049086415::get_offset_of_value_0(),
	Node_t2049086415::get_offset_of_previous_1(),
	Node_t2049086415::get_offset_of_next_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1119 = { sizeof (Category_t1200126069)+ sizeof (Il2CppObject), sizeof(uint16_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1119[146] = 
{
	Category_t1200126069::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1120 = { sizeof (CategoryUtils_t3167997394), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1121 = { sizeof (LinkRef_t2971865410), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1122 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1123 = { sizeof (InterpreterFactory_t533216624), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1123[4] = 
{
	InterpreterFactory_t533216624::get_offset_of_mapping_0(),
	InterpreterFactory_t533216624::get_offset_of_pattern_1(),
	InterpreterFactory_t533216624::get_offset_of_namesMapping_2(),
	InterpreterFactory_t533216624::get_offset_of_gap_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1124 = { sizeof (PatternCompiler_t4036359803), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1124[1] = 
{
	PatternCompiler_t4036359803::get_offset_of_pgm_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1125 = { sizeof (PatternLinkStack_t976787442), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1125[1] = 
{
	PatternLinkStack_t976787442::get_offset_of_link_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1126 = { sizeof (Link_t3395949159)+ sizeof (Il2CppObject), sizeof(Link_t3395949159 ), 0, 0 };
extern const int32_t g_FieldOffsetTable1126[2] = 
{
	Link_t3395949159::get_offset_of_base_addr_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Link_t3395949159::get_offset_of_offset_addr_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1127 = { sizeof (LinkStack_t887727776), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1127[1] = 
{
	LinkStack_t887727776::get_offset_of_stack_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1128 = { sizeof (Mark_t3471605523)+ sizeof (Il2CppObject), sizeof(Mark_t3471605523 ), 0, 0 };
extern const int32_t g_FieldOffsetTable1128[3] = 
{
	Mark_t3471605523::get_offset_of_Start_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Mark_t3471605523::get_offset_of_End_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Mark_t3471605523::get_offset_of_Previous_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1129 = { sizeof (Interpreter_t582715701), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1129[16] = 
{
	Interpreter_t582715701::get_offset_of_program_1(),
	Interpreter_t582715701::get_offset_of_program_start_2(),
	Interpreter_t582715701::get_offset_of_text_3(),
	Interpreter_t582715701::get_offset_of_text_end_4(),
	Interpreter_t582715701::get_offset_of_group_count_5(),
	Interpreter_t582715701::get_offset_of_match_min_6(),
	Interpreter_t582715701::get_offset_of_qs_7(),
	Interpreter_t582715701::get_offset_of_scan_ptr_8(),
	Interpreter_t582715701::get_offset_of_repeat_9(),
	Interpreter_t582715701::get_offset_of_fast_10(),
	Interpreter_t582715701::get_offset_of_stack_11(),
	Interpreter_t582715701::get_offset_of_deep_12(),
	Interpreter_t582715701::get_offset_of_marks_13(),
	Interpreter_t582715701::get_offset_of_mark_start_14(),
	Interpreter_t582715701::get_offset_of_mark_end_15(),
	Interpreter_t582715701::get_offset_of_groups_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1130 = { sizeof (IntStack_t2189327687)+ sizeof (Il2CppObject), sizeof(IntStack_t2189327687_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1130[2] = 
{
	IntStack_t2189327687::get_offset_of_values_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	IntStack_t2189327687::get_offset_of_count_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1131 = { sizeof (RepeatContext_t1214863076), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1131[7] = 
{
	RepeatContext_t1214863076::get_offset_of_start_0(),
	RepeatContext_t1214863076::get_offset_of_min_1(),
	RepeatContext_t1214863076::get_offset_of_max_2(),
	RepeatContext_t1214863076::get_offset_of_lazy_3(),
	RepeatContext_t1214863076::get_offset_of_expr_pc_4(),
	RepeatContext_t1214863076::get_offset_of_previous_5(),
	RepeatContext_t1214863076::get_offset_of_count_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1132 = { sizeof (Mode_t3692532274)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1132[4] = 
{
	Mode_t3692532274::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1133 = { sizeof (Interval_t1802865632)+ sizeof (Il2CppObject), sizeof(Interval_t1802865632_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1133[3] = 
{
	Interval_t1802865632::get_offset_of_low_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Interval_t1802865632::get_offset_of_high_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Interval_t1802865632::get_offset_of_contiguous_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1134 = { sizeof (IntervalCollection_t2609070824), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1134[1] = 
{
	IntervalCollection_t2609070824::get_offset_of_intervals_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1135 = { sizeof (Enumerator_t737725276), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1135[2] = 
{
	Enumerator_t737725276::get_offset_of_list_0(),
	Enumerator_t737725276::get_offset_of_ptr_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1136 = { sizeof (CostDelegate_t1722821004), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1137 = { sizeof (Parser_t2430509383), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1137[6] = 
{
	Parser_t2430509383::get_offset_of_pattern_0(),
	Parser_t2430509383::get_offset_of_ptr_1(),
	Parser_t2430509383::get_offset_of_caps_2(),
	Parser_t2430509383::get_offset_of_refs_3(),
	Parser_t2430509383::get_offset_of_num_groups_4(),
	Parser_t2430509383::get_offset_of_gap_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1138 = { sizeof (QuickSearch_t2588090110), -1, sizeof(QuickSearch_t2588090110_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1138[7] = 
{
	QuickSearch_t2588090110::get_offset_of_str_0(),
	QuickSearch_t2588090110::get_offset_of_len_1(),
	QuickSearch_t2588090110::get_offset_of_ignore_2(),
	QuickSearch_t2588090110::get_offset_of_reverse_3(),
	QuickSearch_t2588090110::get_offset_of_shift_4(),
	QuickSearch_t2588090110::get_offset_of_shiftExtended_5(),
	QuickSearch_t2588090110_StaticFields::get_offset_of_THRESHOLD_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1139 = { sizeof (ExpressionCollection_t1810289389), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1140 = { sizeof (Expression_t2722445759), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1141 = { sizeof (CompositeExpression_t1252229802), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1141[1] = 
{
	CompositeExpression_t1252229802::get_offset_of_expressions_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1142 = { sizeof (Group_t1458537008), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1143 = { sizeof (RegularExpression_t3834220169), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1143[1] = 
{
	RegularExpression_t3834220169::get_offset_of_group_count_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1144 = { sizeof (CapturingGroup_t751358689), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1144[2] = 
{
	CapturingGroup_t751358689::get_offset_of_gid_1(),
	CapturingGroup_t751358689::get_offset_of_name_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1145 = { sizeof (BalancingGroup_t2395658894), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1145[1] = 
{
	BalancingGroup_t2395658894::get_offset_of_balance_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1146 = { sizeof (NonBacktrackingGroup_t3074098547), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1147 = { sizeof (Repetition_t2393242404), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1147[3] = 
{
	Repetition_t2393242404::get_offset_of_min_1(),
	Repetition_t2393242404::get_offset_of_max_2(),
	Repetition_t2393242404::get_offset_of_lazy_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1148 = { sizeof (Assertion_t3267412828), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1149 = { sizeof (CaptureAssertion_t3786084589), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1149[3] = 
{
	CaptureAssertion_t3786084589::get_offset_of_alternate_1(),
	CaptureAssertion_t3786084589::get_offset_of_group_2(),
	CaptureAssertion_t3786084589::get_offset_of_literal_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1150 = { sizeof (ExpressionAssertion_t1861210811), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1150[2] = 
{
	ExpressionAssertion_t1861210811::get_offset_of_reverse_1(),
	ExpressionAssertion_t1861210811::get_offset_of_negate_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1151 = { sizeof (Alternation_t625481451), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1152 = { sizeof (Literal_t434143540), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1152[2] = 
{
	Literal_t434143540::get_offset_of_str_0(),
	Literal_t434143540::get_offset_of_ignore_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1153 = { sizeof (PositionAssertion_t3339288061), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1153[1] = 
{
	PositionAssertion_t3339288061::get_offset_of_pos_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1154 = { sizeof (Reference_t1799410108), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1154[2] = 
{
	Reference_t1799410108::get_offset_of_group_0(),
	Reference_t1799410108::get_offset_of_ignore_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1155 = { sizeof (BackslashNumber_t3656518667), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1155[2] = 
{
	BackslashNumber_t3656518667::get_offset_of_literal_2(),
	BackslashNumber_t3656518667::get_offset_of_ecma_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1156 = { sizeof (CharacterClass_t839120860), -1, sizeof(CharacterClass_t839120860_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1156[6] = 
{
	CharacterClass_t839120860_StaticFields::get_offset_of_upper_case_characters_0(),
	CharacterClass_t839120860::get_offset_of_negate_1(),
	CharacterClass_t839120860::get_offset_of_ignore_2(),
	CharacterClass_t839120860::get_offset_of_pos_cats_3(),
	CharacterClass_t839120860::get_offset_of_neg_cats_4(),
	CharacterClass_t839120860::get_offset_of_intervals_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1157 = { sizeof (AnchorInfo_t3387011151), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1157[6] = 
{
	AnchorInfo_t3387011151::get_offset_of_expr_0(),
	AnchorInfo_t3387011151::get_offset_of_pos_1(),
	AnchorInfo_t3387011151::get_offset_of_offset_2(),
	AnchorInfo_t3387011151::get_offset_of_str_3(),
	AnchorInfo_t3387011151::get_offset_of_width_4(),
	AnchorInfo_t3387011151::get_offset_of_ignore_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1158 = { sizeof (DefaultUriParser_t95882050), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1159 = { sizeof (GenericUriParser_t1141496137), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1160 = { sizeof (Uri_t100236324), -1, sizeof(Uri_t100236324_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1160[33] = 
{
	Uri_t100236324::get_offset_of_isUnixFilePath_0(),
	Uri_t100236324::get_offset_of_source_1(),
	Uri_t100236324::get_offset_of_scheme_2(),
	Uri_t100236324::get_offset_of_host_3(),
	Uri_t100236324::get_offset_of_port_4(),
	Uri_t100236324::get_offset_of_path_5(),
	Uri_t100236324::get_offset_of_query_6(),
	Uri_t100236324::get_offset_of_fragment_7(),
	Uri_t100236324::get_offset_of_userinfo_8(),
	Uri_t100236324::get_offset_of_isUnc_9(),
	Uri_t100236324::get_offset_of_isOpaquePart_10(),
	Uri_t100236324::get_offset_of_isAbsoluteUri_11(),
	Uri_t100236324::get_offset_of_userEscaped_12(),
	Uri_t100236324::get_offset_of_cachedAbsoluteUri_13(),
	Uri_t100236324::get_offset_of_cachedToString_14(),
	Uri_t100236324::get_offset_of_cachedHashCode_15(),
	Uri_t100236324_StaticFields::get_offset_of_hexUpperChars_16(),
	Uri_t100236324_StaticFields::get_offset_of_SchemeDelimiter_17(),
	Uri_t100236324_StaticFields::get_offset_of_UriSchemeFile_18(),
	Uri_t100236324_StaticFields::get_offset_of_UriSchemeFtp_19(),
	Uri_t100236324_StaticFields::get_offset_of_UriSchemeGopher_20(),
	Uri_t100236324_StaticFields::get_offset_of_UriSchemeHttp_21(),
	Uri_t100236324_StaticFields::get_offset_of_UriSchemeHttps_22(),
	Uri_t100236324_StaticFields::get_offset_of_UriSchemeMailto_23(),
	Uri_t100236324_StaticFields::get_offset_of_UriSchemeNews_24(),
	Uri_t100236324_StaticFields::get_offset_of_UriSchemeNntp_25(),
	Uri_t100236324_StaticFields::get_offset_of_UriSchemeNetPipe_26(),
	Uri_t100236324_StaticFields::get_offset_of_UriSchemeNetTcp_27(),
	Uri_t100236324_StaticFields::get_offset_of_schemes_28(),
	Uri_t100236324::get_offset_of_parser_29(),
	Uri_t100236324_StaticFields::get_offset_of_U3CU3Ef__switchU24map14_30(),
	Uri_t100236324_StaticFields::get_offset_of_U3CU3Ef__switchU24map15_31(),
	Uri_t100236324_StaticFields::get_offset_of_U3CU3Ef__switchU24map16_32(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1161 = { sizeof (UriScheme_t722425697)+ sizeof (Il2CppObject), sizeof(UriScheme_t722425697_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1161[3] = 
{
	UriScheme_t722425697::get_offset_of_scheme_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	UriScheme_t722425697::get_offset_of_delimiter_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	UriScheme_t722425697::get_offset_of_defaultPort_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1162 = { sizeof (UriFormatException_t953270471), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1163 = { sizeof (UriHostNameType_t881866241)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1163[6] = 
{
	UriHostNameType_t881866241::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1164 = { sizeof (UriKind_t3816567336)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1164[4] = 
{
	UriKind_t3816567336::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1165 = { sizeof (UriParser_t3890150400), -1, sizeof(UriParser_t3890150400_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1165[6] = 
{
	UriParser_t3890150400_StaticFields::get_offset_of_lock_object_0(),
	UriParser_t3890150400_StaticFields::get_offset_of_table_1(),
	UriParser_t3890150400::get_offset_of_scheme_name_2(),
	UriParser_t3890150400::get_offset_of_default_port_3(),
	UriParser_t3890150400_StaticFields::get_offset_of_uri_regex_4(),
	UriParser_t3890150400_StaticFields::get_offset_of_auth_regex_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1166 = { sizeof (UriPartial_t1736313903)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1166[5] = 
{
	UriPartial_t1736313903::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1167 = { sizeof (UriTypeConverter_t3695916615), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1168 = { sizeof (RemoteCertificateValidationCallback_t3014364904), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1169 = { sizeof (U3CPrivateImplementationDetailsU3E_t3057255363), -1, sizeof(U3CPrivateImplementationDetailsU3E_t3057255363_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1169[3] = 
{
	U3CPrivateImplementationDetailsU3E_t3057255363_StaticFields::get_offset_of_U24U24fieldU2D2_0(),
	U3CPrivateImplementationDetailsU3E_t3057255363_StaticFields::get_offset_of_U24U24fieldU2D3_1(),
	U3CPrivateImplementationDetailsU3E_t3057255363_StaticFields::get_offset_of_U24U24fieldU2D4_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1170 = { sizeof (U24ArrayTypeU24128_t4289081660)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU24128_t4289081660 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1171 = { sizeof (U24ArrayTypeU2412_t2490092598)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU2412_t2490092598 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1172 = { sizeof (U3CModuleU3E_t692745528), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1173 = { sizeof (ExtensionAttribute_t1723066603), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1174 = { sizeof (Locale_t4128636110), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1175 = { sizeof (MonoTODOAttribute_t4131080583), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1176 = { sizeof (KeyBuilder_t2049230356), -1, sizeof(KeyBuilder_t2049230356_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1176[1] = 
{
	KeyBuilder_t2049230356_StaticFields::get_offset_of_rng_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1177 = { sizeof (SymmetricTransform_t3802591843), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1177[12] = 
{
	SymmetricTransform_t3802591843::get_offset_of_algo_0(),
	SymmetricTransform_t3802591843::get_offset_of_encrypt_1(),
	SymmetricTransform_t3802591843::get_offset_of_BlockSizeByte_2(),
	SymmetricTransform_t3802591843::get_offset_of_temp_3(),
	SymmetricTransform_t3802591843::get_offset_of_temp2_4(),
	SymmetricTransform_t3802591843::get_offset_of_workBuff_5(),
	SymmetricTransform_t3802591843::get_offset_of_workout_6(),
	SymmetricTransform_t3802591843::get_offset_of_FeedBackByte_7(),
	SymmetricTransform_t3802591843::get_offset_of_FeedBackIter_8(),
	SymmetricTransform_t3802591843::get_offset_of_m_disposed_9(),
	SymmetricTransform_t3802591843::get_offset_of_lastBlock_10(),
	SymmetricTransform_t3802591843::get_offset_of__rng_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1178 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1178[14] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1179 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1179[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1180 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1180[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1181 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1181[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1182 = { sizeof (Check_t192468399), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1183 = { sizeof (Enumerable_t538148348), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1184 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1184[8] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1185 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1186 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1186[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1187 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1187[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1188 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1188[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1189 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1189[8] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1190 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1190[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1191 = { sizeof (SortDirection_t3222219096)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1191[3] = 
{
	SortDirection_t3222219096::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1192 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1192[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1193 = { sizeof (Aes_t1218282760), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1194 = { sizeof (AesManaged_t1129950597), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1195 = { sizeof (AesTransform_t2957123611), -1, sizeof(AesTransform_t2957123611_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1195[14] = 
{
	AesTransform_t2957123611::get_offset_of_expandedKey_12(),
	AesTransform_t2957123611::get_offset_of_Nk_13(),
	AesTransform_t2957123611::get_offset_of_Nr_14(),
	AesTransform_t2957123611_StaticFields::get_offset_of_Rcon_15(),
	AesTransform_t2957123611_StaticFields::get_offset_of_SBox_16(),
	AesTransform_t2957123611_StaticFields::get_offset_of_iSBox_17(),
	AesTransform_t2957123611_StaticFields::get_offset_of_T0_18(),
	AesTransform_t2957123611_StaticFields::get_offset_of_T1_19(),
	AesTransform_t2957123611_StaticFields::get_offset_of_T2_20(),
	AesTransform_t2957123611_StaticFields::get_offset_of_T3_21(),
	AesTransform_t2957123611_StaticFields::get_offset_of_iT0_22(),
	AesTransform_t2957123611_StaticFields::get_offset_of_iT1_23(),
	AesTransform_t2957123611_StaticFields::get_offset_of_iT2_24(),
	AesTransform_t2957123611_StaticFields::get_offset_of_iT3_25(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1196 = { sizeof (Action_t1264377477), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1197 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1198 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1199 = { 0, 0, 0, 0 };
#ifdef __clang__
#pragma clang diagnostic pop
#endif
