﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "WikitudeUnityPlugin_Wikitude_RecognizedTarget3529911668.h"
#include "UnityEngine_UnityEngine_Vector33722313464.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Wikitude.ObjectTarget
struct  ObjectTarget_t688225044  : public RecognizedTarget_t3529911668
{
public:
	// UnityEngine.Vector3 Wikitude.ObjectTarget::_scale
	Vector3_t3722313464  ____scale_4;

public:
	inline static int32_t get_offset_of__scale_4() { return static_cast<int32_t>(offsetof(ObjectTarget_t688225044, ____scale_4)); }
	inline Vector3_t3722313464  get__scale_4() const { return ____scale_4; }
	inline Vector3_t3722313464 * get_address_of__scale_4() { return &____scale_4; }
	inline void set__scale_4(Vector3_t3722313464  value)
	{
		____scale_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
