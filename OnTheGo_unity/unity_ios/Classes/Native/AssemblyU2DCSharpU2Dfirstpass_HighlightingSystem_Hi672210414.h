﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour3962482529.h"
#include "UnityEngine_UnityEngine_Color2555686324.h"
#include "AssemblyU2DCSharpU2Dfirstpass_HighlightingSystem_H2127932823.h"

// System.Collections.Generic.List`1<System.Type>
struct List_1_t3956019502;
// System.String
struct String_t;
// HighlightingSystem.Highlighter/Mode[]
struct ModeU5BU5D_t3256553006;
// System.Collections.Generic.HashSet`1<HighlightingSystem.Highlighter>
struct HashSet_1_t3532127184;
// UnityEngine.Transform
struct Transform_t3600365921;
// System.Collections.Generic.List`1<HighlightingSystem.HighlighterRenderer>
struct List_1_t3248237193;
// System.Collections.Generic.List`1<UnityEngine.Component>
struct List_1_t3395709193;
// UnityEngine.Shader
struct Shader_t4151988712;
// UnityEngine.Material
struct Material_t340375123;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HighlightingSystem.Highlighter
struct  Highlighter_t672210414  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Color HighlightingSystem.Highlighter::occluderColor
	Color_t2555686324  ___occluderColor_5;
	// System.Boolean HighlightingSystem.Highlighter::seeThrough
	bool ___seeThrough_8;
	// System.Boolean HighlightingSystem.Highlighter::occluder
	bool ___occluder_9;
	// System.Boolean HighlightingSystem.Highlighter::forceRender
	bool ___forceRender_10;
	// UnityEngine.Transform HighlightingSystem.Highlighter::tr
	Transform_t3600365921 * ___tr_11;
	// System.Collections.Generic.List`1<HighlightingSystem.HighlighterRenderer> HighlightingSystem.Highlighter::highlightableRenderers
	List_1_t3248237193 * ___highlightableRenderers_12;
	// System.Boolean HighlightingSystem.Highlighter::renderersDirty
	bool ___renderersDirty_13;
	// HighlightingSystem.Highlighter/Mode HighlightingSystem.Highlighter::mode
	int32_t ___mode_15;
	// System.Boolean HighlightingSystem.Highlighter::cachedSeeThrough
	bool ___cachedSeeThrough_16;
	// System.Int32 HighlightingSystem.Highlighter::cachedOnce
	int32_t ___cachedOnce_17;
	// System.Boolean HighlightingSystem.Highlighter::flashing
	bool ___flashing_18;
	// UnityEngine.Color HighlightingSystem.Highlighter::currentColor
	Color_t2555686324  ___currentColor_19;
	// System.Single HighlightingSystem.Highlighter::transitionValue
	float ___transitionValue_20;
	// System.Single HighlightingSystem.Highlighter::transitionTarget
	float ___transitionTarget_21;
	// System.Single HighlightingSystem.Highlighter::transitionTime
	float ___transitionTime_22;
	// UnityEngine.Color HighlightingSystem.Highlighter::onceColor
	Color_t2555686324  ___onceColor_23;
	// System.Single HighlightingSystem.Highlighter::flashingFreq
	float ___flashingFreq_24;
	// UnityEngine.Color HighlightingSystem.Highlighter::flashingColorMin
	Color_t2555686324  ___flashingColorMin_25;
	// UnityEngine.Color HighlightingSystem.Highlighter::flashingColorMax
	Color_t2555686324  ___flashingColorMax_26;
	// UnityEngine.Color HighlightingSystem.Highlighter::constantColor
	Color_t2555686324  ___constantColor_27;
	// UnityEngine.Material HighlightingSystem.Highlighter::_opaqueMaterial
	Material_t340375123 * ____opaqueMaterial_30;

public:
	inline static int32_t get_offset_of_occluderColor_5() { return static_cast<int32_t>(offsetof(Highlighter_t672210414, ___occluderColor_5)); }
	inline Color_t2555686324  get_occluderColor_5() const { return ___occluderColor_5; }
	inline Color_t2555686324 * get_address_of_occluderColor_5() { return &___occluderColor_5; }
	inline void set_occluderColor_5(Color_t2555686324  value)
	{
		___occluderColor_5 = value;
	}

	inline static int32_t get_offset_of_seeThrough_8() { return static_cast<int32_t>(offsetof(Highlighter_t672210414, ___seeThrough_8)); }
	inline bool get_seeThrough_8() const { return ___seeThrough_8; }
	inline bool* get_address_of_seeThrough_8() { return &___seeThrough_8; }
	inline void set_seeThrough_8(bool value)
	{
		___seeThrough_8 = value;
	}

	inline static int32_t get_offset_of_occluder_9() { return static_cast<int32_t>(offsetof(Highlighter_t672210414, ___occluder_9)); }
	inline bool get_occluder_9() const { return ___occluder_9; }
	inline bool* get_address_of_occluder_9() { return &___occluder_9; }
	inline void set_occluder_9(bool value)
	{
		___occluder_9 = value;
	}

	inline static int32_t get_offset_of_forceRender_10() { return static_cast<int32_t>(offsetof(Highlighter_t672210414, ___forceRender_10)); }
	inline bool get_forceRender_10() const { return ___forceRender_10; }
	inline bool* get_address_of_forceRender_10() { return &___forceRender_10; }
	inline void set_forceRender_10(bool value)
	{
		___forceRender_10 = value;
	}

	inline static int32_t get_offset_of_tr_11() { return static_cast<int32_t>(offsetof(Highlighter_t672210414, ___tr_11)); }
	inline Transform_t3600365921 * get_tr_11() const { return ___tr_11; }
	inline Transform_t3600365921 ** get_address_of_tr_11() { return &___tr_11; }
	inline void set_tr_11(Transform_t3600365921 * value)
	{
		___tr_11 = value;
		Il2CppCodeGenWriteBarrier(&___tr_11, value);
	}

	inline static int32_t get_offset_of_highlightableRenderers_12() { return static_cast<int32_t>(offsetof(Highlighter_t672210414, ___highlightableRenderers_12)); }
	inline List_1_t3248237193 * get_highlightableRenderers_12() const { return ___highlightableRenderers_12; }
	inline List_1_t3248237193 ** get_address_of_highlightableRenderers_12() { return &___highlightableRenderers_12; }
	inline void set_highlightableRenderers_12(List_1_t3248237193 * value)
	{
		___highlightableRenderers_12 = value;
		Il2CppCodeGenWriteBarrier(&___highlightableRenderers_12, value);
	}

	inline static int32_t get_offset_of_renderersDirty_13() { return static_cast<int32_t>(offsetof(Highlighter_t672210414, ___renderersDirty_13)); }
	inline bool get_renderersDirty_13() const { return ___renderersDirty_13; }
	inline bool* get_address_of_renderersDirty_13() { return &___renderersDirty_13; }
	inline void set_renderersDirty_13(bool value)
	{
		___renderersDirty_13 = value;
	}

	inline static int32_t get_offset_of_mode_15() { return static_cast<int32_t>(offsetof(Highlighter_t672210414, ___mode_15)); }
	inline int32_t get_mode_15() const { return ___mode_15; }
	inline int32_t* get_address_of_mode_15() { return &___mode_15; }
	inline void set_mode_15(int32_t value)
	{
		___mode_15 = value;
	}

	inline static int32_t get_offset_of_cachedSeeThrough_16() { return static_cast<int32_t>(offsetof(Highlighter_t672210414, ___cachedSeeThrough_16)); }
	inline bool get_cachedSeeThrough_16() const { return ___cachedSeeThrough_16; }
	inline bool* get_address_of_cachedSeeThrough_16() { return &___cachedSeeThrough_16; }
	inline void set_cachedSeeThrough_16(bool value)
	{
		___cachedSeeThrough_16 = value;
	}

	inline static int32_t get_offset_of_cachedOnce_17() { return static_cast<int32_t>(offsetof(Highlighter_t672210414, ___cachedOnce_17)); }
	inline int32_t get_cachedOnce_17() const { return ___cachedOnce_17; }
	inline int32_t* get_address_of_cachedOnce_17() { return &___cachedOnce_17; }
	inline void set_cachedOnce_17(int32_t value)
	{
		___cachedOnce_17 = value;
	}

	inline static int32_t get_offset_of_flashing_18() { return static_cast<int32_t>(offsetof(Highlighter_t672210414, ___flashing_18)); }
	inline bool get_flashing_18() const { return ___flashing_18; }
	inline bool* get_address_of_flashing_18() { return &___flashing_18; }
	inline void set_flashing_18(bool value)
	{
		___flashing_18 = value;
	}

	inline static int32_t get_offset_of_currentColor_19() { return static_cast<int32_t>(offsetof(Highlighter_t672210414, ___currentColor_19)); }
	inline Color_t2555686324  get_currentColor_19() const { return ___currentColor_19; }
	inline Color_t2555686324 * get_address_of_currentColor_19() { return &___currentColor_19; }
	inline void set_currentColor_19(Color_t2555686324  value)
	{
		___currentColor_19 = value;
	}

	inline static int32_t get_offset_of_transitionValue_20() { return static_cast<int32_t>(offsetof(Highlighter_t672210414, ___transitionValue_20)); }
	inline float get_transitionValue_20() const { return ___transitionValue_20; }
	inline float* get_address_of_transitionValue_20() { return &___transitionValue_20; }
	inline void set_transitionValue_20(float value)
	{
		___transitionValue_20 = value;
	}

	inline static int32_t get_offset_of_transitionTarget_21() { return static_cast<int32_t>(offsetof(Highlighter_t672210414, ___transitionTarget_21)); }
	inline float get_transitionTarget_21() const { return ___transitionTarget_21; }
	inline float* get_address_of_transitionTarget_21() { return &___transitionTarget_21; }
	inline void set_transitionTarget_21(float value)
	{
		___transitionTarget_21 = value;
	}

	inline static int32_t get_offset_of_transitionTime_22() { return static_cast<int32_t>(offsetof(Highlighter_t672210414, ___transitionTime_22)); }
	inline float get_transitionTime_22() const { return ___transitionTime_22; }
	inline float* get_address_of_transitionTime_22() { return &___transitionTime_22; }
	inline void set_transitionTime_22(float value)
	{
		___transitionTime_22 = value;
	}

	inline static int32_t get_offset_of_onceColor_23() { return static_cast<int32_t>(offsetof(Highlighter_t672210414, ___onceColor_23)); }
	inline Color_t2555686324  get_onceColor_23() const { return ___onceColor_23; }
	inline Color_t2555686324 * get_address_of_onceColor_23() { return &___onceColor_23; }
	inline void set_onceColor_23(Color_t2555686324  value)
	{
		___onceColor_23 = value;
	}

	inline static int32_t get_offset_of_flashingFreq_24() { return static_cast<int32_t>(offsetof(Highlighter_t672210414, ___flashingFreq_24)); }
	inline float get_flashingFreq_24() const { return ___flashingFreq_24; }
	inline float* get_address_of_flashingFreq_24() { return &___flashingFreq_24; }
	inline void set_flashingFreq_24(float value)
	{
		___flashingFreq_24 = value;
	}

	inline static int32_t get_offset_of_flashingColorMin_25() { return static_cast<int32_t>(offsetof(Highlighter_t672210414, ___flashingColorMin_25)); }
	inline Color_t2555686324  get_flashingColorMin_25() const { return ___flashingColorMin_25; }
	inline Color_t2555686324 * get_address_of_flashingColorMin_25() { return &___flashingColorMin_25; }
	inline void set_flashingColorMin_25(Color_t2555686324  value)
	{
		___flashingColorMin_25 = value;
	}

	inline static int32_t get_offset_of_flashingColorMax_26() { return static_cast<int32_t>(offsetof(Highlighter_t672210414, ___flashingColorMax_26)); }
	inline Color_t2555686324  get_flashingColorMax_26() const { return ___flashingColorMax_26; }
	inline Color_t2555686324 * get_address_of_flashingColorMax_26() { return &___flashingColorMax_26; }
	inline void set_flashingColorMax_26(Color_t2555686324  value)
	{
		___flashingColorMax_26 = value;
	}

	inline static int32_t get_offset_of_constantColor_27() { return static_cast<int32_t>(offsetof(Highlighter_t672210414, ___constantColor_27)); }
	inline Color_t2555686324  get_constantColor_27() const { return ___constantColor_27; }
	inline Color_t2555686324 * get_address_of_constantColor_27() { return &___constantColor_27; }
	inline void set_constantColor_27(Color_t2555686324  value)
	{
		___constantColor_27 = value;
	}

	inline static int32_t get_offset_of__opaqueMaterial_30() { return static_cast<int32_t>(offsetof(Highlighter_t672210414, ____opaqueMaterial_30)); }
	inline Material_t340375123 * get__opaqueMaterial_30() const { return ____opaqueMaterial_30; }
	inline Material_t340375123 ** get_address_of__opaqueMaterial_30() { return &____opaqueMaterial_30; }
	inline void set__opaqueMaterial_30(Material_t340375123 * value)
	{
		____opaqueMaterial_30 = value;
		Il2CppCodeGenWriteBarrier(&____opaqueMaterial_30, value);
	}
};

struct Highlighter_t672210414_StaticFields
{
public:
	// System.Collections.Generic.List`1<System.Type> HighlightingSystem.Highlighter::types
	List_1_t3956019502 * ___types_2;
	// HighlightingSystem.Highlighter/Mode[] HighlightingSystem.Highlighter::renderingOrder
	ModeU5BU5D_t3256553006* ___renderingOrder_6;
	// System.Collections.Generic.HashSet`1<HighlightingSystem.Highlighter> HighlightingSystem.Highlighter::highlighters
	HashSet_1_t3532127184 * ___highlighters_7;
	// System.Collections.Generic.List`1<UnityEngine.Component> HighlightingSystem.Highlighter::sComponents
	List_1_t3395709193 * ___sComponents_14;
	// UnityEngine.Shader HighlightingSystem.Highlighter::_opaqueShader
	Shader_t4151988712 * ____opaqueShader_28;
	// UnityEngine.Shader HighlightingSystem.Highlighter::_transparentShader
	Shader_t4151988712 * ____transparentShader_29;

public:
	inline static int32_t get_offset_of_types_2() { return static_cast<int32_t>(offsetof(Highlighter_t672210414_StaticFields, ___types_2)); }
	inline List_1_t3956019502 * get_types_2() const { return ___types_2; }
	inline List_1_t3956019502 ** get_address_of_types_2() { return &___types_2; }
	inline void set_types_2(List_1_t3956019502 * value)
	{
		___types_2 = value;
		Il2CppCodeGenWriteBarrier(&___types_2, value);
	}

	inline static int32_t get_offset_of_renderingOrder_6() { return static_cast<int32_t>(offsetof(Highlighter_t672210414_StaticFields, ___renderingOrder_6)); }
	inline ModeU5BU5D_t3256553006* get_renderingOrder_6() const { return ___renderingOrder_6; }
	inline ModeU5BU5D_t3256553006** get_address_of_renderingOrder_6() { return &___renderingOrder_6; }
	inline void set_renderingOrder_6(ModeU5BU5D_t3256553006* value)
	{
		___renderingOrder_6 = value;
		Il2CppCodeGenWriteBarrier(&___renderingOrder_6, value);
	}

	inline static int32_t get_offset_of_highlighters_7() { return static_cast<int32_t>(offsetof(Highlighter_t672210414_StaticFields, ___highlighters_7)); }
	inline HashSet_1_t3532127184 * get_highlighters_7() const { return ___highlighters_7; }
	inline HashSet_1_t3532127184 ** get_address_of_highlighters_7() { return &___highlighters_7; }
	inline void set_highlighters_7(HashSet_1_t3532127184 * value)
	{
		___highlighters_7 = value;
		Il2CppCodeGenWriteBarrier(&___highlighters_7, value);
	}

	inline static int32_t get_offset_of_sComponents_14() { return static_cast<int32_t>(offsetof(Highlighter_t672210414_StaticFields, ___sComponents_14)); }
	inline List_1_t3395709193 * get_sComponents_14() const { return ___sComponents_14; }
	inline List_1_t3395709193 ** get_address_of_sComponents_14() { return &___sComponents_14; }
	inline void set_sComponents_14(List_1_t3395709193 * value)
	{
		___sComponents_14 = value;
		Il2CppCodeGenWriteBarrier(&___sComponents_14, value);
	}

	inline static int32_t get_offset_of__opaqueShader_28() { return static_cast<int32_t>(offsetof(Highlighter_t672210414_StaticFields, ____opaqueShader_28)); }
	inline Shader_t4151988712 * get__opaqueShader_28() const { return ____opaqueShader_28; }
	inline Shader_t4151988712 ** get_address_of__opaqueShader_28() { return &____opaqueShader_28; }
	inline void set__opaqueShader_28(Shader_t4151988712 * value)
	{
		____opaqueShader_28 = value;
		Il2CppCodeGenWriteBarrier(&____opaqueShader_28, value);
	}

	inline static int32_t get_offset_of__transparentShader_29() { return static_cast<int32_t>(offsetof(Highlighter_t672210414_StaticFields, ____transparentShader_29)); }
	inline Shader_t4151988712 * get__transparentShader_29() const { return ____transparentShader_29; }
	inline Shader_t4151988712 ** get_address_of__transparentShader_29() { return &____transparentShader_29; }
	inline void set__transparentShader_29(Shader_t4151988712 * value)
	{
		____transparentShader_29 = value;
		Il2CppCodeGenWriteBarrier(&____transparentShader_29, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
