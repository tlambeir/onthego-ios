﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour3962482529.h"
#include "UnityEngine_UnityEngine_Vector33722313464.h"
#include "UnityEngine_UnityEngine_Color2555686324.h"

// UnityEngine.Transform
struct Transform_t3600365921;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LabelController
struct  LabelController_t2075514664  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Transform LabelController::parentTransform
	Transform_t3600365921 * ___parentTransform_2;
	// UnityEngine.Vector3 LabelController::Offset
	Vector3_t3722313464  ___Offset_3;
	// System.Single LabelController::lineSlant
	float ___lineSlant_4;
	// System.Single LabelController::lineWidth
	float ___lineWidth_5;
	// System.Single LabelController::labelHeight
	float ___labelHeight_6;
	// System.Single LabelController::labelWidth
	float ___labelWidth_7;
	// UnityEngine.Color LabelController::color
	Color_t2555686324  ___color_8;
	// UnityEngine.Transform LabelController::labelTextTransform
	Transform_t3600365921 * ___labelTextTransform_9;

public:
	inline static int32_t get_offset_of_parentTransform_2() { return static_cast<int32_t>(offsetof(LabelController_t2075514664, ___parentTransform_2)); }
	inline Transform_t3600365921 * get_parentTransform_2() const { return ___parentTransform_2; }
	inline Transform_t3600365921 ** get_address_of_parentTransform_2() { return &___parentTransform_2; }
	inline void set_parentTransform_2(Transform_t3600365921 * value)
	{
		___parentTransform_2 = value;
		Il2CppCodeGenWriteBarrier(&___parentTransform_2, value);
	}

	inline static int32_t get_offset_of_Offset_3() { return static_cast<int32_t>(offsetof(LabelController_t2075514664, ___Offset_3)); }
	inline Vector3_t3722313464  get_Offset_3() const { return ___Offset_3; }
	inline Vector3_t3722313464 * get_address_of_Offset_3() { return &___Offset_3; }
	inline void set_Offset_3(Vector3_t3722313464  value)
	{
		___Offset_3 = value;
	}

	inline static int32_t get_offset_of_lineSlant_4() { return static_cast<int32_t>(offsetof(LabelController_t2075514664, ___lineSlant_4)); }
	inline float get_lineSlant_4() const { return ___lineSlant_4; }
	inline float* get_address_of_lineSlant_4() { return &___lineSlant_4; }
	inline void set_lineSlant_4(float value)
	{
		___lineSlant_4 = value;
	}

	inline static int32_t get_offset_of_lineWidth_5() { return static_cast<int32_t>(offsetof(LabelController_t2075514664, ___lineWidth_5)); }
	inline float get_lineWidth_5() const { return ___lineWidth_5; }
	inline float* get_address_of_lineWidth_5() { return &___lineWidth_5; }
	inline void set_lineWidth_5(float value)
	{
		___lineWidth_5 = value;
	}

	inline static int32_t get_offset_of_labelHeight_6() { return static_cast<int32_t>(offsetof(LabelController_t2075514664, ___labelHeight_6)); }
	inline float get_labelHeight_6() const { return ___labelHeight_6; }
	inline float* get_address_of_labelHeight_6() { return &___labelHeight_6; }
	inline void set_labelHeight_6(float value)
	{
		___labelHeight_6 = value;
	}

	inline static int32_t get_offset_of_labelWidth_7() { return static_cast<int32_t>(offsetof(LabelController_t2075514664, ___labelWidth_7)); }
	inline float get_labelWidth_7() const { return ___labelWidth_7; }
	inline float* get_address_of_labelWidth_7() { return &___labelWidth_7; }
	inline void set_labelWidth_7(float value)
	{
		___labelWidth_7 = value;
	}

	inline static int32_t get_offset_of_color_8() { return static_cast<int32_t>(offsetof(LabelController_t2075514664, ___color_8)); }
	inline Color_t2555686324  get_color_8() const { return ___color_8; }
	inline Color_t2555686324 * get_address_of_color_8() { return &___color_8; }
	inline void set_color_8(Color_t2555686324  value)
	{
		___color_8 = value;
	}

	inline static int32_t get_offset_of_labelTextTransform_9() { return static_cast<int32_t>(offsetof(LabelController_t2075514664, ___labelTextTransform_9)); }
	inline Transform_t3600365921 * get_labelTextTransform_9() const { return ___labelTextTransform_9; }
	inline Transform_t3600365921 ** get_address_of_labelTextTransform_9() { return &___labelTextTransform_9; }
	inline void set_labelTextTransform_9(Transform_t3600365921 * value)
	{
		___labelTextTransform_9 = value;
		Il2CppCodeGenWriteBarrier(&___labelTextTransform_9, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
