﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour3962482529.h"

// UnityEngine.GameObject
struct GameObject_t1113636619;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// showValues
struct  showValues_t2313984496  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject showValues::CameraSystem
	GameObject_t1113636619 * ___CameraSystem_2;
	// UnityEngine.GameObject showValues::gameObjectForShowingSpeed
	GameObject_t1113636619 * ___gameObjectForShowingSpeed_3;
	// UnityEngine.GameObject showValues::gameObjectForShowingSmooth
	GameObject_t1113636619 * ___gameObjectForShowingSmooth_4;
	// UnityEngine.GameObject showValues::gameObjectForShowingOrbitSpeed
	GameObject_t1113636619 * ___gameObjectForShowingOrbitSpeed_5;
	// UnityEngine.GameObject showValues::gameObjectForShowingOrbit
	GameObject_t1113636619 * ___gameObjectForShowingOrbit_6;

public:
	inline static int32_t get_offset_of_CameraSystem_2() { return static_cast<int32_t>(offsetof(showValues_t2313984496, ___CameraSystem_2)); }
	inline GameObject_t1113636619 * get_CameraSystem_2() const { return ___CameraSystem_2; }
	inline GameObject_t1113636619 ** get_address_of_CameraSystem_2() { return &___CameraSystem_2; }
	inline void set_CameraSystem_2(GameObject_t1113636619 * value)
	{
		___CameraSystem_2 = value;
		Il2CppCodeGenWriteBarrier(&___CameraSystem_2, value);
	}

	inline static int32_t get_offset_of_gameObjectForShowingSpeed_3() { return static_cast<int32_t>(offsetof(showValues_t2313984496, ___gameObjectForShowingSpeed_3)); }
	inline GameObject_t1113636619 * get_gameObjectForShowingSpeed_3() const { return ___gameObjectForShowingSpeed_3; }
	inline GameObject_t1113636619 ** get_address_of_gameObjectForShowingSpeed_3() { return &___gameObjectForShowingSpeed_3; }
	inline void set_gameObjectForShowingSpeed_3(GameObject_t1113636619 * value)
	{
		___gameObjectForShowingSpeed_3 = value;
		Il2CppCodeGenWriteBarrier(&___gameObjectForShowingSpeed_3, value);
	}

	inline static int32_t get_offset_of_gameObjectForShowingSmooth_4() { return static_cast<int32_t>(offsetof(showValues_t2313984496, ___gameObjectForShowingSmooth_4)); }
	inline GameObject_t1113636619 * get_gameObjectForShowingSmooth_4() const { return ___gameObjectForShowingSmooth_4; }
	inline GameObject_t1113636619 ** get_address_of_gameObjectForShowingSmooth_4() { return &___gameObjectForShowingSmooth_4; }
	inline void set_gameObjectForShowingSmooth_4(GameObject_t1113636619 * value)
	{
		___gameObjectForShowingSmooth_4 = value;
		Il2CppCodeGenWriteBarrier(&___gameObjectForShowingSmooth_4, value);
	}

	inline static int32_t get_offset_of_gameObjectForShowingOrbitSpeed_5() { return static_cast<int32_t>(offsetof(showValues_t2313984496, ___gameObjectForShowingOrbitSpeed_5)); }
	inline GameObject_t1113636619 * get_gameObjectForShowingOrbitSpeed_5() const { return ___gameObjectForShowingOrbitSpeed_5; }
	inline GameObject_t1113636619 ** get_address_of_gameObjectForShowingOrbitSpeed_5() { return &___gameObjectForShowingOrbitSpeed_5; }
	inline void set_gameObjectForShowingOrbitSpeed_5(GameObject_t1113636619 * value)
	{
		___gameObjectForShowingOrbitSpeed_5 = value;
		Il2CppCodeGenWriteBarrier(&___gameObjectForShowingOrbitSpeed_5, value);
	}

	inline static int32_t get_offset_of_gameObjectForShowingOrbit_6() { return static_cast<int32_t>(offsetof(showValues_t2313984496, ___gameObjectForShowingOrbit_6)); }
	inline GameObject_t1113636619 * get_gameObjectForShowingOrbit_6() const { return ___gameObjectForShowingOrbit_6; }
	inline GameObject_t1113636619 ** get_address_of_gameObjectForShowingOrbit_6() { return &___gameObjectForShowingOrbit_6; }
	inline void set_gameObjectForShowingOrbit_6(GameObject_t1113636619 * value)
	{
		___gameObjectForShowingOrbit_6 = value;
		Il2CppCodeGenWriteBarrier(&___gameObjectForShowingOrbit_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
