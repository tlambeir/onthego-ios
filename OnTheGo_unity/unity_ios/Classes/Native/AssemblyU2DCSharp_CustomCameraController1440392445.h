﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_SampleController2879308770.h"

// Wikitude.WikitudeCamera
struct WikitudeCamera_t2188881370;
// UnityEngine.WebCamTexture
struct WebCamTexture_t1514609158;
// System.Collections.Generic.List`1<CustomCameraController/InputFrameData>
struct List_1_t4046041642;
// UnityEngine.Color32[]
struct Color32U5BU5D_t3850468773;
// CustomCameraRenderer
struct CustomCameraRenderer_t2825061581;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CustomCameraController
struct  CustomCameraController_t1440392445  : public SampleController_t2879308770
{
public:
	// Wikitude.WikitudeCamera CustomCameraController::WikitudeCam
	WikitudeCamera_t2188881370 * ___WikitudeCam_2;
	// UnityEngine.WebCamTexture CustomCameraController::_feed
	WebCamTexture_t1514609158 * ____feed_3;
	// System.Int32 CustomCameraController::_frameDataSize
	int32_t ____frameDataSize_6;
	// System.Int32 CustomCameraController::_frameIndex
	int32_t ____frameIndex_7;
	// System.Int32 CustomCameraController::_bufferWriteIndex
	int32_t ____bufferWriteIndex_8;
	// System.Int32 CustomCameraController::_bufferReadIndex
	int32_t ____bufferReadIndex_9;
	// System.Int32 CustomCameraController::_bufferCount
	int32_t ____bufferCount_10;
	// System.Collections.Generic.List`1<CustomCameraController/InputFrameData> CustomCameraController::_ringBuffer
	List_1_t4046041642 * ____ringBuffer_11;
	// UnityEngine.Color32[] CustomCameraController::_colorData
	Color32U5BU5D_t3850468773* ____colorData_12;
	// CustomCameraRenderer CustomCameraController::Renderer
	CustomCameraRenderer_t2825061581 * ___Renderer_13;

public:
	inline static int32_t get_offset_of_WikitudeCam_2() { return static_cast<int32_t>(offsetof(CustomCameraController_t1440392445, ___WikitudeCam_2)); }
	inline WikitudeCamera_t2188881370 * get_WikitudeCam_2() const { return ___WikitudeCam_2; }
	inline WikitudeCamera_t2188881370 ** get_address_of_WikitudeCam_2() { return &___WikitudeCam_2; }
	inline void set_WikitudeCam_2(WikitudeCamera_t2188881370 * value)
	{
		___WikitudeCam_2 = value;
		Il2CppCodeGenWriteBarrier(&___WikitudeCam_2, value);
	}

	inline static int32_t get_offset_of__feed_3() { return static_cast<int32_t>(offsetof(CustomCameraController_t1440392445, ____feed_3)); }
	inline WebCamTexture_t1514609158 * get__feed_3() const { return ____feed_3; }
	inline WebCamTexture_t1514609158 ** get_address_of__feed_3() { return &____feed_3; }
	inline void set__feed_3(WebCamTexture_t1514609158 * value)
	{
		____feed_3 = value;
		Il2CppCodeGenWriteBarrier(&____feed_3, value);
	}

	inline static int32_t get_offset_of__frameDataSize_6() { return static_cast<int32_t>(offsetof(CustomCameraController_t1440392445, ____frameDataSize_6)); }
	inline int32_t get__frameDataSize_6() const { return ____frameDataSize_6; }
	inline int32_t* get_address_of__frameDataSize_6() { return &____frameDataSize_6; }
	inline void set__frameDataSize_6(int32_t value)
	{
		____frameDataSize_6 = value;
	}

	inline static int32_t get_offset_of__frameIndex_7() { return static_cast<int32_t>(offsetof(CustomCameraController_t1440392445, ____frameIndex_7)); }
	inline int32_t get__frameIndex_7() const { return ____frameIndex_7; }
	inline int32_t* get_address_of__frameIndex_7() { return &____frameIndex_7; }
	inline void set__frameIndex_7(int32_t value)
	{
		____frameIndex_7 = value;
	}

	inline static int32_t get_offset_of__bufferWriteIndex_8() { return static_cast<int32_t>(offsetof(CustomCameraController_t1440392445, ____bufferWriteIndex_8)); }
	inline int32_t get__bufferWriteIndex_8() const { return ____bufferWriteIndex_8; }
	inline int32_t* get_address_of__bufferWriteIndex_8() { return &____bufferWriteIndex_8; }
	inline void set__bufferWriteIndex_8(int32_t value)
	{
		____bufferWriteIndex_8 = value;
	}

	inline static int32_t get_offset_of__bufferReadIndex_9() { return static_cast<int32_t>(offsetof(CustomCameraController_t1440392445, ____bufferReadIndex_9)); }
	inline int32_t get__bufferReadIndex_9() const { return ____bufferReadIndex_9; }
	inline int32_t* get_address_of__bufferReadIndex_9() { return &____bufferReadIndex_9; }
	inline void set__bufferReadIndex_9(int32_t value)
	{
		____bufferReadIndex_9 = value;
	}

	inline static int32_t get_offset_of__bufferCount_10() { return static_cast<int32_t>(offsetof(CustomCameraController_t1440392445, ____bufferCount_10)); }
	inline int32_t get__bufferCount_10() const { return ____bufferCount_10; }
	inline int32_t* get_address_of__bufferCount_10() { return &____bufferCount_10; }
	inline void set__bufferCount_10(int32_t value)
	{
		____bufferCount_10 = value;
	}

	inline static int32_t get_offset_of__ringBuffer_11() { return static_cast<int32_t>(offsetof(CustomCameraController_t1440392445, ____ringBuffer_11)); }
	inline List_1_t4046041642 * get__ringBuffer_11() const { return ____ringBuffer_11; }
	inline List_1_t4046041642 ** get_address_of__ringBuffer_11() { return &____ringBuffer_11; }
	inline void set__ringBuffer_11(List_1_t4046041642 * value)
	{
		____ringBuffer_11 = value;
		Il2CppCodeGenWriteBarrier(&____ringBuffer_11, value);
	}

	inline static int32_t get_offset_of__colorData_12() { return static_cast<int32_t>(offsetof(CustomCameraController_t1440392445, ____colorData_12)); }
	inline Color32U5BU5D_t3850468773* get__colorData_12() const { return ____colorData_12; }
	inline Color32U5BU5D_t3850468773** get_address_of__colorData_12() { return &____colorData_12; }
	inline void set__colorData_12(Color32U5BU5D_t3850468773* value)
	{
		____colorData_12 = value;
		Il2CppCodeGenWriteBarrier(&____colorData_12, value);
	}

	inline static int32_t get_offset_of_Renderer_13() { return static_cast<int32_t>(offsetof(CustomCameraController_t1440392445, ___Renderer_13)); }
	inline CustomCameraRenderer_t2825061581 * get_Renderer_13() const { return ___Renderer_13; }
	inline CustomCameraRenderer_t2825061581 ** get_address_of_Renderer_13() { return &___Renderer_13; }
	inline void set_Renderer_13(CustomCameraRenderer_t2825061581 * value)
	{
		___Renderer_13 = value;
		Il2CppCodeGenWriteBarrier(&___Renderer_13, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
