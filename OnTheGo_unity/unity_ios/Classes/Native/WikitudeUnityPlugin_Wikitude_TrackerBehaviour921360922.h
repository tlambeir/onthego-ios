﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour3962482529.h"
#include "UnityEngine_UnityEngine_Vector33722313464.h"
#include "UnityEngine_UnityEngine_Quaternion2301928331.h"

// Wikitude.TrackerManager
struct TrackerManager_t2102804711;
// System.Collections.Generic.Dictionary`2<Wikitude.Trackable,System.Collections.Generic.HashSet`1<Wikitude.RecognizedTarget>>
struct Dictionary_2_t1769459038;
// Wikitude.RecognizedTarget
struct RecognizedTarget_t3529911668;
// System.Collections.Generic.HashSet`1<Wikitude.TrackableBehaviour>
struct HashSet_1_t1034764117;
// UnityEngine.Camera
struct Camera_t4157153871;
// Wikitude.WikitudeCamera
struct WikitudeCamera_t2188881370;
// Wikitude.TrackerBehaviour/OnTargetsLoadedEvent
struct OnTargetsLoadedEvent_t1820866191;
// Wikitude.TrackerBehaviour/OnErrorLoadingTargetsEvent
struct OnErrorLoadingTargetsEvent_t394526247;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Wikitude.TrackerBehaviour
struct  TrackerBehaviour_t921360922  : public MonoBehaviour_t3962482529
{
public:
	// Wikitude.TrackerManager Wikitude.TrackerBehaviour::_manager
	TrackerManager_t2102804711 * ____manager_2;
	// System.Collections.Generic.Dictionary`2<Wikitude.Trackable,System.Collections.Generic.HashSet`1<Wikitude.RecognizedTarget>> Wikitude.TrackerBehaviour::_registeredTrackables
	Dictionary_2_t1769459038 * ____registeredTrackables_3;
	// Wikitude.RecognizedTarget Wikitude.TrackerBehaviour::_anchorTarget
	RecognizedTarget_t3529911668 * ____anchorTarget_4;
	// UnityEngine.Vector3 Wikitude.TrackerBehaviour::_anchorPositionOffset
	Vector3_t3722313464  ____anchorPositionOffset_5;
	// UnityEngine.Quaternion Wikitude.TrackerBehaviour::_anchorRotationOffset
	Quaternion_t2301928331  ____anchorRotationOffset_6;
	// System.Collections.Generic.HashSet`1<Wikitude.TrackableBehaviour> Wikitude.TrackerBehaviour::_legacyTrackables
	HashSet_1_t1034764117 * ____legacyTrackables_7;
	// System.Boolean Wikitude.TrackerBehaviour::<Initialized>k__BackingField
	bool ___U3CInitializedU3Ek__BackingField_8;
	// UnityEngine.Camera Wikitude.TrackerBehaviour::_sceneCamera
	Camera_t4157153871 * ____sceneCamera_9;
	// UnityEngine.Vector3 Wikitude.TrackerBehaviour::_initialSceneCameraPosition
	Vector3_t3722313464  ____initialSceneCameraPosition_10;
	// UnityEngine.Quaternion Wikitude.TrackerBehaviour::_initialSceneCameraRotation
	Quaternion_t2301928331  ____initialSceneCameraRotation_11;
	// Wikitude.WikitudeCamera Wikitude.TrackerBehaviour::_wikitudeCamera
	WikitudeCamera_t2188881370 * ____wikitudeCamera_12;
	// Wikitude.TrackerBehaviour/OnTargetsLoadedEvent Wikitude.TrackerBehaviour::OnTargetsLoaded
	OnTargetsLoadedEvent_t1820866191 * ___OnTargetsLoaded_13;
	// Wikitude.TrackerBehaviour/OnErrorLoadingTargetsEvent Wikitude.TrackerBehaviour::OnErrorLoadingTargets
	OnErrorLoadingTargetsEvent_t394526247 * ___OnErrorLoadingTargets_14;

public:
	inline static int32_t get_offset_of__manager_2() { return static_cast<int32_t>(offsetof(TrackerBehaviour_t921360922, ____manager_2)); }
	inline TrackerManager_t2102804711 * get__manager_2() const { return ____manager_2; }
	inline TrackerManager_t2102804711 ** get_address_of__manager_2() { return &____manager_2; }
	inline void set__manager_2(TrackerManager_t2102804711 * value)
	{
		____manager_2 = value;
		Il2CppCodeGenWriteBarrier(&____manager_2, value);
	}

	inline static int32_t get_offset_of__registeredTrackables_3() { return static_cast<int32_t>(offsetof(TrackerBehaviour_t921360922, ____registeredTrackables_3)); }
	inline Dictionary_2_t1769459038 * get__registeredTrackables_3() const { return ____registeredTrackables_3; }
	inline Dictionary_2_t1769459038 ** get_address_of__registeredTrackables_3() { return &____registeredTrackables_3; }
	inline void set__registeredTrackables_3(Dictionary_2_t1769459038 * value)
	{
		____registeredTrackables_3 = value;
		Il2CppCodeGenWriteBarrier(&____registeredTrackables_3, value);
	}

	inline static int32_t get_offset_of__anchorTarget_4() { return static_cast<int32_t>(offsetof(TrackerBehaviour_t921360922, ____anchorTarget_4)); }
	inline RecognizedTarget_t3529911668 * get__anchorTarget_4() const { return ____anchorTarget_4; }
	inline RecognizedTarget_t3529911668 ** get_address_of__anchorTarget_4() { return &____anchorTarget_4; }
	inline void set__anchorTarget_4(RecognizedTarget_t3529911668 * value)
	{
		____anchorTarget_4 = value;
		Il2CppCodeGenWriteBarrier(&____anchorTarget_4, value);
	}

	inline static int32_t get_offset_of__anchorPositionOffset_5() { return static_cast<int32_t>(offsetof(TrackerBehaviour_t921360922, ____anchorPositionOffset_5)); }
	inline Vector3_t3722313464  get__anchorPositionOffset_5() const { return ____anchorPositionOffset_5; }
	inline Vector3_t3722313464 * get_address_of__anchorPositionOffset_5() { return &____anchorPositionOffset_5; }
	inline void set__anchorPositionOffset_5(Vector3_t3722313464  value)
	{
		____anchorPositionOffset_5 = value;
	}

	inline static int32_t get_offset_of__anchorRotationOffset_6() { return static_cast<int32_t>(offsetof(TrackerBehaviour_t921360922, ____anchorRotationOffset_6)); }
	inline Quaternion_t2301928331  get__anchorRotationOffset_6() const { return ____anchorRotationOffset_6; }
	inline Quaternion_t2301928331 * get_address_of__anchorRotationOffset_6() { return &____anchorRotationOffset_6; }
	inline void set__anchorRotationOffset_6(Quaternion_t2301928331  value)
	{
		____anchorRotationOffset_6 = value;
	}

	inline static int32_t get_offset_of__legacyTrackables_7() { return static_cast<int32_t>(offsetof(TrackerBehaviour_t921360922, ____legacyTrackables_7)); }
	inline HashSet_1_t1034764117 * get__legacyTrackables_7() const { return ____legacyTrackables_7; }
	inline HashSet_1_t1034764117 ** get_address_of__legacyTrackables_7() { return &____legacyTrackables_7; }
	inline void set__legacyTrackables_7(HashSet_1_t1034764117 * value)
	{
		____legacyTrackables_7 = value;
		Il2CppCodeGenWriteBarrier(&____legacyTrackables_7, value);
	}

	inline static int32_t get_offset_of_U3CInitializedU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(TrackerBehaviour_t921360922, ___U3CInitializedU3Ek__BackingField_8)); }
	inline bool get_U3CInitializedU3Ek__BackingField_8() const { return ___U3CInitializedU3Ek__BackingField_8; }
	inline bool* get_address_of_U3CInitializedU3Ek__BackingField_8() { return &___U3CInitializedU3Ek__BackingField_8; }
	inline void set_U3CInitializedU3Ek__BackingField_8(bool value)
	{
		___U3CInitializedU3Ek__BackingField_8 = value;
	}

	inline static int32_t get_offset_of__sceneCamera_9() { return static_cast<int32_t>(offsetof(TrackerBehaviour_t921360922, ____sceneCamera_9)); }
	inline Camera_t4157153871 * get__sceneCamera_9() const { return ____sceneCamera_9; }
	inline Camera_t4157153871 ** get_address_of__sceneCamera_9() { return &____sceneCamera_9; }
	inline void set__sceneCamera_9(Camera_t4157153871 * value)
	{
		____sceneCamera_9 = value;
		Il2CppCodeGenWriteBarrier(&____sceneCamera_9, value);
	}

	inline static int32_t get_offset_of__initialSceneCameraPosition_10() { return static_cast<int32_t>(offsetof(TrackerBehaviour_t921360922, ____initialSceneCameraPosition_10)); }
	inline Vector3_t3722313464  get__initialSceneCameraPosition_10() const { return ____initialSceneCameraPosition_10; }
	inline Vector3_t3722313464 * get_address_of__initialSceneCameraPosition_10() { return &____initialSceneCameraPosition_10; }
	inline void set__initialSceneCameraPosition_10(Vector3_t3722313464  value)
	{
		____initialSceneCameraPosition_10 = value;
	}

	inline static int32_t get_offset_of__initialSceneCameraRotation_11() { return static_cast<int32_t>(offsetof(TrackerBehaviour_t921360922, ____initialSceneCameraRotation_11)); }
	inline Quaternion_t2301928331  get__initialSceneCameraRotation_11() const { return ____initialSceneCameraRotation_11; }
	inline Quaternion_t2301928331 * get_address_of__initialSceneCameraRotation_11() { return &____initialSceneCameraRotation_11; }
	inline void set__initialSceneCameraRotation_11(Quaternion_t2301928331  value)
	{
		____initialSceneCameraRotation_11 = value;
	}

	inline static int32_t get_offset_of__wikitudeCamera_12() { return static_cast<int32_t>(offsetof(TrackerBehaviour_t921360922, ____wikitudeCamera_12)); }
	inline WikitudeCamera_t2188881370 * get__wikitudeCamera_12() const { return ____wikitudeCamera_12; }
	inline WikitudeCamera_t2188881370 ** get_address_of__wikitudeCamera_12() { return &____wikitudeCamera_12; }
	inline void set__wikitudeCamera_12(WikitudeCamera_t2188881370 * value)
	{
		____wikitudeCamera_12 = value;
		Il2CppCodeGenWriteBarrier(&____wikitudeCamera_12, value);
	}

	inline static int32_t get_offset_of_OnTargetsLoaded_13() { return static_cast<int32_t>(offsetof(TrackerBehaviour_t921360922, ___OnTargetsLoaded_13)); }
	inline OnTargetsLoadedEvent_t1820866191 * get_OnTargetsLoaded_13() const { return ___OnTargetsLoaded_13; }
	inline OnTargetsLoadedEvent_t1820866191 ** get_address_of_OnTargetsLoaded_13() { return &___OnTargetsLoaded_13; }
	inline void set_OnTargetsLoaded_13(OnTargetsLoadedEvent_t1820866191 * value)
	{
		___OnTargetsLoaded_13 = value;
		Il2CppCodeGenWriteBarrier(&___OnTargetsLoaded_13, value);
	}

	inline static int32_t get_offset_of_OnErrorLoadingTargets_14() { return static_cast<int32_t>(offsetof(TrackerBehaviour_t921360922, ___OnErrorLoadingTargets_14)); }
	inline OnErrorLoadingTargetsEvent_t394526247 * get_OnErrorLoadingTargets_14() const { return ___OnErrorLoadingTargets_14; }
	inline OnErrorLoadingTargetsEvent_t394526247 ** get_address_of_OnErrorLoadingTargets_14() { return &___OnErrorLoadingTargets_14; }
	inline void set_OnErrorLoadingTargets_14(OnErrorLoadingTargetsEvent_t394526247 * value)
	{
		___OnErrorLoadingTargets_14 = value;
		Il2CppCodeGenWriteBarrier(&___OnErrorLoadingTargets_14, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
