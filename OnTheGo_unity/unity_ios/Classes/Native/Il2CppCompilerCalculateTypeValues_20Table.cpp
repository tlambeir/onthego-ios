﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "WikitudeUnityPlugin_Wikitude_TrackerBehaviour_OnTa1820866191.h"
#include "WikitudeUnityPlugin_Wikitude_TrackerBehaviour_OnErr394526247.h"
#include "WikitudeUnityPlugin_Wikitude_TrackerManager2102804711.h"
#include "WikitudeUnityPlugin_Wikitude_TrackerManager_Bridge1304078858.h"
#include "WikitudeUnityPlugin_Wikitude_TrackerManager_CloudR2146108836.h"
#include "WikitudeUnityPlugin_Wikitude_CaptureDevicePosition3635123692.h"
#include "WikitudeUnityPlugin_Wikitude_CaptureFocusMode3452326368.h"
#include "WikitudeUnityPlugin_Wikitude_CaptureAutoFocusRestr4132688021.h"
#include "WikitudeUnityPlugin_Wikitude_CaptureFlashMode2444581339.h"
#include "WikitudeUnityPlugin_Wikitude_CaptureDeviceResoluti2690595878.h"
#include "WikitudeUnityPlugin_Wikitude_CaptureDeviceFramerat2114957004.h"
#include "WikitudeUnityPlugin_Wikitude_CameraInfo2490161545.h"
#include "WikitudeUnityPlugin_Wikitude_WikitudeCamera2188881370.h"
#include "WikitudeUnityPlugin_Wikitude_WikitudeCamera_OnCame2345510068.h"
#include "WikitudeUnityPlugin_Wikitude_WikitudeCamera_OnInput818001259.h"
#include "WikitudeUnityPlugin_Wikitude_BackgroundCamera2368011250.h"
#include "WikitudeUnityPlugin_Wikitude_CloudRecognitionServi1770101589.h"
#include "WikitudeUnityPlugin_Wikitude_CloudRecognitionServi3866570042.h"
#include "WikitudeUnityPlugin_Wikitude_CloudRecognitionServi1242699385.h"
#include "WikitudeUnityPlugin_Wikitude_CloudRecognitionServic511199451.h"
#include "WikitudeUnityPlugin_Wikitude_CloudRecognitionServi3712700845.h"
#include "WikitudeUnityPlugin_Wikitude_CloudRecognitionServi2212464566.h"
#include "WikitudeUnityPlugin_Wikitude_CloudRecognitionServi1530367979.h"
#include "WikitudeUnityPlugin_Wikitude_FrameColorSpace1906248721.h"
#include "WikitudeUnityPlugin_Wikitude_FrameStrides352336826.h"
#include "WikitudeUnityPlugin_Wikitude_Frame1062777224.h"
#include "WikitudeUnityPlugin_Wikitude_ImageTarget2322341322.h"
#include "WikitudeUnityPlugin_Wikitude_InstantTarget3313867941.h"
#include "WikitudeUnityPlugin_Wikitude_ObjectTarget688225044.h"
#include "WikitudeUnityPlugin_Wikitude_RecognizedTarget3529911668.h"
#include "WikitudeUnityPlugin_Wikitude_SDKBuildInformation1497247620.h"
#include "WikitudeUnityPlugin_Wikitude_TargetCollectionResourc66041399.h"
#include "WikitudeUnityPlugin_Wikitude_TargetCollectionResou3173959162.h"
#include "WikitudeUnityPlugin_Wikitude_TargetCollectionResou3790443523.h"
#include "WikitudeUnityPlugin_Wikitude_TargetSourceType830527111.h"
#include "WikitudeUnityPlugin_Wikitude_TargetSource2046190744.h"
#include "WikitudeUnityPlugin_Wikitude_WikitudeSDK895845057.h"
#include "WikitudeUnityPlugin_Wikitude_WikitudeSDK_Compatibi1827897472.h"
#include "WikitudeUnityPlugin_Wikitude_CameraProperties3467328122.h"
#include "WikitudeUnityPlugin_Wikitude_Log2685642413.h"
#include "WikitudeUnityPlugin_Wikitude_Math1859187520.h"
#include "WikitudeUnityPlugin_Wikitude_TransformProperties2631539258.h"
#include "WikitudeUnityPlugin_Wikitude_UnityVersion2410471165.h"
#include "WikitudeUnityPlugin_Wikitude_PlatformBase2513924928.h"
#include "WikitudeUnityPlugin_WikitudeEditor_PointCloudRende3664016937.h"
#include "WikitudeUnityPlugin_Wikitude_MapPointCloud955983077.h"
#include "WikitudeUnityPlugin_Wikitude_TrackableBehaviour2469814643.h"
#include "WikitudeUnityPlugin_Wikitude_TrackableBehaviour_On2975486640.h"
#include "WikitudeUnityPlugin_Wikitude_TrackableBehaviour_On2776302756.h"
#include "WikitudeUnityPlugin_U3CPrivateImplementationDetail3057255361.h"
#include "WikitudeUnityPlugin_U3CPrivateImplementationDetail1336283963.h"
#include "WikitudeUnityPlugin_U3CPrivateImplementationDetails498138225.h"
#include "AssemblyU2DCSharpU2Dfirstpass_U3CModuleU3E692745525.h"
#include "AssemblyU2DCSharpU2Dfirstpass_HighlightingSystem_Hi672210414.h"
#include "AssemblyU2DCSharpU2Dfirstpass_HighlightingSystem_H2127932823.h"
#include "AssemblyU2DCSharpU2Dfirstpass_HighlightingSystem_H3431127703.h"
#include "AssemblyU2DCSharpU2Dfirstpass_HighlightingSystem_H1505432117.h"
#include "AssemblyU2DCSharpU2Dfirstpass_HighlightingSystem_H1923179915.h"
#include "AssemblyU2DCSharpU2Dfirstpass_HighlightingSystem_H1776162451.h"
#include "AssemblyU2DCSharpU2Dfirstpass_HighlightingSystem_Hi434873072.h"
#include "AssemblyU2DCSharpU2Dfirstpass_HighlightingSystem_H1838601441.h"
#include "AssemblyU2DCSharpU2Dfirstpass_HighlightingSystem_B2745566494.h"
#include "AssemblyU2DCSharpU2Dfirstpass_HighlightingSystem_An468925605.h"
#include "AssemblyU2DCSharpU2Dfirstpass_HighlightingSystem_Hi582374880.h"
#include "AssemblyU2DCSharpU2Dfirstpass_HighlightingSystem_Hi635619791.h"
#include "AssemblyU2DCSharpU2Dfirstpass_HighlightingSystem_Sh567755140.h"
#include "AssemblyU2DCSharpU2Dfirstpass_MoveSample3412539464.h"
#include "AssemblyU2DCSharpU2Dfirstpass_RotateSample3002381122.h"
#include "AssemblyU2DCSharpU2Dfirstpass_SampleInfo3693012684.h"
#include "AssemblyU2DCSharpU2Dfirstpass_iTween770867771.h"
#include "AssemblyU2DCSharpU2Dfirstpass_iTween_EasingFunctio2767217938.h"
#include "AssemblyU2DCSharpU2Dfirstpass_iTween_ApplyTween3327999347.h"
#include "AssemblyU2DCSharpU2Dfirstpass_iTween_EaseType2573404410.h"
#include "AssemblyU2DCSharpU2Dfirstpass_iTween_LoopType369612249.h"
#include "AssemblyU2DCSharpU2Dfirstpass_iTween_NamedValueCol1091574706.h"
#include "AssemblyU2DCSharpU2Dfirstpass_iTween_Defaults3148213711.h"
#include "AssemblyU2DCSharpU2Dfirstpass_iTween_CRSpline2815350084.h"
#include "AssemblyU2DCSharpU2Dfirstpass_iTween_U3CTweenDelay2686771544.h"
#include "AssemblyU2DCSharpU2Dfirstpass_iTween_U3CTweenResta1737386981.h"
#include "AssemblyU2DCSharpU2Dfirstpass_iTween_U3CStartU3Ec_2390838266.h"
#include "AssemblyU2DCSharpU2Dfirstpass_U3CPrivateImplementa3057255361.h"
#include "AssemblyU2DCSharpU2Dfirstpass_U3CPrivateImplementa2488454196.h"
#include "AssemblyU2DCSharp_U3CModuleU3E692745525.h"
#include "AssemblyU2DCSharp_ContentRenderer1629455466.h"
#include "AssemblyU2DCSharp_CameraTargeting675235903.h"
#include "AssemblyU2DCSharp_HighlighterBase3392950569.h"
#include "AssemblyU2DCSharp_HighlighterConstant2856436213.h"
#include "AssemblyU2DCSharp_HighlighterFlashing2956766845.h"
#include "AssemblyU2DCSharp_HighlighterFlashing_U3CDelayFlash125463829.h"
#include "AssemblyU2DCSharp_HighlighterInteractive1072699259.h"
#include "AssemblyU2DCSharp_HighlighterItem659879438.h"
#include "AssemblyU2DCSharp_HighlighterOccluder927562279.h"
#include "AssemblyU2DCSharp_HighlighterRevealer3738669238.h"
#include "AssemblyU2DCSharp_HighlighterSpectrum1162657826.h"
#include "AssemblyU2DCSharp_HighlighterToggle1139734396.h"
#include "AssemblyU2DCSharp_HighlighterToggle_U3CToggleRouti3067914062.h"
#include "AssemblyU2DCSharp_HighlightingPresetLoop1609534497.h"
#include "AssemblyU2DCSharp_HighlightingPresetLoop_U3CStartU3Ec3858791.h"
#include "AssemblyU2DCSharp_CSHighlighterController1044835482.h"







#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2000 = { sizeof (OnTargetsLoadedEvent_t1820866191), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2001 = { sizeof (OnErrorLoadingTargetsEvent_t394526247), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2002 = { sizeof (TrackerManager_t2102804711), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2002[9] = 
{
	TrackerManager_t2102804711::get_offset_of__targetCollectionResourceId_2(),
	TrackerManager_t2102804711::get_offset_of__cloudRecognitionServiceId_3(),
	TrackerManager_t2102804711::get_offset_of__registeredResources_4(),
	TrackerManager_t2102804711::get_offset_of__registeredCloudRecognitionServices_5(),
	TrackerManager_t2102804711::get_offset_of__recognizedTargets_6(),
	TrackerManager_t2102804711::get_offset_of__bridge_7(),
	TrackerManager_t2102804711::get_offset_of__registeredTrackers_8(),
	TrackerManager_t2102804711::get_offset_of__activeTracker_9(),
	TrackerManager_t2102804711::get_offset_of__wikitudeCamera_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2003 = { sizeof (BridgeError_t1304078858), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2003[3] = 
{
	BridgeError_t1304078858::get_offset_of_U3CCodeU3Ek__BackingField_0(),
	BridgeError_t1304078858::get_offset_of_U3CMessageU3Ek__BackingField_1(),
	BridgeError_t1304078858::get_offset_of_U3CIdentifierU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2004 = { sizeof (CloudRecognitionServerRegion_t2146108836)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2004[4] = 
{
	CloudRecognitionServerRegion_t2146108836::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2005 = { sizeof (CaptureDevicePosition_t3635123692)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2005[3] = 
{
	CaptureDevicePosition_t3635123692::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2006 = { sizeof (CaptureFocusMode_t3452326368)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2006[4] = 
{
	CaptureFocusMode_t3452326368::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2007 = { sizeof (CaptureAutoFocusRestriction_t4132688021)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2007[4] = 
{
	CaptureAutoFocusRestriction_t4132688021::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2008 = { sizeof (CaptureFlashMode_t2444581339)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2008[3] = 
{
	CaptureFlashMode_t2444581339::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2009 = { sizeof (CaptureDeviceResolution_t2690595878)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2009[5] = 
{
	CaptureDeviceResolution_t2690595878::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2010 = { sizeof (CaptureDeviceFramerate_t2114957004)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2010[4] = 
{
	CaptureDeviceFramerate_t2114957004::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2011 = { sizeof (CameraInfo_t2490161545)+ sizeof (Il2CppObject), sizeof(CameraInfo_t2490161545 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2011[3] = 
{
	CameraInfo_t2490161545::get_offset_of_Width_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	CameraInfo_t2490161545::get_offset_of_Height_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	CameraInfo_t2490161545::get_offset_of_Framerate_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2012 = { sizeof (WikitudeCamera_t2188881370), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2012[26] = 
{
	WikitudeCamera_t2188881370::get_offset_of__WikitudeLicenseKey_2(),
	WikitudeCamera_t2188881370::get_offset_of__cameraTexture_3(),
	WikitudeCamera_t2188881370::get_offset_of__cachedDevicePosition_4(),
	WikitudeCamera_t2188881370::get_offset_of__cachedFocusMode_5(),
	WikitudeCamera_t2188881370::get_offset_of__cachedAutoFocusRestriction_6(),
	WikitudeCamera_t2188881370::get_offset_of__cachedZoomLevel_7(),
	WikitudeCamera_t2188881370::get_offset_of__cachedFlashMode_8(),
	WikitudeCamera_t2188881370::get_offset_of__desiredDeviceResolution_9(),
	WikitudeCamera_t2188881370::get_offset_of__desiredDeviceFramerate_10(),
	WikitudeCamera_t2188881370::get_offset_of__enableCamera2_11(),
	WikitudeCamera_t2188881370::get_offset_of__enableInputPlugin_12(),
	WikitudeCamera_t2188881370::get_offset_of__enableMirroring_13(),
	WikitudeCamera_t2188881370::get_offset_of__invertedFrame_14(),
	WikitudeCamera_t2188881370::get_offset_of__mirroredFrame_15(),
	WikitudeCamera_t2188881370::get_offset_of__inputFrameColorSpace_16(),
	WikitudeCamera_t2188881370::get_offset_of__horizontalAngle_17(),
	WikitudeCamera_t2188881370::get_offset_of__inputFrameWidth_18(),
	WikitudeCamera_t2188881370::get_offset_of__inputFrameHeight_19(),
	WikitudeCamera_t2188881370::get_offset_of__requestInputFrameRendering_20(),
	WikitudeCamera_t2188881370::get_offset_of_OnInputPluginRegistered_21(),
	WikitudeCamera_t2188881370::get_offset_of_OnInputPluginFailure_22(),
	WikitudeCamera_t2188881370::get_offset_of_OnCameraFailure_23(),
	WikitudeCamera_t2188881370::get_offset_of__inputPluginRegistered_24(),
	WikitudeCamera_t2188881370::get_offset_of__cameraOpened_25(),
	WikitudeCamera_t2188881370::get_offset_of__bridge_26(),
	WikitudeCamera_t2188881370::get_offset_of__blackPixels_27(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2013 = { sizeof (OnCameraFailureEvent_t2345510068), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2014 = { sizeof (OnInputPluginFailureEvent_t818001259), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2015 = { sizeof (BackgroundCamera_t2368011250), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2015[1] = 
{
	BackgroundCamera_t2368011250::get_offset_of_WikitudeCamera_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2016 = { sizeof (CloudRecognitionServiceResponse_t1770101589), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2016[3] = 
{
	CloudRecognitionServiceResponse_t1770101589::get_offset_of_U3CRecognizedU3Ek__BackingField_0(),
	CloudRecognitionServiceResponse_t1770101589::get_offset_of_U3CInfoU3Ek__BackingField_1(),
	CloudRecognitionServiceResponse_t1770101589::get_offset_of_U3CMetadataU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2017 = { sizeof (CloudRecognitionService_t3866570042), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2017[10] = 
{
	CloudRecognitionService_t3866570042::get_offset_of__clientToken_2(),
	CloudRecognitionService_t3866570042::get_offset_of__targetCollectionId_3(),
	CloudRecognitionService_t3866570042::get_offset_of__serverRegion_4(),
	CloudRecognitionService_t3866570042::get_offset_of__customServerURL_5(),
	CloudRecognitionService_t3866570042::get_offset_of_OnInitialized_6(),
	CloudRecognitionService_t3866570042::get_offset_of_OnInitializationError_7(),
	CloudRecognitionService_t3866570042::get_offset_of_OnRecognitionResponse_8(),
	CloudRecognitionService_t3866570042::get_offset_of_OnRecognitionError_9(),
	CloudRecognitionService_t3866570042::get_offset_of_OnInterruption_10(),
	CloudRecognitionService_t3866570042::get_offset_of_U3CIsContinuousRecognitionRunningU3Ek__BackingField_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2018 = { sizeof (OnInitializedEvent_t1242699385), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2019 = { sizeof (OnInitializationErrorEvent_t511199451), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2020 = { sizeof (OnRecognitionResponseEvent_t3712700845), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2021 = { sizeof (OnRecognitionErrorEvent_t2212464566), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2022 = { sizeof (OnInterruptionEvent_t1530367979), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2023 = { sizeof (FrameColorSpace_t1906248721)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2023[5] = 
{
	FrameColorSpace_t1906248721::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2024 = { sizeof (FrameStrides_t352336826)+ sizeof (Il2CppObject), sizeof(FrameStrides_t352336826 ), 0, 0 };
extern const int32_t g_FieldOffsetTable2024[6] = 
{
	FrameStrides_t352336826::get_offset_of_LuminancePixelStride_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	FrameStrides_t352336826::get_offset_of_LuminanceRowStride_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	FrameStrides_t352336826::get_offset_of_ChrominanceRedPixelStride_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	FrameStrides_t352336826::get_offset_of_ChrominanceRedRowStride_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	FrameStrides_t352336826::get_offset_of_ChrominanceBluePixelStride_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	FrameStrides_t352336826::get_offset_of_ChrominanceBlueRowStride_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2025 = { sizeof (Frame_t1062777224)+ sizeof (Il2CppObject), sizeof(Frame_t1062777224_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2025[7] = 
{
	Frame_t1062777224::get_offset_of_Data_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Frame_t1062777224::get_offset_of_DataSize_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Frame_t1062777224::get_offset_of_Width_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Frame_t1062777224::get_offset_of_Height_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Frame_t1062777224::get_offset_of_ColorSpace_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Frame_t1062777224::get_offset_of_HasStrides_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Frame_t1062777224::get_offset_of_Strides_6() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2026 = { sizeof (ImageTarget_t2322341322), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2026[1] = 
{
	ImageTarget_t2322341322::get_offset_of__scale_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2027 = { sizeof (InstantTarget_t3313867941), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2028 = { sizeof (ObjectTarget_t688225044), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2028[1] = 
{
	ObjectTarget_t688225044::get_offset_of__scale_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2029 = { sizeof (RecognizedTarget_t3529911668), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2029[4] = 
{
	RecognizedTarget_t3529911668::get_offset_of_Drawable_0(),
	RecognizedTarget_t3529911668::get_offset_of_Name_1(),
	RecognizedTarget_t3529911668::get_offset_of_ID_2(),
	RecognizedTarget_t3529911668::get_offset_of_ModelViewMatrix_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2030 = { sizeof (SDKBuildInformation_t1497247620), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2030[4] = 
{
	SDKBuildInformation_t1497247620::get_offset_of_BuildConfiguration_0(),
	SDKBuildInformation_t1497247620::get_offset_of_BuildDate_1(),
	SDKBuildInformation_t1497247620::get_offset_of_BuildNumber_2(),
	SDKBuildInformation_t1497247620::get_offset_of_SDKVersion_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2031 = { sizeof (TargetCollectionResource_t66041399), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2031[4] = 
{
	TargetCollectionResource_t66041399::get_offset_of__targetPath_2(),
	TargetCollectionResource_t66041399::get_offset_of__useCustomURL_3(),
	TargetCollectionResource_t66041399::get_offset_of_OnFinishLoading_4(),
	TargetCollectionResource_t66041399::get_offset_of_OnErrorLoading_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2032 = { sizeof (OnFinishLoadingEvent_t3173959162), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2033 = { sizeof (OnErrorLoadingEvent_t3790443523), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2034 = { sizeof (TargetSourceType_t830527111)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2034[3] = 
{
	TargetSourceType_t830527111::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2035 = { sizeof (TargetSource_t2046190744), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2035[2] = 
{
	TargetSource_t2046190744::get_offset_of__identifier_0(),
	TargetSource_t2046190744::get_offset_of_U3CIsRegisteredU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2036 = { sizeof (WikitudeSDK_t895845057), -1, sizeof(WikitudeSDK_t895845057_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2036[3] = 
{
	0,
	WikitudeSDK_t895845057_StaticFields::get_offset_of__bridge_1(),
	WikitudeSDK_t895845057_StaticFields::get_offset_of__cachedBuildInformation_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2037 = { sizeof (CompatibilityLevel_t1827897472)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2037[3] = 
{
	CompatibilityLevel_t1827897472::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2038 = { sizeof (CameraProperties_t3467328122), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2038[1] = 
{
	CameraProperties_t3467328122::get_offset_of_FieldOfView_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2039 = { sizeof (Log_t2685642413), -1, sizeof(Log_t2685642413_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2039[1] = 
{
	Log_t2685642413_StaticFields::get_offset_of_TAG_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2040 = { sizeof (Math_t1859187520), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2041 = { sizeof (TransformProperties_t2631539258), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2041[3] = 
{
	TransformProperties_t2631539258::get_offset_of_Position_0(),
	TransformProperties_t2631539258::get_offset_of_Rotation_1(),
	TransformProperties_t2631539258::get_offset_of_Scale_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2042 = { sizeof (UnityVersion_t2410471165)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2042[7] = 
{
	UnityVersion_t2410471165::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2043 = { sizeof (PlatformBase_t2513924928), -1, sizeof(PlatformBase_t2513924928_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2043[1] = 
{
	PlatformBase_t2513924928_StaticFields::get_offset_of__instance_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2044 = { sizeof (PointCloudRenderer_t3664016937), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2044[10] = 
{
	PointCloudRenderer_t3664016937::get_offset_of__testMesh_0(),
	PointCloudRenderer_t3664016937::get_offset_of__sceneCamera_1(),
	PointCloudRenderer_t3664016937::get_offset_of__renderMaterial_2(),
	PointCloudRenderer_t3664016937::get_offset_of__cubePoints_3(),
	PointCloudRenderer_t3664016937::get_offset_of__cubeIndices_4(),
	PointCloudRenderer_t3664016937::get_offset_of__meshVertexCount_5(),
	PointCloudRenderer_t3664016937::get_offset_of__drawWithCommandBuffer_6(),
	PointCloudRenderer_t3664016937::get_offset_of__vertices_7(),
	PointCloudRenderer_t3664016937::get_offset_of__indices_8(),
	PointCloudRenderer_t3664016937::get_offset_of__vertexColors_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2045 = { sizeof (MapPointCloud_t955983077), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2045[3] = 
{
	MapPointCloud_t955983077::get_offset_of_FeaturePoints_0(),
	MapPointCloud_t955983077::get_offset_of_Colors_1(),
	MapPointCloud_t955983077::get_offset_of_Scale_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2046 = { sizeof (TrackableBehaviour_t2469814643), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2046[14] = 
{
	TrackableBehaviour_t2469814643::get_offset_of__targetPattern_2(),
	TrackableBehaviour_t2469814643::get_offset_of__targetPatternRegex_3(),
	TrackableBehaviour_t2469814643::get_offset_of__isKnown_4(),
	TrackableBehaviour_t2469814643::get_offset_of__currentTargetName_5(),
	TrackableBehaviour_t2469814643::get_offset_of__extendedTracking_6(),
	TrackableBehaviour_t2469814643::get_offset_of__targetsForExtendedTracking_7(),
	TrackableBehaviour_t2469814643::get_offset_of__autoToggleVisibility_8(),
	TrackableBehaviour_t2469814643::get_offset_of_OnEnterFieldOfVision_9(),
	TrackableBehaviour_t2469814643::get_offset_of_OnExitFieldOfVision_10(),
	TrackableBehaviour_t2469814643::get_offset_of__eventsFoldout_11(),
	TrackableBehaviour_t2469814643::get_offset_of__registeredToTracker_12(),
	TrackableBehaviour_t2469814643::get_offset_of__preview_13(),
	TrackableBehaviour_t2469814643::get_offset_of__previewMaterial_14(),
	TrackableBehaviour_t2469814643::get_offset_of__previewMesh_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2047 = { sizeof (OnEnterFieldOfVisionEvent_t2975486640), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2048 = { sizeof (OnExitFieldOfVisionEvent_t2776302756), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2049 = { sizeof (U3CPrivateImplementationDetailsU3E_t3057255367), -1, sizeof(U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2049[5] = 
{
	U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields::get_offset_of_U24fieldU2D71799C3591A2A1461288D7825CCEBCFDDC7A2E97_0(),
	U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields::get_offset_of_U24fieldU2D49C6DE458D8745BBBB590E4099EA99A067F4ABAD_1(),
	U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields::get_offset_of_U24fieldU2D690A3D8669A212C56F6164F2BF473D2B2074D4DE_2(),
	U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields::get_offset_of_U24fieldU2D5FE77B6BAEF5CB4D4A63CC6C9E211648F35979AF_3(),
	U3CPrivateImplementationDetailsU3E_t3057255367_StaticFields::get_offset_of_U24fieldU2DCD81E62297C7ECE146ADA340591341414F9422DB_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2050 = { sizeof (U24ArrayTypeU3D48_t1336283963)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU3D48_t1336283963 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2051 = { sizeof (U24ArrayTypeU3D64_t498138225)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU3D64_t498138225 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2052 = { sizeof (U3CModuleU3E_t692745533), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2053 = { sizeof (Highlighter_t672210414), -1, sizeof(Highlighter_t672210414_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2053[29] = 
{
	Highlighter_t672210414_StaticFields::get_offset_of_types_2(),
	0,
	0,
	Highlighter_t672210414::get_offset_of_occluderColor_5(),
	Highlighter_t672210414_StaticFields::get_offset_of_renderingOrder_6(),
	Highlighter_t672210414_StaticFields::get_offset_of_highlighters_7(),
	Highlighter_t672210414::get_offset_of_seeThrough_8(),
	Highlighter_t672210414::get_offset_of_occluder_9(),
	Highlighter_t672210414::get_offset_of_forceRender_10(),
	Highlighter_t672210414::get_offset_of_tr_11(),
	Highlighter_t672210414::get_offset_of_highlightableRenderers_12(),
	Highlighter_t672210414::get_offset_of_renderersDirty_13(),
	Highlighter_t672210414_StaticFields::get_offset_of_sComponents_14(),
	Highlighter_t672210414::get_offset_of_mode_15(),
	Highlighter_t672210414::get_offset_of_cachedSeeThrough_16(),
	Highlighter_t672210414::get_offset_of_cachedOnce_17(),
	Highlighter_t672210414::get_offset_of_flashing_18(),
	Highlighter_t672210414::get_offset_of_currentColor_19(),
	Highlighter_t672210414::get_offset_of_transitionValue_20(),
	Highlighter_t672210414::get_offset_of_transitionTarget_21(),
	Highlighter_t672210414::get_offset_of_transitionTime_22(),
	Highlighter_t672210414::get_offset_of_onceColor_23(),
	Highlighter_t672210414::get_offset_of_flashingFreq_24(),
	Highlighter_t672210414::get_offset_of_flashingColorMin_25(),
	Highlighter_t672210414::get_offset_of_flashingColorMax_26(),
	Highlighter_t672210414::get_offset_of_constantColor_27(),
	Highlighter_t672210414_StaticFields::get_offset_of__opaqueShader_28(),
	Highlighter_t672210414_StaticFields::get_offset_of__transparentShader_29(),
	Highlighter_t672210414::get_offset_of__opaqueMaterial_30(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2054 = { sizeof (Mode_t2127932823)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2054[5] = 
{
	Mode_t2127932823::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2055 = { sizeof (HighlighterBlocker_t3431127703), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2056 = { sizeof (HighlightingBlitter_t1505432117), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2056[1] = 
{
	HighlightingBlitter_t1505432117::get_offset_of_renderers_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2057 = { sizeof (HighlightingRenderer_t1923179915), -1, sizeof(HighlightingRenderer_t1923179915_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2057[3] = 
{
	HighlightingRenderer_t1923179915_StaticFields::get_offset_of_defaultPresets_37(),
	HighlightingRenderer_t1923179915::get_offset_of__presets_38(),
	HighlightingRenderer_t1923179915::get_offset_of__presetsReadonly_39(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2058 = { sizeof (HighlighterRenderer_t1776162451), -1, sizeof(HighlighterRenderer_t1776162451_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2058[14] = 
{
	HighlighterRenderer_t1776162451_StaticFields::get_offset_of_transparentCutoff_2(),
	0,
	0,
	HighlighterRenderer_t1776162451_StaticFields::get_offset_of_waitForEndOfFrame_5(),
	HighlighterRenderer_t1776162451_StaticFields::get_offset_of_sRenderType_6(),
	HighlighterRenderer_t1776162451_StaticFields::get_offset_of_sOpaque_7(),
	HighlighterRenderer_t1776162451_StaticFields::get_offset_of_sTransparent_8(),
	HighlighterRenderer_t1776162451_StaticFields::get_offset_of_sTransparentCutout_9(),
	HighlighterRenderer_t1776162451_StaticFields::get_offset_of_sMainTex_10(),
	HighlighterRenderer_t1776162451::get_offset_of_r_11(),
	HighlighterRenderer_t1776162451::get_offset_of_data_12(),
	HighlighterRenderer_t1776162451::get_offset_of_lastCamera_13(),
	HighlighterRenderer_t1776162451::get_offset_of_isAlive_14(),
	HighlighterRenderer_t1776162451::get_offset_of_endOfFrame_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2059 = { sizeof (Data_t434873072)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2059[3] = 
{
	Data_t434873072::get_offset_of_material_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Data_t434873072::get_offset_of_submeshIndex_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Data_t434873072::get_offset_of_transparent_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2060 = { sizeof (U3CEndOfFrameU3Ec__Iterator0_t1838601441), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2060[4] = 
{
	U3CEndOfFrameU3Ec__Iterator0_t1838601441::get_offset_of_U24this_0(),
	U3CEndOfFrameU3Ec__Iterator0_t1838601441::get_offset_of_U24current_1(),
	U3CEndOfFrameU3Ec__Iterator0_t1838601441::get_offset_of_U24disposing_2(),
	U3CEndOfFrameU3Ec__Iterator0_t1838601441::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2061 = { sizeof (BlurDirections_t2745566494)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2061[4] = 
{
	BlurDirections_t2745566494::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2062 = { sizeof (AntiAliasing_t468925605)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2062[6] = 
{
	AntiAliasing_t468925605::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2063 = { sizeof (HighlightingBase_t582374880), -1, sizeof(HighlightingBase_t582374880_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2063[35] = 
{
	HighlightingBase_t582374880_StaticFields::get_offset_of_colorClear_2(),
	HighlightingBase_t582374880_StaticFields::get_offset_of_renderBufferName_3(),
	HighlightingBase_t582374880_StaticFields::get_offset_of_identityMatrix_4(),
	HighlightingBase_t582374880_StaticFields::get_offset_of_keywordStraightDirections_5(),
	HighlightingBase_t582374880_StaticFields::get_offset_of_keywordAllDirections_6(),
	0,
	HighlightingBase_t582374880_StaticFields::get_offset_of__shaderPropertyID_8(),
	HighlightingBase_t582374880::get_offset_of_renderBuffer_9(),
	HighlightingBase_t582374880::get_offset_of_cachedWidth_10(),
	HighlightingBase_t582374880::get_offset_of_cachedHeight_11(),
	HighlightingBase_t582374880::get_offset_of_cachedAA_12(),
	HighlightingBase_t582374880::get_offset_of__downsampleFactor_13(),
	HighlightingBase_t582374880::get_offset_of__iterations_14(),
	HighlightingBase_t582374880::get_offset_of__blurMinSpread_15(),
	HighlightingBase_t582374880::get_offset_of__blurSpread_16(),
	HighlightingBase_t582374880::get_offset_of__blurIntensity_17(),
	HighlightingBase_t582374880::get_offset_of__blurDirections_18(),
	HighlightingBase_t582374880::get_offset_of__blitter_19(),
	HighlightingBase_t582374880::get_offset_of__antiAliasing_20(),
	HighlightingBase_t582374880::get_offset_of_highlightingBufferID_21(),
	HighlightingBase_t582374880::get_offset_of_blur1ID_22(),
	HighlightingBase_t582374880::get_offset_of_blur2ID_23(),
	HighlightingBase_t582374880::get_offset_of_highlightingBuffer_24(),
	HighlightingBase_t582374880::get_offset_of_cam_25(),
	0,
	0,
	0,
	HighlightingBase_t582374880_StaticFields::get_offset_of_shaderPaths_29(),
	HighlightingBase_t582374880_StaticFields::get_offset_of_shaders_30(),
	HighlightingBase_t582374880_StaticFields::get_offset_of_materials_31(),
	HighlightingBase_t582374880::get_offset_of_blurMaterial_32(),
	HighlightingBase_t582374880::get_offset_of_cutMaterial_33(),
	HighlightingBase_t582374880::get_offset_of_compMaterial_34(),
	HighlightingBase_t582374880_StaticFields::get_offset_of_initialized_35(),
	HighlightingBase_t582374880_StaticFields::get_offset_of_cameras_36(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2064 = { sizeof (HighlightingPreset_t635619791)+ sizeof (Il2CppObject), sizeof(HighlightingPreset_t635619791_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2064[7] = 
{
	HighlightingPreset_t635619791::get_offset_of__name_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	HighlightingPreset_t635619791::get_offset_of__downsampleFactor_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	HighlightingPreset_t635619791::get_offset_of__iterations_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	HighlightingPreset_t635619791::get_offset_of__blurMinSpread_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	HighlightingPreset_t635619791::get_offset_of__blurSpread_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	HighlightingPreset_t635619791::get_offset_of__blurIntensity_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
	HighlightingPreset_t635619791::get_offset_of__blurDirections_6() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2065 = { sizeof (ShaderPropertyID_t567755140), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2065[9] = 
{
	ShaderPropertyID_t567755140::get_offset_of__MainTex_0(),
	ShaderPropertyID_t567755140::get_offset_of__Color_1(),
	ShaderPropertyID_t567755140::get_offset_of__Cutoff_2(),
	ShaderPropertyID_t567755140::get_offset_of__Intensity_3(),
	ShaderPropertyID_t567755140::get_offset_of__Cull_4(),
	ShaderPropertyID_t567755140::get_offset_of__HighlightingBlur1_5(),
	ShaderPropertyID_t567755140::get_offset_of__HighlightingBlur2_6(),
	ShaderPropertyID_t567755140::get_offset_of__HighlightingBuffer_7(),
	ShaderPropertyID_t567755140::get_offset_of__HighlightingBlurOffset_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2066 = { sizeof (MoveSample_t3412539464), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2067 = { sizeof (RotateSample_t3002381122), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2068 = { sizeof (SampleInfo_t3693012684), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2069 = { sizeof (iTween_t770867771), -1, sizeof(iTween_t770867771_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2069[39] = 
{
	iTween_t770867771_StaticFields::get_offset_of_tweens_2(),
	iTween_t770867771_StaticFields::get_offset_of_cameraFade_3(),
	iTween_t770867771::get_offset_of_id_4(),
	iTween_t770867771::get_offset_of_type_5(),
	iTween_t770867771::get_offset_of_method_6(),
	iTween_t770867771::get_offset_of_easeType_7(),
	iTween_t770867771::get_offset_of_time_8(),
	iTween_t770867771::get_offset_of_delay_9(),
	iTween_t770867771::get_offset_of_loopType_10(),
	iTween_t770867771::get_offset_of_isRunning_11(),
	iTween_t770867771::get_offset_of_isPaused_12(),
	iTween_t770867771::get_offset_of__name_13(),
	iTween_t770867771::get_offset_of_runningTime_14(),
	iTween_t770867771::get_offset_of_percentage_15(),
	iTween_t770867771::get_offset_of_delayStarted_16(),
	iTween_t770867771::get_offset_of_kinematic_17(),
	iTween_t770867771::get_offset_of_isLocal_18(),
	iTween_t770867771::get_offset_of_loop_19(),
	iTween_t770867771::get_offset_of_reverse_20(),
	iTween_t770867771::get_offset_of_wasPaused_21(),
	iTween_t770867771::get_offset_of_physics_22(),
	iTween_t770867771::get_offset_of_tweenArguments_23(),
	iTween_t770867771::get_offset_of_space_24(),
	iTween_t770867771::get_offset_of_ease_25(),
	iTween_t770867771::get_offset_of_apply_26(),
	iTween_t770867771::get_offset_of_audioSource_27(),
	iTween_t770867771::get_offset_of_vector3s_28(),
	iTween_t770867771::get_offset_of_vector2s_29(),
	iTween_t770867771::get_offset_of_colors_30(),
	iTween_t770867771::get_offset_of_floats_31(),
	iTween_t770867771::get_offset_of_rects_32(),
	iTween_t770867771::get_offset_of_path_33(),
	iTween_t770867771::get_offset_of_preUpdate_34(),
	iTween_t770867771::get_offset_of_postUpdate_35(),
	iTween_t770867771::get_offset_of_namedcolorvalue_36(),
	iTween_t770867771::get_offset_of_lastRealTime_37(),
	iTween_t770867771::get_offset_of_useRealTime_38(),
	iTween_t770867771::get_offset_of_thisTransform_39(),
	iTween_t770867771_StaticFields::get_offset_of_U3CU3Ef__switchU24map0_40(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2070 = { sizeof (EasingFunction_t2767217938), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2071 = { sizeof (ApplyTween_t3327999347), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2072 = { sizeof (EaseType_t2573404410)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2072[34] = 
{
	EaseType_t2573404410::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2073 = { sizeof (LoopType_t369612249)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2073[4] = 
{
	LoopType_t369612249::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2074 = { sizeof (NamedValueColor_t1091574706)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2074[5] = 
{
	NamedValueColor_t1091574706::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2075 = { sizeof (Defaults_t3148213711), -1, sizeof(Defaults_t3148213711_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2075[16] = 
{
	Defaults_t3148213711_StaticFields::get_offset_of_time_0(),
	Defaults_t3148213711_StaticFields::get_offset_of_delay_1(),
	Defaults_t3148213711_StaticFields::get_offset_of_namedColorValue_2(),
	Defaults_t3148213711_StaticFields::get_offset_of_loopType_3(),
	Defaults_t3148213711_StaticFields::get_offset_of_easeType_4(),
	Defaults_t3148213711_StaticFields::get_offset_of_lookSpeed_5(),
	Defaults_t3148213711_StaticFields::get_offset_of_isLocal_6(),
	Defaults_t3148213711_StaticFields::get_offset_of_space_7(),
	Defaults_t3148213711_StaticFields::get_offset_of_orientToPath_8(),
	Defaults_t3148213711_StaticFields::get_offset_of_color_9(),
	Defaults_t3148213711_StaticFields::get_offset_of_updateTimePercentage_10(),
	Defaults_t3148213711_StaticFields::get_offset_of_updateTime_11(),
	Defaults_t3148213711_StaticFields::get_offset_of_cameraFadeDepth_12(),
	Defaults_t3148213711_StaticFields::get_offset_of_lookAhead_13(),
	Defaults_t3148213711_StaticFields::get_offset_of_useRealTime_14(),
	Defaults_t3148213711_StaticFields::get_offset_of_up_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2076 = { sizeof (CRSpline_t2815350084), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2076[1] = 
{
	CRSpline_t2815350084::get_offset_of_pts_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2077 = { sizeof (U3CTweenDelayU3Ec__Iterator0_t2686771544), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2077[4] = 
{
	U3CTweenDelayU3Ec__Iterator0_t2686771544::get_offset_of_U24this_0(),
	U3CTweenDelayU3Ec__Iterator0_t2686771544::get_offset_of_U24current_1(),
	U3CTweenDelayU3Ec__Iterator0_t2686771544::get_offset_of_U24disposing_2(),
	U3CTweenDelayU3Ec__Iterator0_t2686771544::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2078 = { sizeof (U3CTweenRestartU3Ec__Iterator1_t1737386981), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2078[4] = 
{
	U3CTweenRestartU3Ec__Iterator1_t1737386981::get_offset_of_U24this_0(),
	U3CTweenRestartU3Ec__Iterator1_t1737386981::get_offset_of_U24current_1(),
	U3CTweenRestartU3Ec__Iterator1_t1737386981::get_offset_of_U24disposing_2(),
	U3CTweenRestartU3Ec__Iterator1_t1737386981::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2079 = { sizeof (U3CStartU3Ec__Iterator2_t2390838266), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2079[4] = 
{
	U3CStartU3Ec__Iterator2_t2390838266::get_offset_of_U24this_0(),
	U3CStartU3Ec__Iterator2_t2390838266::get_offset_of_U24current_1(),
	U3CStartU3Ec__Iterator2_t2390838266::get_offset_of_U24disposing_2(),
	U3CStartU3Ec__Iterator2_t2390838266::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2080 = { sizeof (U3CPrivateImplementationDetailsU3E_t3057255368), -1, sizeof(U3CPrivateImplementationDetailsU3E_t3057255368_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2080[1] = 
{
	U3CPrivateImplementationDetailsU3E_t3057255368_StaticFields::get_offset_of_U24fieldU2DE429CCA3F703A39CC5954A6572FEC9086135B34E_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2081 = { sizeof (U24ArrayTypeU3D12_t2488454197)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU3D12_t2488454197 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2082 = { sizeof (U3CModuleU3E_t692745534), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2083 = { sizeof (ContentRenderer_t1629455466), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2084 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2085 = { sizeof (CameraTargeting_t675235903), -1, sizeof(CameraTargeting_t675235903_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2085[5] = 
{
	CameraTargeting_t675235903::get_offset_of_targetingLayerMask_2(),
	CameraTargeting_t675235903::get_offset_of_targetingRayLength_3(),
	CameraTargeting_t675235903::get_offset_of_cam_4(),
	CameraTargeting_t675235903_StaticFields::get_offset_of_buttonFire1_5(),
	CameraTargeting_t675235903_StaticFields::get_offset_of_buttonFire2_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2086 = { sizeof (HighlighterBase_t3392950569), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2086[2] = 
{
	HighlighterBase_t3392950569::get_offset_of_seeThrough_2(),
	HighlighterBase_t3392950569::get_offset_of_h_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2087 = { sizeof (HighlighterConstant_t2856436213), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2087[1] = 
{
	HighlighterConstant_t2856436213::get_offset_of_color_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2088 = { sizeof (HighlighterFlashing_t2956766845), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2088[5] = 
{
	HighlighterFlashing_t2956766845::get_offset_of_flashingStartColor_4(),
	HighlighterFlashing_t2956766845::get_offset_of_flashingEndColor_5(),
	HighlighterFlashing_t2956766845::get_offset_of_flashingDelay_6(),
	HighlighterFlashing_t2956766845::get_offset_of_flashingFrequency_7(),
	HighlighterFlashing_t2956766845::get_offset_of_coroutine_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2089 = { sizeof (U3CDelayFlashingU3Ec__Iterator0_t125463829), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2089[4] = 
{
	U3CDelayFlashingU3Ec__Iterator0_t125463829::get_offset_of_U24this_0(),
	U3CDelayFlashingU3Ec__Iterator0_t125463829::get_offset_of_U24current_1(),
	U3CDelayFlashingU3Ec__Iterator0_t125463829::get_offset_of_U24disposing_2(),
	U3CDelayFlashingU3Ec__Iterator0_t125463829::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2090 = { sizeof (HighlighterInteractive_t1072699259), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2091 = { sizeof (HighlighterItem_t659879438), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2091[4] = 
{
	HighlighterItem_t659879438::get_offset_of_seeThrough_2(),
	HighlighterItem_t659879438::get_offset_of_revealColor_3(),
	HighlighterItem_t659879438::get_offset_of_h_4(),
	HighlighterItem_t659879438::get_offset_of_revealCount_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2092 = { sizeof (HighlighterOccluder_t927562279), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2093 = { sizeof (HighlighterRevealer_t3738669238), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2093[6] = 
{
	HighlighterRevealer_t3738669238::get_offset_of_radius_2(),
	HighlighterRevealer_t3738669238::get_offset_of_layerMask_3(),
	HighlighterRevealer_t3738669238::get_offset_of_items_4(),
	HighlighterRevealer_t3738669238::get_offset_of_tr_5(),
	HighlighterRevealer_t3738669238::get_offset_of_sphereMesh_6(),
	HighlighterRevealer_t3738669238::get_offset_of_sphereMaterial_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2094 = { sizeof (HighlighterSpectrum_t1162657826), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2094[3] = 
{
	HighlighterSpectrum_t1162657826::get_offset_of_random_4(),
	HighlighterSpectrum_t1162657826::get_offset_of_velocity_5(),
	HighlighterSpectrum_t1162657826::get_offset_of_t_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2095 = { sizeof (HighlighterToggle_t1139734396), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2095[3] = 
{
	HighlighterToggle_t1139734396::get_offset_of_delayMin_4(),
	HighlighterToggle_t1139734396::get_offset_of_delayMax_5(),
	HighlighterToggle_t1139734396::get_offset_of_state_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2096 = { sizeof (U3CToggleRoutineU3Ec__Iterator0_t3067914062), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2096[4] = 
{
	U3CToggleRoutineU3Ec__Iterator0_t3067914062::get_offset_of_U24this_0(),
	U3CToggleRoutineU3Ec__Iterator0_t3067914062::get_offset_of_U24current_1(),
	U3CToggleRoutineU3Ec__Iterator0_t3067914062::get_offset_of_U24disposing_2(),
	U3CToggleRoutineU3Ec__Iterator0_t3067914062::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2097 = { sizeof (HighlightingPresetLoop_t1609534497), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2097[3] = 
{
	HighlightingPresetLoop_t1609534497::get_offset_of_highlightingRenderer_2(),
	HighlightingPresetLoop_t1609534497::get_offset_of_interval_3(),
	HighlightingPresetLoop_t1609534497::get_offset_of_customPresets_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2098 = { sizeof (U3CStartU3Ec__Iterator0_t3858791), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2098[7] = 
{
	U3CStartU3Ec__Iterator0_t3858791::get_offset_of_U3CindexU3E__0_0(),
	U3CStartU3Ec__Iterator0_t3858791::get_offset_of_U3CpresetsU3E__1_1(),
	U3CStartU3Ec__Iterator0_t3858791::get_offset_of_U3CpresetU3E__1_2(),
	U3CStartU3Ec__Iterator0_t3858791::get_offset_of_U24this_3(),
	U3CStartU3Ec__Iterator0_t3858791::get_offset_of_U24current_4(),
	U3CStartU3Ec__Iterator0_t3858791::get_offset_of_U24disposing_5(),
	U3CStartU3Ec__Iterator0_t3858791::get_offset_of_U24PC_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2099 = { sizeof (CSHighlighterController_t1044835482), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2099[1] = 
{
	CSHighlighterController_t1044835482::get_offset_of_h_2(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
