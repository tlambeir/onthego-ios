﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour3962482529.h"
#include "UnityEngine_UnityEngine_Vector33722313464.h"

// Lean.Touch.LeanCameraZoomSmooth
struct LeanCameraZoomSmooth_t1926209604;
// SmoothOrbitCam
struct SmoothOrbitCam_t3313789169;
// UnityEngine.Transform
struct Transform_t3600365921;
// Lean.Touch.LeanCameraMoveSmooth
struct LeanCameraMoveSmooth_t3552812877;
// Launch_Animation
struct Launch_Animation_t763963988;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CenterScript
struct  CenterScript_t2028411258  : public MonoBehaviour_t3962482529
{
public:
	// Lean.Touch.LeanCameraZoomSmooth CenterScript::cameraZoom
	LeanCameraZoomSmooth_t1926209604 * ___cameraZoom_2;
	// Lean.Touch.LeanCameraZoomSmooth CenterScript::cameraZoomUI
	LeanCameraZoomSmooth_t1926209604 * ___cameraZoomUI_3;
	// SmoothOrbitCam CenterScript::SmoothOrbitCam
	SmoothOrbitCam_t3313789169 * ___SmoothOrbitCam_4;
	// UnityEngine.Transform CenterScript::mainCameraTransform
	Transform_t3600365921 * ___mainCameraTransform_5;
	// Lean.Touch.LeanCameraMoveSmooth CenterScript::leanCameraMoveSmooth
	LeanCameraMoveSmooth_t3552812877 * ___leanCameraMoveSmooth_6;
	// System.Single CenterScript::originalZoom
	float ___originalZoom_7;
	// System.Single CenterScript::originalZoomUI
	float ___originalZoomUI_8;
	// UnityEngine.Vector3 CenterScript::originalposition
	Vector3_t3722313464  ___originalposition_9;
	// Launch_Animation CenterScript::animationLauncher
	Launch_Animation_t763963988 * ___animationLauncher_10;

public:
	inline static int32_t get_offset_of_cameraZoom_2() { return static_cast<int32_t>(offsetof(CenterScript_t2028411258, ___cameraZoom_2)); }
	inline LeanCameraZoomSmooth_t1926209604 * get_cameraZoom_2() const { return ___cameraZoom_2; }
	inline LeanCameraZoomSmooth_t1926209604 ** get_address_of_cameraZoom_2() { return &___cameraZoom_2; }
	inline void set_cameraZoom_2(LeanCameraZoomSmooth_t1926209604 * value)
	{
		___cameraZoom_2 = value;
		Il2CppCodeGenWriteBarrier(&___cameraZoom_2, value);
	}

	inline static int32_t get_offset_of_cameraZoomUI_3() { return static_cast<int32_t>(offsetof(CenterScript_t2028411258, ___cameraZoomUI_3)); }
	inline LeanCameraZoomSmooth_t1926209604 * get_cameraZoomUI_3() const { return ___cameraZoomUI_3; }
	inline LeanCameraZoomSmooth_t1926209604 ** get_address_of_cameraZoomUI_3() { return &___cameraZoomUI_3; }
	inline void set_cameraZoomUI_3(LeanCameraZoomSmooth_t1926209604 * value)
	{
		___cameraZoomUI_3 = value;
		Il2CppCodeGenWriteBarrier(&___cameraZoomUI_3, value);
	}

	inline static int32_t get_offset_of_SmoothOrbitCam_4() { return static_cast<int32_t>(offsetof(CenterScript_t2028411258, ___SmoothOrbitCam_4)); }
	inline SmoothOrbitCam_t3313789169 * get_SmoothOrbitCam_4() const { return ___SmoothOrbitCam_4; }
	inline SmoothOrbitCam_t3313789169 ** get_address_of_SmoothOrbitCam_4() { return &___SmoothOrbitCam_4; }
	inline void set_SmoothOrbitCam_4(SmoothOrbitCam_t3313789169 * value)
	{
		___SmoothOrbitCam_4 = value;
		Il2CppCodeGenWriteBarrier(&___SmoothOrbitCam_4, value);
	}

	inline static int32_t get_offset_of_mainCameraTransform_5() { return static_cast<int32_t>(offsetof(CenterScript_t2028411258, ___mainCameraTransform_5)); }
	inline Transform_t3600365921 * get_mainCameraTransform_5() const { return ___mainCameraTransform_5; }
	inline Transform_t3600365921 ** get_address_of_mainCameraTransform_5() { return &___mainCameraTransform_5; }
	inline void set_mainCameraTransform_5(Transform_t3600365921 * value)
	{
		___mainCameraTransform_5 = value;
		Il2CppCodeGenWriteBarrier(&___mainCameraTransform_5, value);
	}

	inline static int32_t get_offset_of_leanCameraMoveSmooth_6() { return static_cast<int32_t>(offsetof(CenterScript_t2028411258, ___leanCameraMoveSmooth_6)); }
	inline LeanCameraMoveSmooth_t3552812877 * get_leanCameraMoveSmooth_6() const { return ___leanCameraMoveSmooth_6; }
	inline LeanCameraMoveSmooth_t3552812877 ** get_address_of_leanCameraMoveSmooth_6() { return &___leanCameraMoveSmooth_6; }
	inline void set_leanCameraMoveSmooth_6(LeanCameraMoveSmooth_t3552812877 * value)
	{
		___leanCameraMoveSmooth_6 = value;
		Il2CppCodeGenWriteBarrier(&___leanCameraMoveSmooth_6, value);
	}

	inline static int32_t get_offset_of_originalZoom_7() { return static_cast<int32_t>(offsetof(CenterScript_t2028411258, ___originalZoom_7)); }
	inline float get_originalZoom_7() const { return ___originalZoom_7; }
	inline float* get_address_of_originalZoom_7() { return &___originalZoom_7; }
	inline void set_originalZoom_7(float value)
	{
		___originalZoom_7 = value;
	}

	inline static int32_t get_offset_of_originalZoomUI_8() { return static_cast<int32_t>(offsetof(CenterScript_t2028411258, ___originalZoomUI_8)); }
	inline float get_originalZoomUI_8() const { return ___originalZoomUI_8; }
	inline float* get_address_of_originalZoomUI_8() { return &___originalZoomUI_8; }
	inline void set_originalZoomUI_8(float value)
	{
		___originalZoomUI_8 = value;
	}

	inline static int32_t get_offset_of_originalposition_9() { return static_cast<int32_t>(offsetof(CenterScript_t2028411258, ___originalposition_9)); }
	inline Vector3_t3722313464  get_originalposition_9() const { return ___originalposition_9; }
	inline Vector3_t3722313464 * get_address_of_originalposition_9() { return &___originalposition_9; }
	inline void set_originalposition_9(Vector3_t3722313464  value)
	{
		___originalposition_9 = value;
	}

	inline static int32_t get_offset_of_animationLauncher_10() { return static_cast<int32_t>(offsetof(CenterScript_t2028411258, ___animationLauncher_10)); }
	inline Launch_Animation_t763963988 * get_animationLauncher_10() const { return ___animationLauncher_10; }
	inline Launch_Animation_t763963988 ** get_address_of_animationLauncher_10() { return &___animationLauncher_10; }
	inline void set_animationLauncher_10(Launch_Animation_t763963988 * value)
	{
		___animationLauncher_10 = value;
		Il2CppCodeGenWriteBarrier(&___animationLauncher_10, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
