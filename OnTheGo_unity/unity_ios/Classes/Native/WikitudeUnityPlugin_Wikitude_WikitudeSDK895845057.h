﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// Wikitude.IPlatformBridge
struct IPlatformBridge_t1124789254;
// Wikitude.SDKBuildInformation
struct SDKBuildInformation_t1497247620;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Wikitude.WikitudeSDK
struct  WikitudeSDK_t895845057  : public Il2CppObject
{
public:

public:
};

struct WikitudeSDK_t895845057_StaticFields
{
public:
	// Wikitude.IPlatformBridge Wikitude.WikitudeSDK::_bridge
	Il2CppObject * ____bridge_1;
	// Wikitude.SDKBuildInformation Wikitude.WikitudeSDK::_cachedBuildInformation
	SDKBuildInformation_t1497247620 * ____cachedBuildInformation_2;

public:
	inline static int32_t get_offset_of__bridge_1() { return static_cast<int32_t>(offsetof(WikitudeSDK_t895845057_StaticFields, ____bridge_1)); }
	inline Il2CppObject * get__bridge_1() const { return ____bridge_1; }
	inline Il2CppObject ** get_address_of__bridge_1() { return &____bridge_1; }
	inline void set__bridge_1(Il2CppObject * value)
	{
		____bridge_1 = value;
		Il2CppCodeGenWriteBarrier(&____bridge_1, value);
	}

	inline static int32_t get_offset_of__cachedBuildInformation_2() { return static_cast<int32_t>(offsetof(WikitudeSDK_t895845057_StaticFields, ____cachedBuildInformation_2)); }
	inline SDKBuildInformation_t1497247620 * get__cachedBuildInformation_2() const { return ____cachedBuildInformation_2; }
	inline SDKBuildInformation_t1497247620 ** get_address_of__cachedBuildInformation_2() { return &____cachedBuildInformation_2; }
	inline void set__cachedBuildInformation_2(SDKBuildInformation_t1497247620 * value)
	{
		____cachedBuildInformation_2 = value;
		Il2CppCodeGenWriteBarrier(&____cachedBuildInformation_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
