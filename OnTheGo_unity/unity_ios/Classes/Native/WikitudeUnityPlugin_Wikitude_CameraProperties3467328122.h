﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "WikitudeUnityPlugin_Wikitude_TransformProperties2631539258.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Wikitude.CameraProperties
struct  CameraProperties_t3467328122  : public TransformProperties_t2631539258
{
public:
	// System.Single Wikitude.CameraProperties::FieldOfView
	float ___FieldOfView_3;

public:
	inline static int32_t get_offset_of_FieldOfView_3() { return static_cast<int32_t>(offsetof(CameraProperties_t3467328122, ___FieldOfView_3)); }
	inline float get_FieldOfView_3() const { return ___FieldOfView_3; }
	inline float* get_address_of_FieldOfView_3() { return &___FieldOfView_3; }
	inline void set_FieldOfView_3(float value)
	{
		___FieldOfView_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
