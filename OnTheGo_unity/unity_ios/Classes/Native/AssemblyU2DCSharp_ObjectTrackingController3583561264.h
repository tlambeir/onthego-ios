﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_SampleController2879308770.h"

// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ObjectTrackingController
struct  ObjectTrackingController_t3583561264  : public SampleController_t2879308770
{
public:
	// System.Boolean ObjectTrackingController::_isInstructionsAnimationPlaying
	bool ____isInstructionsAnimationPlaying_7;
	// System.Boolean ObjectTrackingController::_isSirenAnimationPlaying
	bool ____isSirenAnimationPlaying_8;

public:
	inline static int32_t get_offset_of__isInstructionsAnimationPlaying_7() { return static_cast<int32_t>(offsetof(ObjectTrackingController_t3583561264, ____isInstructionsAnimationPlaying_7)); }
	inline bool get__isInstructionsAnimationPlaying_7() const { return ____isInstructionsAnimationPlaying_7; }
	inline bool* get_address_of__isInstructionsAnimationPlaying_7() { return &____isInstructionsAnimationPlaying_7; }
	inline void set__isInstructionsAnimationPlaying_7(bool value)
	{
		____isInstructionsAnimationPlaying_7 = value;
	}

	inline static int32_t get_offset_of__isSirenAnimationPlaying_8() { return static_cast<int32_t>(offsetof(ObjectTrackingController_t3583561264, ____isSirenAnimationPlaying_8)); }
	inline bool get__isSirenAnimationPlaying_8() const { return ____isSirenAnimationPlaying_8; }
	inline bool* get_address_of__isSirenAnimationPlaying_8() { return &____isSirenAnimationPlaying_8; }
	inline void set__isSirenAnimationPlaying_8(bool value)
	{
		____isSirenAnimationPlaying_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
