﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"
#include "AssemblyU2DCSharpU2Dfirstpass_U3CPrivateImplementa2488454196.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>
struct  U3CPrivateImplementationDetailsU3E_t3057255368  : public Il2CppObject
{
public:

public:
};

struct U3CPrivateImplementationDetailsU3E_t3057255368_StaticFields
{
public:
	// <PrivateImplementationDetails>/$ArrayType=12 <PrivateImplementationDetails>::$field-E429CCA3F703A39CC5954A6572FEC9086135B34E
	U24ArrayTypeU3D12_t2488454197  ___U24fieldU2DE429CCA3F703A39CC5954A6572FEC9086135B34E_0;

public:
	inline static int32_t get_offset_of_U24fieldU2DE429CCA3F703A39CC5954A6572FEC9086135B34E_0() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255368_StaticFields, ___U24fieldU2DE429CCA3F703A39CC5954A6572FEC9086135B34E_0)); }
	inline U24ArrayTypeU3D12_t2488454197  get_U24fieldU2DE429CCA3F703A39CC5954A6572FEC9086135B34E_0() const { return ___U24fieldU2DE429CCA3F703A39CC5954A6572FEC9086135B34E_0; }
	inline U24ArrayTypeU3D12_t2488454197 * get_address_of_U24fieldU2DE429CCA3F703A39CC5954A6572FEC9086135B34E_0() { return &___U24fieldU2DE429CCA3F703A39CC5954A6572FEC9086135B34E_0; }
	inline void set_U24fieldU2DE429CCA3F703A39CC5954A6572FEC9086135B34E_0(U24ArrayTypeU3D12_t2488454197  value)
	{
		___U24fieldU2DE429CCA3F703A39CC5954A6572FEC9086135B34E_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
