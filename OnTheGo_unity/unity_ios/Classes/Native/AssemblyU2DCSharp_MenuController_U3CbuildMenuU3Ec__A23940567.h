﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// Branch
struct Branch_t1428165248;
// MenuController
struct MenuController_t3930949237;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MenuController/<buildMenu>c__AnonStorey1
struct  U3CbuildMenuU3Ec__AnonStorey1_t23940567  : public Il2CppObject
{
public:
	// Branch MenuController/<buildMenu>c__AnonStorey1::root
	Branch_t1428165248 * ___root_0;
	// MenuController MenuController/<buildMenu>c__AnonStorey1::$this
	MenuController_t3930949237 * ___U24this_1;

public:
	inline static int32_t get_offset_of_root_0() { return static_cast<int32_t>(offsetof(U3CbuildMenuU3Ec__AnonStorey1_t23940567, ___root_0)); }
	inline Branch_t1428165248 * get_root_0() const { return ___root_0; }
	inline Branch_t1428165248 ** get_address_of_root_0() { return &___root_0; }
	inline void set_root_0(Branch_t1428165248 * value)
	{
		___root_0 = value;
		Il2CppCodeGenWriteBarrier(&___root_0, value);
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CbuildMenuU3Ec__AnonStorey1_t23940567, ___U24this_1)); }
	inline MenuController_t3930949237 * get_U24this_1() const { return ___U24this_1; }
	inline MenuController_t3930949237 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(MenuController_t3930949237 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier(&___U24this_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
