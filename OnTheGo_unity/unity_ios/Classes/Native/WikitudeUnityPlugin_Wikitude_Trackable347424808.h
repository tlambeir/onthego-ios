﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour3962482529.h"

// System.String
struct String_t;
// System.Text.RegularExpressions.Regex
struct Regex_t3657309853;
// UnityEngine.GameObject
struct GameObject_t1113636619;
// Wikitude.TrackerBehaviour
struct TrackerBehaviour_t921360922;
// System.Collections.Generic.Dictionary`2<Wikitude.RecognizedTarget,UnityEngine.GameObject>
struct Dictionary_2_t2728509719;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Wikitude.Trackable
struct  Trackable_t347424808  : public MonoBehaviour_t3962482529
{
public:
	// System.String Wikitude.Trackable::_targetPattern
	String_t* ____targetPattern_2;
	// System.Text.RegularExpressions.Regex Wikitude.Trackable::_targetPatternRegex
	Regex_t3657309853 * ____targetPatternRegex_3;
	// UnityEngine.GameObject Wikitude.Trackable::_drawable
	GameObject_t1113636619 * ____drawable_4;
	// System.Boolean Wikitude.Trackable::_autoToggleVisibility
	bool ____autoToggleVisibility_5;
	// System.Boolean Wikitude.Trackable::_registeredToTracker
	bool ____registeredToTracker_6;
	// Wikitude.TrackerBehaviour Wikitude.Trackable::_tracker
	TrackerBehaviour_t921360922 * ____tracker_7;
	// System.Collections.Generic.Dictionary`2<Wikitude.RecognizedTarget,UnityEngine.GameObject> Wikitude.Trackable::_activeDrawables
	Dictionary_2_t2728509719 * ____activeDrawables_8;

public:
	inline static int32_t get_offset_of__targetPattern_2() { return static_cast<int32_t>(offsetof(Trackable_t347424808, ____targetPattern_2)); }
	inline String_t* get__targetPattern_2() const { return ____targetPattern_2; }
	inline String_t** get_address_of__targetPattern_2() { return &____targetPattern_2; }
	inline void set__targetPattern_2(String_t* value)
	{
		____targetPattern_2 = value;
		Il2CppCodeGenWriteBarrier(&____targetPattern_2, value);
	}

	inline static int32_t get_offset_of__targetPatternRegex_3() { return static_cast<int32_t>(offsetof(Trackable_t347424808, ____targetPatternRegex_3)); }
	inline Regex_t3657309853 * get__targetPatternRegex_3() const { return ____targetPatternRegex_3; }
	inline Regex_t3657309853 ** get_address_of__targetPatternRegex_3() { return &____targetPatternRegex_3; }
	inline void set__targetPatternRegex_3(Regex_t3657309853 * value)
	{
		____targetPatternRegex_3 = value;
		Il2CppCodeGenWriteBarrier(&____targetPatternRegex_3, value);
	}

	inline static int32_t get_offset_of__drawable_4() { return static_cast<int32_t>(offsetof(Trackable_t347424808, ____drawable_4)); }
	inline GameObject_t1113636619 * get__drawable_4() const { return ____drawable_4; }
	inline GameObject_t1113636619 ** get_address_of__drawable_4() { return &____drawable_4; }
	inline void set__drawable_4(GameObject_t1113636619 * value)
	{
		____drawable_4 = value;
		Il2CppCodeGenWriteBarrier(&____drawable_4, value);
	}

	inline static int32_t get_offset_of__autoToggleVisibility_5() { return static_cast<int32_t>(offsetof(Trackable_t347424808, ____autoToggleVisibility_5)); }
	inline bool get__autoToggleVisibility_5() const { return ____autoToggleVisibility_5; }
	inline bool* get_address_of__autoToggleVisibility_5() { return &____autoToggleVisibility_5; }
	inline void set__autoToggleVisibility_5(bool value)
	{
		____autoToggleVisibility_5 = value;
	}

	inline static int32_t get_offset_of__registeredToTracker_6() { return static_cast<int32_t>(offsetof(Trackable_t347424808, ____registeredToTracker_6)); }
	inline bool get__registeredToTracker_6() const { return ____registeredToTracker_6; }
	inline bool* get_address_of__registeredToTracker_6() { return &____registeredToTracker_6; }
	inline void set__registeredToTracker_6(bool value)
	{
		____registeredToTracker_6 = value;
	}

	inline static int32_t get_offset_of__tracker_7() { return static_cast<int32_t>(offsetof(Trackable_t347424808, ____tracker_7)); }
	inline TrackerBehaviour_t921360922 * get__tracker_7() const { return ____tracker_7; }
	inline TrackerBehaviour_t921360922 ** get_address_of__tracker_7() { return &____tracker_7; }
	inline void set__tracker_7(TrackerBehaviour_t921360922 * value)
	{
		____tracker_7 = value;
		Il2CppCodeGenWriteBarrier(&____tracker_7, value);
	}

	inline static int32_t get_offset_of__activeDrawables_8() { return static_cast<int32_t>(offsetof(Trackable_t347424808, ____activeDrawables_8)); }
	inline Dictionary_2_t2728509719 * get__activeDrawables_8() const { return ____activeDrawables_8; }
	inline Dictionary_2_t2728509719 ** get_address_of__activeDrawables_8() { return &____activeDrawables_8; }
	inline void set__activeDrawables_8(Dictionary_2_t2728509719 * value)
	{
		____activeDrawables_8 = value;
		Il2CppCodeGenWriteBarrier(&____activeDrawables_8, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
