﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// UnityEngine.Material
struct Material_t340375123;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_MaterialManager/<>c__DisplayClass12_0
struct  U3CU3Ec__DisplayClass12_0_t173182957  : public Il2CppObject
{
public:
	// UnityEngine.Material TMPro.TMP_MaterialManager/<>c__DisplayClass12_0::stencilMaterial
	Material_t340375123 * ___stencilMaterial_0;

public:
	inline static int32_t get_offset_of_stencilMaterial_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass12_0_t173182957, ___stencilMaterial_0)); }
	inline Material_t340375123 * get_stencilMaterial_0() const { return ___stencilMaterial_0; }
	inline Material_t340375123 ** get_address_of_stencilMaterial_0() { return &___stencilMaterial_0; }
	inline void set_stencilMaterial_0(Material_t340375123 * value)
	{
		___stencilMaterial_0 = value;
		Il2CppCodeGenWriteBarrier(&___stencilMaterial_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
