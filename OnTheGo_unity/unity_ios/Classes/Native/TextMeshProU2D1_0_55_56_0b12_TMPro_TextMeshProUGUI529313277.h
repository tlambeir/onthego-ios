﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "TextMeshProU2D1_0_55_56_0b12_TMPro_TMP_Text2599618874.h"
#include "UnityEngine_UnityEngine_Vector43319028937.h"
#include "UnityEngine_UnityEngine_Matrix4x41817901843.h"

// TMPro.TMP_SubMeshUI[]
struct TMP_SubMeshUIU5BU5D_t3119380694;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t1718750761;
// UnityEngine.CanvasRenderer
struct CanvasRenderer_t2598313366;
// UnityEngine.Canvas
struct Canvas_t3310196443;
// UnityEngine.Material
struct Material_t340375123;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TextMeshProUGUI
struct  TextMeshProUGUI_t529313277  : public TMP_Text_t2599618874
{
public:
	// System.Boolean TMPro.TextMeshProUGUI::m_hasFontAssetChanged
	bool ___m_hasFontAssetChanged_230;
	// TMPro.TMP_SubMeshUI[] TMPro.TextMeshProUGUI::m_subTextObjects
	TMP_SubMeshUIU5BU5D_t3119380694* ___m_subTextObjects_231;
	// System.Single TMPro.TextMeshProUGUI::m_previousLossyScaleY
	float ___m_previousLossyScaleY_232;
	// UnityEngine.Vector3[] TMPro.TextMeshProUGUI::m_RectTransformCorners
	Vector3U5BU5D_t1718750761* ___m_RectTransformCorners_233;
	// UnityEngine.CanvasRenderer TMPro.TextMeshProUGUI::m_canvasRenderer
	CanvasRenderer_t2598313366 * ___m_canvasRenderer_234;
	// UnityEngine.Canvas TMPro.TextMeshProUGUI::m_canvas
	Canvas_t3310196443 * ___m_canvas_235;
	// System.Boolean TMPro.TextMeshProUGUI::m_isFirstAllocation
	bool ___m_isFirstAllocation_236;
	// System.Int32 TMPro.TextMeshProUGUI::m_max_characters
	int32_t ___m_max_characters_237;
	// System.Boolean TMPro.TextMeshProUGUI::m_isMaskingEnabled
	bool ___m_isMaskingEnabled_238;
	// UnityEngine.Material TMPro.TextMeshProUGUI::m_baseMaterial
	Material_t340375123 * ___m_baseMaterial_239;
	// System.Boolean TMPro.TextMeshProUGUI::m_isScrollRegionSet
	bool ___m_isScrollRegionSet_240;
	// System.Int32 TMPro.TextMeshProUGUI::m_stencilID
	int32_t ___m_stencilID_241;
	// UnityEngine.Vector4 TMPro.TextMeshProUGUI::m_maskOffset
	Vector4_t3319028937  ___m_maskOffset_242;
	// UnityEngine.Matrix4x4 TMPro.TextMeshProUGUI::m_EnvMapMatrix
	Matrix4x4_t1817901843  ___m_EnvMapMatrix_243;
	// System.Boolean TMPro.TextMeshProUGUI::m_isRegisteredForEvents
	bool ___m_isRegisteredForEvents_244;
	// System.Int32 TMPro.TextMeshProUGUI::m_recursiveCountA
	int32_t ___m_recursiveCountA_245;
	// System.Int32 TMPro.TextMeshProUGUI::loopCountA
	int32_t ___loopCountA_246;
	// System.Boolean TMPro.TextMeshProUGUI::m_isRebuildingLayout
	bool ___m_isRebuildingLayout_247;

public:
	inline static int32_t get_offset_of_m_hasFontAssetChanged_230() { return static_cast<int32_t>(offsetof(TextMeshProUGUI_t529313277, ___m_hasFontAssetChanged_230)); }
	inline bool get_m_hasFontAssetChanged_230() const { return ___m_hasFontAssetChanged_230; }
	inline bool* get_address_of_m_hasFontAssetChanged_230() { return &___m_hasFontAssetChanged_230; }
	inline void set_m_hasFontAssetChanged_230(bool value)
	{
		___m_hasFontAssetChanged_230 = value;
	}

	inline static int32_t get_offset_of_m_subTextObjects_231() { return static_cast<int32_t>(offsetof(TextMeshProUGUI_t529313277, ___m_subTextObjects_231)); }
	inline TMP_SubMeshUIU5BU5D_t3119380694* get_m_subTextObjects_231() const { return ___m_subTextObjects_231; }
	inline TMP_SubMeshUIU5BU5D_t3119380694** get_address_of_m_subTextObjects_231() { return &___m_subTextObjects_231; }
	inline void set_m_subTextObjects_231(TMP_SubMeshUIU5BU5D_t3119380694* value)
	{
		___m_subTextObjects_231 = value;
		Il2CppCodeGenWriteBarrier(&___m_subTextObjects_231, value);
	}

	inline static int32_t get_offset_of_m_previousLossyScaleY_232() { return static_cast<int32_t>(offsetof(TextMeshProUGUI_t529313277, ___m_previousLossyScaleY_232)); }
	inline float get_m_previousLossyScaleY_232() const { return ___m_previousLossyScaleY_232; }
	inline float* get_address_of_m_previousLossyScaleY_232() { return &___m_previousLossyScaleY_232; }
	inline void set_m_previousLossyScaleY_232(float value)
	{
		___m_previousLossyScaleY_232 = value;
	}

	inline static int32_t get_offset_of_m_RectTransformCorners_233() { return static_cast<int32_t>(offsetof(TextMeshProUGUI_t529313277, ___m_RectTransformCorners_233)); }
	inline Vector3U5BU5D_t1718750761* get_m_RectTransformCorners_233() const { return ___m_RectTransformCorners_233; }
	inline Vector3U5BU5D_t1718750761** get_address_of_m_RectTransformCorners_233() { return &___m_RectTransformCorners_233; }
	inline void set_m_RectTransformCorners_233(Vector3U5BU5D_t1718750761* value)
	{
		___m_RectTransformCorners_233 = value;
		Il2CppCodeGenWriteBarrier(&___m_RectTransformCorners_233, value);
	}

	inline static int32_t get_offset_of_m_canvasRenderer_234() { return static_cast<int32_t>(offsetof(TextMeshProUGUI_t529313277, ___m_canvasRenderer_234)); }
	inline CanvasRenderer_t2598313366 * get_m_canvasRenderer_234() const { return ___m_canvasRenderer_234; }
	inline CanvasRenderer_t2598313366 ** get_address_of_m_canvasRenderer_234() { return &___m_canvasRenderer_234; }
	inline void set_m_canvasRenderer_234(CanvasRenderer_t2598313366 * value)
	{
		___m_canvasRenderer_234 = value;
		Il2CppCodeGenWriteBarrier(&___m_canvasRenderer_234, value);
	}

	inline static int32_t get_offset_of_m_canvas_235() { return static_cast<int32_t>(offsetof(TextMeshProUGUI_t529313277, ___m_canvas_235)); }
	inline Canvas_t3310196443 * get_m_canvas_235() const { return ___m_canvas_235; }
	inline Canvas_t3310196443 ** get_address_of_m_canvas_235() { return &___m_canvas_235; }
	inline void set_m_canvas_235(Canvas_t3310196443 * value)
	{
		___m_canvas_235 = value;
		Il2CppCodeGenWriteBarrier(&___m_canvas_235, value);
	}

	inline static int32_t get_offset_of_m_isFirstAllocation_236() { return static_cast<int32_t>(offsetof(TextMeshProUGUI_t529313277, ___m_isFirstAllocation_236)); }
	inline bool get_m_isFirstAllocation_236() const { return ___m_isFirstAllocation_236; }
	inline bool* get_address_of_m_isFirstAllocation_236() { return &___m_isFirstAllocation_236; }
	inline void set_m_isFirstAllocation_236(bool value)
	{
		___m_isFirstAllocation_236 = value;
	}

	inline static int32_t get_offset_of_m_max_characters_237() { return static_cast<int32_t>(offsetof(TextMeshProUGUI_t529313277, ___m_max_characters_237)); }
	inline int32_t get_m_max_characters_237() const { return ___m_max_characters_237; }
	inline int32_t* get_address_of_m_max_characters_237() { return &___m_max_characters_237; }
	inline void set_m_max_characters_237(int32_t value)
	{
		___m_max_characters_237 = value;
	}

	inline static int32_t get_offset_of_m_isMaskingEnabled_238() { return static_cast<int32_t>(offsetof(TextMeshProUGUI_t529313277, ___m_isMaskingEnabled_238)); }
	inline bool get_m_isMaskingEnabled_238() const { return ___m_isMaskingEnabled_238; }
	inline bool* get_address_of_m_isMaskingEnabled_238() { return &___m_isMaskingEnabled_238; }
	inline void set_m_isMaskingEnabled_238(bool value)
	{
		___m_isMaskingEnabled_238 = value;
	}

	inline static int32_t get_offset_of_m_baseMaterial_239() { return static_cast<int32_t>(offsetof(TextMeshProUGUI_t529313277, ___m_baseMaterial_239)); }
	inline Material_t340375123 * get_m_baseMaterial_239() const { return ___m_baseMaterial_239; }
	inline Material_t340375123 ** get_address_of_m_baseMaterial_239() { return &___m_baseMaterial_239; }
	inline void set_m_baseMaterial_239(Material_t340375123 * value)
	{
		___m_baseMaterial_239 = value;
		Il2CppCodeGenWriteBarrier(&___m_baseMaterial_239, value);
	}

	inline static int32_t get_offset_of_m_isScrollRegionSet_240() { return static_cast<int32_t>(offsetof(TextMeshProUGUI_t529313277, ___m_isScrollRegionSet_240)); }
	inline bool get_m_isScrollRegionSet_240() const { return ___m_isScrollRegionSet_240; }
	inline bool* get_address_of_m_isScrollRegionSet_240() { return &___m_isScrollRegionSet_240; }
	inline void set_m_isScrollRegionSet_240(bool value)
	{
		___m_isScrollRegionSet_240 = value;
	}

	inline static int32_t get_offset_of_m_stencilID_241() { return static_cast<int32_t>(offsetof(TextMeshProUGUI_t529313277, ___m_stencilID_241)); }
	inline int32_t get_m_stencilID_241() const { return ___m_stencilID_241; }
	inline int32_t* get_address_of_m_stencilID_241() { return &___m_stencilID_241; }
	inline void set_m_stencilID_241(int32_t value)
	{
		___m_stencilID_241 = value;
	}

	inline static int32_t get_offset_of_m_maskOffset_242() { return static_cast<int32_t>(offsetof(TextMeshProUGUI_t529313277, ___m_maskOffset_242)); }
	inline Vector4_t3319028937  get_m_maskOffset_242() const { return ___m_maskOffset_242; }
	inline Vector4_t3319028937 * get_address_of_m_maskOffset_242() { return &___m_maskOffset_242; }
	inline void set_m_maskOffset_242(Vector4_t3319028937  value)
	{
		___m_maskOffset_242 = value;
	}

	inline static int32_t get_offset_of_m_EnvMapMatrix_243() { return static_cast<int32_t>(offsetof(TextMeshProUGUI_t529313277, ___m_EnvMapMatrix_243)); }
	inline Matrix4x4_t1817901843  get_m_EnvMapMatrix_243() const { return ___m_EnvMapMatrix_243; }
	inline Matrix4x4_t1817901843 * get_address_of_m_EnvMapMatrix_243() { return &___m_EnvMapMatrix_243; }
	inline void set_m_EnvMapMatrix_243(Matrix4x4_t1817901843  value)
	{
		___m_EnvMapMatrix_243 = value;
	}

	inline static int32_t get_offset_of_m_isRegisteredForEvents_244() { return static_cast<int32_t>(offsetof(TextMeshProUGUI_t529313277, ___m_isRegisteredForEvents_244)); }
	inline bool get_m_isRegisteredForEvents_244() const { return ___m_isRegisteredForEvents_244; }
	inline bool* get_address_of_m_isRegisteredForEvents_244() { return &___m_isRegisteredForEvents_244; }
	inline void set_m_isRegisteredForEvents_244(bool value)
	{
		___m_isRegisteredForEvents_244 = value;
	}

	inline static int32_t get_offset_of_m_recursiveCountA_245() { return static_cast<int32_t>(offsetof(TextMeshProUGUI_t529313277, ___m_recursiveCountA_245)); }
	inline int32_t get_m_recursiveCountA_245() const { return ___m_recursiveCountA_245; }
	inline int32_t* get_address_of_m_recursiveCountA_245() { return &___m_recursiveCountA_245; }
	inline void set_m_recursiveCountA_245(int32_t value)
	{
		___m_recursiveCountA_245 = value;
	}

	inline static int32_t get_offset_of_loopCountA_246() { return static_cast<int32_t>(offsetof(TextMeshProUGUI_t529313277, ___loopCountA_246)); }
	inline int32_t get_loopCountA_246() const { return ___loopCountA_246; }
	inline int32_t* get_address_of_loopCountA_246() { return &___loopCountA_246; }
	inline void set_loopCountA_246(int32_t value)
	{
		___loopCountA_246 = value;
	}

	inline static int32_t get_offset_of_m_isRebuildingLayout_247() { return static_cast<int32_t>(offsetof(TextMeshProUGUI_t529313277, ___m_isRebuildingLayout_247)); }
	inline bool get_m_isRebuildingLayout_247() const { return ___m_isRebuildingLayout_247; }
	inline bool* get_address_of_m_isRebuildingLayout_247() { return &___m_isRebuildingLayout_247; }
	inline void set_m_isRebuildingLayout_247(bool value)
	{
		___m_isRebuildingLayout_247 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
