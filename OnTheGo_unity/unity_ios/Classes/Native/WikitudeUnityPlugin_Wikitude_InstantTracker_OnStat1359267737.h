﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_Events_UnityEvent_1_gen560102991.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Wikitude.InstantTracker/OnStateChangedEvent
struct  OnStateChangedEvent_t1359267737  : public UnityEvent_1_t560102991
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
