﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour3962482529.h"

// HighlightingSystem.HighlightingRenderer
struct HighlightingRenderer_t1923179915;
// System.Collections.Generic.List`1<HighlightingSystem.HighlightingPreset>
struct List_1_t2107694533;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HighlightingPresetLoop
struct  HighlightingPresetLoop_t1609534497  : public MonoBehaviour_t3962482529
{
public:
	// HighlightingSystem.HighlightingRenderer HighlightingPresetLoop::highlightingRenderer
	HighlightingRenderer_t1923179915 * ___highlightingRenderer_2;
	// System.Single HighlightingPresetLoop::interval
	float ___interval_3;
	// System.Collections.Generic.List`1<HighlightingSystem.HighlightingPreset> HighlightingPresetLoop::customPresets
	List_1_t2107694533 * ___customPresets_4;

public:
	inline static int32_t get_offset_of_highlightingRenderer_2() { return static_cast<int32_t>(offsetof(HighlightingPresetLoop_t1609534497, ___highlightingRenderer_2)); }
	inline HighlightingRenderer_t1923179915 * get_highlightingRenderer_2() const { return ___highlightingRenderer_2; }
	inline HighlightingRenderer_t1923179915 ** get_address_of_highlightingRenderer_2() { return &___highlightingRenderer_2; }
	inline void set_highlightingRenderer_2(HighlightingRenderer_t1923179915 * value)
	{
		___highlightingRenderer_2 = value;
		Il2CppCodeGenWriteBarrier(&___highlightingRenderer_2, value);
	}

	inline static int32_t get_offset_of_interval_3() { return static_cast<int32_t>(offsetof(HighlightingPresetLoop_t1609534497, ___interval_3)); }
	inline float get_interval_3() const { return ___interval_3; }
	inline float* get_address_of_interval_3() { return &___interval_3; }
	inline void set_interval_3(float value)
	{
		___interval_3 = value;
	}

	inline static int32_t get_offset_of_customPresets_4() { return static_cast<int32_t>(offsetof(HighlightingPresetLoop_t1609534497, ___customPresets_4)); }
	inline List_1_t2107694533 * get_customPresets_4() const { return ___customPresets_4; }
	inline List_1_t2107694533 ** get_address_of_customPresets_4() { return &___customPresets_4; }
	inline void set_customPresets_4(List_1_t2107694533 * value)
	{
		___customPresets_4 = value;
		Il2CppCodeGenWriteBarrier(&___customPresets_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
