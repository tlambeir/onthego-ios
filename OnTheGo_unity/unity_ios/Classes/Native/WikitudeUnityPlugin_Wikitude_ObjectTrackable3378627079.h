﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "WikitudeUnityPlugin_Wikitude_Trackable347424808.h"

// Wikitude.ObjectTrackable/OnObjectRecognizedEvent
struct OnObjectRecognizedEvent_t3443511796;
// Wikitude.ObjectTrackable/OnObjectLostEvent
struct OnObjectLostEvent_t460465675;
// Wikitude.MapPointCloud
struct MapPointCloud_t955983077;
// WikitudeEditor.PointCloudRenderer
struct PointCloudRenderer_t3664016937;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Wikitude.ObjectTrackable
struct  ObjectTrackable_t3378627079  : public Trackable_t347424808
{
public:
	// Wikitude.ObjectTrackable/OnObjectRecognizedEvent Wikitude.ObjectTrackable::OnObjectRecognized
	OnObjectRecognizedEvent_t3443511796 * ___OnObjectRecognized_9;
	// Wikitude.ObjectTrackable/OnObjectLostEvent Wikitude.ObjectTrackable::OnObjectLost
	OnObjectLostEvent_t460465675 * ___OnObjectLost_10;
	// Wikitude.MapPointCloud Wikitude.ObjectTrackable::_pointCloud
	MapPointCloud_t955983077 * ____pointCloud_11;
	// WikitudeEditor.PointCloudRenderer Wikitude.ObjectTrackable::_mapRenderer
	PointCloudRenderer_t3664016937 * ____mapRenderer_12;

public:
	inline static int32_t get_offset_of_OnObjectRecognized_9() { return static_cast<int32_t>(offsetof(ObjectTrackable_t3378627079, ___OnObjectRecognized_9)); }
	inline OnObjectRecognizedEvent_t3443511796 * get_OnObjectRecognized_9() const { return ___OnObjectRecognized_9; }
	inline OnObjectRecognizedEvent_t3443511796 ** get_address_of_OnObjectRecognized_9() { return &___OnObjectRecognized_9; }
	inline void set_OnObjectRecognized_9(OnObjectRecognizedEvent_t3443511796 * value)
	{
		___OnObjectRecognized_9 = value;
		Il2CppCodeGenWriteBarrier(&___OnObjectRecognized_9, value);
	}

	inline static int32_t get_offset_of_OnObjectLost_10() { return static_cast<int32_t>(offsetof(ObjectTrackable_t3378627079, ___OnObjectLost_10)); }
	inline OnObjectLostEvent_t460465675 * get_OnObjectLost_10() const { return ___OnObjectLost_10; }
	inline OnObjectLostEvent_t460465675 ** get_address_of_OnObjectLost_10() { return &___OnObjectLost_10; }
	inline void set_OnObjectLost_10(OnObjectLostEvent_t460465675 * value)
	{
		___OnObjectLost_10 = value;
		Il2CppCodeGenWriteBarrier(&___OnObjectLost_10, value);
	}

	inline static int32_t get_offset_of__pointCloud_11() { return static_cast<int32_t>(offsetof(ObjectTrackable_t3378627079, ____pointCloud_11)); }
	inline MapPointCloud_t955983077 * get__pointCloud_11() const { return ____pointCloud_11; }
	inline MapPointCloud_t955983077 ** get_address_of__pointCloud_11() { return &____pointCloud_11; }
	inline void set__pointCloud_11(MapPointCloud_t955983077 * value)
	{
		____pointCloud_11 = value;
		Il2CppCodeGenWriteBarrier(&____pointCloud_11, value);
	}

	inline static int32_t get_offset_of__mapRenderer_12() { return static_cast<int32_t>(offsetof(ObjectTrackable_t3378627079, ____mapRenderer_12)); }
	inline PointCloudRenderer_t3664016937 * get__mapRenderer_12() const { return ____mapRenderer_12; }
	inline PointCloudRenderer_t3664016937 ** get_address_of__mapRenderer_12() { return &____mapRenderer_12; }
	inline void set__mapRenderer_12(PointCloudRenderer_t3664016937 * value)
	{
		____mapRenderer_12 = value;
		Il2CppCodeGenWriteBarrier(&____mapRenderer_12, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
