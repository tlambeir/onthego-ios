﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour3962482529.h"

// UnityEngine.UI.Text
struct Text_t1901882714;
// TMPro.TextMeshProUGUI
struct TextMeshProUGUI_t529313277;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// descriptionmanager
struct  descriptionmanager_t4101791333  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.UI.Text descriptionmanager::targetTitle
	Text_t1901882714 * ___targetTitle_2;
	// TMPro.TextMeshProUGUI descriptionmanager::targetDescription
	TextMeshProUGUI_t529313277 * ___targetDescription_3;
	// UnityEngine.UI.Text descriptionmanager::sourceTitle
	Text_t1901882714 * ___sourceTitle_4;
	// TMPro.TextMeshProUGUI descriptionmanager::sourceDescription
	TextMeshProUGUI_t529313277 * ___sourceDescription_5;

public:
	inline static int32_t get_offset_of_targetTitle_2() { return static_cast<int32_t>(offsetof(descriptionmanager_t4101791333, ___targetTitle_2)); }
	inline Text_t1901882714 * get_targetTitle_2() const { return ___targetTitle_2; }
	inline Text_t1901882714 ** get_address_of_targetTitle_2() { return &___targetTitle_2; }
	inline void set_targetTitle_2(Text_t1901882714 * value)
	{
		___targetTitle_2 = value;
		Il2CppCodeGenWriteBarrier(&___targetTitle_2, value);
	}

	inline static int32_t get_offset_of_targetDescription_3() { return static_cast<int32_t>(offsetof(descriptionmanager_t4101791333, ___targetDescription_3)); }
	inline TextMeshProUGUI_t529313277 * get_targetDescription_3() const { return ___targetDescription_3; }
	inline TextMeshProUGUI_t529313277 ** get_address_of_targetDescription_3() { return &___targetDescription_3; }
	inline void set_targetDescription_3(TextMeshProUGUI_t529313277 * value)
	{
		___targetDescription_3 = value;
		Il2CppCodeGenWriteBarrier(&___targetDescription_3, value);
	}

	inline static int32_t get_offset_of_sourceTitle_4() { return static_cast<int32_t>(offsetof(descriptionmanager_t4101791333, ___sourceTitle_4)); }
	inline Text_t1901882714 * get_sourceTitle_4() const { return ___sourceTitle_4; }
	inline Text_t1901882714 ** get_address_of_sourceTitle_4() { return &___sourceTitle_4; }
	inline void set_sourceTitle_4(Text_t1901882714 * value)
	{
		___sourceTitle_4 = value;
		Il2CppCodeGenWriteBarrier(&___sourceTitle_4, value);
	}

	inline static int32_t get_offset_of_sourceDescription_5() { return static_cast<int32_t>(offsetof(descriptionmanager_t4101791333, ___sourceDescription_5)); }
	inline TextMeshProUGUI_t529313277 * get_sourceDescription_5() const { return ___sourceDescription_5; }
	inline TextMeshProUGUI_t529313277 ** get_address_of_sourceDescription_5() { return &___sourceDescription_5; }
	inline void set_sourceDescription_5(TextMeshProUGUI_t529313277 * value)
	{
		___sourceDescription_5 = value;
		Il2CppCodeGenWriteBarrier(&___sourceDescription_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
