﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "TextMeshProU2D1_0_55_56_0b12_TMPro_TextRenderFlags2418684345.h"
#include "TextMeshProU2D1_0_55_56_0b12_TMPro_TMP_TextElement1276645592.h"
#include "TextMeshProU2D1_0_55_56_0b12_TMPro_MaskingTypes3687969768.h"
#include "TextMeshProU2D1_0_55_56_0b12_TMPro_TextOverflowMod1430035314.h"
#include "TextMeshProU2D1_0_55_56_0b12_TMPro_MaskingOffsetMo2266644590.h"
#include "TextMeshProU2D1_0_55_56_0b12_TMPro_TextureMappingOp270963663.h"
#include "TextMeshProU2D1_0_55_56_0b12_TMPro_FontStyles3828945032.h"
#include "TextMeshProU2D1_0_55_56_0b12_TMPro_FontWeights3122883458.h"
#include "TextMeshProU2D1_0_55_56_0b12_TMPro_TagUnits1169424683.h"
#include "TextMeshProU2D1_0_55_56_0b12_TMPro_TagType123236451.h"
#include "TextMeshProU2D1_0_55_56_0b12_TMPro_TMP_Text2599618874.h"
#include "TextMeshProU2D1_0_55_56_0b12_TMPro_TMP_Text_TextIn1522115805.h"
#include "TextMeshProU2D1_0_55_56_0b12_TMPro_TMP_TextElement129727469.h"
#include "TextMeshProU2D1_0_55_56_0b12_TMPro_TMP_TextInfo3598145122.h"
#include "TextMeshProU2D1_0_55_56_0b12_TMPro_CaretPosition3997512201.h"
#include "TextMeshProU2D1_0_55_56_0b12_TMPro_CaretInfo841780893.h"
#include "TextMeshProU2D1_0_55_56_0b12_TMPro_TMP_TextUtiliti2105690005.h"
#include "TextMeshProU2D1_0_55_56_0b12_TMPro_TMP_TextUtiliti1526544958.h"
#include "TextMeshProU2D1_0_55_56_0b12_TMPro_TMP_UpdateManag4114267509.h"
#include "TextMeshProU2D1_0_55_56_0b12_TMPro_TMP_UpdateRegist461608481.h"
#include "TextMeshProU2D1_0_55_56_0b12_TMPro_TMP_BasicXmlTag2962628096.h"
#include "TextMeshProU2D1_0_55_56_0b12_TMPro_Compute_Distanc2554612104.h"
#include "TextMeshProU2D1_0_55_56_0b12_TMPro_TMPro_EventManag712497257.h"
#include "TextMeshProU2D1_0_55_56_0b12_TMPro_Compute_DT_Even1071353166.h"
#include "TextMeshProU2D1_0_55_56_0b12_TMPro_TMPro_Extension1453208966.h"
#include "TextMeshProU2D1_0_55_56_0b12_TMPro_TMP_Math624304809.h"
#include "TextMeshProU2D1_0_55_56_0b12_TMPro_FaceInfo2243299176.h"
#include "TextMeshProU2D1_0_55_56_0b12_TMPro_TMP_Glyph581847833.h"
#include "TextMeshProU2D1_0_55_56_0b12_TMPro_FontCreationSett628772060.h"
#include "TextMeshProU2D1_0_55_56_0b12_TMPro_Glyph2D1260586688.h"
#include "TextMeshProU2D1_0_55_56_0b12_TMPro_KerningPairKey536493877.h"
#include "TextMeshProU2D1_0_55_56_0b12_TMPro_KerningPair2270855589.h"
#include "TextMeshProU2D1_0_55_56_0b12_TMPro_KerningTable2322366871.h"
#include "TextMeshProU2D1_0_55_56_0b12_TMPro_KerningTable_U32918531336.h"
#include "TextMeshProU2D1_0_55_56_0b12_TMPro_KerningTable_U32918531329.h"
#include "TextMeshProU2D1_0_55_56_0b12_TMPro_KerningTable_U34077839018.h"
#include "TextMeshProU2D1_0_55_56_0b12_TMPro_TMP_FontUtiliti2599150238.h"
#include "TextMeshProU2D1_0_55_56_0b12_TMPro_TMP_VertexDataUp388000256.h"
#include "TextMeshProU2D1_0_55_56_0b12_TMPro_TMP_CharacterIn3185626797.h"
#include "TextMeshProU2D1_0_55_56_0b12_TMPro_TMP_Vertex2404176824.h"
#include "TextMeshProU2D1_0_55_56_0b12_TMPro_VertexGradient345148380.h"
#include "TextMeshProU2D1_0_55_56_0b12_TMPro_TMP_PageInfo2608430633.h"
#include "TextMeshProU2D1_0_55_56_0b12_TMPro_TMP_LinkInfo1092083476.h"
#include "TextMeshProU2D1_0_55_56_0b12_TMPro_TMP_WordInfo3331066303.h"
#include "TextMeshProU2D1_0_55_56_0b12_TMPro_TMP_SpriteInfo2726321384.h"
#include "TextMeshProU2D1_0_55_56_0b12_TMPro_Extents3837212874.h"
#include "TextMeshProU2D1_0_55_56_0b12_TMPro_Mesh_Extents3388355125.h"
#include "TextMeshProU2D1_0_55_56_0b12_TMPro_WordWrapState341939652.h"
#include "TextMeshProU2D1_0_55_56_0b12_TMPro_TagAttribute688278634.h"
#include "TextMeshProU2D1_0_55_56_0b12_TMPro_XML_TagAttribut1174424309.h"
#include "TextMeshProU2D1_0_55_56_0b12_TMPro_TextMeshPro2393593166.h"
#include "TextMeshProU2D1_0_55_56_0b12_TMPro_ShaderUtilities714255158.h"
#include "TextMeshProU2D1_0_55_56_0b12_TMPro_TextMeshProUGUI529313277.h"
#include "TextMeshProU2D1_0_55_56_0b12_TMPro_TextContainerAnc945851193.h"
#include "TextMeshProU2D1_0_55_56_0b12_TMPro_TextContainer97923372.h"
#include "TextMeshProU2D1_0_55_56_0b12_TMPro_SpriteAssetUtili116390639.h"
#include "TextMeshProU2D1_0_55_56_0b12_TMPro_SpriteAssetUtil3148178657.h"
#include "TextMeshProU2D1_0_55_56_0b12_TMPro_SpriteAssetUtil3912389194.h"
#include "TextMeshProU2D1_0_55_56_0b12_TMPro_SpriteAssetUtil3355290999.h"
#include "TextMeshProU2D1_0_55_56_0b12_TMPro_SpriteAssetUtil3048397587.h"
#include "TextMeshProU2D1_0_55_56_0b12_TMPro_SpriteAssetUtili308163541.h"
#include "TextMeshProU2D1_0_55_56_0b12_U3CPrivateImplementat3057255361.h"
#include "TextMeshProU2D1_0_55_56_0b12_U3CPrivateImplementat2710994317.h"
#include "TextMeshProU2D1_0_55_56_0b12_U3CPrivateImplementat1547998295.h"
#include "WikitudeUnityPlugin_U3CModuleU3E692745525.h"
#include "WikitudeUnityPlugin_Wikitude_AndroidBridge2441337961.h"
#include "WikitudeUnityPlugin_Wikitude_iOSBridge3849146092.h"
#include "WikitudeUnityPlugin_Wikitude_UnityBridge2504510425.h"
#include "WikitudeUnityPlugin_Wikitude_WikitudeBridge4110292378.h"
#include "WikitudeUnityPlugin_Wikitude_UnityEditorSimulator2033215626.h"
#include "WikitudeUnityPlugin_Wikitude_UnityEditorSimulator_1564170948.h"
#include "WikitudeUnityPlugin_Wikitude_ImageTrackable2462741734.h"
#include "WikitudeUnityPlugin_Wikitude_ImageTrackable_OnImage773132985.h"
#include "WikitudeUnityPlugin_Wikitude_ImageTrackable_OnImage966623312.h"
#include "WikitudeUnityPlugin_Wikitude_ExtendedTrackingQualit364496425.h"
#include "WikitudeUnityPlugin_Wikitude_ImageRecognitionRangeE604843042.h"
#include "WikitudeUnityPlugin_Wikitude_ImageTracker271395264.h"
#include "WikitudeUnityPlugin_Wikitude_ImageTracker_OnExtend2560195974.h"
#include "WikitudeUnityPlugin_Wikitude_InstantTrackable3187174337.h"
#include "WikitudeUnityPlugin_Wikitude_InstantTrackable_OnIni272217273.h"
#include "WikitudeUnityPlugin_Wikitude_InstantTrackable_OnIn4207911327.h"
#include "WikitudeUnityPlugin_Wikitude_InstantTrackable_OnSc2416994635.h"
#include "WikitudeUnityPlugin_Wikitude_InstantTrackable_OnSce389101256.h"
#include "WikitudeUnityPlugin_Wikitude_InstantTrackingPlaneO1439566444.h"
#include "WikitudeUnityPlugin_Wikitude_InstantTrackingState3973410783.h"
#include "WikitudeUnityPlugin_Wikitude_InstantTracker684352120.h"
#include "WikitudeUnityPlugin_Wikitude_InstantTracker_OnStat1359267737.h"
#include "WikitudeUnityPlugin_Wikitude_InstantTracker_OnScree608500589.h"
#include "WikitudeUnityPlugin_Wikitude_InstantTracker_OnPoin2541379958.h"
#include "WikitudeUnityPlugin_Wikitude_ObjectTrackable3378627079.h"
#include "WikitudeUnityPlugin_Wikitude_ObjectTrackable_OnObj3443511796.h"
#include "WikitudeUnityPlugin_Wikitude_ObjectTrackable_OnObje460465675.h"
#include "WikitudeUnityPlugin_Wikitude_ObjectTracker3006704158.h"
#include "WikitudeUnityPlugin_Wikitude_PluginManager1017991214.h"
#include "WikitudeUnityPlugin_Wikitude_PluginManager_OnCamer3843633858.h"
#include "WikitudeUnityPlugin_Wikitude_PluginManager_OnPlugi2376488508.h"
#include "WikitudeUnityPlugin_Wikitude_Trackable347424808.h"
#include "WikitudeUnityPlugin_Wikitude_TrackerBehaviour921360922.h"







#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1900 = { sizeof (TextRenderFlags_t2418684345)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1900[3] = 
{
	TextRenderFlags_t2418684345::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1901 = { sizeof (TMP_TextElementType_t1276645592)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1901[3] = 
{
	TMP_TextElementType_t1276645592::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1902 = { sizeof (MaskingTypes_t3687969768)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1902[4] = 
{
	MaskingTypes_t3687969768::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1903 = { sizeof (TextOverflowModes_t1430035314)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1903[8] = 
{
	TextOverflowModes_t1430035314::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1904 = { sizeof (MaskingOffsetMode_t2266644590)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1904[3] = 
{
	MaskingOffsetMode_t2266644590::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1905 = { sizeof (TextureMappingOptions_t270963663)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1905[5] = 
{
	TextureMappingOptions_t270963663::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1906 = { sizeof (FontStyles_t3828945032)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1906[12] = 
{
	FontStyles_t3828945032::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1907 = { sizeof (FontWeights_t3122883458)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1907[10] = 
{
	FontWeights_t3122883458::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1908 = { sizeof (TagUnits_t1169424683)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1908[4] = 
{
	TagUnits_t1169424683::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1909 = { sizeof (TagType_t123236451)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1909[5] = 
{
	TagType_t123236451::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1910 = { sizeof (TMP_Text_t2599618874), -1, sizeof(TMP_Text_t2599618874_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1910[202] = 
{
	TMP_Text_t2599618874::get_offset_of_m_text_28(),
	TMP_Text_t2599618874::get_offset_of_m_isRightToLeft_29(),
	TMP_Text_t2599618874::get_offset_of_m_fontAsset_30(),
	TMP_Text_t2599618874::get_offset_of_m_currentFontAsset_31(),
	TMP_Text_t2599618874::get_offset_of_m_isSDFShader_32(),
	TMP_Text_t2599618874::get_offset_of_m_sharedMaterial_33(),
	TMP_Text_t2599618874::get_offset_of_m_currentMaterial_34(),
	TMP_Text_t2599618874::get_offset_of_m_materialReferences_35(),
	TMP_Text_t2599618874::get_offset_of_m_materialReferenceIndexLookup_36(),
	TMP_Text_t2599618874::get_offset_of_m_materialReferenceStack_37(),
	TMP_Text_t2599618874::get_offset_of_m_currentMaterialIndex_38(),
	TMP_Text_t2599618874::get_offset_of_m_fontSharedMaterials_39(),
	TMP_Text_t2599618874::get_offset_of_m_fontMaterial_40(),
	TMP_Text_t2599618874::get_offset_of_m_fontMaterials_41(),
	TMP_Text_t2599618874::get_offset_of_m_isMaterialDirty_42(),
	TMP_Text_t2599618874::get_offset_of_m_fontColor32_43(),
	TMP_Text_t2599618874::get_offset_of_m_fontColor_44(),
	TMP_Text_t2599618874_StaticFields::get_offset_of_s_colorWhite_45(),
	TMP_Text_t2599618874::get_offset_of_m_underlineColor_46(),
	TMP_Text_t2599618874::get_offset_of_m_strikethroughColor_47(),
	TMP_Text_t2599618874::get_offset_of_m_highlightColor_48(),
	TMP_Text_t2599618874::get_offset_of_m_enableVertexGradient_49(),
	TMP_Text_t2599618874::get_offset_of_m_fontColorGradient_50(),
	TMP_Text_t2599618874::get_offset_of_m_fontColorGradientPreset_51(),
	TMP_Text_t2599618874::get_offset_of_m_spriteAsset_52(),
	TMP_Text_t2599618874::get_offset_of_m_tintAllSprites_53(),
	TMP_Text_t2599618874::get_offset_of_m_tintSprite_54(),
	TMP_Text_t2599618874::get_offset_of_m_spriteColor_55(),
	TMP_Text_t2599618874::get_offset_of_m_overrideHtmlColors_56(),
	TMP_Text_t2599618874::get_offset_of_m_faceColor_57(),
	TMP_Text_t2599618874::get_offset_of_m_outlineColor_58(),
	TMP_Text_t2599618874::get_offset_of_m_outlineWidth_59(),
	TMP_Text_t2599618874::get_offset_of_m_fontSize_60(),
	TMP_Text_t2599618874::get_offset_of_m_currentFontSize_61(),
	TMP_Text_t2599618874::get_offset_of_m_fontSizeBase_62(),
	TMP_Text_t2599618874::get_offset_of_m_sizeStack_63(),
	TMP_Text_t2599618874::get_offset_of_m_fontWeight_64(),
	TMP_Text_t2599618874::get_offset_of_m_fontWeightInternal_65(),
	TMP_Text_t2599618874::get_offset_of_m_fontWeightStack_66(),
	TMP_Text_t2599618874::get_offset_of_m_enableAutoSizing_67(),
	TMP_Text_t2599618874::get_offset_of_m_maxFontSize_68(),
	TMP_Text_t2599618874::get_offset_of_m_minFontSize_69(),
	TMP_Text_t2599618874::get_offset_of_m_fontSizeMin_70(),
	TMP_Text_t2599618874::get_offset_of_m_fontSizeMax_71(),
	TMP_Text_t2599618874::get_offset_of_m_fontStyle_72(),
	TMP_Text_t2599618874::get_offset_of_m_style_73(),
	TMP_Text_t2599618874::get_offset_of_m_fontStyleStack_74(),
	TMP_Text_t2599618874::get_offset_of_m_isUsingBold_75(),
	TMP_Text_t2599618874::get_offset_of_m_textAlignment_76(),
	TMP_Text_t2599618874::get_offset_of_m_lineJustification_77(),
	TMP_Text_t2599618874::get_offset_of_m_lineJustificationStack_78(),
	TMP_Text_t2599618874::get_offset_of_m_textContainerLocalCorners_79(),
	TMP_Text_t2599618874::get_offset_of_m_isAlignmentEnumConverted_80(),
	TMP_Text_t2599618874::get_offset_of_m_characterSpacing_81(),
	TMP_Text_t2599618874::get_offset_of_m_cSpacing_82(),
	TMP_Text_t2599618874::get_offset_of_m_monoSpacing_83(),
	TMP_Text_t2599618874::get_offset_of_m_wordSpacing_84(),
	TMP_Text_t2599618874::get_offset_of_m_lineSpacing_85(),
	TMP_Text_t2599618874::get_offset_of_m_lineSpacingDelta_86(),
	TMP_Text_t2599618874::get_offset_of_m_lineHeight_87(),
	TMP_Text_t2599618874::get_offset_of_m_lineSpacingMax_88(),
	TMP_Text_t2599618874::get_offset_of_m_paragraphSpacing_89(),
	TMP_Text_t2599618874::get_offset_of_m_charWidthMaxAdj_90(),
	TMP_Text_t2599618874::get_offset_of_m_charWidthAdjDelta_91(),
	TMP_Text_t2599618874::get_offset_of_m_enableWordWrapping_92(),
	TMP_Text_t2599618874::get_offset_of_m_isCharacterWrappingEnabled_93(),
	TMP_Text_t2599618874::get_offset_of_m_isNonBreakingSpace_94(),
	TMP_Text_t2599618874::get_offset_of_m_isIgnoringAlignment_95(),
	TMP_Text_t2599618874::get_offset_of_m_wordWrappingRatios_96(),
	TMP_Text_t2599618874::get_offset_of_m_overflowMode_97(),
	TMP_Text_t2599618874::get_offset_of_m_firstOverflowCharacterIndex_98(),
	TMP_Text_t2599618874::get_offset_of_m_linkedTextComponent_99(),
	TMP_Text_t2599618874::get_offset_of_m_isLinkedTextComponent_100(),
	TMP_Text_t2599618874::get_offset_of_m_isTextTruncated_101(),
	TMP_Text_t2599618874::get_offset_of_m_enableKerning_102(),
	TMP_Text_t2599618874::get_offset_of_m_enableExtraPadding_103(),
	TMP_Text_t2599618874::get_offset_of_checkPaddingRequired_104(),
	TMP_Text_t2599618874::get_offset_of_m_isRichText_105(),
	TMP_Text_t2599618874::get_offset_of_m_parseCtrlCharacters_106(),
	TMP_Text_t2599618874::get_offset_of_m_isOverlay_107(),
	TMP_Text_t2599618874::get_offset_of_m_isOrthographic_108(),
	TMP_Text_t2599618874::get_offset_of_m_isCullingEnabled_109(),
	TMP_Text_t2599618874::get_offset_of_m_ignoreRectMaskCulling_110(),
	TMP_Text_t2599618874::get_offset_of_m_ignoreCulling_111(),
	TMP_Text_t2599618874::get_offset_of_m_horizontalMapping_112(),
	TMP_Text_t2599618874::get_offset_of_m_verticalMapping_113(),
	TMP_Text_t2599618874::get_offset_of_m_uvLineOffset_114(),
	TMP_Text_t2599618874::get_offset_of_m_renderMode_115(),
	TMP_Text_t2599618874::get_offset_of_m_geometrySortingOrder_116(),
	TMP_Text_t2599618874::get_offset_of_m_firstVisibleCharacter_117(),
	TMP_Text_t2599618874::get_offset_of_m_maxVisibleCharacters_118(),
	TMP_Text_t2599618874::get_offset_of_m_maxVisibleWords_119(),
	TMP_Text_t2599618874::get_offset_of_m_maxVisibleLines_120(),
	TMP_Text_t2599618874::get_offset_of_m_useMaxVisibleDescender_121(),
	TMP_Text_t2599618874::get_offset_of_m_pageToDisplay_122(),
	TMP_Text_t2599618874::get_offset_of_m_isNewPage_123(),
	TMP_Text_t2599618874::get_offset_of_m_margin_124(),
	TMP_Text_t2599618874::get_offset_of_m_marginLeft_125(),
	TMP_Text_t2599618874::get_offset_of_m_marginRight_126(),
	TMP_Text_t2599618874::get_offset_of_m_marginWidth_127(),
	TMP_Text_t2599618874::get_offset_of_m_marginHeight_128(),
	TMP_Text_t2599618874::get_offset_of_m_width_129(),
	TMP_Text_t2599618874::get_offset_of_m_textInfo_130(),
	TMP_Text_t2599618874::get_offset_of_m_havePropertiesChanged_131(),
	TMP_Text_t2599618874::get_offset_of_m_isUsingLegacyAnimationComponent_132(),
	TMP_Text_t2599618874::get_offset_of_m_transform_133(),
	TMP_Text_t2599618874::get_offset_of_m_rectTransform_134(),
	TMP_Text_t2599618874::get_offset_of_U3CautoSizeTextContainerU3Ek__BackingField_135(),
	TMP_Text_t2599618874::get_offset_of_m_autoSizeTextContainer_136(),
	TMP_Text_t2599618874::get_offset_of_m_mesh_137(),
	TMP_Text_t2599618874::get_offset_of_m_isVolumetricText_138(),
	TMP_Text_t2599618874::get_offset_of_m_spriteAnimator_139(),
	TMP_Text_t2599618874::get_offset_of_m_flexibleHeight_140(),
	TMP_Text_t2599618874::get_offset_of_m_flexibleWidth_141(),
	TMP_Text_t2599618874::get_offset_of_m_minWidth_142(),
	TMP_Text_t2599618874::get_offset_of_m_minHeight_143(),
	TMP_Text_t2599618874::get_offset_of_m_maxWidth_144(),
	TMP_Text_t2599618874::get_offset_of_m_maxHeight_145(),
	TMP_Text_t2599618874::get_offset_of_m_LayoutElement_146(),
	TMP_Text_t2599618874::get_offset_of_m_preferredWidth_147(),
	TMP_Text_t2599618874::get_offset_of_m_renderedWidth_148(),
	TMP_Text_t2599618874::get_offset_of_m_isPreferredWidthDirty_149(),
	TMP_Text_t2599618874::get_offset_of_m_preferredHeight_150(),
	TMP_Text_t2599618874::get_offset_of_m_renderedHeight_151(),
	TMP_Text_t2599618874::get_offset_of_m_isPreferredHeightDirty_152(),
	TMP_Text_t2599618874::get_offset_of_m_isCalculatingPreferredValues_153(),
	TMP_Text_t2599618874::get_offset_of_m_recursiveCount_154(),
	TMP_Text_t2599618874::get_offset_of_m_layoutPriority_155(),
	TMP_Text_t2599618874::get_offset_of_m_isCalculateSizeRequired_156(),
	TMP_Text_t2599618874::get_offset_of_m_isLayoutDirty_157(),
	TMP_Text_t2599618874::get_offset_of_m_verticesAlreadyDirty_158(),
	TMP_Text_t2599618874::get_offset_of_m_layoutAlreadyDirty_159(),
	TMP_Text_t2599618874::get_offset_of_m_isAwake_160(),
	TMP_Text_t2599618874::get_offset_of_m_isInputParsingRequired_161(),
	TMP_Text_t2599618874::get_offset_of_m_inputSource_162(),
	TMP_Text_t2599618874::get_offset_of_old_text_163(),
	TMP_Text_t2599618874::get_offset_of_m_fontScale_164(),
	TMP_Text_t2599618874::get_offset_of_m_fontScaleMultiplier_165(),
	TMP_Text_t2599618874::get_offset_of_m_htmlTag_166(),
	TMP_Text_t2599618874::get_offset_of_m_xmlAttribute_167(),
	TMP_Text_t2599618874::get_offset_of_m_attributeParameterValues_168(),
	TMP_Text_t2599618874::get_offset_of_tag_LineIndent_169(),
	TMP_Text_t2599618874::get_offset_of_tag_Indent_170(),
	TMP_Text_t2599618874::get_offset_of_m_indentStack_171(),
	TMP_Text_t2599618874::get_offset_of_tag_NoParsing_172(),
	TMP_Text_t2599618874::get_offset_of_m_isParsingText_173(),
	TMP_Text_t2599618874::get_offset_of_m_FXMatrix_174(),
	TMP_Text_t2599618874::get_offset_of_m_isFXMatrixSet_175(),
	TMP_Text_t2599618874::get_offset_of_m_char_buffer_176(),
	TMP_Text_t2599618874::get_offset_of_m_internalCharacterInfo_177(),
	TMP_Text_t2599618874::get_offset_of_m_input_CharArray_178(),
	TMP_Text_t2599618874::get_offset_of_m_charArray_Length_179(),
	TMP_Text_t2599618874::get_offset_of_m_totalCharacterCount_180(),
	TMP_Text_t2599618874::get_offset_of_m_SavedWordWrapState_181(),
	TMP_Text_t2599618874::get_offset_of_m_SavedLineState_182(),
	TMP_Text_t2599618874::get_offset_of_m_characterCount_183(),
	TMP_Text_t2599618874::get_offset_of_m_firstCharacterOfLine_184(),
	TMP_Text_t2599618874::get_offset_of_m_firstVisibleCharacterOfLine_185(),
	TMP_Text_t2599618874::get_offset_of_m_lastCharacterOfLine_186(),
	TMP_Text_t2599618874::get_offset_of_m_lastVisibleCharacterOfLine_187(),
	TMP_Text_t2599618874::get_offset_of_m_lineNumber_188(),
	TMP_Text_t2599618874::get_offset_of_m_lineVisibleCharacterCount_189(),
	TMP_Text_t2599618874::get_offset_of_m_pageNumber_190(),
	TMP_Text_t2599618874::get_offset_of_m_maxAscender_191(),
	TMP_Text_t2599618874::get_offset_of_m_maxCapHeight_192(),
	TMP_Text_t2599618874::get_offset_of_m_maxDescender_193(),
	TMP_Text_t2599618874::get_offset_of_m_maxLineAscender_194(),
	TMP_Text_t2599618874::get_offset_of_m_maxLineDescender_195(),
	TMP_Text_t2599618874::get_offset_of_m_startOfLineAscender_196(),
	TMP_Text_t2599618874::get_offset_of_m_lineOffset_197(),
	TMP_Text_t2599618874::get_offset_of_m_meshExtents_198(),
	TMP_Text_t2599618874::get_offset_of_m_htmlColor_199(),
	TMP_Text_t2599618874::get_offset_of_m_colorStack_200(),
	TMP_Text_t2599618874::get_offset_of_m_underlineColorStack_201(),
	TMP_Text_t2599618874::get_offset_of_m_strikethroughColorStack_202(),
	TMP_Text_t2599618874::get_offset_of_m_highlightColorStack_203(),
	TMP_Text_t2599618874::get_offset_of_m_tabSpacing_204(),
	TMP_Text_t2599618874::get_offset_of_m_spacing_205(),
	TMP_Text_t2599618874::get_offset_of_m_styleStack_206(),
	TMP_Text_t2599618874::get_offset_of_m_actionStack_207(),
	TMP_Text_t2599618874::get_offset_of_m_padding_208(),
	TMP_Text_t2599618874::get_offset_of_m_baselineOffset_209(),
	TMP_Text_t2599618874::get_offset_of_m_baselineOffsetStack_210(),
	TMP_Text_t2599618874::get_offset_of_m_xAdvance_211(),
	TMP_Text_t2599618874::get_offset_of_m_textElementType_212(),
	TMP_Text_t2599618874::get_offset_of_m_cached_TextElement_213(),
	TMP_Text_t2599618874::get_offset_of_m_cached_Underline_GlyphInfo_214(),
	TMP_Text_t2599618874::get_offset_of_m_cached_Ellipsis_GlyphInfo_215(),
	TMP_Text_t2599618874::get_offset_of_m_defaultSpriteAsset_216(),
	TMP_Text_t2599618874::get_offset_of_m_currentSpriteAsset_217(),
	TMP_Text_t2599618874::get_offset_of_m_spriteCount_218(),
	TMP_Text_t2599618874::get_offset_of_m_spriteIndex_219(),
	TMP_Text_t2599618874::get_offset_of_m_inlineGraphics_220(),
	TMP_Text_t2599618874::get_offset_of_m_spriteAnimationID_221(),
	TMP_Text_t2599618874::get_offset_of_m_ignoreActiveState_222(),
	TMP_Text_t2599618874::get_offset_of_k_Power_223(),
	TMP_Text_t2599618874_StaticFields::get_offset_of_k_LargePositiveVector2_224(),
	TMP_Text_t2599618874_StaticFields::get_offset_of_k_LargeNegativeVector2_225(),
	TMP_Text_t2599618874_StaticFields::get_offset_of_k_LargePositiveFloat_226(),
	TMP_Text_t2599618874_StaticFields::get_offset_of_k_LargeNegativeFloat_227(),
	TMP_Text_t2599618874_StaticFields::get_offset_of_k_LargePositiveInt_228(),
	TMP_Text_t2599618874_StaticFields::get_offset_of_k_LargeNegativeInt_229(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1911 = { sizeof (TextInputSources_t1522115805)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1911[5] = 
{
	TextInputSources_t1522115805::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1912 = { sizeof (TMP_TextElement_t129727469), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1912[9] = 
{
	TMP_TextElement_t129727469::get_offset_of_id_0(),
	TMP_TextElement_t129727469::get_offset_of_x_1(),
	TMP_TextElement_t129727469::get_offset_of_y_2(),
	TMP_TextElement_t129727469::get_offset_of_width_3(),
	TMP_TextElement_t129727469::get_offset_of_height_4(),
	TMP_TextElement_t129727469::get_offset_of_xOffset_5(),
	TMP_TextElement_t129727469::get_offset_of_yOffset_6(),
	TMP_TextElement_t129727469::get_offset_of_xAdvance_7(),
	TMP_TextElement_t129727469::get_offset_of_scale_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1913 = { sizeof (TMP_TextInfo_t3598145122), -1, sizeof(TMP_TextInfo_t3598145122_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1913[18] = 
{
	TMP_TextInfo_t3598145122_StaticFields::get_offset_of_k_InfinityVectorPositive_0(),
	TMP_TextInfo_t3598145122_StaticFields::get_offset_of_k_InfinityVectorNegative_1(),
	TMP_TextInfo_t3598145122::get_offset_of_textComponent_2(),
	TMP_TextInfo_t3598145122::get_offset_of_characterCount_3(),
	TMP_TextInfo_t3598145122::get_offset_of_spriteCount_4(),
	TMP_TextInfo_t3598145122::get_offset_of_spaceCount_5(),
	TMP_TextInfo_t3598145122::get_offset_of_wordCount_6(),
	TMP_TextInfo_t3598145122::get_offset_of_linkCount_7(),
	TMP_TextInfo_t3598145122::get_offset_of_lineCount_8(),
	TMP_TextInfo_t3598145122::get_offset_of_pageCount_9(),
	TMP_TextInfo_t3598145122::get_offset_of_materialCount_10(),
	TMP_TextInfo_t3598145122::get_offset_of_characterInfo_11(),
	TMP_TextInfo_t3598145122::get_offset_of_wordInfo_12(),
	TMP_TextInfo_t3598145122::get_offset_of_linkInfo_13(),
	TMP_TextInfo_t3598145122::get_offset_of_lineInfo_14(),
	TMP_TextInfo_t3598145122::get_offset_of_pageInfo_15(),
	TMP_TextInfo_t3598145122::get_offset_of_meshInfo_16(),
	TMP_TextInfo_t3598145122::get_offset_of_m_CachedMeshInfo_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1914 = { sizeof (CaretPosition_t3997512201)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1914[4] = 
{
	CaretPosition_t3997512201::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1915 = { sizeof (CaretInfo_t841780893)+ sizeof (Il2CppObject), sizeof(CaretInfo_t841780893 ), 0, 0 };
extern const int32_t g_FieldOffsetTable1915[2] = 
{
	CaretInfo_t841780893::get_offset_of_index_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	CaretInfo_t841780893::get_offset_of_position_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1916 = { sizeof (TMP_TextUtilities_t2105690005), -1, sizeof(TMP_TextUtilities_t2105690005_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1916[3] = 
{
	TMP_TextUtilities_t2105690005_StaticFields::get_offset_of_m_rectWorldCorners_0(),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1917 = { sizeof (LineSegment_t1526544958)+ sizeof (Il2CppObject), sizeof(LineSegment_t1526544958 ), 0, 0 };
extern const int32_t g_FieldOffsetTable1917[2] = 
{
	LineSegment_t1526544958::get_offset_of_Point1_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	LineSegment_t1526544958::get_offset_of_Point2_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1918 = { sizeof (TMP_UpdateManager_t4114267509), -1, sizeof(TMP_UpdateManager_t4114267509_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1918[5] = 
{
	TMP_UpdateManager_t4114267509_StaticFields::get_offset_of_s_Instance_0(),
	TMP_UpdateManager_t4114267509::get_offset_of_m_LayoutRebuildQueue_1(),
	TMP_UpdateManager_t4114267509::get_offset_of_m_LayoutQueueLookup_2(),
	TMP_UpdateManager_t4114267509::get_offset_of_m_GraphicRebuildQueue_3(),
	TMP_UpdateManager_t4114267509::get_offset_of_m_GraphicQueueLookup_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1919 = { sizeof (TMP_UpdateRegistry_t461608481), -1, sizeof(TMP_UpdateRegistry_t461608481_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1919[5] = 
{
	TMP_UpdateRegistry_t461608481_StaticFields::get_offset_of_s_Instance_0(),
	TMP_UpdateRegistry_t461608481::get_offset_of_m_LayoutRebuildQueue_1(),
	TMP_UpdateRegistry_t461608481::get_offset_of_m_LayoutQueueLookup_2(),
	TMP_UpdateRegistry_t461608481::get_offset_of_m_GraphicRebuildQueue_3(),
	TMP_UpdateRegistry_t461608481::get_offset_of_m_GraphicQueueLookup_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1920 = { sizeof (TMP_BasicXmlTagStack_t2962628096)+ sizeof (Il2CppObject), sizeof(TMP_BasicXmlTagStack_t2962628096 ), 0, 0 };
extern const int32_t g_FieldOffsetTable1920[10] = 
{
	TMP_BasicXmlTagStack_t2962628096::get_offset_of_bold_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TMP_BasicXmlTagStack_t2962628096::get_offset_of_italic_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TMP_BasicXmlTagStack_t2962628096::get_offset_of_underline_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TMP_BasicXmlTagStack_t2962628096::get_offset_of_strikethrough_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TMP_BasicXmlTagStack_t2962628096::get_offset_of_highlight_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TMP_BasicXmlTagStack_t2962628096::get_offset_of_superscript_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TMP_BasicXmlTagStack_t2962628096::get_offset_of_subscript_6() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TMP_BasicXmlTagStack_t2962628096::get_offset_of_uppercase_7() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TMP_BasicXmlTagStack_t2962628096::get_offset_of_lowercase_8() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TMP_BasicXmlTagStack_t2962628096::get_offset_of_smallcaps_9() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1921 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1921[5] = 
{
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1922 = { sizeof (Compute_DistanceTransform_EventTypes_t2554612104)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1922[3] = 
{
	Compute_DistanceTransform_EventTypes_t2554612104::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1923 = { sizeof (TMPro_EventManager_t712497257), -1, sizeof(TMPro_EventManager_t712497257_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1923[12] = 
{
	TMPro_EventManager_t712497257_StaticFields::get_offset_of_COMPUTE_DT_EVENT_0(),
	TMPro_EventManager_t712497257_StaticFields::get_offset_of_MATERIAL_PROPERTY_EVENT_1(),
	TMPro_EventManager_t712497257_StaticFields::get_offset_of_FONT_PROPERTY_EVENT_2(),
	TMPro_EventManager_t712497257_StaticFields::get_offset_of_SPRITE_ASSET_PROPERTY_EVENT_3(),
	TMPro_EventManager_t712497257_StaticFields::get_offset_of_TEXTMESHPRO_PROPERTY_EVENT_4(),
	TMPro_EventManager_t712497257_StaticFields::get_offset_of_DRAG_AND_DROP_MATERIAL_EVENT_5(),
	TMPro_EventManager_t712497257_StaticFields::get_offset_of_TEXT_STYLE_PROPERTY_EVENT_6(),
	TMPro_EventManager_t712497257_StaticFields::get_offset_of_COLOR_GRADIENT_PROPERTY_EVENT_7(),
	TMPro_EventManager_t712497257_StaticFields::get_offset_of_TMP_SETTINGS_PROPERTY_EVENT_8(),
	TMPro_EventManager_t712497257_StaticFields::get_offset_of_TEXTMESHPRO_UGUI_PROPERTY_EVENT_9(),
	TMPro_EventManager_t712497257_StaticFields::get_offset_of_OnPreRenderObject_Event_10(),
	TMPro_EventManager_t712497257_StaticFields::get_offset_of_TEXT_CHANGED_EVENT_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1924 = { sizeof (Compute_DT_EventArgs_t1071353166), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1924[3] = 
{
	Compute_DT_EventArgs_t1071353166::get_offset_of_EventType_0(),
	Compute_DT_EventArgs_t1071353166::get_offset_of_ProgressPercentage_1(),
	Compute_DT_EventArgs_t1071353166::get_offset_of_Colors_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1925 = { sizeof (TMPro_ExtensionMethods_t1453208966), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1926 = { sizeof (TMP_Math_t624304809), -1, sizeof(TMP_Math_t624304809_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1926[8] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	TMP_Math_t624304809_StaticFields::get_offset_of_MAX_16BIT_6(),
	TMP_Math_t624304809_StaticFields::get_offset_of_MIN_16BIT_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1927 = { sizeof (FaceInfo_t2243299176), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1927[21] = 
{
	FaceInfo_t2243299176::get_offset_of_Name_0(),
	FaceInfo_t2243299176::get_offset_of_PointSize_1(),
	FaceInfo_t2243299176::get_offset_of_Scale_2(),
	FaceInfo_t2243299176::get_offset_of_CharacterCount_3(),
	FaceInfo_t2243299176::get_offset_of_LineHeight_4(),
	FaceInfo_t2243299176::get_offset_of_Baseline_5(),
	FaceInfo_t2243299176::get_offset_of_Ascender_6(),
	FaceInfo_t2243299176::get_offset_of_CapHeight_7(),
	FaceInfo_t2243299176::get_offset_of_Descender_8(),
	FaceInfo_t2243299176::get_offset_of_CenterLine_9(),
	FaceInfo_t2243299176::get_offset_of_SuperscriptOffset_10(),
	FaceInfo_t2243299176::get_offset_of_SubscriptOffset_11(),
	FaceInfo_t2243299176::get_offset_of_SubSize_12(),
	FaceInfo_t2243299176::get_offset_of_Underline_13(),
	FaceInfo_t2243299176::get_offset_of_UnderlineThickness_14(),
	FaceInfo_t2243299176::get_offset_of_strikethrough_15(),
	FaceInfo_t2243299176::get_offset_of_strikethroughThickness_16(),
	FaceInfo_t2243299176::get_offset_of_TabWidth_17(),
	FaceInfo_t2243299176::get_offset_of_Padding_18(),
	FaceInfo_t2243299176::get_offset_of_AtlasWidth_19(),
	FaceInfo_t2243299176::get_offset_of_AtlasHeight_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1928 = { sizeof (TMP_Glyph_t581847833), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1929 = { sizeof (FontCreationSetting_t628772060)+ sizeof (Il2CppObject), sizeof(FontCreationSetting_t628772060_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1929[12] = 
{
	FontCreationSetting_t628772060::get_offset_of_fontSourcePath_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	FontCreationSetting_t628772060::get_offset_of_fontSizingMode_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	FontCreationSetting_t628772060::get_offset_of_fontSize_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	FontCreationSetting_t628772060::get_offset_of_fontPadding_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	FontCreationSetting_t628772060::get_offset_of_fontPackingMode_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	FontCreationSetting_t628772060::get_offset_of_fontAtlasWidth_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
	FontCreationSetting_t628772060::get_offset_of_fontAtlasHeight_6() + static_cast<int32_t>(sizeof(Il2CppObject)),
	FontCreationSetting_t628772060::get_offset_of_fontCharacterSet_7() + static_cast<int32_t>(sizeof(Il2CppObject)),
	FontCreationSetting_t628772060::get_offset_of_fontStyle_8() + static_cast<int32_t>(sizeof(Il2CppObject)),
	FontCreationSetting_t628772060::get_offset_of_fontStlyeModifier_9() + static_cast<int32_t>(sizeof(Il2CppObject)),
	FontCreationSetting_t628772060::get_offset_of_fontRenderMode_10() + static_cast<int32_t>(sizeof(Il2CppObject)),
	FontCreationSetting_t628772060::get_offset_of_fontKerning_11() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1930 = { sizeof (Glyph2D_t1260586688), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1930[8] = 
{
	Glyph2D_t1260586688::get_offset_of_bottomLeft_0(),
	Glyph2D_t1260586688::get_offset_of_topLeft_1(),
	Glyph2D_t1260586688::get_offset_of_bottomRight_2(),
	Glyph2D_t1260586688::get_offset_of_topRight_3(),
	Glyph2D_t1260586688::get_offset_of_uv0_4(),
	Glyph2D_t1260586688::get_offset_of_uv1_5(),
	Glyph2D_t1260586688::get_offset_of_uv2_6(),
	Glyph2D_t1260586688::get_offset_of_uv3_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1931 = { sizeof (KerningPairKey_t536493877)+ sizeof (Il2CppObject), sizeof(KerningPairKey_t536493877 ), 0, 0 };
extern const int32_t g_FieldOffsetTable1931[3] = 
{
	KerningPairKey_t536493877::get_offset_of_ascii_Left_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	KerningPairKey_t536493877::get_offset_of_ascii_Right_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	KerningPairKey_t536493877::get_offset_of_key_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1932 = { sizeof (KerningPair_t2270855589), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1932[3] = 
{
	KerningPair_t2270855589::get_offset_of_AscII_Left_0(),
	KerningPair_t2270855589::get_offset_of_AscII_Right_1(),
	KerningPair_t2270855589::get_offset_of_XadvanceOffset_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1933 = { sizeof (KerningTable_t2322366871), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1933[1] = 
{
	KerningTable_t2322366871::get_offset_of_kerningPairs_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1934 = { sizeof (U3CU3Ec__DisplayClass3_0_t2918531336), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1934[2] = 
{
	U3CU3Ec__DisplayClass3_0_t2918531336::get_offset_of_left_0(),
	U3CU3Ec__DisplayClass3_0_t2918531336::get_offset_of_right_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1935 = { sizeof (U3CU3Ec__DisplayClass4_0_t2918531329), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1935[2] = 
{
	U3CU3Ec__DisplayClass4_0_t2918531329::get_offset_of_left_0(),
	U3CU3Ec__DisplayClass4_0_t2918531329::get_offset_of_right_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1936 = { sizeof (U3CU3Ec_t4077839018), -1, sizeof(U3CU3Ec_t4077839018_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1936[3] = 
{
	U3CU3Ec_t4077839018_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_t4077839018_StaticFields::get_offset_of_U3CU3E9__6_0_1(),
	U3CU3Ec_t4077839018_StaticFields::get_offset_of_U3CU3E9__6_1_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1937 = { sizeof (TMP_FontUtilities_t2599150238), -1, sizeof(TMP_FontUtilities_t2599150238_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1937[1] = 
{
	TMP_FontUtilities_t2599150238_StaticFields::get_offset_of_k_searchedFontAssets_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1938 = { sizeof (TMP_VertexDataUpdateFlags_t388000256)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1938[8] = 
{
	TMP_VertexDataUpdateFlags_t388000256::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1939 = { sizeof (TMP_CharacterInfo_t3185626797)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1939[35] = 
{
	TMP_CharacterInfo_t3185626797::get_offset_of_character_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TMP_CharacterInfo_t3185626797::get_offset_of_index_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TMP_CharacterInfo_t3185626797::get_offset_of_elementType_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TMP_CharacterInfo_t3185626797::get_offset_of_textElement_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TMP_CharacterInfo_t3185626797::get_offset_of_fontAsset_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TMP_CharacterInfo_t3185626797::get_offset_of_spriteAsset_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TMP_CharacterInfo_t3185626797::get_offset_of_spriteIndex_6() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TMP_CharacterInfo_t3185626797::get_offset_of_material_7() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TMP_CharacterInfo_t3185626797::get_offset_of_materialReferenceIndex_8() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TMP_CharacterInfo_t3185626797::get_offset_of_isUsingAlternateTypeface_9() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TMP_CharacterInfo_t3185626797::get_offset_of_pointSize_10() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TMP_CharacterInfo_t3185626797::get_offset_of_lineNumber_11() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TMP_CharacterInfo_t3185626797::get_offset_of_pageNumber_12() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TMP_CharacterInfo_t3185626797::get_offset_of_vertexIndex_13() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TMP_CharacterInfo_t3185626797::get_offset_of_vertex_TL_14() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TMP_CharacterInfo_t3185626797::get_offset_of_vertex_BL_15() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TMP_CharacterInfo_t3185626797::get_offset_of_vertex_TR_16() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TMP_CharacterInfo_t3185626797::get_offset_of_vertex_BR_17() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TMP_CharacterInfo_t3185626797::get_offset_of_topLeft_18() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TMP_CharacterInfo_t3185626797::get_offset_of_bottomLeft_19() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TMP_CharacterInfo_t3185626797::get_offset_of_topRight_20() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TMP_CharacterInfo_t3185626797::get_offset_of_bottomRight_21() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TMP_CharacterInfo_t3185626797::get_offset_of_origin_22() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TMP_CharacterInfo_t3185626797::get_offset_of_ascender_23() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TMP_CharacterInfo_t3185626797::get_offset_of_baseLine_24() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TMP_CharacterInfo_t3185626797::get_offset_of_descender_25() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TMP_CharacterInfo_t3185626797::get_offset_of_xAdvance_26() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TMP_CharacterInfo_t3185626797::get_offset_of_aspectRatio_27() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TMP_CharacterInfo_t3185626797::get_offset_of_scale_28() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TMP_CharacterInfo_t3185626797::get_offset_of_color_29() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TMP_CharacterInfo_t3185626797::get_offset_of_underlineColor_30() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TMP_CharacterInfo_t3185626797::get_offset_of_strikethroughColor_31() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TMP_CharacterInfo_t3185626797::get_offset_of_highlightColor_32() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TMP_CharacterInfo_t3185626797::get_offset_of_style_33() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TMP_CharacterInfo_t3185626797::get_offset_of_isVisible_34() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1940 = { sizeof (TMP_Vertex_t2404176824)+ sizeof (Il2CppObject), sizeof(TMP_Vertex_t2404176824 ), 0, 0 };
extern const int32_t g_FieldOffsetTable1940[5] = 
{
	TMP_Vertex_t2404176824::get_offset_of_position_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TMP_Vertex_t2404176824::get_offset_of_uv_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TMP_Vertex_t2404176824::get_offset_of_uv2_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TMP_Vertex_t2404176824::get_offset_of_uv4_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TMP_Vertex_t2404176824::get_offset_of_color_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1941 = { sizeof (VertexGradient_t345148380)+ sizeof (Il2CppObject), sizeof(VertexGradient_t345148380 ), 0, 0 };
extern const int32_t g_FieldOffsetTable1941[4] = 
{
	VertexGradient_t345148380::get_offset_of_topLeft_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	VertexGradient_t345148380::get_offset_of_topRight_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	VertexGradient_t345148380::get_offset_of_bottomLeft_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	VertexGradient_t345148380::get_offset_of_bottomRight_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1942 = { sizeof (TMP_PageInfo_t2608430633)+ sizeof (Il2CppObject), sizeof(TMP_PageInfo_t2608430633 ), 0, 0 };
extern const int32_t g_FieldOffsetTable1942[5] = 
{
	TMP_PageInfo_t2608430633::get_offset_of_firstCharacterIndex_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TMP_PageInfo_t2608430633::get_offset_of_lastCharacterIndex_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TMP_PageInfo_t2608430633::get_offset_of_ascender_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TMP_PageInfo_t2608430633::get_offset_of_baseLine_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TMP_PageInfo_t2608430633::get_offset_of_descender_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1943 = { sizeof (TMP_LinkInfo_t1092083476)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1943[7] = 
{
	TMP_LinkInfo_t1092083476::get_offset_of_textComponent_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TMP_LinkInfo_t1092083476::get_offset_of_hashCode_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TMP_LinkInfo_t1092083476::get_offset_of_linkIdFirstCharacterIndex_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TMP_LinkInfo_t1092083476::get_offset_of_linkIdLength_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TMP_LinkInfo_t1092083476::get_offset_of_linkTextfirstCharacterIndex_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TMP_LinkInfo_t1092083476::get_offset_of_linkTextLength_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TMP_LinkInfo_t1092083476::get_offset_of_linkID_6() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1944 = { sizeof (TMP_WordInfo_t3331066303)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1944[4] = 
{
	TMP_WordInfo_t3331066303::get_offset_of_textComponent_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TMP_WordInfo_t3331066303::get_offset_of_firstCharacterIndex_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TMP_WordInfo_t3331066303::get_offset_of_lastCharacterIndex_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TMP_WordInfo_t3331066303::get_offset_of_characterCount_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1945 = { sizeof (TMP_SpriteInfo_t2726321384)+ sizeof (Il2CppObject), sizeof(TMP_SpriteInfo_t2726321384 ), 0, 0 };
extern const int32_t g_FieldOffsetTable1945[3] = 
{
	TMP_SpriteInfo_t2726321384::get_offset_of_spriteIndex_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TMP_SpriteInfo_t2726321384::get_offset_of_characterIndex_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TMP_SpriteInfo_t2726321384::get_offset_of_vertexIndex_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1946 = { sizeof (Extents_t3837212874)+ sizeof (Il2CppObject), sizeof(Extents_t3837212874 ), 0, 0 };
extern const int32_t g_FieldOffsetTable1946[2] = 
{
	Extents_t3837212874::get_offset_of_min_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Extents_t3837212874::get_offset_of_max_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1947 = { sizeof (Mesh_Extents_t3388355125)+ sizeof (Il2CppObject), sizeof(Mesh_Extents_t3388355125 ), 0, 0 };
extern const int32_t g_FieldOffsetTable1947[2] = 
{
	Mesh_Extents_t3388355125::get_offset_of_min_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Mesh_Extents_t3388355125::get_offset_of_max_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1948 = { sizeof (WordWrapState_t341939652)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1948[53] = 
{
	WordWrapState_t341939652::get_offset_of_previous_WordBreak_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	WordWrapState_t341939652::get_offset_of_total_CharacterCount_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	WordWrapState_t341939652::get_offset_of_visible_CharacterCount_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	WordWrapState_t341939652::get_offset_of_visible_SpriteCount_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	WordWrapState_t341939652::get_offset_of_visible_LinkCount_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	WordWrapState_t341939652::get_offset_of_firstCharacterIndex_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
	WordWrapState_t341939652::get_offset_of_firstVisibleCharacterIndex_6() + static_cast<int32_t>(sizeof(Il2CppObject)),
	WordWrapState_t341939652::get_offset_of_lastCharacterIndex_7() + static_cast<int32_t>(sizeof(Il2CppObject)),
	WordWrapState_t341939652::get_offset_of_lastVisibleCharIndex_8() + static_cast<int32_t>(sizeof(Il2CppObject)),
	WordWrapState_t341939652::get_offset_of_lineNumber_9() + static_cast<int32_t>(sizeof(Il2CppObject)),
	WordWrapState_t341939652::get_offset_of_maxCapHeight_10() + static_cast<int32_t>(sizeof(Il2CppObject)),
	WordWrapState_t341939652::get_offset_of_maxAscender_11() + static_cast<int32_t>(sizeof(Il2CppObject)),
	WordWrapState_t341939652::get_offset_of_maxDescender_12() + static_cast<int32_t>(sizeof(Il2CppObject)),
	WordWrapState_t341939652::get_offset_of_maxLineAscender_13() + static_cast<int32_t>(sizeof(Il2CppObject)),
	WordWrapState_t341939652::get_offset_of_maxLineDescender_14() + static_cast<int32_t>(sizeof(Il2CppObject)),
	WordWrapState_t341939652::get_offset_of_previousLineAscender_15() + static_cast<int32_t>(sizeof(Il2CppObject)),
	WordWrapState_t341939652::get_offset_of_xAdvance_16() + static_cast<int32_t>(sizeof(Il2CppObject)),
	WordWrapState_t341939652::get_offset_of_preferredWidth_17() + static_cast<int32_t>(sizeof(Il2CppObject)),
	WordWrapState_t341939652::get_offset_of_preferredHeight_18() + static_cast<int32_t>(sizeof(Il2CppObject)),
	WordWrapState_t341939652::get_offset_of_previousLineScale_19() + static_cast<int32_t>(sizeof(Il2CppObject)),
	WordWrapState_t341939652::get_offset_of_wordCount_20() + static_cast<int32_t>(sizeof(Il2CppObject)),
	WordWrapState_t341939652::get_offset_of_fontStyle_21() + static_cast<int32_t>(sizeof(Il2CppObject)),
	WordWrapState_t341939652::get_offset_of_fontScale_22() + static_cast<int32_t>(sizeof(Il2CppObject)),
	WordWrapState_t341939652::get_offset_of_fontScaleMultiplier_23() + static_cast<int32_t>(sizeof(Il2CppObject)),
	WordWrapState_t341939652::get_offset_of_currentFontSize_24() + static_cast<int32_t>(sizeof(Il2CppObject)),
	WordWrapState_t341939652::get_offset_of_baselineOffset_25() + static_cast<int32_t>(sizeof(Il2CppObject)),
	WordWrapState_t341939652::get_offset_of_lineOffset_26() + static_cast<int32_t>(sizeof(Il2CppObject)),
	WordWrapState_t341939652::get_offset_of_textInfo_27() + static_cast<int32_t>(sizeof(Il2CppObject)),
	WordWrapState_t341939652::get_offset_of_lineInfo_28() + static_cast<int32_t>(sizeof(Il2CppObject)),
	WordWrapState_t341939652::get_offset_of_vertexColor_29() + static_cast<int32_t>(sizeof(Il2CppObject)),
	WordWrapState_t341939652::get_offset_of_underlineColor_30() + static_cast<int32_t>(sizeof(Il2CppObject)),
	WordWrapState_t341939652::get_offset_of_strikethroughColor_31() + static_cast<int32_t>(sizeof(Il2CppObject)),
	WordWrapState_t341939652::get_offset_of_highlightColor_32() + static_cast<int32_t>(sizeof(Il2CppObject)),
	WordWrapState_t341939652::get_offset_of_basicStyleStack_33() + static_cast<int32_t>(sizeof(Il2CppObject)),
	WordWrapState_t341939652::get_offset_of_colorStack_34() + static_cast<int32_t>(sizeof(Il2CppObject)),
	WordWrapState_t341939652::get_offset_of_underlineColorStack_35() + static_cast<int32_t>(sizeof(Il2CppObject)),
	WordWrapState_t341939652::get_offset_of_strikethroughColorStack_36() + static_cast<int32_t>(sizeof(Il2CppObject)),
	WordWrapState_t341939652::get_offset_of_highlightColorStack_37() + static_cast<int32_t>(sizeof(Il2CppObject)),
	WordWrapState_t341939652::get_offset_of_sizeStack_38() + static_cast<int32_t>(sizeof(Il2CppObject)),
	WordWrapState_t341939652::get_offset_of_indentStack_39() + static_cast<int32_t>(sizeof(Il2CppObject)),
	WordWrapState_t341939652::get_offset_of_fontWeightStack_40() + static_cast<int32_t>(sizeof(Il2CppObject)),
	WordWrapState_t341939652::get_offset_of_styleStack_41() + static_cast<int32_t>(sizeof(Il2CppObject)),
	WordWrapState_t341939652::get_offset_of_baselineStack_42() + static_cast<int32_t>(sizeof(Il2CppObject)),
	WordWrapState_t341939652::get_offset_of_actionStack_43() + static_cast<int32_t>(sizeof(Il2CppObject)),
	WordWrapState_t341939652::get_offset_of_materialReferenceStack_44() + static_cast<int32_t>(sizeof(Il2CppObject)),
	WordWrapState_t341939652::get_offset_of_lineJustificationStack_45() + static_cast<int32_t>(sizeof(Il2CppObject)),
	WordWrapState_t341939652::get_offset_of_spriteAnimationID_46() + static_cast<int32_t>(sizeof(Il2CppObject)),
	WordWrapState_t341939652::get_offset_of_currentFontAsset_47() + static_cast<int32_t>(sizeof(Il2CppObject)),
	WordWrapState_t341939652::get_offset_of_currentSpriteAsset_48() + static_cast<int32_t>(sizeof(Il2CppObject)),
	WordWrapState_t341939652::get_offset_of_currentMaterial_49() + static_cast<int32_t>(sizeof(Il2CppObject)),
	WordWrapState_t341939652::get_offset_of_currentMaterialIndex_50() + static_cast<int32_t>(sizeof(Il2CppObject)),
	WordWrapState_t341939652::get_offset_of_meshExtents_51() + static_cast<int32_t>(sizeof(Il2CppObject)),
	WordWrapState_t341939652::get_offset_of_tagNoParsing_52() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1949 = { sizeof (TagAttribute_t688278634)+ sizeof (Il2CppObject), sizeof(TagAttribute_t688278634 ), 0, 0 };
extern const int32_t g_FieldOffsetTable1949[3] = 
{
	TagAttribute_t688278634::get_offset_of_startIndex_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TagAttribute_t688278634::get_offset_of_length_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TagAttribute_t688278634::get_offset_of_hashCode_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1950 = { sizeof (XML_TagAttribute_t1174424309)+ sizeof (Il2CppObject), sizeof(XML_TagAttribute_t1174424309 ), 0, 0 };
extern const int32_t g_FieldOffsetTable1950[5] = 
{
	XML_TagAttribute_t1174424309::get_offset_of_nameHashCode_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	XML_TagAttribute_t1174424309::get_offset_of_valueType_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	XML_TagAttribute_t1174424309::get_offset_of_valueStartIndex_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	XML_TagAttribute_t1174424309::get_offset_of_valueLength_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	XML_TagAttribute_t1174424309::get_offset_of_valueHashCode_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1951 = { sizeof (TextMeshPro_t2393593166), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1951[17] = 
{
	TextMeshPro_t2393593166::get_offset_of_m_hasFontAssetChanged_230(),
	TextMeshPro_t2393593166::get_offset_of_m_previousLossyScaleY_231(),
	TextMeshPro_t2393593166::get_offset_of_m_renderer_232(),
	TextMeshPro_t2393593166::get_offset_of_m_meshFilter_233(),
	TextMeshPro_t2393593166::get_offset_of_m_isFirstAllocation_234(),
	TextMeshPro_t2393593166::get_offset_of_m_max_characters_235(),
	TextMeshPro_t2393593166::get_offset_of_m_max_numberOfLines_236(),
	TextMeshPro_t2393593166::get_offset_of_m_default_bounds_237(),
	TextMeshPro_t2393593166::get_offset_of_m_subTextObjects_238(),
	TextMeshPro_t2393593166::get_offset_of_m_isMaskingEnabled_239(),
	TextMeshPro_t2393593166::get_offset_of_isMaskUpdateRequired_240(),
	TextMeshPro_t2393593166::get_offset_of_m_maskType_241(),
	TextMeshPro_t2393593166::get_offset_of_m_EnvMapMatrix_242(),
	TextMeshPro_t2393593166::get_offset_of_m_RectTransformCorners_243(),
	TextMeshPro_t2393593166::get_offset_of_m_isRegisteredForEvents_244(),
	TextMeshPro_t2393593166::get_offset_of_loopCountA_245(),
	TextMeshPro_t2393593166::get_offset_of_m_currentAutoSizeMode_246(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1952 = { sizeof (ShaderUtilities_t714255158), -1, sizeof(ShaderUtilities_t714255158_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1952[59] = 
{
	ShaderUtilities_t714255158_StaticFields::get_offset_of_ID_MainTex_0(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_ID_FaceTex_1(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_ID_FaceColor_2(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_ID_FaceDilate_3(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_ID_Shininess_4(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_ID_UnderlayColor_5(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_ID_UnderlayOffsetX_6(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_ID_UnderlayOffsetY_7(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_ID_UnderlayDilate_8(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_ID_UnderlaySoftness_9(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_ID_WeightNormal_10(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_ID_WeightBold_11(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_ID_OutlineTex_12(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_ID_OutlineWidth_13(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_ID_OutlineSoftness_14(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_ID_OutlineColor_15(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_ID_GradientScale_16(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_ID_ScaleX_17(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_ID_ScaleY_18(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_ID_PerspectiveFilter_19(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_ID_TextureWidth_20(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_ID_TextureHeight_21(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_ID_BevelAmount_22(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_ID_GlowColor_23(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_ID_GlowOffset_24(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_ID_GlowPower_25(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_ID_GlowOuter_26(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_ID_LightAngle_27(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_ID_EnvMap_28(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_ID_EnvMatrix_29(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_ID_EnvMatrixRotation_30(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_ID_MaskCoord_31(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_ID_ClipRect_32(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_ID_MaskSoftnessX_33(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_ID_MaskSoftnessY_34(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_ID_VertexOffsetX_35(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_ID_VertexOffsetY_36(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_ID_UseClipRect_37(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_ID_StencilID_38(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_ID_StencilOp_39(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_ID_StencilComp_40(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_ID_StencilReadMask_41(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_ID_StencilWriteMask_42(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_ID_ShaderFlags_43(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_ID_ScaleRatio_A_44(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_ID_ScaleRatio_B_45(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_ID_ScaleRatio_C_46(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_Keyword_Bevel_47(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_Keyword_Glow_48(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_Keyword_Underlay_49(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_Keyword_Ratios_50(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_Keyword_MASK_SOFT_51(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_Keyword_MASK_HARD_52(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_Keyword_MASK_TEX_53(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_Keyword_Outline_54(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_ShaderTag_ZTestMode_55(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_ShaderTag_CullMode_56(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_m_clamp_57(),
	ShaderUtilities_t714255158_StaticFields::get_offset_of_isInitialized_58(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1953 = { sizeof (TextMeshProUGUI_t529313277), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1953[18] = 
{
	TextMeshProUGUI_t529313277::get_offset_of_m_hasFontAssetChanged_230(),
	TextMeshProUGUI_t529313277::get_offset_of_m_subTextObjects_231(),
	TextMeshProUGUI_t529313277::get_offset_of_m_previousLossyScaleY_232(),
	TextMeshProUGUI_t529313277::get_offset_of_m_RectTransformCorners_233(),
	TextMeshProUGUI_t529313277::get_offset_of_m_canvasRenderer_234(),
	TextMeshProUGUI_t529313277::get_offset_of_m_canvas_235(),
	TextMeshProUGUI_t529313277::get_offset_of_m_isFirstAllocation_236(),
	TextMeshProUGUI_t529313277::get_offset_of_m_max_characters_237(),
	TextMeshProUGUI_t529313277::get_offset_of_m_isMaskingEnabled_238(),
	TextMeshProUGUI_t529313277::get_offset_of_m_baseMaterial_239(),
	TextMeshProUGUI_t529313277::get_offset_of_m_isScrollRegionSet_240(),
	TextMeshProUGUI_t529313277::get_offset_of_m_stencilID_241(),
	TextMeshProUGUI_t529313277::get_offset_of_m_maskOffset_242(),
	TextMeshProUGUI_t529313277::get_offset_of_m_EnvMapMatrix_243(),
	TextMeshProUGUI_t529313277::get_offset_of_m_isRegisteredForEvents_244(),
	TextMeshProUGUI_t529313277::get_offset_of_m_recursiveCountA_245(),
	TextMeshProUGUI_t529313277::get_offset_of_loopCountA_246(),
	TextMeshProUGUI_t529313277::get_offset_of_m_isRebuildingLayout_247(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1954 = { sizeof (TextContainerAnchors_t945851193)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1954[11] = 
{
	TextContainerAnchors_t945851193::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1955 = { sizeof (TextContainer_t97923372), -1, sizeof(TextContainer_t97923372_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1955[13] = 
{
	TextContainer_t97923372::get_offset_of_m_hasChanged_2(),
	TextContainer_t97923372::get_offset_of_m_pivot_3(),
	TextContainer_t97923372::get_offset_of_m_anchorPosition_4(),
	TextContainer_t97923372::get_offset_of_m_rect_5(),
	TextContainer_t97923372::get_offset_of_m_isDefaultWidth_6(),
	TextContainer_t97923372::get_offset_of_m_isDefaultHeight_7(),
	TextContainer_t97923372::get_offset_of_m_isAutoFitting_8(),
	TextContainer_t97923372::get_offset_of_m_corners_9(),
	TextContainer_t97923372::get_offset_of_m_worldCorners_10(),
	TextContainer_t97923372::get_offset_of_m_margins_11(),
	TextContainer_t97923372::get_offset_of_m_rectTransform_12(),
	TextContainer_t97923372_StaticFields::get_offset_of_k_defaultSize_13(),
	TextContainer_t97923372::get_offset_of_m_textMeshPro_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1956 = { sizeof (SpriteAssetImportFormats_t116390639)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1956[3] = 
{
	SpriteAssetImportFormats_t116390639::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1957 = { sizeof (TexturePacker_t3148178657), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1958 = { sizeof (SpriteFrame_t3912389194)+ sizeof (Il2CppObject), sizeof(SpriteFrame_t3912389194 ), 0, 0 };
extern const int32_t g_FieldOffsetTable1958[4] = 
{
	SpriteFrame_t3912389194::get_offset_of_x_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	SpriteFrame_t3912389194::get_offset_of_y_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	SpriteFrame_t3912389194::get_offset_of_w_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	SpriteFrame_t3912389194::get_offset_of_h_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1959 = { sizeof (SpriteSize_t3355290999)+ sizeof (Il2CppObject), sizeof(SpriteSize_t3355290999 ), 0, 0 };
extern const int32_t g_FieldOffsetTable1959[2] = 
{
	SpriteSize_t3355290999::get_offset_of_w_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	SpriteSize_t3355290999::get_offset_of_h_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1960 = { sizeof (SpriteData_t3048397587)+ sizeof (Il2CppObject), sizeof(SpriteData_t3048397587_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1960[7] = 
{
	SpriteData_t3048397587::get_offset_of_filename_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	SpriteData_t3048397587::get_offset_of_frame_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	SpriteData_t3048397587::get_offset_of_rotated_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	SpriteData_t3048397587::get_offset_of_trimmed_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	SpriteData_t3048397587::get_offset_of_spriteSourceSize_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	SpriteData_t3048397587::get_offset_of_sourceSize_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
	SpriteData_t3048397587::get_offset_of_pivot_6() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1961 = { sizeof (SpriteDataObject_t308163541), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1961[1] = 
{
	SpriteDataObject_t308163541::get_offset_of_frames_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1962 = { sizeof (U3CPrivateImplementationDetailsU3E_t3057255366), -1, sizeof(U3CPrivateImplementationDetailsU3E_t3057255366_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1962[2] = 
{
	U3CPrivateImplementationDetailsU3E_t3057255366_StaticFields::get_offset_of_U37BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0(),
	U3CPrivateImplementationDetailsU3E_t3057255366_StaticFields::get_offset_of_U39E6378168821DBABB7EE3D0154346480FAC8AEF1_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1963 = { sizeof (__StaticArrayInitTypeSizeU3D12_t2710994317)+ sizeof (Il2CppObject), sizeof(__StaticArrayInitTypeSizeU3D12_t2710994317 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1964 = { sizeof (__StaticArrayInitTypeSizeU3D40_t1547998295)+ sizeof (Il2CppObject), sizeof(__StaticArrayInitTypeSizeU3D40_t1547998295 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1965 = { sizeof (U3CModuleU3E_t692745532), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1966 = { sizeof (AndroidBridge_t2441337961), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1966[12] = 
{
	AndroidBridge_t2441337961::get_offset_of_androidClass_0(),
	AndroidBridge_t2441337961::get_offset_of_androidJNIType_1(),
	AndroidBridge_t2441337961::get_offset_of_androidActivity_2(),
	AndroidBridge_t2441337961::get_offset_of__callMethod_3(),
	AndroidBridge_t2441337961::get_offset_of__callStringMethod_4(),
	AndroidBridge_t2441337961::get_offset_of__callFloatArrayMethod_5(),
	AndroidBridge_t2441337961::get_offset_of__callLongArrayMethod_6(),
	AndroidBridge_t2441337961::get_offset_of__callIntMethod_7(),
	AndroidBridge_t2441337961::get_offset_of__callFloatMethod_8(),
	AndroidBridge_t2441337961::get_offset_of__callBoolMethod_9(),
	AndroidBridge_t2441337961::get_offset_of__callLongMethod_10(),
	AndroidBridge_t2441337961::get_offset_of__nativeFramePtr_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1967 = { sizeof (iOSBridge_t3849146092), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1967[2] = 
{
	iOSBridge_t3849146092::get_offset_of__screenOrientation_0(),
	iOSBridge_t3849146092::get_offset_of__nativeFramePtr_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1968 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1969 = { sizeof (UnityBridge_t2504510425), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1969[9] = 
{
	UnityBridge_t2504510425::get_offset_of_GenericTrackingMatrices_0(),
	UnityBridge_t2504510425::get_offset_of_GenericProjectionMatrix_1(),
	UnityBridge_t2504510425::get_offset_of_ObjectTrackingMatrix_2(),
	UnityBridge_t2504510425::get_offset_of_ObjectProjectionMatrix_3(),
	UnityBridge_t2504510425::get_offset_of_TargetIDs_4(),
	UnityBridge_t2504510425::get_offset_of_TrackedTargets_5(),
	UnityBridge_t2504510425::get_offset_of_TargetCount_6(),
	UnityBridge_t2504510425::get_offset_of__isObjectTrackingRunning_7(),
	UnityBridge_t2504510425::get_offset_of__trackerManagerName_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1970 = { sizeof (WikitudeBridge_t4110292378), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1970[2] = 
{
	WikitudeBridge_t4110292378::get_offset_of__trackerManager_0(),
	WikitudeBridge_t4110292378::get_offset_of__trackerManagerGameObject_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1971 = { sizeof (UnityEditorSimulator_t2033215626), -1, sizeof(UnityEditorSimulator_t2033215626_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1971[2] = 
{
	UnityEditorSimulator_t2033215626_StaticFields::get_offset_of__defaultTrackingMatrix_0(),
	UnityEditorSimulator_t2033215626_StaticFields::get_offset_of__currentTargets_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1972 = { sizeof (SimulatedTarget_t1564170948)+ sizeof (Il2CppObject), sizeof(SimulatedTarget_t1564170948_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1972[3] = 
{
	SimulatedTarget_t1564170948::get_offset_of_Name_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	SimulatedTarget_t1564170948::get_offset_of_UniqueId_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	SimulatedTarget_t1564170948::get_offset_of_Offset_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1973 = { sizeof (ImageTrackable_t2462741734), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1973[7] = 
{
	ImageTrackable_t2462741734::get_offset_of__extendedTracking_9(),
	ImageTrackable_t2462741734::get_offset_of__targetsForExtendedTracking_10(),
	ImageTrackable_t2462741734::get_offset_of_OnImageRecognized_11(),
	ImageTrackable_t2462741734::get_offset_of_OnImageLost_12(),
	ImageTrackable_t2462741734::get_offset_of__preview_13(),
	ImageTrackable_t2462741734::get_offset_of__previewMaterial_14(),
	ImageTrackable_t2462741734::get_offset_of__previewMesh_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1974 = { sizeof (OnImageRecognizedEvent_t773132985), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1975 = { sizeof (OnImageLostEvent_t966623312), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1976 = { sizeof (ExtendedTrackingQuality_t364496425)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1976[4] = 
{
	ExtendedTrackingQuality_t364496425::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1977 = { sizeof (ImageRecognitionRangeExtension_t604843042)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1977[4] = 
{
	ImageRecognitionRangeExtension_t604843042::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1978 = { sizeof (ImageTracker_t271395264), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1978[8] = 
{
	ImageTracker_t271395264::get_offset_of_OnExtendedTrackingQualityChanged_15(),
	ImageTracker_t271395264::get_offset_of__targetSourceType_16(),
	ImageTracker_t271395264::get_offset_of__targetCollectionResource_17(),
	ImageTracker_t271395264::get_offset_of__cloudRecognitionService_18(),
	ImageTracker_t271395264::get_offset_of__maximumNumberOfConcurrentTrackableTargets_19(),
	ImageTracker_t271395264::get_offset_of__rangeExtension_20(),
	ImageTracker_t271395264::get_offset_of_U3CPhysicalTargetImageHeightsU3Ek__BackingField_21(),
	ImageTracker_t271395264::get_offset_of_U3CIsRegisteredU3Ek__BackingField_22(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1979 = { sizeof (OnExtendedTrackingQualityChangedEvent_t2560195974), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1980 = { sizeof (InstantTrackable_t3187174337), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1980[4] = 
{
	InstantTrackable_t3187174337::get_offset_of_OnInitializationStarted_9(),
	InstantTrackable_t3187174337::get_offset_of_OnInitializationStopped_10(),
	InstantTrackable_t3187174337::get_offset_of_OnSceneRecognized_11(),
	InstantTrackable_t3187174337::get_offset_of_OnSceneLost_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1981 = { sizeof (OnInitializationStartedEvent_t272217273), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1982 = { sizeof (OnInitializationStoppedEvent_t4207911327), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1983 = { sizeof (OnSceneRecognizedEvent_t2416994635), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1984 = { sizeof (OnSceneLostEvent_t389101256), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1985 = { sizeof (InstantTrackingPlaneOrientation_t1439566444)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1985[4] = 
{
	InstantTrackingPlaneOrientation_t1439566444::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1986 = { sizeof (InstantTrackingState_t3973410783)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1986[3] = 
{
	InstantTrackingState_t3973410783::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1987 = { sizeof (InstantTracker_t684352120), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1987[8] = 
{
	InstantTracker_t684352120::get_offset_of_OnStateChanged_15(),
	InstantTracker_t684352120::get_offset_of__deviceHeightAboveGround_16(),
	InstantTracker_t684352120::get_offset_of_OnScreenConversionComputed_17(),
	InstantTracker_t684352120::get_offset_of_OnPointCloudRequestFinished_18(),
	InstantTracker_t684352120::get_offset_of__trackingPlaneOrientation_19(),
	InstantTracker_t684352120::get_offset_of__trackingPlaneOrientationAngle_20(),
	InstantTracker_t684352120::get_offset_of__pointCloudRequestPending_21(),
	InstantTracker_t684352120::get_offset_of_CurrentState_22(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1988 = { sizeof (OnStateChangedEvent_t1359267737), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1989 = { sizeof (OnScreenConversionComputedEvent_t608500589), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1990 = { sizeof (OnPointCloudRequestFinishedEvent_t2541379958), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1991 = { sizeof (ObjectTrackable_t3378627079), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1991[4] = 
{
	ObjectTrackable_t3378627079::get_offset_of_OnObjectRecognized_9(),
	ObjectTrackable_t3378627079::get_offset_of_OnObjectLost_10(),
	ObjectTrackable_t3378627079::get_offset_of__pointCloud_11(),
	ObjectTrackable_t3378627079::get_offset_of__mapRenderer_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1992 = { sizeof (OnObjectRecognizedEvent_t3443511796), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1993 = { sizeof (OnObjectLostEvent_t460465675), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1994 = { sizeof (ObjectTracker_t3006704158), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1994[2] = 
{
	ObjectTracker_t3006704158::get_offset_of__targetCollectionResource_15(),
	ObjectTracker_t3006704158::get_offset_of_U3CIsRegisteredU3Ek__BackingField_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1995 = { sizeof (PluginManager_t1017991214), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1995[3] = 
{
	PluginManager_t1017991214::get_offset_of_OnCameraFrameAvailable_2(),
	PluginManager_t1017991214::get_offset_of_OnPluginFailure_3(),
	PluginManager_t1017991214::get_offset_of__bridge_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1996 = { sizeof (OnCameraFrameAvailableEvent_t3843633858), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1997 = { sizeof (OnPluginFailureEvent_t2376488508), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1998 = { sizeof (Trackable_t347424808), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1998[7] = 
{
	Trackable_t347424808::get_offset_of__targetPattern_2(),
	Trackable_t347424808::get_offset_of__targetPatternRegex_3(),
	Trackable_t347424808::get_offset_of__drawable_4(),
	Trackable_t347424808::get_offset_of__autoToggleVisibility_5(),
	Trackable_t347424808::get_offset_of__registeredToTracker_6(),
	Trackable_t347424808::get_offset_of__tracker_7(),
	Trackable_t347424808::get_offset_of__activeDrawables_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1999 = { sizeof (TrackerBehaviour_t921360922), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1999[13] = 
{
	TrackerBehaviour_t921360922::get_offset_of__manager_2(),
	TrackerBehaviour_t921360922::get_offset_of__registeredTrackables_3(),
	TrackerBehaviour_t921360922::get_offset_of__anchorTarget_4(),
	TrackerBehaviour_t921360922::get_offset_of__anchorPositionOffset_5(),
	TrackerBehaviour_t921360922::get_offset_of__anchorRotationOffset_6(),
	TrackerBehaviour_t921360922::get_offset_of__legacyTrackables_7(),
	TrackerBehaviour_t921360922::get_offset_of_U3CInitializedU3Ek__BackingField_8(),
	TrackerBehaviour_t921360922::get_offset_of__sceneCamera_9(),
	TrackerBehaviour_t921360922::get_offset_of__initialSceneCameraPosition_10(),
	TrackerBehaviour_t921360922::get_offset_of__initialSceneCameraRotation_11(),
	TrackerBehaviour_t921360922::get_offset_of__wikitudeCamera_12(),
	TrackerBehaviour_t921360922::get_offset_of_OnTargetsLoaded_13(),
	TrackerBehaviour_t921360922::get_offset_of_OnErrorLoadingTargets_14(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
