﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"





extern "C" void Context_t1744531130_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Context_t1744531130_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Context_t1744531130_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType Context_t1744531130_0_0_0;
extern "C" void Escape_t3294788190_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Escape_t3294788190_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Escape_t3294788190_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType Escape_t3294788190_0_0_0;
extern "C" void PreviousInfo_t2148130204_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void PreviousInfo_t2148130204_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void PreviousInfo_t2148130204_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType PreviousInfo_t2148130204_0_0_0;
extern "C" void DelegatePInvokeWrapper_AppDomainInitializer_t682969308();
extern const Il2CppType AppDomainInitializer_t682969308_0_0_0;
extern "C" void DelegatePInvokeWrapper_Swapper_t2822380397();
extern const Il2CppType Swapper_t2822380397_0_0_0;
extern "C" void DictionaryEntry_t3123975638_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void DictionaryEntry_t3123975638_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void DictionaryEntry_t3123975638_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType DictionaryEntry_t3123975638_0_0_0;
extern "C" void Slot_t3975888750_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Slot_t3975888750_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Slot_t3975888750_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType Slot_t3975888750_0_0_0;
extern "C" void Slot_t384495010_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Slot_t384495010_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Slot_t384495010_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType Slot_t384495010_0_0_0;
extern "C" void Enum_t4135868527_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Enum_t4135868527_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Enum_t4135868527_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType Enum_t4135868527_0_0_0;
extern "C" void DelegatePInvokeWrapper_ReadDelegate_t714865915();
extern const Il2CppType ReadDelegate_t714865915_0_0_0;
extern "C" void DelegatePInvokeWrapper_WriteDelegate_t4270993571();
extern const Il2CppType WriteDelegate_t4270993571_0_0_0;
extern "C" void MonoIOStat_t592533987_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void MonoIOStat_t592533987_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void MonoIOStat_t592533987_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType MonoIOStat_t592533987_0_0_0;
extern "C" void MonoEnumInfo_t3694469084_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void MonoEnumInfo_t3694469084_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void MonoEnumInfo_t3694469084_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType MonoEnumInfo_t3694469084_0_0_0;
extern "C" void CustomAttributeNamedArgument_t287865710_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void CustomAttributeNamedArgument_t287865710_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void CustomAttributeNamedArgument_t287865710_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType CustomAttributeNamedArgument_t287865710_0_0_0;
extern "C" void CustomAttributeTypedArgument_t2723150157_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void CustomAttributeTypedArgument_t2723150157_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void CustomAttributeTypedArgument_t2723150157_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType CustomAttributeTypedArgument_t2723150157_0_0_0;
extern "C" void ILTokenInfo_t2325775114_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void ILTokenInfo_t2325775114_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void ILTokenInfo_t2325775114_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType ILTokenInfo_t2325775114_0_0_0;
extern "C" void MonoEventInfo_t346866618_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void MonoEventInfo_t346866618_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void MonoEventInfo_t346866618_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType MonoEventInfo_t346866618_0_0_0;
extern "C" void MonoMethodInfo_t1248819020_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void MonoMethodInfo_t1248819020_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void MonoMethodInfo_t1248819020_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType MonoMethodInfo_t1248819020_0_0_0;
extern "C" void MonoPropertyInfo_t3087356066_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void MonoPropertyInfo_t3087356066_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void MonoPropertyInfo_t3087356066_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType MonoPropertyInfo_t3087356066_0_0_0;
extern "C" void ParameterModifier_t1461694466_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void ParameterModifier_t1461694466_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void ParameterModifier_t1461694466_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType ParameterModifier_t1461694466_0_0_0;
extern "C" void ResourceCacheItem_t51292791_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void ResourceCacheItem_t51292791_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void ResourceCacheItem_t51292791_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType ResourceCacheItem_t51292791_0_0_0;
extern "C" void ResourceInfo_t2872965302_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void ResourceInfo_t2872965302_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void ResourceInfo_t2872965302_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType ResourceInfo_t2872965302_0_0_0;
extern "C" void DelegatePInvokeWrapper_CrossContextDelegate_t387175271();
extern const Il2CppType CrossContextDelegate_t387175271_0_0_0;
extern "C" void DelegatePInvokeWrapper_CallbackHandler_t3280319253();
extern const Il2CppType CallbackHandler_t3280319253_0_0_0;
extern "C" void SerializationEntry_t648286436_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void SerializationEntry_t648286436_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void SerializationEntry_t648286436_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType SerializationEntry_t648286436_0_0_0;
extern "C" void StreamingContext_t3711869237_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void StreamingContext_t3711869237_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void StreamingContext_t3711869237_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType StreamingContext_t3711869237_0_0_0;
extern "C" void DSAParameters_t1885824122_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void DSAParameters_t1885824122_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void DSAParameters_t1885824122_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType DSAParameters_t1885824122_0_0_0;
extern "C" void RSAParameters_t1728406613_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void RSAParameters_t1728406613_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void RSAParameters_t1728406613_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType RSAParameters_t1728406613_0_0_0;
extern "C" void SecurityFrame_t1422462475_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void SecurityFrame_t1422462475_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void SecurityFrame_t1422462475_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType SecurityFrame_t1422462475_0_0_0;
extern "C" void DelegatePInvokeWrapper_ThreadStart_t1006689297();
extern const Il2CppType ThreadStart_t1006689297_0_0_0;
extern "C" void ValueType_t3640485471_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void ValueType_t3640485471_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void ValueType_t3640485471_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType ValueType_t3640485471_0_0_0;
extern "C" void X509ChainStatus_t133602714_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void X509ChainStatus_t133602714_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void X509ChainStatus_t133602714_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType X509ChainStatus_t133602714_0_0_0;
extern "C" void IntStack_t2189327687_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void IntStack_t2189327687_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void IntStack_t2189327687_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType IntStack_t2189327687_0_0_0;
extern "C" void Interval_t1802865632_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Interval_t1802865632_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Interval_t1802865632_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType Interval_t1802865632_0_0_0;
extern "C" void DelegatePInvokeWrapper_CostDelegate_t1722821004();
extern const Il2CppType CostDelegate_t1722821004_0_0_0;
extern "C" void UriScheme_t722425697_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void UriScheme_t722425697_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void UriScheme_t722425697_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType UriScheme_t722425697_0_0_0;
extern "C" void DelegatePInvokeWrapper_Action_t1264377477();
extern const Il2CppType Action_t1264377477_0_0_0;
extern "C" void DelegatePInvokeWrapper_OnNavMeshPreUpdate_t1580782682();
extern const Il2CppType OnNavMeshPreUpdate_t1580782682_0_0_0;
extern "C" void AnimationCurve_t3046754366_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void AnimationCurve_t3046754366_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void AnimationCurve_t3046754366_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType AnimationCurve_t3046754366_0_0_0;
extern "C" void AnimationEvent_t1536042487_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void AnimationEvent_t1536042487_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void AnimationEvent_t1536042487_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType AnimationEvent_t1536042487_0_0_0;
extern "C" void AnimatorTransitionInfo_t2534804151_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void AnimatorTransitionInfo_t2534804151_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void AnimatorTransitionInfo_t2534804151_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType AnimatorTransitionInfo_t2534804151_0_0_0;
extern "C" void DelegatePInvokeWrapper_LogCallback_t3588208630();
extern const Il2CppType LogCallback_t3588208630_0_0_0;
extern "C" void DelegatePInvokeWrapper_LowMemoryCallback_t4104246196();
extern const Il2CppType LowMemoryCallback_t4104246196_0_0_0;
extern "C" void AssetBundleRequest_t699759206_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void AssetBundleRequest_t699759206_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void AssetBundleRequest_t699759206_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType AssetBundleRequest_t699759206_0_0_0;
extern "C" void AsyncOperation_t1445031843_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void AsyncOperation_t1445031843_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void AsyncOperation_t1445031843_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType AsyncOperation_t1445031843_0_0_0;
extern "C" void DelegatePInvokeWrapper_PCMReaderCallback_t1677636661();
extern const Il2CppType PCMReaderCallback_t1677636661_0_0_0;
extern "C" void DelegatePInvokeWrapper_PCMSetPositionCallback_t1059417452();
extern const Il2CppType PCMSetPositionCallback_t1059417452_0_0_0;
extern "C" void DelegatePInvokeWrapper_AudioConfigurationChangeHandler_t2089929874();
extern const Il2CppType AudioConfigurationChangeHandler_t2089929874_0_0_0;
extern "C" void DelegatePInvokeWrapper_WillRenderCanvases_t3309123499();
extern const Il2CppType WillRenderCanvases_t3309123499_0_0_0;
extern "C" void CharacterInfo_t1228754872_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void CharacterInfo_t1228754872_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void CharacterInfo_t1228754872_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType CharacterInfo_t1228754872_0_0_0;
extern "C" void Collision_t4262080450_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Collision_t4262080450_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Collision_t4262080450_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType Collision_t4262080450_0_0_0;
extern "C" void Collision2D_t2842956331_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Collision2D_t2842956331_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Collision2D_t2842956331_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType Collision2D_t2842956331_0_0_0;
extern "C" void ContactFilter2D_t3805203441_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void ContactFilter2D_t3805203441_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void ContactFilter2D_t3805203441_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType ContactFilter2D_t3805203441_0_0_0;
extern "C" void ControllerColliderHit_t240592346_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void ControllerColliderHit_t240592346_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void ControllerColliderHit_t240592346_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType ControllerColliderHit_t240592346_0_0_0;
extern "C" void Coroutine_t3829159415_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Coroutine_t3829159415_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Coroutine_t3829159415_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType Coroutine_t3829159415_0_0_0;
extern "C" void CullingGroup_t2096318768_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void CullingGroup_t2096318768_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void CullingGroup_t2096318768_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType CullingGroup_t2096318768_0_0_0;
extern "C" void DelegatePInvokeWrapper_StateChanged_t2136737110();
extern const Il2CppType StateChanged_t2136737110_0_0_0;
extern "C" void DelegatePInvokeWrapper_DisplaysUpdatedDelegate_t51287044();
extern const Il2CppType DisplaysUpdatedDelegate_t51287044_0_0_0;
extern "C" void Event_t2956885303_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Event_t2956885303_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Event_t2956885303_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType Event_t2956885303_0_0_0;
extern "C" void DelegatePInvokeWrapper_UnityAction_t3245792599();
extern const Il2CppType UnityAction_t3245792599_0_0_0;
extern "C" void DelegatePInvokeWrapper_FontTextureRebuildCallback_t2467502454();
extern const Il2CppType FontTextureRebuildCallback_t2467502454_0_0_0;
extern "C" void Gradient_t3067099924_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Gradient_t3067099924_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Gradient_t3067099924_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType Gradient_t3067099924_0_0_0;
extern "C" void DelegatePInvokeWrapper_WindowFunction_t3146511083();
extern const Il2CppType WindowFunction_t3146511083_0_0_0;
extern "C" void GUIContent_t3050628031_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void GUIContent_t3050628031_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void GUIContent_t3050628031_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType GUIContent_t3050628031_0_0_0;
extern "C" void DelegatePInvokeWrapper_SkinChangedDelegate_t1143955295();
extern const Il2CppType SkinChangedDelegate_t1143955295_0_0_0;
extern "C" void GUIStyle_t3956901511_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void GUIStyle_t3956901511_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void GUIStyle_t3956901511_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType GUIStyle_t3956901511_0_0_0;
extern "C" void GUIStyleState_t1397964415_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void GUIStyleState_t1397964415_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void GUIStyleState_t1397964415_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType GUIStyleState_t1397964415_0_0_0;
extern "C" void HumanBone_t2465339518_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void HumanBone_t2465339518_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void HumanBone_t2465339518_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType HumanBone_t2465339518_0_0_0;
extern "C" void Internal_DrawMeshMatrixArguments_t3033464300_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Internal_DrawMeshMatrixArguments_t3033464300_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Internal_DrawMeshMatrixArguments_t3033464300_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType Internal_DrawMeshMatrixArguments_t3033464300_0_0_0;
extern "C" void Internal_DrawTextureArguments_t1705718261_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Internal_DrawTextureArguments_t1705718261_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Internal_DrawTextureArguments_t1705718261_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType Internal_DrawTextureArguments_t1705718261_0_0_0;
extern "C" void LightmapData_t2568046604_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void LightmapData_t2568046604_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void LightmapData_t2568046604_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType LightmapData_t2568046604_0_0_0;
extern "C" void DownloadHandler_t2937767557_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void DownloadHandler_t2937767557_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void DownloadHandler_t2937767557_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType DownloadHandler_t2937767557_0_0_0;
extern "C" void DownloadHandlerAudioClip_t3167892435_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void DownloadHandlerAudioClip_t3167892435_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void DownloadHandlerAudioClip_t3167892435_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType DownloadHandlerAudioClip_t3167892435_0_0_0;
extern "C" void Object_t631007953_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Object_t631007953_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Object_t631007953_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType Object_t631007953_0_0_0;
extern "C" void CollisionModule_t1950979710_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void CollisionModule_t1950979710_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void CollisionModule_t1950979710_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType CollisionModule_t1950979710_0_0_0;
extern "C" void ColorBySpeedModule_t3740209408_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void ColorBySpeedModule_t3740209408_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void ColorBySpeedModule_t3740209408_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType ColorBySpeedModule_t3740209408_0_0_0;
extern "C" void ColorOverLifetimeModule_t3039228654_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void ColorOverLifetimeModule_t3039228654_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void ColorOverLifetimeModule_t3039228654_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType ColorOverLifetimeModule_t3039228654_0_0_0;
extern "C" void CustomDataModule_t2135829708_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void CustomDataModule_t2135829708_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void CustomDataModule_t2135829708_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType CustomDataModule_t2135829708_0_0_0;
extern "C" void EmissionModule_t311448003_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void EmissionModule_t311448003_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void EmissionModule_t311448003_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType EmissionModule_t311448003_0_0_0;
extern "C" void EmitParams_t2216423628_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void EmitParams_t2216423628_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void EmitParams_t2216423628_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType EmitParams_t2216423628_0_0_0;
extern "C" void ExternalForcesModule_t1424795933_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void ExternalForcesModule_t1424795933_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void ExternalForcesModule_t1424795933_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType ExternalForcesModule_t1424795933_0_0_0;
extern "C" void ForceOverLifetimeModule_t4029962193_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void ForceOverLifetimeModule_t4029962193_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void ForceOverLifetimeModule_t4029962193_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType ForceOverLifetimeModule_t4029962193_0_0_0;
extern "C" void InheritVelocityModule_t3940044026_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void InheritVelocityModule_t3940044026_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void InheritVelocityModule_t3940044026_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType InheritVelocityModule_t3940044026_0_0_0;
extern "C" void LightsModule_t3616883284_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void LightsModule_t3616883284_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void LightsModule_t3616883284_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType LightsModule_t3616883284_0_0_0;
extern "C" void LimitVelocityOverLifetimeModule_t686589569_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void LimitVelocityOverLifetimeModule_t686589569_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void LimitVelocityOverLifetimeModule_t686589569_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType LimitVelocityOverLifetimeModule_t686589569_0_0_0;
extern "C" void MainModule_t2320046318_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void MainModule_t2320046318_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void MainModule_t2320046318_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType MainModule_t2320046318_0_0_0;
extern "C" void MinMaxCurve_t1067599125_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void MinMaxCurve_t1067599125_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void MinMaxCurve_t1067599125_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType MinMaxCurve_t1067599125_0_0_0;
extern "C" void NoiseModule_t962525627_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void NoiseModule_t962525627_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void NoiseModule_t962525627_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType NoiseModule_t962525627_0_0_0;
extern "C" void RotationBySpeedModule_t3497409583_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void RotationBySpeedModule_t3497409583_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void RotationBySpeedModule_t3497409583_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType RotationBySpeedModule_t3497409583_0_0_0;
extern "C" void RotationOverLifetimeModule_t1164372224_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void RotationOverLifetimeModule_t1164372224_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void RotationOverLifetimeModule_t1164372224_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType RotationOverLifetimeModule_t1164372224_0_0_0;
extern "C" void ShapeModule_t3608330829_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void ShapeModule_t3608330829_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void ShapeModule_t3608330829_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType ShapeModule_t3608330829_0_0_0;
extern "C" void SizeBySpeedModule_t1515126846_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void SizeBySpeedModule_t1515126846_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void SizeBySpeedModule_t1515126846_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType SizeBySpeedModule_t1515126846_0_0_0;
extern "C" void SizeOverLifetimeModule_t1101123803_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void SizeOverLifetimeModule_t1101123803_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void SizeOverLifetimeModule_t1101123803_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType SizeOverLifetimeModule_t1101123803_0_0_0;
extern "C" void SubEmittersModule_t903775760_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void SubEmittersModule_t903775760_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void SubEmittersModule_t903775760_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType SubEmittersModule_t903775760_0_0_0;
extern "C" void TextureSheetAnimationModule_t738696839_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void TextureSheetAnimationModule_t738696839_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void TextureSheetAnimationModule_t738696839_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType TextureSheetAnimationModule_t738696839_0_0_0;
extern "C" void TrailModule_t2282589118_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void TrailModule_t2282589118_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void TrailModule_t2282589118_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType TrailModule_t2282589118_0_0_0;
extern "C" void TriggerModule_t1157986180_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void TriggerModule_t1157986180_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void TriggerModule_t1157986180_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType TriggerModule_t1157986180_0_0_0;
extern "C" void VelocityOverLifetimeModule_t1982232382_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void VelocityOverLifetimeModule_t1982232382_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void VelocityOverLifetimeModule_t1982232382_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType VelocityOverLifetimeModule_t1982232382_0_0_0;
extern "C" void RaycastHit_t1056001966_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void RaycastHit_t1056001966_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void RaycastHit_t1056001966_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType RaycastHit_t1056001966_0_0_0;
extern "C" void RaycastHit2D_t2279581989_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void RaycastHit2D_t2279581989_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void RaycastHit2D_t2279581989_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType RaycastHit2D_t2279581989_0_0_0;
extern "C" void RectOffset_t1369453676_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void RectOffset_t1369453676_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void RectOffset_t1369453676_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType RectOffset_t1369453676_0_0_0;
extern "C" void DelegatePInvokeWrapper_UpdatedEventHandler_t1027848393();
extern const Il2CppType UpdatedEventHandler_t1027848393_0_0_0;
extern "C" void ResourceRequest_t3109103591_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void ResourceRequest_t3109103591_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void ResourceRequest_t3109103591_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType ResourceRequest_t3109103591_0_0_0;
extern "C" void ScriptableObject_t2528358522_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void ScriptableObject_t2528358522_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void ScriptableObject_t2528358522_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType ScriptableObject_t2528358522_0_0_0;
extern "C" void HitInfo_t3229609740_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void HitInfo_t3229609740_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void HitInfo_t3229609740_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType HitInfo_t3229609740_0_0_0;
extern "C" void SkeletonBone_t4134054672_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void SkeletonBone_t4134054672_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void SkeletonBone_t4134054672_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType SkeletonBone_t4134054672_0_0_0;
extern "C" void GcAchievementData_t675222246_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void GcAchievementData_t675222246_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void GcAchievementData_t675222246_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType GcAchievementData_t675222246_0_0_0;
extern "C" void GcAchievementDescriptionData_t643925653_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void GcAchievementDescriptionData_t643925653_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void GcAchievementDescriptionData_t643925653_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType GcAchievementDescriptionData_t643925653_0_0_0;
extern "C" void GcLeaderboard_t4132273028_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void GcLeaderboard_t4132273028_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void GcLeaderboard_t4132273028_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType GcLeaderboard_t4132273028_0_0_0;
extern "C" void GcScoreData_t2125309831_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void GcScoreData_t2125309831_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void GcScoreData_t2125309831_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType GcScoreData_t2125309831_0_0_0;
extern "C" void GcUserProfileData_t2719720026_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void GcUserProfileData_t2719720026_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void GcUserProfileData_t2719720026_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType GcUserProfileData_t2719720026_0_0_0;
extern "C" void TextGenerationSettings_t1351628751_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void TextGenerationSettings_t1351628751_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void TextGenerationSettings_t1351628751_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType TextGenerationSettings_t1351628751_0_0_0;
extern "C" void TextGenerator_t3211863866_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void TextGenerator_t3211863866_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void TextGenerator_t3211863866_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType TextGenerator_t3211863866_0_0_0;
extern "C" void TrackedReference_t1199777556_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void TrackedReference_t1199777556_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void TrackedReference_t1199777556_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType TrackedReference_t1199777556_0_0_0;
extern "C" void WaitForSeconds_t1699091251_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void WaitForSeconds_t1699091251_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void WaitForSeconds_t1699091251_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType WaitForSeconds_t1699091251_0_0_0;
extern "C" void WebCamDevice_t1322781432_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void WebCamDevice_t1322781432_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void WebCamDevice_t1322781432_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType WebCamDevice_t1322781432_0_0_0;
extern "C" void YieldInstruction_t403091072_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void YieldInstruction_t403091072_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void YieldInstruction_t403091072_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType YieldInstruction_t403091072_0_0_0;
extern "C" void RaycastResult_t3360306849_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void RaycastResult_t3360306849_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void RaycastResult_t3360306849_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType RaycastResult_t3360306849_0_0_0;
extern "C" void ColorTween_t809614380_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void ColorTween_t809614380_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void ColorTween_t809614380_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType ColorTween_t809614380_0_0_0;
extern "C" void FloatTween_t1274330004_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void FloatTween_t1274330004_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void FloatTween_t1274330004_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType FloatTween_t1274330004_0_0_0;
extern "C" void Resources_t1597885468_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Resources_t1597885468_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Resources_t1597885468_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType Resources_t1597885468_0_0_0;
extern "C" void DelegatePInvokeWrapper_OnValidateInput_t2355412304();
extern const Il2CppType OnValidateInput_t2355412304_0_0_0;
extern "C" void Navigation_t3049316579_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Navigation_t3049316579_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Navigation_t3049316579_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType Navigation_t3049316579_0_0_0;
extern "C" void SpriteState_t1362986479_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void SpriteState_t1362986479_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void SpriteState_t1362986479_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType SpriteState_t1362986479_0_0_0;
extern "C" void ColorTween_t378116136_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void ColorTween_t378116136_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void ColorTween_t378116136_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType ColorTween_t378116136_0_0_0;
extern "C" void FloatTween_t3783157226_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void FloatTween_t3783157226_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void FloatTween_t3783157226_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType FloatTween_t3783157226_0_0_0;
extern "C" void FontCreationSetting_t628772060_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void FontCreationSetting_t628772060_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void FontCreationSetting_t628772060_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType FontCreationSetting_t628772060_0_0_0;
extern "C" void MaterialReference_t1952344632_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void MaterialReference_t1952344632_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void MaterialReference_t1952344632_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType MaterialReference_t1952344632_0_0_0;
extern "C" void SpriteData_t3048397587_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void SpriteData_t3048397587_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void SpriteData_t3048397587_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType SpriteData_t3048397587_0_0_0;
extern "C" void TMP_CharacterInfo_t3185626797_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void TMP_CharacterInfo_t3185626797_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void TMP_CharacterInfo_t3185626797_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType TMP_CharacterInfo_t3185626797_0_0_0;
extern "C" void Resources_t2155109485_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Resources_t2155109485_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Resources_t2155109485_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType Resources_t2155109485_0_0_0;
extern "C" void TMP_FontWeights_t916301067_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void TMP_FontWeights_t916301067_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void TMP_FontWeights_t916301067_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType TMP_FontWeights_t916301067_0_0_0;
extern "C" void DelegatePInvokeWrapper_OnValidateInput_t373909109();
extern const Il2CppType OnValidateInput_t373909109_0_0_0;
extern "C" void TMP_LinkInfo_t1092083476_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void TMP_LinkInfo_t1092083476_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void TMP_LinkInfo_t1092083476_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType TMP_LinkInfo_t1092083476_0_0_0;
extern "C" void TMP_MeshInfo_t2771747634_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void TMP_MeshInfo_t2771747634_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void TMP_MeshInfo_t2771747634_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType TMP_MeshInfo_t2771747634_0_0_0;
extern "C" void TMP_WordInfo_t3331066303_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void TMP_WordInfo_t3331066303_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void TMP_WordInfo_t3331066303_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType TMP_WordInfo_t3331066303_0_0_0;
extern "C" void WordWrapState_t341939652_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void WordWrapState_t341939652_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void WordWrapState_t341939652_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType WordWrapState_t341939652_0_0_0;
extern "C" void Frame_t1062777224_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Frame_t1062777224_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Frame_t1062777224_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType Frame_t1062777224_0_0_0;
extern "C" void SimulatedTarget_t1564170948_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void SimulatedTarget_t1564170948_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void SimulatedTarget_t1564170948_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType SimulatedTarget_t1564170948_0_0_0;
extern "C" void Data_t434873072_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Data_t434873072_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Data_t434873072_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType Data_t434873072_0_0_0;
extern "C" void HighlightingPreset_t635619791_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void HighlightingPreset_t635619791_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void HighlightingPreset_t635619791_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType HighlightingPreset_t635619791_0_0_0;
extern "C" void DelegatePInvokeWrapper_ApplyTween_t3327999347();
extern const Il2CppType ApplyTween_t3327999347_0_0_0;
extern "C" void DelegatePInvokeWrapper_EasingFunction_t2767217938();
extern const Il2CppType EasingFunction_t2767217938_0_0_0;
extern "C" void InputFrameData_t2573966900_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void InputFrameData_t2573966900_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void InputFrameData_t2573966900_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType InputFrameData_t2573966900_0_0_0;
extern "C" void ImageRotation_t1158944265_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void ImageRotation_t1158944265_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void ImageRotation_t1158944265_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const Il2CppType ImageRotation_t1158944265_0_0_0;
extern "C" void DelegatePInvokeWrapper_FieldNotFound_t2620845099();
extern const Il2CppType FieldNotFound_t2620845099_0_0_0;
extern Il2CppInteropData g_Il2CppInteropData[147] = 
{
	{ NULL, Context_t1744531130_marshal_pinvoke, Context_t1744531130_marshal_pinvoke_back, Context_t1744531130_marshal_pinvoke_cleanup, NULL, NULL, &Context_t1744531130_0_0_0 } /* Mono.Globalization.Unicode.SimpleCollator/Context */,
	{ NULL, Escape_t3294788190_marshal_pinvoke, Escape_t3294788190_marshal_pinvoke_back, Escape_t3294788190_marshal_pinvoke_cleanup, NULL, NULL, &Escape_t3294788190_0_0_0 } /* Mono.Globalization.Unicode.SimpleCollator/Escape */,
	{ NULL, PreviousInfo_t2148130204_marshal_pinvoke, PreviousInfo_t2148130204_marshal_pinvoke_back, PreviousInfo_t2148130204_marshal_pinvoke_cleanup, NULL, NULL, &PreviousInfo_t2148130204_0_0_0 } /* Mono.Globalization.Unicode.SimpleCollator/PreviousInfo */,
	{ DelegatePInvokeWrapper_AppDomainInitializer_t682969308, NULL, NULL, NULL, NULL, NULL, &AppDomainInitializer_t682969308_0_0_0 } /* System.AppDomainInitializer */,
	{ DelegatePInvokeWrapper_Swapper_t2822380397, NULL, NULL, NULL, NULL, NULL, &Swapper_t2822380397_0_0_0 } /* System.Array/Swapper */,
	{ NULL, DictionaryEntry_t3123975638_marshal_pinvoke, DictionaryEntry_t3123975638_marshal_pinvoke_back, DictionaryEntry_t3123975638_marshal_pinvoke_cleanup, NULL, NULL, &DictionaryEntry_t3123975638_0_0_0 } /* System.Collections.DictionaryEntry */,
	{ NULL, Slot_t3975888750_marshal_pinvoke, Slot_t3975888750_marshal_pinvoke_back, Slot_t3975888750_marshal_pinvoke_cleanup, NULL, NULL, &Slot_t3975888750_0_0_0 } /* System.Collections.Hashtable/Slot */,
	{ NULL, Slot_t384495010_marshal_pinvoke, Slot_t384495010_marshal_pinvoke_back, Slot_t384495010_marshal_pinvoke_cleanup, NULL, NULL, &Slot_t384495010_0_0_0 } /* System.Collections.SortedList/Slot */,
	{ NULL, Enum_t4135868527_marshal_pinvoke, Enum_t4135868527_marshal_pinvoke_back, Enum_t4135868527_marshal_pinvoke_cleanup, NULL, NULL, &Enum_t4135868527_0_0_0 } /* System.Enum */,
	{ DelegatePInvokeWrapper_ReadDelegate_t714865915, NULL, NULL, NULL, NULL, NULL, &ReadDelegate_t714865915_0_0_0 } /* System.IO.FileStream/ReadDelegate */,
	{ DelegatePInvokeWrapper_WriteDelegate_t4270993571, NULL, NULL, NULL, NULL, NULL, &WriteDelegate_t4270993571_0_0_0 } /* System.IO.FileStream/WriteDelegate */,
	{ NULL, MonoIOStat_t592533987_marshal_pinvoke, MonoIOStat_t592533987_marshal_pinvoke_back, MonoIOStat_t592533987_marshal_pinvoke_cleanup, NULL, NULL, &MonoIOStat_t592533987_0_0_0 } /* System.IO.MonoIOStat */,
	{ NULL, MonoEnumInfo_t3694469084_marshal_pinvoke, MonoEnumInfo_t3694469084_marshal_pinvoke_back, MonoEnumInfo_t3694469084_marshal_pinvoke_cleanup, NULL, NULL, &MonoEnumInfo_t3694469084_0_0_0 } /* System.MonoEnumInfo */,
	{ NULL, CustomAttributeNamedArgument_t287865710_marshal_pinvoke, CustomAttributeNamedArgument_t287865710_marshal_pinvoke_back, CustomAttributeNamedArgument_t287865710_marshal_pinvoke_cleanup, NULL, NULL, &CustomAttributeNamedArgument_t287865710_0_0_0 } /* System.Reflection.CustomAttributeNamedArgument */,
	{ NULL, CustomAttributeTypedArgument_t2723150157_marshal_pinvoke, CustomAttributeTypedArgument_t2723150157_marshal_pinvoke_back, CustomAttributeTypedArgument_t2723150157_marshal_pinvoke_cleanup, NULL, NULL, &CustomAttributeTypedArgument_t2723150157_0_0_0 } /* System.Reflection.CustomAttributeTypedArgument */,
	{ NULL, ILTokenInfo_t2325775114_marshal_pinvoke, ILTokenInfo_t2325775114_marshal_pinvoke_back, ILTokenInfo_t2325775114_marshal_pinvoke_cleanup, NULL, NULL, &ILTokenInfo_t2325775114_0_0_0 } /* System.Reflection.Emit.ILTokenInfo */,
	{ NULL, MonoEventInfo_t346866618_marshal_pinvoke, MonoEventInfo_t346866618_marshal_pinvoke_back, MonoEventInfo_t346866618_marshal_pinvoke_cleanup, NULL, NULL, &MonoEventInfo_t346866618_0_0_0 } /* System.Reflection.MonoEventInfo */,
	{ NULL, MonoMethodInfo_t1248819020_marshal_pinvoke, MonoMethodInfo_t1248819020_marshal_pinvoke_back, MonoMethodInfo_t1248819020_marshal_pinvoke_cleanup, NULL, NULL, &MonoMethodInfo_t1248819020_0_0_0 } /* System.Reflection.MonoMethodInfo */,
	{ NULL, MonoPropertyInfo_t3087356066_marshal_pinvoke, MonoPropertyInfo_t3087356066_marshal_pinvoke_back, MonoPropertyInfo_t3087356066_marshal_pinvoke_cleanup, NULL, NULL, &MonoPropertyInfo_t3087356066_0_0_0 } /* System.Reflection.MonoPropertyInfo */,
	{ NULL, ParameterModifier_t1461694466_marshal_pinvoke, ParameterModifier_t1461694466_marshal_pinvoke_back, ParameterModifier_t1461694466_marshal_pinvoke_cleanup, NULL, NULL, &ParameterModifier_t1461694466_0_0_0 } /* System.Reflection.ParameterModifier */,
	{ NULL, ResourceCacheItem_t51292791_marshal_pinvoke, ResourceCacheItem_t51292791_marshal_pinvoke_back, ResourceCacheItem_t51292791_marshal_pinvoke_cleanup, NULL, NULL, &ResourceCacheItem_t51292791_0_0_0 } /* System.Resources.ResourceReader/ResourceCacheItem */,
	{ NULL, ResourceInfo_t2872965302_marshal_pinvoke, ResourceInfo_t2872965302_marshal_pinvoke_back, ResourceInfo_t2872965302_marshal_pinvoke_cleanup, NULL, NULL, &ResourceInfo_t2872965302_0_0_0 } /* System.Resources.ResourceReader/ResourceInfo */,
	{ DelegatePInvokeWrapper_CrossContextDelegate_t387175271, NULL, NULL, NULL, NULL, NULL, &CrossContextDelegate_t387175271_0_0_0 } /* System.Runtime.Remoting.Contexts.CrossContextDelegate */,
	{ DelegatePInvokeWrapper_CallbackHandler_t3280319253, NULL, NULL, NULL, NULL, NULL, &CallbackHandler_t3280319253_0_0_0 } /* System.Runtime.Serialization.SerializationCallbacks/CallbackHandler */,
	{ NULL, SerializationEntry_t648286436_marshal_pinvoke, SerializationEntry_t648286436_marshal_pinvoke_back, SerializationEntry_t648286436_marshal_pinvoke_cleanup, NULL, NULL, &SerializationEntry_t648286436_0_0_0 } /* System.Runtime.Serialization.SerializationEntry */,
	{ NULL, StreamingContext_t3711869237_marshal_pinvoke, StreamingContext_t3711869237_marshal_pinvoke_back, StreamingContext_t3711869237_marshal_pinvoke_cleanup, NULL, NULL, &StreamingContext_t3711869237_0_0_0 } /* System.Runtime.Serialization.StreamingContext */,
	{ NULL, DSAParameters_t1885824122_marshal_pinvoke, DSAParameters_t1885824122_marshal_pinvoke_back, DSAParameters_t1885824122_marshal_pinvoke_cleanup, NULL, NULL, &DSAParameters_t1885824122_0_0_0 } /* System.Security.Cryptography.DSAParameters */,
	{ NULL, RSAParameters_t1728406613_marshal_pinvoke, RSAParameters_t1728406613_marshal_pinvoke_back, RSAParameters_t1728406613_marshal_pinvoke_cleanup, NULL, NULL, &RSAParameters_t1728406613_0_0_0 } /* System.Security.Cryptography.RSAParameters */,
	{ NULL, SecurityFrame_t1422462475_marshal_pinvoke, SecurityFrame_t1422462475_marshal_pinvoke_back, SecurityFrame_t1422462475_marshal_pinvoke_cleanup, NULL, NULL, &SecurityFrame_t1422462475_0_0_0 } /* System.Security.SecurityFrame */,
	{ DelegatePInvokeWrapper_ThreadStart_t1006689297, NULL, NULL, NULL, NULL, NULL, &ThreadStart_t1006689297_0_0_0 } /* System.Threading.ThreadStart */,
	{ NULL, ValueType_t3640485471_marshal_pinvoke, ValueType_t3640485471_marshal_pinvoke_back, ValueType_t3640485471_marshal_pinvoke_cleanup, NULL, NULL, &ValueType_t3640485471_0_0_0 } /* System.ValueType */,
	{ NULL, X509ChainStatus_t133602714_marshal_pinvoke, X509ChainStatus_t133602714_marshal_pinvoke_back, X509ChainStatus_t133602714_marshal_pinvoke_cleanup, NULL, NULL, &X509ChainStatus_t133602714_0_0_0 } /* System.Security.Cryptography.X509Certificates.X509ChainStatus */,
	{ NULL, IntStack_t2189327687_marshal_pinvoke, IntStack_t2189327687_marshal_pinvoke_back, IntStack_t2189327687_marshal_pinvoke_cleanup, NULL, NULL, &IntStack_t2189327687_0_0_0 } /* System.Text.RegularExpressions.Interpreter/IntStack */,
	{ NULL, Interval_t1802865632_marshal_pinvoke, Interval_t1802865632_marshal_pinvoke_back, Interval_t1802865632_marshal_pinvoke_cleanup, NULL, NULL, &Interval_t1802865632_0_0_0 } /* System.Text.RegularExpressions.Interval */,
	{ DelegatePInvokeWrapper_CostDelegate_t1722821004, NULL, NULL, NULL, NULL, NULL, &CostDelegate_t1722821004_0_0_0 } /* System.Text.RegularExpressions.IntervalCollection/CostDelegate */,
	{ NULL, UriScheme_t722425697_marshal_pinvoke, UriScheme_t722425697_marshal_pinvoke_back, UriScheme_t722425697_marshal_pinvoke_cleanup, NULL, NULL, &UriScheme_t722425697_0_0_0 } /* System.Uri/UriScheme */,
	{ DelegatePInvokeWrapper_Action_t1264377477, NULL, NULL, NULL, NULL, NULL, &Action_t1264377477_0_0_0 } /* System.Action */,
	{ DelegatePInvokeWrapper_OnNavMeshPreUpdate_t1580782682, NULL, NULL, NULL, NULL, NULL, &OnNavMeshPreUpdate_t1580782682_0_0_0 } /* UnityEngine.AI.NavMesh/OnNavMeshPreUpdate */,
	{ NULL, AnimationCurve_t3046754366_marshal_pinvoke, AnimationCurve_t3046754366_marshal_pinvoke_back, AnimationCurve_t3046754366_marshal_pinvoke_cleanup, NULL, NULL, &AnimationCurve_t3046754366_0_0_0 } /* UnityEngine.AnimationCurve */,
	{ NULL, AnimationEvent_t1536042487_marshal_pinvoke, AnimationEvent_t1536042487_marshal_pinvoke_back, AnimationEvent_t1536042487_marshal_pinvoke_cleanup, NULL, NULL, &AnimationEvent_t1536042487_0_0_0 } /* UnityEngine.AnimationEvent */,
	{ NULL, AnimatorTransitionInfo_t2534804151_marshal_pinvoke, AnimatorTransitionInfo_t2534804151_marshal_pinvoke_back, AnimatorTransitionInfo_t2534804151_marshal_pinvoke_cleanup, NULL, NULL, &AnimatorTransitionInfo_t2534804151_0_0_0 } /* UnityEngine.AnimatorTransitionInfo */,
	{ DelegatePInvokeWrapper_LogCallback_t3588208630, NULL, NULL, NULL, NULL, NULL, &LogCallback_t3588208630_0_0_0 } /* UnityEngine.Application/LogCallback */,
	{ DelegatePInvokeWrapper_LowMemoryCallback_t4104246196, NULL, NULL, NULL, NULL, NULL, &LowMemoryCallback_t4104246196_0_0_0 } /* UnityEngine.Application/LowMemoryCallback */,
	{ NULL, AssetBundleRequest_t699759206_marshal_pinvoke, AssetBundleRequest_t699759206_marshal_pinvoke_back, AssetBundleRequest_t699759206_marshal_pinvoke_cleanup, NULL, NULL, &AssetBundleRequest_t699759206_0_0_0 } /* UnityEngine.AssetBundleRequest */,
	{ NULL, AsyncOperation_t1445031843_marshal_pinvoke, AsyncOperation_t1445031843_marshal_pinvoke_back, AsyncOperation_t1445031843_marshal_pinvoke_cleanup, NULL, NULL, &AsyncOperation_t1445031843_0_0_0 } /* UnityEngine.AsyncOperation */,
	{ DelegatePInvokeWrapper_PCMReaderCallback_t1677636661, NULL, NULL, NULL, NULL, NULL, &PCMReaderCallback_t1677636661_0_0_0 } /* UnityEngine.AudioClip/PCMReaderCallback */,
	{ DelegatePInvokeWrapper_PCMSetPositionCallback_t1059417452, NULL, NULL, NULL, NULL, NULL, &PCMSetPositionCallback_t1059417452_0_0_0 } /* UnityEngine.AudioClip/PCMSetPositionCallback */,
	{ DelegatePInvokeWrapper_AudioConfigurationChangeHandler_t2089929874, NULL, NULL, NULL, NULL, NULL, &AudioConfigurationChangeHandler_t2089929874_0_0_0 } /* UnityEngine.AudioSettings/AudioConfigurationChangeHandler */,
	{ DelegatePInvokeWrapper_WillRenderCanvases_t3309123499, NULL, NULL, NULL, NULL, NULL, &WillRenderCanvases_t3309123499_0_0_0 } /* UnityEngine.Canvas/WillRenderCanvases */,
	{ NULL, CharacterInfo_t1228754872_marshal_pinvoke, CharacterInfo_t1228754872_marshal_pinvoke_back, CharacterInfo_t1228754872_marshal_pinvoke_cleanup, NULL, NULL, &CharacterInfo_t1228754872_0_0_0 } /* UnityEngine.CharacterInfo */,
	{ NULL, Collision_t4262080450_marshal_pinvoke, Collision_t4262080450_marshal_pinvoke_back, Collision_t4262080450_marshal_pinvoke_cleanup, NULL, NULL, &Collision_t4262080450_0_0_0 } /* UnityEngine.Collision */,
	{ NULL, Collision2D_t2842956331_marshal_pinvoke, Collision2D_t2842956331_marshal_pinvoke_back, Collision2D_t2842956331_marshal_pinvoke_cleanup, NULL, NULL, &Collision2D_t2842956331_0_0_0 } /* UnityEngine.Collision2D */,
	{ NULL, ContactFilter2D_t3805203441_marshal_pinvoke, ContactFilter2D_t3805203441_marshal_pinvoke_back, ContactFilter2D_t3805203441_marshal_pinvoke_cleanup, NULL, NULL, &ContactFilter2D_t3805203441_0_0_0 } /* UnityEngine.ContactFilter2D */,
	{ NULL, ControllerColliderHit_t240592346_marshal_pinvoke, ControllerColliderHit_t240592346_marshal_pinvoke_back, ControllerColliderHit_t240592346_marshal_pinvoke_cleanup, NULL, NULL, &ControllerColliderHit_t240592346_0_0_0 } /* UnityEngine.ControllerColliderHit */,
	{ NULL, Coroutine_t3829159415_marshal_pinvoke, Coroutine_t3829159415_marshal_pinvoke_back, Coroutine_t3829159415_marshal_pinvoke_cleanup, NULL, NULL, &Coroutine_t3829159415_0_0_0 } /* UnityEngine.Coroutine */,
	{ NULL, CullingGroup_t2096318768_marshal_pinvoke, CullingGroup_t2096318768_marshal_pinvoke_back, CullingGroup_t2096318768_marshal_pinvoke_cleanup, NULL, NULL, &CullingGroup_t2096318768_0_0_0 } /* UnityEngine.CullingGroup */,
	{ DelegatePInvokeWrapper_StateChanged_t2136737110, NULL, NULL, NULL, NULL, NULL, &StateChanged_t2136737110_0_0_0 } /* UnityEngine.CullingGroup/StateChanged */,
	{ DelegatePInvokeWrapper_DisplaysUpdatedDelegate_t51287044, NULL, NULL, NULL, NULL, NULL, &DisplaysUpdatedDelegate_t51287044_0_0_0 } /* UnityEngine.Display/DisplaysUpdatedDelegate */,
	{ NULL, Event_t2956885303_marshal_pinvoke, Event_t2956885303_marshal_pinvoke_back, Event_t2956885303_marshal_pinvoke_cleanup, NULL, NULL, &Event_t2956885303_0_0_0 } /* UnityEngine.Event */,
	{ DelegatePInvokeWrapper_UnityAction_t3245792599, NULL, NULL, NULL, NULL, NULL, &UnityAction_t3245792599_0_0_0 } /* UnityEngine.Events.UnityAction */,
	{ DelegatePInvokeWrapper_FontTextureRebuildCallback_t2467502454, NULL, NULL, NULL, NULL, NULL, &FontTextureRebuildCallback_t2467502454_0_0_0 } /* UnityEngine.Font/FontTextureRebuildCallback */,
	{ NULL, Gradient_t3067099924_marshal_pinvoke, Gradient_t3067099924_marshal_pinvoke_back, Gradient_t3067099924_marshal_pinvoke_cleanup, NULL, NULL, &Gradient_t3067099924_0_0_0 } /* UnityEngine.Gradient */,
	{ DelegatePInvokeWrapper_WindowFunction_t3146511083, NULL, NULL, NULL, NULL, NULL, &WindowFunction_t3146511083_0_0_0 } /* UnityEngine.GUI/WindowFunction */,
	{ NULL, GUIContent_t3050628031_marshal_pinvoke, GUIContent_t3050628031_marshal_pinvoke_back, GUIContent_t3050628031_marshal_pinvoke_cleanup, NULL, NULL, &GUIContent_t3050628031_0_0_0 } /* UnityEngine.GUIContent */,
	{ DelegatePInvokeWrapper_SkinChangedDelegate_t1143955295, NULL, NULL, NULL, NULL, NULL, &SkinChangedDelegate_t1143955295_0_0_0 } /* UnityEngine.GUISkin/SkinChangedDelegate */,
	{ NULL, GUIStyle_t3956901511_marshal_pinvoke, GUIStyle_t3956901511_marshal_pinvoke_back, GUIStyle_t3956901511_marshal_pinvoke_cleanup, NULL, NULL, &GUIStyle_t3956901511_0_0_0 } /* UnityEngine.GUIStyle */,
	{ NULL, GUIStyleState_t1397964415_marshal_pinvoke, GUIStyleState_t1397964415_marshal_pinvoke_back, GUIStyleState_t1397964415_marshal_pinvoke_cleanup, NULL, NULL, &GUIStyleState_t1397964415_0_0_0 } /* UnityEngine.GUIStyleState */,
	{ NULL, HumanBone_t2465339518_marshal_pinvoke, HumanBone_t2465339518_marshal_pinvoke_back, HumanBone_t2465339518_marshal_pinvoke_cleanup, NULL, NULL, &HumanBone_t2465339518_0_0_0 } /* UnityEngine.HumanBone */,
	{ NULL, Internal_DrawMeshMatrixArguments_t3033464300_marshal_pinvoke, Internal_DrawMeshMatrixArguments_t3033464300_marshal_pinvoke_back, Internal_DrawMeshMatrixArguments_t3033464300_marshal_pinvoke_cleanup, NULL, NULL, &Internal_DrawMeshMatrixArguments_t3033464300_0_0_0 } /* UnityEngine.Internal_DrawMeshMatrixArguments */,
	{ NULL, Internal_DrawTextureArguments_t1705718261_marshal_pinvoke, Internal_DrawTextureArguments_t1705718261_marshal_pinvoke_back, Internal_DrawTextureArguments_t1705718261_marshal_pinvoke_cleanup, NULL, NULL, &Internal_DrawTextureArguments_t1705718261_0_0_0 } /* UnityEngine.Internal_DrawTextureArguments */,
	{ NULL, LightmapData_t2568046604_marshal_pinvoke, LightmapData_t2568046604_marshal_pinvoke_back, LightmapData_t2568046604_marshal_pinvoke_cleanup, NULL, NULL, &LightmapData_t2568046604_0_0_0 } /* UnityEngine.LightmapData */,
	{ NULL, DownloadHandler_t2937767557_marshal_pinvoke, DownloadHandler_t2937767557_marshal_pinvoke_back, DownloadHandler_t2937767557_marshal_pinvoke_cleanup, NULL, NULL, &DownloadHandler_t2937767557_0_0_0 } /* UnityEngine.Networking.DownloadHandler */,
	{ NULL, DownloadHandlerAudioClip_t3167892435_marshal_pinvoke, DownloadHandlerAudioClip_t3167892435_marshal_pinvoke_back, DownloadHandlerAudioClip_t3167892435_marshal_pinvoke_cleanup, NULL, NULL, &DownloadHandlerAudioClip_t3167892435_0_0_0 } /* UnityEngine.Networking.DownloadHandlerAudioClip */,
	{ NULL, Object_t631007953_marshal_pinvoke, Object_t631007953_marshal_pinvoke_back, Object_t631007953_marshal_pinvoke_cleanup, NULL, NULL, &Object_t631007953_0_0_0 } /* UnityEngine.Object */,
	{ NULL, CollisionModule_t1950979710_marshal_pinvoke, CollisionModule_t1950979710_marshal_pinvoke_back, CollisionModule_t1950979710_marshal_pinvoke_cleanup, NULL, NULL, &CollisionModule_t1950979710_0_0_0 } /* UnityEngine.ParticleSystem/CollisionModule */,
	{ NULL, ColorBySpeedModule_t3740209408_marshal_pinvoke, ColorBySpeedModule_t3740209408_marshal_pinvoke_back, ColorBySpeedModule_t3740209408_marshal_pinvoke_cleanup, NULL, NULL, &ColorBySpeedModule_t3740209408_0_0_0 } /* UnityEngine.ParticleSystem/ColorBySpeedModule */,
	{ NULL, ColorOverLifetimeModule_t3039228654_marshal_pinvoke, ColorOverLifetimeModule_t3039228654_marshal_pinvoke_back, ColorOverLifetimeModule_t3039228654_marshal_pinvoke_cleanup, NULL, NULL, &ColorOverLifetimeModule_t3039228654_0_0_0 } /* UnityEngine.ParticleSystem/ColorOverLifetimeModule */,
	{ NULL, CustomDataModule_t2135829708_marshal_pinvoke, CustomDataModule_t2135829708_marshal_pinvoke_back, CustomDataModule_t2135829708_marshal_pinvoke_cleanup, NULL, NULL, &CustomDataModule_t2135829708_0_0_0 } /* UnityEngine.ParticleSystem/CustomDataModule */,
	{ NULL, EmissionModule_t311448003_marshal_pinvoke, EmissionModule_t311448003_marshal_pinvoke_back, EmissionModule_t311448003_marshal_pinvoke_cleanup, NULL, NULL, &EmissionModule_t311448003_0_0_0 } /* UnityEngine.ParticleSystem/EmissionModule */,
	{ NULL, EmitParams_t2216423628_marshal_pinvoke, EmitParams_t2216423628_marshal_pinvoke_back, EmitParams_t2216423628_marshal_pinvoke_cleanup, NULL, NULL, &EmitParams_t2216423628_0_0_0 } /* UnityEngine.ParticleSystem/EmitParams */,
	{ NULL, ExternalForcesModule_t1424795933_marshal_pinvoke, ExternalForcesModule_t1424795933_marshal_pinvoke_back, ExternalForcesModule_t1424795933_marshal_pinvoke_cleanup, NULL, NULL, &ExternalForcesModule_t1424795933_0_0_0 } /* UnityEngine.ParticleSystem/ExternalForcesModule */,
	{ NULL, ForceOverLifetimeModule_t4029962193_marshal_pinvoke, ForceOverLifetimeModule_t4029962193_marshal_pinvoke_back, ForceOverLifetimeModule_t4029962193_marshal_pinvoke_cleanup, NULL, NULL, &ForceOverLifetimeModule_t4029962193_0_0_0 } /* UnityEngine.ParticleSystem/ForceOverLifetimeModule */,
	{ NULL, InheritVelocityModule_t3940044026_marshal_pinvoke, InheritVelocityModule_t3940044026_marshal_pinvoke_back, InheritVelocityModule_t3940044026_marshal_pinvoke_cleanup, NULL, NULL, &InheritVelocityModule_t3940044026_0_0_0 } /* UnityEngine.ParticleSystem/InheritVelocityModule */,
	{ NULL, LightsModule_t3616883284_marshal_pinvoke, LightsModule_t3616883284_marshal_pinvoke_back, LightsModule_t3616883284_marshal_pinvoke_cleanup, NULL, NULL, &LightsModule_t3616883284_0_0_0 } /* UnityEngine.ParticleSystem/LightsModule */,
	{ NULL, LimitVelocityOverLifetimeModule_t686589569_marshal_pinvoke, LimitVelocityOverLifetimeModule_t686589569_marshal_pinvoke_back, LimitVelocityOverLifetimeModule_t686589569_marshal_pinvoke_cleanup, NULL, NULL, &LimitVelocityOverLifetimeModule_t686589569_0_0_0 } /* UnityEngine.ParticleSystem/LimitVelocityOverLifetimeModule */,
	{ NULL, MainModule_t2320046318_marshal_pinvoke, MainModule_t2320046318_marshal_pinvoke_back, MainModule_t2320046318_marshal_pinvoke_cleanup, NULL, NULL, &MainModule_t2320046318_0_0_0 } /* UnityEngine.ParticleSystem/MainModule */,
	{ NULL, MinMaxCurve_t1067599125_marshal_pinvoke, MinMaxCurve_t1067599125_marshal_pinvoke_back, MinMaxCurve_t1067599125_marshal_pinvoke_cleanup, NULL, NULL, &MinMaxCurve_t1067599125_0_0_0 } /* UnityEngine.ParticleSystem/MinMaxCurve */,
	{ NULL, NoiseModule_t962525627_marshal_pinvoke, NoiseModule_t962525627_marshal_pinvoke_back, NoiseModule_t962525627_marshal_pinvoke_cleanup, NULL, NULL, &NoiseModule_t962525627_0_0_0 } /* UnityEngine.ParticleSystem/NoiseModule */,
	{ NULL, RotationBySpeedModule_t3497409583_marshal_pinvoke, RotationBySpeedModule_t3497409583_marshal_pinvoke_back, RotationBySpeedModule_t3497409583_marshal_pinvoke_cleanup, NULL, NULL, &RotationBySpeedModule_t3497409583_0_0_0 } /* UnityEngine.ParticleSystem/RotationBySpeedModule */,
	{ NULL, RotationOverLifetimeModule_t1164372224_marshal_pinvoke, RotationOverLifetimeModule_t1164372224_marshal_pinvoke_back, RotationOverLifetimeModule_t1164372224_marshal_pinvoke_cleanup, NULL, NULL, &RotationOverLifetimeModule_t1164372224_0_0_0 } /* UnityEngine.ParticleSystem/RotationOverLifetimeModule */,
	{ NULL, ShapeModule_t3608330829_marshal_pinvoke, ShapeModule_t3608330829_marshal_pinvoke_back, ShapeModule_t3608330829_marshal_pinvoke_cleanup, NULL, NULL, &ShapeModule_t3608330829_0_0_0 } /* UnityEngine.ParticleSystem/ShapeModule */,
	{ NULL, SizeBySpeedModule_t1515126846_marshal_pinvoke, SizeBySpeedModule_t1515126846_marshal_pinvoke_back, SizeBySpeedModule_t1515126846_marshal_pinvoke_cleanup, NULL, NULL, &SizeBySpeedModule_t1515126846_0_0_0 } /* UnityEngine.ParticleSystem/SizeBySpeedModule */,
	{ NULL, SizeOverLifetimeModule_t1101123803_marshal_pinvoke, SizeOverLifetimeModule_t1101123803_marshal_pinvoke_back, SizeOverLifetimeModule_t1101123803_marshal_pinvoke_cleanup, NULL, NULL, &SizeOverLifetimeModule_t1101123803_0_0_0 } /* UnityEngine.ParticleSystem/SizeOverLifetimeModule */,
	{ NULL, SubEmittersModule_t903775760_marshal_pinvoke, SubEmittersModule_t903775760_marshal_pinvoke_back, SubEmittersModule_t903775760_marshal_pinvoke_cleanup, NULL, NULL, &SubEmittersModule_t903775760_0_0_0 } /* UnityEngine.ParticleSystem/SubEmittersModule */,
	{ NULL, TextureSheetAnimationModule_t738696839_marshal_pinvoke, TextureSheetAnimationModule_t738696839_marshal_pinvoke_back, TextureSheetAnimationModule_t738696839_marshal_pinvoke_cleanup, NULL, NULL, &TextureSheetAnimationModule_t738696839_0_0_0 } /* UnityEngine.ParticleSystem/TextureSheetAnimationModule */,
	{ NULL, TrailModule_t2282589118_marshal_pinvoke, TrailModule_t2282589118_marshal_pinvoke_back, TrailModule_t2282589118_marshal_pinvoke_cleanup, NULL, NULL, &TrailModule_t2282589118_0_0_0 } /* UnityEngine.ParticleSystem/TrailModule */,
	{ NULL, TriggerModule_t1157986180_marshal_pinvoke, TriggerModule_t1157986180_marshal_pinvoke_back, TriggerModule_t1157986180_marshal_pinvoke_cleanup, NULL, NULL, &TriggerModule_t1157986180_0_0_0 } /* UnityEngine.ParticleSystem/TriggerModule */,
	{ NULL, VelocityOverLifetimeModule_t1982232382_marshal_pinvoke, VelocityOverLifetimeModule_t1982232382_marshal_pinvoke_back, VelocityOverLifetimeModule_t1982232382_marshal_pinvoke_cleanup, NULL, NULL, &VelocityOverLifetimeModule_t1982232382_0_0_0 } /* UnityEngine.ParticleSystem/VelocityOverLifetimeModule */,
	{ NULL, RaycastHit_t1056001966_marshal_pinvoke, RaycastHit_t1056001966_marshal_pinvoke_back, RaycastHit_t1056001966_marshal_pinvoke_cleanup, NULL, NULL, &RaycastHit_t1056001966_0_0_0 } /* UnityEngine.RaycastHit */,
	{ NULL, RaycastHit2D_t2279581989_marshal_pinvoke, RaycastHit2D_t2279581989_marshal_pinvoke_back, RaycastHit2D_t2279581989_marshal_pinvoke_cleanup, NULL, NULL, &RaycastHit2D_t2279581989_0_0_0 } /* UnityEngine.RaycastHit2D */,
	{ NULL, RectOffset_t1369453676_marshal_pinvoke, RectOffset_t1369453676_marshal_pinvoke_back, RectOffset_t1369453676_marshal_pinvoke_cleanup, NULL, NULL, &RectOffset_t1369453676_0_0_0 } /* UnityEngine.RectOffset */,
	{ DelegatePInvokeWrapper_UpdatedEventHandler_t1027848393, NULL, NULL, NULL, NULL, NULL, &UpdatedEventHandler_t1027848393_0_0_0 } /* UnityEngine.RemoteSettings/UpdatedEventHandler */,
	{ NULL, ResourceRequest_t3109103591_marshal_pinvoke, ResourceRequest_t3109103591_marshal_pinvoke_back, ResourceRequest_t3109103591_marshal_pinvoke_cleanup, NULL, NULL, &ResourceRequest_t3109103591_0_0_0 } /* UnityEngine.ResourceRequest */,
	{ NULL, ScriptableObject_t2528358522_marshal_pinvoke, ScriptableObject_t2528358522_marshal_pinvoke_back, ScriptableObject_t2528358522_marshal_pinvoke_cleanup, NULL, NULL, &ScriptableObject_t2528358522_0_0_0 } /* UnityEngine.ScriptableObject */,
	{ NULL, HitInfo_t3229609740_marshal_pinvoke, HitInfo_t3229609740_marshal_pinvoke_back, HitInfo_t3229609740_marshal_pinvoke_cleanup, NULL, NULL, &HitInfo_t3229609740_0_0_0 } /* UnityEngine.SendMouseEvents/HitInfo */,
	{ NULL, SkeletonBone_t4134054672_marshal_pinvoke, SkeletonBone_t4134054672_marshal_pinvoke_back, SkeletonBone_t4134054672_marshal_pinvoke_cleanup, NULL, NULL, &SkeletonBone_t4134054672_0_0_0 } /* UnityEngine.SkeletonBone */,
	{ NULL, GcAchievementData_t675222246_marshal_pinvoke, GcAchievementData_t675222246_marshal_pinvoke_back, GcAchievementData_t675222246_marshal_pinvoke_cleanup, NULL, NULL, &GcAchievementData_t675222246_0_0_0 } /* UnityEngine.SocialPlatforms.GameCenter.GcAchievementData */,
	{ NULL, GcAchievementDescriptionData_t643925653_marshal_pinvoke, GcAchievementDescriptionData_t643925653_marshal_pinvoke_back, GcAchievementDescriptionData_t643925653_marshal_pinvoke_cleanup, NULL, NULL, &GcAchievementDescriptionData_t643925653_0_0_0 } /* UnityEngine.SocialPlatforms.GameCenter.GcAchievementDescriptionData */,
	{ NULL, GcLeaderboard_t4132273028_marshal_pinvoke, GcLeaderboard_t4132273028_marshal_pinvoke_back, GcLeaderboard_t4132273028_marshal_pinvoke_cleanup, NULL, NULL, &GcLeaderboard_t4132273028_0_0_0 } /* UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard */,
	{ NULL, GcScoreData_t2125309831_marshal_pinvoke, GcScoreData_t2125309831_marshal_pinvoke_back, GcScoreData_t2125309831_marshal_pinvoke_cleanup, NULL, NULL, &GcScoreData_t2125309831_0_0_0 } /* UnityEngine.SocialPlatforms.GameCenter.GcScoreData */,
	{ NULL, GcUserProfileData_t2719720026_marshal_pinvoke, GcUserProfileData_t2719720026_marshal_pinvoke_back, GcUserProfileData_t2719720026_marshal_pinvoke_cleanup, NULL, NULL, &GcUserProfileData_t2719720026_0_0_0 } /* UnityEngine.SocialPlatforms.GameCenter.GcUserProfileData */,
	{ NULL, TextGenerationSettings_t1351628751_marshal_pinvoke, TextGenerationSettings_t1351628751_marshal_pinvoke_back, TextGenerationSettings_t1351628751_marshal_pinvoke_cleanup, NULL, NULL, &TextGenerationSettings_t1351628751_0_0_0 } /* UnityEngine.TextGenerationSettings */,
	{ NULL, TextGenerator_t3211863866_marshal_pinvoke, TextGenerator_t3211863866_marshal_pinvoke_back, TextGenerator_t3211863866_marshal_pinvoke_cleanup, NULL, NULL, &TextGenerator_t3211863866_0_0_0 } /* UnityEngine.TextGenerator */,
	{ NULL, TrackedReference_t1199777556_marshal_pinvoke, TrackedReference_t1199777556_marshal_pinvoke_back, TrackedReference_t1199777556_marshal_pinvoke_cleanup, NULL, NULL, &TrackedReference_t1199777556_0_0_0 } /* UnityEngine.TrackedReference */,
	{ NULL, WaitForSeconds_t1699091251_marshal_pinvoke, WaitForSeconds_t1699091251_marshal_pinvoke_back, WaitForSeconds_t1699091251_marshal_pinvoke_cleanup, NULL, NULL, &WaitForSeconds_t1699091251_0_0_0 } /* UnityEngine.WaitForSeconds */,
	{ NULL, WebCamDevice_t1322781432_marshal_pinvoke, WebCamDevice_t1322781432_marshal_pinvoke_back, WebCamDevice_t1322781432_marshal_pinvoke_cleanup, NULL, NULL, &WebCamDevice_t1322781432_0_0_0 } /* UnityEngine.WebCamDevice */,
	{ NULL, YieldInstruction_t403091072_marshal_pinvoke, YieldInstruction_t403091072_marshal_pinvoke_back, YieldInstruction_t403091072_marshal_pinvoke_cleanup, NULL, NULL, &YieldInstruction_t403091072_0_0_0 } /* UnityEngine.YieldInstruction */,
	{ NULL, RaycastResult_t3360306849_marshal_pinvoke, RaycastResult_t3360306849_marshal_pinvoke_back, RaycastResult_t3360306849_marshal_pinvoke_cleanup, NULL, NULL, &RaycastResult_t3360306849_0_0_0 } /* UnityEngine.EventSystems.RaycastResult */,
	{ NULL, ColorTween_t809614380_marshal_pinvoke, ColorTween_t809614380_marshal_pinvoke_back, ColorTween_t809614380_marshal_pinvoke_cleanup, NULL, NULL, &ColorTween_t809614380_0_0_0 } /* UnityEngine.UI.CoroutineTween.ColorTween */,
	{ NULL, FloatTween_t1274330004_marshal_pinvoke, FloatTween_t1274330004_marshal_pinvoke_back, FloatTween_t1274330004_marshal_pinvoke_cleanup, NULL, NULL, &FloatTween_t1274330004_0_0_0 } /* UnityEngine.UI.CoroutineTween.FloatTween */,
	{ NULL, Resources_t1597885468_marshal_pinvoke, Resources_t1597885468_marshal_pinvoke_back, Resources_t1597885468_marshal_pinvoke_cleanup, NULL, NULL, &Resources_t1597885468_0_0_0 } /* UnityEngine.UI.DefaultControls/Resources */,
	{ DelegatePInvokeWrapper_OnValidateInput_t2355412304, NULL, NULL, NULL, NULL, NULL, &OnValidateInput_t2355412304_0_0_0 } /* UnityEngine.UI.InputField/OnValidateInput */,
	{ NULL, Navigation_t3049316579_marshal_pinvoke, Navigation_t3049316579_marshal_pinvoke_back, Navigation_t3049316579_marshal_pinvoke_cleanup, NULL, NULL, &Navigation_t3049316579_0_0_0 } /* UnityEngine.UI.Navigation */,
	{ NULL, SpriteState_t1362986479_marshal_pinvoke, SpriteState_t1362986479_marshal_pinvoke_back, SpriteState_t1362986479_marshal_pinvoke_cleanup, NULL, NULL, &SpriteState_t1362986479_0_0_0 } /* UnityEngine.UI.SpriteState */,
	{ NULL, ColorTween_t378116136_marshal_pinvoke, ColorTween_t378116136_marshal_pinvoke_back, ColorTween_t378116136_marshal_pinvoke_cleanup, NULL, NULL, &ColorTween_t378116136_0_0_0 } /* TMPro.ColorTween */,
	{ NULL, FloatTween_t3783157226_marshal_pinvoke, FloatTween_t3783157226_marshal_pinvoke_back, FloatTween_t3783157226_marshal_pinvoke_cleanup, NULL, NULL, &FloatTween_t3783157226_0_0_0 } /* TMPro.FloatTween */,
	{ NULL, FontCreationSetting_t628772060_marshal_pinvoke, FontCreationSetting_t628772060_marshal_pinvoke_back, FontCreationSetting_t628772060_marshal_pinvoke_cleanup, NULL, NULL, &FontCreationSetting_t628772060_0_0_0 } /* TMPro.FontCreationSetting */,
	{ NULL, MaterialReference_t1952344632_marshal_pinvoke, MaterialReference_t1952344632_marshal_pinvoke_back, MaterialReference_t1952344632_marshal_pinvoke_cleanup, NULL, NULL, &MaterialReference_t1952344632_0_0_0 } /* TMPro.MaterialReference */,
	{ NULL, SpriteData_t3048397587_marshal_pinvoke, SpriteData_t3048397587_marshal_pinvoke_back, SpriteData_t3048397587_marshal_pinvoke_cleanup, NULL, NULL, &SpriteData_t3048397587_0_0_0 } /* TMPro.SpriteAssetUtilities.TexturePacker/SpriteData */,
	{ NULL, TMP_CharacterInfo_t3185626797_marshal_pinvoke, TMP_CharacterInfo_t3185626797_marshal_pinvoke_back, TMP_CharacterInfo_t3185626797_marshal_pinvoke_cleanup, NULL, NULL, &TMP_CharacterInfo_t3185626797_0_0_0 } /* TMPro.TMP_CharacterInfo */,
	{ NULL, Resources_t2155109485_marshal_pinvoke, Resources_t2155109485_marshal_pinvoke_back, Resources_t2155109485_marshal_pinvoke_cleanup, NULL, NULL, &Resources_t2155109485_0_0_0 } /* TMPro.TMP_DefaultControls/Resources */,
	{ NULL, TMP_FontWeights_t916301067_marshal_pinvoke, TMP_FontWeights_t916301067_marshal_pinvoke_back, TMP_FontWeights_t916301067_marshal_pinvoke_cleanup, NULL, NULL, &TMP_FontWeights_t916301067_0_0_0 } /* TMPro.TMP_FontWeights */,
	{ DelegatePInvokeWrapper_OnValidateInput_t373909109, NULL, NULL, NULL, NULL, NULL, &OnValidateInput_t373909109_0_0_0 } /* TMPro.TMP_InputField/OnValidateInput */,
	{ NULL, TMP_LinkInfo_t1092083476_marshal_pinvoke, TMP_LinkInfo_t1092083476_marshal_pinvoke_back, TMP_LinkInfo_t1092083476_marshal_pinvoke_cleanup, NULL, NULL, &TMP_LinkInfo_t1092083476_0_0_0 } /* TMPro.TMP_LinkInfo */,
	{ NULL, TMP_MeshInfo_t2771747634_marshal_pinvoke, TMP_MeshInfo_t2771747634_marshal_pinvoke_back, TMP_MeshInfo_t2771747634_marshal_pinvoke_cleanup, NULL, NULL, &TMP_MeshInfo_t2771747634_0_0_0 } /* TMPro.TMP_MeshInfo */,
	{ NULL, TMP_WordInfo_t3331066303_marshal_pinvoke, TMP_WordInfo_t3331066303_marshal_pinvoke_back, TMP_WordInfo_t3331066303_marshal_pinvoke_cleanup, NULL, NULL, &TMP_WordInfo_t3331066303_0_0_0 } /* TMPro.TMP_WordInfo */,
	{ NULL, WordWrapState_t341939652_marshal_pinvoke, WordWrapState_t341939652_marshal_pinvoke_back, WordWrapState_t341939652_marshal_pinvoke_cleanup, NULL, NULL, &WordWrapState_t341939652_0_0_0 } /* TMPro.WordWrapState */,
	{ NULL, Frame_t1062777224_marshal_pinvoke, Frame_t1062777224_marshal_pinvoke_back, Frame_t1062777224_marshal_pinvoke_cleanup, NULL, NULL, &Frame_t1062777224_0_0_0 } /* Wikitude.Frame */,
	{ NULL, SimulatedTarget_t1564170948_marshal_pinvoke, SimulatedTarget_t1564170948_marshal_pinvoke_back, SimulatedTarget_t1564170948_marshal_pinvoke_cleanup, NULL, NULL, &SimulatedTarget_t1564170948_0_0_0 } /* Wikitude.UnityEditorSimulator/SimulatedTarget */,
	{ NULL, Data_t434873072_marshal_pinvoke, Data_t434873072_marshal_pinvoke_back, Data_t434873072_marshal_pinvoke_cleanup, NULL, NULL, &Data_t434873072_0_0_0 } /* HighlightingSystem.HighlighterRenderer/Data */,
	{ NULL, HighlightingPreset_t635619791_marshal_pinvoke, HighlightingPreset_t635619791_marshal_pinvoke_back, HighlightingPreset_t635619791_marshal_pinvoke_cleanup, NULL, NULL, &HighlightingPreset_t635619791_0_0_0 } /* HighlightingSystem.HighlightingPreset */,
	{ DelegatePInvokeWrapper_ApplyTween_t3327999347, NULL, NULL, NULL, NULL, NULL, &ApplyTween_t3327999347_0_0_0 } /* iTween/ApplyTween */,
	{ DelegatePInvokeWrapper_EasingFunction_t2767217938, NULL, NULL, NULL, NULL, NULL, &EasingFunction_t2767217938_0_0_0 } /* iTween/EasingFunction */,
	{ NULL, InputFrameData_t2573966900_marshal_pinvoke, InputFrameData_t2573966900_marshal_pinvoke_back, InputFrameData_t2573966900_marshal_pinvoke_cleanup, NULL, NULL, &InputFrameData_t2573966900_0_0_0 } /* CustomCameraController/InputFrameData */,
	{ NULL, ImageRotation_t1158944265_marshal_pinvoke, ImageRotation_t1158944265_marshal_pinvoke_back, ImageRotation_t1158944265_marshal_pinvoke_cleanup, NULL, NULL, &ImageRotation_t1158944265_0_0_0 } /* CustomCameraRenderer/ImageRotation */,
	{ DelegatePInvokeWrapper_FieldNotFound_t2620845099, NULL, NULL, NULL, NULL, NULL, &FieldNotFound_t2620845099_0_0_0 } /* JSONObject/FieldNotFound */,
	NULL,
};
