﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// UnityEngine.Vector3[]
struct Vector3U5BU5D_t1718750761;
// UnityEngine.Color[]
struct ColorU5BU5D_t941916413;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Wikitude.MapPointCloud
struct  MapPointCloud_t955983077  : public Il2CppObject
{
public:
	// UnityEngine.Vector3[] Wikitude.MapPointCloud::FeaturePoints
	Vector3U5BU5D_t1718750761* ___FeaturePoints_0;
	// UnityEngine.Color[] Wikitude.MapPointCloud::Colors
	ColorU5BU5D_t941916413* ___Colors_1;
	// System.Single Wikitude.MapPointCloud::Scale
	float ___Scale_2;

public:
	inline static int32_t get_offset_of_FeaturePoints_0() { return static_cast<int32_t>(offsetof(MapPointCloud_t955983077, ___FeaturePoints_0)); }
	inline Vector3U5BU5D_t1718750761* get_FeaturePoints_0() const { return ___FeaturePoints_0; }
	inline Vector3U5BU5D_t1718750761** get_address_of_FeaturePoints_0() { return &___FeaturePoints_0; }
	inline void set_FeaturePoints_0(Vector3U5BU5D_t1718750761* value)
	{
		___FeaturePoints_0 = value;
		Il2CppCodeGenWriteBarrier(&___FeaturePoints_0, value);
	}

	inline static int32_t get_offset_of_Colors_1() { return static_cast<int32_t>(offsetof(MapPointCloud_t955983077, ___Colors_1)); }
	inline ColorU5BU5D_t941916413* get_Colors_1() const { return ___Colors_1; }
	inline ColorU5BU5D_t941916413** get_address_of_Colors_1() { return &___Colors_1; }
	inline void set_Colors_1(ColorU5BU5D_t941916413* value)
	{
		___Colors_1 = value;
		Il2CppCodeGenWriteBarrier(&___Colors_1, value);
	}

	inline static int32_t get_offset_of_Scale_2() { return static_cast<int32_t>(offsetof(MapPointCloud_t955983077, ___Scale_2)); }
	inline float get_Scale_2() const { return ___Scale_2; }
	inline float* get_address_of_Scale_2() { return &___Scale_2; }
	inline void set_Scale_2(float value)
	{
		___Scale_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
