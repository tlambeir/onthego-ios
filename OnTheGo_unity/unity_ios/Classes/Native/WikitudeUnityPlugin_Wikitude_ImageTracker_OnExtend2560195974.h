﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_Events_UnityEvent_2_gen631475969.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Wikitude.ImageTracker/OnExtendedTrackingQualityChangedEvent
struct  OnExtendedTrackingQualityChangedEvent_t2560195974  : public UnityEvent_2_t631475969
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
