﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour3962482529.h"

// UnityEngine.Color[]
struct ColorU5BU5D_t941916413;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Test2
struct  Test2_t3743331433  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Color[] Test2::textColor
	ColorU5BU5D_t941916413* ___textColor_2;
	// System.Int32 Test2::index
	int32_t ___index_3;

public:
	inline static int32_t get_offset_of_textColor_2() { return static_cast<int32_t>(offsetof(Test2_t3743331433, ___textColor_2)); }
	inline ColorU5BU5D_t941916413* get_textColor_2() const { return ___textColor_2; }
	inline ColorU5BU5D_t941916413** get_address_of_textColor_2() { return &___textColor_2; }
	inline void set_textColor_2(ColorU5BU5D_t941916413* value)
	{
		___textColor_2 = value;
		Il2CppCodeGenWriteBarrier(&___textColor_2, value);
	}

	inline static int32_t get_offset_of_index_3() { return static_cast<int32_t>(offsetof(Test2_t3743331433, ___index_3)); }
	inline int32_t get_index_3() const { return ___index_3; }
	inline int32_t* get_address_of_index_3() { return &___index_3; }
	inline void set_index_3(int32_t value)
	{
		___index_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
