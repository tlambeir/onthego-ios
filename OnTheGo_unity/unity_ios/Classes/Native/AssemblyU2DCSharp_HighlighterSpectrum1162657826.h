﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_HighlighterInteractive1072699259.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HighlighterSpectrum
struct  HighlighterSpectrum_t1162657826  : public HighlighterInteractive_t1072699259
{
public:
	// System.Boolean HighlighterSpectrum::random
	bool ___random_4;
	// System.Single HighlighterSpectrum::velocity
	float ___velocity_5;
	// System.Single HighlighterSpectrum::t
	float ___t_6;

public:
	inline static int32_t get_offset_of_random_4() { return static_cast<int32_t>(offsetof(HighlighterSpectrum_t1162657826, ___random_4)); }
	inline bool get_random_4() const { return ___random_4; }
	inline bool* get_address_of_random_4() { return &___random_4; }
	inline void set_random_4(bool value)
	{
		___random_4 = value;
	}

	inline static int32_t get_offset_of_velocity_5() { return static_cast<int32_t>(offsetof(HighlighterSpectrum_t1162657826, ___velocity_5)); }
	inline float get_velocity_5() const { return ___velocity_5; }
	inline float* get_address_of_velocity_5() { return &___velocity_5; }
	inline void set_velocity_5(float value)
	{
		___velocity_5 = value;
	}

	inline static int32_t get_offset_of_t_6() { return static_cast<int32_t>(offsetof(HighlighterSpectrum_t1162657826, ___t_6)); }
	inline float get_t_6() const { return ___t_6; }
	inline float* get_address_of_t_6() { return &___t_6; }
	inline void set_t_6(float value)
	{
		___t_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
