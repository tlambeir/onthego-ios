﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour3962482529.h"
#include "AssemblyU2DCSharp_Dinosaur_State4260191658.h"

// System.String
struct String_t;
// UnityEngine.Coroutine
struct Coroutine_t3829159415;
// Dinosaur
struct Dinosaur_t1302057126;
// UnityEngine.Animator
struct Animator_t434523843;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Dinosaur
struct  Dinosaur_t1302057126  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Coroutine Dinosaur::_sequenceCoroutine
	Coroutine_t3829159415 * ____sequenceCoroutine_10;
	// UnityEngine.Coroutine Dinosaur::_walkingSpeedCoroutine
	Coroutine_t3829159415 * ____walkingSpeedCoroutine_11;
	// Dinosaur Dinosaur::<TargetDinosaur>k__BackingField
	Dinosaur_t1302057126 * ___U3CTargetDinosaurU3Ek__BackingField_12;
	// Dinosaur Dinosaur::<AttackingDinosaur>k__BackingField
	Dinosaur_t1302057126 * ___U3CAttackingDinosaurU3Ek__BackingField_13;
	// UnityEngine.Animator Dinosaur::_animator
	Animator_t434523843 * ____animator_14;
	// System.Single Dinosaur::RotationSpeed
	float ___RotationSpeed_15;
	// System.Single Dinosaur::MovementSpeed
	float ___MovementSpeed_16;
	// System.Boolean Dinosaur::<InBattle>k__BackingField
	bool ___U3CInBattleU3Ek__BackingField_17;
	// Dinosaur/State Dinosaur::_currentState
	int32_t ____currentState_18;

public:
	inline static int32_t get_offset_of__sequenceCoroutine_10() { return static_cast<int32_t>(offsetof(Dinosaur_t1302057126, ____sequenceCoroutine_10)); }
	inline Coroutine_t3829159415 * get__sequenceCoroutine_10() const { return ____sequenceCoroutine_10; }
	inline Coroutine_t3829159415 ** get_address_of__sequenceCoroutine_10() { return &____sequenceCoroutine_10; }
	inline void set__sequenceCoroutine_10(Coroutine_t3829159415 * value)
	{
		____sequenceCoroutine_10 = value;
		Il2CppCodeGenWriteBarrier(&____sequenceCoroutine_10, value);
	}

	inline static int32_t get_offset_of__walkingSpeedCoroutine_11() { return static_cast<int32_t>(offsetof(Dinosaur_t1302057126, ____walkingSpeedCoroutine_11)); }
	inline Coroutine_t3829159415 * get__walkingSpeedCoroutine_11() const { return ____walkingSpeedCoroutine_11; }
	inline Coroutine_t3829159415 ** get_address_of__walkingSpeedCoroutine_11() { return &____walkingSpeedCoroutine_11; }
	inline void set__walkingSpeedCoroutine_11(Coroutine_t3829159415 * value)
	{
		____walkingSpeedCoroutine_11 = value;
		Il2CppCodeGenWriteBarrier(&____walkingSpeedCoroutine_11, value);
	}

	inline static int32_t get_offset_of_U3CTargetDinosaurU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(Dinosaur_t1302057126, ___U3CTargetDinosaurU3Ek__BackingField_12)); }
	inline Dinosaur_t1302057126 * get_U3CTargetDinosaurU3Ek__BackingField_12() const { return ___U3CTargetDinosaurU3Ek__BackingField_12; }
	inline Dinosaur_t1302057126 ** get_address_of_U3CTargetDinosaurU3Ek__BackingField_12() { return &___U3CTargetDinosaurU3Ek__BackingField_12; }
	inline void set_U3CTargetDinosaurU3Ek__BackingField_12(Dinosaur_t1302057126 * value)
	{
		___U3CTargetDinosaurU3Ek__BackingField_12 = value;
		Il2CppCodeGenWriteBarrier(&___U3CTargetDinosaurU3Ek__BackingField_12, value);
	}

	inline static int32_t get_offset_of_U3CAttackingDinosaurU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(Dinosaur_t1302057126, ___U3CAttackingDinosaurU3Ek__BackingField_13)); }
	inline Dinosaur_t1302057126 * get_U3CAttackingDinosaurU3Ek__BackingField_13() const { return ___U3CAttackingDinosaurU3Ek__BackingField_13; }
	inline Dinosaur_t1302057126 ** get_address_of_U3CAttackingDinosaurU3Ek__BackingField_13() { return &___U3CAttackingDinosaurU3Ek__BackingField_13; }
	inline void set_U3CAttackingDinosaurU3Ek__BackingField_13(Dinosaur_t1302057126 * value)
	{
		___U3CAttackingDinosaurU3Ek__BackingField_13 = value;
		Il2CppCodeGenWriteBarrier(&___U3CAttackingDinosaurU3Ek__BackingField_13, value);
	}

	inline static int32_t get_offset_of__animator_14() { return static_cast<int32_t>(offsetof(Dinosaur_t1302057126, ____animator_14)); }
	inline Animator_t434523843 * get__animator_14() const { return ____animator_14; }
	inline Animator_t434523843 ** get_address_of__animator_14() { return &____animator_14; }
	inline void set__animator_14(Animator_t434523843 * value)
	{
		____animator_14 = value;
		Il2CppCodeGenWriteBarrier(&____animator_14, value);
	}

	inline static int32_t get_offset_of_RotationSpeed_15() { return static_cast<int32_t>(offsetof(Dinosaur_t1302057126, ___RotationSpeed_15)); }
	inline float get_RotationSpeed_15() const { return ___RotationSpeed_15; }
	inline float* get_address_of_RotationSpeed_15() { return &___RotationSpeed_15; }
	inline void set_RotationSpeed_15(float value)
	{
		___RotationSpeed_15 = value;
	}

	inline static int32_t get_offset_of_MovementSpeed_16() { return static_cast<int32_t>(offsetof(Dinosaur_t1302057126, ___MovementSpeed_16)); }
	inline float get_MovementSpeed_16() const { return ___MovementSpeed_16; }
	inline float* get_address_of_MovementSpeed_16() { return &___MovementSpeed_16; }
	inline void set_MovementSpeed_16(float value)
	{
		___MovementSpeed_16 = value;
	}

	inline static int32_t get_offset_of_U3CInBattleU3Ek__BackingField_17() { return static_cast<int32_t>(offsetof(Dinosaur_t1302057126, ___U3CInBattleU3Ek__BackingField_17)); }
	inline bool get_U3CInBattleU3Ek__BackingField_17() const { return ___U3CInBattleU3Ek__BackingField_17; }
	inline bool* get_address_of_U3CInBattleU3Ek__BackingField_17() { return &___U3CInBattleU3Ek__BackingField_17; }
	inline void set_U3CInBattleU3Ek__BackingField_17(bool value)
	{
		___U3CInBattleU3Ek__BackingField_17 = value;
	}

	inline static int32_t get_offset_of__currentState_18() { return static_cast<int32_t>(offsetof(Dinosaur_t1302057126, ____currentState_18)); }
	inline int32_t get__currentState_18() const { return ____currentState_18; }
	inline int32_t* get_address_of__currentState_18() { return &____currentState_18; }
	inline void set__currentState_18(int32_t value)
	{
		____currentState_18 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
