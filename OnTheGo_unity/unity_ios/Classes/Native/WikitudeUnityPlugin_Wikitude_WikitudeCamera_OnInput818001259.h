﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_Events_UnityEvent_2_gen1217887265.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Wikitude.WikitudeCamera/OnInputPluginFailureEvent
struct  OnInputPluginFailureEvent_t818001259  : public UnityEvent_2_t1217887265
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
