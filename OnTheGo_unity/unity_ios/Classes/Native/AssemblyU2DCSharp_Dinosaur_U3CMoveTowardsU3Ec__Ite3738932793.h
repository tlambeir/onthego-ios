﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"
#include "UnityEngine_UnityEngine_Vector33722313464.h"

// UnityEngine.Transform
struct Transform_t3600365921;
// Dinosaur
struct Dinosaur_t1302057126;
// System.Object
struct Il2CppObject;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Dinosaur/<MoveTowards>c__Iterator4
struct  U3CMoveTowardsU3Ec__Iterator4_t3738932793  : public Il2CppObject
{
public:
	// UnityEngine.Transform Dinosaur/<MoveTowards>c__Iterator4::moveTarget
	Transform_t3600365921 * ___moveTarget_0;
	// System.Single Dinosaur/<MoveTowards>c__Iterator4::<distanceToTarget>__0
	float ___U3CdistanceToTargetU3E__0_1;
	// System.Single Dinosaur/<MoveTowards>c__Iterator4::distanceThreshold
	float ___distanceThreshold_2;
	// UnityEngine.Vector3 Dinosaur/<MoveTowards>c__Iterator4::<direction>__1
	Vector3_t3722313464  ___U3CdirectionU3E__1_3;
	// Dinosaur Dinosaur/<MoveTowards>c__Iterator4::$this
	Dinosaur_t1302057126 * ___U24this_4;
	// System.Object Dinosaur/<MoveTowards>c__Iterator4::$current
	Il2CppObject * ___U24current_5;
	// System.Boolean Dinosaur/<MoveTowards>c__Iterator4::$disposing
	bool ___U24disposing_6;
	// System.Int32 Dinosaur/<MoveTowards>c__Iterator4::$PC
	int32_t ___U24PC_7;

public:
	inline static int32_t get_offset_of_moveTarget_0() { return static_cast<int32_t>(offsetof(U3CMoveTowardsU3Ec__Iterator4_t3738932793, ___moveTarget_0)); }
	inline Transform_t3600365921 * get_moveTarget_0() const { return ___moveTarget_0; }
	inline Transform_t3600365921 ** get_address_of_moveTarget_0() { return &___moveTarget_0; }
	inline void set_moveTarget_0(Transform_t3600365921 * value)
	{
		___moveTarget_0 = value;
		Il2CppCodeGenWriteBarrier(&___moveTarget_0, value);
	}

	inline static int32_t get_offset_of_U3CdistanceToTargetU3E__0_1() { return static_cast<int32_t>(offsetof(U3CMoveTowardsU3Ec__Iterator4_t3738932793, ___U3CdistanceToTargetU3E__0_1)); }
	inline float get_U3CdistanceToTargetU3E__0_1() const { return ___U3CdistanceToTargetU3E__0_1; }
	inline float* get_address_of_U3CdistanceToTargetU3E__0_1() { return &___U3CdistanceToTargetU3E__0_1; }
	inline void set_U3CdistanceToTargetU3E__0_1(float value)
	{
		___U3CdistanceToTargetU3E__0_1 = value;
	}

	inline static int32_t get_offset_of_distanceThreshold_2() { return static_cast<int32_t>(offsetof(U3CMoveTowardsU3Ec__Iterator4_t3738932793, ___distanceThreshold_2)); }
	inline float get_distanceThreshold_2() const { return ___distanceThreshold_2; }
	inline float* get_address_of_distanceThreshold_2() { return &___distanceThreshold_2; }
	inline void set_distanceThreshold_2(float value)
	{
		___distanceThreshold_2 = value;
	}

	inline static int32_t get_offset_of_U3CdirectionU3E__1_3() { return static_cast<int32_t>(offsetof(U3CMoveTowardsU3Ec__Iterator4_t3738932793, ___U3CdirectionU3E__1_3)); }
	inline Vector3_t3722313464  get_U3CdirectionU3E__1_3() const { return ___U3CdirectionU3E__1_3; }
	inline Vector3_t3722313464 * get_address_of_U3CdirectionU3E__1_3() { return &___U3CdirectionU3E__1_3; }
	inline void set_U3CdirectionU3E__1_3(Vector3_t3722313464  value)
	{
		___U3CdirectionU3E__1_3 = value;
	}

	inline static int32_t get_offset_of_U24this_4() { return static_cast<int32_t>(offsetof(U3CMoveTowardsU3Ec__Iterator4_t3738932793, ___U24this_4)); }
	inline Dinosaur_t1302057126 * get_U24this_4() const { return ___U24this_4; }
	inline Dinosaur_t1302057126 ** get_address_of_U24this_4() { return &___U24this_4; }
	inline void set_U24this_4(Dinosaur_t1302057126 * value)
	{
		___U24this_4 = value;
		Il2CppCodeGenWriteBarrier(&___U24this_4, value);
	}

	inline static int32_t get_offset_of_U24current_5() { return static_cast<int32_t>(offsetof(U3CMoveTowardsU3Ec__Iterator4_t3738932793, ___U24current_5)); }
	inline Il2CppObject * get_U24current_5() const { return ___U24current_5; }
	inline Il2CppObject ** get_address_of_U24current_5() { return &___U24current_5; }
	inline void set_U24current_5(Il2CppObject * value)
	{
		___U24current_5 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_5, value);
	}

	inline static int32_t get_offset_of_U24disposing_6() { return static_cast<int32_t>(offsetof(U3CMoveTowardsU3Ec__Iterator4_t3738932793, ___U24disposing_6)); }
	inline bool get_U24disposing_6() const { return ___U24disposing_6; }
	inline bool* get_address_of_U24disposing_6() { return &___U24disposing_6; }
	inline void set_U24disposing_6(bool value)
	{
		___U24disposing_6 = value;
	}

	inline static int32_t get_offset_of_U24PC_7() { return static_cast<int32_t>(offsetof(U3CMoveTowardsU3Ec__Iterator4_t3738932793, ___U24PC_7)); }
	inline int32_t get_U24PC_7() const { return ___U24PC_7; }
	inline int32_t* get_address_of_U24PC_7() { return &___U24PC_7; }
	inline void set_U24PC_7(int32_t value)
	{
		___U24PC_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
