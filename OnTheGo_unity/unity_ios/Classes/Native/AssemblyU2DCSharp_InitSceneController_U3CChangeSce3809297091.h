﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"
#include "UnityEngine_UnityEngine_SceneManagement_Scene2348375561.h"

// System.String
struct String_t;
// UnityEngine.Camera
struct Camera_t4157153871;
// UnityEngine.AsyncOperation
struct AsyncOperation_t1445031843;
// InitSceneController
struct InitSceneController_t315947004;
// System.Object
struct Il2CppObject;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// InitSceneController/<ChangeScene>c__Iterator1
struct  U3CChangeSceneU3Ec__Iterator1_t3809297091  : public Il2CppObject
{
public:
	// System.String InitSceneController/<ChangeScene>c__Iterator1::sceneName
	String_t* ___sceneName_0;
	// UnityEngine.SceneManagement.Scene InitSceneController/<ChangeScene>c__Iterator1::<newScene>__0
	Scene_t2348375561  ___U3CnewSceneU3E__0_1;
	// UnityEngine.Camera InitSceneController/<ChangeScene>c__Iterator1::<oldMainCamera>__0
	Camera_t4157153871 * ___U3ColdMainCameraU3E__0_2;
	// UnityEngine.AsyncOperation InitSceneController/<ChangeScene>c__Iterator1::<asyncOperation>__0
	AsyncOperation_t1445031843 * ___U3CasyncOperationU3E__0_3;
	// InitSceneController InitSceneController/<ChangeScene>c__Iterator1::$this
	InitSceneController_t315947004 * ___U24this_4;
	// System.Object InitSceneController/<ChangeScene>c__Iterator1::$current
	Il2CppObject * ___U24current_5;
	// System.Boolean InitSceneController/<ChangeScene>c__Iterator1::$disposing
	bool ___U24disposing_6;
	// System.Int32 InitSceneController/<ChangeScene>c__Iterator1::$PC
	int32_t ___U24PC_7;

public:
	inline static int32_t get_offset_of_sceneName_0() { return static_cast<int32_t>(offsetof(U3CChangeSceneU3Ec__Iterator1_t3809297091, ___sceneName_0)); }
	inline String_t* get_sceneName_0() const { return ___sceneName_0; }
	inline String_t** get_address_of_sceneName_0() { return &___sceneName_0; }
	inline void set_sceneName_0(String_t* value)
	{
		___sceneName_0 = value;
		Il2CppCodeGenWriteBarrier(&___sceneName_0, value);
	}

	inline static int32_t get_offset_of_U3CnewSceneU3E__0_1() { return static_cast<int32_t>(offsetof(U3CChangeSceneU3Ec__Iterator1_t3809297091, ___U3CnewSceneU3E__0_1)); }
	inline Scene_t2348375561  get_U3CnewSceneU3E__0_1() const { return ___U3CnewSceneU3E__0_1; }
	inline Scene_t2348375561 * get_address_of_U3CnewSceneU3E__0_1() { return &___U3CnewSceneU3E__0_1; }
	inline void set_U3CnewSceneU3E__0_1(Scene_t2348375561  value)
	{
		___U3CnewSceneU3E__0_1 = value;
	}

	inline static int32_t get_offset_of_U3ColdMainCameraU3E__0_2() { return static_cast<int32_t>(offsetof(U3CChangeSceneU3Ec__Iterator1_t3809297091, ___U3ColdMainCameraU3E__0_2)); }
	inline Camera_t4157153871 * get_U3ColdMainCameraU3E__0_2() const { return ___U3ColdMainCameraU3E__0_2; }
	inline Camera_t4157153871 ** get_address_of_U3ColdMainCameraU3E__0_2() { return &___U3ColdMainCameraU3E__0_2; }
	inline void set_U3ColdMainCameraU3E__0_2(Camera_t4157153871 * value)
	{
		___U3ColdMainCameraU3E__0_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3ColdMainCameraU3E__0_2, value);
	}

	inline static int32_t get_offset_of_U3CasyncOperationU3E__0_3() { return static_cast<int32_t>(offsetof(U3CChangeSceneU3Ec__Iterator1_t3809297091, ___U3CasyncOperationU3E__0_3)); }
	inline AsyncOperation_t1445031843 * get_U3CasyncOperationU3E__0_3() const { return ___U3CasyncOperationU3E__0_3; }
	inline AsyncOperation_t1445031843 ** get_address_of_U3CasyncOperationU3E__0_3() { return &___U3CasyncOperationU3E__0_3; }
	inline void set_U3CasyncOperationU3E__0_3(AsyncOperation_t1445031843 * value)
	{
		___U3CasyncOperationU3E__0_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CasyncOperationU3E__0_3, value);
	}

	inline static int32_t get_offset_of_U24this_4() { return static_cast<int32_t>(offsetof(U3CChangeSceneU3Ec__Iterator1_t3809297091, ___U24this_4)); }
	inline InitSceneController_t315947004 * get_U24this_4() const { return ___U24this_4; }
	inline InitSceneController_t315947004 ** get_address_of_U24this_4() { return &___U24this_4; }
	inline void set_U24this_4(InitSceneController_t315947004 * value)
	{
		___U24this_4 = value;
		Il2CppCodeGenWriteBarrier(&___U24this_4, value);
	}

	inline static int32_t get_offset_of_U24current_5() { return static_cast<int32_t>(offsetof(U3CChangeSceneU3Ec__Iterator1_t3809297091, ___U24current_5)); }
	inline Il2CppObject * get_U24current_5() const { return ___U24current_5; }
	inline Il2CppObject ** get_address_of_U24current_5() { return &___U24current_5; }
	inline void set_U24current_5(Il2CppObject * value)
	{
		___U24current_5 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_5, value);
	}

	inline static int32_t get_offset_of_U24disposing_6() { return static_cast<int32_t>(offsetof(U3CChangeSceneU3Ec__Iterator1_t3809297091, ___U24disposing_6)); }
	inline bool get_U24disposing_6() const { return ___U24disposing_6; }
	inline bool* get_address_of_U24disposing_6() { return &___U24disposing_6; }
	inline void set_U24disposing_6(bool value)
	{
		___U24disposing_6 = value;
	}

	inline static int32_t get_offset_of_U24PC_7() { return static_cast<int32_t>(offsetof(U3CChangeSceneU3Ec__Iterator1_t3809297091, ___U24PC_7)); }
	inline int32_t get_U24PC_7() const { return ___U24PC_7; }
	inline int32_t* get_address_of_U24PC_7() { return &___U24PC_7; }
	inline void set_U24PC_7(int32_t value)
	{
		___U24PC_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
