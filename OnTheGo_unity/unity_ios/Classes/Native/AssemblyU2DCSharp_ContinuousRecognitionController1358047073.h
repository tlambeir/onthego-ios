﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_SampleController2879308770.h"

// Wikitude.ImageTracker
struct ImageTracker_t271395264;
// UnityEngine.UI.Text
struct Text_t1901882714;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ContinuousRecognitionController
struct  ContinuousRecognitionController_t1358047073  : public SampleController_t2879308770
{
public:
	// Wikitude.ImageTracker ContinuousRecognitionController::Tracker
	ImageTracker_t271395264 * ___Tracker_2;
	// UnityEngine.UI.Text ContinuousRecognitionController::buttonText
	Text_t1901882714 * ___buttonText_3;
	// System.Boolean ContinuousRecognitionController::_trackerRunning
	bool ____trackerRunning_4;
	// System.Boolean ContinuousRecognitionController::_connectionInitialized
	bool ____connectionInitialized_5;
	// System.Double ContinuousRecognitionController::_recognitionInterval
	double ____recognitionInterval_6;

public:
	inline static int32_t get_offset_of_Tracker_2() { return static_cast<int32_t>(offsetof(ContinuousRecognitionController_t1358047073, ___Tracker_2)); }
	inline ImageTracker_t271395264 * get_Tracker_2() const { return ___Tracker_2; }
	inline ImageTracker_t271395264 ** get_address_of_Tracker_2() { return &___Tracker_2; }
	inline void set_Tracker_2(ImageTracker_t271395264 * value)
	{
		___Tracker_2 = value;
		Il2CppCodeGenWriteBarrier(&___Tracker_2, value);
	}

	inline static int32_t get_offset_of_buttonText_3() { return static_cast<int32_t>(offsetof(ContinuousRecognitionController_t1358047073, ___buttonText_3)); }
	inline Text_t1901882714 * get_buttonText_3() const { return ___buttonText_3; }
	inline Text_t1901882714 ** get_address_of_buttonText_3() { return &___buttonText_3; }
	inline void set_buttonText_3(Text_t1901882714 * value)
	{
		___buttonText_3 = value;
		Il2CppCodeGenWriteBarrier(&___buttonText_3, value);
	}

	inline static int32_t get_offset_of__trackerRunning_4() { return static_cast<int32_t>(offsetof(ContinuousRecognitionController_t1358047073, ____trackerRunning_4)); }
	inline bool get__trackerRunning_4() const { return ____trackerRunning_4; }
	inline bool* get_address_of__trackerRunning_4() { return &____trackerRunning_4; }
	inline void set__trackerRunning_4(bool value)
	{
		____trackerRunning_4 = value;
	}

	inline static int32_t get_offset_of__connectionInitialized_5() { return static_cast<int32_t>(offsetof(ContinuousRecognitionController_t1358047073, ____connectionInitialized_5)); }
	inline bool get__connectionInitialized_5() const { return ____connectionInitialized_5; }
	inline bool* get_address_of__connectionInitialized_5() { return &____connectionInitialized_5; }
	inline void set__connectionInitialized_5(bool value)
	{
		____connectionInitialized_5 = value;
	}

	inline static int32_t get_offset_of__recognitionInterval_6() { return static_cast<int32_t>(offsetof(ContinuousRecognitionController_t1358047073, ____recognitionInterval_6)); }
	inline double get__recognitionInterval_6() const { return ____recognitionInterval_6; }
	inline double* get_address_of__recognitionInterval_6() { return &____recognitionInterval_6; }
	inline void set__recognitionInterval_6(double value)
	{
		____recognitionInterval_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
