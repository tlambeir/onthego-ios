﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"
#include "UnityEngine_UnityEngine_Vector22156229523.h"
#include "UnityEngine_UnityEngine_Rect2360479859.h"

// System.Object
struct Il2CppObject;
// UnityEngine.EventSystems.PointerEventData
struct PointerEventData_t3807901092;
// TMPro.TMP_InputField
struct TMP_InputField_t1099764886;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_InputField/<MouseDragOutsideRect>d__255
struct  U3CMouseDragOutsideRectU3Ed__255_t2073557366  : public Il2CppObject
{
public:
	// System.Int32 TMPro.TMP_InputField/<MouseDragOutsideRect>d__255::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object TMPro.TMP_InputField/<MouseDragOutsideRect>d__255::<>2__current
	Il2CppObject * ___U3CU3E2__current_1;
	// UnityEngine.EventSystems.PointerEventData TMPro.TMP_InputField/<MouseDragOutsideRect>d__255::eventData
	PointerEventData_t3807901092 * ___eventData_2;
	// TMPro.TMP_InputField TMPro.TMP_InputField/<MouseDragOutsideRect>d__255::<>4__this
	TMP_InputField_t1099764886 * ___U3CU3E4__this_3;
	// UnityEngine.Vector2 TMPro.TMP_InputField/<MouseDragOutsideRect>d__255::<localMousePos>5__1
	Vector2_t2156229523  ___U3ClocalMousePosU3E5__1_4;
	// UnityEngine.Rect TMPro.TMP_InputField/<MouseDragOutsideRect>d__255::<rect>5__2
	Rect_t2360479859  ___U3CrectU3E5__2_5;
	// System.Single TMPro.TMP_InputField/<MouseDragOutsideRect>d__255::<delay>5__3
	float ___U3CdelayU3E5__3_6;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CMouseDragOutsideRectU3Ed__255_t2073557366, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CMouseDragOutsideRectU3Ed__255_t2073557366, ___U3CU3E2__current_1)); }
	inline Il2CppObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline Il2CppObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(Il2CppObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3E2__current_1, value);
	}

	inline static int32_t get_offset_of_eventData_2() { return static_cast<int32_t>(offsetof(U3CMouseDragOutsideRectU3Ed__255_t2073557366, ___eventData_2)); }
	inline PointerEventData_t3807901092 * get_eventData_2() const { return ___eventData_2; }
	inline PointerEventData_t3807901092 ** get_address_of_eventData_2() { return &___eventData_2; }
	inline void set_eventData_2(PointerEventData_t3807901092 * value)
	{
		___eventData_2 = value;
		Il2CppCodeGenWriteBarrier(&___eventData_2, value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_3() { return static_cast<int32_t>(offsetof(U3CMouseDragOutsideRectU3Ed__255_t2073557366, ___U3CU3E4__this_3)); }
	inline TMP_InputField_t1099764886 * get_U3CU3E4__this_3() const { return ___U3CU3E4__this_3; }
	inline TMP_InputField_t1099764886 ** get_address_of_U3CU3E4__this_3() { return &___U3CU3E4__this_3; }
	inline void set_U3CU3E4__this_3(TMP_InputField_t1099764886 * value)
	{
		___U3CU3E4__this_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3E4__this_3, value);
	}

	inline static int32_t get_offset_of_U3ClocalMousePosU3E5__1_4() { return static_cast<int32_t>(offsetof(U3CMouseDragOutsideRectU3Ed__255_t2073557366, ___U3ClocalMousePosU3E5__1_4)); }
	inline Vector2_t2156229523  get_U3ClocalMousePosU3E5__1_4() const { return ___U3ClocalMousePosU3E5__1_4; }
	inline Vector2_t2156229523 * get_address_of_U3ClocalMousePosU3E5__1_4() { return &___U3ClocalMousePosU3E5__1_4; }
	inline void set_U3ClocalMousePosU3E5__1_4(Vector2_t2156229523  value)
	{
		___U3ClocalMousePosU3E5__1_4 = value;
	}

	inline static int32_t get_offset_of_U3CrectU3E5__2_5() { return static_cast<int32_t>(offsetof(U3CMouseDragOutsideRectU3Ed__255_t2073557366, ___U3CrectU3E5__2_5)); }
	inline Rect_t2360479859  get_U3CrectU3E5__2_5() const { return ___U3CrectU3E5__2_5; }
	inline Rect_t2360479859 * get_address_of_U3CrectU3E5__2_5() { return &___U3CrectU3E5__2_5; }
	inline void set_U3CrectU3E5__2_5(Rect_t2360479859  value)
	{
		___U3CrectU3E5__2_5 = value;
	}

	inline static int32_t get_offset_of_U3CdelayU3E5__3_6() { return static_cast<int32_t>(offsetof(U3CMouseDragOutsideRectU3Ed__255_t2073557366, ___U3CdelayU3E5__3_6)); }
	inline float get_U3CdelayU3E5__3_6() const { return ___U3CdelayU3E5__3_6; }
	inline float* get_address_of_U3CdelayU3E5__3_6() { return &___U3CdelayU3E5__3_6; }
	inline void set_U3CdelayU3E5__3_6(float value)
	{
		___U3CdelayU3E5__3_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
