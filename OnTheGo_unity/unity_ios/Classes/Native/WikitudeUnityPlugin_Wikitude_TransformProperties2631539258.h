﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"
#include "UnityEngine_UnityEngine_Vector33722313464.h"
#include "UnityEngine_UnityEngine_Quaternion2301928331.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Wikitude.TransformProperties
struct  TransformProperties_t2631539258  : public Il2CppObject
{
public:
	// UnityEngine.Vector3 Wikitude.TransformProperties::Position
	Vector3_t3722313464  ___Position_0;
	// UnityEngine.Quaternion Wikitude.TransformProperties::Rotation
	Quaternion_t2301928331  ___Rotation_1;
	// UnityEngine.Vector3 Wikitude.TransformProperties::Scale
	Vector3_t3722313464  ___Scale_2;

public:
	inline static int32_t get_offset_of_Position_0() { return static_cast<int32_t>(offsetof(TransformProperties_t2631539258, ___Position_0)); }
	inline Vector3_t3722313464  get_Position_0() const { return ___Position_0; }
	inline Vector3_t3722313464 * get_address_of_Position_0() { return &___Position_0; }
	inline void set_Position_0(Vector3_t3722313464  value)
	{
		___Position_0 = value;
	}

	inline static int32_t get_offset_of_Rotation_1() { return static_cast<int32_t>(offsetof(TransformProperties_t2631539258, ___Rotation_1)); }
	inline Quaternion_t2301928331  get_Rotation_1() const { return ___Rotation_1; }
	inline Quaternion_t2301928331 * get_address_of_Rotation_1() { return &___Rotation_1; }
	inline void set_Rotation_1(Quaternion_t2301928331  value)
	{
		___Rotation_1 = value;
	}

	inline static int32_t get_offset_of_Scale_2() { return static_cast<int32_t>(offsetof(TransformProperties_t2631539258, ___Scale_2)); }
	inline Vector3_t3722313464  get_Scale_2() const { return ___Scale_2; }
	inline Vector3_t3722313464 * get_address_of_Scale_2() { return &___Scale_2; }
	inline void set_Scale_2(Vector3_t3722313464  value)
	{
		___Scale_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
