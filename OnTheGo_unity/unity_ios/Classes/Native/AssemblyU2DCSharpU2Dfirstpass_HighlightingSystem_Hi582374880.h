﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour3962482529.h"
#include "UnityEngine_UnityEngine_Color2555686324.h"
#include "UnityEngine_UnityEngine_Matrix4x41817901843.h"
#include "UnityEngine_UnityEngine_Rendering_CameraEvent2033959522.h"
#include "AssemblyU2DCSharpU2Dfirstpass_HighlightingSystem_B2745566494.h"
#include "AssemblyU2DCSharpU2Dfirstpass_HighlightingSystem_An468925605.h"
#include "UnityEngine_UnityEngine_Rendering_RenderTargetIden2079184500.h"

// System.String
struct String_t;
// HighlightingSystem.ShaderPropertyID
struct ShaderPropertyID_t567755140;
// UnityEngine.Rendering.CommandBuffer
struct CommandBuffer_t2206337031;
// HighlightingSystem.HighlightingBlitter
struct HighlightingBlitter_t1505432117;
// UnityEngine.RenderTexture
struct RenderTexture_t2108887433;
// UnityEngine.Camera
struct Camera_t4157153871;
// System.String[]
struct StringU5BU5D_t1281789340;
// UnityEngine.Shader[]
struct ShaderU5BU5D_t2047402361;
// UnityEngine.Material[]
struct MaterialU5BU5D_t561872642;
// UnityEngine.Material
struct Material_t340375123;
// System.Collections.Generic.HashSet`1<UnityEngine.Camera>
struct HashSet_1_t2722103345;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HighlightingSystem.HighlightingBase
struct  HighlightingBase_t582374880  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Rendering.CommandBuffer HighlightingSystem.HighlightingBase::renderBuffer
	CommandBuffer_t2206337031 * ___renderBuffer_9;
	// System.Int32 HighlightingSystem.HighlightingBase::cachedWidth
	int32_t ___cachedWidth_10;
	// System.Int32 HighlightingSystem.HighlightingBase::cachedHeight
	int32_t ___cachedHeight_11;
	// System.Int32 HighlightingSystem.HighlightingBase::cachedAA
	int32_t ___cachedAA_12;
	// System.Int32 HighlightingSystem.HighlightingBase::_downsampleFactor
	int32_t ____downsampleFactor_13;
	// System.Int32 HighlightingSystem.HighlightingBase::_iterations
	int32_t ____iterations_14;
	// System.Single HighlightingSystem.HighlightingBase::_blurMinSpread
	float ____blurMinSpread_15;
	// System.Single HighlightingSystem.HighlightingBase::_blurSpread
	float ____blurSpread_16;
	// System.Single HighlightingSystem.HighlightingBase::_blurIntensity
	float ____blurIntensity_17;
	// HighlightingSystem.BlurDirections HighlightingSystem.HighlightingBase::_blurDirections
	int32_t ____blurDirections_18;
	// HighlightingSystem.HighlightingBlitter HighlightingSystem.HighlightingBase::_blitter
	HighlightingBlitter_t1505432117 * ____blitter_19;
	// HighlightingSystem.AntiAliasing HighlightingSystem.HighlightingBase::_antiAliasing
	int32_t ____antiAliasing_20;
	// UnityEngine.Rendering.RenderTargetIdentifier HighlightingSystem.HighlightingBase::highlightingBufferID
	RenderTargetIdentifier_t2079184500  ___highlightingBufferID_21;
	// UnityEngine.Rendering.RenderTargetIdentifier HighlightingSystem.HighlightingBase::blur1ID
	RenderTargetIdentifier_t2079184500  ___blur1ID_22;
	// UnityEngine.Rendering.RenderTargetIdentifier HighlightingSystem.HighlightingBase::blur2ID
	RenderTargetIdentifier_t2079184500  ___blur2ID_23;
	// UnityEngine.RenderTexture HighlightingSystem.HighlightingBase::highlightingBuffer
	RenderTexture_t2108887433 * ___highlightingBuffer_24;
	// UnityEngine.Camera HighlightingSystem.HighlightingBase::cam
	Camera_t4157153871 * ___cam_25;
	// UnityEngine.Material HighlightingSystem.HighlightingBase::blurMaterial
	Material_t340375123 * ___blurMaterial_32;
	// UnityEngine.Material HighlightingSystem.HighlightingBase::cutMaterial
	Material_t340375123 * ___cutMaterial_33;
	// UnityEngine.Material HighlightingSystem.HighlightingBase::compMaterial
	Material_t340375123 * ___compMaterial_34;

public:
	inline static int32_t get_offset_of_renderBuffer_9() { return static_cast<int32_t>(offsetof(HighlightingBase_t582374880, ___renderBuffer_9)); }
	inline CommandBuffer_t2206337031 * get_renderBuffer_9() const { return ___renderBuffer_9; }
	inline CommandBuffer_t2206337031 ** get_address_of_renderBuffer_9() { return &___renderBuffer_9; }
	inline void set_renderBuffer_9(CommandBuffer_t2206337031 * value)
	{
		___renderBuffer_9 = value;
		Il2CppCodeGenWriteBarrier(&___renderBuffer_9, value);
	}

	inline static int32_t get_offset_of_cachedWidth_10() { return static_cast<int32_t>(offsetof(HighlightingBase_t582374880, ___cachedWidth_10)); }
	inline int32_t get_cachedWidth_10() const { return ___cachedWidth_10; }
	inline int32_t* get_address_of_cachedWidth_10() { return &___cachedWidth_10; }
	inline void set_cachedWidth_10(int32_t value)
	{
		___cachedWidth_10 = value;
	}

	inline static int32_t get_offset_of_cachedHeight_11() { return static_cast<int32_t>(offsetof(HighlightingBase_t582374880, ___cachedHeight_11)); }
	inline int32_t get_cachedHeight_11() const { return ___cachedHeight_11; }
	inline int32_t* get_address_of_cachedHeight_11() { return &___cachedHeight_11; }
	inline void set_cachedHeight_11(int32_t value)
	{
		___cachedHeight_11 = value;
	}

	inline static int32_t get_offset_of_cachedAA_12() { return static_cast<int32_t>(offsetof(HighlightingBase_t582374880, ___cachedAA_12)); }
	inline int32_t get_cachedAA_12() const { return ___cachedAA_12; }
	inline int32_t* get_address_of_cachedAA_12() { return &___cachedAA_12; }
	inline void set_cachedAA_12(int32_t value)
	{
		___cachedAA_12 = value;
	}

	inline static int32_t get_offset_of__downsampleFactor_13() { return static_cast<int32_t>(offsetof(HighlightingBase_t582374880, ____downsampleFactor_13)); }
	inline int32_t get__downsampleFactor_13() const { return ____downsampleFactor_13; }
	inline int32_t* get_address_of__downsampleFactor_13() { return &____downsampleFactor_13; }
	inline void set__downsampleFactor_13(int32_t value)
	{
		____downsampleFactor_13 = value;
	}

	inline static int32_t get_offset_of__iterations_14() { return static_cast<int32_t>(offsetof(HighlightingBase_t582374880, ____iterations_14)); }
	inline int32_t get__iterations_14() const { return ____iterations_14; }
	inline int32_t* get_address_of__iterations_14() { return &____iterations_14; }
	inline void set__iterations_14(int32_t value)
	{
		____iterations_14 = value;
	}

	inline static int32_t get_offset_of__blurMinSpread_15() { return static_cast<int32_t>(offsetof(HighlightingBase_t582374880, ____blurMinSpread_15)); }
	inline float get__blurMinSpread_15() const { return ____blurMinSpread_15; }
	inline float* get_address_of__blurMinSpread_15() { return &____blurMinSpread_15; }
	inline void set__blurMinSpread_15(float value)
	{
		____blurMinSpread_15 = value;
	}

	inline static int32_t get_offset_of__blurSpread_16() { return static_cast<int32_t>(offsetof(HighlightingBase_t582374880, ____blurSpread_16)); }
	inline float get__blurSpread_16() const { return ____blurSpread_16; }
	inline float* get_address_of__blurSpread_16() { return &____blurSpread_16; }
	inline void set__blurSpread_16(float value)
	{
		____blurSpread_16 = value;
	}

	inline static int32_t get_offset_of__blurIntensity_17() { return static_cast<int32_t>(offsetof(HighlightingBase_t582374880, ____blurIntensity_17)); }
	inline float get__blurIntensity_17() const { return ____blurIntensity_17; }
	inline float* get_address_of__blurIntensity_17() { return &____blurIntensity_17; }
	inline void set__blurIntensity_17(float value)
	{
		____blurIntensity_17 = value;
	}

	inline static int32_t get_offset_of__blurDirections_18() { return static_cast<int32_t>(offsetof(HighlightingBase_t582374880, ____blurDirections_18)); }
	inline int32_t get__blurDirections_18() const { return ____blurDirections_18; }
	inline int32_t* get_address_of__blurDirections_18() { return &____blurDirections_18; }
	inline void set__blurDirections_18(int32_t value)
	{
		____blurDirections_18 = value;
	}

	inline static int32_t get_offset_of__blitter_19() { return static_cast<int32_t>(offsetof(HighlightingBase_t582374880, ____blitter_19)); }
	inline HighlightingBlitter_t1505432117 * get__blitter_19() const { return ____blitter_19; }
	inline HighlightingBlitter_t1505432117 ** get_address_of__blitter_19() { return &____blitter_19; }
	inline void set__blitter_19(HighlightingBlitter_t1505432117 * value)
	{
		____blitter_19 = value;
		Il2CppCodeGenWriteBarrier(&____blitter_19, value);
	}

	inline static int32_t get_offset_of__antiAliasing_20() { return static_cast<int32_t>(offsetof(HighlightingBase_t582374880, ____antiAliasing_20)); }
	inline int32_t get__antiAliasing_20() const { return ____antiAliasing_20; }
	inline int32_t* get_address_of__antiAliasing_20() { return &____antiAliasing_20; }
	inline void set__antiAliasing_20(int32_t value)
	{
		____antiAliasing_20 = value;
	}

	inline static int32_t get_offset_of_highlightingBufferID_21() { return static_cast<int32_t>(offsetof(HighlightingBase_t582374880, ___highlightingBufferID_21)); }
	inline RenderTargetIdentifier_t2079184500  get_highlightingBufferID_21() const { return ___highlightingBufferID_21; }
	inline RenderTargetIdentifier_t2079184500 * get_address_of_highlightingBufferID_21() { return &___highlightingBufferID_21; }
	inline void set_highlightingBufferID_21(RenderTargetIdentifier_t2079184500  value)
	{
		___highlightingBufferID_21 = value;
	}

	inline static int32_t get_offset_of_blur1ID_22() { return static_cast<int32_t>(offsetof(HighlightingBase_t582374880, ___blur1ID_22)); }
	inline RenderTargetIdentifier_t2079184500  get_blur1ID_22() const { return ___blur1ID_22; }
	inline RenderTargetIdentifier_t2079184500 * get_address_of_blur1ID_22() { return &___blur1ID_22; }
	inline void set_blur1ID_22(RenderTargetIdentifier_t2079184500  value)
	{
		___blur1ID_22 = value;
	}

	inline static int32_t get_offset_of_blur2ID_23() { return static_cast<int32_t>(offsetof(HighlightingBase_t582374880, ___blur2ID_23)); }
	inline RenderTargetIdentifier_t2079184500  get_blur2ID_23() const { return ___blur2ID_23; }
	inline RenderTargetIdentifier_t2079184500 * get_address_of_blur2ID_23() { return &___blur2ID_23; }
	inline void set_blur2ID_23(RenderTargetIdentifier_t2079184500  value)
	{
		___blur2ID_23 = value;
	}

	inline static int32_t get_offset_of_highlightingBuffer_24() { return static_cast<int32_t>(offsetof(HighlightingBase_t582374880, ___highlightingBuffer_24)); }
	inline RenderTexture_t2108887433 * get_highlightingBuffer_24() const { return ___highlightingBuffer_24; }
	inline RenderTexture_t2108887433 ** get_address_of_highlightingBuffer_24() { return &___highlightingBuffer_24; }
	inline void set_highlightingBuffer_24(RenderTexture_t2108887433 * value)
	{
		___highlightingBuffer_24 = value;
		Il2CppCodeGenWriteBarrier(&___highlightingBuffer_24, value);
	}

	inline static int32_t get_offset_of_cam_25() { return static_cast<int32_t>(offsetof(HighlightingBase_t582374880, ___cam_25)); }
	inline Camera_t4157153871 * get_cam_25() const { return ___cam_25; }
	inline Camera_t4157153871 ** get_address_of_cam_25() { return &___cam_25; }
	inline void set_cam_25(Camera_t4157153871 * value)
	{
		___cam_25 = value;
		Il2CppCodeGenWriteBarrier(&___cam_25, value);
	}

	inline static int32_t get_offset_of_blurMaterial_32() { return static_cast<int32_t>(offsetof(HighlightingBase_t582374880, ___blurMaterial_32)); }
	inline Material_t340375123 * get_blurMaterial_32() const { return ___blurMaterial_32; }
	inline Material_t340375123 ** get_address_of_blurMaterial_32() { return &___blurMaterial_32; }
	inline void set_blurMaterial_32(Material_t340375123 * value)
	{
		___blurMaterial_32 = value;
		Il2CppCodeGenWriteBarrier(&___blurMaterial_32, value);
	}

	inline static int32_t get_offset_of_cutMaterial_33() { return static_cast<int32_t>(offsetof(HighlightingBase_t582374880, ___cutMaterial_33)); }
	inline Material_t340375123 * get_cutMaterial_33() const { return ___cutMaterial_33; }
	inline Material_t340375123 ** get_address_of_cutMaterial_33() { return &___cutMaterial_33; }
	inline void set_cutMaterial_33(Material_t340375123 * value)
	{
		___cutMaterial_33 = value;
		Il2CppCodeGenWriteBarrier(&___cutMaterial_33, value);
	}

	inline static int32_t get_offset_of_compMaterial_34() { return static_cast<int32_t>(offsetof(HighlightingBase_t582374880, ___compMaterial_34)); }
	inline Material_t340375123 * get_compMaterial_34() const { return ___compMaterial_34; }
	inline Material_t340375123 ** get_address_of_compMaterial_34() { return &___compMaterial_34; }
	inline void set_compMaterial_34(Material_t340375123 * value)
	{
		___compMaterial_34 = value;
		Il2CppCodeGenWriteBarrier(&___compMaterial_34, value);
	}
};

struct HighlightingBase_t582374880_StaticFields
{
public:
	// UnityEngine.Color HighlightingSystem.HighlightingBase::colorClear
	Color_t2555686324  ___colorClear_2;
	// System.String HighlightingSystem.HighlightingBase::renderBufferName
	String_t* ___renderBufferName_3;
	// UnityEngine.Matrix4x4 HighlightingSystem.HighlightingBase::identityMatrix
	Matrix4x4_t1817901843  ___identityMatrix_4;
	// System.String HighlightingSystem.HighlightingBase::keywordStraightDirections
	String_t* ___keywordStraightDirections_5;
	// System.String HighlightingSystem.HighlightingBase::keywordAllDirections
	String_t* ___keywordAllDirections_6;
	// HighlightingSystem.ShaderPropertyID HighlightingSystem.HighlightingBase::_shaderPropertyID
	ShaderPropertyID_t567755140 * ____shaderPropertyID_8;
	// System.String[] HighlightingSystem.HighlightingBase::shaderPaths
	StringU5BU5D_t1281789340* ___shaderPaths_29;
	// UnityEngine.Shader[] HighlightingSystem.HighlightingBase::shaders
	ShaderU5BU5D_t2047402361* ___shaders_30;
	// UnityEngine.Material[] HighlightingSystem.HighlightingBase::materials
	MaterialU5BU5D_t561872642* ___materials_31;
	// System.Boolean HighlightingSystem.HighlightingBase::initialized
	bool ___initialized_35;
	// System.Collections.Generic.HashSet`1<UnityEngine.Camera> HighlightingSystem.HighlightingBase::cameras
	HashSet_1_t2722103345 * ___cameras_36;

public:
	inline static int32_t get_offset_of_colorClear_2() { return static_cast<int32_t>(offsetof(HighlightingBase_t582374880_StaticFields, ___colorClear_2)); }
	inline Color_t2555686324  get_colorClear_2() const { return ___colorClear_2; }
	inline Color_t2555686324 * get_address_of_colorClear_2() { return &___colorClear_2; }
	inline void set_colorClear_2(Color_t2555686324  value)
	{
		___colorClear_2 = value;
	}

	inline static int32_t get_offset_of_renderBufferName_3() { return static_cast<int32_t>(offsetof(HighlightingBase_t582374880_StaticFields, ___renderBufferName_3)); }
	inline String_t* get_renderBufferName_3() const { return ___renderBufferName_3; }
	inline String_t** get_address_of_renderBufferName_3() { return &___renderBufferName_3; }
	inline void set_renderBufferName_3(String_t* value)
	{
		___renderBufferName_3 = value;
		Il2CppCodeGenWriteBarrier(&___renderBufferName_3, value);
	}

	inline static int32_t get_offset_of_identityMatrix_4() { return static_cast<int32_t>(offsetof(HighlightingBase_t582374880_StaticFields, ___identityMatrix_4)); }
	inline Matrix4x4_t1817901843  get_identityMatrix_4() const { return ___identityMatrix_4; }
	inline Matrix4x4_t1817901843 * get_address_of_identityMatrix_4() { return &___identityMatrix_4; }
	inline void set_identityMatrix_4(Matrix4x4_t1817901843  value)
	{
		___identityMatrix_4 = value;
	}

	inline static int32_t get_offset_of_keywordStraightDirections_5() { return static_cast<int32_t>(offsetof(HighlightingBase_t582374880_StaticFields, ___keywordStraightDirections_5)); }
	inline String_t* get_keywordStraightDirections_5() const { return ___keywordStraightDirections_5; }
	inline String_t** get_address_of_keywordStraightDirections_5() { return &___keywordStraightDirections_5; }
	inline void set_keywordStraightDirections_5(String_t* value)
	{
		___keywordStraightDirections_5 = value;
		Il2CppCodeGenWriteBarrier(&___keywordStraightDirections_5, value);
	}

	inline static int32_t get_offset_of_keywordAllDirections_6() { return static_cast<int32_t>(offsetof(HighlightingBase_t582374880_StaticFields, ___keywordAllDirections_6)); }
	inline String_t* get_keywordAllDirections_6() const { return ___keywordAllDirections_6; }
	inline String_t** get_address_of_keywordAllDirections_6() { return &___keywordAllDirections_6; }
	inline void set_keywordAllDirections_6(String_t* value)
	{
		___keywordAllDirections_6 = value;
		Il2CppCodeGenWriteBarrier(&___keywordAllDirections_6, value);
	}

	inline static int32_t get_offset_of__shaderPropertyID_8() { return static_cast<int32_t>(offsetof(HighlightingBase_t582374880_StaticFields, ____shaderPropertyID_8)); }
	inline ShaderPropertyID_t567755140 * get__shaderPropertyID_8() const { return ____shaderPropertyID_8; }
	inline ShaderPropertyID_t567755140 ** get_address_of__shaderPropertyID_8() { return &____shaderPropertyID_8; }
	inline void set__shaderPropertyID_8(ShaderPropertyID_t567755140 * value)
	{
		____shaderPropertyID_8 = value;
		Il2CppCodeGenWriteBarrier(&____shaderPropertyID_8, value);
	}

	inline static int32_t get_offset_of_shaderPaths_29() { return static_cast<int32_t>(offsetof(HighlightingBase_t582374880_StaticFields, ___shaderPaths_29)); }
	inline StringU5BU5D_t1281789340* get_shaderPaths_29() const { return ___shaderPaths_29; }
	inline StringU5BU5D_t1281789340** get_address_of_shaderPaths_29() { return &___shaderPaths_29; }
	inline void set_shaderPaths_29(StringU5BU5D_t1281789340* value)
	{
		___shaderPaths_29 = value;
		Il2CppCodeGenWriteBarrier(&___shaderPaths_29, value);
	}

	inline static int32_t get_offset_of_shaders_30() { return static_cast<int32_t>(offsetof(HighlightingBase_t582374880_StaticFields, ___shaders_30)); }
	inline ShaderU5BU5D_t2047402361* get_shaders_30() const { return ___shaders_30; }
	inline ShaderU5BU5D_t2047402361** get_address_of_shaders_30() { return &___shaders_30; }
	inline void set_shaders_30(ShaderU5BU5D_t2047402361* value)
	{
		___shaders_30 = value;
		Il2CppCodeGenWriteBarrier(&___shaders_30, value);
	}

	inline static int32_t get_offset_of_materials_31() { return static_cast<int32_t>(offsetof(HighlightingBase_t582374880_StaticFields, ___materials_31)); }
	inline MaterialU5BU5D_t561872642* get_materials_31() const { return ___materials_31; }
	inline MaterialU5BU5D_t561872642** get_address_of_materials_31() { return &___materials_31; }
	inline void set_materials_31(MaterialU5BU5D_t561872642* value)
	{
		___materials_31 = value;
		Il2CppCodeGenWriteBarrier(&___materials_31, value);
	}

	inline static int32_t get_offset_of_initialized_35() { return static_cast<int32_t>(offsetof(HighlightingBase_t582374880_StaticFields, ___initialized_35)); }
	inline bool get_initialized_35() const { return ___initialized_35; }
	inline bool* get_address_of_initialized_35() { return &___initialized_35; }
	inline void set_initialized_35(bool value)
	{
		___initialized_35 = value;
	}

	inline static int32_t get_offset_of_cameras_36() { return static_cast<int32_t>(offsetof(HighlightingBase_t582374880_StaticFields, ___cameras_36)); }
	inline HashSet_1_t2722103345 * get_cameras_36() const { return ___cameras_36; }
	inline HashSet_1_t2722103345 ** get_address_of_cameras_36() { return &___cameras_36; }
	inline void set_cameras_36(HashSet_1_t2722103345 * value)
	{
		___cameras_36 = value;
		Il2CppCodeGenWriteBarrier(&___cameras_36, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
