﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "WikitudeUnityPlugin_Wikitude_RecognizedTarget3529911668.h"
#include "UnityEngine_UnityEngine_Vector22156229523.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Wikitude.ImageTarget
struct  ImageTarget_t2322341322  : public RecognizedTarget_t3529911668
{
public:
	// UnityEngine.Vector2 Wikitude.ImageTarget::_scale
	Vector2_t2156229523  ____scale_4;

public:
	inline static int32_t get_offset_of__scale_4() { return static_cast<int32_t>(offsetof(ImageTarget_t2322341322, ____scale_4)); }
	inline Vector2_t2156229523  get__scale_4() const { return ____scale_4; }
	inline Vector2_t2156229523 * get_address_of__scale_4() { return &____scale_4; }
	inline void set__scale_4(Vector2_t2156229523  value)
	{
		____scale_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
