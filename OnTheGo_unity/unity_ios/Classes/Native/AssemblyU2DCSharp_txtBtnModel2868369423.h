﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour3962482529.h"
#include "UnityEngine_UnityEngine_Color2555686324.h"
#include "UnityEngine_UI_UnityEngine_UI_ColorBlock2139031574.h"

// UnityEngine.GameObject
struct GameObject_t1113636619;
// UnityEngine.Sprite
struct Sprite_t280657092;
// UnityEngine.UI.Text
struct Text_t1901882714;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// txtBtnModel
struct  txtBtnModel_t2868369423  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject txtBtnModel::icon
	GameObject_t1113636619 * ___icon_2;
	// UnityEngine.Sprite txtBtnModel::iconSprite
	Sprite_t280657092 * ___iconSprite_3;
	// UnityEngine.UI.Text txtBtnModel::text
	Text_t1901882714 * ___text_4;
	// UnityEngine.Color txtBtnModel::titleColor
	Color_t2555686324  ___titleColor_5;
	// UnityEngine.Color txtBtnModel::itemColor
	Color_t2555686324  ___itemColor_6;
	// UnityEngine.Color txtBtnModel::highlightColor
	Color_t2555686324  ___highlightColor_7;
	// UnityEngine.UI.ColorBlock txtBtnModel::theColor
	ColorBlock_t2139031574  ___theColor_8;
	// UnityEngine.Color txtBtnModel::originalColor
	Color_t2555686324  ___originalColor_9;

public:
	inline static int32_t get_offset_of_icon_2() { return static_cast<int32_t>(offsetof(txtBtnModel_t2868369423, ___icon_2)); }
	inline GameObject_t1113636619 * get_icon_2() const { return ___icon_2; }
	inline GameObject_t1113636619 ** get_address_of_icon_2() { return &___icon_2; }
	inline void set_icon_2(GameObject_t1113636619 * value)
	{
		___icon_2 = value;
		Il2CppCodeGenWriteBarrier(&___icon_2, value);
	}

	inline static int32_t get_offset_of_iconSprite_3() { return static_cast<int32_t>(offsetof(txtBtnModel_t2868369423, ___iconSprite_3)); }
	inline Sprite_t280657092 * get_iconSprite_3() const { return ___iconSprite_3; }
	inline Sprite_t280657092 ** get_address_of_iconSprite_3() { return &___iconSprite_3; }
	inline void set_iconSprite_3(Sprite_t280657092 * value)
	{
		___iconSprite_3 = value;
		Il2CppCodeGenWriteBarrier(&___iconSprite_3, value);
	}

	inline static int32_t get_offset_of_text_4() { return static_cast<int32_t>(offsetof(txtBtnModel_t2868369423, ___text_4)); }
	inline Text_t1901882714 * get_text_4() const { return ___text_4; }
	inline Text_t1901882714 ** get_address_of_text_4() { return &___text_4; }
	inline void set_text_4(Text_t1901882714 * value)
	{
		___text_4 = value;
		Il2CppCodeGenWriteBarrier(&___text_4, value);
	}

	inline static int32_t get_offset_of_titleColor_5() { return static_cast<int32_t>(offsetof(txtBtnModel_t2868369423, ___titleColor_5)); }
	inline Color_t2555686324  get_titleColor_5() const { return ___titleColor_5; }
	inline Color_t2555686324 * get_address_of_titleColor_5() { return &___titleColor_5; }
	inline void set_titleColor_5(Color_t2555686324  value)
	{
		___titleColor_5 = value;
	}

	inline static int32_t get_offset_of_itemColor_6() { return static_cast<int32_t>(offsetof(txtBtnModel_t2868369423, ___itemColor_6)); }
	inline Color_t2555686324  get_itemColor_6() const { return ___itemColor_6; }
	inline Color_t2555686324 * get_address_of_itemColor_6() { return &___itemColor_6; }
	inline void set_itemColor_6(Color_t2555686324  value)
	{
		___itemColor_6 = value;
	}

	inline static int32_t get_offset_of_highlightColor_7() { return static_cast<int32_t>(offsetof(txtBtnModel_t2868369423, ___highlightColor_7)); }
	inline Color_t2555686324  get_highlightColor_7() const { return ___highlightColor_7; }
	inline Color_t2555686324 * get_address_of_highlightColor_7() { return &___highlightColor_7; }
	inline void set_highlightColor_7(Color_t2555686324  value)
	{
		___highlightColor_7 = value;
	}

	inline static int32_t get_offset_of_theColor_8() { return static_cast<int32_t>(offsetof(txtBtnModel_t2868369423, ___theColor_8)); }
	inline ColorBlock_t2139031574  get_theColor_8() const { return ___theColor_8; }
	inline ColorBlock_t2139031574 * get_address_of_theColor_8() { return &___theColor_8; }
	inline void set_theColor_8(ColorBlock_t2139031574  value)
	{
		___theColor_8 = value;
	}

	inline static int32_t get_offset_of_originalColor_9() { return static_cast<int32_t>(offsetof(txtBtnModel_t2868369423, ___originalColor_9)); }
	inline Color_t2555686324  get_originalColor_9() const { return ___originalColor_9; }
	inline Color_t2555686324 * get_address_of_originalColor_9() { return &___originalColor_9; }
	inline void set_originalColor_9(Color_t2555686324  value)
	{
		___originalColor_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
