﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour3962482529.h"
#include "UnityEngine_UnityEngine_Vector22156229523.h"

// UnityEngine.RectTransform
struct RectTransform_t3704657025;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// landscapeTransformManager
struct  landscapeTransformManager_t1971019274  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.RectTransform landscapeTransformManager::rectTransform
	RectTransform_t3704657025 * ___rectTransform_2;
	// UnityEngine.Vector2 landscapeTransformManager::offsetMin
	Vector2_t2156229523  ___offsetMin_3;
	// UnityEngine.Vector2 landscapeTransformManager::offsetMax
	Vector2_t2156229523  ___offsetMax_4;
	// System.Single landscapeTransformManager::target
	float ___target_5;

public:
	inline static int32_t get_offset_of_rectTransform_2() { return static_cast<int32_t>(offsetof(landscapeTransformManager_t1971019274, ___rectTransform_2)); }
	inline RectTransform_t3704657025 * get_rectTransform_2() const { return ___rectTransform_2; }
	inline RectTransform_t3704657025 ** get_address_of_rectTransform_2() { return &___rectTransform_2; }
	inline void set_rectTransform_2(RectTransform_t3704657025 * value)
	{
		___rectTransform_2 = value;
		Il2CppCodeGenWriteBarrier(&___rectTransform_2, value);
	}

	inline static int32_t get_offset_of_offsetMin_3() { return static_cast<int32_t>(offsetof(landscapeTransformManager_t1971019274, ___offsetMin_3)); }
	inline Vector2_t2156229523  get_offsetMin_3() const { return ___offsetMin_3; }
	inline Vector2_t2156229523 * get_address_of_offsetMin_3() { return &___offsetMin_3; }
	inline void set_offsetMin_3(Vector2_t2156229523  value)
	{
		___offsetMin_3 = value;
	}

	inline static int32_t get_offset_of_offsetMax_4() { return static_cast<int32_t>(offsetof(landscapeTransformManager_t1971019274, ___offsetMax_4)); }
	inline Vector2_t2156229523  get_offsetMax_4() const { return ___offsetMax_4; }
	inline Vector2_t2156229523 * get_address_of_offsetMax_4() { return &___offsetMax_4; }
	inline void set_offsetMax_4(Vector2_t2156229523  value)
	{
		___offsetMax_4 = value;
	}

	inline static int32_t get_offset_of_target_5() { return static_cast<int32_t>(offsetof(landscapeTransformManager_t1971019274, ___target_5)); }
	inline float get_target_5() const { return ___target_5; }
	inline float* get_address_of_target_5() { return &___target_5; }
	inline void set_target_5(float value)
	{
		___target_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
