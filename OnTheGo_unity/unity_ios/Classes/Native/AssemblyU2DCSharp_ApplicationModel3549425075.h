﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"
#include "UnityEngine_UnityEngine_Vector33722313464.h"

// System.String
struct String_t;
// JSONObject
struct JSONObject_t1339445639;
// UnityEngine.AssetBundle
struct AssetBundle_t1153907252;
// System.Collections.Generic.List`1<UnityEngine.Object>
struct List_1_t2103082695;
// UnityEngine.GameObject
struct GameObject_t1113636619;
// UnityEngine.Object[]
struct ObjectU5BU5D_t1417781964;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ApplicationModel
struct  ApplicationModel_t3549425075  : public Il2CppObject
{
public:

public:
};

struct ApplicationModel_t3549425075_StaticFields
{
public:
	// System.String ApplicationModel::url
	String_t* ___url_0;
	// System.String ApplicationModel::model
	String_t* ___model_1;
	// System.String ApplicationModel::mapFilePath
	String_t* ___mapFilePath_2;
	// System.String ApplicationModel::mapFileName
	String_t* ___mapFileName_3;
	// JSONObject ApplicationModel::structure
	JSONObject_t1339445639 * ___structure_4;
	// UnityEngine.AssetBundle ApplicationModel::bundle
	AssetBundle_t1153907252 * ___bundle_5;
	// System.Collections.Generic.List`1<UnityEngine.Object> ApplicationModel::gameobjects
	List_1_t2103082695 * ___gameobjects_6;
	// UnityEngine.GameObject ApplicationModel::gameObjectToLoad
	GameObject_t1113636619 * ___gameObjectToLoad_7;
	// System.Single ApplicationModel::oldx
	float ___oldx_8;
	// System.Single ApplicationModel::oldy
	float ___oldy_9;
	// System.Single ApplicationModel::oldz
	float ___oldz_10;
	// UnityEngine.Vector3 ApplicationModel::oldEulerAngles
	Vector3_t3722313464  ___oldEulerAngles_11;
	// System.String ApplicationModel::arAnimation
	String_t* ___arAnimation_12;
	// UnityEngine.Object[] ApplicationModel::clips
	ObjectU5BU5D_t1417781964* ___clips_13;
	// UnityEngine.Object[] ApplicationModel::images
	ObjectU5BU5D_t1417781964* ___images_14;

public:
	inline static int32_t get_offset_of_url_0() { return static_cast<int32_t>(offsetof(ApplicationModel_t3549425075_StaticFields, ___url_0)); }
	inline String_t* get_url_0() const { return ___url_0; }
	inline String_t** get_address_of_url_0() { return &___url_0; }
	inline void set_url_0(String_t* value)
	{
		___url_0 = value;
		Il2CppCodeGenWriteBarrier(&___url_0, value);
	}

	inline static int32_t get_offset_of_model_1() { return static_cast<int32_t>(offsetof(ApplicationModel_t3549425075_StaticFields, ___model_1)); }
	inline String_t* get_model_1() const { return ___model_1; }
	inline String_t** get_address_of_model_1() { return &___model_1; }
	inline void set_model_1(String_t* value)
	{
		___model_1 = value;
		Il2CppCodeGenWriteBarrier(&___model_1, value);
	}

	inline static int32_t get_offset_of_mapFilePath_2() { return static_cast<int32_t>(offsetof(ApplicationModel_t3549425075_StaticFields, ___mapFilePath_2)); }
	inline String_t* get_mapFilePath_2() const { return ___mapFilePath_2; }
	inline String_t** get_address_of_mapFilePath_2() { return &___mapFilePath_2; }
	inline void set_mapFilePath_2(String_t* value)
	{
		___mapFilePath_2 = value;
		Il2CppCodeGenWriteBarrier(&___mapFilePath_2, value);
	}

	inline static int32_t get_offset_of_mapFileName_3() { return static_cast<int32_t>(offsetof(ApplicationModel_t3549425075_StaticFields, ___mapFileName_3)); }
	inline String_t* get_mapFileName_3() const { return ___mapFileName_3; }
	inline String_t** get_address_of_mapFileName_3() { return &___mapFileName_3; }
	inline void set_mapFileName_3(String_t* value)
	{
		___mapFileName_3 = value;
		Il2CppCodeGenWriteBarrier(&___mapFileName_3, value);
	}

	inline static int32_t get_offset_of_structure_4() { return static_cast<int32_t>(offsetof(ApplicationModel_t3549425075_StaticFields, ___structure_4)); }
	inline JSONObject_t1339445639 * get_structure_4() const { return ___structure_4; }
	inline JSONObject_t1339445639 ** get_address_of_structure_4() { return &___structure_4; }
	inline void set_structure_4(JSONObject_t1339445639 * value)
	{
		___structure_4 = value;
		Il2CppCodeGenWriteBarrier(&___structure_4, value);
	}

	inline static int32_t get_offset_of_bundle_5() { return static_cast<int32_t>(offsetof(ApplicationModel_t3549425075_StaticFields, ___bundle_5)); }
	inline AssetBundle_t1153907252 * get_bundle_5() const { return ___bundle_5; }
	inline AssetBundle_t1153907252 ** get_address_of_bundle_5() { return &___bundle_5; }
	inline void set_bundle_5(AssetBundle_t1153907252 * value)
	{
		___bundle_5 = value;
		Il2CppCodeGenWriteBarrier(&___bundle_5, value);
	}

	inline static int32_t get_offset_of_gameobjects_6() { return static_cast<int32_t>(offsetof(ApplicationModel_t3549425075_StaticFields, ___gameobjects_6)); }
	inline List_1_t2103082695 * get_gameobjects_6() const { return ___gameobjects_6; }
	inline List_1_t2103082695 ** get_address_of_gameobjects_6() { return &___gameobjects_6; }
	inline void set_gameobjects_6(List_1_t2103082695 * value)
	{
		___gameobjects_6 = value;
		Il2CppCodeGenWriteBarrier(&___gameobjects_6, value);
	}

	inline static int32_t get_offset_of_gameObjectToLoad_7() { return static_cast<int32_t>(offsetof(ApplicationModel_t3549425075_StaticFields, ___gameObjectToLoad_7)); }
	inline GameObject_t1113636619 * get_gameObjectToLoad_7() const { return ___gameObjectToLoad_7; }
	inline GameObject_t1113636619 ** get_address_of_gameObjectToLoad_7() { return &___gameObjectToLoad_7; }
	inline void set_gameObjectToLoad_7(GameObject_t1113636619 * value)
	{
		___gameObjectToLoad_7 = value;
		Il2CppCodeGenWriteBarrier(&___gameObjectToLoad_7, value);
	}

	inline static int32_t get_offset_of_oldx_8() { return static_cast<int32_t>(offsetof(ApplicationModel_t3549425075_StaticFields, ___oldx_8)); }
	inline float get_oldx_8() const { return ___oldx_8; }
	inline float* get_address_of_oldx_8() { return &___oldx_8; }
	inline void set_oldx_8(float value)
	{
		___oldx_8 = value;
	}

	inline static int32_t get_offset_of_oldy_9() { return static_cast<int32_t>(offsetof(ApplicationModel_t3549425075_StaticFields, ___oldy_9)); }
	inline float get_oldy_9() const { return ___oldy_9; }
	inline float* get_address_of_oldy_9() { return &___oldy_9; }
	inline void set_oldy_9(float value)
	{
		___oldy_9 = value;
	}

	inline static int32_t get_offset_of_oldz_10() { return static_cast<int32_t>(offsetof(ApplicationModel_t3549425075_StaticFields, ___oldz_10)); }
	inline float get_oldz_10() const { return ___oldz_10; }
	inline float* get_address_of_oldz_10() { return &___oldz_10; }
	inline void set_oldz_10(float value)
	{
		___oldz_10 = value;
	}

	inline static int32_t get_offset_of_oldEulerAngles_11() { return static_cast<int32_t>(offsetof(ApplicationModel_t3549425075_StaticFields, ___oldEulerAngles_11)); }
	inline Vector3_t3722313464  get_oldEulerAngles_11() const { return ___oldEulerAngles_11; }
	inline Vector3_t3722313464 * get_address_of_oldEulerAngles_11() { return &___oldEulerAngles_11; }
	inline void set_oldEulerAngles_11(Vector3_t3722313464  value)
	{
		___oldEulerAngles_11 = value;
	}

	inline static int32_t get_offset_of_arAnimation_12() { return static_cast<int32_t>(offsetof(ApplicationModel_t3549425075_StaticFields, ___arAnimation_12)); }
	inline String_t* get_arAnimation_12() const { return ___arAnimation_12; }
	inline String_t** get_address_of_arAnimation_12() { return &___arAnimation_12; }
	inline void set_arAnimation_12(String_t* value)
	{
		___arAnimation_12 = value;
		Il2CppCodeGenWriteBarrier(&___arAnimation_12, value);
	}

	inline static int32_t get_offset_of_clips_13() { return static_cast<int32_t>(offsetof(ApplicationModel_t3549425075_StaticFields, ___clips_13)); }
	inline ObjectU5BU5D_t1417781964* get_clips_13() const { return ___clips_13; }
	inline ObjectU5BU5D_t1417781964** get_address_of_clips_13() { return &___clips_13; }
	inline void set_clips_13(ObjectU5BU5D_t1417781964* value)
	{
		___clips_13 = value;
		Il2CppCodeGenWriteBarrier(&___clips_13, value);
	}

	inline static int32_t get_offset_of_images_14() { return static_cast<int32_t>(offsetof(ApplicationModel_t3549425075_StaticFields, ___images_14)); }
	inline ObjectU5BU5D_t1417781964* get_images_14() const { return ___images_14; }
	inline ObjectU5BU5D_t1417781964** get_address_of_images_14() { return &___images_14; }
	inline void set_images_14(ObjectU5BU5D_t1417781964* value)
	{
		___images_14 = value;
		Il2CppCodeGenWriteBarrier(&___images_14, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
