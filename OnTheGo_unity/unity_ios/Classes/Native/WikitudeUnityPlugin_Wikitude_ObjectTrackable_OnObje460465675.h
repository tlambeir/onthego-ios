﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_Events_UnityEvent_1_gen1569884548.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Wikitude.ObjectTrackable/OnObjectLostEvent
struct  OnObjectLostEvent_t460465675  : public UnityEvent_1_t1569884548
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
