﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour3962482529.h"

// UnityEngine.Canvas
struct Canvas_t3310196443;
// TMPro.TMP_FontAsset
struct TMP_FontAsset_t364381626;
// UnityEngine.Font
struct Font_t1956802104;
// TMPro.TextMeshProUGUI
struct TextMeshProUGUI_t529313277;
// UnityEngine.UI.Text
struct Text_t1901882714;
// System.String
struct String_t;
// UnityEngine.Material
struct Material_t340375123;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.Benchmark01_UGUI
struct  Benchmark01_UGUI_t3264177817  : public MonoBehaviour_t3962482529
{
public:
	// System.Int32 TMPro.Examples.Benchmark01_UGUI::BenchmarkType
	int32_t ___BenchmarkType_2;
	// UnityEngine.Canvas TMPro.Examples.Benchmark01_UGUI::canvas
	Canvas_t3310196443 * ___canvas_3;
	// TMPro.TMP_FontAsset TMPro.Examples.Benchmark01_UGUI::TMProFont
	TMP_FontAsset_t364381626 * ___TMProFont_4;
	// UnityEngine.Font TMPro.Examples.Benchmark01_UGUI::TextMeshFont
	Font_t1956802104 * ___TextMeshFont_5;
	// TMPro.TextMeshProUGUI TMPro.Examples.Benchmark01_UGUI::m_textMeshPro
	TextMeshProUGUI_t529313277 * ___m_textMeshPro_6;
	// UnityEngine.UI.Text TMPro.Examples.Benchmark01_UGUI::m_textMesh
	Text_t1901882714 * ___m_textMesh_7;
	// UnityEngine.Material TMPro.Examples.Benchmark01_UGUI::m_material01
	Material_t340375123 * ___m_material01_10;
	// UnityEngine.Material TMPro.Examples.Benchmark01_UGUI::m_material02
	Material_t340375123 * ___m_material02_11;

public:
	inline static int32_t get_offset_of_BenchmarkType_2() { return static_cast<int32_t>(offsetof(Benchmark01_UGUI_t3264177817, ___BenchmarkType_2)); }
	inline int32_t get_BenchmarkType_2() const { return ___BenchmarkType_2; }
	inline int32_t* get_address_of_BenchmarkType_2() { return &___BenchmarkType_2; }
	inline void set_BenchmarkType_2(int32_t value)
	{
		___BenchmarkType_2 = value;
	}

	inline static int32_t get_offset_of_canvas_3() { return static_cast<int32_t>(offsetof(Benchmark01_UGUI_t3264177817, ___canvas_3)); }
	inline Canvas_t3310196443 * get_canvas_3() const { return ___canvas_3; }
	inline Canvas_t3310196443 ** get_address_of_canvas_3() { return &___canvas_3; }
	inline void set_canvas_3(Canvas_t3310196443 * value)
	{
		___canvas_3 = value;
		Il2CppCodeGenWriteBarrier(&___canvas_3, value);
	}

	inline static int32_t get_offset_of_TMProFont_4() { return static_cast<int32_t>(offsetof(Benchmark01_UGUI_t3264177817, ___TMProFont_4)); }
	inline TMP_FontAsset_t364381626 * get_TMProFont_4() const { return ___TMProFont_4; }
	inline TMP_FontAsset_t364381626 ** get_address_of_TMProFont_4() { return &___TMProFont_4; }
	inline void set_TMProFont_4(TMP_FontAsset_t364381626 * value)
	{
		___TMProFont_4 = value;
		Il2CppCodeGenWriteBarrier(&___TMProFont_4, value);
	}

	inline static int32_t get_offset_of_TextMeshFont_5() { return static_cast<int32_t>(offsetof(Benchmark01_UGUI_t3264177817, ___TextMeshFont_5)); }
	inline Font_t1956802104 * get_TextMeshFont_5() const { return ___TextMeshFont_5; }
	inline Font_t1956802104 ** get_address_of_TextMeshFont_5() { return &___TextMeshFont_5; }
	inline void set_TextMeshFont_5(Font_t1956802104 * value)
	{
		___TextMeshFont_5 = value;
		Il2CppCodeGenWriteBarrier(&___TextMeshFont_5, value);
	}

	inline static int32_t get_offset_of_m_textMeshPro_6() { return static_cast<int32_t>(offsetof(Benchmark01_UGUI_t3264177817, ___m_textMeshPro_6)); }
	inline TextMeshProUGUI_t529313277 * get_m_textMeshPro_6() const { return ___m_textMeshPro_6; }
	inline TextMeshProUGUI_t529313277 ** get_address_of_m_textMeshPro_6() { return &___m_textMeshPro_6; }
	inline void set_m_textMeshPro_6(TextMeshProUGUI_t529313277 * value)
	{
		___m_textMeshPro_6 = value;
		Il2CppCodeGenWriteBarrier(&___m_textMeshPro_6, value);
	}

	inline static int32_t get_offset_of_m_textMesh_7() { return static_cast<int32_t>(offsetof(Benchmark01_UGUI_t3264177817, ___m_textMesh_7)); }
	inline Text_t1901882714 * get_m_textMesh_7() const { return ___m_textMesh_7; }
	inline Text_t1901882714 ** get_address_of_m_textMesh_7() { return &___m_textMesh_7; }
	inline void set_m_textMesh_7(Text_t1901882714 * value)
	{
		___m_textMesh_7 = value;
		Il2CppCodeGenWriteBarrier(&___m_textMesh_7, value);
	}

	inline static int32_t get_offset_of_m_material01_10() { return static_cast<int32_t>(offsetof(Benchmark01_UGUI_t3264177817, ___m_material01_10)); }
	inline Material_t340375123 * get_m_material01_10() const { return ___m_material01_10; }
	inline Material_t340375123 ** get_address_of_m_material01_10() { return &___m_material01_10; }
	inline void set_m_material01_10(Material_t340375123 * value)
	{
		___m_material01_10 = value;
		Il2CppCodeGenWriteBarrier(&___m_material01_10, value);
	}

	inline static int32_t get_offset_of_m_material02_11() { return static_cast<int32_t>(offsetof(Benchmark01_UGUI_t3264177817, ___m_material02_11)); }
	inline Material_t340375123 * get_m_material02_11() const { return ___m_material02_11; }
	inline Material_t340375123 ** get_address_of_m_material02_11() { return &___m_material02_11; }
	inline void set_m_material02_11(Material_t340375123 * value)
	{
		___m_material02_11 = value;
		Il2CppCodeGenWriteBarrier(&___m_material02_11, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
