﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour3962482529.h"
#include "UnityEngine_UnityEngine_Vector33722313464.h"

// TMPro.TMP_Text
struct TMP_Text_t2599618874;
// UnityEngine.Material
struct Material_t340375123;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EnvMapAnimator
struct  EnvMapAnimator_t1140999784  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Vector3 EnvMapAnimator::RotationSpeeds
	Vector3_t3722313464  ___RotationSpeeds_2;
	// TMPro.TMP_Text EnvMapAnimator::m_textMeshPro
	TMP_Text_t2599618874 * ___m_textMeshPro_3;
	// UnityEngine.Material EnvMapAnimator::m_material
	Material_t340375123 * ___m_material_4;

public:
	inline static int32_t get_offset_of_RotationSpeeds_2() { return static_cast<int32_t>(offsetof(EnvMapAnimator_t1140999784, ___RotationSpeeds_2)); }
	inline Vector3_t3722313464  get_RotationSpeeds_2() const { return ___RotationSpeeds_2; }
	inline Vector3_t3722313464 * get_address_of_RotationSpeeds_2() { return &___RotationSpeeds_2; }
	inline void set_RotationSpeeds_2(Vector3_t3722313464  value)
	{
		___RotationSpeeds_2 = value;
	}

	inline static int32_t get_offset_of_m_textMeshPro_3() { return static_cast<int32_t>(offsetof(EnvMapAnimator_t1140999784, ___m_textMeshPro_3)); }
	inline TMP_Text_t2599618874 * get_m_textMeshPro_3() const { return ___m_textMeshPro_3; }
	inline TMP_Text_t2599618874 ** get_address_of_m_textMeshPro_3() { return &___m_textMeshPro_3; }
	inline void set_m_textMeshPro_3(TMP_Text_t2599618874 * value)
	{
		___m_textMeshPro_3 = value;
		Il2CppCodeGenWriteBarrier(&___m_textMeshPro_3, value);
	}

	inline static int32_t get_offset_of_m_material_4() { return static_cast<int32_t>(offsetof(EnvMapAnimator_t1140999784, ___m_material_4)); }
	inline Material_t340375123 * get_m_material_4() const { return ___m_material_4; }
	inline Material_t340375123 ** get_address_of_m_material_4() { return &___m_material_4; }
	inline void set_m_material_4(Material_t340375123 * value)
	{
		___m_material_4 = value;
		Il2CppCodeGenWriteBarrier(&___m_material_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
