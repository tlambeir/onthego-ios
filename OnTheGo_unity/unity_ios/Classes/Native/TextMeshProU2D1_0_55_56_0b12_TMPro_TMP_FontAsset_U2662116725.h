﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// TMPro.TMP_FontAsset/<>c
struct U3CU3Ec_t2662116725;
// System.Func`2<TMPro.TMP_Glyph,System.Int32>
struct Func_2_t3920652434;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_FontAsset/<>c
struct  U3CU3Ec_t2662116725  : public Il2CppObject
{
public:

public:
};

struct U3CU3Ec_t2662116725_StaticFields
{
public:
	// TMPro.TMP_FontAsset/<>c TMPro.TMP_FontAsset/<>c::<>9
	U3CU3Ec_t2662116725 * ___U3CU3E9_0;
	// System.Func`2<TMPro.TMP_Glyph,System.Int32> TMPro.TMP_FontAsset/<>c::<>9__34_0
	Func_2_t3920652434 * ___U3CU3E9__34_0_1;
	// System.Func`2<TMPro.TMP_Glyph,System.Int32> TMPro.TMP_FontAsset/<>c::<>9__37_0
	Func_2_t3920652434 * ___U3CU3E9__37_0_2;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t2662116725_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t2662116725 * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t2662116725 ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t2662116725 * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3E9_0, value);
	}

	inline static int32_t get_offset_of_U3CU3E9__34_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_t2662116725_StaticFields, ___U3CU3E9__34_0_1)); }
	inline Func_2_t3920652434 * get_U3CU3E9__34_0_1() const { return ___U3CU3E9__34_0_1; }
	inline Func_2_t3920652434 ** get_address_of_U3CU3E9__34_0_1() { return &___U3CU3E9__34_0_1; }
	inline void set_U3CU3E9__34_0_1(Func_2_t3920652434 * value)
	{
		___U3CU3E9__34_0_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3E9__34_0_1, value);
	}

	inline static int32_t get_offset_of_U3CU3E9__37_0_2() { return static_cast<int32_t>(offsetof(U3CU3Ec_t2662116725_StaticFields, ___U3CU3E9__37_0_2)); }
	inline Func_2_t3920652434 * get_U3CU3E9__37_0_2() const { return ___U3CU3E9__37_0_2; }
	inline Func_2_t3920652434 ** get_address_of_U3CU3E9__37_0_2() { return &___U3CU3E9__37_0_2; }
	inline void set_U3CU3E9__37_0_2(Func_2_t3920652434 * value)
	{
		___U3CU3E9__37_0_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3E9__37_0_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
