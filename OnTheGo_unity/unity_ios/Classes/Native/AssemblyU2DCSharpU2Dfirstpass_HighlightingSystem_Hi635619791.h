﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_ValueType3640485471.h"
#include "AssemblyU2DCSharpU2Dfirstpass_HighlightingSystem_B2745566494.h"

// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HighlightingSystem.HighlightingPreset
struct  HighlightingPreset_t635619791 
{
public:
	// System.String HighlightingSystem.HighlightingPreset::_name
	String_t* ____name_0;
	// System.Int32 HighlightingSystem.HighlightingPreset::_downsampleFactor
	int32_t ____downsampleFactor_1;
	// System.Int32 HighlightingSystem.HighlightingPreset::_iterations
	int32_t ____iterations_2;
	// System.Single HighlightingSystem.HighlightingPreset::_blurMinSpread
	float ____blurMinSpread_3;
	// System.Single HighlightingSystem.HighlightingPreset::_blurSpread
	float ____blurSpread_4;
	// System.Single HighlightingSystem.HighlightingPreset::_blurIntensity
	float ____blurIntensity_5;
	// HighlightingSystem.BlurDirections HighlightingSystem.HighlightingPreset::_blurDirections
	int32_t ____blurDirections_6;

public:
	inline static int32_t get_offset_of__name_0() { return static_cast<int32_t>(offsetof(HighlightingPreset_t635619791, ____name_0)); }
	inline String_t* get__name_0() const { return ____name_0; }
	inline String_t** get_address_of__name_0() { return &____name_0; }
	inline void set__name_0(String_t* value)
	{
		____name_0 = value;
		Il2CppCodeGenWriteBarrier(&____name_0, value);
	}

	inline static int32_t get_offset_of__downsampleFactor_1() { return static_cast<int32_t>(offsetof(HighlightingPreset_t635619791, ____downsampleFactor_1)); }
	inline int32_t get__downsampleFactor_1() const { return ____downsampleFactor_1; }
	inline int32_t* get_address_of__downsampleFactor_1() { return &____downsampleFactor_1; }
	inline void set__downsampleFactor_1(int32_t value)
	{
		____downsampleFactor_1 = value;
	}

	inline static int32_t get_offset_of__iterations_2() { return static_cast<int32_t>(offsetof(HighlightingPreset_t635619791, ____iterations_2)); }
	inline int32_t get__iterations_2() const { return ____iterations_2; }
	inline int32_t* get_address_of__iterations_2() { return &____iterations_2; }
	inline void set__iterations_2(int32_t value)
	{
		____iterations_2 = value;
	}

	inline static int32_t get_offset_of__blurMinSpread_3() { return static_cast<int32_t>(offsetof(HighlightingPreset_t635619791, ____blurMinSpread_3)); }
	inline float get__blurMinSpread_3() const { return ____blurMinSpread_3; }
	inline float* get_address_of__blurMinSpread_3() { return &____blurMinSpread_3; }
	inline void set__blurMinSpread_3(float value)
	{
		____blurMinSpread_3 = value;
	}

	inline static int32_t get_offset_of__blurSpread_4() { return static_cast<int32_t>(offsetof(HighlightingPreset_t635619791, ____blurSpread_4)); }
	inline float get__blurSpread_4() const { return ____blurSpread_4; }
	inline float* get_address_of__blurSpread_4() { return &____blurSpread_4; }
	inline void set__blurSpread_4(float value)
	{
		____blurSpread_4 = value;
	}

	inline static int32_t get_offset_of__blurIntensity_5() { return static_cast<int32_t>(offsetof(HighlightingPreset_t635619791, ____blurIntensity_5)); }
	inline float get__blurIntensity_5() const { return ____blurIntensity_5; }
	inline float* get_address_of__blurIntensity_5() { return &____blurIntensity_5; }
	inline void set__blurIntensity_5(float value)
	{
		____blurIntensity_5 = value;
	}

	inline static int32_t get_offset_of__blurDirections_6() { return static_cast<int32_t>(offsetof(HighlightingPreset_t635619791, ____blurDirections_6)); }
	inline int32_t get__blurDirections_6() const { return ____blurDirections_6; }
	inline int32_t* get_address_of__blurDirections_6() { return &____blurDirections_6; }
	inline void set__blurDirections_6(int32_t value)
	{
		____blurDirections_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of HighlightingSystem.HighlightingPreset
struct HighlightingPreset_t635619791_marshaled_pinvoke
{
	char* ____name_0;
	int32_t ____downsampleFactor_1;
	int32_t ____iterations_2;
	float ____blurMinSpread_3;
	float ____blurSpread_4;
	float ____blurIntensity_5;
	int32_t ____blurDirections_6;
};
// Native definition for COM marshalling of HighlightingSystem.HighlightingPreset
struct HighlightingPreset_t635619791_marshaled_com
{
	Il2CppChar* ____name_0;
	int32_t ____downsampleFactor_1;
	int32_t ____iterations_2;
	float ____blurMinSpread_3;
	float ____blurSpread_4;
	float ____blurIntensity_5;
	int32_t ____blurDirections_6;
};
