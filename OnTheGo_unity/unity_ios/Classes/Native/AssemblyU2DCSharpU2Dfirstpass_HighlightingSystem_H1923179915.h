﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharpU2Dfirstpass_HighlightingSystem_Hi582374880.h"

// System.Collections.Generic.List`1<HighlightingSystem.HighlightingPreset>
struct List_1_t2107694533;
// System.Collections.ObjectModel.ReadOnlyCollection`1<HighlightingSystem.HighlightingPreset>
struct ReadOnlyCollection_1_t1848196078;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HighlightingSystem.HighlightingRenderer
struct  HighlightingRenderer_t1923179915  : public HighlightingBase_t582374880
{
public:
	// System.Collections.Generic.List`1<HighlightingSystem.HighlightingPreset> HighlightingSystem.HighlightingRenderer::_presets
	List_1_t2107694533 * ____presets_38;
	// System.Collections.ObjectModel.ReadOnlyCollection`1<HighlightingSystem.HighlightingPreset> HighlightingSystem.HighlightingRenderer::_presetsReadonly
	ReadOnlyCollection_1_t1848196078 * ____presetsReadonly_39;

public:
	inline static int32_t get_offset_of__presets_38() { return static_cast<int32_t>(offsetof(HighlightingRenderer_t1923179915, ____presets_38)); }
	inline List_1_t2107694533 * get__presets_38() const { return ____presets_38; }
	inline List_1_t2107694533 ** get_address_of__presets_38() { return &____presets_38; }
	inline void set__presets_38(List_1_t2107694533 * value)
	{
		____presets_38 = value;
		Il2CppCodeGenWriteBarrier(&____presets_38, value);
	}

	inline static int32_t get_offset_of__presetsReadonly_39() { return static_cast<int32_t>(offsetof(HighlightingRenderer_t1923179915, ____presetsReadonly_39)); }
	inline ReadOnlyCollection_1_t1848196078 * get__presetsReadonly_39() const { return ____presetsReadonly_39; }
	inline ReadOnlyCollection_1_t1848196078 ** get_address_of__presetsReadonly_39() { return &____presetsReadonly_39; }
	inline void set__presetsReadonly_39(ReadOnlyCollection_1_t1848196078 * value)
	{
		____presetsReadonly_39 = value;
		Il2CppCodeGenWriteBarrier(&____presetsReadonly_39, value);
	}
};

struct HighlightingRenderer_t1923179915_StaticFields
{
public:
	// System.Collections.Generic.List`1<HighlightingSystem.HighlightingPreset> HighlightingSystem.HighlightingRenderer::defaultPresets
	List_1_t2107694533 * ___defaultPresets_37;

public:
	inline static int32_t get_offset_of_defaultPresets_37() { return static_cast<int32_t>(offsetof(HighlightingRenderer_t1923179915_StaticFields, ___defaultPresets_37)); }
	inline List_1_t2107694533 * get_defaultPresets_37() const { return ___defaultPresets_37; }
	inline List_1_t2107694533 ** get_address_of_defaultPresets_37() { return &___defaultPresets_37; }
	inline void set_defaultPresets_37(List_1_t2107694533 * value)
	{
		___defaultPresets_37 = value;
		Il2CppCodeGenWriteBarrier(&___defaultPresets_37, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
