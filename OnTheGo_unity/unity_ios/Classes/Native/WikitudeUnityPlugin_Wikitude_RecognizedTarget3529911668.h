﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// UnityEngine.GameObject
struct GameObject_t1113636619;
// System.String
struct String_t;
// System.Single[]
struct SingleU5BU5D_t1444911251;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Wikitude.RecognizedTarget
struct  RecognizedTarget_t3529911668  : public Il2CppObject
{
public:
	// UnityEngine.GameObject Wikitude.RecognizedTarget::Drawable
	GameObject_t1113636619 * ___Drawable_0;
	// System.String Wikitude.RecognizedTarget::Name
	String_t* ___Name_1;
	// System.Int64 Wikitude.RecognizedTarget::ID
	int64_t ___ID_2;
	// System.Single[] Wikitude.RecognizedTarget::ModelViewMatrix
	SingleU5BU5D_t1444911251* ___ModelViewMatrix_3;

public:
	inline static int32_t get_offset_of_Drawable_0() { return static_cast<int32_t>(offsetof(RecognizedTarget_t3529911668, ___Drawable_0)); }
	inline GameObject_t1113636619 * get_Drawable_0() const { return ___Drawable_0; }
	inline GameObject_t1113636619 ** get_address_of_Drawable_0() { return &___Drawable_0; }
	inline void set_Drawable_0(GameObject_t1113636619 * value)
	{
		___Drawable_0 = value;
		Il2CppCodeGenWriteBarrier(&___Drawable_0, value);
	}

	inline static int32_t get_offset_of_Name_1() { return static_cast<int32_t>(offsetof(RecognizedTarget_t3529911668, ___Name_1)); }
	inline String_t* get_Name_1() const { return ___Name_1; }
	inline String_t** get_address_of_Name_1() { return &___Name_1; }
	inline void set_Name_1(String_t* value)
	{
		___Name_1 = value;
		Il2CppCodeGenWriteBarrier(&___Name_1, value);
	}

	inline static int32_t get_offset_of_ID_2() { return static_cast<int32_t>(offsetof(RecognizedTarget_t3529911668, ___ID_2)); }
	inline int64_t get_ID_2() const { return ___ID_2; }
	inline int64_t* get_address_of_ID_2() { return &___ID_2; }
	inline void set_ID_2(int64_t value)
	{
		___ID_2 = value;
	}

	inline static int32_t get_offset_of_ModelViewMatrix_3() { return static_cast<int32_t>(offsetof(RecognizedTarget_t3529911668, ___ModelViewMatrix_3)); }
	inline SingleU5BU5D_t1444911251* get_ModelViewMatrix_3() const { return ___ModelViewMatrix_3; }
	inline SingleU5BU5D_t1444911251** get_address_of_ModelViewMatrix_3() { return &___ModelViewMatrix_3; }
	inline void set_ModelViewMatrix_3(SingleU5BU5D_t1444911251* value)
	{
		___ModelViewMatrix_3 = value;
		Il2CppCodeGenWriteBarrier(&___ModelViewMatrix_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
