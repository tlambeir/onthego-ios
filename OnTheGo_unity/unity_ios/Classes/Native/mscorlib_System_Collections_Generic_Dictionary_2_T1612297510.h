﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_MulticastDelegate157516450.h"

// System.Collections.Generic.HashSet`1<Wikitude.RecognizedTarget>
struct HashSet_1_t2094861142;
// Wikitude.Trackable
struct Trackable_t347424808;
// System.IAsyncResult
struct IAsyncResult_t767004451;
// System.AsyncCallback
struct AsyncCallback_t3962456242;
// System.Object
struct Il2CppObject;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.Dictionary`2/Transform`1<Wikitude.Trackable,System.Collections.Generic.HashSet`1<Wikitude.RecognizedTarget>,System.Collections.Generic.HashSet`1<Wikitude.RecognizedTarget>>
struct  Transform_1_t1612297510  : public MulticastDelegate_t157516450
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
