﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.String
struct String_t;
// Branch
struct Branch_t1428165248;
// TreeBranches
struct TreeBranches_t1493482931;
// UnityEngine.Sprite
struct Sprite_t280657092;
// System.Collections.Generic.List`1<System.String>
struct List_1_t3319525431;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Branch
struct  Branch_t1428165248  : public Il2CppObject
{
public:
	// System.String Branch::<Name>k__BackingField
	String_t* ___U3CNameU3Ek__BackingField_0;
	// System.String Branch::<Description>k__BackingField
	String_t* ___U3CDescriptionU3Ek__BackingField_1;
	// System.String Branch::<Type>k__BackingField
	String_t* ___U3CTypeU3Ek__BackingField_2;
	// System.String Branch::<Asset>k__BackingField
	String_t* ___U3CAssetU3Ek__BackingField_3;
	// Branch Branch::<Parent>k__BackingField
	Branch_t1428165248 * ___U3CParentU3Ek__BackingField_4;
	// TreeBranches Branch::<Children>k__BackingField
	TreeBranches_t1493482931 * ___U3CChildrenU3Ek__BackingField_5;
	// System.Boolean Branch::isRoot
	bool ___isRoot_6;
	// UnityEngine.Sprite Branch::iconSprite
	Sprite_t280657092 * ___iconSprite_7;
	// System.Boolean Branch::isAnimation
	bool ___isAnimation_8;
	// System.Boolean Branch::isHide
	bool ___isHide_9;
	// System.String Branch::animationTrigger
	String_t* ___animationTrigger_10;
	// System.String Branch::path
	String_t* ___path_11;
	// System.String Branch::pdate
	String_t* ___pdate_12;
	// System.String Branch::mesh
	String_t* ___mesh_13;
	// System.Boolean Branch::showLabel
	bool ___showLabel_14;
	// System.Collections.Generic.List`1<System.String> Branch::steps
	List_1_t3319525431 * ___steps_15;
	// System.Collections.Generic.List`1<System.String> Branch::hides
	List_1_t3319525431 * ___hides_16;
	// System.Boolean Branch::isStepAnimation
	bool ___isStepAnimation_17;
	// System.Boolean Branch::isActive
	bool ___isActive_18;

public:
	inline static int32_t get_offset_of_U3CNameU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(Branch_t1428165248, ___U3CNameU3Ek__BackingField_0)); }
	inline String_t* get_U3CNameU3Ek__BackingField_0() const { return ___U3CNameU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CNameU3Ek__BackingField_0() { return &___U3CNameU3Ek__BackingField_0; }
	inline void set_U3CNameU3Ek__BackingField_0(String_t* value)
	{
		___U3CNameU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CNameU3Ek__BackingField_0, value);
	}

	inline static int32_t get_offset_of_U3CDescriptionU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(Branch_t1428165248, ___U3CDescriptionU3Ek__BackingField_1)); }
	inline String_t* get_U3CDescriptionU3Ek__BackingField_1() const { return ___U3CDescriptionU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CDescriptionU3Ek__BackingField_1() { return &___U3CDescriptionU3Ek__BackingField_1; }
	inline void set_U3CDescriptionU3Ek__BackingField_1(String_t* value)
	{
		___U3CDescriptionU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CDescriptionU3Ek__BackingField_1, value);
	}

	inline static int32_t get_offset_of_U3CTypeU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(Branch_t1428165248, ___U3CTypeU3Ek__BackingField_2)); }
	inline String_t* get_U3CTypeU3Ek__BackingField_2() const { return ___U3CTypeU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3CTypeU3Ek__BackingField_2() { return &___U3CTypeU3Ek__BackingField_2; }
	inline void set_U3CTypeU3Ek__BackingField_2(String_t* value)
	{
		___U3CTypeU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CTypeU3Ek__BackingField_2, value);
	}

	inline static int32_t get_offset_of_U3CAssetU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(Branch_t1428165248, ___U3CAssetU3Ek__BackingField_3)); }
	inline String_t* get_U3CAssetU3Ek__BackingField_3() const { return ___U3CAssetU3Ek__BackingField_3; }
	inline String_t** get_address_of_U3CAssetU3Ek__BackingField_3() { return &___U3CAssetU3Ek__BackingField_3; }
	inline void set_U3CAssetU3Ek__BackingField_3(String_t* value)
	{
		___U3CAssetU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CAssetU3Ek__BackingField_3, value);
	}

	inline static int32_t get_offset_of_U3CParentU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(Branch_t1428165248, ___U3CParentU3Ek__BackingField_4)); }
	inline Branch_t1428165248 * get_U3CParentU3Ek__BackingField_4() const { return ___U3CParentU3Ek__BackingField_4; }
	inline Branch_t1428165248 ** get_address_of_U3CParentU3Ek__BackingField_4() { return &___U3CParentU3Ek__BackingField_4; }
	inline void set_U3CParentU3Ek__BackingField_4(Branch_t1428165248 * value)
	{
		___U3CParentU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier(&___U3CParentU3Ek__BackingField_4, value);
	}

	inline static int32_t get_offset_of_U3CChildrenU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(Branch_t1428165248, ___U3CChildrenU3Ek__BackingField_5)); }
	inline TreeBranches_t1493482931 * get_U3CChildrenU3Ek__BackingField_5() const { return ___U3CChildrenU3Ek__BackingField_5; }
	inline TreeBranches_t1493482931 ** get_address_of_U3CChildrenU3Ek__BackingField_5() { return &___U3CChildrenU3Ek__BackingField_5; }
	inline void set_U3CChildrenU3Ek__BackingField_5(TreeBranches_t1493482931 * value)
	{
		___U3CChildrenU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier(&___U3CChildrenU3Ek__BackingField_5, value);
	}

	inline static int32_t get_offset_of_isRoot_6() { return static_cast<int32_t>(offsetof(Branch_t1428165248, ___isRoot_6)); }
	inline bool get_isRoot_6() const { return ___isRoot_6; }
	inline bool* get_address_of_isRoot_6() { return &___isRoot_6; }
	inline void set_isRoot_6(bool value)
	{
		___isRoot_6 = value;
	}

	inline static int32_t get_offset_of_iconSprite_7() { return static_cast<int32_t>(offsetof(Branch_t1428165248, ___iconSprite_7)); }
	inline Sprite_t280657092 * get_iconSprite_7() const { return ___iconSprite_7; }
	inline Sprite_t280657092 ** get_address_of_iconSprite_7() { return &___iconSprite_7; }
	inline void set_iconSprite_7(Sprite_t280657092 * value)
	{
		___iconSprite_7 = value;
		Il2CppCodeGenWriteBarrier(&___iconSprite_7, value);
	}

	inline static int32_t get_offset_of_isAnimation_8() { return static_cast<int32_t>(offsetof(Branch_t1428165248, ___isAnimation_8)); }
	inline bool get_isAnimation_8() const { return ___isAnimation_8; }
	inline bool* get_address_of_isAnimation_8() { return &___isAnimation_8; }
	inline void set_isAnimation_8(bool value)
	{
		___isAnimation_8 = value;
	}

	inline static int32_t get_offset_of_isHide_9() { return static_cast<int32_t>(offsetof(Branch_t1428165248, ___isHide_9)); }
	inline bool get_isHide_9() const { return ___isHide_9; }
	inline bool* get_address_of_isHide_9() { return &___isHide_9; }
	inline void set_isHide_9(bool value)
	{
		___isHide_9 = value;
	}

	inline static int32_t get_offset_of_animationTrigger_10() { return static_cast<int32_t>(offsetof(Branch_t1428165248, ___animationTrigger_10)); }
	inline String_t* get_animationTrigger_10() const { return ___animationTrigger_10; }
	inline String_t** get_address_of_animationTrigger_10() { return &___animationTrigger_10; }
	inline void set_animationTrigger_10(String_t* value)
	{
		___animationTrigger_10 = value;
		Il2CppCodeGenWriteBarrier(&___animationTrigger_10, value);
	}

	inline static int32_t get_offset_of_path_11() { return static_cast<int32_t>(offsetof(Branch_t1428165248, ___path_11)); }
	inline String_t* get_path_11() const { return ___path_11; }
	inline String_t** get_address_of_path_11() { return &___path_11; }
	inline void set_path_11(String_t* value)
	{
		___path_11 = value;
		Il2CppCodeGenWriteBarrier(&___path_11, value);
	}

	inline static int32_t get_offset_of_pdate_12() { return static_cast<int32_t>(offsetof(Branch_t1428165248, ___pdate_12)); }
	inline String_t* get_pdate_12() const { return ___pdate_12; }
	inline String_t** get_address_of_pdate_12() { return &___pdate_12; }
	inline void set_pdate_12(String_t* value)
	{
		___pdate_12 = value;
		Il2CppCodeGenWriteBarrier(&___pdate_12, value);
	}

	inline static int32_t get_offset_of_mesh_13() { return static_cast<int32_t>(offsetof(Branch_t1428165248, ___mesh_13)); }
	inline String_t* get_mesh_13() const { return ___mesh_13; }
	inline String_t** get_address_of_mesh_13() { return &___mesh_13; }
	inline void set_mesh_13(String_t* value)
	{
		___mesh_13 = value;
		Il2CppCodeGenWriteBarrier(&___mesh_13, value);
	}

	inline static int32_t get_offset_of_showLabel_14() { return static_cast<int32_t>(offsetof(Branch_t1428165248, ___showLabel_14)); }
	inline bool get_showLabel_14() const { return ___showLabel_14; }
	inline bool* get_address_of_showLabel_14() { return &___showLabel_14; }
	inline void set_showLabel_14(bool value)
	{
		___showLabel_14 = value;
	}

	inline static int32_t get_offset_of_steps_15() { return static_cast<int32_t>(offsetof(Branch_t1428165248, ___steps_15)); }
	inline List_1_t3319525431 * get_steps_15() const { return ___steps_15; }
	inline List_1_t3319525431 ** get_address_of_steps_15() { return &___steps_15; }
	inline void set_steps_15(List_1_t3319525431 * value)
	{
		___steps_15 = value;
		Il2CppCodeGenWriteBarrier(&___steps_15, value);
	}

	inline static int32_t get_offset_of_hides_16() { return static_cast<int32_t>(offsetof(Branch_t1428165248, ___hides_16)); }
	inline List_1_t3319525431 * get_hides_16() const { return ___hides_16; }
	inline List_1_t3319525431 ** get_address_of_hides_16() { return &___hides_16; }
	inline void set_hides_16(List_1_t3319525431 * value)
	{
		___hides_16 = value;
		Il2CppCodeGenWriteBarrier(&___hides_16, value);
	}

	inline static int32_t get_offset_of_isStepAnimation_17() { return static_cast<int32_t>(offsetof(Branch_t1428165248, ___isStepAnimation_17)); }
	inline bool get_isStepAnimation_17() const { return ___isStepAnimation_17; }
	inline bool* get_address_of_isStepAnimation_17() { return &___isStepAnimation_17; }
	inline void set_isStepAnimation_17(bool value)
	{
		___isStepAnimation_17 = value;
	}

	inline static int32_t get_offset_of_isActive_18() { return static_cast<int32_t>(offsetof(Branch_t1428165248, ___isActive_18)); }
	inline bool get_isActive_18() const { return ___isActive_18; }
	inline bool* get_address_of_isActive_18() { return &___isActive_18; }
	inline void set_isActive_18(bool value)
	{
		___isActive_18 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
