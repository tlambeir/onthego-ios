﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UI_UnityEngine_UI_Selectable3250028441.h"
#include "TextMeshProU2D1_0_55_56_0b12_TMPro_TMP_InputField_1128941285.h"
#include "TextMeshProU2D1_0_55_56_0b12_TMPro_TMP_InputField_2510134850.h"
#include "UnityEngine_UnityEngine_TouchScreenKeyboardType1530597702.h"
#include "TextMeshProU2D1_0_55_56_0b12_TMPro_TMP_InputField_2817627283.h"
#include "TextMeshProU2D1_0_55_56_0b12_TMPro_TMP_InputField_3801396403.h"
#include "UnityEngine_UnityEngine_Color2555686324.h"
#include "UnityEngine_UnityEngine_Vector22156229523.h"

// UnityEngine.TouchScreenKeyboard
struct TouchScreenKeyboard_t731888065;
// System.Char[]
struct CharU5BU5D_t3528271667;
// UnityEngine.RectTransform
struct RectTransform_t3704657025;
// TMPro.TMP_Text
struct TMP_Text_t2599618874;
// UnityEngine.UI.Graphic
struct Graphic_t1660335611;
// UnityEngine.UI.Scrollbar
struct Scrollbar_t1494447233;
// TMPro.TMP_ScrollbarEventHandler
struct TMP_ScrollbarEventHandler_t374199898;
// System.String
struct String_t;
// TMPro.TMP_InputField/SubmitEvent
struct SubmitEvent_t1343580625;
// TMPro.TMP_InputField/SelectionEvent
struct SelectionEvent_t4268942288;
// TMPro.TMP_InputField/TextSelectionEvent
struct TextSelectionEvent_t1023421581;
// TMPro.TMP_InputField/OnChangeEvent
struct OnChangeEvent_t4257774360;
// TMPro.TMP_InputField/OnValidateInput
struct OnValidateInput_t373909109;
// UnityEngine.UIVertex[]
struct UIVertexU5BU5D_t1981460040;
// UnityEngine.CanvasRenderer
struct CanvasRenderer_t2598313366;
// UnityEngine.Mesh
struct Mesh_t3648964284;
// UnityEngine.Coroutine
struct Coroutine_t3829159415;
// TMPro.TMP_FontAsset
struct TMP_FontAsset_t364381626;
// TMPro.TMP_InputValidator
struct TMP_InputValidator_t1385053824;
// UnityEngine.Event
struct Event_t2956885303;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_InputField
struct  TMP_InputField_t1099764886  : public Selectable_t3250028441
{
public:
	// UnityEngine.TouchScreenKeyboard TMPro.TMP_InputField::m_Keyboard
	TouchScreenKeyboard_t731888065 * ___m_Keyboard_16;
	// UnityEngine.RectTransform TMPro.TMP_InputField::m_TextViewport
	RectTransform_t3704657025 * ___m_TextViewport_18;
	// TMPro.TMP_Text TMPro.TMP_InputField::m_TextComponent
	TMP_Text_t2599618874 * ___m_TextComponent_19;
	// UnityEngine.RectTransform TMPro.TMP_InputField::m_TextComponentRectTransform
	RectTransform_t3704657025 * ___m_TextComponentRectTransform_20;
	// UnityEngine.UI.Graphic TMPro.TMP_InputField::m_Placeholder
	Graphic_t1660335611 * ___m_Placeholder_21;
	// UnityEngine.UI.Scrollbar TMPro.TMP_InputField::m_VerticalScrollbar
	Scrollbar_t1494447233 * ___m_VerticalScrollbar_22;
	// TMPro.TMP_ScrollbarEventHandler TMPro.TMP_InputField::m_VerticalScrollbarEventHandler
	TMP_ScrollbarEventHandler_t374199898 * ___m_VerticalScrollbarEventHandler_23;
	// System.Single TMPro.TMP_InputField::m_ScrollPosition
	float ___m_ScrollPosition_24;
	// System.Single TMPro.TMP_InputField::m_ScrollSensitivity
	float ___m_ScrollSensitivity_25;
	// TMPro.TMP_InputField/ContentType TMPro.TMP_InputField::m_ContentType
	int32_t ___m_ContentType_26;
	// TMPro.TMP_InputField/InputType TMPro.TMP_InputField::m_InputType
	int32_t ___m_InputType_27;
	// System.Char TMPro.TMP_InputField::m_AsteriskChar
	Il2CppChar ___m_AsteriskChar_28;
	// UnityEngine.TouchScreenKeyboardType TMPro.TMP_InputField::m_KeyboardType
	int32_t ___m_KeyboardType_29;
	// TMPro.TMP_InputField/LineType TMPro.TMP_InputField::m_LineType
	int32_t ___m_LineType_30;
	// System.Boolean TMPro.TMP_InputField::m_HideMobileInput
	bool ___m_HideMobileInput_31;
	// TMPro.TMP_InputField/CharacterValidation TMPro.TMP_InputField::m_CharacterValidation
	int32_t ___m_CharacterValidation_32;
	// System.String TMPro.TMP_InputField::m_RegexValue
	String_t* ___m_RegexValue_33;
	// System.Single TMPro.TMP_InputField::m_GlobalPointSize
	float ___m_GlobalPointSize_34;
	// System.Int32 TMPro.TMP_InputField::m_CharacterLimit
	int32_t ___m_CharacterLimit_35;
	// TMPro.TMP_InputField/SubmitEvent TMPro.TMP_InputField::m_OnEndEdit
	SubmitEvent_t1343580625 * ___m_OnEndEdit_36;
	// TMPro.TMP_InputField/SubmitEvent TMPro.TMP_InputField::m_OnSubmit
	SubmitEvent_t1343580625 * ___m_OnSubmit_37;
	// TMPro.TMP_InputField/SelectionEvent TMPro.TMP_InputField::m_OnSelect
	SelectionEvent_t4268942288 * ___m_OnSelect_38;
	// TMPro.TMP_InputField/SelectionEvent TMPro.TMP_InputField::m_OnDeselect
	SelectionEvent_t4268942288 * ___m_OnDeselect_39;
	// TMPro.TMP_InputField/TextSelectionEvent TMPro.TMP_InputField::m_OnTextSelection
	TextSelectionEvent_t1023421581 * ___m_OnTextSelection_40;
	// TMPro.TMP_InputField/TextSelectionEvent TMPro.TMP_InputField::m_OnEndTextSelection
	TextSelectionEvent_t1023421581 * ___m_OnEndTextSelection_41;
	// TMPro.TMP_InputField/OnChangeEvent TMPro.TMP_InputField::m_OnValueChanged
	OnChangeEvent_t4257774360 * ___m_OnValueChanged_42;
	// TMPro.TMP_InputField/OnValidateInput TMPro.TMP_InputField::m_OnValidateInput
	OnValidateInput_t373909109 * ___m_OnValidateInput_43;
	// UnityEngine.Color TMPro.TMP_InputField::m_CaretColor
	Color_t2555686324  ___m_CaretColor_44;
	// System.Boolean TMPro.TMP_InputField::m_CustomCaretColor
	bool ___m_CustomCaretColor_45;
	// UnityEngine.Color TMPro.TMP_InputField::m_SelectionColor
	Color_t2555686324  ___m_SelectionColor_46;
	// System.String TMPro.TMP_InputField::m_Text
	String_t* ___m_Text_47;
	// System.Single TMPro.TMP_InputField::m_CaretBlinkRate
	float ___m_CaretBlinkRate_48;
	// System.Int32 TMPro.TMP_InputField::m_CaretWidth
	int32_t ___m_CaretWidth_49;
	// System.Boolean TMPro.TMP_InputField::m_ReadOnly
	bool ___m_ReadOnly_50;
	// System.Boolean TMPro.TMP_InputField::m_RichText
	bool ___m_RichText_51;
	// System.Int32 TMPro.TMP_InputField::m_StringPosition
	int32_t ___m_StringPosition_52;
	// System.Int32 TMPro.TMP_InputField::m_StringSelectPosition
	int32_t ___m_StringSelectPosition_53;
	// System.Int32 TMPro.TMP_InputField::m_CaretPosition
	int32_t ___m_CaretPosition_54;
	// System.Int32 TMPro.TMP_InputField::m_CaretSelectPosition
	int32_t ___m_CaretSelectPosition_55;
	// UnityEngine.RectTransform TMPro.TMP_InputField::caretRectTrans
	RectTransform_t3704657025 * ___caretRectTrans_56;
	// UnityEngine.UIVertex[] TMPro.TMP_InputField::m_CursorVerts
	UIVertexU5BU5D_t1981460040* ___m_CursorVerts_57;
	// UnityEngine.CanvasRenderer TMPro.TMP_InputField::m_CachedInputRenderer
	CanvasRenderer_t2598313366 * ___m_CachedInputRenderer_58;
	// UnityEngine.Vector2 TMPro.TMP_InputField::m_DefaultTransformPosition
	Vector2_t2156229523  ___m_DefaultTransformPosition_59;
	// UnityEngine.Vector2 TMPro.TMP_InputField::m_LastPosition
	Vector2_t2156229523  ___m_LastPosition_60;
	// UnityEngine.Mesh TMPro.TMP_InputField::m_Mesh
	Mesh_t3648964284 * ___m_Mesh_61;
	// System.Boolean TMPro.TMP_InputField::m_AllowInput
	bool ___m_AllowInput_62;
	// System.Boolean TMPro.TMP_InputField::m_ShouldActivateNextUpdate
	bool ___m_ShouldActivateNextUpdate_63;
	// System.Boolean TMPro.TMP_InputField::m_UpdateDrag
	bool ___m_UpdateDrag_64;
	// System.Boolean TMPro.TMP_InputField::m_DragPositionOutOfBounds
	bool ___m_DragPositionOutOfBounds_65;
	// System.Boolean TMPro.TMP_InputField::m_CaretVisible
	bool ___m_CaretVisible_68;
	// UnityEngine.Coroutine TMPro.TMP_InputField::m_BlinkCoroutine
	Coroutine_t3829159415 * ___m_BlinkCoroutine_69;
	// System.Single TMPro.TMP_InputField::m_BlinkStartTime
	float ___m_BlinkStartTime_70;
	// UnityEngine.Coroutine TMPro.TMP_InputField::m_DragCoroutine
	Coroutine_t3829159415 * ___m_DragCoroutine_71;
	// System.String TMPro.TMP_InputField::m_OriginalText
	String_t* ___m_OriginalText_72;
	// System.Boolean TMPro.TMP_InputField::m_WasCanceled
	bool ___m_WasCanceled_73;
	// System.Boolean TMPro.TMP_InputField::m_HasDoneFocusTransition
	bool ___m_HasDoneFocusTransition_74;
	// System.Boolean TMPro.TMP_InputField::m_IsScrollbarUpdateRequired
	bool ___m_IsScrollbarUpdateRequired_75;
	// System.Boolean TMPro.TMP_InputField::m_IsUpdatingScrollbarValues
	bool ___m_IsUpdatingScrollbarValues_76;
	// System.Boolean TMPro.TMP_InputField::m_isLastKeyBackspace
	bool ___m_isLastKeyBackspace_77;
	// System.Single TMPro.TMP_InputField::m_ClickStartTime
	float ___m_ClickStartTime_78;
	// System.Single TMPro.TMP_InputField::m_DoubleClickDelay
	float ___m_DoubleClickDelay_79;
	// TMPro.TMP_FontAsset TMPro.TMP_InputField::m_GlobalFontAsset
	TMP_FontAsset_t364381626 * ___m_GlobalFontAsset_81;
	// System.Boolean TMPro.TMP_InputField::m_OnFocusSelectAll
	bool ___m_OnFocusSelectAll_82;
	// System.Boolean TMPro.TMP_InputField::m_isSelectAll
	bool ___m_isSelectAll_83;
	// System.Boolean TMPro.TMP_InputField::m_ResetOnDeActivation
	bool ___m_ResetOnDeActivation_84;
	// System.Boolean TMPro.TMP_InputField::m_RestoreOriginalTextOnEscape
	bool ___m_RestoreOriginalTextOnEscape_85;
	// System.Boolean TMPro.TMP_InputField::m_isRichTextEditingAllowed
	bool ___m_isRichTextEditingAllowed_86;
	// TMPro.TMP_InputValidator TMPro.TMP_InputField::m_InputValidator
	TMP_InputValidator_t1385053824 * ___m_InputValidator_87;
	// System.Boolean TMPro.TMP_InputField::m_isSelected
	bool ___m_isSelected_88;
	// System.Boolean TMPro.TMP_InputField::isStringPositionDirty
	bool ___isStringPositionDirty_89;
	// System.Boolean TMPro.TMP_InputField::m_forceRectTransformAdjustment
	bool ___m_forceRectTransformAdjustment_90;
	// UnityEngine.Event TMPro.TMP_InputField::m_ProcessingEvent
	Event_t2956885303 * ___m_ProcessingEvent_91;

public:
	inline static int32_t get_offset_of_m_Keyboard_16() { return static_cast<int32_t>(offsetof(TMP_InputField_t1099764886, ___m_Keyboard_16)); }
	inline TouchScreenKeyboard_t731888065 * get_m_Keyboard_16() const { return ___m_Keyboard_16; }
	inline TouchScreenKeyboard_t731888065 ** get_address_of_m_Keyboard_16() { return &___m_Keyboard_16; }
	inline void set_m_Keyboard_16(TouchScreenKeyboard_t731888065 * value)
	{
		___m_Keyboard_16 = value;
		Il2CppCodeGenWriteBarrier(&___m_Keyboard_16, value);
	}

	inline static int32_t get_offset_of_m_TextViewport_18() { return static_cast<int32_t>(offsetof(TMP_InputField_t1099764886, ___m_TextViewport_18)); }
	inline RectTransform_t3704657025 * get_m_TextViewport_18() const { return ___m_TextViewport_18; }
	inline RectTransform_t3704657025 ** get_address_of_m_TextViewport_18() { return &___m_TextViewport_18; }
	inline void set_m_TextViewport_18(RectTransform_t3704657025 * value)
	{
		___m_TextViewport_18 = value;
		Il2CppCodeGenWriteBarrier(&___m_TextViewport_18, value);
	}

	inline static int32_t get_offset_of_m_TextComponent_19() { return static_cast<int32_t>(offsetof(TMP_InputField_t1099764886, ___m_TextComponent_19)); }
	inline TMP_Text_t2599618874 * get_m_TextComponent_19() const { return ___m_TextComponent_19; }
	inline TMP_Text_t2599618874 ** get_address_of_m_TextComponent_19() { return &___m_TextComponent_19; }
	inline void set_m_TextComponent_19(TMP_Text_t2599618874 * value)
	{
		___m_TextComponent_19 = value;
		Il2CppCodeGenWriteBarrier(&___m_TextComponent_19, value);
	}

	inline static int32_t get_offset_of_m_TextComponentRectTransform_20() { return static_cast<int32_t>(offsetof(TMP_InputField_t1099764886, ___m_TextComponentRectTransform_20)); }
	inline RectTransform_t3704657025 * get_m_TextComponentRectTransform_20() const { return ___m_TextComponentRectTransform_20; }
	inline RectTransform_t3704657025 ** get_address_of_m_TextComponentRectTransform_20() { return &___m_TextComponentRectTransform_20; }
	inline void set_m_TextComponentRectTransform_20(RectTransform_t3704657025 * value)
	{
		___m_TextComponentRectTransform_20 = value;
		Il2CppCodeGenWriteBarrier(&___m_TextComponentRectTransform_20, value);
	}

	inline static int32_t get_offset_of_m_Placeholder_21() { return static_cast<int32_t>(offsetof(TMP_InputField_t1099764886, ___m_Placeholder_21)); }
	inline Graphic_t1660335611 * get_m_Placeholder_21() const { return ___m_Placeholder_21; }
	inline Graphic_t1660335611 ** get_address_of_m_Placeholder_21() { return &___m_Placeholder_21; }
	inline void set_m_Placeholder_21(Graphic_t1660335611 * value)
	{
		___m_Placeholder_21 = value;
		Il2CppCodeGenWriteBarrier(&___m_Placeholder_21, value);
	}

	inline static int32_t get_offset_of_m_VerticalScrollbar_22() { return static_cast<int32_t>(offsetof(TMP_InputField_t1099764886, ___m_VerticalScrollbar_22)); }
	inline Scrollbar_t1494447233 * get_m_VerticalScrollbar_22() const { return ___m_VerticalScrollbar_22; }
	inline Scrollbar_t1494447233 ** get_address_of_m_VerticalScrollbar_22() { return &___m_VerticalScrollbar_22; }
	inline void set_m_VerticalScrollbar_22(Scrollbar_t1494447233 * value)
	{
		___m_VerticalScrollbar_22 = value;
		Il2CppCodeGenWriteBarrier(&___m_VerticalScrollbar_22, value);
	}

	inline static int32_t get_offset_of_m_VerticalScrollbarEventHandler_23() { return static_cast<int32_t>(offsetof(TMP_InputField_t1099764886, ___m_VerticalScrollbarEventHandler_23)); }
	inline TMP_ScrollbarEventHandler_t374199898 * get_m_VerticalScrollbarEventHandler_23() const { return ___m_VerticalScrollbarEventHandler_23; }
	inline TMP_ScrollbarEventHandler_t374199898 ** get_address_of_m_VerticalScrollbarEventHandler_23() { return &___m_VerticalScrollbarEventHandler_23; }
	inline void set_m_VerticalScrollbarEventHandler_23(TMP_ScrollbarEventHandler_t374199898 * value)
	{
		___m_VerticalScrollbarEventHandler_23 = value;
		Il2CppCodeGenWriteBarrier(&___m_VerticalScrollbarEventHandler_23, value);
	}

	inline static int32_t get_offset_of_m_ScrollPosition_24() { return static_cast<int32_t>(offsetof(TMP_InputField_t1099764886, ___m_ScrollPosition_24)); }
	inline float get_m_ScrollPosition_24() const { return ___m_ScrollPosition_24; }
	inline float* get_address_of_m_ScrollPosition_24() { return &___m_ScrollPosition_24; }
	inline void set_m_ScrollPosition_24(float value)
	{
		___m_ScrollPosition_24 = value;
	}

	inline static int32_t get_offset_of_m_ScrollSensitivity_25() { return static_cast<int32_t>(offsetof(TMP_InputField_t1099764886, ___m_ScrollSensitivity_25)); }
	inline float get_m_ScrollSensitivity_25() const { return ___m_ScrollSensitivity_25; }
	inline float* get_address_of_m_ScrollSensitivity_25() { return &___m_ScrollSensitivity_25; }
	inline void set_m_ScrollSensitivity_25(float value)
	{
		___m_ScrollSensitivity_25 = value;
	}

	inline static int32_t get_offset_of_m_ContentType_26() { return static_cast<int32_t>(offsetof(TMP_InputField_t1099764886, ___m_ContentType_26)); }
	inline int32_t get_m_ContentType_26() const { return ___m_ContentType_26; }
	inline int32_t* get_address_of_m_ContentType_26() { return &___m_ContentType_26; }
	inline void set_m_ContentType_26(int32_t value)
	{
		___m_ContentType_26 = value;
	}

	inline static int32_t get_offset_of_m_InputType_27() { return static_cast<int32_t>(offsetof(TMP_InputField_t1099764886, ___m_InputType_27)); }
	inline int32_t get_m_InputType_27() const { return ___m_InputType_27; }
	inline int32_t* get_address_of_m_InputType_27() { return &___m_InputType_27; }
	inline void set_m_InputType_27(int32_t value)
	{
		___m_InputType_27 = value;
	}

	inline static int32_t get_offset_of_m_AsteriskChar_28() { return static_cast<int32_t>(offsetof(TMP_InputField_t1099764886, ___m_AsteriskChar_28)); }
	inline Il2CppChar get_m_AsteriskChar_28() const { return ___m_AsteriskChar_28; }
	inline Il2CppChar* get_address_of_m_AsteriskChar_28() { return &___m_AsteriskChar_28; }
	inline void set_m_AsteriskChar_28(Il2CppChar value)
	{
		___m_AsteriskChar_28 = value;
	}

	inline static int32_t get_offset_of_m_KeyboardType_29() { return static_cast<int32_t>(offsetof(TMP_InputField_t1099764886, ___m_KeyboardType_29)); }
	inline int32_t get_m_KeyboardType_29() const { return ___m_KeyboardType_29; }
	inline int32_t* get_address_of_m_KeyboardType_29() { return &___m_KeyboardType_29; }
	inline void set_m_KeyboardType_29(int32_t value)
	{
		___m_KeyboardType_29 = value;
	}

	inline static int32_t get_offset_of_m_LineType_30() { return static_cast<int32_t>(offsetof(TMP_InputField_t1099764886, ___m_LineType_30)); }
	inline int32_t get_m_LineType_30() const { return ___m_LineType_30; }
	inline int32_t* get_address_of_m_LineType_30() { return &___m_LineType_30; }
	inline void set_m_LineType_30(int32_t value)
	{
		___m_LineType_30 = value;
	}

	inline static int32_t get_offset_of_m_HideMobileInput_31() { return static_cast<int32_t>(offsetof(TMP_InputField_t1099764886, ___m_HideMobileInput_31)); }
	inline bool get_m_HideMobileInput_31() const { return ___m_HideMobileInput_31; }
	inline bool* get_address_of_m_HideMobileInput_31() { return &___m_HideMobileInput_31; }
	inline void set_m_HideMobileInput_31(bool value)
	{
		___m_HideMobileInput_31 = value;
	}

	inline static int32_t get_offset_of_m_CharacterValidation_32() { return static_cast<int32_t>(offsetof(TMP_InputField_t1099764886, ___m_CharacterValidation_32)); }
	inline int32_t get_m_CharacterValidation_32() const { return ___m_CharacterValidation_32; }
	inline int32_t* get_address_of_m_CharacterValidation_32() { return &___m_CharacterValidation_32; }
	inline void set_m_CharacterValidation_32(int32_t value)
	{
		___m_CharacterValidation_32 = value;
	}

	inline static int32_t get_offset_of_m_RegexValue_33() { return static_cast<int32_t>(offsetof(TMP_InputField_t1099764886, ___m_RegexValue_33)); }
	inline String_t* get_m_RegexValue_33() const { return ___m_RegexValue_33; }
	inline String_t** get_address_of_m_RegexValue_33() { return &___m_RegexValue_33; }
	inline void set_m_RegexValue_33(String_t* value)
	{
		___m_RegexValue_33 = value;
		Il2CppCodeGenWriteBarrier(&___m_RegexValue_33, value);
	}

	inline static int32_t get_offset_of_m_GlobalPointSize_34() { return static_cast<int32_t>(offsetof(TMP_InputField_t1099764886, ___m_GlobalPointSize_34)); }
	inline float get_m_GlobalPointSize_34() const { return ___m_GlobalPointSize_34; }
	inline float* get_address_of_m_GlobalPointSize_34() { return &___m_GlobalPointSize_34; }
	inline void set_m_GlobalPointSize_34(float value)
	{
		___m_GlobalPointSize_34 = value;
	}

	inline static int32_t get_offset_of_m_CharacterLimit_35() { return static_cast<int32_t>(offsetof(TMP_InputField_t1099764886, ___m_CharacterLimit_35)); }
	inline int32_t get_m_CharacterLimit_35() const { return ___m_CharacterLimit_35; }
	inline int32_t* get_address_of_m_CharacterLimit_35() { return &___m_CharacterLimit_35; }
	inline void set_m_CharacterLimit_35(int32_t value)
	{
		___m_CharacterLimit_35 = value;
	}

	inline static int32_t get_offset_of_m_OnEndEdit_36() { return static_cast<int32_t>(offsetof(TMP_InputField_t1099764886, ___m_OnEndEdit_36)); }
	inline SubmitEvent_t1343580625 * get_m_OnEndEdit_36() const { return ___m_OnEndEdit_36; }
	inline SubmitEvent_t1343580625 ** get_address_of_m_OnEndEdit_36() { return &___m_OnEndEdit_36; }
	inline void set_m_OnEndEdit_36(SubmitEvent_t1343580625 * value)
	{
		___m_OnEndEdit_36 = value;
		Il2CppCodeGenWriteBarrier(&___m_OnEndEdit_36, value);
	}

	inline static int32_t get_offset_of_m_OnSubmit_37() { return static_cast<int32_t>(offsetof(TMP_InputField_t1099764886, ___m_OnSubmit_37)); }
	inline SubmitEvent_t1343580625 * get_m_OnSubmit_37() const { return ___m_OnSubmit_37; }
	inline SubmitEvent_t1343580625 ** get_address_of_m_OnSubmit_37() { return &___m_OnSubmit_37; }
	inline void set_m_OnSubmit_37(SubmitEvent_t1343580625 * value)
	{
		___m_OnSubmit_37 = value;
		Il2CppCodeGenWriteBarrier(&___m_OnSubmit_37, value);
	}

	inline static int32_t get_offset_of_m_OnSelect_38() { return static_cast<int32_t>(offsetof(TMP_InputField_t1099764886, ___m_OnSelect_38)); }
	inline SelectionEvent_t4268942288 * get_m_OnSelect_38() const { return ___m_OnSelect_38; }
	inline SelectionEvent_t4268942288 ** get_address_of_m_OnSelect_38() { return &___m_OnSelect_38; }
	inline void set_m_OnSelect_38(SelectionEvent_t4268942288 * value)
	{
		___m_OnSelect_38 = value;
		Il2CppCodeGenWriteBarrier(&___m_OnSelect_38, value);
	}

	inline static int32_t get_offset_of_m_OnDeselect_39() { return static_cast<int32_t>(offsetof(TMP_InputField_t1099764886, ___m_OnDeselect_39)); }
	inline SelectionEvent_t4268942288 * get_m_OnDeselect_39() const { return ___m_OnDeselect_39; }
	inline SelectionEvent_t4268942288 ** get_address_of_m_OnDeselect_39() { return &___m_OnDeselect_39; }
	inline void set_m_OnDeselect_39(SelectionEvent_t4268942288 * value)
	{
		___m_OnDeselect_39 = value;
		Il2CppCodeGenWriteBarrier(&___m_OnDeselect_39, value);
	}

	inline static int32_t get_offset_of_m_OnTextSelection_40() { return static_cast<int32_t>(offsetof(TMP_InputField_t1099764886, ___m_OnTextSelection_40)); }
	inline TextSelectionEvent_t1023421581 * get_m_OnTextSelection_40() const { return ___m_OnTextSelection_40; }
	inline TextSelectionEvent_t1023421581 ** get_address_of_m_OnTextSelection_40() { return &___m_OnTextSelection_40; }
	inline void set_m_OnTextSelection_40(TextSelectionEvent_t1023421581 * value)
	{
		___m_OnTextSelection_40 = value;
		Il2CppCodeGenWriteBarrier(&___m_OnTextSelection_40, value);
	}

	inline static int32_t get_offset_of_m_OnEndTextSelection_41() { return static_cast<int32_t>(offsetof(TMP_InputField_t1099764886, ___m_OnEndTextSelection_41)); }
	inline TextSelectionEvent_t1023421581 * get_m_OnEndTextSelection_41() const { return ___m_OnEndTextSelection_41; }
	inline TextSelectionEvent_t1023421581 ** get_address_of_m_OnEndTextSelection_41() { return &___m_OnEndTextSelection_41; }
	inline void set_m_OnEndTextSelection_41(TextSelectionEvent_t1023421581 * value)
	{
		___m_OnEndTextSelection_41 = value;
		Il2CppCodeGenWriteBarrier(&___m_OnEndTextSelection_41, value);
	}

	inline static int32_t get_offset_of_m_OnValueChanged_42() { return static_cast<int32_t>(offsetof(TMP_InputField_t1099764886, ___m_OnValueChanged_42)); }
	inline OnChangeEvent_t4257774360 * get_m_OnValueChanged_42() const { return ___m_OnValueChanged_42; }
	inline OnChangeEvent_t4257774360 ** get_address_of_m_OnValueChanged_42() { return &___m_OnValueChanged_42; }
	inline void set_m_OnValueChanged_42(OnChangeEvent_t4257774360 * value)
	{
		___m_OnValueChanged_42 = value;
		Il2CppCodeGenWriteBarrier(&___m_OnValueChanged_42, value);
	}

	inline static int32_t get_offset_of_m_OnValidateInput_43() { return static_cast<int32_t>(offsetof(TMP_InputField_t1099764886, ___m_OnValidateInput_43)); }
	inline OnValidateInput_t373909109 * get_m_OnValidateInput_43() const { return ___m_OnValidateInput_43; }
	inline OnValidateInput_t373909109 ** get_address_of_m_OnValidateInput_43() { return &___m_OnValidateInput_43; }
	inline void set_m_OnValidateInput_43(OnValidateInput_t373909109 * value)
	{
		___m_OnValidateInput_43 = value;
		Il2CppCodeGenWriteBarrier(&___m_OnValidateInput_43, value);
	}

	inline static int32_t get_offset_of_m_CaretColor_44() { return static_cast<int32_t>(offsetof(TMP_InputField_t1099764886, ___m_CaretColor_44)); }
	inline Color_t2555686324  get_m_CaretColor_44() const { return ___m_CaretColor_44; }
	inline Color_t2555686324 * get_address_of_m_CaretColor_44() { return &___m_CaretColor_44; }
	inline void set_m_CaretColor_44(Color_t2555686324  value)
	{
		___m_CaretColor_44 = value;
	}

	inline static int32_t get_offset_of_m_CustomCaretColor_45() { return static_cast<int32_t>(offsetof(TMP_InputField_t1099764886, ___m_CustomCaretColor_45)); }
	inline bool get_m_CustomCaretColor_45() const { return ___m_CustomCaretColor_45; }
	inline bool* get_address_of_m_CustomCaretColor_45() { return &___m_CustomCaretColor_45; }
	inline void set_m_CustomCaretColor_45(bool value)
	{
		___m_CustomCaretColor_45 = value;
	}

	inline static int32_t get_offset_of_m_SelectionColor_46() { return static_cast<int32_t>(offsetof(TMP_InputField_t1099764886, ___m_SelectionColor_46)); }
	inline Color_t2555686324  get_m_SelectionColor_46() const { return ___m_SelectionColor_46; }
	inline Color_t2555686324 * get_address_of_m_SelectionColor_46() { return &___m_SelectionColor_46; }
	inline void set_m_SelectionColor_46(Color_t2555686324  value)
	{
		___m_SelectionColor_46 = value;
	}

	inline static int32_t get_offset_of_m_Text_47() { return static_cast<int32_t>(offsetof(TMP_InputField_t1099764886, ___m_Text_47)); }
	inline String_t* get_m_Text_47() const { return ___m_Text_47; }
	inline String_t** get_address_of_m_Text_47() { return &___m_Text_47; }
	inline void set_m_Text_47(String_t* value)
	{
		___m_Text_47 = value;
		Il2CppCodeGenWriteBarrier(&___m_Text_47, value);
	}

	inline static int32_t get_offset_of_m_CaretBlinkRate_48() { return static_cast<int32_t>(offsetof(TMP_InputField_t1099764886, ___m_CaretBlinkRate_48)); }
	inline float get_m_CaretBlinkRate_48() const { return ___m_CaretBlinkRate_48; }
	inline float* get_address_of_m_CaretBlinkRate_48() { return &___m_CaretBlinkRate_48; }
	inline void set_m_CaretBlinkRate_48(float value)
	{
		___m_CaretBlinkRate_48 = value;
	}

	inline static int32_t get_offset_of_m_CaretWidth_49() { return static_cast<int32_t>(offsetof(TMP_InputField_t1099764886, ___m_CaretWidth_49)); }
	inline int32_t get_m_CaretWidth_49() const { return ___m_CaretWidth_49; }
	inline int32_t* get_address_of_m_CaretWidth_49() { return &___m_CaretWidth_49; }
	inline void set_m_CaretWidth_49(int32_t value)
	{
		___m_CaretWidth_49 = value;
	}

	inline static int32_t get_offset_of_m_ReadOnly_50() { return static_cast<int32_t>(offsetof(TMP_InputField_t1099764886, ___m_ReadOnly_50)); }
	inline bool get_m_ReadOnly_50() const { return ___m_ReadOnly_50; }
	inline bool* get_address_of_m_ReadOnly_50() { return &___m_ReadOnly_50; }
	inline void set_m_ReadOnly_50(bool value)
	{
		___m_ReadOnly_50 = value;
	}

	inline static int32_t get_offset_of_m_RichText_51() { return static_cast<int32_t>(offsetof(TMP_InputField_t1099764886, ___m_RichText_51)); }
	inline bool get_m_RichText_51() const { return ___m_RichText_51; }
	inline bool* get_address_of_m_RichText_51() { return &___m_RichText_51; }
	inline void set_m_RichText_51(bool value)
	{
		___m_RichText_51 = value;
	}

	inline static int32_t get_offset_of_m_StringPosition_52() { return static_cast<int32_t>(offsetof(TMP_InputField_t1099764886, ___m_StringPosition_52)); }
	inline int32_t get_m_StringPosition_52() const { return ___m_StringPosition_52; }
	inline int32_t* get_address_of_m_StringPosition_52() { return &___m_StringPosition_52; }
	inline void set_m_StringPosition_52(int32_t value)
	{
		___m_StringPosition_52 = value;
	}

	inline static int32_t get_offset_of_m_StringSelectPosition_53() { return static_cast<int32_t>(offsetof(TMP_InputField_t1099764886, ___m_StringSelectPosition_53)); }
	inline int32_t get_m_StringSelectPosition_53() const { return ___m_StringSelectPosition_53; }
	inline int32_t* get_address_of_m_StringSelectPosition_53() { return &___m_StringSelectPosition_53; }
	inline void set_m_StringSelectPosition_53(int32_t value)
	{
		___m_StringSelectPosition_53 = value;
	}

	inline static int32_t get_offset_of_m_CaretPosition_54() { return static_cast<int32_t>(offsetof(TMP_InputField_t1099764886, ___m_CaretPosition_54)); }
	inline int32_t get_m_CaretPosition_54() const { return ___m_CaretPosition_54; }
	inline int32_t* get_address_of_m_CaretPosition_54() { return &___m_CaretPosition_54; }
	inline void set_m_CaretPosition_54(int32_t value)
	{
		___m_CaretPosition_54 = value;
	}

	inline static int32_t get_offset_of_m_CaretSelectPosition_55() { return static_cast<int32_t>(offsetof(TMP_InputField_t1099764886, ___m_CaretSelectPosition_55)); }
	inline int32_t get_m_CaretSelectPosition_55() const { return ___m_CaretSelectPosition_55; }
	inline int32_t* get_address_of_m_CaretSelectPosition_55() { return &___m_CaretSelectPosition_55; }
	inline void set_m_CaretSelectPosition_55(int32_t value)
	{
		___m_CaretSelectPosition_55 = value;
	}

	inline static int32_t get_offset_of_caretRectTrans_56() { return static_cast<int32_t>(offsetof(TMP_InputField_t1099764886, ___caretRectTrans_56)); }
	inline RectTransform_t3704657025 * get_caretRectTrans_56() const { return ___caretRectTrans_56; }
	inline RectTransform_t3704657025 ** get_address_of_caretRectTrans_56() { return &___caretRectTrans_56; }
	inline void set_caretRectTrans_56(RectTransform_t3704657025 * value)
	{
		___caretRectTrans_56 = value;
		Il2CppCodeGenWriteBarrier(&___caretRectTrans_56, value);
	}

	inline static int32_t get_offset_of_m_CursorVerts_57() { return static_cast<int32_t>(offsetof(TMP_InputField_t1099764886, ___m_CursorVerts_57)); }
	inline UIVertexU5BU5D_t1981460040* get_m_CursorVerts_57() const { return ___m_CursorVerts_57; }
	inline UIVertexU5BU5D_t1981460040** get_address_of_m_CursorVerts_57() { return &___m_CursorVerts_57; }
	inline void set_m_CursorVerts_57(UIVertexU5BU5D_t1981460040* value)
	{
		___m_CursorVerts_57 = value;
		Il2CppCodeGenWriteBarrier(&___m_CursorVerts_57, value);
	}

	inline static int32_t get_offset_of_m_CachedInputRenderer_58() { return static_cast<int32_t>(offsetof(TMP_InputField_t1099764886, ___m_CachedInputRenderer_58)); }
	inline CanvasRenderer_t2598313366 * get_m_CachedInputRenderer_58() const { return ___m_CachedInputRenderer_58; }
	inline CanvasRenderer_t2598313366 ** get_address_of_m_CachedInputRenderer_58() { return &___m_CachedInputRenderer_58; }
	inline void set_m_CachedInputRenderer_58(CanvasRenderer_t2598313366 * value)
	{
		___m_CachedInputRenderer_58 = value;
		Il2CppCodeGenWriteBarrier(&___m_CachedInputRenderer_58, value);
	}

	inline static int32_t get_offset_of_m_DefaultTransformPosition_59() { return static_cast<int32_t>(offsetof(TMP_InputField_t1099764886, ___m_DefaultTransformPosition_59)); }
	inline Vector2_t2156229523  get_m_DefaultTransformPosition_59() const { return ___m_DefaultTransformPosition_59; }
	inline Vector2_t2156229523 * get_address_of_m_DefaultTransformPosition_59() { return &___m_DefaultTransformPosition_59; }
	inline void set_m_DefaultTransformPosition_59(Vector2_t2156229523  value)
	{
		___m_DefaultTransformPosition_59 = value;
	}

	inline static int32_t get_offset_of_m_LastPosition_60() { return static_cast<int32_t>(offsetof(TMP_InputField_t1099764886, ___m_LastPosition_60)); }
	inline Vector2_t2156229523  get_m_LastPosition_60() const { return ___m_LastPosition_60; }
	inline Vector2_t2156229523 * get_address_of_m_LastPosition_60() { return &___m_LastPosition_60; }
	inline void set_m_LastPosition_60(Vector2_t2156229523  value)
	{
		___m_LastPosition_60 = value;
	}

	inline static int32_t get_offset_of_m_Mesh_61() { return static_cast<int32_t>(offsetof(TMP_InputField_t1099764886, ___m_Mesh_61)); }
	inline Mesh_t3648964284 * get_m_Mesh_61() const { return ___m_Mesh_61; }
	inline Mesh_t3648964284 ** get_address_of_m_Mesh_61() { return &___m_Mesh_61; }
	inline void set_m_Mesh_61(Mesh_t3648964284 * value)
	{
		___m_Mesh_61 = value;
		Il2CppCodeGenWriteBarrier(&___m_Mesh_61, value);
	}

	inline static int32_t get_offset_of_m_AllowInput_62() { return static_cast<int32_t>(offsetof(TMP_InputField_t1099764886, ___m_AllowInput_62)); }
	inline bool get_m_AllowInput_62() const { return ___m_AllowInput_62; }
	inline bool* get_address_of_m_AllowInput_62() { return &___m_AllowInput_62; }
	inline void set_m_AllowInput_62(bool value)
	{
		___m_AllowInput_62 = value;
	}

	inline static int32_t get_offset_of_m_ShouldActivateNextUpdate_63() { return static_cast<int32_t>(offsetof(TMP_InputField_t1099764886, ___m_ShouldActivateNextUpdate_63)); }
	inline bool get_m_ShouldActivateNextUpdate_63() const { return ___m_ShouldActivateNextUpdate_63; }
	inline bool* get_address_of_m_ShouldActivateNextUpdate_63() { return &___m_ShouldActivateNextUpdate_63; }
	inline void set_m_ShouldActivateNextUpdate_63(bool value)
	{
		___m_ShouldActivateNextUpdate_63 = value;
	}

	inline static int32_t get_offset_of_m_UpdateDrag_64() { return static_cast<int32_t>(offsetof(TMP_InputField_t1099764886, ___m_UpdateDrag_64)); }
	inline bool get_m_UpdateDrag_64() const { return ___m_UpdateDrag_64; }
	inline bool* get_address_of_m_UpdateDrag_64() { return &___m_UpdateDrag_64; }
	inline void set_m_UpdateDrag_64(bool value)
	{
		___m_UpdateDrag_64 = value;
	}

	inline static int32_t get_offset_of_m_DragPositionOutOfBounds_65() { return static_cast<int32_t>(offsetof(TMP_InputField_t1099764886, ___m_DragPositionOutOfBounds_65)); }
	inline bool get_m_DragPositionOutOfBounds_65() const { return ___m_DragPositionOutOfBounds_65; }
	inline bool* get_address_of_m_DragPositionOutOfBounds_65() { return &___m_DragPositionOutOfBounds_65; }
	inline void set_m_DragPositionOutOfBounds_65(bool value)
	{
		___m_DragPositionOutOfBounds_65 = value;
	}

	inline static int32_t get_offset_of_m_CaretVisible_68() { return static_cast<int32_t>(offsetof(TMP_InputField_t1099764886, ___m_CaretVisible_68)); }
	inline bool get_m_CaretVisible_68() const { return ___m_CaretVisible_68; }
	inline bool* get_address_of_m_CaretVisible_68() { return &___m_CaretVisible_68; }
	inline void set_m_CaretVisible_68(bool value)
	{
		___m_CaretVisible_68 = value;
	}

	inline static int32_t get_offset_of_m_BlinkCoroutine_69() { return static_cast<int32_t>(offsetof(TMP_InputField_t1099764886, ___m_BlinkCoroutine_69)); }
	inline Coroutine_t3829159415 * get_m_BlinkCoroutine_69() const { return ___m_BlinkCoroutine_69; }
	inline Coroutine_t3829159415 ** get_address_of_m_BlinkCoroutine_69() { return &___m_BlinkCoroutine_69; }
	inline void set_m_BlinkCoroutine_69(Coroutine_t3829159415 * value)
	{
		___m_BlinkCoroutine_69 = value;
		Il2CppCodeGenWriteBarrier(&___m_BlinkCoroutine_69, value);
	}

	inline static int32_t get_offset_of_m_BlinkStartTime_70() { return static_cast<int32_t>(offsetof(TMP_InputField_t1099764886, ___m_BlinkStartTime_70)); }
	inline float get_m_BlinkStartTime_70() const { return ___m_BlinkStartTime_70; }
	inline float* get_address_of_m_BlinkStartTime_70() { return &___m_BlinkStartTime_70; }
	inline void set_m_BlinkStartTime_70(float value)
	{
		___m_BlinkStartTime_70 = value;
	}

	inline static int32_t get_offset_of_m_DragCoroutine_71() { return static_cast<int32_t>(offsetof(TMP_InputField_t1099764886, ___m_DragCoroutine_71)); }
	inline Coroutine_t3829159415 * get_m_DragCoroutine_71() const { return ___m_DragCoroutine_71; }
	inline Coroutine_t3829159415 ** get_address_of_m_DragCoroutine_71() { return &___m_DragCoroutine_71; }
	inline void set_m_DragCoroutine_71(Coroutine_t3829159415 * value)
	{
		___m_DragCoroutine_71 = value;
		Il2CppCodeGenWriteBarrier(&___m_DragCoroutine_71, value);
	}

	inline static int32_t get_offset_of_m_OriginalText_72() { return static_cast<int32_t>(offsetof(TMP_InputField_t1099764886, ___m_OriginalText_72)); }
	inline String_t* get_m_OriginalText_72() const { return ___m_OriginalText_72; }
	inline String_t** get_address_of_m_OriginalText_72() { return &___m_OriginalText_72; }
	inline void set_m_OriginalText_72(String_t* value)
	{
		___m_OriginalText_72 = value;
		Il2CppCodeGenWriteBarrier(&___m_OriginalText_72, value);
	}

	inline static int32_t get_offset_of_m_WasCanceled_73() { return static_cast<int32_t>(offsetof(TMP_InputField_t1099764886, ___m_WasCanceled_73)); }
	inline bool get_m_WasCanceled_73() const { return ___m_WasCanceled_73; }
	inline bool* get_address_of_m_WasCanceled_73() { return &___m_WasCanceled_73; }
	inline void set_m_WasCanceled_73(bool value)
	{
		___m_WasCanceled_73 = value;
	}

	inline static int32_t get_offset_of_m_HasDoneFocusTransition_74() { return static_cast<int32_t>(offsetof(TMP_InputField_t1099764886, ___m_HasDoneFocusTransition_74)); }
	inline bool get_m_HasDoneFocusTransition_74() const { return ___m_HasDoneFocusTransition_74; }
	inline bool* get_address_of_m_HasDoneFocusTransition_74() { return &___m_HasDoneFocusTransition_74; }
	inline void set_m_HasDoneFocusTransition_74(bool value)
	{
		___m_HasDoneFocusTransition_74 = value;
	}

	inline static int32_t get_offset_of_m_IsScrollbarUpdateRequired_75() { return static_cast<int32_t>(offsetof(TMP_InputField_t1099764886, ___m_IsScrollbarUpdateRequired_75)); }
	inline bool get_m_IsScrollbarUpdateRequired_75() const { return ___m_IsScrollbarUpdateRequired_75; }
	inline bool* get_address_of_m_IsScrollbarUpdateRequired_75() { return &___m_IsScrollbarUpdateRequired_75; }
	inline void set_m_IsScrollbarUpdateRequired_75(bool value)
	{
		___m_IsScrollbarUpdateRequired_75 = value;
	}

	inline static int32_t get_offset_of_m_IsUpdatingScrollbarValues_76() { return static_cast<int32_t>(offsetof(TMP_InputField_t1099764886, ___m_IsUpdatingScrollbarValues_76)); }
	inline bool get_m_IsUpdatingScrollbarValues_76() const { return ___m_IsUpdatingScrollbarValues_76; }
	inline bool* get_address_of_m_IsUpdatingScrollbarValues_76() { return &___m_IsUpdatingScrollbarValues_76; }
	inline void set_m_IsUpdatingScrollbarValues_76(bool value)
	{
		___m_IsUpdatingScrollbarValues_76 = value;
	}

	inline static int32_t get_offset_of_m_isLastKeyBackspace_77() { return static_cast<int32_t>(offsetof(TMP_InputField_t1099764886, ___m_isLastKeyBackspace_77)); }
	inline bool get_m_isLastKeyBackspace_77() const { return ___m_isLastKeyBackspace_77; }
	inline bool* get_address_of_m_isLastKeyBackspace_77() { return &___m_isLastKeyBackspace_77; }
	inline void set_m_isLastKeyBackspace_77(bool value)
	{
		___m_isLastKeyBackspace_77 = value;
	}

	inline static int32_t get_offset_of_m_ClickStartTime_78() { return static_cast<int32_t>(offsetof(TMP_InputField_t1099764886, ___m_ClickStartTime_78)); }
	inline float get_m_ClickStartTime_78() const { return ___m_ClickStartTime_78; }
	inline float* get_address_of_m_ClickStartTime_78() { return &___m_ClickStartTime_78; }
	inline void set_m_ClickStartTime_78(float value)
	{
		___m_ClickStartTime_78 = value;
	}

	inline static int32_t get_offset_of_m_DoubleClickDelay_79() { return static_cast<int32_t>(offsetof(TMP_InputField_t1099764886, ___m_DoubleClickDelay_79)); }
	inline float get_m_DoubleClickDelay_79() const { return ___m_DoubleClickDelay_79; }
	inline float* get_address_of_m_DoubleClickDelay_79() { return &___m_DoubleClickDelay_79; }
	inline void set_m_DoubleClickDelay_79(float value)
	{
		___m_DoubleClickDelay_79 = value;
	}

	inline static int32_t get_offset_of_m_GlobalFontAsset_81() { return static_cast<int32_t>(offsetof(TMP_InputField_t1099764886, ___m_GlobalFontAsset_81)); }
	inline TMP_FontAsset_t364381626 * get_m_GlobalFontAsset_81() const { return ___m_GlobalFontAsset_81; }
	inline TMP_FontAsset_t364381626 ** get_address_of_m_GlobalFontAsset_81() { return &___m_GlobalFontAsset_81; }
	inline void set_m_GlobalFontAsset_81(TMP_FontAsset_t364381626 * value)
	{
		___m_GlobalFontAsset_81 = value;
		Il2CppCodeGenWriteBarrier(&___m_GlobalFontAsset_81, value);
	}

	inline static int32_t get_offset_of_m_OnFocusSelectAll_82() { return static_cast<int32_t>(offsetof(TMP_InputField_t1099764886, ___m_OnFocusSelectAll_82)); }
	inline bool get_m_OnFocusSelectAll_82() const { return ___m_OnFocusSelectAll_82; }
	inline bool* get_address_of_m_OnFocusSelectAll_82() { return &___m_OnFocusSelectAll_82; }
	inline void set_m_OnFocusSelectAll_82(bool value)
	{
		___m_OnFocusSelectAll_82 = value;
	}

	inline static int32_t get_offset_of_m_isSelectAll_83() { return static_cast<int32_t>(offsetof(TMP_InputField_t1099764886, ___m_isSelectAll_83)); }
	inline bool get_m_isSelectAll_83() const { return ___m_isSelectAll_83; }
	inline bool* get_address_of_m_isSelectAll_83() { return &___m_isSelectAll_83; }
	inline void set_m_isSelectAll_83(bool value)
	{
		___m_isSelectAll_83 = value;
	}

	inline static int32_t get_offset_of_m_ResetOnDeActivation_84() { return static_cast<int32_t>(offsetof(TMP_InputField_t1099764886, ___m_ResetOnDeActivation_84)); }
	inline bool get_m_ResetOnDeActivation_84() const { return ___m_ResetOnDeActivation_84; }
	inline bool* get_address_of_m_ResetOnDeActivation_84() { return &___m_ResetOnDeActivation_84; }
	inline void set_m_ResetOnDeActivation_84(bool value)
	{
		___m_ResetOnDeActivation_84 = value;
	}

	inline static int32_t get_offset_of_m_RestoreOriginalTextOnEscape_85() { return static_cast<int32_t>(offsetof(TMP_InputField_t1099764886, ___m_RestoreOriginalTextOnEscape_85)); }
	inline bool get_m_RestoreOriginalTextOnEscape_85() const { return ___m_RestoreOriginalTextOnEscape_85; }
	inline bool* get_address_of_m_RestoreOriginalTextOnEscape_85() { return &___m_RestoreOriginalTextOnEscape_85; }
	inline void set_m_RestoreOriginalTextOnEscape_85(bool value)
	{
		___m_RestoreOriginalTextOnEscape_85 = value;
	}

	inline static int32_t get_offset_of_m_isRichTextEditingAllowed_86() { return static_cast<int32_t>(offsetof(TMP_InputField_t1099764886, ___m_isRichTextEditingAllowed_86)); }
	inline bool get_m_isRichTextEditingAllowed_86() const { return ___m_isRichTextEditingAllowed_86; }
	inline bool* get_address_of_m_isRichTextEditingAllowed_86() { return &___m_isRichTextEditingAllowed_86; }
	inline void set_m_isRichTextEditingAllowed_86(bool value)
	{
		___m_isRichTextEditingAllowed_86 = value;
	}

	inline static int32_t get_offset_of_m_InputValidator_87() { return static_cast<int32_t>(offsetof(TMP_InputField_t1099764886, ___m_InputValidator_87)); }
	inline TMP_InputValidator_t1385053824 * get_m_InputValidator_87() const { return ___m_InputValidator_87; }
	inline TMP_InputValidator_t1385053824 ** get_address_of_m_InputValidator_87() { return &___m_InputValidator_87; }
	inline void set_m_InputValidator_87(TMP_InputValidator_t1385053824 * value)
	{
		___m_InputValidator_87 = value;
		Il2CppCodeGenWriteBarrier(&___m_InputValidator_87, value);
	}

	inline static int32_t get_offset_of_m_isSelected_88() { return static_cast<int32_t>(offsetof(TMP_InputField_t1099764886, ___m_isSelected_88)); }
	inline bool get_m_isSelected_88() const { return ___m_isSelected_88; }
	inline bool* get_address_of_m_isSelected_88() { return &___m_isSelected_88; }
	inline void set_m_isSelected_88(bool value)
	{
		___m_isSelected_88 = value;
	}

	inline static int32_t get_offset_of_isStringPositionDirty_89() { return static_cast<int32_t>(offsetof(TMP_InputField_t1099764886, ___isStringPositionDirty_89)); }
	inline bool get_isStringPositionDirty_89() const { return ___isStringPositionDirty_89; }
	inline bool* get_address_of_isStringPositionDirty_89() { return &___isStringPositionDirty_89; }
	inline void set_isStringPositionDirty_89(bool value)
	{
		___isStringPositionDirty_89 = value;
	}

	inline static int32_t get_offset_of_m_forceRectTransformAdjustment_90() { return static_cast<int32_t>(offsetof(TMP_InputField_t1099764886, ___m_forceRectTransformAdjustment_90)); }
	inline bool get_m_forceRectTransformAdjustment_90() const { return ___m_forceRectTransformAdjustment_90; }
	inline bool* get_address_of_m_forceRectTransformAdjustment_90() { return &___m_forceRectTransformAdjustment_90; }
	inline void set_m_forceRectTransformAdjustment_90(bool value)
	{
		___m_forceRectTransformAdjustment_90 = value;
	}

	inline static int32_t get_offset_of_m_ProcessingEvent_91() { return static_cast<int32_t>(offsetof(TMP_InputField_t1099764886, ___m_ProcessingEvent_91)); }
	inline Event_t2956885303 * get_m_ProcessingEvent_91() const { return ___m_ProcessingEvent_91; }
	inline Event_t2956885303 ** get_address_of_m_ProcessingEvent_91() { return &___m_ProcessingEvent_91; }
	inline void set_m_ProcessingEvent_91(Event_t2956885303 * value)
	{
		___m_ProcessingEvent_91 = value;
		Il2CppCodeGenWriteBarrier(&___m_ProcessingEvent_91, value);
	}
};

struct TMP_InputField_t1099764886_StaticFields
{
public:
	// System.Char[] TMPro.TMP_InputField::kSeparators
	CharU5BU5D_t3528271667* ___kSeparators_17;

public:
	inline static int32_t get_offset_of_kSeparators_17() { return static_cast<int32_t>(offsetof(TMP_InputField_t1099764886_StaticFields, ___kSeparators_17)); }
	inline CharU5BU5D_t3528271667* get_kSeparators_17() const { return ___kSeparators_17; }
	inline CharU5BU5D_t3528271667** get_address_of_kSeparators_17() { return &___kSeparators_17; }
	inline void set_kSeparators_17(CharU5BU5D_t3528271667* value)
	{
		___kSeparators_17 = value;
		Il2CppCodeGenWriteBarrier(&___kSeparators_17, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
