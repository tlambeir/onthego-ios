﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "WikitudeUnityPlugin_Wikitude_Trackable347424808.h"

// System.String[]
struct StringU5BU5D_t1281789340;
// Wikitude.ImageTrackable/OnImageRecognizedEvent
struct OnImageRecognizedEvent_t773132985;
// Wikitude.ImageTrackable/OnImageLostEvent
struct OnImageLostEvent_t966623312;
// UnityEngine.Texture2D
struct Texture2D_t3840446185;
// UnityEngine.Material
struct Material_t340375123;
// UnityEngine.Mesh
struct Mesh_t3648964284;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Wikitude.ImageTrackable
struct  ImageTrackable_t2462741734  : public Trackable_t347424808
{
public:
	// System.Boolean Wikitude.ImageTrackable::_extendedTracking
	bool ____extendedTracking_9;
	// System.String[] Wikitude.ImageTrackable::_targetsForExtendedTracking
	StringU5BU5D_t1281789340* ____targetsForExtendedTracking_10;
	// Wikitude.ImageTrackable/OnImageRecognizedEvent Wikitude.ImageTrackable::OnImageRecognized
	OnImageRecognizedEvent_t773132985 * ___OnImageRecognized_11;
	// Wikitude.ImageTrackable/OnImageLostEvent Wikitude.ImageTrackable::OnImageLost
	OnImageLostEvent_t966623312 * ___OnImageLost_12;
	// UnityEngine.Texture2D Wikitude.ImageTrackable::_preview
	Texture2D_t3840446185 * ____preview_13;
	// UnityEngine.Material Wikitude.ImageTrackable::_previewMaterial
	Material_t340375123 * ____previewMaterial_14;
	// UnityEngine.Mesh Wikitude.ImageTrackable::_previewMesh
	Mesh_t3648964284 * ____previewMesh_15;

public:
	inline static int32_t get_offset_of__extendedTracking_9() { return static_cast<int32_t>(offsetof(ImageTrackable_t2462741734, ____extendedTracking_9)); }
	inline bool get__extendedTracking_9() const { return ____extendedTracking_9; }
	inline bool* get_address_of__extendedTracking_9() { return &____extendedTracking_9; }
	inline void set__extendedTracking_9(bool value)
	{
		____extendedTracking_9 = value;
	}

	inline static int32_t get_offset_of__targetsForExtendedTracking_10() { return static_cast<int32_t>(offsetof(ImageTrackable_t2462741734, ____targetsForExtendedTracking_10)); }
	inline StringU5BU5D_t1281789340* get__targetsForExtendedTracking_10() const { return ____targetsForExtendedTracking_10; }
	inline StringU5BU5D_t1281789340** get_address_of__targetsForExtendedTracking_10() { return &____targetsForExtendedTracking_10; }
	inline void set__targetsForExtendedTracking_10(StringU5BU5D_t1281789340* value)
	{
		____targetsForExtendedTracking_10 = value;
		Il2CppCodeGenWriteBarrier(&____targetsForExtendedTracking_10, value);
	}

	inline static int32_t get_offset_of_OnImageRecognized_11() { return static_cast<int32_t>(offsetof(ImageTrackable_t2462741734, ___OnImageRecognized_11)); }
	inline OnImageRecognizedEvent_t773132985 * get_OnImageRecognized_11() const { return ___OnImageRecognized_11; }
	inline OnImageRecognizedEvent_t773132985 ** get_address_of_OnImageRecognized_11() { return &___OnImageRecognized_11; }
	inline void set_OnImageRecognized_11(OnImageRecognizedEvent_t773132985 * value)
	{
		___OnImageRecognized_11 = value;
		Il2CppCodeGenWriteBarrier(&___OnImageRecognized_11, value);
	}

	inline static int32_t get_offset_of_OnImageLost_12() { return static_cast<int32_t>(offsetof(ImageTrackable_t2462741734, ___OnImageLost_12)); }
	inline OnImageLostEvent_t966623312 * get_OnImageLost_12() const { return ___OnImageLost_12; }
	inline OnImageLostEvent_t966623312 ** get_address_of_OnImageLost_12() { return &___OnImageLost_12; }
	inline void set_OnImageLost_12(OnImageLostEvent_t966623312 * value)
	{
		___OnImageLost_12 = value;
		Il2CppCodeGenWriteBarrier(&___OnImageLost_12, value);
	}

	inline static int32_t get_offset_of__preview_13() { return static_cast<int32_t>(offsetof(ImageTrackable_t2462741734, ____preview_13)); }
	inline Texture2D_t3840446185 * get__preview_13() const { return ____preview_13; }
	inline Texture2D_t3840446185 ** get_address_of__preview_13() { return &____preview_13; }
	inline void set__preview_13(Texture2D_t3840446185 * value)
	{
		____preview_13 = value;
		Il2CppCodeGenWriteBarrier(&____preview_13, value);
	}

	inline static int32_t get_offset_of__previewMaterial_14() { return static_cast<int32_t>(offsetof(ImageTrackable_t2462741734, ____previewMaterial_14)); }
	inline Material_t340375123 * get__previewMaterial_14() const { return ____previewMaterial_14; }
	inline Material_t340375123 ** get_address_of__previewMaterial_14() { return &____previewMaterial_14; }
	inline void set__previewMaterial_14(Material_t340375123 * value)
	{
		____previewMaterial_14 = value;
		Il2CppCodeGenWriteBarrier(&____previewMaterial_14, value);
	}

	inline static int32_t get_offset_of__previewMesh_15() { return static_cast<int32_t>(offsetof(ImageTrackable_t2462741734, ____previewMesh_15)); }
	inline Mesh_t3648964284 * get__previewMesh_15() const { return ____previewMesh_15; }
	inline Mesh_t3648964284 ** get_address_of__previewMesh_15() { return &____previewMesh_15; }
	inline void set__previewMesh_15(Mesh_t3648964284 * value)
	{
		____previewMesh_15 = value;
		Il2CppCodeGenWriteBarrier(&____previewMesh_15, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
