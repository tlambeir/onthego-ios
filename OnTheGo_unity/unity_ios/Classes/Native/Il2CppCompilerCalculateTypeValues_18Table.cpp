﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UI_UnityEngine_UI_ReflectionMethodsCac2103211062.h"
#include "UnityEngine_UI_UnityEngine_UI_ReflectionMethodsCach701940803.h"
#include "UnityEngine_UI_UnityEngine_UI_ReflectionMethodsCach768590915.h"
#include "UnityEngine_UI_UnityEngine_UI_ReflectionMethodsCac1884415901.h"
#include "UnityEngine_UI_UnityEngine_UI_ReflectionMethodsCac3913627115.h"
#include "UnityEngine_UI_UnityEngine_UI_VertexHelper2453304189.h"
#include "UnityEngine_UI_UnityEngine_UI_BaseVertexEffect2675891272.h"
#include "UnityEngine_UI_UnityEngine_UI_BaseMeshEffect2440176439.h"
#include "UnityEngine_UI_UnityEngine_UI_Outline2536100125.h"
#include "UnityEngine_UI_UnityEngine_UI_PositionAsUV13991086357.h"
#include "UnityEngine_UI_UnityEngine_UI_Shadow773074319.h"
#include "UnityEngine_UI_U3CPrivateImplementationDetailsU3E3057255361.h"
#include "UnityEngine_UI_U3CPrivateImplementationDetailsU3E_2488454196.h"
#include "TextMeshProU2D1_0_55_56_0b12_U3CModuleU3E692745525.h"
#include "TextMeshProU2D1_0_55_56_0b12_TMPro_FastAction3491443480.h"
#include "TextMeshProU2D1_0_55_56_0b12_TMPro_InlineGraphic2901727699.h"
#include "TextMeshProU2D1_0_55_56_0b12_TMPro_InlineGraphicMa2871008645.h"
#include "TextMeshProU2D1_0_55_56_0b12_TMPro_InlineGraphicMa1214464005.h"
#include "TextMeshProU2D1_0_55_56_0b12_TMPro_InlineGraphicMa1214529541.h"
#include "TextMeshProU2D1_0_55_56_0b12_TMPro_MaterialReferen2114976864.h"
#include "TextMeshProU2D1_0_55_56_0b12_TMPro_MaterialReferen1952344632.h"
#include "TextMeshProU2D1_0_55_56_0b12_TMPro_TMP_Asset2469957285.h"
#include "TextMeshProU2D1_0_55_56_0b12_TMPro_TMP_ColorGradie3678055768.h"
#include "TextMeshProU2D1_0_55_56_0b12_TMPro_TMP_Compatibili2923561388.h"
#include "TextMeshProU2D1_0_55_56_0b12_TMPro_TMP_Compatibili1063569770.h"
#include "TextMeshProU2D1_0_55_56_0b12_TMPro_ColorTween378116136.h"
#include "TextMeshProU2D1_0_55_56_0b12_TMPro_ColorTween_Colo4194101034.h"
#include "TextMeshProU2D1_0_55_56_0b12_TMPro_ColorTween_Color824784811.h"
#include "TextMeshProU2D1_0_55_56_0b12_TMPro_FloatTween3783157226.h"
#include "TextMeshProU2D1_0_55_56_0b12_TMPro_FloatTween_Float556094317.h"
#include "TextMeshProU2D1_0_55_56_0b12_TMPro_TMP_DefaultCont2837752704.h"
#include "TextMeshProU2D1_0_55_56_0b12_TMPro_TMP_DefaultCont2155109485.h"
#include "TextMeshProU2D1_0_55_56_0b12_TMPro_TMP_Dropdown3024694699.h"
#include "TextMeshProU2D1_0_55_56_0b12_TMPro_TMP_Dropdown_Dr1842596711.h"
#include "TextMeshProU2D1_0_55_56_0b12_TMPro_TMP_Dropdown_Op1114640268.h"
#include "TextMeshProU2D1_0_55_56_0b12_TMPro_TMP_Dropdown_Op2293557512.h"
#include "TextMeshProU2D1_0_55_56_0b12_TMPro_TMP_Dropdown_Dr1704673280.h"
#include "TextMeshProU2D1_0_55_56_0b12_TMPro_TMP_Dropdown_U32378633237.h"
#include "TextMeshProU2D1_0_55_56_0b12_TMPro_TMP_Dropdown_U33448141726.h"
#include "TextMeshProU2D1_0_55_56_0b12_TMPro_TMP_FontWeights916301067.h"
#include "TextMeshProU2D1_0_55_56_0b12_TMPro_TMP_FontAsset364381626.h"
#include "TextMeshProU2D1_0_55_56_0b12_TMPro_TMP_FontAsset_F1222456209.h"
#include "TextMeshProU2D1_0_55_56_0b12_TMPro_TMP_FontAsset_U2662116725.h"
#include "TextMeshProU2D1_0_55_56_0b12_TMPro_TMP_InputField1099764886.h"
#include "TextMeshProU2D1_0_55_56_0b12_TMPro_TMP_InputField_1128941285.h"
#include "TextMeshProU2D1_0_55_56_0b12_TMPro_TMP_InputField_2510134850.h"
#include "TextMeshProU2D1_0_55_56_0b12_TMPro_TMP_InputField_3801396403.h"
#include "TextMeshProU2D1_0_55_56_0b12_TMPro_TMP_InputField_2817627283.h"
#include "TextMeshProU2D1_0_55_56_0b12_TMPro_TMP_InputField_O373909109.h"
#include "TextMeshProU2D1_0_55_56_0b12_TMPro_TMP_InputField_1343580625.h"
#include "TextMeshProU2D1_0_55_56_0b12_TMPro_TMP_InputField_4257774360.h"
#include "TextMeshProU2D1_0_55_56_0b12_TMPro_TMP_InputField_4268942288.h"
#include "TextMeshProU2D1_0_55_56_0b12_TMPro_TMP_InputField_1023421581.h"
#include "TextMeshProU2D1_0_55_56_0b12_TMPro_TMP_InputField_2966235475.h"
#include "TextMeshProU2D1_0_55_56_0b12_TMPro_TMP_InputField_3270037332.h"
#include "TextMeshProU2D1_0_55_56_0b12_TMPro_TMP_InputField_2073557366.h"
#include "TextMeshProU2D1_0_55_56_0b12_TMPro_SetPropertyUtil2931591262.h"
#include "TextMeshProU2D1_0_55_56_0b12_TMPro_TMP_InputValida1385053824.h"
#include "TextMeshProU2D1_0_55_56_0b12_TMPro_TMP_LineInfo1079631636.h"
#include "TextMeshProU2D1_0_55_56_0b12_TMPro_TMP_MaterialMan2944071966.h"
#include "TextMeshProU2D1_0_55_56_0b12_TMPro_TMP_MaterialMan4171378819.h"
#include "TextMeshProU2D1_0_55_56_0b12_TMPro_TMP_MaterialManag16896275.h"
#include "TextMeshProU2D1_0_55_56_0b12_TMPro_TMP_MaterialMana173182959.h"
#include "TextMeshProU2D1_0_55_56_0b12_TMPro_TMP_MaterialMana173182957.h"
#include "TextMeshProU2D1_0_55_56_0b12_TMPro_TMP_MaterialMana173182956.h"
#include "TextMeshProU2D1_0_55_56_0b12_TMPro_TMP_MaterialMana173182963.h"
#include "TextMeshProU2D1_0_55_56_0b12_TMPro_VertexSortingOr2659893934.h"
#include "TextMeshProU2D1_0_55_56_0b12_TMPro_TMP_MeshInfo2771747634.h"
#include "TextMeshProU2D1_0_55_56_0b12_TMPro_TMP_PhoneNumberV743649728.h"
#include "TextMeshProU2D1_0_55_56_0b12_TMPro_TMP_ScrollbarEve374199898.h"
#include "TextMeshProU2D1_0_55_56_0b12_TMPro_TMP_SelectionCar407257285.h"
#include "TextMeshProU2D1_0_55_56_0b12_TMPro_TMP_Settings2071156274.h"
#include "TextMeshProU2D1_0_55_56_0b12_TMPro_TMP_Settings_Li1457697482.h"
#include "TextMeshProU2D1_0_55_56_0b12_TMPro_TMP_Sprite554067146.h"
#include "TextMeshProU2D1_0_55_56_0b12_TMPro_TMP_SpriteAnima2836635477.h"
#include "TextMeshProU2D1_0_55_56_0b12_TMPro_TMP_SpriteAnimat549195582.h"
#include "TextMeshProU2D1_0_55_56_0b12_TMPro_TMP_SpriteAsset484820633.h"
#include "TextMeshProU2D1_0_55_56_0b12_TMPro_TMP_Style3479380764.h"
#include "TextMeshProU2D1_0_55_56_0b12_TMPro_TMP_StyleSheet917564226.h"
#include "TextMeshProU2D1_0_55_56_0b12_TMPro_TMP_SubMesh2613037997.h"
#include "TextMeshProU2D1_0_55_56_0b12_TMPro_TMP_SubMeshUI1578871311.h"
#include "TextMeshProU2D1_0_55_56_0b12_TMPro_TextAlignmentOp4036791236.h"
#include "TextMeshProU2D1_0_55_56_0b12_TMPro__HorizontalAlig2379014378.h"
#include "TextMeshProU2D1_0_55_56_0b12_TMPro__VerticalAlignm2825528260.h"







#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1800 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1801 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1801[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1802 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1802[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1803 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1803[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1804 = { sizeof (ReflectionMethodsCache_t2103211062), -1, sizeof(ReflectionMethodsCache_t2103211062_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1804[5] = 
{
	ReflectionMethodsCache_t2103211062::get_offset_of_raycast3D_0(),
	ReflectionMethodsCache_t2103211062::get_offset_of_raycast3DAll_1(),
	ReflectionMethodsCache_t2103211062::get_offset_of_raycast2D_2(),
	ReflectionMethodsCache_t2103211062::get_offset_of_getRayIntersectionAll_3(),
	ReflectionMethodsCache_t2103211062_StaticFields::get_offset_of_s_ReflectionMethodsCache_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1805 = { sizeof (Raycast3DCallback_t701940803), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1806 = { sizeof (Raycast2DCallback_t768590915), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1807 = { sizeof (RaycastAllCallback_t1884415901), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1808 = { sizeof (GetRayIntersectionAllCallback_t3913627115), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1809 = { sizeof (VertexHelper_t2453304189), -1, sizeof(VertexHelper_t2453304189_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1809[11] = 
{
	VertexHelper_t2453304189::get_offset_of_m_Positions_0(),
	VertexHelper_t2453304189::get_offset_of_m_Colors_1(),
	VertexHelper_t2453304189::get_offset_of_m_Uv0S_2(),
	VertexHelper_t2453304189::get_offset_of_m_Uv1S_3(),
	VertexHelper_t2453304189::get_offset_of_m_Uv2S_4(),
	VertexHelper_t2453304189::get_offset_of_m_Uv3S_5(),
	VertexHelper_t2453304189::get_offset_of_m_Normals_6(),
	VertexHelper_t2453304189::get_offset_of_m_Tangents_7(),
	VertexHelper_t2453304189::get_offset_of_m_Indices_8(),
	VertexHelper_t2453304189_StaticFields::get_offset_of_s_DefaultTangent_9(),
	VertexHelper_t2453304189_StaticFields::get_offset_of_s_DefaultNormal_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1810 = { sizeof (BaseVertexEffect_t2675891272), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1811 = { sizeof (BaseMeshEffect_t2440176439), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1811[1] = 
{
	BaseMeshEffect_t2440176439::get_offset_of_m_Graphic_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1812 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1813 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1814 = { sizeof (Outline_t2536100125), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1815 = { sizeof (PositionAsUV1_t3991086357), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1816 = { sizeof (Shadow_t773074319), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1816[4] = 
{
	Shadow_t773074319::get_offset_of_m_EffectColor_3(),
	Shadow_t773074319::get_offset_of_m_EffectDistance_4(),
	Shadow_t773074319::get_offset_of_m_UseGraphicAlpha_5(),
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1817 = { sizeof (U3CPrivateImplementationDetailsU3E_t3057255365), -1, sizeof(U3CPrivateImplementationDetailsU3E_t3057255365_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1817[1] = 
{
	U3CPrivateImplementationDetailsU3E_t3057255365_StaticFields::get_offset_of_U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1818 = { sizeof (U24ArrayTypeU3D12_t2488454196)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU3D12_t2488454196 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1819 = { sizeof (U3CModuleU3E_t692745531), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1820 = { sizeof (FastAction_t3491443480), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1820[2] = 
{
	FastAction_t3491443480::get_offset_of_delegates_0(),
	FastAction_t3491443480::get_offset_of_lookup_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1821 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1821[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1822 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1822[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1823 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1823[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1824 = { sizeof (InlineGraphic_t2901727699), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1824[4] = 
{
	InlineGraphic_t2901727699::get_offset_of_texture_28(),
	InlineGraphic_t2901727699::get_offset_of_m_manager_29(),
	InlineGraphic_t2901727699::get_offset_of_m_RectTransform_30(),
	InlineGraphic_t2901727699::get_offset_of_m_ParentRectTransform_31(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1825 = { sizeof (InlineGraphicManager_t2871008645), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1825[7] = 
{
	InlineGraphicManager_t2871008645::get_offset_of_m_spriteAsset_2(),
	InlineGraphicManager_t2871008645::get_offset_of_m_inlineGraphic_3(),
	InlineGraphicManager_t2871008645::get_offset_of_m_inlineGraphicCanvasRenderer_4(),
	InlineGraphicManager_t2871008645::get_offset_of_m_uiVertex_5(),
	InlineGraphicManager_t2871008645::get_offset_of_m_inlineGraphicRectTransform_6(),
	InlineGraphicManager_t2871008645::get_offset_of_m_textComponent_7(),
	InlineGraphicManager_t2871008645::get_offset_of_m_isInitialized_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1826 = { sizeof (U3CU3Ec__DisplayClass28_0_t1214464005), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1826[1] = 
{
	U3CU3Ec__DisplayClass28_0_t1214464005::get_offset_of_hashCode_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1827 = { sizeof (U3CU3Ec__DisplayClass29_0_t1214529541), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1827[1] = 
{
	U3CU3Ec__DisplayClass29_0_t1214529541::get_offset_of_index_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1828 = { sizeof (MaterialReferenceManager_t2114976864), -1, sizeof(MaterialReferenceManager_t2114976864_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1828[4] = 
{
	MaterialReferenceManager_t2114976864_StaticFields::get_offset_of_s_Instance_0(),
	MaterialReferenceManager_t2114976864::get_offset_of_m_FontMaterialReferenceLookup_1(),
	MaterialReferenceManager_t2114976864::get_offset_of_m_FontAssetReferenceLookup_2(),
	MaterialReferenceManager_t2114976864::get_offset_of_m_SpriteAssetReferenceLookup_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1829 = { sizeof (MaterialReference_t1952344632)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1829[9] = 
{
	MaterialReference_t1952344632::get_offset_of_index_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	MaterialReference_t1952344632::get_offset_of_fontAsset_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	MaterialReference_t1952344632::get_offset_of_spriteAsset_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	MaterialReference_t1952344632::get_offset_of_material_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	MaterialReference_t1952344632::get_offset_of_isDefaultMaterial_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	MaterialReference_t1952344632::get_offset_of_isFallbackMaterial_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
	MaterialReference_t1952344632::get_offset_of_fallbackMaterial_6() + static_cast<int32_t>(sizeof(Il2CppObject)),
	MaterialReference_t1952344632::get_offset_of_padding_7() + static_cast<int32_t>(sizeof(Il2CppObject)),
	MaterialReference_t1952344632::get_offset_of_referenceCount_8() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1830 = { sizeof (TMP_Asset_t2469957285), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1830[3] = 
{
	TMP_Asset_t2469957285::get_offset_of_hashCode_2(),
	TMP_Asset_t2469957285::get_offset_of_material_3(),
	TMP_Asset_t2469957285::get_offset_of_materialHashCode_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1831 = { sizeof (TMP_ColorGradient_t3678055768), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1831[4] = 
{
	TMP_ColorGradient_t3678055768::get_offset_of_topLeft_2(),
	TMP_ColorGradient_t3678055768::get_offset_of_topRight_3(),
	TMP_ColorGradient_t3678055768::get_offset_of_bottomLeft_4(),
	TMP_ColorGradient_t3678055768::get_offset_of_bottomRight_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1832 = { sizeof (TMP_Compatibility_t2923561388), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1833 = { sizeof (AnchorPositions_t1063569770)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1833[12] = 
{
	AnchorPositions_t1063569770::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1834 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1835 = { sizeof (ColorTween_t378116136)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1835[6] = 
{
	ColorTween_t378116136::get_offset_of_m_Target_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ColorTween_t378116136::get_offset_of_m_StartColor_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ColorTween_t378116136::get_offset_of_m_TargetColor_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ColorTween_t378116136::get_offset_of_m_TweenMode_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ColorTween_t378116136::get_offset_of_m_Duration_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ColorTween_t378116136::get_offset_of_m_IgnoreTimeScale_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1836 = { sizeof (ColorTweenMode_t4194101034)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1836[4] = 
{
	ColorTweenMode_t4194101034::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1837 = { sizeof (ColorTweenCallback_t824784811), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1838 = { sizeof (FloatTween_t3783157226)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1838[5] = 
{
	FloatTween_t3783157226::get_offset_of_m_Target_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	FloatTween_t3783157226::get_offset_of_m_StartValue_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	FloatTween_t3783157226::get_offset_of_m_TargetValue_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	FloatTween_t3783157226::get_offset_of_m_Duration_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	FloatTween_t3783157226::get_offset_of_m_IgnoreTimeScale_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1839 = { sizeof (FloatTweenCallback_t556094317), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1840 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1840[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1841 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1841[5] = 
{
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1842 = { sizeof (TMP_DefaultControls_t2837752704), -1, sizeof(TMP_DefaultControls_t2837752704_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1842[7] = 
{
	0,
	0,
	0,
	TMP_DefaultControls_t2837752704_StaticFields::get_offset_of_s_ThickElementSize_3(),
	TMP_DefaultControls_t2837752704_StaticFields::get_offset_of_s_ThinElementSize_4(),
	TMP_DefaultControls_t2837752704_StaticFields::get_offset_of_s_DefaultSelectableColor_5(),
	TMP_DefaultControls_t2837752704_StaticFields::get_offset_of_s_TextColor_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1843 = { sizeof (Resources_t2155109485)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1843[7] = 
{
	Resources_t2155109485::get_offset_of_standard_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Resources_t2155109485::get_offset_of_background_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Resources_t2155109485::get_offset_of_inputField_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Resources_t2155109485::get_offset_of_knob_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Resources_t2155109485::get_offset_of_checkmark_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Resources_t2155109485::get_offset_of_dropdown_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Resources_t2155109485::get_offset_of_mask_6() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1844 = { sizeof (TMP_Dropdown_t3024694699), -1, sizeof(TMP_Dropdown_t3024694699_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1844[14] = 
{
	TMP_Dropdown_t3024694699::get_offset_of_m_Template_16(),
	TMP_Dropdown_t3024694699::get_offset_of_m_CaptionText_17(),
	TMP_Dropdown_t3024694699::get_offset_of_m_CaptionImage_18(),
	TMP_Dropdown_t3024694699::get_offset_of_m_ItemText_19(),
	TMP_Dropdown_t3024694699::get_offset_of_m_ItemImage_20(),
	TMP_Dropdown_t3024694699::get_offset_of_m_Value_21(),
	TMP_Dropdown_t3024694699::get_offset_of_m_Options_22(),
	TMP_Dropdown_t3024694699::get_offset_of_m_OnValueChanged_23(),
	TMP_Dropdown_t3024694699::get_offset_of_m_Dropdown_24(),
	TMP_Dropdown_t3024694699::get_offset_of_m_Blocker_25(),
	TMP_Dropdown_t3024694699::get_offset_of_m_Items_26(),
	TMP_Dropdown_t3024694699::get_offset_of_m_AlphaTweenRunner_27(),
	TMP_Dropdown_t3024694699::get_offset_of_validTemplate_28(),
	TMP_Dropdown_t3024694699_StaticFields::get_offset_of_s_NoOptionData_29(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1845 = { sizeof (DropdownItem_t1842596711), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1845[4] = 
{
	DropdownItem_t1842596711::get_offset_of_m_Text_2(),
	DropdownItem_t1842596711::get_offset_of_m_Image_3(),
	DropdownItem_t1842596711::get_offset_of_m_RectTransform_4(),
	DropdownItem_t1842596711::get_offset_of_m_Toggle_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1846 = { sizeof (OptionData_t1114640268), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1846[2] = 
{
	OptionData_t1114640268::get_offset_of_m_Text_0(),
	OptionData_t1114640268::get_offset_of_m_Image_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1847 = { sizeof (OptionDataList_t2293557512), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1847[1] = 
{
	OptionDataList_t2293557512::get_offset_of_m_Options_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1848 = { sizeof (DropdownEvent_t1704673280), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1849 = { sizeof (U3CU3Ec__DisplayClass56_0_t2378633237), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1849[2] = 
{
	U3CU3Ec__DisplayClass56_0_t2378633237::get_offset_of_item_0(),
	U3CU3Ec__DisplayClass56_0_t2378633237::get_offset_of_U3CU3E4__this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1850 = { sizeof (U3CDelayedDestroyDropdownListU3Ed__68_t3448141726), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1850[5] = 
{
	U3CDelayedDestroyDropdownListU3Ed__68_t3448141726::get_offset_of_U3CU3E1__state_0(),
	U3CDelayedDestroyDropdownListU3Ed__68_t3448141726::get_offset_of_U3CU3E2__current_1(),
	U3CDelayedDestroyDropdownListU3Ed__68_t3448141726::get_offset_of_delay_2(),
	U3CDelayedDestroyDropdownListU3Ed__68_t3448141726::get_offset_of_U3CU3E4__this_3(),
	U3CDelayedDestroyDropdownListU3Ed__68_t3448141726::get_offset_of_U3CiU3E5__1_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1851 = { sizeof (TMP_FontWeights_t916301067)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1851[2] = 
{
	TMP_FontWeights_t916301067::get_offset_of_regularTypeface_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TMP_FontWeights_t916301067::get_offset_of_italicTypeface_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1852 = { sizeof (TMP_FontAsset_t364381626), -1, sizeof(TMP_FontAsset_t364381626_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1852[20] = 
{
	TMP_FontAsset_t364381626_StaticFields::get_offset_of_s_defaultFontAsset_5(),
	TMP_FontAsset_t364381626::get_offset_of_fontAssetType_6(),
	TMP_FontAsset_t364381626::get_offset_of_m_fontInfo_7(),
	TMP_FontAsset_t364381626::get_offset_of_atlas_8(),
	TMP_FontAsset_t364381626::get_offset_of_m_glyphInfoList_9(),
	TMP_FontAsset_t364381626::get_offset_of_m_characterDictionary_10(),
	TMP_FontAsset_t364381626::get_offset_of_m_kerningDictionary_11(),
	TMP_FontAsset_t364381626::get_offset_of_m_kerningInfo_12(),
	TMP_FontAsset_t364381626::get_offset_of_m_kerningPair_13(),
	TMP_FontAsset_t364381626::get_offset_of_fallbackFontAssets_14(),
	TMP_FontAsset_t364381626::get_offset_of_fontCreationSettings_15(),
	TMP_FontAsset_t364381626::get_offset_of_fontWeights_16(),
	TMP_FontAsset_t364381626::get_offset_of_m_characterSet_17(),
	TMP_FontAsset_t364381626::get_offset_of_normalStyle_18(),
	TMP_FontAsset_t364381626::get_offset_of_normalSpacingOffset_19(),
	TMP_FontAsset_t364381626::get_offset_of_boldStyle_20(),
	TMP_FontAsset_t364381626::get_offset_of_boldSpacing_21(),
	TMP_FontAsset_t364381626::get_offset_of_italicStyle_22(),
	TMP_FontAsset_t364381626::get_offset_of_tabSize_23(),
	TMP_FontAsset_t364381626::get_offset_of_m_oldTabSize_24(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1853 = { sizeof (FontAssetTypes_t1222456209)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1853[4] = 
{
	FontAssetTypes_t1222456209::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1854 = { sizeof (U3CU3Ec_t2662116725), -1, sizeof(U3CU3Ec_t2662116725_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1854[3] = 
{
	U3CU3Ec_t2662116725_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_t2662116725_StaticFields::get_offset_of_U3CU3E9__34_0_1(),
	U3CU3Ec_t2662116725_StaticFields::get_offset_of_U3CU3E9__37_0_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1855 = { sizeof (TMP_InputField_t1099764886), -1, sizeof(TMP_InputField_t1099764886_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1855[76] = 
{
	TMP_InputField_t1099764886::get_offset_of_m_Keyboard_16(),
	TMP_InputField_t1099764886_StaticFields::get_offset_of_kSeparators_17(),
	TMP_InputField_t1099764886::get_offset_of_m_TextViewport_18(),
	TMP_InputField_t1099764886::get_offset_of_m_TextComponent_19(),
	TMP_InputField_t1099764886::get_offset_of_m_TextComponentRectTransform_20(),
	TMP_InputField_t1099764886::get_offset_of_m_Placeholder_21(),
	TMP_InputField_t1099764886::get_offset_of_m_VerticalScrollbar_22(),
	TMP_InputField_t1099764886::get_offset_of_m_VerticalScrollbarEventHandler_23(),
	TMP_InputField_t1099764886::get_offset_of_m_ScrollPosition_24(),
	TMP_InputField_t1099764886::get_offset_of_m_ScrollSensitivity_25(),
	TMP_InputField_t1099764886::get_offset_of_m_ContentType_26(),
	TMP_InputField_t1099764886::get_offset_of_m_InputType_27(),
	TMP_InputField_t1099764886::get_offset_of_m_AsteriskChar_28(),
	TMP_InputField_t1099764886::get_offset_of_m_KeyboardType_29(),
	TMP_InputField_t1099764886::get_offset_of_m_LineType_30(),
	TMP_InputField_t1099764886::get_offset_of_m_HideMobileInput_31(),
	TMP_InputField_t1099764886::get_offset_of_m_CharacterValidation_32(),
	TMP_InputField_t1099764886::get_offset_of_m_RegexValue_33(),
	TMP_InputField_t1099764886::get_offset_of_m_GlobalPointSize_34(),
	TMP_InputField_t1099764886::get_offset_of_m_CharacterLimit_35(),
	TMP_InputField_t1099764886::get_offset_of_m_OnEndEdit_36(),
	TMP_InputField_t1099764886::get_offset_of_m_OnSubmit_37(),
	TMP_InputField_t1099764886::get_offset_of_m_OnSelect_38(),
	TMP_InputField_t1099764886::get_offset_of_m_OnDeselect_39(),
	TMP_InputField_t1099764886::get_offset_of_m_OnTextSelection_40(),
	TMP_InputField_t1099764886::get_offset_of_m_OnEndTextSelection_41(),
	TMP_InputField_t1099764886::get_offset_of_m_OnValueChanged_42(),
	TMP_InputField_t1099764886::get_offset_of_m_OnValidateInput_43(),
	TMP_InputField_t1099764886::get_offset_of_m_CaretColor_44(),
	TMP_InputField_t1099764886::get_offset_of_m_CustomCaretColor_45(),
	TMP_InputField_t1099764886::get_offset_of_m_SelectionColor_46(),
	TMP_InputField_t1099764886::get_offset_of_m_Text_47(),
	TMP_InputField_t1099764886::get_offset_of_m_CaretBlinkRate_48(),
	TMP_InputField_t1099764886::get_offset_of_m_CaretWidth_49(),
	TMP_InputField_t1099764886::get_offset_of_m_ReadOnly_50(),
	TMP_InputField_t1099764886::get_offset_of_m_RichText_51(),
	TMP_InputField_t1099764886::get_offset_of_m_StringPosition_52(),
	TMP_InputField_t1099764886::get_offset_of_m_StringSelectPosition_53(),
	TMP_InputField_t1099764886::get_offset_of_m_CaretPosition_54(),
	TMP_InputField_t1099764886::get_offset_of_m_CaretSelectPosition_55(),
	TMP_InputField_t1099764886::get_offset_of_caretRectTrans_56(),
	TMP_InputField_t1099764886::get_offset_of_m_CursorVerts_57(),
	TMP_InputField_t1099764886::get_offset_of_m_CachedInputRenderer_58(),
	TMP_InputField_t1099764886::get_offset_of_m_DefaultTransformPosition_59(),
	TMP_InputField_t1099764886::get_offset_of_m_LastPosition_60(),
	TMP_InputField_t1099764886::get_offset_of_m_Mesh_61(),
	TMP_InputField_t1099764886::get_offset_of_m_AllowInput_62(),
	TMP_InputField_t1099764886::get_offset_of_m_ShouldActivateNextUpdate_63(),
	TMP_InputField_t1099764886::get_offset_of_m_UpdateDrag_64(),
	TMP_InputField_t1099764886::get_offset_of_m_DragPositionOutOfBounds_65(),
	0,
	0,
	TMP_InputField_t1099764886::get_offset_of_m_CaretVisible_68(),
	TMP_InputField_t1099764886::get_offset_of_m_BlinkCoroutine_69(),
	TMP_InputField_t1099764886::get_offset_of_m_BlinkStartTime_70(),
	TMP_InputField_t1099764886::get_offset_of_m_DragCoroutine_71(),
	TMP_InputField_t1099764886::get_offset_of_m_OriginalText_72(),
	TMP_InputField_t1099764886::get_offset_of_m_WasCanceled_73(),
	TMP_InputField_t1099764886::get_offset_of_m_HasDoneFocusTransition_74(),
	TMP_InputField_t1099764886::get_offset_of_m_IsScrollbarUpdateRequired_75(),
	TMP_InputField_t1099764886::get_offset_of_m_IsUpdatingScrollbarValues_76(),
	TMP_InputField_t1099764886::get_offset_of_m_isLastKeyBackspace_77(),
	TMP_InputField_t1099764886::get_offset_of_m_ClickStartTime_78(),
	TMP_InputField_t1099764886::get_offset_of_m_DoubleClickDelay_79(),
	0,
	TMP_InputField_t1099764886::get_offset_of_m_GlobalFontAsset_81(),
	TMP_InputField_t1099764886::get_offset_of_m_OnFocusSelectAll_82(),
	TMP_InputField_t1099764886::get_offset_of_m_isSelectAll_83(),
	TMP_InputField_t1099764886::get_offset_of_m_ResetOnDeActivation_84(),
	TMP_InputField_t1099764886::get_offset_of_m_RestoreOriginalTextOnEscape_85(),
	TMP_InputField_t1099764886::get_offset_of_m_isRichTextEditingAllowed_86(),
	TMP_InputField_t1099764886::get_offset_of_m_InputValidator_87(),
	TMP_InputField_t1099764886::get_offset_of_m_isSelected_88(),
	TMP_InputField_t1099764886::get_offset_of_isStringPositionDirty_89(),
	TMP_InputField_t1099764886::get_offset_of_m_forceRectTransformAdjustment_90(),
	TMP_InputField_t1099764886::get_offset_of_m_ProcessingEvent_91(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1856 = { sizeof (ContentType_t1128941285)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1856[11] = 
{
	ContentType_t1128941285::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1857 = { sizeof (InputType_t2510134850)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1857[4] = 
{
	InputType_t2510134850::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1858 = { sizeof (CharacterValidation_t3801396403)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1858[10] = 
{
	CharacterValidation_t3801396403::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1859 = { sizeof (LineType_t2817627283)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1859[4] = 
{
	LineType_t2817627283::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1860 = { sizeof (OnValidateInput_t373909109), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1861 = { sizeof (SubmitEvent_t1343580625), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1862 = { sizeof (OnChangeEvent_t4257774360), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1863 = { sizeof (SelectionEvent_t4268942288), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1864 = { sizeof (TextSelectionEvent_t1023421581), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1865 = { sizeof (EditState_t2966235475)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1865[3] = 
{
	EditState_t2966235475::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1866 = { sizeof (U3CCaretBlinkU3Ed__238_t3270037332), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1866[5] = 
{
	U3CCaretBlinkU3Ed__238_t3270037332::get_offset_of_U3CU3E1__state_0(),
	U3CCaretBlinkU3Ed__238_t3270037332::get_offset_of_U3CU3E2__current_1(),
	U3CCaretBlinkU3Ed__238_t3270037332::get_offset_of_U3CU3E4__this_2(),
	U3CCaretBlinkU3Ed__238_t3270037332::get_offset_of_U3CblinkPeriodU3E5__1_3(),
	U3CCaretBlinkU3Ed__238_t3270037332::get_offset_of_U3CblinkStateU3E5__2_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1867 = { sizeof (U3CMouseDragOutsideRectU3Ed__255_t2073557366), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1867[7] = 
{
	U3CMouseDragOutsideRectU3Ed__255_t2073557366::get_offset_of_U3CU3E1__state_0(),
	U3CMouseDragOutsideRectU3Ed__255_t2073557366::get_offset_of_U3CU3E2__current_1(),
	U3CMouseDragOutsideRectU3Ed__255_t2073557366::get_offset_of_eventData_2(),
	U3CMouseDragOutsideRectU3Ed__255_t2073557366::get_offset_of_U3CU3E4__this_3(),
	U3CMouseDragOutsideRectU3Ed__255_t2073557366::get_offset_of_U3ClocalMousePosU3E5__1_4(),
	U3CMouseDragOutsideRectU3Ed__255_t2073557366::get_offset_of_U3CrectU3E5__2_5(),
	U3CMouseDragOutsideRectU3Ed__255_t2073557366::get_offset_of_U3CdelayU3E5__3_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1868 = { sizeof (SetPropertyUtility_t2931591262), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1869 = { sizeof (TMP_InputValidator_t1385053824), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1870 = { sizeof (TMP_LineInfo_t1079631636)+ sizeof (Il2CppObject), sizeof(TMP_LineInfo_t1079631636 ), 0, 0 };
extern const int32_t g_FieldOffsetTable1870[19] = 
{
	TMP_LineInfo_t1079631636::get_offset_of_characterCount_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TMP_LineInfo_t1079631636::get_offset_of_visibleCharacterCount_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TMP_LineInfo_t1079631636::get_offset_of_spaceCount_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TMP_LineInfo_t1079631636::get_offset_of_wordCount_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TMP_LineInfo_t1079631636::get_offset_of_firstCharacterIndex_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TMP_LineInfo_t1079631636::get_offset_of_firstVisibleCharacterIndex_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TMP_LineInfo_t1079631636::get_offset_of_lastCharacterIndex_6() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TMP_LineInfo_t1079631636::get_offset_of_lastVisibleCharacterIndex_7() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TMP_LineInfo_t1079631636::get_offset_of_length_8() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TMP_LineInfo_t1079631636::get_offset_of_lineHeight_9() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TMP_LineInfo_t1079631636::get_offset_of_ascender_10() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TMP_LineInfo_t1079631636::get_offset_of_baseline_11() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TMP_LineInfo_t1079631636::get_offset_of_descender_12() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TMP_LineInfo_t1079631636::get_offset_of_maxAdvance_13() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TMP_LineInfo_t1079631636::get_offset_of_width_14() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TMP_LineInfo_t1079631636::get_offset_of_marginLeft_15() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TMP_LineInfo_t1079631636::get_offset_of_marginRight_16() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TMP_LineInfo_t1079631636::get_offset_of_alignment_17() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TMP_LineInfo_t1079631636::get_offset_of_lineExtents_18() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1871 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1871[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1872 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1872[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1873 = { sizeof (TMP_MaterialManager_t2944071966), -1, sizeof(TMP_MaterialManager_t2944071966_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1873[5] = 
{
	TMP_MaterialManager_t2944071966_StaticFields::get_offset_of_m_materialList_0(),
	TMP_MaterialManager_t2944071966_StaticFields::get_offset_of_m_fallbackMaterials_1(),
	TMP_MaterialManager_t2944071966_StaticFields::get_offset_of_m_fallbackMaterialLookup_2(),
	TMP_MaterialManager_t2944071966_StaticFields::get_offset_of_m_fallbackCleanupList_3(),
	TMP_MaterialManager_t2944071966_StaticFields::get_offset_of_isFallbackListDirty_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1874 = { sizeof (FallbackMaterial_t4171378819), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1874[5] = 
{
	FallbackMaterial_t4171378819::get_offset_of_baseID_0(),
	FallbackMaterial_t4171378819::get_offset_of_baseMaterial_1(),
	FallbackMaterial_t4171378819::get_offset_of_fallbackID_2(),
	FallbackMaterial_t4171378819::get_offset_of_fallbackMaterial_3(),
	FallbackMaterial_t4171378819::get_offset_of_count_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1875 = { sizeof (MaskingMaterial_t16896275), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1875[4] = 
{
	MaskingMaterial_t16896275::get_offset_of_baseMaterial_0(),
	MaskingMaterial_t16896275::get_offset_of_stencilMaterial_1(),
	MaskingMaterial_t16896275::get_offset_of_count_2(),
	MaskingMaterial_t16896275::get_offset_of_stencilID_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1876 = { sizeof (U3CU3Ec__DisplayClass10_0_t173182959), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1876[1] = 
{
	U3CU3Ec__DisplayClass10_0_t173182959::get_offset_of_stencilMaterial_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1877 = { sizeof (U3CU3Ec__DisplayClass12_0_t173182957), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1877[1] = 
{
	U3CU3Ec__DisplayClass12_0_t173182957::get_offset_of_stencilMaterial_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1878 = { sizeof (U3CU3Ec__DisplayClass13_0_t173182956), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1878[1] = 
{
	U3CU3Ec__DisplayClass13_0_t173182956::get_offset_of_stencilMaterial_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1879 = { sizeof (U3CU3Ec__DisplayClass14_0_t173182963), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1879[1] = 
{
	U3CU3Ec__DisplayClass14_0_t173182963::get_offset_of_baseMaterial_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1880 = { sizeof (VertexSortingOrder_t2659893934)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1880[3] = 
{
	VertexSortingOrder_t2659893934::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1881 = { sizeof (TMP_MeshInfo_t2771747634)+ sizeof (Il2CppObject), -1, sizeof(TMP_MeshInfo_t2771747634_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1881[12] = 
{
	TMP_MeshInfo_t2771747634_StaticFields::get_offset_of_s_DefaultColor_0(),
	TMP_MeshInfo_t2771747634_StaticFields::get_offset_of_s_DefaultNormal_1(),
	TMP_MeshInfo_t2771747634_StaticFields::get_offset_of_s_DefaultTangent_2(),
	TMP_MeshInfo_t2771747634::get_offset_of_mesh_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TMP_MeshInfo_t2771747634::get_offset_of_vertexCount_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TMP_MeshInfo_t2771747634::get_offset_of_vertices_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TMP_MeshInfo_t2771747634::get_offset_of_normals_6() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TMP_MeshInfo_t2771747634::get_offset_of_tangents_7() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TMP_MeshInfo_t2771747634::get_offset_of_uvs0_8() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TMP_MeshInfo_t2771747634::get_offset_of_uvs2_9() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TMP_MeshInfo_t2771747634::get_offset_of_colors32_10() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TMP_MeshInfo_t2771747634::get_offset_of_triangles_11() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1882 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1882[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1883 = { sizeof (TMP_PhoneNumberValidator_t743649728), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1884 = { sizeof (TMP_ScrollbarEventHandler_t374199898), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1884[1] = 
{
	TMP_ScrollbarEventHandler_t374199898::get_offset_of_isSelected_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1885 = { sizeof (TMP_SelectionCaret_t407257285), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1886 = { sizeof (TMP_Settings_t2071156274), -1, sizeof(TMP_Settings_t2071156274_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1886[25] = 
{
	TMP_Settings_t2071156274_StaticFields::get_offset_of_s_Instance_2(),
	TMP_Settings_t2071156274::get_offset_of_m_enableWordWrapping_3(),
	TMP_Settings_t2071156274::get_offset_of_m_enableKerning_4(),
	TMP_Settings_t2071156274::get_offset_of_m_enableExtraPadding_5(),
	TMP_Settings_t2071156274::get_offset_of_m_enableTintAllSprites_6(),
	TMP_Settings_t2071156274::get_offset_of_m_enableParseEscapeCharacters_7(),
	TMP_Settings_t2071156274::get_offset_of_m_missingGlyphCharacter_8(),
	TMP_Settings_t2071156274::get_offset_of_m_warningsDisabled_9(),
	TMP_Settings_t2071156274::get_offset_of_m_defaultFontAsset_10(),
	TMP_Settings_t2071156274::get_offset_of_m_defaultFontAssetPath_11(),
	TMP_Settings_t2071156274::get_offset_of_m_defaultFontSize_12(),
	TMP_Settings_t2071156274::get_offset_of_m_defaultAutoSizeMinRatio_13(),
	TMP_Settings_t2071156274::get_offset_of_m_defaultAutoSizeMaxRatio_14(),
	TMP_Settings_t2071156274::get_offset_of_m_defaultTextMeshProTextContainerSize_15(),
	TMP_Settings_t2071156274::get_offset_of_m_defaultTextMeshProUITextContainerSize_16(),
	TMP_Settings_t2071156274::get_offset_of_m_autoSizeTextContainer_17(),
	TMP_Settings_t2071156274::get_offset_of_m_fallbackFontAssets_18(),
	TMP_Settings_t2071156274::get_offset_of_m_matchMaterialPreset_19(),
	TMP_Settings_t2071156274::get_offset_of_m_defaultSpriteAsset_20(),
	TMP_Settings_t2071156274::get_offset_of_m_defaultSpriteAssetPath_21(),
	TMP_Settings_t2071156274::get_offset_of_m_enableEmojiSupport_22(),
	TMP_Settings_t2071156274::get_offset_of_m_defaultStyleSheet_23(),
	TMP_Settings_t2071156274::get_offset_of_m_leadingCharacters_24(),
	TMP_Settings_t2071156274::get_offset_of_m_followingCharacters_25(),
	TMP_Settings_t2071156274::get_offset_of_m_linebreakingRules_26(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1887 = { sizeof (LineBreakingTable_t1457697482), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1887[2] = 
{
	LineBreakingTable_t1457697482::get_offset_of_leadingCharacters_0(),
	LineBreakingTable_t1457697482::get_offset_of_followingCharacters_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1888 = { sizeof (TMP_Sprite_t554067146), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1888[5] = 
{
	TMP_Sprite_t554067146::get_offset_of_name_9(),
	TMP_Sprite_t554067146::get_offset_of_hashCode_10(),
	TMP_Sprite_t554067146::get_offset_of_unicode_11(),
	TMP_Sprite_t554067146::get_offset_of_pivot_12(),
	TMP_Sprite_t554067146::get_offset_of_sprite_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1889 = { sizeof (TMP_SpriteAnimator_t2836635477), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1889[2] = 
{
	TMP_SpriteAnimator_t2836635477::get_offset_of_m_animations_2(),
	TMP_SpriteAnimator_t2836635477::get_offset_of_m_TextComponent_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1890 = { sizeof (U3CDoSpriteAnimationInternalU3Ed__7_t549195582), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1890[28] = 
{
	U3CDoSpriteAnimationInternalU3Ed__7_t549195582::get_offset_of_U3CU3E1__state_0(),
	U3CDoSpriteAnimationInternalU3Ed__7_t549195582::get_offset_of_U3CU3E2__current_1(),
	U3CDoSpriteAnimationInternalU3Ed__7_t549195582::get_offset_of_currentCharacter_2(),
	U3CDoSpriteAnimationInternalU3Ed__7_t549195582::get_offset_of_spriteAsset_3(),
	U3CDoSpriteAnimationInternalU3Ed__7_t549195582::get_offset_of_start_4(),
	U3CDoSpriteAnimationInternalU3Ed__7_t549195582::get_offset_of_end_5(),
	U3CDoSpriteAnimationInternalU3Ed__7_t549195582::get_offset_of_framerate_6(),
	U3CDoSpriteAnimationInternalU3Ed__7_t549195582::get_offset_of_U3CU3E4__this_7(),
	U3CDoSpriteAnimationInternalU3Ed__7_t549195582::get_offset_of_U3CcurrentFrameU3E5__1_8(),
	U3CDoSpriteAnimationInternalU3Ed__7_t549195582::get_offset_of_U3CcharInfoU3E5__2_9(),
	U3CDoSpriteAnimationInternalU3Ed__7_t549195582::get_offset_of_U3CmaterialIndexU3E5__3_10(),
	U3CDoSpriteAnimationInternalU3Ed__7_t549195582::get_offset_of_U3CvertexIndexU3E5__4_11(),
	U3CDoSpriteAnimationInternalU3Ed__7_t549195582::get_offset_of_U3CmeshInfoU3E5__5_12(),
	U3CDoSpriteAnimationInternalU3Ed__7_t549195582::get_offset_of_U3CelapsedTimeU3E5__6_13(),
	U3CDoSpriteAnimationInternalU3Ed__7_t549195582::get_offset_of_U3CtargetTimeU3E5__7_14(),
	U3CDoSpriteAnimationInternalU3Ed__7_t549195582::get_offset_of_U3CspriteU3E5__8_15(),
	U3CDoSpriteAnimationInternalU3Ed__7_t549195582::get_offset_of_U3CverticesU3E5__9_16(),
	U3CDoSpriteAnimationInternalU3Ed__7_t549195582::get_offset_of_U3CoriginU3E5__10_17(),
	U3CDoSpriteAnimationInternalU3Ed__7_t549195582::get_offset_of_U3CspriteScaleU3E5__11_18(),
	U3CDoSpriteAnimationInternalU3Ed__7_t549195582::get_offset_of_U3CblU3E5__12_19(),
	U3CDoSpriteAnimationInternalU3Ed__7_t549195582::get_offset_of_U3CtlU3E5__13_20(),
	U3CDoSpriteAnimationInternalU3Ed__7_t549195582::get_offset_of_U3CtrU3E5__14_21(),
	U3CDoSpriteAnimationInternalU3Ed__7_t549195582::get_offset_of_U3CbrU3E5__15_22(),
	U3CDoSpriteAnimationInternalU3Ed__7_t549195582::get_offset_of_U3Cuvs0U3E5__16_23(),
	U3CDoSpriteAnimationInternalU3Ed__7_t549195582::get_offset_of_U3Cuv0U3E5__17_24(),
	U3CDoSpriteAnimationInternalU3Ed__7_t549195582::get_offset_of_U3Cuv1U3E5__18_25(),
	U3CDoSpriteAnimationInternalU3Ed__7_t549195582::get_offset_of_U3Cuv2U3E5__19_26(),
	U3CDoSpriteAnimationInternalU3Ed__7_t549195582::get_offset_of_U3Cuv3U3E5__20_27(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1891 = { sizeof (TMP_SpriteAsset_t484820633), -1, sizeof(TMP_SpriteAsset_t484820633_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1891[7] = 
{
	TMP_SpriteAsset_t484820633::get_offset_of_m_UnicodeLookup_5(),
	TMP_SpriteAsset_t484820633::get_offset_of_m_NameLookup_6(),
	TMP_SpriteAsset_t484820633_StaticFields::get_offset_of_m_defaultSpriteAsset_7(),
	TMP_SpriteAsset_t484820633::get_offset_of_spriteSheet_8(),
	TMP_SpriteAsset_t484820633::get_offset_of_spriteInfoList_9(),
	TMP_SpriteAsset_t484820633::get_offset_of_m_SpriteUnicodeLookup_10(),
	TMP_SpriteAsset_t484820633::get_offset_of_fallbackSpriteAssets_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1892 = { sizeof (TMP_Style_t3479380764), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1892[6] = 
{
	TMP_Style_t3479380764::get_offset_of_m_Name_0(),
	TMP_Style_t3479380764::get_offset_of_m_HashCode_1(),
	TMP_Style_t3479380764::get_offset_of_m_OpeningDefinition_2(),
	TMP_Style_t3479380764::get_offset_of_m_ClosingDefinition_3(),
	TMP_Style_t3479380764::get_offset_of_m_OpeningTagArray_4(),
	TMP_Style_t3479380764::get_offset_of_m_ClosingTagArray_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1893 = { sizeof (TMP_StyleSheet_t917564226), -1, sizeof(TMP_StyleSheet_t917564226_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1893[3] = 
{
	TMP_StyleSheet_t917564226_StaticFields::get_offset_of_s_Instance_2(),
	TMP_StyleSheet_t917564226::get_offset_of_m_StyleList_3(),
	TMP_StyleSheet_t917564226::get_offset_of_m_StyleDictionary_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1894 = { sizeof (TMP_SubMesh_t2613037997), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1894[14] = 
{
	TMP_SubMesh_t2613037997::get_offset_of_m_fontAsset_2(),
	TMP_SubMesh_t2613037997::get_offset_of_m_spriteAsset_3(),
	TMP_SubMesh_t2613037997::get_offset_of_m_material_4(),
	TMP_SubMesh_t2613037997::get_offset_of_m_sharedMaterial_5(),
	TMP_SubMesh_t2613037997::get_offset_of_m_fallbackMaterial_6(),
	TMP_SubMesh_t2613037997::get_offset_of_m_fallbackSourceMaterial_7(),
	TMP_SubMesh_t2613037997::get_offset_of_m_isDefaultMaterial_8(),
	TMP_SubMesh_t2613037997::get_offset_of_m_padding_9(),
	TMP_SubMesh_t2613037997::get_offset_of_m_renderer_10(),
	TMP_SubMesh_t2613037997::get_offset_of_m_meshFilter_11(),
	TMP_SubMesh_t2613037997::get_offset_of_m_mesh_12(),
	TMP_SubMesh_t2613037997::get_offset_of_m_boxCollider_13(),
	TMP_SubMesh_t2613037997::get_offset_of_m_TextComponent_14(),
	TMP_SubMesh_t2613037997::get_offset_of_m_isRegisteredForEvents_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1895 = { sizeof (TMP_SubMeshUI_t1578871311), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1895[14] = 
{
	TMP_SubMeshUI_t1578871311::get_offset_of_m_fontAsset_28(),
	TMP_SubMeshUI_t1578871311::get_offset_of_m_spriteAsset_29(),
	TMP_SubMeshUI_t1578871311::get_offset_of_m_material_30(),
	TMP_SubMeshUI_t1578871311::get_offset_of_m_sharedMaterial_31(),
	TMP_SubMeshUI_t1578871311::get_offset_of_m_fallbackMaterial_32(),
	TMP_SubMeshUI_t1578871311::get_offset_of_m_fallbackSourceMaterial_33(),
	TMP_SubMeshUI_t1578871311::get_offset_of_m_isDefaultMaterial_34(),
	TMP_SubMeshUI_t1578871311::get_offset_of_m_padding_35(),
	TMP_SubMeshUI_t1578871311::get_offset_of_m_canvasRenderer_36(),
	TMP_SubMeshUI_t1578871311::get_offset_of_m_mesh_37(),
	TMP_SubMeshUI_t1578871311::get_offset_of_m_TextComponent_38(),
	TMP_SubMeshUI_t1578871311::get_offset_of_m_isRegisteredForEvents_39(),
	TMP_SubMeshUI_t1578871311::get_offset_of_m_materialDirty_40(),
	TMP_SubMeshUI_t1578871311::get_offset_of_m_materialReferenceIndex_41(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1896 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1897 = { sizeof (TextAlignmentOptions_t4036791236)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1897[37] = 
{
	TextAlignmentOptions_t4036791236::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1898 = { sizeof (_HorizontalAlignmentOptions_t2379014378)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1898[7] = 
{
	_HorizontalAlignmentOptions_t2379014378::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1899 = { sizeof (_VerticalAlignmentOptions_t2825528260)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1899[7] = 
{
	_VerticalAlignmentOptions_t2825528260::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
