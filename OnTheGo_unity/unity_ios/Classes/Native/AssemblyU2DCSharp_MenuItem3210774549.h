﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.String
struct String_t;
// MenuItem
struct MenuItem_t3210774549;
// System.Collections.Generic.List`1<MenuItem>
struct List_1_t387881995;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MenuItem
struct  MenuItem_t3210774549  : public Il2CppObject
{
public:
	// System.String MenuItem::Title
	String_t* ___Title_0;
	// System.String MenuItem::Description
	String_t* ___Description_1;
	// MenuItem MenuItem::Parent
	MenuItem_t3210774549 * ___Parent_2;
	// System.Collections.Generic.List`1<MenuItem> MenuItem::Children
	List_1_t387881995 * ___Children_3;

public:
	inline static int32_t get_offset_of_Title_0() { return static_cast<int32_t>(offsetof(MenuItem_t3210774549, ___Title_0)); }
	inline String_t* get_Title_0() const { return ___Title_0; }
	inline String_t** get_address_of_Title_0() { return &___Title_0; }
	inline void set_Title_0(String_t* value)
	{
		___Title_0 = value;
		Il2CppCodeGenWriteBarrier(&___Title_0, value);
	}

	inline static int32_t get_offset_of_Description_1() { return static_cast<int32_t>(offsetof(MenuItem_t3210774549, ___Description_1)); }
	inline String_t* get_Description_1() const { return ___Description_1; }
	inline String_t** get_address_of_Description_1() { return &___Description_1; }
	inline void set_Description_1(String_t* value)
	{
		___Description_1 = value;
		Il2CppCodeGenWriteBarrier(&___Description_1, value);
	}

	inline static int32_t get_offset_of_Parent_2() { return static_cast<int32_t>(offsetof(MenuItem_t3210774549, ___Parent_2)); }
	inline MenuItem_t3210774549 * get_Parent_2() const { return ___Parent_2; }
	inline MenuItem_t3210774549 ** get_address_of_Parent_2() { return &___Parent_2; }
	inline void set_Parent_2(MenuItem_t3210774549 * value)
	{
		___Parent_2 = value;
		Il2CppCodeGenWriteBarrier(&___Parent_2, value);
	}

	inline static int32_t get_offset_of_Children_3() { return static_cast<int32_t>(offsetof(MenuItem_t3210774549, ___Children_3)); }
	inline List_1_t387881995 * get_Children_3() const { return ___Children_3; }
	inline List_1_t387881995 ** get_address_of_Children_3() { return &___Children_3; }
	inline void set_Children_3(List_1_t387881995 * value)
	{
		___Children_3 = value;
		Il2CppCodeGenWriteBarrier(&___Children_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
