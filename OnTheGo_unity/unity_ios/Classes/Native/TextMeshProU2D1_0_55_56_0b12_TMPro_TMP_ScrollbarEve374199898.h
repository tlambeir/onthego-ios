﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour3962482529.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_ScrollbarEventHandler
struct  TMP_ScrollbarEventHandler_t374199898  : public MonoBehaviour_t3962482529
{
public:
	// System.Boolean TMPro.TMP_ScrollbarEventHandler::isSelected
	bool ___isSelected_2;

public:
	inline static int32_t get_offset_of_isSelected_2() { return static_cast<int32_t>(offsetof(TMP_ScrollbarEventHandler_t374199898, ___isSelected_2)); }
	inline bool get_isSelected_2() const { return ___isSelected_2; }
	inline bool* get_address_of_isSelected_2() { return &___isSelected_2; }
	inline void set_isSelected_2(bool value)
	{
		___isSelected_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
