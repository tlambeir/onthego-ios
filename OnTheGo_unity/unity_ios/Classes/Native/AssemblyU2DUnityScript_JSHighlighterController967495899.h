﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour3962482529.h"

// HighlightingSystem.Highlighter
struct Highlighter_t672210414;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// JSHighlighterController
struct  JSHighlighterController_t967495899  : public MonoBehaviour_t3962482529
{
public:
	// HighlightingSystem.Highlighter JSHighlighterController::h
	Highlighter_t672210414 * ___h_2;

public:
	inline static int32_t get_offset_of_h_2() { return static_cast<int32_t>(offsetof(JSHighlighterController_t967495899, ___h_2)); }
	inline Highlighter_t672210414 * get_h_2() const { return ___h_2; }
	inline Highlighter_t672210414 ** get_address_of_h_2() { return &___h_2; }
	inline void set_h_2(Highlighter_t672210414 * value)
	{
		___h_2 = value;
		Il2CppCodeGenWriteBarrier(&___h_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
