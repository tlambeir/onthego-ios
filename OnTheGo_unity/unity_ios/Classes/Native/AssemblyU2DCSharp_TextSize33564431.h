﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.Collections.Hashtable
struct Hashtable_t1853889766;
// UnityEngine.TextMesh
struct TextMesh_t1536577757;
// UnityEngine.Renderer
struct Renderer_t2627027031;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TextSize
struct  TextSize_t33564431  : public Il2CppObject
{
public:
	// System.Collections.Hashtable TextSize::dict
	Hashtable_t1853889766 * ___dict_0;
	// UnityEngine.TextMesh TextSize::textMesh
	TextMesh_t1536577757 * ___textMesh_1;
	// UnityEngine.Renderer TextSize::renderer
	Renderer_t2627027031 * ___renderer_2;

public:
	inline static int32_t get_offset_of_dict_0() { return static_cast<int32_t>(offsetof(TextSize_t33564431, ___dict_0)); }
	inline Hashtable_t1853889766 * get_dict_0() const { return ___dict_0; }
	inline Hashtable_t1853889766 ** get_address_of_dict_0() { return &___dict_0; }
	inline void set_dict_0(Hashtable_t1853889766 * value)
	{
		___dict_0 = value;
		Il2CppCodeGenWriteBarrier(&___dict_0, value);
	}

	inline static int32_t get_offset_of_textMesh_1() { return static_cast<int32_t>(offsetof(TextSize_t33564431, ___textMesh_1)); }
	inline TextMesh_t1536577757 * get_textMesh_1() const { return ___textMesh_1; }
	inline TextMesh_t1536577757 ** get_address_of_textMesh_1() { return &___textMesh_1; }
	inline void set_textMesh_1(TextMesh_t1536577757 * value)
	{
		___textMesh_1 = value;
		Il2CppCodeGenWriteBarrier(&___textMesh_1, value);
	}

	inline static int32_t get_offset_of_renderer_2() { return static_cast<int32_t>(offsetof(TextSize_t33564431, ___renderer_2)); }
	inline Renderer_t2627027031 * get_renderer_2() const { return ___renderer_2; }
	inline Renderer_t2627027031 ** get_address_of_renderer_2() { return &___renderer_2; }
	inline void set_renderer_2(Renderer_t2627027031 * value)
	{
		___renderer_2 = value;
		Il2CppCodeGenWriteBarrier(&___renderer_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
