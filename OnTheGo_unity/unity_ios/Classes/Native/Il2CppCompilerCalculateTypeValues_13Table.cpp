﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Experimental_Director_Play3531467704.h"
#include "UnityEngine_UnityEngine_Experimental_Director_Playa882318307.h"
#include "UnityEngine_UnityEngine_Experimental_Director_Play3858037524.h"
#include "UnityEngine_UnityEngine_Experimental_Director_Scri1800649443.h"
#include "UnityEngine_UnityEngine_iOS_CalendarIdentifier3366210299.h"
#include "UnityEngine_UnityEngine_iOS_CalendarUnit1546283851.h"
#include "UnityEngine_UnityEngine_iOS_LocalNotification1697500732.h"
#include "UnityEngine_UnityEngine_iOS_RemoteNotification818046696.h"
#include "UnityEngine_UnityEngine_SceneManagement_Scene2348375561.h"
#include "UnityEngine_UnityEngine_SceneManagement_LoadSceneM3251202195.h"
#include "UnityEngine_UnityEngine_SceneManagement_SceneManag2787271929.h"
#include "UnityEngine_UnityEngine_Experimental_Rendering_Scri274343796.h"
#include "UnityEngine_UnityEngine_Transform3600365921.h"
#include "UnityEngine_UnityEngine_Transform_Enumerator3442430665.h"
#include "UnityEngine_UnityEngine_DrivenTransformProperties3813433528.h"
#include "UnityEngine_UnityEngine_DrivenRectTransformTracker2562230146.h"
#include "UnityEngine_UnityEngine_RectTransform3704657025.h"
#include "UnityEngine_UnityEngine_RectTransform_ReapplyDrive1258266594.h"
#include "UnityEngine_UnityEngine_RectTransform_Edge1530570602.h"
#include "UnityEngine_UnityEngine_RectTransform_Axis1856666072.h"
#include "UnityEngine_UnityEngine_ParticleSystemCurveMode3859704052.h"
#include "UnityEngine_UnityEngine_ParticleSystemSimulationSp2969500608.h"
#include "UnityEngine_UnityEngine_ParticleSystemStopBehavior2808326180.h"
#include "UnityEngine_UnityEngine_ParticleSystemScalingMode2278533876.h"
#include "UnityEngine_UnityEngine_ParticleSystemCustomData1949455375.h"
#include "UnityEngine_UnityEngine_ParticleSystem1800779281.h"
#include "UnityEngine_UnityEngine_ParticleSystem_MinMaxCurve1067599125.h"
#include "UnityEngine_UnityEngine_ParticleSystem_MainModule2320046318.h"
#include "UnityEngine_UnityEngine_ParticleSystem_EmissionModu311448003.h"
#include "UnityEngine_UnityEngine_ParticleSystem_ShapeModule3608330829.h"
#include "UnityEngine_UnityEngine_ParticleSystem_VelocityOve1982232382.h"
#include "UnityEngine_UnityEngine_ParticleSystem_LimitVelocit686589569.h"
#include "UnityEngine_UnityEngine_ParticleSystem_InheritVelo3940044026.h"
#include "UnityEngine_UnityEngine_ParticleSystem_ForceOverLi4029962193.h"
#include "UnityEngine_UnityEngine_ParticleSystem_ColorOverLi3039228654.h"
#include "UnityEngine_UnityEngine_ParticleSystem_ColorBySpee3740209408.h"
#include "UnityEngine_UnityEngine_ParticleSystem_SizeOverLif1101123803.h"
#include "UnityEngine_UnityEngine_ParticleSystem_SizeBySpeed1515126846.h"
#include "UnityEngine_UnityEngine_ParticleSystem_RotationOve1164372224.h"
#include "UnityEngine_UnityEngine_ParticleSystem_RotationByS3497409583.h"
#include "UnityEngine_UnityEngine_ParticleSystem_ExternalFor1424795933.h"
#include "UnityEngine_UnityEngine_ParticleSystem_NoiseModule962525627.h"
#include "UnityEngine_UnityEngine_ParticleSystem_CollisionMo1950979710.h"
#include "UnityEngine_UnityEngine_ParticleSystem_TriggerModu1157986180.h"
#include "UnityEngine_UnityEngine_ParticleSystem_SubEmittersM903775760.h"
#include "UnityEngine_UnityEngine_ParticleSystem_TextureSheet738696839.h"
#include "UnityEngine_UnityEngine_ParticleSystem_LightsModul3616883284.h"
#include "UnityEngine_UnityEngine_ParticleSystem_TrailModule2282589118.h"
#include "UnityEngine_UnityEngine_ParticleSystem_CustomDataM2135829708.h"
#include "UnityEngine_UnityEngine_ParticleSystem_Particle1882894987.h"
#include "UnityEngine_UnityEngine_ParticleSystem_EmitParams2216423628.h"
#include "UnityEngine_UnityEngine_ParticleSystem_IteratorDel2387635027.h"
#include "UnityEngine_UnityEngine_ParticleSystem_U3CSimulateU651750728.h"
#include "UnityEngine_UnityEngine_ParticleSystem_U3CStopU3Ec_849324769.h"
#include "UnityEngine_UnityEngine_ParticleSystemRenderer2065813411.h"
#include "UnityEngine_UnityEngine_ForceMode3656391766.h"
#include "UnityEngine_UnityEngine_ControllerColliderHit240592346.h"
#include "UnityEngine_UnityEngine_Collision4262080450.h"
#include "UnityEngine_UnityEngine_QueryTriggerInteraction962663221.h"
#include "UnityEngine_UnityEngine_Physics2310948930.h"
#include "UnityEngine_UnityEngine_ContactPoint3758755253.h"
#include "UnityEngine_UnityEngine_Rigidbody3916780224.h"
#include "UnityEngine_UnityEngine_Collider1773347010.h"
#include "UnityEngine_UnityEngine_BoxCollider1640800422.h"
#include "UnityEngine_UnityEngine_MeshCollider903564387.h"
#include "UnityEngine_UnityEngine_RaycastHit1056001966.h"
#include "UnityEngine_UnityEngine_CharacterController1138636865.h"
#include "UnityEngine_UnityEngine_RaycastHit2D2279581989.h"
#include "UnityEngine_UnityEngine_Physics2D1528932956.h"
#include "UnityEngine_UnityEngine_ForceMode2D255358695.h"
#include "UnityEngine_UnityEngine_ContactFilter2D3805203441.h"
#include "UnityEngine_UnityEngine_Rigidbody2D939494601.h"
#include "UnityEngine_UnityEngine_Collider2D2806799626.h"
#include "UnityEngine_UnityEngine_ContactPoint2D3390240644.h"
#include "UnityEngine_UnityEngine_Collision2D2842956331.h"
#include "UnityEngine_UnityEngine_AI_NavMesh1865600375.h"
#include "UnityEngine_UnityEngine_AI_NavMesh_OnNavMeshPreUpd1580782682.h"
#include "UnityEngine_UnityEngine_AudioSettings3587374600.h"
#include "UnityEngine_UnityEngine_AudioSettings_AudioConfigu2089929874.h"
#include "UnityEngine_UnityEngine_AudioClip3680889665.h"
#include "UnityEngine_UnityEngine_AudioClip_PCMReaderCallbac1677636661.h"
#include "UnityEngine_UnityEngine_AudioClip_PCMSetPositionCa1059417452.h"
#include "UnityEngine_UnityEngine_AudioSource3935305588.h"
#include "UnityEngine_UnityEngine_WebCamDevice1322781432.h"
#include "UnityEngine_UnityEngine_WebCamTexture1514609158.h"
#include "UnityEngine_UnityEngine_SharedBetweenAnimatorsAttr2857104338.h"
#include "UnityEngine_UnityEngine_StateMachineBehaviour957311111.h"
#include "UnityEngine_UnityEngine_AnimationEventSource3045280095.h"
#include "UnityEngine_UnityEngine_AnimationEvent1536042487.h"
#include "UnityEngine_UnityEngine_AnimationClip2318505987.h"
#include "UnityEngine_UnityEngine_AnimationState1108360407.h"
#include "UnityEngine_UnityEngine_AnimatorControllerParamete3317225440.h"
#include "UnityEngine_UnityEngine_AnimatorClipInfo3156717155.h"
#include "UnityEngine_UnityEngine_AnimatorStateInfo509032636.h"
#include "UnityEngine_UnityEngine_AnimatorTransitionInfo2534804151.h"
#include "UnityEngine_UnityEngine_Animator434523843.h"
#include "UnityEngine_UnityEngine_AnimatorControllerParamete1758260042.h"
#include "UnityEngine_UnityEngine_SkeletonBone4134054672.h"
#include "UnityEngine_UnityEngine_HumanLimit2901552972.h"
#include "UnityEngine_UnityEngine_HumanBone2465339518.h"







#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1300 = { sizeof (PlayState_t3531467704)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1300[3] = 
{
	PlayState_t3531467704::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1301 = { sizeof (PlayableHandle_t882318307)+ sizeof (Il2CppObject), sizeof(PlayableHandle_t882318307 ), 0, 0 };
extern const int32_t g_FieldOffsetTable1301[2] = 
{
	PlayableHandle_t882318307::get_offset_of_m_Handle_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	PlayableHandle_t882318307::get_offset_of_m_Version_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1302 = { sizeof (Playable_t3858037524), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1302[1] = 
{
	Playable_t3858037524::get_offset_of_handle_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1303 = { sizeof (ScriptPlayable_t1800649443), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1304 = { sizeof (CalendarIdentifier_t3366210299)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1304[12] = 
{
	CalendarIdentifier_t3366210299::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1305 = { sizeof (CalendarUnit_t1546283851)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1305[12] = 
{
	CalendarUnit_t1546283851::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1306 = { sizeof (LocalNotification_t1697500732), -1, sizeof(LocalNotification_t1697500732_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1306[2] = 
{
	LocalNotification_t1697500732::get_offset_of_notificationWrapper_0(),
	LocalNotification_t1697500732_StaticFields::get_offset_of_m_NSReferenceDateTicks_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1307 = { sizeof (RemoteNotification_t818046696), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1307[1] = 
{
	RemoteNotification_t818046696::get_offset_of_notificationWrapper_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1308 = { sizeof (Scene_t2348375561)+ sizeof (Il2CppObject), sizeof(Scene_t2348375561 ), 0, 0 };
extern const int32_t g_FieldOffsetTable1308[1] = 
{
	Scene_t2348375561::get_offset_of_m_Handle_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1309 = { sizeof (LoadSceneMode_t3251202195)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1309[3] = 
{
	LoadSceneMode_t3251202195::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1310 = { sizeof (SceneManager_t2787271929), -1, sizeof(SceneManager_t2787271929_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1310[3] = 
{
	SceneManager_t2787271929_StaticFields::get_offset_of_sceneLoaded_0(),
	SceneManager_t2787271929_StaticFields::get_offset_of_sceneUnloaded_1(),
	SceneManager_t2787271929_StaticFields::get_offset_of_activeSceneChanged_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1311 = { sizeof (ScriptableRenderContext_t274343796)+ sizeof (Il2CppObject), sizeof(ScriptableRenderContext_t274343796 ), 0, 0 };
extern const int32_t g_FieldOffsetTable1311[1] = 
{
	ScriptableRenderContext_t274343796::get_offset_of_m_Ptr_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1312 = { sizeof (Transform_t3600365921), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1313 = { sizeof (Enumerator_t3442430665), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1313[2] = 
{
	Enumerator_t3442430665::get_offset_of_outer_0(),
	Enumerator_t3442430665::get_offset_of_currentIndex_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1314 = { sizeof (DrivenTransformProperties_t3813433528)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1314[26] = 
{
	DrivenTransformProperties_t3813433528::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1315 = { sizeof (DrivenRectTransformTracker_t2562230146)+ sizeof (Il2CppObject), sizeof(DrivenRectTransformTracker_t2562230146 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1316 = { sizeof (RectTransform_t3704657025), -1, sizeof(RectTransform_t3704657025_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1316[1] = 
{
	RectTransform_t3704657025_StaticFields::get_offset_of_reapplyDrivenProperties_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1317 = { sizeof (ReapplyDrivenProperties_t1258266594), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1318 = { sizeof (Edge_t1530570602)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1318[5] = 
{
	Edge_t1530570602::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1319 = { sizeof (Axis_t1856666072)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1319[3] = 
{
	Axis_t1856666072::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1320 = { sizeof (ParticleSystemCurveMode_t3859704052)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1320[5] = 
{
	ParticleSystemCurveMode_t3859704052::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1321 = { sizeof (ParticleSystemSimulationSpace_t2969500608)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1321[4] = 
{
	ParticleSystemSimulationSpace_t2969500608::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1322 = { sizeof (ParticleSystemStopBehavior_t2808326180)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1322[3] = 
{
	ParticleSystemStopBehavior_t2808326180::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1323 = { sizeof (ParticleSystemScalingMode_t2278533876)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1323[4] = 
{
	ParticleSystemScalingMode_t2278533876::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1324 = { sizeof (ParticleSystemCustomData_t1949455375)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1324[3] = 
{
	ParticleSystemCustomData_t1949455375::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1325 = { sizeof (ParticleSystem_t1800779281), -1, sizeof(ParticleSystem_t1800779281_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1325[4] = 
{
	ParticleSystem_t1800779281_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_2(),
	ParticleSystem_t1800779281_StaticFields::get_offset_of_U3CU3Ef__amU24cache1_3(),
	ParticleSystem_t1800779281_StaticFields::get_offset_of_U3CU3Ef__amU24cache2_4(),
	ParticleSystem_t1800779281_StaticFields::get_offset_of_U3CU3Ef__amU24cache3_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1326 = { sizeof (MinMaxCurve_t1067599125)+ sizeof (Il2CppObject), sizeof(MinMaxCurve_t1067599125_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1326[6] = 
{
	MinMaxCurve_t1067599125::get_offset_of_m_Mode_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	MinMaxCurve_t1067599125::get_offset_of_m_CurveMultiplier_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	MinMaxCurve_t1067599125::get_offset_of_m_CurveMin_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	MinMaxCurve_t1067599125::get_offset_of_m_CurveMax_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	MinMaxCurve_t1067599125::get_offset_of_m_ConstantMin_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	MinMaxCurve_t1067599125::get_offset_of_m_ConstantMax_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1327 = { sizeof (MainModule_t2320046318)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1327[1] = 
{
	MainModule_t2320046318::get_offset_of_m_ParticleSystem_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1328 = { sizeof (EmissionModule_t311448003)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1328[1] = 
{
	EmissionModule_t311448003::get_offset_of_m_ParticleSystem_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1329 = { sizeof (ShapeModule_t3608330829)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1329[1] = 
{
	ShapeModule_t3608330829::get_offset_of_m_ParticleSystem_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1330 = { sizeof (VelocityOverLifetimeModule_t1982232382)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1330[1] = 
{
	VelocityOverLifetimeModule_t1982232382::get_offset_of_m_ParticleSystem_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1331 = { sizeof (LimitVelocityOverLifetimeModule_t686589569)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1331[1] = 
{
	LimitVelocityOverLifetimeModule_t686589569::get_offset_of_m_ParticleSystem_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1332 = { sizeof (InheritVelocityModule_t3940044026)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1332[1] = 
{
	InheritVelocityModule_t3940044026::get_offset_of_m_ParticleSystem_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1333 = { sizeof (ForceOverLifetimeModule_t4029962193)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1333[1] = 
{
	ForceOverLifetimeModule_t4029962193::get_offset_of_m_ParticleSystem_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1334 = { sizeof (ColorOverLifetimeModule_t3039228654)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1334[1] = 
{
	ColorOverLifetimeModule_t3039228654::get_offset_of_m_ParticleSystem_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1335 = { sizeof (ColorBySpeedModule_t3740209408)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1335[1] = 
{
	ColorBySpeedModule_t3740209408::get_offset_of_m_ParticleSystem_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1336 = { sizeof (SizeOverLifetimeModule_t1101123803)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1336[1] = 
{
	SizeOverLifetimeModule_t1101123803::get_offset_of_m_ParticleSystem_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1337 = { sizeof (SizeBySpeedModule_t1515126846)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1337[1] = 
{
	SizeBySpeedModule_t1515126846::get_offset_of_m_ParticleSystem_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1338 = { sizeof (RotationOverLifetimeModule_t1164372224)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1338[1] = 
{
	RotationOverLifetimeModule_t1164372224::get_offset_of_m_ParticleSystem_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1339 = { sizeof (RotationBySpeedModule_t3497409583)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1339[1] = 
{
	RotationBySpeedModule_t3497409583::get_offset_of_m_ParticleSystem_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1340 = { sizeof (ExternalForcesModule_t1424795933)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1340[1] = 
{
	ExternalForcesModule_t1424795933::get_offset_of_m_ParticleSystem_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1341 = { sizeof (NoiseModule_t962525627)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1341[1] = 
{
	NoiseModule_t962525627::get_offset_of_m_ParticleSystem_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1342 = { sizeof (CollisionModule_t1950979710)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1342[1] = 
{
	CollisionModule_t1950979710::get_offset_of_m_ParticleSystem_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1343 = { sizeof (TriggerModule_t1157986180)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1343[1] = 
{
	TriggerModule_t1157986180::get_offset_of_m_ParticleSystem_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1344 = { sizeof (SubEmittersModule_t903775760)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1344[1] = 
{
	SubEmittersModule_t903775760::get_offset_of_m_ParticleSystem_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1345 = { sizeof (TextureSheetAnimationModule_t738696839)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1345[1] = 
{
	TextureSheetAnimationModule_t738696839::get_offset_of_m_ParticleSystem_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1346 = { sizeof (LightsModule_t3616883284)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1346[1] = 
{
	LightsModule_t3616883284::get_offset_of_m_ParticleSystem_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1347 = { sizeof (TrailModule_t2282589118)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1347[1] = 
{
	TrailModule_t2282589118::get_offset_of_m_ParticleSystem_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1348 = { sizeof (CustomDataModule_t2135829708)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1348[1] = 
{
	CustomDataModule_t2135829708::get_offset_of_m_ParticleSystem_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1349 = { sizeof (Particle_t1882894987)+ sizeof (Il2CppObject), sizeof(Particle_t1882894987 ), 0, 0 };
extern const int32_t g_FieldOffsetTable1349[14] = 
{
	Particle_t1882894987::get_offset_of_m_Position_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Particle_t1882894987::get_offset_of_m_Velocity_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Particle_t1882894987::get_offset_of_m_AnimatedVelocity_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Particle_t1882894987::get_offset_of_m_InitialVelocity_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Particle_t1882894987::get_offset_of_m_AxisOfRotation_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Particle_t1882894987::get_offset_of_m_Rotation_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Particle_t1882894987::get_offset_of_m_AngularVelocity_6() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Particle_t1882894987::get_offset_of_m_StartSize_7() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Particle_t1882894987::get_offset_of_m_StartColor_8() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Particle_t1882894987::get_offset_of_m_RandomSeed_9() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Particle_t1882894987::get_offset_of_m_Lifetime_10() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Particle_t1882894987::get_offset_of_m_StartLifetime_11() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Particle_t1882894987::get_offset_of_m_EmitAccumulator0_12() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Particle_t1882894987::get_offset_of_m_EmitAccumulator1_13() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1350 = { sizeof (EmitParams_t2216423628)+ sizeof (Il2CppObject), sizeof(EmitParams_t2216423628_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1350[11] = 
{
	EmitParams_t2216423628::get_offset_of_m_Particle_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	EmitParams_t2216423628::get_offset_of_m_PositionSet_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	EmitParams_t2216423628::get_offset_of_m_VelocitySet_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	EmitParams_t2216423628::get_offset_of_m_AxisOfRotationSet_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	EmitParams_t2216423628::get_offset_of_m_RotationSet_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	EmitParams_t2216423628::get_offset_of_m_AngularVelocitySet_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
	EmitParams_t2216423628::get_offset_of_m_StartSizeSet_6() + static_cast<int32_t>(sizeof(Il2CppObject)),
	EmitParams_t2216423628::get_offset_of_m_StartColorSet_7() + static_cast<int32_t>(sizeof(Il2CppObject)),
	EmitParams_t2216423628::get_offset_of_m_RandomSeedSet_8() + static_cast<int32_t>(sizeof(Il2CppObject)),
	EmitParams_t2216423628::get_offset_of_m_StartLifetimeSet_9() + static_cast<int32_t>(sizeof(Il2CppObject)),
	EmitParams_t2216423628::get_offset_of_m_ApplyShapeToPosition_10() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1351 = { sizeof (IteratorDelegate_t2387635027), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1352 = { sizeof (U3CSimulateU3Ec__AnonStorey0_t651750728), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1352[3] = 
{
	U3CSimulateU3Ec__AnonStorey0_t651750728::get_offset_of_t_0(),
	U3CSimulateU3Ec__AnonStorey0_t651750728::get_offset_of_restart_1(),
	U3CSimulateU3Ec__AnonStorey0_t651750728::get_offset_of_fixedTimeStep_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1353 = { sizeof (U3CStopU3Ec__AnonStorey1_t849324769), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1353[1] = 
{
	U3CStopU3Ec__AnonStorey1_t849324769::get_offset_of_stopBehavior_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1354 = { sizeof (ParticleSystemRenderer_t2065813411), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1355 = { sizeof (ForceMode_t3656391766)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1355[5] = 
{
	ForceMode_t3656391766::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1356 = { sizeof (ControllerColliderHit_t240592346), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1356[7] = 
{
	ControllerColliderHit_t240592346::get_offset_of_m_Controller_0(),
	ControllerColliderHit_t240592346::get_offset_of_m_Collider_1(),
	ControllerColliderHit_t240592346::get_offset_of_m_Point_2(),
	ControllerColliderHit_t240592346::get_offset_of_m_Normal_3(),
	ControllerColliderHit_t240592346::get_offset_of_m_MoveDirection_4(),
	ControllerColliderHit_t240592346::get_offset_of_m_MoveLength_5(),
	ControllerColliderHit_t240592346::get_offset_of_m_Push_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1357 = { sizeof (Collision_t4262080450), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1357[5] = 
{
	Collision_t4262080450::get_offset_of_m_Impulse_0(),
	Collision_t4262080450::get_offset_of_m_RelativeVelocity_1(),
	Collision_t4262080450::get_offset_of_m_Rigidbody_2(),
	Collision_t4262080450::get_offset_of_m_Collider_3(),
	Collision_t4262080450::get_offset_of_m_Contacts_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1358 = { sizeof (QueryTriggerInteraction_t962663221)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1358[4] = 
{
	QueryTriggerInteraction_t962663221::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1359 = { sizeof (Physics_t2310948930), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1360 = { sizeof (ContactPoint_t3758755253)+ sizeof (Il2CppObject), sizeof(ContactPoint_t3758755253 ), 0, 0 };
extern const int32_t g_FieldOffsetTable1360[5] = 
{
	ContactPoint_t3758755253::get_offset_of_m_Point_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ContactPoint_t3758755253::get_offset_of_m_Normal_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ContactPoint_t3758755253::get_offset_of_m_ThisColliderInstanceID_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ContactPoint_t3758755253::get_offset_of_m_OtherColliderInstanceID_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ContactPoint_t3758755253::get_offset_of_m_Separation_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1361 = { sizeof (Rigidbody_t3916780224), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1362 = { sizeof (Collider_t1773347010), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1363 = { sizeof (BoxCollider_t1640800422), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1364 = { sizeof (MeshCollider_t903564387), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1365 = { sizeof (RaycastHit_t1056001966)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1365[6] = 
{
	RaycastHit_t1056001966::get_offset_of_m_Point_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RaycastHit_t1056001966::get_offset_of_m_Normal_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RaycastHit_t1056001966::get_offset_of_m_FaceID_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RaycastHit_t1056001966::get_offset_of_m_Distance_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RaycastHit_t1056001966::get_offset_of_m_UV_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RaycastHit_t1056001966::get_offset_of_m_Collider_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1366 = { sizeof (CharacterController_t1138636865), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1367 = { sizeof (RaycastHit2D_t2279581989)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1367[6] = 
{
	RaycastHit2D_t2279581989::get_offset_of_m_Centroid_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RaycastHit2D_t2279581989::get_offset_of_m_Point_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RaycastHit2D_t2279581989::get_offset_of_m_Normal_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RaycastHit2D_t2279581989::get_offset_of_m_Distance_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RaycastHit2D_t2279581989::get_offset_of_m_Fraction_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RaycastHit2D_t2279581989::get_offset_of_m_Collider_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1368 = { sizeof (Physics2D_t1528932956), -1, sizeof(Physics2D_t1528932956_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1368[1] = 
{
	Physics2D_t1528932956_StaticFields::get_offset_of_m_LastDisabledRigidbody2D_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1369 = { sizeof (ForceMode2D_t255358695)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1369[3] = 
{
	ForceMode2D_t255358695::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1370 = { sizeof (ContactFilter2D_t3805203441)+ sizeof (Il2CppObject), sizeof(ContactFilter2D_t3805203441_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1370[11] = 
{
	ContactFilter2D_t3805203441::get_offset_of_useTriggers_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ContactFilter2D_t3805203441::get_offset_of_useLayerMask_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ContactFilter2D_t3805203441::get_offset_of_useDepth_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ContactFilter2D_t3805203441::get_offset_of_useOutsideDepth_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ContactFilter2D_t3805203441::get_offset_of_useNormalAngle_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ContactFilter2D_t3805203441::get_offset_of_useOutsideNormalAngle_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ContactFilter2D_t3805203441::get_offset_of_layerMask_6() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ContactFilter2D_t3805203441::get_offset_of_minDepth_7() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ContactFilter2D_t3805203441::get_offset_of_maxDepth_8() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ContactFilter2D_t3805203441::get_offset_of_minNormalAngle_9() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ContactFilter2D_t3805203441::get_offset_of_maxNormalAngle_10() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1371 = { sizeof (Rigidbody2D_t939494601), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1372 = { sizeof (Collider2D_t2806799626), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1373 = { sizeof (ContactPoint2D_t3390240644)+ sizeof (Il2CppObject), sizeof(ContactPoint2D_t3390240644 ), 0, 0 };
extern const int32_t g_FieldOffsetTable1373[11] = 
{
	ContactPoint2D_t3390240644::get_offset_of_m_Point_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ContactPoint2D_t3390240644::get_offset_of_m_Normal_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ContactPoint2D_t3390240644::get_offset_of_m_RelativeVelocity_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ContactPoint2D_t3390240644::get_offset_of_m_Separation_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ContactPoint2D_t3390240644::get_offset_of_m_NormalImpulse_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ContactPoint2D_t3390240644::get_offset_of_m_TangentImpulse_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ContactPoint2D_t3390240644::get_offset_of_m_Collider_6() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ContactPoint2D_t3390240644::get_offset_of_m_OtherCollider_7() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ContactPoint2D_t3390240644::get_offset_of_m_Rigidbody_8() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ContactPoint2D_t3390240644::get_offset_of_m_OtherRigidbody_9() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ContactPoint2D_t3390240644::get_offset_of_m_Enabled_10() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1374 = { sizeof (Collision2D_t2842956331), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1374[7] = 
{
	Collision2D_t2842956331::get_offset_of_m_Collider_0(),
	Collision2D_t2842956331::get_offset_of_m_OtherCollider_1(),
	Collision2D_t2842956331::get_offset_of_m_Rigidbody_2(),
	Collision2D_t2842956331::get_offset_of_m_OtherRigidbody_3(),
	Collision2D_t2842956331::get_offset_of_m_Contacts_4(),
	Collision2D_t2842956331::get_offset_of_m_RelativeVelocity_5(),
	Collision2D_t2842956331::get_offset_of_m_Enabled_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1375 = { sizeof (NavMesh_t1865600375), -1, sizeof(NavMesh_t1865600375_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1375[1] = 
{
	NavMesh_t1865600375_StaticFields::get_offset_of_onPreUpdate_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1376 = { sizeof (OnNavMeshPreUpdate_t1580782682), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1377 = { sizeof (AudioSettings_t3587374600), -1, sizeof(AudioSettings_t3587374600_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1377[1] = 
{
	AudioSettings_t3587374600_StaticFields::get_offset_of_OnAudioConfigurationChanged_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1378 = { sizeof (AudioConfigurationChangeHandler_t2089929874), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1379 = { sizeof (AudioClip_t3680889665), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1379[2] = 
{
	AudioClip_t3680889665::get_offset_of_m_PCMReaderCallback_2(),
	AudioClip_t3680889665::get_offset_of_m_PCMSetPositionCallback_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1380 = { sizeof (PCMReaderCallback_t1677636661), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1381 = { sizeof (PCMSetPositionCallback_t1059417452), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1382 = { sizeof (AudioSource_t3935305588), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1383 = { sizeof (WebCamDevice_t1322781432)+ sizeof (Il2CppObject), sizeof(WebCamDevice_t1322781432_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1383[2] = 
{
	WebCamDevice_t1322781432::get_offset_of_m_Name_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	WebCamDevice_t1322781432::get_offset_of_m_Flags_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1384 = { sizeof (WebCamTexture_t1514609158), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1385 = { sizeof (SharedBetweenAnimatorsAttribute_t2857104338), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1386 = { sizeof (StateMachineBehaviour_t957311111), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1387 = { sizeof (AnimationEventSource_t3045280095)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1387[4] = 
{
	AnimationEventSource_t3045280095::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1388 = { sizeof (AnimationEvent_t1536042487), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1388[11] = 
{
	AnimationEvent_t1536042487::get_offset_of_m_Time_0(),
	AnimationEvent_t1536042487::get_offset_of_m_FunctionName_1(),
	AnimationEvent_t1536042487::get_offset_of_m_StringParameter_2(),
	AnimationEvent_t1536042487::get_offset_of_m_ObjectReferenceParameter_3(),
	AnimationEvent_t1536042487::get_offset_of_m_FloatParameter_4(),
	AnimationEvent_t1536042487::get_offset_of_m_IntParameter_5(),
	AnimationEvent_t1536042487::get_offset_of_m_MessageOptions_6(),
	AnimationEvent_t1536042487::get_offset_of_m_Source_7(),
	AnimationEvent_t1536042487::get_offset_of_m_StateSender_8(),
	AnimationEvent_t1536042487::get_offset_of_m_AnimatorStateInfo_9(),
	AnimationEvent_t1536042487::get_offset_of_m_AnimatorClipInfo_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1389 = { sizeof (AnimationClip_t2318505987), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1390 = { sizeof (AnimationState_t1108360407), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1391 = { sizeof (AnimatorControllerParameterType_t3317225440)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1391[5] = 
{
	AnimatorControllerParameterType_t3317225440::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1392 = { sizeof (AnimatorClipInfo_t3156717155)+ sizeof (Il2CppObject), sizeof(AnimatorClipInfo_t3156717155 ), 0, 0 };
extern const int32_t g_FieldOffsetTable1392[2] = 
{
	AnimatorClipInfo_t3156717155::get_offset_of_m_ClipInstanceID_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	AnimatorClipInfo_t3156717155::get_offset_of_m_Weight_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1393 = { sizeof (AnimatorStateInfo_t509032636)+ sizeof (Il2CppObject), sizeof(AnimatorStateInfo_t509032636 ), 0, 0 };
extern const int32_t g_FieldOffsetTable1393[9] = 
{
	AnimatorStateInfo_t509032636::get_offset_of_m_Name_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	AnimatorStateInfo_t509032636::get_offset_of_m_Path_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	AnimatorStateInfo_t509032636::get_offset_of_m_FullPath_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	AnimatorStateInfo_t509032636::get_offset_of_m_NormalizedTime_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	AnimatorStateInfo_t509032636::get_offset_of_m_Length_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	AnimatorStateInfo_t509032636::get_offset_of_m_Speed_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
	AnimatorStateInfo_t509032636::get_offset_of_m_SpeedMultiplier_6() + static_cast<int32_t>(sizeof(Il2CppObject)),
	AnimatorStateInfo_t509032636::get_offset_of_m_Tag_7() + static_cast<int32_t>(sizeof(Il2CppObject)),
	AnimatorStateInfo_t509032636::get_offset_of_m_Loop_8() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1394 = { sizeof (AnimatorTransitionInfo_t2534804151)+ sizeof (Il2CppObject), sizeof(AnimatorTransitionInfo_t2534804151_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1394[6] = 
{
	AnimatorTransitionInfo_t2534804151::get_offset_of_m_FullPath_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	AnimatorTransitionInfo_t2534804151::get_offset_of_m_UserName_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	AnimatorTransitionInfo_t2534804151::get_offset_of_m_Name_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	AnimatorTransitionInfo_t2534804151::get_offset_of_m_NormalizedTime_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	AnimatorTransitionInfo_t2534804151::get_offset_of_m_AnyState_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	AnimatorTransitionInfo_t2534804151::get_offset_of_m_TransitionType_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1395 = { sizeof (Animator_t434523843), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1396 = { sizeof (AnimatorControllerParameter_t1758260042), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1396[5] = 
{
	AnimatorControllerParameter_t1758260042::get_offset_of_m_Name_0(),
	AnimatorControllerParameter_t1758260042::get_offset_of_m_Type_1(),
	AnimatorControllerParameter_t1758260042::get_offset_of_m_DefaultFloat_2(),
	AnimatorControllerParameter_t1758260042::get_offset_of_m_DefaultInt_3(),
	AnimatorControllerParameter_t1758260042::get_offset_of_m_DefaultBool_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1397 = { sizeof (SkeletonBone_t4134054672)+ sizeof (Il2CppObject), sizeof(SkeletonBone_t4134054672_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1397[5] = 
{
	SkeletonBone_t4134054672::get_offset_of_name_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	SkeletonBone_t4134054672::get_offset_of_parentName_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	SkeletonBone_t4134054672::get_offset_of_position_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	SkeletonBone_t4134054672::get_offset_of_rotation_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	SkeletonBone_t4134054672::get_offset_of_scale_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1398 = { sizeof (HumanLimit_t2901552972)+ sizeof (Il2CppObject), sizeof(HumanLimit_t2901552972 ), 0, 0 };
extern const int32_t g_FieldOffsetTable1398[5] = 
{
	HumanLimit_t2901552972::get_offset_of_m_Min_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	HumanLimit_t2901552972::get_offset_of_m_Max_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	HumanLimit_t2901552972::get_offset_of_m_Center_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	HumanLimit_t2901552972::get_offset_of_m_AxisLength_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	HumanLimit_t2901552972::get_offset_of_m_UseDefaultValues_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1399 = { sizeof (HumanBone_t2465339518)+ sizeof (Il2CppObject), sizeof(HumanBone_t2465339518_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1399[3] = 
{
	HumanBone_t2465339518::get_offset_of_m_BoneName_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	HumanBone_t2465339518::get_offset_of_m_HumanName_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	HumanBone_t2465339518::get_offset_of_limit_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
