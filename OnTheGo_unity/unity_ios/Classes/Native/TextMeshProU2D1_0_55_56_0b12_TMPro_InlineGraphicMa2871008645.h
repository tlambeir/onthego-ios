﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour3962482529.h"

// TMPro.TMP_SpriteAsset
struct TMP_SpriteAsset_t484820633;
// TMPro.InlineGraphic
struct InlineGraphic_t2901727699;
// UnityEngine.CanvasRenderer
struct CanvasRenderer_t2598313366;
// UnityEngine.UIVertex[]
struct UIVertexU5BU5D_t1981460040;
// UnityEngine.RectTransform
struct RectTransform_t3704657025;
// TMPro.TMP_Text
struct TMP_Text_t2599618874;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.InlineGraphicManager
struct  InlineGraphicManager_t2871008645  : public MonoBehaviour_t3962482529
{
public:
	// TMPro.TMP_SpriteAsset TMPro.InlineGraphicManager::m_spriteAsset
	TMP_SpriteAsset_t484820633 * ___m_spriteAsset_2;
	// TMPro.InlineGraphic TMPro.InlineGraphicManager::m_inlineGraphic
	InlineGraphic_t2901727699 * ___m_inlineGraphic_3;
	// UnityEngine.CanvasRenderer TMPro.InlineGraphicManager::m_inlineGraphicCanvasRenderer
	CanvasRenderer_t2598313366 * ___m_inlineGraphicCanvasRenderer_4;
	// UnityEngine.UIVertex[] TMPro.InlineGraphicManager::m_uiVertex
	UIVertexU5BU5D_t1981460040* ___m_uiVertex_5;
	// UnityEngine.RectTransform TMPro.InlineGraphicManager::m_inlineGraphicRectTransform
	RectTransform_t3704657025 * ___m_inlineGraphicRectTransform_6;
	// TMPro.TMP_Text TMPro.InlineGraphicManager::m_textComponent
	TMP_Text_t2599618874 * ___m_textComponent_7;
	// System.Boolean TMPro.InlineGraphicManager::m_isInitialized
	bool ___m_isInitialized_8;

public:
	inline static int32_t get_offset_of_m_spriteAsset_2() { return static_cast<int32_t>(offsetof(InlineGraphicManager_t2871008645, ___m_spriteAsset_2)); }
	inline TMP_SpriteAsset_t484820633 * get_m_spriteAsset_2() const { return ___m_spriteAsset_2; }
	inline TMP_SpriteAsset_t484820633 ** get_address_of_m_spriteAsset_2() { return &___m_spriteAsset_2; }
	inline void set_m_spriteAsset_2(TMP_SpriteAsset_t484820633 * value)
	{
		___m_spriteAsset_2 = value;
		Il2CppCodeGenWriteBarrier(&___m_spriteAsset_2, value);
	}

	inline static int32_t get_offset_of_m_inlineGraphic_3() { return static_cast<int32_t>(offsetof(InlineGraphicManager_t2871008645, ___m_inlineGraphic_3)); }
	inline InlineGraphic_t2901727699 * get_m_inlineGraphic_3() const { return ___m_inlineGraphic_3; }
	inline InlineGraphic_t2901727699 ** get_address_of_m_inlineGraphic_3() { return &___m_inlineGraphic_3; }
	inline void set_m_inlineGraphic_3(InlineGraphic_t2901727699 * value)
	{
		___m_inlineGraphic_3 = value;
		Il2CppCodeGenWriteBarrier(&___m_inlineGraphic_3, value);
	}

	inline static int32_t get_offset_of_m_inlineGraphicCanvasRenderer_4() { return static_cast<int32_t>(offsetof(InlineGraphicManager_t2871008645, ___m_inlineGraphicCanvasRenderer_4)); }
	inline CanvasRenderer_t2598313366 * get_m_inlineGraphicCanvasRenderer_4() const { return ___m_inlineGraphicCanvasRenderer_4; }
	inline CanvasRenderer_t2598313366 ** get_address_of_m_inlineGraphicCanvasRenderer_4() { return &___m_inlineGraphicCanvasRenderer_4; }
	inline void set_m_inlineGraphicCanvasRenderer_4(CanvasRenderer_t2598313366 * value)
	{
		___m_inlineGraphicCanvasRenderer_4 = value;
		Il2CppCodeGenWriteBarrier(&___m_inlineGraphicCanvasRenderer_4, value);
	}

	inline static int32_t get_offset_of_m_uiVertex_5() { return static_cast<int32_t>(offsetof(InlineGraphicManager_t2871008645, ___m_uiVertex_5)); }
	inline UIVertexU5BU5D_t1981460040* get_m_uiVertex_5() const { return ___m_uiVertex_5; }
	inline UIVertexU5BU5D_t1981460040** get_address_of_m_uiVertex_5() { return &___m_uiVertex_5; }
	inline void set_m_uiVertex_5(UIVertexU5BU5D_t1981460040* value)
	{
		___m_uiVertex_5 = value;
		Il2CppCodeGenWriteBarrier(&___m_uiVertex_5, value);
	}

	inline static int32_t get_offset_of_m_inlineGraphicRectTransform_6() { return static_cast<int32_t>(offsetof(InlineGraphicManager_t2871008645, ___m_inlineGraphicRectTransform_6)); }
	inline RectTransform_t3704657025 * get_m_inlineGraphicRectTransform_6() const { return ___m_inlineGraphicRectTransform_6; }
	inline RectTransform_t3704657025 ** get_address_of_m_inlineGraphicRectTransform_6() { return &___m_inlineGraphicRectTransform_6; }
	inline void set_m_inlineGraphicRectTransform_6(RectTransform_t3704657025 * value)
	{
		___m_inlineGraphicRectTransform_6 = value;
		Il2CppCodeGenWriteBarrier(&___m_inlineGraphicRectTransform_6, value);
	}

	inline static int32_t get_offset_of_m_textComponent_7() { return static_cast<int32_t>(offsetof(InlineGraphicManager_t2871008645, ___m_textComponent_7)); }
	inline TMP_Text_t2599618874 * get_m_textComponent_7() const { return ___m_textComponent_7; }
	inline TMP_Text_t2599618874 ** get_address_of_m_textComponent_7() { return &___m_textComponent_7; }
	inline void set_m_textComponent_7(TMP_Text_t2599618874 * value)
	{
		___m_textComponent_7 = value;
		Il2CppCodeGenWriteBarrier(&___m_textComponent_7, value);
	}

	inline static int32_t get_offset_of_m_isInitialized_8() { return static_cast<int32_t>(offsetof(InlineGraphicManager_t2871008645, ___m_isInitialized_8)); }
	inline bool get_m_isInitialized_8() const { return ___m_isInitialized_8; }
	inline bool* get_address_of_m_isInitialized_8() { return &___m_isInitialized_8; }
	inline void set_m_isInitialized_8(bool value)
	{
		___m_isInitialized_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
