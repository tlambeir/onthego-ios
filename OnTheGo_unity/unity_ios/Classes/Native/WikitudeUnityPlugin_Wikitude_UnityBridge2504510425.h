﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.Single[]
struct SingleU5BU5D_t1444911251;
// System.Int64[]
struct Int64U5BU5D_t2559172825;
// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Wikitude.UnityBridge
struct  UnityBridge_t2504510425  : public Il2CppObject
{
public:
	// System.Single[] Wikitude.UnityBridge::GenericTrackingMatrices
	SingleU5BU5D_t1444911251* ___GenericTrackingMatrices_0;
	// System.Single[] Wikitude.UnityBridge::GenericProjectionMatrix
	SingleU5BU5D_t1444911251* ___GenericProjectionMatrix_1;
	// System.Single[] Wikitude.UnityBridge::ObjectTrackingMatrix
	SingleU5BU5D_t1444911251* ___ObjectTrackingMatrix_2;
	// System.Single[] Wikitude.UnityBridge::ObjectProjectionMatrix
	SingleU5BU5D_t1444911251* ___ObjectProjectionMatrix_3;
	// System.Int64[] Wikitude.UnityBridge::TargetIDs
	Int64U5BU5D_t2559172825* ___TargetIDs_4;
	// System.String Wikitude.UnityBridge::TrackedTargets
	String_t* ___TrackedTargets_5;
	// System.Int32 Wikitude.UnityBridge::TargetCount
	int32_t ___TargetCount_6;
	// System.Boolean Wikitude.UnityBridge::_isObjectTrackingRunning
	bool ____isObjectTrackingRunning_7;
	// System.String Wikitude.UnityBridge::_trackerManagerName
	String_t* ____trackerManagerName_8;

public:
	inline static int32_t get_offset_of_GenericTrackingMatrices_0() { return static_cast<int32_t>(offsetof(UnityBridge_t2504510425, ___GenericTrackingMatrices_0)); }
	inline SingleU5BU5D_t1444911251* get_GenericTrackingMatrices_0() const { return ___GenericTrackingMatrices_0; }
	inline SingleU5BU5D_t1444911251** get_address_of_GenericTrackingMatrices_0() { return &___GenericTrackingMatrices_0; }
	inline void set_GenericTrackingMatrices_0(SingleU5BU5D_t1444911251* value)
	{
		___GenericTrackingMatrices_0 = value;
		Il2CppCodeGenWriteBarrier(&___GenericTrackingMatrices_0, value);
	}

	inline static int32_t get_offset_of_GenericProjectionMatrix_1() { return static_cast<int32_t>(offsetof(UnityBridge_t2504510425, ___GenericProjectionMatrix_1)); }
	inline SingleU5BU5D_t1444911251* get_GenericProjectionMatrix_1() const { return ___GenericProjectionMatrix_1; }
	inline SingleU5BU5D_t1444911251** get_address_of_GenericProjectionMatrix_1() { return &___GenericProjectionMatrix_1; }
	inline void set_GenericProjectionMatrix_1(SingleU5BU5D_t1444911251* value)
	{
		___GenericProjectionMatrix_1 = value;
		Il2CppCodeGenWriteBarrier(&___GenericProjectionMatrix_1, value);
	}

	inline static int32_t get_offset_of_ObjectTrackingMatrix_2() { return static_cast<int32_t>(offsetof(UnityBridge_t2504510425, ___ObjectTrackingMatrix_2)); }
	inline SingleU5BU5D_t1444911251* get_ObjectTrackingMatrix_2() const { return ___ObjectTrackingMatrix_2; }
	inline SingleU5BU5D_t1444911251** get_address_of_ObjectTrackingMatrix_2() { return &___ObjectTrackingMatrix_2; }
	inline void set_ObjectTrackingMatrix_2(SingleU5BU5D_t1444911251* value)
	{
		___ObjectTrackingMatrix_2 = value;
		Il2CppCodeGenWriteBarrier(&___ObjectTrackingMatrix_2, value);
	}

	inline static int32_t get_offset_of_ObjectProjectionMatrix_3() { return static_cast<int32_t>(offsetof(UnityBridge_t2504510425, ___ObjectProjectionMatrix_3)); }
	inline SingleU5BU5D_t1444911251* get_ObjectProjectionMatrix_3() const { return ___ObjectProjectionMatrix_3; }
	inline SingleU5BU5D_t1444911251** get_address_of_ObjectProjectionMatrix_3() { return &___ObjectProjectionMatrix_3; }
	inline void set_ObjectProjectionMatrix_3(SingleU5BU5D_t1444911251* value)
	{
		___ObjectProjectionMatrix_3 = value;
		Il2CppCodeGenWriteBarrier(&___ObjectProjectionMatrix_3, value);
	}

	inline static int32_t get_offset_of_TargetIDs_4() { return static_cast<int32_t>(offsetof(UnityBridge_t2504510425, ___TargetIDs_4)); }
	inline Int64U5BU5D_t2559172825* get_TargetIDs_4() const { return ___TargetIDs_4; }
	inline Int64U5BU5D_t2559172825** get_address_of_TargetIDs_4() { return &___TargetIDs_4; }
	inline void set_TargetIDs_4(Int64U5BU5D_t2559172825* value)
	{
		___TargetIDs_4 = value;
		Il2CppCodeGenWriteBarrier(&___TargetIDs_4, value);
	}

	inline static int32_t get_offset_of_TrackedTargets_5() { return static_cast<int32_t>(offsetof(UnityBridge_t2504510425, ___TrackedTargets_5)); }
	inline String_t* get_TrackedTargets_5() const { return ___TrackedTargets_5; }
	inline String_t** get_address_of_TrackedTargets_5() { return &___TrackedTargets_5; }
	inline void set_TrackedTargets_5(String_t* value)
	{
		___TrackedTargets_5 = value;
		Il2CppCodeGenWriteBarrier(&___TrackedTargets_5, value);
	}

	inline static int32_t get_offset_of_TargetCount_6() { return static_cast<int32_t>(offsetof(UnityBridge_t2504510425, ___TargetCount_6)); }
	inline int32_t get_TargetCount_6() const { return ___TargetCount_6; }
	inline int32_t* get_address_of_TargetCount_6() { return &___TargetCount_6; }
	inline void set_TargetCount_6(int32_t value)
	{
		___TargetCount_6 = value;
	}

	inline static int32_t get_offset_of__isObjectTrackingRunning_7() { return static_cast<int32_t>(offsetof(UnityBridge_t2504510425, ____isObjectTrackingRunning_7)); }
	inline bool get__isObjectTrackingRunning_7() const { return ____isObjectTrackingRunning_7; }
	inline bool* get_address_of__isObjectTrackingRunning_7() { return &____isObjectTrackingRunning_7; }
	inline void set__isObjectTrackingRunning_7(bool value)
	{
		____isObjectTrackingRunning_7 = value;
	}

	inline static int32_t get_offset_of__trackerManagerName_8() { return static_cast<int32_t>(offsetof(UnityBridge_t2504510425, ____trackerManagerName_8)); }
	inline String_t* get__trackerManagerName_8() const { return ____trackerManagerName_8; }
	inline String_t** get_address_of__trackerManagerName_8() { return &____trackerManagerName_8; }
	inline void set__trackerManagerName_8(String_t* value)
	{
		____trackerManagerName_8 = value;
		Il2CppCodeGenWriteBarrier(&____trackerManagerName_8, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
