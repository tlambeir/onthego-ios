﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_Events_UnityEvent_1_gen3204000826.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Wikitude.ImageTrackable/OnImageRecognizedEvent
struct  OnImageRecognizedEvent_t773132985  : public UnityEvent_1_t3204000826
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
