﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// Wikitude.PlatformBase
struct PlatformBase_t2513924928;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Wikitude.PlatformBase
struct  PlatformBase_t2513924928  : public Il2CppObject
{
public:

public:
};

struct PlatformBase_t2513924928_StaticFields
{
public:
	// Wikitude.PlatformBase Wikitude.PlatformBase::_instance
	PlatformBase_t2513924928 * ____instance_0;

public:
	inline static int32_t get_offset_of__instance_0() { return static_cast<int32_t>(offsetof(PlatformBase_t2513924928_StaticFields, ____instance_0)); }
	inline PlatformBase_t2513924928 * get__instance_0() const { return ____instance_0; }
	inline PlatformBase_t2513924928 ** get_address_of__instance_0() { return &____instance_0; }
	inline void set__instance_0(PlatformBase_t2513924928 * value)
	{
		____instance_0 = value;
		Il2CppCodeGenWriteBarrier(&____instance_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
