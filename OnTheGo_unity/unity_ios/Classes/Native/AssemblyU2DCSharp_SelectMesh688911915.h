﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour3962482529.h"

// MenuController
struct MenuController_t3930949237;
// Branch
struct Branch_t1428165248;
// UnityEngine.GameObject
struct GameObject_t1113636619;
// UnityEngine.UI.Text
struct Text_t1901882714;
// TMPro.TextMeshProUGUI
struct TextMeshProUGUI_t529313277;
// UnityEngine.Transform
struct Transform_t3600365921;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SelectMesh
struct  SelectMesh_t688911915  : public MonoBehaviour_t3962482529
{
public:
	// MenuController SelectMesh::MenuController
	MenuController_t3930949237 * ___MenuController_2;
	// Branch SelectMesh::root
	Branch_t1428165248 * ___root_3;
	// UnityEngine.GameObject SelectMesh::newMenuItem
	GameObject_t1113636619 * ___newMenuItem_4;
	// UnityEngine.UI.Text SelectMesh::Title
	Text_t1901882714 * ___Title_5;
	// TMPro.TextMeshProUGUI SelectMesh::Description
	TextMeshProUGUI_t529313277 * ___Description_6;
	// UnityEngine.Transform SelectMesh::verticalLayoutGroup
	Transform_t3600365921 * ___verticalLayoutGroup_7;
	// UnityEngine.GameObject SelectMesh::DescriptionGO
	GameObject_t1113636619 * ___DescriptionGO_8;

public:
	inline static int32_t get_offset_of_MenuController_2() { return static_cast<int32_t>(offsetof(SelectMesh_t688911915, ___MenuController_2)); }
	inline MenuController_t3930949237 * get_MenuController_2() const { return ___MenuController_2; }
	inline MenuController_t3930949237 ** get_address_of_MenuController_2() { return &___MenuController_2; }
	inline void set_MenuController_2(MenuController_t3930949237 * value)
	{
		___MenuController_2 = value;
		Il2CppCodeGenWriteBarrier(&___MenuController_2, value);
	}

	inline static int32_t get_offset_of_root_3() { return static_cast<int32_t>(offsetof(SelectMesh_t688911915, ___root_3)); }
	inline Branch_t1428165248 * get_root_3() const { return ___root_3; }
	inline Branch_t1428165248 ** get_address_of_root_3() { return &___root_3; }
	inline void set_root_3(Branch_t1428165248 * value)
	{
		___root_3 = value;
		Il2CppCodeGenWriteBarrier(&___root_3, value);
	}

	inline static int32_t get_offset_of_newMenuItem_4() { return static_cast<int32_t>(offsetof(SelectMesh_t688911915, ___newMenuItem_4)); }
	inline GameObject_t1113636619 * get_newMenuItem_4() const { return ___newMenuItem_4; }
	inline GameObject_t1113636619 ** get_address_of_newMenuItem_4() { return &___newMenuItem_4; }
	inline void set_newMenuItem_4(GameObject_t1113636619 * value)
	{
		___newMenuItem_4 = value;
		Il2CppCodeGenWriteBarrier(&___newMenuItem_4, value);
	}

	inline static int32_t get_offset_of_Title_5() { return static_cast<int32_t>(offsetof(SelectMesh_t688911915, ___Title_5)); }
	inline Text_t1901882714 * get_Title_5() const { return ___Title_5; }
	inline Text_t1901882714 ** get_address_of_Title_5() { return &___Title_5; }
	inline void set_Title_5(Text_t1901882714 * value)
	{
		___Title_5 = value;
		Il2CppCodeGenWriteBarrier(&___Title_5, value);
	}

	inline static int32_t get_offset_of_Description_6() { return static_cast<int32_t>(offsetof(SelectMesh_t688911915, ___Description_6)); }
	inline TextMeshProUGUI_t529313277 * get_Description_6() const { return ___Description_6; }
	inline TextMeshProUGUI_t529313277 ** get_address_of_Description_6() { return &___Description_6; }
	inline void set_Description_6(TextMeshProUGUI_t529313277 * value)
	{
		___Description_6 = value;
		Il2CppCodeGenWriteBarrier(&___Description_6, value);
	}

	inline static int32_t get_offset_of_verticalLayoutGroup_7() { return static_cast<int32_t>(offsetof(SelectMesh_t688911915, ___verticalLayoutGroup_7)); }
	inline Transform_t3600365921 * get_verticalLayoutGroup_7() const { return ___verticalLayoutGroup_7; }
	inline Transform_t3600365921 ** get_address_of_verticalLayoutGroup_7() { return &___verticalLayoutGroup_7; }
	inline void set_verticalLayoutGroup_7(Transform_t3600365921 * value)
	{
		___verticalLayoutGroup_7 = value;
		Il2CppCodeGenWriteBarrier(&___verticalLayoutGroup_7, value);
	}

	inline static int32_t get_offset_of_DescriptionGO_8() { return static_cast<int32_t>(offsetof(SelectMesh_t688911915, ___DescriptionGO_8)); }
	inline GameObject_t1113636619 * get_DescriptionGO_8() const { return ___DescriptionGO_8; }
	inline GameObject_t1113636619 ** get_address_of_DescriptionGO_8() { return &___DescriptionGO_8; }
	inline void set_DescriptionGO_8(GameObject_t1113636619 * value)
	{
		___DescriptionGO_8 = value;
		Il2CppCodeGenWriteBarrier(&___DescriptionGO_8, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
