﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Collections_ReadOnlyAttrib2029203740.h"
#include "UnityEngine_UnityEngine_Collections_ReadWriteAttrib306517538.h"
#include "UnityEngine_UnityEngine_Collections_WriteOnlyAttrib595109273.h"
#include "UnityEngine_UnityEngine_Collections_DeallocateOnJo3131681843.h"
#include "UnityEngine_UnityEngine_Collections_NativeContaine2600515814.h"
#include "UnityEngine_UnityEngine_Collections_NativeContaine3790689680.h"
#include "UnityEngine_UnityEngine_Collections_NativeContaine1586929818.h"
#include "UnityEngine_UnityEngine_Experimental_Director_Fram3985821396.h"
#include "UnityEngine_UnityEngine_Experimental_Director_Fram4130423782.h"
#include "UnityEngine_UnityEngine_Internal_DefaultValueAttri2337225216.h"
#include "UnityEngine_UnityEngine_Internal_ExcludeFromDocsAt3592494112.h"
#include "UnityEngine_UnityEngine_Logger274032455.h"
#include "UnityEngine_UnityEngine_Networking_PlayerConnectio1170575784.h"
#include "UnityEngine_UnityEngine_Networking_PlayerConnectio3081694049.h"
#include "UnityEngine_UnityEngine_Networking_PlayerConnectio3411787513.h"
#include "UnityEngine_UnityEngine_Networking_PlayerConnectio1491597365.h"
#include "UnityEngine_UnityEngine_Networking_PlayerConnectio3251856151.h"
#include "UnityEngine_UnityEngine_Networking_PlayerConnectio1684935770.h"
#include "UnityEngine_UnityEngine_Networking_PlayerConnection907692441.h"
#include "UnityEngine_UnityEngine_Experimental_Rendering_Ren4036911426.h"
#include "UnityEngine_UnityEngine_Scripting_PreserveAttribut1583619344.h"
#include "UnityEngine_UnityEngine_Scripting_UsedByNativeCode1703770351.h"
#include "UnityEngine_UnityEngine_Scripting_RequiredByNative4130846357.h"
#include "UnityEngine_UnityEngine_Scripting_APIUpdating_Moved481952341.h"
#include "UnityEngine_UnityEngine_Serialization_FormerlySeri2859083114.h"
#include "UnityEngine_UnityEngineInternal_TypeInferenceRules96689094.h"
#include "UnityEngine_UnityEngineInternal_TypeInferenceRuleAt254868554.h"
#include "UnityEngine_UnityEngineInternal_GenericStack1310059385.h"
#include "UnityEngine_UnityEngineInternal_NetFxCoreExtension4089902045.h"
#include "UnityEngine_UI_U3CModuleU3E692745525.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_EventHandle600343995.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_EventSyste1003666588.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_EventTrigg1076084509.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_EventTrigg3867320123.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_EventTrigg3344766165.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_EventTrigger55832929.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_ExecuteEve3484638744.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_MoveDirect1216237838.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_RaycasterM2536340562.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_RaycastRes3360306849.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_UIBehaviou3495933518.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_AxisEventD2331243652.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_AbstractEv4171500731.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_BaseEventD3903027533.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_PointerEve3807901092.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_PointerEve3704011348.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_PointerEve3039385657.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_BaseInput3630163547.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_BaseInputM2019268878.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_PointerInp3453173740.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_PointerInpu857139936.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_PointerInpu384203932.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_PointerInp3190347560.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_Standalone2760469101.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_Standalone3382566315.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_TouchInput4248229598.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_BaseRaycas4150874583.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_Physics2DR3382992964.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_PhysicsRayc437419520.h"
#include "UnityEngine_UI_UnityEngine_UI_CoroutineTween_ColorT809614380.h"
#include "UnityEngine_UI_UnityEngine_UI_CoroutineTween_Color1000778859.h"
#include "UnityEngine_UI_UnityEngine_UI_CoroutineTween_Color1121741130.h"
#include "UnityEngine_UI_UnityEngine_UI_CoroutineTween_Float1274330004.h"
#include "UnityEngine_UI_UnityEngine_UI_CoroutineTween_Float1856710240.h"
#include "UnityEngine_UI_UnityEngine_UI_AnimationTriggers2532145056.h"
#include "UnityEngine_UI_UnityEngine_UI_Button4055032469.h"
#include "UnityEngine_UI_UnityEngine_UI_Button_ButtonClickedEv48803504.h"
#include "UnityEngine_UI_UnityEngine_UI_Button_U3COnFinishSu3413438900.h"
#include "UnityEngine_UI_UnityEngine_UI_CanvasUpdate2572322932.h"
#include "UnityEngine_UI_UnityEngine_UI_CanvasUpdateRegistry2720824592.h"
#include "UnityEngine_UI_UnityEngine_UI_ColorBlock2139031574.h"
#include "UnityEngine_UI_UnityEngine_UI_DefaultControls4098465386.h"







#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1600 = { sizeof (ReadOnlyAttribute_t2029203740), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1601 = { sizeof (ReadWriteAttribute_t306517538), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1602 = { sizeof (WriteOnlyAttribute_t595109273), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1603 = { sizeof (DeallocateOnJobCompletionAttribute_t3131681843), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1604 = { sizeof (NativeContainerAttribute_t2600515814), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1605 = { sizeof (NativeContainerSupportsAtomicWriteAttribute_t3790689680), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1606 = { sizeof (NativeContainerSupportsMinMaxWriteRestrictionAttribute_t1586929818), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1607 = { sizeof (FrameData_t3985821396)+ sizeof (Il2CppObject), sizeof(FrameData_t3985821396 ), 0, 0 };
extern const int32_t g_FieldOffsetTable1607[6] = 
{
	FrameData_t3985821396::get_offset_of_m_FrameID_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	FrameData_t3985821396::get_offset_of_m_DeltaTime_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	FrameData_t3985821396::get_offset_of_m_Weight_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	FrameData_t3985821396::get_offset_of_m_EffectiveWeight_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	FrameData_t3985821396::get_offset_of_m_EffectiveSpeed_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	FrameData_t3985821396::get_offset_of_m_Flags_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1608 = { sizeof (Flags_t4130423782)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1608[3] = 
{
	Flags_t4130423782::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1609 = { sizeof (DefaultValueAttribute_t2337225216), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1609[1] = 
{
	DefaultValueAttribute_t2337225216::get_offset_of_DefaultValue_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1610 = { sizeof (ExcludeFromDocsAttribute_t3592494112), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1611 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1612 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1613 = { sizeof (Logger_t274032455), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1613[3] = 
{
	Logger_t274032455::get_offset_of_U3ClogHandlerU3Ek__BackingField_0(),
	Logger_t274032455::get_offset_of_U3ClogEnabledU3Ek__BackingField_1(),
	Logger_t274032455::get_offset_of_U3CfilterLogTypeU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1614 = { sizeof (MessageEventArgs_t1170575784), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1614[2] = 
{
	MessageEventArgs_t1170575784::get_offset_of_playerId_0(),
	MessageEventArgs_t1170575784::get_offset_of_data_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1615 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1616 = { sizeof (PlayerConnection_t3081694049), -1, sizeof(PlayerConnection_t3081694049_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1616[3] = 
{
	PlayerConnection_t3081694049::get_offset_of_m_PlayerEditorConnectionEvents_2(),
	PlayerConnection_t3081694049::get_offset_of_m_connectedPlayers_3(),
	PlayerConnection_t3081694049_StaticFields::get_offset_of_s_Instance_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1617 = { sizeof (PlayerEditorConnectionEvents_t3411787513), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1617[3] = 
{
	PlayerEditorConnectionEvents_t3411787513::get_offset_of_messageTypeSubscribers_0(),
	PlayerEditorConnectionEvents_t3411787513::get_offset_of_connectionEvent_1(),
	PlayerEditorConnectionEvents_t3411787513::get_offset_of_disconnectionEvent_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1618 = { sizeof (MessageEvent_t1491597365), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1619 = { sizeof (ConnectionChangeEvent_t3251856151), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1620 = { sizeof (MessageTypeSubscribers_t1684935770), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1620[3] = 
{
	MessageTypeSubscribers_t1684935770::get_offset_of_m_messageTypeId_0(),
	MessageTypeSubscribers_t1684935770::get_offset_of_subscriberCount_1(),
	MessageTypeSubscribers_t1684935770::get_offset_of_messageCallback_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1621 = { sizeof (U3CInvokeMessageIdSubscribersU3Ec__AnonStorey0_t907692441), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1621[1] = 
{
	U3CInvokeMessageIdSubscribersU3Ec__AnonStorey0_t907692441::get_offset_of_messageId_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1622 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1623 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1624 = { sizeof (RenderPipelineManager_t4036911426), -1, sizeof(RenderPipelineManager_t4036911426_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1624[2] = 
{
	RenderPipelineManager_t4036911426_StaticFields::get_offset_of_s_CurrentPipelineAsset_0(),
	RenderPipelineManager_t4036911426_StaticFields::get_offset_of_U3CcurrentPipelineU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1625 = { sizeof (PreserveAttribute_t1583619344), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1626 = { sizeof (UsedByNativeCodeAttribute_t1703770351), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1627 = { sizeof (RequiredByNativeCodeAttribute_t4130846357), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1627[2] = 
{
	RequiredByNativeCodeAttribute_t4130846357::get_offset_of_U3CNameU3Ek__BackingField_0(),
	RequiredByNativeCodeAttribute_t4130846357::get_offset_of_U3COptionalU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1628 = { sizeof (MovedFromAttribute_t481952341), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1628[2] = 
{
	MovedFromAttribute_t481952341::get_offset_of_U3CNamespaceU3Ek__BackingField_0(),
	MovedFromAttribute_t481952341::get_offset_of_U3CIsInDifferentAssemblyU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1629 = { sizeof (FormerlySerializedAsAttribute_t2859083114), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1629[1] = 
{
	FormerlySerializedAsAttribute_t2859083114::get_offset_of_m_oldName_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1630 = { sizeof (TypeInferenceRules_t96689094)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1630[5] = 
{
	TypeInferenceRules_t96689094::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1631 = { sizeof (TypeInferenceRuleAttribute_t254868554), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1631[1] = 
{
	TypeInferenceRuleAttribute_t254868554::get_offset_of__rule_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1632 = { sizeof (GenericStack_t1310059385), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1633 = { sizeof (NetFxCoreExtensions_t4089902045), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1634 = { sizeof (U3CModuleU3E_t692745530), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1635 = { sizeof (EventHandle_t600343995)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1635[3] = 
{
	EventHandle_t600343995::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1636 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1637 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1638 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1639 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1640 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1641 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1642 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1643 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1644 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1645 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1646 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1647 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1648 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1649 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1650 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1651 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1652 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1653 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1654 = { sizeof (EventSystem_t1003666588), -1, sizeof(EventSystem_t1003666588_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1654[12] = 
{
	EventSystem_t1003666588::get_offset_of_m_SystemInputModules_2(),
	EventSystem_t1003666588::get_offset_of_m_CurrentInputModule_3(),
	EventSystem_t1003666588_StaticFields::get_offset_of_U3CcurrentU3Ek__BackingField_4(),
	EventSystem_t1003666588::get_offset_of_m_FirstSelected_5(),
	EventSystem_t1003666588::get_offset_of_m_sendNavigationEvents_6(),
	EventSystem_t1003666588::get_offset_of_m_DragThreshold_7(),
	EventSystem_t1003666588::get_offset_of_m_CurrentSelected_8(),
	EventSystem_t1003666588::get_offset_of_m_HasFocus_9(),
	EventSystem_t1003666588::get_offset_of_m_SelectionGuard_10(),
	EventSystem_t1003666588::get_offset_of_m_DummyData_11(),
	EventSystem_t1003666588_StaticFields::get_offset_of_s_RaycastComparer_12(),
	EventSystem_t1003666588_StaticFields::get_offset_of_U3CU3Ef__mgU24cache0_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1655 = { sizeof (EventTrigger_t1076084509), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1655[2] = 
{
	EventTrigger_t1076084509::get_offset_of_m_Delegates_2(),
	EventTrigger_t1076084509::get_offset_of_delegates_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1656 = { sizeof (TriggerEvent_t3867320123), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1657 = { sizeof (Entry_t3344766165), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1657[2] = 
{
	Entry_t3344766165::get_offset_of_eventID_0(),
	Entry_t3344766165::get_offset_of_callback_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1658 = { sizeof (EventTriggerType_t55832929)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1658[18] = 
{
	EventTriggerType_t55832929::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1659 = { sizeof (ExecuteEvents_t3484638744), -1, sizeof(ExecuteEvents_t3484638744_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1659[36] = 
{
	ExecuteEvents_t3484638744_StaticFields::get_offset_of_s_PointerEnterHandler_0(),
	ExecuteEvents_t3484638744_StaticFields::get_offset_of_s_PointerExitHandler_1(),
	ExecuteEvents_t3484638744_StaticFields::get_offset_of_s_PointerDownHandler_2(),
	ExecuteEvents_t3484638744_StaticFields::get_offset_of_s_PointerUpHandler_3(),
	ExecuteEvents_t3484638744_StaticFields::get_offset_of_s_PointerClickHandler_4(),
	ExecuteEvents_t3484638744_StaticFields::get_offset_of_s_InitializePotentialDragHandler_5(),
	ExecuteEvents_t3484638744_StaticFields::get_offset_of_s_BeginDragHandler_6(),
	ExecuteEvents_t3484638744_StaticFields::get_offset_of_s_DragHandler_7(),
	ExecuteEvents_t3484638744_StaticFields::get_offset_of_s_EndDragHandler_8(),
	ExecuteEvents_t3484638744_StaticFields::get_offset_of_s_DropHandler_9(),
	ExecuteEvents_t3484638744_StaticFields::get_offset_of_s_ScrollHandler_10(),
	ExecuteEvents_t3484638744_StaticFields::get_offset_of_s_UpdateSelectedHandler_11(),
	ExecuteEvents_t3484638744_StaticFields::get_offset_of_s_SelectHandler_12(),
	ExecuteEvents_t3484638744_StaticFields::get_offset_of_s_DeselectHandler_13(),
	ExecuteEvents_t3484638744_StaticFields::get_offset_of_s_MoveHandler_14(),
	ExecuteEvents_t3484638744_StaticFields::get_offset_of_s_SubmitHandler_15(),
	ExecuteEvents_t3484638744_StaticFields::get_offset_of_s_CancelHandler_16(),
	ExecuteEvents_t3484638744_StaticFields::get_offset_of_s_HandlerListPool_17(),
	ExecuteEvents_t3484638744_StaticFields::get_offset_of_s_InternalTransformList_18(),
	ExecuteEvents_t3484638744_StaticFields::get_offset_of_U3CU3Ef__mgU24cache0_19(),
	ExecuteEvents_t3484638744_StaticFields::get_offset_of_U3CU3Ef__mgU24cache1_20(),
	ExecuteEvents_t3484638744_StaticFields::get_offset_of_U3CU3Ef__mgU24cache2_21(),
	ExecuteEvents_t3484638744_StaticFields::get_offset_of_U3CU3Ef__mgU24cache3_22(),
	ExecuteEvents_t3484638744_StaticFields::get_offset_of_U3CU3Ef__mgU24cache4_23(),
	ExecuteEvents_t3484638744_StaticFields::get_offset_of_U3CU3Ef__mgU24cache5_24(),
	ExecuteEvents_t3484638744_StaticFields::get_offset_of_U3CU3Ef__mgU24cache6_25(),
	ExecuteEvents_t3484638744_StaticFields::get_offset_of_U3CU3Ef__mgU24cache7_26(),
	ExecuteEvents_t3484638744_StaticFields::get_offset_of_U3CU3Ef__mgU24cache8_27(),
	ExecuteEvents_t3484638744_StaticFields::get_offset_of_U3CU3Ef__mgU24cache9_28(),
	ExecuteEvents_t3484638744_StaticFields::get_offset_of_U3CU3Ef__mgU24cacheA_29(),
	ExecuteEvents_t3484638744_StaticFields::get_offset_of_U3CU3Ef__mgU24cacheB_30(),
	ExecuteEvents_t3484638744_StaticFields::get_offset_of_U3CU3Ef__mgU24cacheC_31(),
	ExecuteEvents_t3484638744_StaticFields::get_offset_of_U3CU3Ef__mgU24cacheD_32(),
	ExecuteEvents_t3484638744_StaticFields::get_offset_of_U3CU3Ef__mgU24cacheE_33(),
	ExecuteEvents_t3484638744_StaticFields::get_offset_of_U3CU3Ef__mgU24cacheF_34(),
	ExecuteEvents_t3484638744_StaticFields::get_offset_of_U3CU3Ef__mgU24cache10_35(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1660 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1661 = { sizeof (MoveDirection_t1216237838)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1661[6] = 
{
	MoveDirection_t1216237838::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1662 = { sizeof (RaycasterManager_t2536340562), -1, sizeof(RaycasterManager_t2536340562_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1662[1] = 
{
	RaycasterManager_t2536340562_StaticFields::get_offset_of_s_Raycasters_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1663 = { sizeof (RaycastResult_t3360306849)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1663[10] = 
{
	RaycastResult_t3360306849::get_offset_of_m_GameObject_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RaycastResult_t3360306849::get_offset_of_module_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RaycastResult_t3360306849::get_offset_of_distance_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RaycastResult_t3360306849::get_offset_of_index_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RaycastResult_t3360306849::get_offset_of_depth_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RaycastResult_t3360306849::get_offset_of_sortingLayer_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RaycastResult_t3360306849::get_offset_of_sortingOrder_6() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RaycastResult_t3360306849::get_offset_of_worldPosition_7() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RaycastResult_t3360306849::get_offset_of_worldNormal_8() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RaycastResult_t3360306849::get_offset_of_screenPosition_9() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1664 = { sizeof (UIBehaviour_t3495933518), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1665 = { sizeof (AxisEventData_t2331243652), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1665[2] = 
{
	AxisEventData_t2331243652::get_offset_of_U3CmoveVectorU3Ek__BackingField_2(),
	AxisEventData_t2331243652::get_offset_of_U3CmoveDirU3Ek__BackingField_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1666 = { sizeof (AbstractEventData_t4171500731), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1666[1] = 
{
	AbstractEventData_t4171500731::get_offset_of_m_Used_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1667 = { sizeof (BaseEventData_t3903027533), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1667[1] = 
{
	BaseEventData_t3903027533::get_offset_of_m_EventSystem_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1668 = { sizeof (PointerEventData_t3807901092), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1668[21] = 
{
	PointerEventData_t3807901092::get_offset_of_U3CpointerEnterU3Ek__BackingField_2(),
	PointerEventData_t3807901092::get_offset_of_m_PointerPress_3(),
	PointerEventData_t3807901092::get_offset_of_U3ClastPressU3Ek__BackingField_4(),
	PointerEventData_t3807901092::get_offset_of_U3CrawPointerPressU3Ek__BackingField_5(),
	PointerEventData_t3807901092::get_offset_of_U3CpointerDragU3Ek__BackingField_6(),
	PointerEventData_t3807901092::get_offset_of_U3CpointerCurrentRaycastU3Ek__BackingField_7(),
	PointerEventData_t3807901092::get_offset_of_U3CpointerPressRaycastU3Ek__BackingField_8(),
	PointerEventData_t3807901092::get_offset_of_hovered_9(),
	PointerEventData_t3807901092::get_offset_of_U3CeligibleForClickU3Ek__BackingField_10(),
	PointerEventData_t3807901092::get_offset_of_U3CpointerIdU3Ek__BackingField_11(),
	PointerEventData_t3807901092::get_offset_of_U3CpositionU3Ek__BackingField_12(),
	PointerEventData_t3807901092::get_offset_of_U3CdeltaU3Ek__BackingField_13(),
	PointerEventData_t3807901092::get_offset_of_U3CpressPositionU3Ek__BackingField_14(),
	PointerEventData_t3807901092::get_offset_of_U3CworldPositionU3Ek__BackingField_15(),
	PointerEventData_t3807901092::get_offset_of_U3CworldNormalU3Ek__BackingField_16(),
	PointerEventData_t3807901092::get_offset_of_U3CclickTimeU3Ek__BackingField_17(),
	PointerEventData_t3807901092::get_offset_of_U3CclickCountU3Ek__BackingField_18(),
	PointerEventData_t3807901092::get_offset_of_U3CscrollDeltaU3Ek__BackingField_19(),
	PointerEventData_t3807901092::get_offset_of_U3CuseDragThresholdU3Ek__BackingField_20(),
	PointerEventData_t3807901092::get_offset_of_U3CdraggingU3Ek__BackingField_21(),
	PointerEventData_t3807901092::get_offset_of_U3CbuttonU3Ek__BackingField_22(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1669 = { sizeof (InputButton_t3704011348)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1669[4] = 
{
	InputButton_t3704011348::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1670 = { sizeof (FramePressState_t3039385657)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1670[5] = 
{
	FramePressState_t3039385657::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1671 = { sizeof (BaseInput_t3630163547), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1672 = { sizeof (BaseInputModule_t2019268878), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1672[6] = 
{
	BaseInputModule_t2019268878::get_offset_of_m_RaycastResultCache_2(),
	BaseInputModule_t2019268878::get_offset_of_m_AxisEventData_3(),
	BaseInputModule_t2019268878::get_offset_of_m_EventSystem_4(),
	BaseInputModule_t2019268878::get_offset_of_m_BaseEventData_5(),
	BaseInputModule_t2019268878::get_offset_of_m_InputOverride_6(),
	BaseInputModule_t2019268878::get_offset_of_m_DefaultInput_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1673 = { sizeof (PointerInputModule_t3453173740), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1673[6] = 
{
	0,
	0,
	0,
	0,
	PointerInputModule_t3453173740::get_offset_of_m_PointerData_12(),
	PointerInputModule_t3453173740::get_offset_of_m_MouseState_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1674 = { sizeof (ButtonState_t857139936), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1674[2] = 
{
	ButtonState_t857139936::get_offset_of_m_Button_0(),
	ButtonState_t857139936::get_offset_of_m_EventData_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1675 = { sizeof (MouseState_t384203932), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1675[1] = 
{
	MouseState_t384203932::get_offset_of_m_TrackedButtons_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1676 = { sizeof (MouseButtonEventData_t3190347560), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1676[2] = 
{
	MouseButtonEventData_t3190347560::get_offset_of_buttonState_0(),
	MouseButtonEventData_t3190347560::get_offset_of_buttonData_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1677 = { sizeof (StandaloneInputModule_t2760469101), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1677[13] = 
{
	StandaloneInputModule_t2760469101::get_offset_of_m_PrevActionTime_14(),
	StandaloneInputModule_t2760469101::get_offset_of_m_LastMoveVector_15(),
	StandaloneInputModule_t2760469101::get_offset_of_m_ConsecutiveMoveCount_16(),
	StandaloneInputModule_t2760469101::get_offset_of_m_LastMousePosition_17(),
	StandaloneInputModule_t2760469101::get_offset_of_m_MousePosition_18(),
	StandaloneInputModule_t2760469101::get_offset_of_m_CurrentFocusedGameObject_19(),
	StandaloneInputModule_t2760469101::get_offset_of_m_HorizontalAxis_20(),
	StandaloneInputModule_t2760469101::get_offset_of_m_VerticalAxis_21(),
	StandaloneInputModule_t2760469101::get_offset_of_m_SubmitButton_22(),
	StandaloneInputModule_t2760469101::get_offset_of_m_CancelButton_23(),
	StandaloneInputModule_t2760469101::get_offset_of_m_InputActionsPerSecond_24(),
	StandaloneInputModule_t2760469101::get_offset_of_m_RepeatDelay_25(),
	StandaloneInputModule_t2760469101::get_offset_of_m_ForceModuleActive_26(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1678 = { sizeof (InputMode_t3382566315)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1678[3] = 
{
	InputMode_t3382566315::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1679 = { sizeof (TouchInputModule_t4248229598), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1679[3] = 
{
	TouchInputModule_t4248229598::get_offset_of_m_LastMousePosition_14(),
	TouchInputModule_t4248229598::get_offset_of_m_MousePosition_15(),
	TouchInputModule_t4248229598::get_offset_of_m_ForceModuleActive_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1680 = { sizeof (BaseRaycaster_t4150874583), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1681 = { sizeof (Physics2DRaycaster_t3382992964), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1682 = { sizeof (PhysicsRaycaster_t437419520), -1, sizeof(PhysicsRaycaster_t437419520_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1682[4] = 
{
	0,
	PhysicsRaycaster_t437419520::get_offset_of_m_EventCamera_3(),
	PhysicsRaycaster_t437419520::get_offset_of_m_EventMask_4(),
	PhysicsRaycaster_t437419520_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1683 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1684 = { sizeof (ColorTween_t809614380)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1684[6] = 
{
	ColorTween_t809614380::get_offset_of_m_Target_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ColorTween_t809614380::get_offset_of_m_StartColor_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ColorTween_t809614380::get_offset_of_m_TargetColor_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ColorTween_t809614380::get_offset_of_m_TweenMode_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ColorTween_t809614380::get_offset_of_m_Duration_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ColorTween_t809614380::get_offset_of_m_IgnoreTimeScale_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1685 = { sizeof (ColorTweenMode_t1000778859)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1685[4] = 
{
	ColorTweenMode_t1000778859::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1686 = { sizeof (ColorTweenCallback_t1121741130), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1687 = { sizeof (FloatTween_t1274330004)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1687[5] = 
{
	FloatTween_t1274330004::get_offset_of_m_Target_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	FloatTween_t1274330004::get_offset_of_m_StartValue_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	FloatTween_t1274330004::get_offset_of_m_TargetValue_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	FloatTween_t1274330004::get_offset_of_m_Duration_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	FloatTween_t1274330004::get_offset_of_m_IgnoreTimeScale_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1688 = { sizeof (FloatTweenCallback_t1856710240), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1689 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1689[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1690 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1690[6] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1691 = { sizeof (AnimationTriggers_t2532145056), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1691[8] = 
{
	0,
	0,
	0,
	0,
	AnimationTriggers_t2532145056::get_offset_of_m_NormalTrigger_4(),
	AnimationTriggers_t2532145056::get_offset_of_m_HighlightedTrigger_5(),
	AnimationTriggers_t2532145056::get_offset_of_m_PressedTrigger_6(),
	AnimationTriggers_t2532145056::get_offset_of_m_DisabledTrigger_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1692 = { sizeof (Button_t4055032469), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1692[1] = 
{
	Button_t4055032469::get_offset_of_m_OnClick_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1693 = { sizeof (ButtonClickedEvent_t48803504), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1694 = { sizeof (U3COnFinishSubmitU3Ec__Iterator0_t3413438900), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1694[6] = 
{
	U3COnFinishSubmitU3Ec__Iterator0_t3413438900::get_offset_of_U3CfadeTimeU3E__0_0(),
	U3COnFinishSubmitU3Ec__Iterator0_t3413438900::get_offset_of_U3CelapsedTimeU3E__0_1(),
	U3COnFinishSubmitU3Ec__Iterator0_t3413438900::get_offset_of_U24this_2(),
	U3COnFinishSubmitU3Ec__Iterator0_t3413438900::get_offset_of_U24current_3(),
	U3COnFinishSubmitU3Ec__Iterator0_t3413438900::get_offset_of_U24disposing_4(),
	U3COnFinishSubmitU3Ec__Iterator0_t3413438900::get_offset_of_U24PC_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1695 = { sizeof (CanvasUpdate_t2572322932)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1695[7] = 
{
	CanvasUpdate_t2572322932::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1696 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1697 = { sizeof (CanvasUpdateRegistry_t2720824592), -1, sizeof(CanvasUpdateRegistry_t2720824592_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1697[7] = 
{
	CanvasUpdateRegistry_t2720824592_StaticFields::get_offset_of_s_Instance_0(),
	CanvasUpdateRegistry_t2720824592::get_offset_of_m_PerformingLayoutUpdate_1(),
	CanvasUpdateRegistry_t2720824592::get_offset_of_m_PerformingGraphicUpdate_2(),
	CanvasUpdateRegistry_t2720824592::get_offset_of_m_LayoutRebuildQueue_3(),
	CanvasUpdateRegistry_t2720824592::get_offset_of_m_GraphicRebuildQueue_4(),
	CanvasUpdateRegistry_t2720824592_StaticFields::get_offset_of_s_SortLayoutFunction_5(),
	CanvasUpdateRegistry_t2720824592_StaticFields::get_offset_of_U3CU3Ef__mgU24cache0_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1698 = { sizeof (ColorBlock_t2139031574)+ sizeof (Il2CppObject), sizeof(ColorBlock_t2139031574 ), 0, 0 };
extern const int32_t g_FieldOffsetTable1698[6] = 
{
	ColorBlock_t2139031574::get_offset_of_m_NormalColor_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ColorBlock_t2139031574::get_offset_of_m_HighlightedColor_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ColorBlock_t2139031574::get_offset_of_m_PressedColor_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ColorBlock_t2139031574::get_offset_of_m_DisabledColor_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ColorBlock_t2139031574::get_offset_of_m_ColorMultiplier_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ColorBlock_t2139031574::get_offset_of_m_FadeDuration_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1699 = { sizeof (DefaultControls_t4098465386), -1, sizeof(DefaultControls_t4098465386_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1699[9] = 
{
	0,
	0,
	0,
	DefaultControls_t4098465386_StaticFields::get_offset_of_s_ThickElementSize_3(),
	DefaultControls_t4098465386_StaticFields::get_offset_of_s_ThinElementSize_4(),
	DefaultControls_t4098465386_StaticFields::get_offset_of_s_ImageElementSize_5(),
	DefaultControls_t4098465386_StaticFields::get_offset_of_s_DefaultSelectableColor_6(),
	DefaultControls_t4098465386_StaticFields::get_offset_of_s_PanelColor_7(),
	DefaultControls_t4098465386_StaticFields::get_offset_of_s_TextColor_8(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
