﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour3962482529.h"

// System.String
struct String_t;
// UnityEngine.WWW
struct WWW_t3688466362;
// UnityEngine.Transform
struct Transform_t3600365921;
// StepNavigation
struct StepNavigation_t1887272446;
// MenuController
struct MenuController_t3930949237;
// UnityEngine.GameObject
struct GameObject_t1113636619;
// UnityEngine.UI.Image
struct Image_t2670269651;
// UnityEngine.UI.Text
struct Text_t1901882714;
// TMPro.TextMeshProUGUI
struct TextMeshProUGUI_t529313277;
// InstantTrackingController
struct InstantTrackingController_t1374048463;
// Wikitude.ImageTracker
struct ImageTracker_t271395264;
// Wikitude.ImageTrackable
struct ImageTrackable_t2462741734;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SceneBundleDownloader
struct  SceneBundleDownloader_t3242655479  : public MonoBehaviour_t3962482529
{
public:
	// System.String SceneBundleDownloader::url
	String_t* ___url_2;
	// System.String SceneBundleDownloader::prefabNameToLoad
	String_t* ___prefabNameToLoad_3;
	// UnityEngine.WWW SceneBundleDownloader::www
	WWW_t3688466362 * ___www_4;
	// UnityEngine.Transform SceneBundleDownloader::spawnPos
	Transform_t3600365921 * ___spawnPos_5;
	// System.Boolean SceneBundleDownloader::isAr
	bool ___isAr_6;
	// System.Boolean SceneBundleDownloader::isImageTracking
	bool ___isImageTracking_7;
	// StepNavigation SceneBundleDownloader::stepNavigation
	StepNavigation_t1887272446 * ___stepNavigation_8;
	// MenuController SceneBundleDownloader::menuController
	MenuController_t3930949237 * ___menuController_9;
	// UnityEngine.GameObject SceneBundleDownloader::initCanvas
	GameObject_t1113636619 * ___initCanvas_10;
	// UnityEngine.GameObject SceneBundleDownloader::guiCanvas
	GameObject_t1113636619 * ___guiCanvas_11;
	// UnityEngine.UI.Image SceneBundleDownloader::imageCompletion
	Image_t2670269651 * ___imageCompletion_12;
	// UnityEngine.UI.Text SceneBundleDownloader::textCompletion
	Text_t1901882714 * ___textCompletion_13;
	// UnityEngine.GameObject SceneBundleDownloader::arEntry
	GameObject_t1113636619 * ___arEntry_14;
	// TMPro.TextMeshProUGUI SceneBundleDownloader::Description
	TextMeshProUGUI_t529313277 * ___Description_15;
	// UnityEngine.GameObject SceneBundleDownloader::txtLoading
	GameObject_t1113636619 * ___txtLoading_16;
	// UnityEngine.GameObject SceneBundleDownloader::btnInitialize
	GameObject_t1113636619 * ___btnInitialize_17;
	// InstantTrackingController SceneBundleDownloader::instantTrackingController
	InstantTrackingController_t1374048463 * ___instantTrackingController_18;
	// UnityEngine.GameObject SceneBundleDownloader::gameObjectToLoad
	GameObject_t1113636619 * ___gameObjectToLoad_19;
	// Wikitude.ImageTracker SceneBundleDownloader::imageTracker
	ImageTracker_t271395264 * ___imageTracker_20;
	// Wikitude.ImageTrackable SceneBundleDownloader::imageTrackable
	ImageTrackable_t2462741734 * ___imageTrackable_21;

public:
	inline static int32_t get_offset_of_url_2() { return static_cast<int32_t>(offsetof(SceneBundleDownloader_t3242655479, ___url_2)); }
	inline String_t* get_url_2() const { return ___url_2; }
	inline String_t** get_address_of_url_2() { return &___url_2; }
	inline void set_url_2(String_t* value)
	{
		___url_2 = value;
		Il2CppCodeGenWriteBarrier(&___url_2, value);
	}

	inline static int32_t get_offset_of_prefabNameToLoad_3() { return static_cast<int32_t>(offsetof(SceneBundleDownloader_t3242655479, ___prefabNameToLoad_3)); }
	inline String_t* get_prefabNameToLoad_3() const { return ___prefabNameToLoad_3; }
	inline String_t** get_address_of_prefabNameToLoad_3() { return &___prefabNameToLoad_3; }
	inline void set_prefabNameToLoad_3(String_t* value)
	{
		___prefabNameToLoad_3 = value;
		Il2CppCodeGenWriteBarrier(&___prefabNameToLoad_3, value);
	}

	inline static int32_t get_offset_of_www_4() { return static_cast<int32_t>(offsetof(SceneBundleDownloader_t3242655479, ___www_4)); }
	inline WWW_t3688466362 * get_www_4() const { return ___www_4; }
	inline WWW_t3688466362 ** get_address_of_www_4() { return &___www_4; }
	inline void set_www_4(WWW_t3688466362 * value)
	{
		___www_4 = value;
		Il2CppCodeGenWriteBarrier(&___www_4, value);
	}

	inline static int32_t get_offset_of_spawnPos_5() { return static_cast<int32_t>(offsetof(SceneBundleDownloader_t3242655479, ___spawnPos_5)); }
	inline Transform_t3600365921 * get_spawnPos_5() const { return ___spawnPos_5; }
	inline Transform_t3600365921 ** get_address_of_spawnPos_5() { return &___spawnPos_5; }
	inline void set_spawnPos_5(Transform_t3600365921 * value)
	{
		___spawnPos_5 = value;
		Il2CppCodeGenWriteBarrier(&___spawnPos_5, value);
	}

	inline static int32_t get_offset_of_isAr_6() { return static_cast<int32_t>(offsetof(SceneBundleDownloader_t3242655479, ___isAr_6)); }
	inline bool get_isAr_6() const { return ___isAr_6; }
	inline bool* get_address_of_isAr_6() { return &___isAr_6; }
	inline void set_isAr_6(bool value)
	{
		___isAr_6 = value;
	}

	inline static int32_t get_offset_of_isImageTracking_7() { return static_cast<int32_t>(offsetof(SceneBundleDownloader_t3242655479, ___isImageTracking_7)); }
	inline bool get_isImageTracking_7() const { return ___isImageTracking_7; }
	inline bool* get_address_of_isImageTracking_7() { return &___isImageTracking_7; }
	inline void set_isImageTracking_7(bool value)
	{
		___isImageTracking_7 = value;
	}

	inline static int32_t get_offset_of_stepNavigation_8() { return static_cast<int32_t>(offsetof(SceneBundleDownloader_t3242655479, ___stepNavigation_8)); }
	inline StepNavigation_t1887272446 * get_stepNavigation_8() const { return ___stepNavigation_8; }
	inline StepNavigation_t1887272446 ** get_address_of_stepNavigation_8() { return &___stepNavigation_8; }
	inline void set_stepNavigation_8(StepNavigation_t1887272446 * value)
	{
		___stepNavigation_8 = value;
		Il2CppCodeGenWriteBarrier(&___stepNavigation_8, value);
	}

	inline static int32_t get_offset_of_menuController_9() { return static_cast<int32_t>(offsetof(SceneBundleDownloader_t3242655479, ___menuController_9)); }
	inline MenuController_t3930949237 * get_menuController_9() const { return ___menuController_9; }
	inline MenuController_t3930949237 ** get_address_of_menuController_9() { return &___menuController_9; }
	inline void set_menuController_9(MenuController_t3930949237 * value)
	{
		___menuController_9 = value;
		Il2CppCodeGenWriteBarrier(&___menuController_9, value);
	}

	inline static int32_t get_offset_of_initCanvas_10() { return static_cast<int32_t>(offsetof(SceneBundleDownloader_t3242655479, ___initCanvas_10)); }
	inline GameObject_t1113636619 * get_initCanvas_10() const { return ___initCanvas_10; }
	inline GameObject_t1113636619 ** get_address_of_initCanvas_10() { return &___initCanvas_10; }
	inline void set_initCanvas_10(GameObject_t1113636619 * value)
	{
		___initCanvas_10 = value;
		Il2CppCodeGenWriteBarrier(&___initCanvas_10, value);
	}

	inline static int32_t get_offset_of_guiCanvas_11() { return static_cast<int32_t>(offsetof(SceneBundleDownloader_t3242655479, ___guiCanvas_11)); }
	inline GameObject_t1113636619 * get_guiCanvas_11() const { return ___guiCanvas_11; }
	inline GameObject_t1113636619 ** get_address_of_guiCanvas_11() { return &___guiCanvas_11; }
	inline void set_guiCanvas_11(GameObject_t1113636619 * value)
	{
		___guiCanvas_11 = value;
		Il2CppCodeGenWriteBarrier(&___guiCanvas_11, value);
	}

	inline static int32_t get_offset_of_imageCompletion_12() { return static_cast<int32_t>(offsetof(SceneBundleDownloader_t3242655479, ___imageCompletion_12)); }
	inline Image_t2670269651 * get_imageCompletion_12() const { return ___imageCompletion_12; }
	inline Image_t2670269651 ** get_address_of_imageCompletion_12() { return &___imageCompletion_12; }
	inline void set_imageCompletion_12(Image_t2670269651 * value)
	{
		___imageCompletion_12 = value;
		Il2CppCodeGenWriteBarrier(&___imageCompletion_12, value);
	}

	inline static int32_t get_offset_of_textCompletion_13() { return static_cast<int32_t>(offsetof(SceneBundleDownloader_t3242655479, ___textCompletion_13)); }
	inline Text_t1901882714 * get_textCompletion_13() const { return ___textCompletion_13; }
	inline Text_t1901882714 ** get_address_of_textCompletion_13() { return &___textCompletion_13; }
	inline void set_textCompletion_13(Text_t1901882714 * value)
	{
		___textCompletion_13 = value;
		Il2CppCodeGenWriteBarrier(&___textCompletion_13, value);
	}

	inline static int32_t get_offset_of_arEntry_14() { return static_cast<int32_t>(offsetof(SceneBundleDownloader_t3242655479, ___arEntry_14)); }
	inline GameObject_t1113636619 * get_arEntry_14() const { return ___arEntry_14; }
	inline GameObject_t1113636619 ** get_address_of_arEntry_14() { return &___arEntry_14; }
	inline void set_arEntry_14(GameObject_t1113636619 * value)
	{
		___arEntry_14 = value;
		Il2CppCodeGenWriteBarrier(&___arEntry_14, value);
	}

	inline static int32_t get_offset_of_Description_15() { return static_cast<int32_t>(offsetof(SceneBundleDownloader_t3242655479, ___Description_15)); }
	inline TextMeshProUGUI_t529313277 * get_Description_15() const { return ___Description_15; }
	inline TextMeshProUGUI_t529313277 ** get_address_of_Description_15() { return &___Description_15; }
	inline void set_Description_15(TextMeshProUGUI_t529313277 * value)
	{
		___Description_15 = value;
		Il2CppCodeGenWriteBarrier(&___Description_15, value);
	}

	inline static int32_t get_offset_of_txtLoading_16() { return static_cast<int32_t>(offsetof(SceneBundleDownloader_t3242655479, ___txtLoading_16)); }
	inline GameObject_t1113636619 * get_txtLoading_16() const { return ___txtLoading_16; }
	inline GameObject_t1113636619 ** get_address_of_txtLoading_16() { return &___txtLoading_16; }
	inline void set_txtLoading_16(GameObject_t1113636619 * value)
	{
		___txtLoading_16 = value;
		Il2CppCodeGenWriteBarrier(&___txtLoading_16, value);
	}

	inline static int32_t get_offset_of_btnInitialize_17() { return static_cast<int32_t>(offsetof(SceneBundleDownloader_t3242655479, ___btnInitialize_17)); }
	inline GameObject_t1113636619 * get_btnInitialize_17() const { return ___btnInitialize_17; }
	inline GameObject_t1113636619 ** get_address_of_btnInitialize_17() { return &___btnInitialize_17; }
	inline void set_btnInitialize_17(GameObject_t1113636619 * value)
	{
		___btnInitialize_17 = value;
		Il2CppCodeGenWriteBarrier(&___btnInitialize_17, value);
	}

	inline static int32_t get_offset_of_instantTrackingController_18() { return static_cast<int32_t>(offsetof(SceneBundleDownloader_t3242655479, ___instantTrackingController_18)); }
	inline InstantTrackingController_t1374048463 * get_instantTrackingController_18() const { return ___instantTrackingController_18; }
	inline InstantTrackingController_t1374048463 ** get_address_of_instantTrackingController_18() { return &___instantTrackingController_18; }
	inline void set_instantTrackingController_18(InstantTrackingController_t1374048463 * value)
	{
		___instantTrackingController_18 = value;
		Il2CppCodeGenWriteBarrier(&___instantTrackingController_18, value);
	}

	inline static int32_t get_offset_of_gameObjectToLoad_19() { return static_cast<int32_t>(offsetof(SceneBundleDownloader_t3242655479, ___gameObjectToLoad_19)); }
	inline GameObject_t1113636619 * get_gameObjectToLoad_19() const { return ___gameObjectToLoad_19; }
	inline GameObject_t1113636619 ** get_address_of_gameObjectToLoad_19() { return &___gameObjectToLoad_19; }
	inline void set_gameObjectToLoad_19(GameObject_t1113636619 * value)
	{
		___gameObjectToLoad_19 = value;
		Il2CppCodeGenWriteBarrier(&___gameObjectToLoad_19, value);
	}

	inline static int32_t get_offset_of_imageTracker_20() { return static_cast<int32_t>(offsetof(SceneBundleDownloader_t3242655479, ___imageTracker_20)); }
	inline ImageTracker_t271395264 * get_imageTracker_20() const { return ___imageTracker_20; }
	inline ImageTracker_t271395264 ** get_address_of_imageTracker_20() { return &___imageTracker_20; }
	inline void set_imageTracker_20(ImageTracker_t271395264 * value)
	{
		___imageTracker_20 = value;
		Il2CppCodeGenWriteBarrier(&___imageTracker_20, value);
	}

	inline static int32_t get_offset_of_imageTrackable_21() { return static_cast<int32_t>(offsetof(SceneBundleDownloader_t3242655479, ___imageTrackable_21)); }
	inline ImageTrackable_t2462741734 * get_imageTrackable_21() const { return ___imageTrackable_21; }
	inline ImageTrackable_t2462741734 ** get_address_of_imageTrackable_21() { return &___imageTrackable_21; }
	inline void set_imageTrackable_21(ImageTrackable_t2462741734 * value)
	{
		___imageTrackable_21 = value;
		Il2CppCodeGenWriteBarrier(&___imageTrackable_21, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
