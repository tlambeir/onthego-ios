﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour3962482529.h"

// System.Collections.Generic.List`1<System.String>
struct List_1_t3319525431;
// UnityEngine.Animator
struct Animator_t434523843;
// UnityEngine.GameObject
struct GameObject_t1113636619;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// StepNavigation
struct  StepNavigation_t1887272446  : public MonoBehaviour_t3962482529
{
public:
	// System.Collections.Generic.List`1<System.String> StepNavigation::triggers
	List_1_t3319525431 * ___triggers_2;
	// UnityEngine.Animator StepNavigation::animator
	Animator_t434523843 * ___animator_3;
	// System.Int32 StepNavigation::currentIndex
	int32_t ___currentIndex_4;
	// UnityEngine.GameObject StepNavigation::prevButton
	GameObject_t1113636619 * ___prevButton_5;
	// UnityEngine.GameObject StepNavigation::nextButton
	GameObject_t1113636619 * ___nextButton_6;

public:
	inline static int32_t get_offset_of_triggers_2() { return static_cast<int32_t>(offsetof(StepNavigation_t1887272446, ___triggers_2)); }
	inline List_1_t3319525431 * get_triggers_2() const { return ___triggers_2; }
	inline List_1_t3319525431 ** get_address_of_triggers_2() { return &___triggers_2; }
	inline void set_triggers_2(List_1_t3319525431 * value)
	{
		___triggers_2 = value;
		Il2CppCodeGenWriteBarrier(&___triggers_2, value);
	}

	inline static int32_t get_offset_of_animator_3() { return static_cast<int32_t>(offsetof(StepNavigation_t1887272446, ___animator_3)); }
	inline Animator_t434523843 * get_animator_3() const { return ___animator_3; }
	inline Animator_t434523843 ** get_address_of_animator_3() { return &___animator_3; }
	inline void set_animator_3(Animator_t434523843 * value)
	{
		___animator_3 = value;
		Il2CppCodeGenWriteBarrier(&___animator_3, value);
	}

	inline static int32_t get_offset_of_currentIndex_4() { return static_cast<int32_t>(offsetof(StepNavigation_t1887272446, ___currentIndex_4)); }
	inline int32_t get_currentIndex_4() const { return ___currentIndex_4; }
	inline int32_t* get_address_of_currentIndex_4() { return &___currentIndex_4; }
	inline void set_currentIndex_4(int32_t value)
	{
		___currentIndex_4 = value;
	}

	inline static int32_t get_offset_of_prevButton_5() { return static_cast<int32_t>(offsetof(StepNavigation_t1887272446, ___prevButton_5)); }
	inline GameObject_t1113636619 * get_prevButton_5() const { return ___prevButton_5; }
	inline GameObject_t1113636619 ** get_address_of_prevButton_5() { return &___prevButton_5; }
	inline void set_prevButton_5(GameObject_t1113636619 * value)
	{
		___prevButton_5 = value;
		Il2CppCodeGenWriteBarrier(&___prevButton_5, value);
	}

	inline static int32_t get_offset_of_nextButton_6() { return static_cast<int32_t>(offsetof(StepNavigation_t1887272446, ___nextButton_6)); }
	inline GameObject_t1113636619 * get_nextButton_6() const { return ___nextButton_6; }
	inline GameObject_t1113636619 ** get_address_of_nextButton_6() { return &___nextButton_6; }
	inline void set_nextButton_6(GameObject_t1113636619 * value)
	{
		___nextButton_6 = value;
		Il2CppCodeGenWriteBarrier(&___nextButton_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
