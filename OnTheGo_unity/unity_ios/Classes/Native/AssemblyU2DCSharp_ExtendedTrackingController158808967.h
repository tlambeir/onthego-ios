﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_SampleController2879308770.h"

// Wikitude.ImageTracker
struct ImageTracker_t271395264;
// UnityEngine.UI.Text
struct Text_t1901882714;
// UnityEngine.UI.Image
struct Image_t2670269651;
// UnityEngine.GameObject
struct GameObject_t1113636619;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ExtendedTrackingController
struct  ExtendedTrackingController_t158808967  : public SampleController_t2879308770
{
public:
	// Wikitude.ImageTracker ExtendedTrackingController::Tracker
	ImageTracker_t271395264 * ___Tracker_2;
	// UnityEngine.UI.Text ExtendedTrackingController::TrackingQualityText
	Text_t1901882714 * ___TrackingQualityText_3;
	// UnityEngine.UI.Image ExtendedTrackingController::TrackingQualityBackground
	Image_t2670269651 * ___TrackingQualityBackground_4;
	// UnityEngine.GameObject ExtendedTrackingController::StopExtendedTrackingButton
	GameObject_t1113636619 * ___StopExtendedTrackingButton_5;

public:
	inline static int32_t get_offset_of_Tracker_2() { return static_cast<int32_t>(offsetof(ExtendedTrackingController_t158808967, ___Tracker_2)); }
	inline ImageTracker_t271395264 * get_Tracker_2() const { return ___Tracker_2; }
	inline ImageTracker_t271395264 ** get_address_of_Tracker_2() { return &___Tracker_2; }
	inline void set_Tracker_2(ImageTracker_t271395264 * value)
	{
		___Tracker_2 = value;
		Il2CppCodeGenWriteBarrier(&___Tracker_2, value);
	}

	inline static int32_t get_offset_of_TrackingQualityText_3() { return static_cast<int32_t>(offsetof(ExtendedTrackingController_t158808967, ___TrackingQualityText_3)); }
	inline Text_t1901882714 * get_TrackingQualityText_3() const { return ___TrackingQualityText_3; }
	inline Text_t1901882714 ** get_address_of_TrackingQualityText_3() { return &___TrackingQualityText_3; }
	inline void set_TrackingQualityText_3(Text_t1901882714 * value)
	{
		___TrackingQualityText_3 = value;
		Il2CppCodeGenWriteBarrier(&___TrackingQualityText_3, value);
	}

	inline static int32_t get_offset_of_TrackingQualityBackground_4() { return static_cast<int32_t>(offsetof(ExtendedTrackingController_t158808967, ___TrackingQualityBackground_4)); }
	inline Image_t2670269651 * get_TrackingQualityBackground_4() const { return ___TrackingQualityBackground_4; }
	inline Image_t2670269651 ** get_address_of_TrackingQualityBackground_4() { return &___TrackingQualityBackground_4; }
	inline void set_TrackingQualityBackground_4(Image_t2670269651 * value)
	{
		___TrackingQualityBackground_4 = value;
		Il2CppCodeGenWriteBarrier(&___TrackingQualityBackground_4, value);
	}

	inline static int32_t get_offset_of_StopExtendedTrackingButton_5() { return static_cast<int32_t>(offsetof(ExtendedTrackingController_t158808967, ___StopExtendedTrackingButton_5)); }
	inline GameObject_t1113636619 * get_StopExtendedTrackingButton_5() const { return ___StopExtendedTrackingButton_5; }
	inline GameObject_t1113636619 ** get_address_of_StopExtendedTrackingButton_5() { return &___StopExtendedTrackingButton_5; }
	inline void set_StopExtendedTrackingButton_5(GameObject_t1113636619 * value)
	{
		___StopExtendedTrackingButton_5 = value;
		Il2CppCodeGenWriteBarrier(&___StopExtendedTrackingButton_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
