﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour3962482529.h"

// UnityEngine.GameObject
struct GameObject_t1113636619;
// UnityEngine.Sprite
struct Sprite_t280657092;
// UnityEngine.Animator
struct Animator_t434523843;
// UnityEngine.UI.Text
struct Text_t1901882714;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LeftNav
struct  LeftNav_t2375583198  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject LeftNav::panel
	GameObject_t1113636619 * ___panel_2;
	// UnityEngine.Sprite LeftNav::backSprite
	Sprite_t280657092 * ___backSprite_3;
	// UnityEngine.Sprite LeftNav::burgerSprite
	Sprite_t280657092 * ___burgerSprite_4;
	// UnityEngine.GameObject LeftNav::button
	GameObject_t1113636619 * ___button_5;
	// UnityEngine.GameObject LeftNav::description
	GameObject_t1113636619 * ___description_6;
	// UnityEngine.Animator LeftNav::anim
	Animator_t434523843 * ___anim_7;
	// System.Boolean LeftNav::isPaused
	bool ___isPaused_8;
	// UnityEngine.UI.Text LeftNav::backtotoptext
	Text_t1901882714 * ___backtotoptext_9;

public:
	inline static int32_t get_offset_of_panel_2() { return static_cast<int32_t>(offsetof(LeftNav_t2375583198, ___panel_2)); }
	inline GameObject_t1113636619 * get_panel_2() const { return ___panel_2; }
	inline GameObject_t1113636619 ** get_address_of_panel_2() { return &___panel_2; }
	inline void set_panel_2(GameObject_t1113636619 * value)
	{
		___panel_2 = value;
		Il2CppCodeGenWriteBarrier(&___panel_2, value);
	}

	inline static int32_t get_offset_of_backSprite_3() { return static_cast<int32_t>(offsetof(LeftNav_t2375583198, ___backSprite_3)); }
	inline Sprite_t280657092 * get_backSprite_3() const { return ___backSprite_3; }
	inline Sprite_t280657092 ** get_address_of_backSprite_3() { return &___backSprite_3; }
	inline void set_backSprite_3(Sprite_t280657092 * value)
	{
		___backSprite_3 = value;
		Il2CppCodeGenWriteBarrier(&___backSprite_3, value);
	}

	inline static int32_t get_offset_of_burgerSprite_4() { return static_cast<int32_t>(offsetof(LeftNav_t2375583198, ___burgerSprite_4)); }
	inline Sprite_t280657092 * get_burgerSprite_4() const { return ___burgerSprite_4; }
	inline Sprite_t280657092 ** get_address_of_burgerSprite_4() { return &___burgerSprite_4; }
	inline void set_burgerSprite_4(Sprite_t280657092 * value)
	{
		___burgerSprite_4 = value;
		Il2CppCodeGenWriteBarrier(&___burgerSprite_4, value);
	}

	inline static int32_t get_offset_of_button_5() { return static_cast<int32_t>(offsetof(LeftNav_t2375583198, ___button_5)); }
	inline GameObject_t1113636619 * get_button_5() const { return ___button_5; }
	inline GameObject_t1113636619 ** get_address_of_button_5() { return &___button_5; }
	inline void set_button_5(GameObject_t1113636619 * value)
	{
		___button_5 = value;
		Il2CppCodeGenWriteBarrier(&___button_5, value);
	}

	inline static int32_t get_offset_of_description_6() { return static_cast<int32_t>(offsetof(LeftNav_t2375583198, ___description_6)); }
	inline GameObject_t1113636619 * get_description_6() const { return ___description_6; }
	inline GameObject_t1113636619 ** get_address_of_description_6() { return &___description_6; }
	inline void set_description_6(GameObject_t1113636619 * value)
	{
		___description_6 = value;
		Il2CppCodeGenWriteBarrier(&___description_6, value);
	}

	inline static int32_t get_offset_of_anim_7() { return static_cast<int32_t>(offsetof(LeftNav_t2375583198, ___anim_7)); }
	inline Animator_t434523843 * get_anim_7() const { return ___anim_7; }
	inline Animator_t434523843 ** get_address_of_anim_7() { return &___anim_7; }
	inline void set_anim_7(Animator_t434523843 * value)
	{
		___anim_7 = value;
		Il2CppCodeGenWriteBarrier(&___anim_7, value);
	}

	inline static int32_t get_offset_of_isPaused_8() { return static_cast<int32_t>(offsetof(LeftNav_t2375583198, ___isPaused_8)); }
	inline bool get_isPaused_8() const { return ___isPaused_8; }
	inline bool* get_address_of_isPaused_8() { return &___isPaused_8; }
	inline void set_isPaused_8(bool value)
	{
		___isPaused_8 = value;
	}

	inline static int32_t get_offset_of_backtotoptext_9() { return static_cast<int32_t>(offsetof(LeftNav_t2375583198, ___backtotoptext_9)); }
	inline Text_t1901882714 * get_backtotoptext_9() const { return ___backtotoptext_9; }
	inline Text_t1901882714 ** get_address_of_backtotoptext_9() { return &___backtotoptext_9; }
	inline void set_backtotoptext_9(Text_t1901882714 * value)
	{
		___backtotoptext_9 = value;
		Il2CppCodeGenWriteBarrier(&___backtotoptext_9, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
