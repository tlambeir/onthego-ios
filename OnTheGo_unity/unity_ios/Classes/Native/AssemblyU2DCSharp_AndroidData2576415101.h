﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AndroidData
struct  AndroidData_t2576415101  : public Il2CppObject
{
public:
	// System.Int32 AndroidData::id
	int32_t ___id_0;
	// System.String AndroidData::url
	String_t* ___url_1;
	// System.String AndroidData::model
	String_t* ___model_2;
	// System.String AndroidData::scene
	String_t* ___scene_3;
	// System.String AndroidData::thumb
	String_t* ___thumb_4;
	// System.String AndroidData::title
	String_t* ___title_5;
	// System.String AndroidData::description
	String_t* ___description_6;
	// System.String AndroidData::structure
	String_t* ___structure_7;

public:
	inline static int32_t get_offset_of_id_0() { return static_cast<int32_t>(offsetof(AndroidData_t2576415101, ___id_0)); }
	inline int32_t get_id_0() const { return ___id_0; }
	inline int32_t* get_address_of_id_0() { return &___id_0; }
	inline void set_id_0(int32_t value)
	{
		___id_0 = value;
	}

	inline static int32_t get_offset_of_url_1() { return static_cast<int32_t>(offsetof(AndroidData_t2576415101, ___url_1)); }
	inline String_t* get_url_1() const { return ___url_1; }
	inline String_t** get_address_of_url_1() { return &___url_1; }
	inline void set_url_1(String_t* value)
	{
		___url_1 = value;
		Il2CppCodeGenWriteBarrier(&___url_1, value);
	}

	inline static int32_t get_offset_of_model_2() { return static_cast<int32_t>(offsetof(AndroidData_t2576415101, ___model_2)); }
	inline String_t* get_model_2() const { return ___model_2; }
	inline String_t** get_address_of_model_2() { return &___model_2; }
	inline void set_model_2(String_t* value)
	{
		___model_2 = value;
		Il2CppCodeGenWriteBarrier(&___model_2, value);
	}

	inline static int32_t get_offset_of_scene_3() { return static_cast<int32_t>(offsetof(AndroidData_t2576415101, ___scene_3)); }
	inline String_t* get_scene_3() const { return ___scene_3; }
	inline String_t** get_address_of_scene_3() { return &___scene_3; }
	inline void set_scene_3(String_t* value)
	{
		___scene_3 = value;
		Il2CppCodeGenWriteBarrier(&___scene_3, value);
	}

	inline static int32_t get_offset_of_thumb_4() { return static_cast<int32_t>(offsetof(AndroidData_t2576415101, ___thumb_4)); }
	inline String_t* get_thumb_4() const { return ___thumb_4; }
	inline String_t** get_address_of_thumb_4() { return &___thumb_4; }
	inline void set_thumb_4(String_t* value)
	{
		___thumb_4 = value;
		Il2CppCodeGenWriteBarrier(&___thumb_4, value);
	}

	inline static int32_t get_offset_of_title_5() { return static_cast<int32_t>(offsetof(AndroidData_t2576415101, ___title_5)); }
	inline String_t* get_title_5() const { return ___title_5; }
	inline String_t** get_address_of_title_5() { return &___title_5; }
	inline void set_title_5(String_t* value)
	{
		___title_5 = value;
		Il2CppCodeGenWriteBarrier(&___title_5, value);
	}

	inline static int32_t get_offset_of_description_6() { return static_cast<int32_t>(offsetof(AndroidData_t2576415101, ___description_6)); }
	inline String_t* get_description_6() const { return ___description_6; }
	inline String_t** get_address_of_description_6() { return &___description_6; }
	inline void set_description_6(String_t* value)
	{
		___description_6 = value;
		Il2CppCodeGenWriteBarrier(&___description_6, value);
	}

	inline static int32_t get_offset_of_structure_7() { return static_cast<int32_t>(offsetof(AndroidData_t2576415101, ___structure_7)); }
	inline String_t* get_structure_7() const { return ___structure_7; }
	inline String_t** get_address_of_structure_7() { return &___structure_7; }
	inline void set_structure_7(String_t* value)
	{
		___structure_7 = value;
		Il2CppCodeGenWriteBarrier(&___structure_7, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
