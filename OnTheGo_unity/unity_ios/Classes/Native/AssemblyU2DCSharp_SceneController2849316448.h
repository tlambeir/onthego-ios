﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour3962482529.h"

// UnityEngine.GameObject
struct GameObject_t1113636619;
// System.String
struct String_t;
// UnityEngine.UI.Text
struct Text_t1901882714;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SceneController
struct  SceneController_t2849316448  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject SceneController::loadingCanvas
	GameObject_t1113636619 * ___loadingCanvas_2;
	// System.String SceneController::scene
	String_t* ___scene_3;
	// UnityEngine.UI.Text SceneController::txtScene
	Text_t1901882714 * ___txtScene_4;

public:
	inline static int32_t get_offset_of_loadingCanvas_2() { return static_cast<int32_t>(offsetof(SceneController_t2849316448, ___loadingCanvas_2)); }
	inline GameObject_t1113636619 * get_loadingCanvas_2() const { return ___loadingCanvas_2; }
	inline GameObject_t1113636619 ** get_address_of_loadingCanvas_2() { return &___loadingCanvas_2; }
	inline void set_loadingCanvas_2(GameObject_t1113636619 * value)
	{
		___loadingCanvas_2 = value;
		Il2CppCodeGenWriteBarrier(&___loadingCanvas_2, value);
	}

	inline static int32_t get_offset_of_scene_3() { return static_cast<int32_t>(offsetof(SceneController_t2849316448, ___scene_3)); }
	inline String_t* get_scene_3() const { return ___scene_3; }
	inline String_t** get_address_of_scene_3() { return &___scene_3; }
	inline void set_scene_3(String_t* value)
	{
		___scene_3 = value;
		Il2CppCodeGenWriteBarrier(&___scene_3, value);
	}

	inline static int32_t get_offset_of_txtScene_4() { return static_cast<int32_t>(offsetof(SceneController_t2849316448, ___txtScene_4)); }
	inline Text_t1901882714 * get_txtScene_4() const { return ___txtScene_4; }
	inline Text_t1901882714 ** get_address_of_txtScene_4() { return &___txtScene_4; }
	inline void set_txtScene_4(Text_t1901882714 * value)
	{
		___txtScene_4 = value;
		Il2CppCodeGenWriteBarrier(&___txtScene_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
