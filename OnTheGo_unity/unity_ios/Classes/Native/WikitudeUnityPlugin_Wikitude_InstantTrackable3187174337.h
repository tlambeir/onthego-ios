﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "WikitudeUnityPlugin_Wikitude_Trackable347424808.h"

// Wikitude.InstantTrackable/OnInitializationStartedEvent
struct OnInitializationStartedEvent_t272217273;
// Wikitude.InstantTrackable/OnInitializationStoppedEvent
struct OnInitializationStoppedEvent_t4207911327;
// Wikitude.InstantTrackable/OnSceneRecognizedEvent
struct OnSceneRecognizedEvent_t2416994635;
// Wikitude.InstantTrackable/OnSceneLostEvent
struct OnSceneLostEvent_t389101256;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Wikitude.InstantTrackable
struct  InstantTrackable_t3187174337  : public Trackable_t347424808
{
public:
	// Wikitude.InstantTrackable/OnInitializationStartedEvent Wikitude.InstantTrackable::OnInitializationStarted
	OnInitializationStartedEvent_t272217273 * ___OnInitializationStarted_9;
	// Wikitude.InstantTrackable/OnInitializationStoppedEvent Wikitude.InstantTrackable::OnInitializationStopped
	OnInitializationStoppedEvent_t4207911327 * ___OnInitializationStopped_10;
	// Wikitude.InstantTrackable/OnSceneRecognizedEvent Wikitude.InstantTrackable::OnSceneRecognized
	OnSceneRecognizedEvent_t2416994635 * ___OnSceneRecognized_11;
	// Wikitude.InstantTrackable/OnSceneLostEvent Wikitude.InstantTrackable::OnSceneLost
	OnSceneLostEvent_t389101256 * ___OnSceneLost_12;

public:
	inline static int32_t get_offset_of_OnInitializationStarted_9() { return static_cast<int32_t>(offsetof(InstantTrackable_t3187174337, ___OnInitializationStarted_9)); }
	inline OnInitializationStartedEvent_t272217273 * get_OnInitializationStarted_9() const { return ___OnInitializationStarted_9; }
	inline OnInitializationStartedEvent_t272217273 ** get_address_of_OnInitializationStarted_9() { return &___OnInitializationStarted_9; }
	inline void set_OnInitializationStarted_9(OnInitializationStartedEvent_t272217273 * value)
	{
		___OnInitializationStarted_9 = value;
		Il2CppCodeGenWriteBarrier(&___OnInitializationStarted_9, value);
	}

	inline static int32_t get_offset_of_OnInitializationStopped_10() { return static_cast<int32_t>(offsetof(InstantTrackable_t3187174337, ___OnInitializationStopped_10)); }
	inline OnInitializationStoppedEvent_t4207911327 * get_OnInitializationStopped_10() const { return ___OnInitializationStopped_10; }
	inline OnInitializationStoppedEvent_t4207911327 ** get_address_of_OnInitializationStopped_10() { return &___OnInitializationStopped_10; }
	inline void set_OnInitializationStopped_10(OnInitializationStoppedEvent_t4207911327 * value)
	{
		___OnInitializationStopped_10 = value;
		Il2CppCodeGenWriteBarrier(&___OnInitializationStopped_10, value);
	}

	inline static int32_t get_offset_of_OnSceneRecognized_11() { return static_cast<int32_t>(offsetof(InstantTrackable_t3187174337, ___OnSceneRecognized_11)); }
	inline OnSceneRecognizedEvent_t2416994635 * get_OnSceneRecognized_11() const { return ___OnSceneRecognized_11; }
	inline OnSceneRecognizedEvent_t2416994635 ** get_address_of_OnSceneRecognized_11() { return &___OnSceneRecognized_11; }
	inline void set_OnSceneRecognized_11(OnSceneRecognizedEvent_t2416994635 * value)
	{
		___OnSceneRecognized_11 = value;
		Il2CppCodeGenWriteBarrier(&___OnSceneRecognized_11, value);
	}

	inline static int32_t get_offset_of_OnSceneLost_12() { return static_cast<int32_t>(offsetof(InstantTrackable_t3187174337, ___OnSceneLost_12)); }
	inline OnSceneLostEvent_t389101256 * get_OnSceneLost_12() const { return ___OnSceneLost_12; }
	inline OnSceneLostEvent_t389101256 ** get_address_of_OnSceneLost_12() { return &___OnSceneLost_12; }
	inline void set_OnSceneLost_12(OnSceneLostEvent_t389101256 * value)
	{
		___OnSceneLost_12 = value;
		Il2CppCodeGenWriteBarrier(&___OnSceneLost_12, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
