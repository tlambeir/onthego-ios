﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_SampleController2879308770.h"
#include "WikitudeUnityPlugin_Wikitude_InstantTrackingState3973410783.h"

// Wikitude.InstantTracker
struct InstantTracker_t684352120;
// UnityEngine.GameObject
struct GameObject_t1113636619;
// UnityEngine.UI.Button
struct Button_t4055032469;
// UnityEngine.UI.Text
struct Text_t1901882714;
// GridRenderer
struct GridRenderer_t1964421403;
// System.Collections.Generic.List`1<UnityEngine.GameObject>
struct List_1_t2585711361;
// Wikitude.InstantTrackable
struct InstantTrackable_t3187174337;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ScenePickingController
struct  ScenePickingController_t1946337528  : public SampleController_t2879308770
{
public:
	// Wikitude.InstantTracker ScenePickingController::Tracker
	InstantTracker_t684352120 * ___Tracker_2;
	// UnityEngine.GameObject ScenePickingController::Augmentation
	GameObject_t1113636619 * ___Augmentation_3;
	// UnityEngine.UI.Button ScenePickingController::ToggleStateButton
	Button_t4055032469 * ___ToggleStateButton_4;
	// UnityEngine.UI.Text ScenePickingController::ToggleStateButtonText
	Text_t1901882714 * ___ToggleStateButtonText_5;
	// Wikitude.InstantTrackingState ScenePickingController::_currentTrackerState
	int32_t ____currentTrackerState_6;
	// System.Boolean ScenePickingController::_changingState
	bool ____changingState_7;
	// GridRenderer ScenePickingController::_gridRenderer
	GridRenderer_t1964421403 * ____gridRenderer_8;
	// System.Collections.Generic.List`1<UnityEngine.GameObject> ScenePickingController::_augmentations
	List_1_t2585711361 * ____augmentations_9;
	// Wikitude.InstantTrackable ScenePickingController::_trackable
	InstantTrackable_t3187174337 * ____trackable_10;
	// System.Boolean ScenePickingController::_isTracking
	bool ____isTracking_11;

public:
	inline static int32_t get_offset_of_Tracker_2() { return static_cast<int32_t>(offsetof(ScenePickingController_t1946337528, ___Tracker_2)); }
	inline InstantTracker_t684352120 * get_Tracker_2() const { return ___Tracker_2; }
	inline InstantTracker_t684352120 ** get_address_of_Tracker_2() { return &___Tracker_2; }
	inline void set_Tracker_2(InstantTracker_t684352120 * value)
	{
		___Tracker_2 = value;
		Il2CppCodeGenWriteBarrier(&___Tracker_2, value);
	}

	inline static int32_t get_offset_of_Augmentation_3() { return static_cast<int32_t>(offsetof(ScenePickingController_t1946337528, ___Augmentation_3)); }
	inline GameObject_t1113636619 * get_Augmentation_3() const { return ___Augmentation_3; }
	inline GameObject_t1113636619 ** get_address_of_Augmentation_3() { return &___Augmentation_3; }
	inline void set_Augmentation_3(GameObject_t1113636619 * value)
	{
		___Augmentation_3 = value;
		Il2CppCodeGenWriteBarrier(&___Augmentation_3, value);
	}

	inline static int32_t get_offset_of_ToggleStateButton_4() { return static_cast<int32_t>(offsetof(ScenePickingController_t1946337528, ___ToggleStateButton_4)); }
	inline Button_t4055032469 * get_ToggleStateButton_4() const { return ___ToggleStateButton_4; }
	inline Button_t4055032469 ** get_address_of_ToggleStateButton_4() { return &___ToggleStateButton_4; }
	inline void set_ToggleStateButton_4(Button_t4055032469 * value)
	{
		___ToggleStateButton_4 = value;
		Il2CppCodeGenWriteBarrier(&___ToggleStateButton_4, value);
	}

	inline static int32_t get_offset_of_ToggleStateButtonText_5() { return static_cast<int32_t>(offsetof(ScenePickingController_t1946337528, ___ToggleStateButtonText_5)); }
	inline Text_t1901882714 * get_ToggleStateButtonText_5() const { return ___ToggleStateButtonText_5; }
	inline Text_t1901882714 ** get_address_of_ToggleStateButtonText_5() { return &___ToggleStateButtonText_5; }
	inline void set_ToggleStateButtonText_5(Text_t1901882714 * value)
	{
		___ToggleStateButtonText_5 = value;
		Il2CppCodeGenWriteBarrier(&___ToggleStateButtonText_5, value);
	}

	inline static int32_t get_offset_of__currentTrackerState_6() { return static_cast<int32_t>(offsetof(ScenePickingController_t1946337528, ____currentTrackerState_6)); }
	inline int32_t get__currentTrackerState_6() const { return ____currentTrackerState_6; }
	inline int32_t* get_address_of__currentTrackerState_6() { return &____currentTrackerState_6; }
	inline void set__currentTrackerState_6(int32_t value)
	{
		____currentTrackerState_6 = value;
	}

	inline static int32_t get_offset_of__changingState_7() { return static_cast<int32_t>(offsetof(ScenePickingController_t1946337528, ____changingState_7)); }
	inline bool get__changingState_7() const { return ____changingState_7; }
	inline bool* get_address_of__changingState_7() { return &____changingState_7; }
	inline void set__changingState_7(bool value)
	{
		____changingState_7 = value;
	}

	inline static int32_t get_offset_of__gridRenderer_8() { return static_cast<int32_t>(offsetof(ScenePickingController_t1946337528, ____gridRenderer_8)); }
	inline GridRenderer_t1964421403 * get__gridRenderer_8() const { return ____gridRenderer_8; }
	inline GridRenderer_t1964421403 ** get_address_of__gridRenderer_8() { return &____gridRenderer_8; }
	inline void set__gridRenderer_8(GridRenderer_t1964421403 * value)
	{
		____gridRenderer_8 = value;
		Il2CppCodeGenWriteBarrier(&____gridRenderer_8, value);
	}

	inline static int32_t get_offset_of__augmentations_9() { return static_cast<int32_t>(offsetof(ScenePickingController_t1946337528, ____augmentations_9)); }
	inline List_1_t2585711361 * get__augmentations_9() const { return ____augmentations_9; }
	inline List_1_t2585711361 ** get_address_of__augmentations_9() { return &____augmentations_9; }
	inline void set__augmentations_9(List_1_t2585711361 * value)
	{
		____augmentations_9 = value;
		Il2CppCodeGenWriteBarrier(&____augmentations_9, value);
	}

	inline static int32_t get_offset_of__trackable_10() { return static_cast<int32_t>(offsetof(ScenePickingController_t1946337528, ____trackable_10)); }
	inline InstantTrackable_t3187174337 * get__trackable_10() const { return ____trackable_10; }
	inline InstantTrackable_t3187174337 ** get_address_of__trackable_10() { return &____trackable_10; }
	inline void set__trackable_10(InstantTrackable_t3187174337 * value)
	{
		____trackable_10 = value;
		Il2CppCodeGenWriteBarrier(&____trackable_10, value);
	}

	inline static int32_t get_offset_of__isTracking_11() { return static_cast<int32_t>(offsetof(ScenePickingController_t1946337528, ____isTracking_11)); }
	inline bool get__isTracking_11() const { return ____isTracking_11; }
	inline bool* get_address_of__isTracking_11() { return &____isTracking_11; }
	inline void set__isTracking_11(bool value)
	{
		____isTracking_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
