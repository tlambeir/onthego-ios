﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour3962482529.h"

// UnityEngine.GameObject
struct GameObject_t1113636619;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TransparancyManager
struct  TransparancyManager_t318855516  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject TransparancyManager::transparentAlternative
	GameObject_t1113636619 * ___transparentAlternative_2;
	// System.Boolean TransparancyManager::isTransparent
	bool ___isTransparent_3;

public:
	inline static int32_t get_offset_of_transparentAlternative_2() { return static_cast<int32_t>(offsetof(TransparancyManager_t318855516, ___transparentAlternative_2)); }
	inline GameObject_t1113636619 * get_transparentAlternative_2() const { return ___transparentAlternative_2; }
	inline GameObject_t1113636619 ** get_address_of_transparentAlternative_2() { return &___transparentAlternative_2; }
	inline void set_transparentAlternative_2(GameObject_t1113636619 * value)
	{
		___transparentAlternative_2 = value;
		Il2CppCodeGenWriteBarrier(&___transparentAlternative_2, value);
	}

	inline static int32_t get_offset_of_isTransparent_3() { return static_cast<int32_t>(offsetof(TransparancyManager_t318855516, ___isTransparent_3)); }
	inline bool get_isTransparent_3() const { return ___isTransparent_3; }
	inline bool* get_address_of_isTransparent_3() { return &___isTransparent_3; }
	inline void set_isTransparent_3(bool value)
	{
		___isTransparent_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
