﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour3962482529.h"

// UnityEngine.RectTransform
struct RectTransform_t3704657025;
// UnityEngine.UI.Image
struct Image_t2670269651;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Loading
struct  Loading_t2310272025  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.RectTransform Loading::rectComponent
	RectTransform_t3704657025 * ___rectComponent_2;
	// UnityEngine.UI.Image Loading::imageComp
	Image_t2670269651 * ___imageComp_3;
	// System.Boolean Loading::up
	bool ___up_4;
	// System.Single Loading::rotateSpeed
	float ___rotateSpeed_5;
	// System.Single Loading::openSpeed
	float ___openSpeed_6;
	// System.Single Loading::closeSpeed
	float ___closeSpeed_7;

public:
	inline static int32_t get_offset_of_rectComponent_2() { return static_cast<int32_t>(offsetof(Loading_t2310272025, ___rectComponent_2)); }
	inline RectTransform_t3704657025 * get_rectComponent_2() const { return ___rectComponent_2; }
	inline RectTransform_t3704657025 ** get_address_of_rectComponent_2() { return &___rectComponent_2; }
	inline void set_rectComponent_2(RectTransform_t3704657025 * value)
	{
		___rectComponent_2 = value;
		Il2CppCodeGenWriteBarrier(&___rectComponent_2, value);
	}

	inline static int32_t get_offset_of_imageComp_3() { return static_cast<int32_t>(offsetof(Loading_t2310272025, ___imageComp_3)); }
	inline Image_t2670269651 * get_imageComp_3() const { return ___imageComp_3; }
	inline Image_t2670269651 ** get_address_of_imageComp_3() { return &___imageComp_3; }
	inline void set_imageComp_3(Image_t2670269651 * value)
	{
		___imageComp_3 = value;
		Il2CppCodeGenWriteBarrier(&___imageComp_3, value);
	}

	inline static int32_t get_offset_of_up_4() { return static_cast<int32_t>(offsetof(Loading_t2310272025, ___up_4)); }
	inline bool get_up_4() const { return ___up_4; }
	inline bool* get_address_of_up_4() { return &___up_4; }
	inline void set_up_4(bool value)
	{
		___up_4 = value;
	}

	inline static int32_t get_offset_of_rotateSpeed_5() { return static_cast<int32_t>(offsetof(Loading_t2310272025, ___rotateSpeed_5)); }
	inline float get_rotateSpeed_5() const { return ___rotateSpeed_5; }
	inline float* get_address_of_rotateSpeed_5() { return &___rotateSpeed_5; }
	inline void set_rotateSpeed_5(float value)
	{
		___rotateSpeed_5 = value;
	}

	inline static int32_t get_offset_of_openSpeed_6() { return static_cast<int32_t>(offsetof(Loading_t2310272025, ___openSpeed_6)); }
	inline float get_openSpeed_6() const { return ___openSpeed_6; }
	inline float* get_address_of_openSpeed_6() { return &___openSpeed_6; }
	inline void set_openSpeed_6(float value)
	{
		___openSpeed_6 = value;
	}

	inline static int32_t get_offset_of_closeSpeed_7() { return static_cast<int32_t>(offsetof(Loading_t2310272025, ___closeSpeed_7)); }
	inline float get_closeSpeed_7() const { return ___closeSpeed_7; }
	inline float* get_address_of_closeSpeed_7() { return &___closeSpeed_7; }
	inline void set_closeSpeed_7(float value)
	{
		___closeSpeed_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
