﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "WikitudeUnityPlugin_Wikitude_TrackerBehaviour921360922.h"
#include "WikitudeUnityPlugin_Wikitude_TargetSourceType830527111.h"
#include "WikitudeUnityPlugin_Wikitude_ImageRecognitionRangeE604843042.h"

// Wikitude.ImageTracker/OnExtendedTrackingQualityChangedEvent
struct OnExtendedTrackingQualityChangedEvent_t2560195974;
// Wikitude.TargetCollectionResource
struct TargetCollectionResource_t66041399;
// Wikitude.CloudRecognitionService
struct CloudRecognitionService_t3866570042;
// System.Collections.Generic.Dictionary`2<System.String,System.Single>
struct Dictionary_2_t1182523073;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Wikitude.ImageTracker
struct  ImageTracker_t271395264  : public TrackerBehaviour_t921360922
{
public:
	// Wikitude.ImageTracker/OnExtendedTrackingQualityChangedEvent Wikitude.ImageTracker::OnExtendedTrackingQualityChanged
	OnExtendedTrackingQualityChangedEvent_t2560195974 * ___OnExtendedTrackingQualityChanged_15;
	// Wikitude.TargetSourceType Wikitude.ImageTracker::_targetSourceType
	int32_t ____targetSourceType_16;
	// Wikitude.TargetCollectionResource Wikitude.ImageTracker::_targetCollectionResource
	TargetCollectionResource_t66041399 * ____targetCollectionResource_17;
	// Wikitude.CloudRecognitionService Wikitude.ImageTracker::_cloudRecognitionService
	CloudRecognitionService_t3866570042 * ____cloudRecognitionService_18;
	// System.Int32 Wikitude.ImageTracker::_maximumNumberOfConcurrentTrackableTargets
	int32_t ____maximumNumberOfConcurrentTrackableTargets_19;
	// Wikitude.ImageRecognitionRangeExtension Wikitude.ImageTracker::_rangeExtension
	int32_t ____rangeExtension_20;
	// System.Collections.Generic.Dictionary`2<System.String,System.Single> Wikitude.ImageTracker::<PhysicalTargetImageHeights>k__BackingField
	Dictionary_2_t1182523073 * ___U3CPhysicalTargetImageHeightsU3Ek__BackingField_21;
	// System.Boolean Wikitude.ImageTracker::<IsRegistered>k__BackingField
	bool ___U3CIsRegisteredU3Ek__BackingField_22;

public:
	inline static int32_t get_offset_of_OnExtendedTrackingQualityChanged_15() { return static_cast<int32_t>(offsetof(ImageTracker_t271395264, ___OnExtendedTrackingQualityChanged_15)); }
	inline OnExtendedTrackingQualityChangedEvent_t2560195974 * get_OnExtendedTrackingQualityChanged_15() const { return ___OnExtendedTrackingQualityChanged_15; }
	inline OnExtendedTrackingQualityChangedEvent_t2560195974 ** get_address_of_OnExtendedTrackingQualityChanged_15() { return &___OnExtendedTrackingQualityChanged_15; }
	inline void set_OnExtendedTrackingQualityChanged_15(OnExtendedTrackingQualityChangedEvent_t2560195974 * value)
	{
		___OnExtendedTrackingQualityChanged_15 = value;
		Il2CppCodeGenWriteBarrier(&___OnExtendedTrackingQualityChanged_15, value);
	}

	inline static int32_t get_offset_of__targetSourceType_16() { return static_cast<int32_t>(offsetof(ImageTracker_t271395264, ____targetSourceType_16)); }
	inline int32_t get__targetSourceType_16() const { return ____targetSourceType_16; }
	inline int32_t* get_address_of__targetSourceType_16() { return &____targetSourceType_16; }
	inline void set__targetSourceType_16(int32_t value)
	{
		____targetSourceType_16 = value;
	}

	inline static int32_t get_offset_of__targetCollectionResource_17() { return static_cast<int32_t>(offsetof(ImageTracker_t271395264, ____targetCollectionResource_17)); }
	inline TargetCollectionResource_t66041399 * get__targetCollectionResource_17() const { return ____targetCollectionResource_17; }
	inline TargetCollectionResource_t66041399 ** get_address_of__targetCollectionResource_17() { return &____targetCollectionResource_17; }
	inline void set__targetCollectionResource_17(TargetCollectionResource_t66041399 * value)
	{
		____targetCollectionResource_17 = value;
		Il2CppCodeGenWriteBarrier(&____targetCollectionResource_17, value);
	}

	inline static int32_t get_offset_of__cloudRecognitionService_18() { return static_cast<int32_t>(offsetof(ImageTracker_t271395264, ____cloudRecognitionService_18)); }
	inline CloudRecognitionService_t3866570042 * get__cloudRecognitionService_18() const { return ____cloudRecognitionService_18; }
	inline CloudRecognitionService_t3866570042 ** get_address_of__cloudRecognitionService_18() { return &____cloudRecognitionService_18; }
	inline void set__cloudRecognitionService_18(CloudRecognitionService_t3866570042 * value)
	{
		____cloudRecognitionService_18 = value;
		Il2CppCodeGenWriteBarrier(&____cloudRecognitionService_18, value);
	}

	inline static int32_t get_offset_of__maximumNumberOfConcurrentTrackableTargets_19() { return static_cast<int32_t>(offsetof(ImageTracker_t271395264, ____maximumNumberOfConcurrentTrackableTargets_19)); }
	inline int32_t get__maximumNumberOfConcurrentTrackableTargets_19() const { return ____maximumNumberOfConcurrentTrackableTargets_19; }
	inline int32_t* get_address_of__maximumNumberOfConcurrentTrackableTargets_19() { return &____maximumNumberOfConcurrentTrackableTargets_19; }
	inline void set__maximumNumberOfConcurrentTrackableTargets_19(int32_t value)
	{
		____maximumNumberOfConcurrentTrackableTargets_19 = value;
	}

	inline static int32_t get_offset_of__rangeExtension_20() { return static_cast<int32_t>(offsetof(ImageTracker_t271395264, ____rangeExtension_20)); }
	inline int32_t get__rangeExtension_20() const { return ____rangeExtension_20; }
	inline int32_t* get_address_of__rangeExtension_20() { return &____rangeExtension_20; }
	inline void set__rangeExtension_20(int32_t value)
	{
		____rangeExtension_20 = value;
	}

	inline static int32_t get_offset_of_U3CPhysicalTargetImageHeightsU3Ek__BackingField_21() { return static_cast<int32_t>(offsetof(ImageTracker_t271395264, ___U3CPhysicalTargetImageHeightsU3Ek__BackingField_21)); }
	inline Dictionary_2_t1182523073 * get_U3CPhysicalTargetImageHeightsU3Ek__BackingField_21() const { return ___U3CPhysicalTargetImageHeightsU3Ek__BackingField_21; }
	inline Dictionary_2_t1182523073 ** get_address_of_U3CPhysicalTargetImageHeightsU3Ek__BackingField_21() { return &___U3CPhysicalTargetImageHeightsU3Ek__BackingField_21; }
	inline void set_U3CPhysicalTargetImageHeightsU3Ek__BackingField_21(Dictionary_2_t1182523073 * value)
	{
		___U3CPhysicalTargetImageHeightsU3Ek__BackingField_21 = value;
		Il2CppCodeGenWriteBarrier(&___U3CPhysicalTargetImageHeightsU3Ek__BackingField_21, value);
	}

	inline static int32_t get_offset_of_U3CIsRegisteredU3Ek__BackingField_22() { return static_cast<int32_t>(offsetof(ImageTracker_t271395264, ___U3CIsRegisteredU3Ek__BackingField_22)); }
	inline bool get_U3CIsRegisteredU3Ek__BackingField_22() const { return ___U3CIsRegisteredU3Ek__BackingField_22; }
	inline bool* get_address_of_U3CIsRegisteredU3Ek__BackingField_22() { return &___U3CIsRegisteredU3Ek__BackingField_22; }
	inline void set_U3CIsRegisteredU3Ek__BackingField_22(bool value)
	{
		___U3CIsRegisteredU3Ek__BackingField_22 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
