﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour3962482529.h"
#include "UnityEngine_UnityEngine_HideFlags4250555765.h"

// UnityEngine.WaitForEndOfFrame
struct WaitForEndOfFrame_t1314943911;
// System.String
struct String_t;
// UnityEngine.Renderer
struct Renderer_t2627027031;
// System.Collections.Generic.List`1<HighlightingSystem.HighlighterRenderer/Data>
struct List_1_t1906947814;
// UnityEngine.Camera
struct Camera_t4157153871;
// UnityEngine.Coroutine
struct Coroutine_t3829159415;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HighlightingSystem.HighlighterRenderer
struct  HighlighterRenderer_t1776162451  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Renderer HighlightingSystem.HighlighterRenderer::r
	Renderer_t2627027031 * ___r_11;
	// System.Collections.Generic.List`1<HighlightingSystem.HighlighterRenderer/Data> HighlightingSystem.HighlighterRenderer::data
	List_1_t1906947814 * ___data_12;
	// UnityEngine.Camera HighlightingSystem.HighlighterRenderer::lastCamera
	Camera_t4157153871 * ___lastCamera_13;
	// System.Boolean HighlightingSystem.HighlighterRenderer::isAlive
	bool ___isAlive_14;
	// UnityEngine.Coroutine HighlightingSystem.HighlighterRenderer::endOfFrame
	Coroutine_t3829159415 * ___endOfFrame_15;

public:
	inline static int32_t get_offset_of_r_11() { return static_cast<int32_t>(offsetof(HighlighterRenderer_t1776162451, ___r_11)); }
	inline Renderer_t2627027031 * get_r_11() const { return ___r_11; }
	inline Renderer_t2627027031 ** get_address_of_r_11() { return &___r_11; }
	inline void set_r_11(Renderer_t2627027031 * value)
	{
		___r_11 = value;
		Il2CppCodeGenWriteBarrier(&___r_11, value);
	}

	inline static int32_t get_offset_of_data_12() { return static_cast<int32_t>(offsetof(HighlighterRenderer_t1776162451, ___data_12)); }
	inline List_1_t1906947814 * get_data_12() const { return ___data_12; }
	inline List_1_t1906947814 ** get_address_of_data_12() { return &___data_12; }
	inline void set_data_12(List_1_t1906947814 * value)
	{
		___data_12 = value;
		Il2CppCodeGenWriteBarrier(&___data_12, value);
	}

	inline static int32_t get_offset_of_lastCamera_13() { return static_cast<int32_t>(offsetof(HighlighterRenderer_t1776162451, ___lastCamera_13)); }
	inline Camera_t4157153871 * get_lastCamera_13() const { return ___lastCamera_13; }
	inline Camera_t4157153871 ** get_address_of_lastCamera_13() { return &___lastCamera_13; }
	inline void set_lastCamera_13(Camera_t4157153871 * value)
	{
		___lastCamera_13 = value;
		Il2CppCodeGenWriteBarrier(&___lastCamera_13, value);
	}

	inline static int32_t get_offset_of_isAlive_14() { return static_cast<int32_t>(offsetof(HighlighterRenderer_t1776162451, ___isAlive_14)); }
	inline bool get_isAlive_14() const { return ___isAlive_14; }
	inline bool* get_address_of_isAlive_14() { return &___isAlive_14; }
	inline void set_isAlive_14(bool value)
	{
		___isAlive_14 = value;
	}

	inline static int32_t get_offset_of_endOfFrame_15() { return static_cast<int32_t>(offsetof(HighlighterRenderer_t1776162451, ___endOfFrame_15)); }
	inline Coroutine_t3829159415 * get_endOfFrame_15() const { return ___endOfFrame_15; }
	inline Coroutine_t3829159415 ** get_address_of_endOfFrame_15() { return &___endOfFrame_15; }
	inline void set_endOfFrame_15(Coroutine_t3829159415 * value)
	{
		___endOfFrame_15 = value;
		Il2CppCodeGenWriteBarrier(&___endOfFrame_15, value);
	}
};

struct HighlighterRenderer_t1776162451_StaticFields
{
public:
	// System.Single HighlightingSystem.HighlighterRenderer::transparentCutoff
	float ___transparentCutoff_2;
	// UnityEngine.WaitForEndOfFrame HighlightingSystem.HighlighterRenderer::waitForEndOfFrame
	WaitForEndOfFrame_t1314943911 * ___waitForEndOfFrame_5;
	// System.String HighlightingSystem.HighlighterRenderer::sRenderType
	String_t* ___sRenderType_6;
	// System.String HighlightingSystem.HighlighterRenderer::sOpaque
	String_t* ___sOpaque_7;
	// System.String HighlightingSystem.HighlighterRenderer::sTransparent
	String_t* ___sTransparent_8;
	// System.String HighlightingSystem.HighlighterRenderer::sTransparentCutout
	String_t* ___sTransparentCutout_9;
	// System.String HighlightingSystem.HighlighterRenderer::sMainTex
	String_t* ___sMainTex_10;

public:
	inline static int32_t get_offset_of_transparentCutoff_2() { return static_cast<int32_t>(offsetof(HighlighterRenderer_t1776162451_StaticFields, ___transparentCutoff_2)); }
	inline float get_transparentCutoff_2() const { return ___transparentCutoff_2; }
	inline float* get_address_of_transparentCutoff_2() { return &___transparentCutoff_2; }
	inline void set_transparentCutoff_2(float value)
	{
		___transparentCutoff_2 = value;
	}

	inline static int32_t get_offset_of_waitForEndOfFrame_5() { return static_cast<int32_t>(offsetof(HighlighterRenderer_t1776162451_StaticFields, ___waitForEndOfFrame_5)); }
	inline WaitForEndOfFrame_t1314943911 * get_waitForEndOfFrame_5() const { return ___waitForEndOfFrame_5; }
	inline WaitForEndOfFrame_t1314943911 ** get_address_of_waitForEndOfFrame_5() { return &___waitForEndOfFrame_5; }
	inline void set_waitForEndOfFrame_5(WaitForEndOfFrame_t1314943911 * value)
	{
		___waitForEndOfFrame_5 = value;
		Il2CppCodeGenWriteBarrier(&___waitForEndOfFrame_5, value);
	}

	inline static int32_t get_offset_of_sRenderType_6() { return static_cast<int32_t>(offsetof(HighlighterRenderer_t1776162451_StaticFields, ___sRenderType_6)); }
	inline String_t* get_sRenderType_6() const { return ___sRenderType_6; }
	inline String_t** get_address_of_sRenderType_6() { return &___sRenderType_6; }
	inline void set_sRenderType_6(String_t* value)
	{
		___sRenderType_6 = value;
		Il2CppCodeGenWriteBarrier(&___sRenderType_6, value);
	}

	inline static int32_t get_offset_of_sOpaque_7() { return static_cast<int32_t>(offsetof(HighlighterRenderer_t1776162451_StaticFields, ___sOpaque_7)); }
	inline String_t* get_sOpaque_7() const { return ___sOpaque_7; }
	inline String_t** get_address_of_sOpaque_7() { return &___sOpaque_7; }
	inline void set_sOpaque_7(String_t* value)
	{
		___sOpaque_7 = value;
		Il2CppCodeGenWriteBarrier(&___sOpaque_7, value);
	}

	inline static int32_t get_offset_of_sTransparent_8() { return static_cast<int32_t>(offsetof(HighlighterRenderer_t1776162451_StaticFields, ___sTransparent_8)); }
	inline String_t* get_sTransparent_8() const { return ___sTransparent_8; }
	inline String_t** get_address_of_sTransparent_8() { return &___sTransparent_8; }
	inline void set_sTransparent_8(String_t* value)
	{
		___sTransparent_8 = value;
		Il2CppCodeGenWriteBarrier(&___sTransparent_8, value);
	}

	inline static int32_t get_offset_of_sTransparentCutout_9() { return static_cast<int32_t>(offsetof(HighlighterRenderer_t1776162451_StaticFields, ___sTransparentCutout_9)); }
	inline String_t* get_sTransparentCutout_9() const { return ___sTransparentCutout_9; }
	inline String_t** get_address_of_sTransparentCutout_9() { return &___sTransparentCutout_9; }
	inline void set_sTransparentCutout_9(String_t* value)
	{
		___sTransparentCutout_9 = value;
		Il2CppCodeGenWriteBarrier(&___sTransparentCutout_9, value);
	}

	inline static int32_t get_offset_of_sMainTex_10() { return static_cast<int32_t>(offsetof(HighlighterRenderer_t1776162451_StaticFields, ___sMainTex_10)); }
	inline String_t* get_sMainTex_10() const { return ___sMainTex_10; }
	inline String_t** get_address_of_sMainTex_10() { return &___sMainTex_10; }
	inline void set_sMainTex_10(String_t* value)
	{
		___sMainTex_10 = value;
		Il2CppCodeGenWriteBarrier(&___sMainTex_10, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
