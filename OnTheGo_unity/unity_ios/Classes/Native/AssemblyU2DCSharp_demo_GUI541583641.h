﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour3962482529.h"

// UnityEngine.GameObject
struct GameObject_t1113636619;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// demo_GUI
struct  demo_GUI_t541583641  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject demo_GUI::targetCamSystem
	GameObject_t1113636619 * ___targetCamSystem_2;

public:
	inline static int32_t get_offset_of_targetCamSystem_2() { return static_cast<int32_t>(offsetof(demo_GUI_t541583641, ___targetCamSystem_2)); }
	inline GameObject_t1113636619 * get_targetCamSystem_2() const { return ___targetCamSystem_2; }
	inline GameObject_t1113636619 ** get_address_of_targetCamSystem_2() { return &___targetCamSystem_2; }
	inline void set_targetCamSystem_2(GameObject_t1113636619 * value)
	{
		___targetCamSystem_2 = value;
		Il2CppCodeGenWriteBarrier(&___targetCamSystem_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
