﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_TMPro_Examples_VertexZoom_U3CAnim446847514.h"
#include "AssemblyU2DCSharp_TMPro_Examples_WarpTextExample3821118074.h"
#include "AssemblyU2DCSharp_TMPro_Examples_WarpTextExample_U4025661343.h"
#include "AssemblyU2DCSharp_Platform1043456379.h"
#include "AssemblyU2DCSharp_BarcodePlugin946579598.h"
#include "AssemblyU2DCSharp_CameraSettingsController1300359422.h"
#include "AssemblyU2DCSharp_CameraSettingsController_U3CResta263482477.h"
#include "AssemblyU2DCSharp_ContinuousRecognitionController1358047073.h"
#include "AssemblyU2DCSharp_ExtendedTrackingController158808967.h"
#include "AssemblyU2DCSharp_CustomCameraController1440392445.h"
#include "AssemblyU2DCSharp_CustomCameraController_InputFram2573966900.h"
#include "AssemblyU2DCSharp_CustomCameraController_U3CInitia1849050269.h"
#include "AssemblyU2DCSharp_CustomCameraRenderer2825061581.h"
#include "AssemblyU2DCSharp_CustomCameraRenderer_ImageRotati1158944265.h"
#include "AssemblyU2DCSharp_SimpleInputPluginController2024056797.h"
#include "AssemblyU2DCSharp_SimpleInputPluginController_U3CI3768135150.h"
#include "AssemblyU2DCSharp_DockUI978320394.h"
#include "AssemblyU2DCSharp_GridRenderer1964421403.h"
#include "AssemblyU2DCSharp_InstantTrackingController1374048463.h"
#include "AssemblyU2DCSharp_MoveController1157877467.h"
#include "AssemblyU2DCSharp_ScaleController681898147.h"
#include "AssemblyU2DCSharp_ScenePickingController1946337528.h"
#include "AssemblyU2DCSharp_MenuControllerWikitude2135746466.h"
#include "AssemblyU2DCSharp_Dinosaur1302057126.h"
#include "AssemblyU2DCSharp_Dinosaur_State4260191658.h"
#include "AssemblyU2DCSharp_Dinosaur_U3CStartAttackSequenceU3157997599.h"
#include "AssemblyU2DCSharp_Dinosaur_U3CWalkBackSequenceU3Ec3959348746.h"
#include "AssemblyU2DCSharp_Dinosaur_U3CStartDefendSequenceU3400060597.h"
#include "AssemblyU2DCSharp_Dinosaur_U3CRotateTowardsU3Ec__I1999157622.h"
#include "AssemblyU2DCSharp_Dinosaur_U3CMoveTowardsU3Ec__Ite3738932793.h"
#include "AssemblyU2DCSharp_Dinosaur_U3CSetWalkingSpeedCorout798448162.h"
#include "AssemblyU2DCSharp_MultipleTargetsController3623068400.h"
#include "AssemblyU2DCSharp_MultipleTrackersController1631955839.h"
#include "AssemblyU2DCSharp_ObjectTrackingController3583561264.h"
#include "AssemblyU2DCSharp_PluginController2790031014.h"
#include "AssemblyU2DCSharp_RuntimeTrackerController400852104.h"
#include "AssemblyU2DCSharp_SampleController2879308770.h"
#include "AssemblyU2DCSharp_SingleRecognitionController1501371400.h"
#include "AssemblyU2DCSharp_descriptionCloseManager1451768383.h"
#include "AssemblyU2DCSharp_descriptionmanager4101791333.h"
#include "AssemblyU2DCSharp_landscapeTransformManager1971019274.h"
#include "AssemblyU2DCSharp_layoutClickManager3446951142.h"
#include "AssemblyU2DCSharp_Loading2310272025.h"
#include "AssemblyU2DCSharp_SimpleLoading205988226.h"
#include "AssemblyU2DCSharp_loadingbar3045447973.h"
#include "AssemblyU2DCSharp_loadingcolorful4083442456.h"
#include "AssemblyU2DCSharp_loadingtext2909752294.h"
#include "AssemblyU2DCSharp_rotatetotate3724567416.h"
#include "AssemblyU2DCSharp_simplerotate361467861.h"
#include "AssemblyU2DCSharp_lookAtCamera384050562.h"
#include "AssemblyU2DCSharp_U3CPrivateImplementationDetailsU3057255361.h"
#include "AssemblyU2DCSharp_U3CPrivateImplementationDetailsU3253128244.h"
#include "AssemblyU2DCSharp_U3CPrivateImplementationDetailsU3120960362.h"
#include "AssemblyU2DCSharp_U3CPrivateImplementationDetailsU2488454196.h"
#include "AssemblyU2DUnityScript_U3CModuleU3E692745525.h"
#include "AssemblyU2DUnityScript_JSHighlighterController967495899.h"







#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2300 = { sizeof (U3CAnimateVertexColorsU3Ec__AnonStorey1_t446847514), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2300[2] = 
{
	U3CAnimateVertexColorsU3Ec__AnonStorey1_t446847514::get_offset_of_modifiedCharScale_0(),
	U3CAnimateVertexColorsU3Ec__AnonStorey1_t446847514::get_offset_of_U3CU3Ef__refU240_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2301 = { sizeof (WarpTextExample_t3821118074), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2301[5] = 
{
	WarpTextExample_t3821118074::get_offset_of_m_TextComponent_2(),
	WarpTextExample_t3821118074::get_offset_of_VertexCurve_3(),
	WarpTextExample_t3821118074::get_offset_of_AngleMultiplier_4(),
	WarpTextExample_t3821118074::get_offset_of_SpeedMultiplier_5(),
	WarpTextExample_t3821118074::get_offset_of_CurveScale_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2302 = { sizeof (U3CWarpTextU3Ec__Iterator0_t4025661343), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2302[12] = 
{
	U3CWarpTextU3Ec__Iterator0_t4025661343::get_offset_of_U3Cold_CurveScaleU3E__0_0(),
	U3CWarpTextU3Ec__Iterator0_t4025661343::get_offset_of_U3Cold_curveU3E__0_1(),
	U3CWarpTextU3Ec__Iterator0_t4025661343::get_offset_of_U3CtextInfoU3E__1_2(),
	U3CWarpTextU3Ec__Iterator0_t4025661343::get_offset_of_U3CcharacterCountU3E__1_3(),
	U3CWarpTextU3Ec__Iterator0_t4025661343::get_offset_of_U3CboundsMinXU3E__1_4(),
	U3CWarpTextU3Ec__Iterator0_t4025661343::get_offset_of_U3CboundsMaxXU3E__1_5(),
	U3CWarpTextU3Ec__Iterator0_t4025661343::get_offset_of_U3CverticesU3E__2_6(),
	U3CWarpTextU3Ec__Iterator0_t4025661343::get_offset_of_U3CmatrixU3E__2_7(),
	U3CWarpTextU3Ec__Iterator0_t4025661343::get_offset_of_U24this_8(),
	U3CWarpTextU3Ec__Iterator0_t4025661343::get_offset_of_U24current_9(),
	U3CWarpTextU3Ec__Iterator0_t4025661343::get_offset_of_U24disposing_10(),
	U3CWarpTextU3Ec__Iterator0_t4025661343::get_offset_of_U24PC_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2303 = { sizeof (Platform_t1043456379), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2304 = { sizeof (BarcodePlugin_t946579598), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2305 = { sizeof (CameraSettingsController_t1300359422), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2305[15] = 
{
	CameraSettingsController_t1300359422::get_offset_of_Camera_2(),
	CameraSettingsController_t1300359422::get_offset_of_CurrentImageTracker_3(),
	CameraSettingsController_t1300359422::get_offset_of_ConfirmationTab_4(),
	CameraSettingsController_t1300359422::get_offset_of_ControlsLayout_5(),
	CameraSettingsController_t1300359422::get_offset_of_PositionDropdown_6(),
	CameraSettingsController_t1300359422::get_offset_of_FocusModeDropdown_7(),
	CameraSettingsController_t1300359422::get_offset_of_AutoFocusRestrictionLayout_8(),
	CameraSettingsController_t1300359422::get_offset_of_AutoFocusRestrictionDropdown_9(),
	CameraSettingsController_t1300359422::get_offset_of_FlashModeDropdown_10(),
	CameraSettingsController_t1300359422::get_offset_of_ZoomLayout_11(),
	CameraSettingsController_t1300359422::get_offset_of_ZoomScrollbar_12(),
	CameraSettingsController_t1300359422::get_offset_of_ManualFocusLayout_13(),
	CameraSettingsController_t1300359422::get_offset_of_WikitudeCameraPrefab_14(),
	CameraSettingsController_t1300359422::get_offset_of_ImageTrackerPrefab_15(),
	CameraSettingsController_t1300359422::get_offset_of_WikitudeEyePrefab_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2306 = { sizeof (U3CRestartU3Ec__Iterator0_t263482477), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2306[4] = 
{
	U3CRestartU3Ec__Iterator0_t263482477::get_offset_of_U24this_0(),
	U3CRestartU3Ec__Iterator0_t263482477::get_offset_of_U24current_1(),
	U3CRestartU3Ec__Iterator0_t263482477::get_offset_of_U24disposing_2(),
	U3CRestartU3Ec__Iterator0_t263482477::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2307 = { sizeof (ContinuousRecognitionController_t1358047073), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2307[5] = 
{
	ContinuousRecognitionController_t1358047073::get_offset_of_Tracker_2(),
	ContinuousRecognitionController_t1358047073::get_offset_of_buttonText_3(),
	ContinuousRecognitionController_t1358047073::get_offset_of__trackerRunning_4(),
	ContinuousRecognitionController_t1358047073::get_offset_of__connectionInitialized_5(),
	ContinuousRecognitionController_t1358047073::get_offset_of__recognitionInterval_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2308 = { sizeof (ExtendedTrackingController_t158808967), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2308[4] = 
{
	ExtendedTrackingController_t158808967::get_offset_of_Tracker_2(),
	ExtendedTrackingController_t158808967::get_offset_of_TrackingQualityText_3(),
	ExtendedTrackingController_t158808967::get_offset_of_TrackingQualityBackground_4(),
	ExtendedTrackingController_t158808967::get_offset_of_StopExtendedTrackingButton_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2309 = { sizeof (CustomCameraController_t1440392445), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2309[12] = 
{
	CustomCameraController_t1440392445::get_offset_of_WikitudeCam_2(),
	CustomCameraController_t1440392445::get_offset_of__feed_3(),
	0,
	0,
	CustomCameraController_t1440392445::get_offset_of__frameDataSize_6(),
	CustomCameraController_t1440392445::get_offset_of__frameIndex_7(),
	CustomCameraController_t1440392445::get_offset_of__bufferWriteIndex_8(),
	CustomCameraController_t1440392445::get_offset_of__bufferReadIndex_9(),
	CustomCameraController_t1440392445::get_offset_of__bufferCount_10(),
	CustomCameraController_t1440392445::get_offset_of__ringBuffer_11(),
	CustomCameraController_t1440392445::get_offset_of__colorData_12(),
	CustomCameraController_t1440392445::get_offset_of_Renderer_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2310 = { sizeof (InputFrameData_t2573966900)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2310[2] = 
{
	InputFrameData_t2573966900::get_offset_of_Index_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	InputFrameData_t2573966900::get_offset_of_Texture_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2311 = { sizeof (U3CInitializeU3Ec__Iterator0_t1849050269), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2311[7] = 
{
	U3CInitializeU3Ec__Iterator0_t1849050269::get_offset_of_firstStart_0(),
	U3CInitializeU3Ec__Iterator0_t1849050269::get_offset_of_U24locvar0_1(),
	U3CInitializeU3Ec__Iterator0_t1849050269::get_offset_of_U24locvar1_2(),
	U3CInitializeU3Ec__Iterator0_t1849050269::get_offset_of_U24this_3(),
	U3CInitializeU3Ec__Iterator0_t1849050269::get_offset_of_U24current_4(),
	U3CInitializeU3Ec__Iterator0_t1849050269::get_offset_of_U24disposing_5(),
	U3CInitializeU3Ec__Iterator0_t1849050269::get_offset_of_U24PC_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2312 = { sizeof (CustomCameraRenderer_t2825061581), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2312[6] = 
{
	CustomCameraRenderer_t2825061581::get_offset_of_EffectMaterial_2(),
	CustomCameraRenderer_t2825061581::get_offset_of__currentFrame_3(),
	CustomCameraRenderer_t2825061581::get_offset_of__flipImage_4(),
	CustomCameraRenderer_t2825061581::get_offset_of__drawFrameBuffer_5(),
	CustomCameraRenderer_t2825061581::get_offset_of__currentScreenWidth_6(),
	CustomCameraRenderer_t2825061581::get_offset_of__currentScreenHeight_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2313 = { sizeof (ImageRotation_t1158944265)+ sizeof (Il2CppObject), sizeof(ImageRotation_t1158944265_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2313[3] = 
{
	ImageRotation_t1158944265::get_offset_of_FlipHorizontally_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ImageRotation_t1158944265::get_offset_of_FlipVertically_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ImageRotation_t1158944265::get_offset_of_Rotate_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2314 = { sizeof (SimpleInputPluginController_t2024056797), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2314[7] = 
{
	SimpleInputPluginController_t2024056797::get_offset_of_WikitudeCam_2(),
	SimpleInputPluginController_t2024056797::get_offset_of__feed_3(),
	0,
	0,
	SimpleInputPluginController_t2024056797::get_offset_of__frameDataSize_6(),
	SimpleInputPluginController_t2024056797::get_offset_of__frameIndex_7(),
	SimpleInputPluginController_t2024056797::get_offset_of__pixels_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2315 = { sizeof (U3CInitializeU3Ec__Iterator0_t3768135150), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2315[6] = 
{
	U3CInitializeU3Ec__Iterator0_t3768135150::get_offset_of_U24locvar0_0(),
	U3CInitializeU3Ec__Iterator0_t3768135150::get_offset_of_U24locvar1_1(),
	U3CInitializeU3Ec__Iterator0_t3768135150::get_offset_of_U24this_2(),
	U3CInitializeU3Ec__Iterator0_t3768135150::get_offset_of_U24current_3(),
	U3CInitializeU3Ec__Iterator0_t3768135150::get_offset_of_U24disposing_4(),
	U3CInitializeU3Ec__Iterator0_t3768135150::get_offset_of_U24PC_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2316 = { sizeof (DockUI_t978320394), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2316[2] = 
{
	DockUI_t978320394::get_offset_of_ButtonsBackground_2(),
	DockUI_t978320394::get_offset_of_ButtonsParent_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2317 = { sizeof (GridRenderer_t1964421403), -1, sizeof(GridRenderer_t1964421403_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2317[9] = 
{
	GridRenderer_t1964421403_StaticFields::get_offset_of_TargetColor_2(),
	GridRenderer_t1964421403_StaticFields::get_offset_of_GridColor_3(),
	GridRenderer_t1964421403_StaticFields::get_offset_of_MainLineColor_4(),
	GridRenderer_t1964421403_StaticFields::get_offset_of_UnitLineColor_5(),
	0,
	0,
	0,
	0,
	GridRenderer_t1964421403::get_offset_of__lineMaterial_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2318 = { sizeof (InstantTrackingController_t1374048463), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2318[17] = 
{
	InstantTrackingController_t1374048463::get_offset_of_ButtonDock_2(),
	InstantTrackingController_t1374048463::get_offset_of_InitializationControls_3(),
	InstantTrackingController_t1374048463::get_offset_of_active_4(),
	InstantTrackingController_t1374048463::get_offset_of_HeightLabel_5(),
	InstantTrackingController_t1374048463::get_offset_of_ScaleLabel_6(),
	InstantTrackingController_t1374048463::get_offset_of_Tracker_7(),
	InstantTrackingController_t1374048463::get_offset_of_Buttons_8(),
	InstantTrackingController_t1374048463::get_offset_of_Models_9(),
	InstantTrackingController_t1374048463::get_offset_of_ActivityIndicator_10(),
	InstantTrackingController_t1374048463::get_offset_of_EnabledColor_11(),
	InstantTrackingController_t1374048463::get_offset_of_DisabledColor_12(),
	InstantTrackingController_t1374048463::get_offset_of__currentDeviceHeightAboveGround_13(),
	InstantTrackingController_t1374048463::get_offset_of__moveController_14(),
	InstantTrackingController_t1374048463::get_offset_of__gridRenderer_15(),
	InstantTrackingController_t1374048463::get_offset_of__activeModels_16(),
	InstantTrackingController_t1374048463::get_offset_of__currentState_17(),
	InstantTrackingController_t1374048463::get_offset_of__isTracking_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2319 = { sizeof (MoveController_t1157877467), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2319[5] = 
{
	MoveController_t1157877467::get_offset_of__activeObject_2(),
	MoveController_t1157877467::get_offset_of__startObjectPosition_3(),
	MoveController_t1157877467::get_offset_of__startTouchPosition_4(),
	MoveController_t1157877467::get_offset_of__touchOffset_5(),
	MoveController_t1157877467::get_offset_of__controller_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2320 = { sizeof (ScaleController_t681898147), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2320[7] = 
{
	ScaleController_t681898147::get_offset_of_MinScale_2(),
	ScaleController_t681898147::get_offset_of_MaxScale_3(),
	ScaleController_t681898147::get_offset_of__controller_4(),
	ScaleController_t681898147::get_offset_of__activeObject_5(),
	ScaleController_t681898147::get_offset_of__touch1StartGroundPosition_6(),
	ScaleController_t681898147::get_offset_of__touch2StartGroundPosition_7(),
	ScaleController_t681898147::get_offset_of__startObjectScale_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2321 = { sizeof (ScenePickingController_t1946337528), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2321[10] = 
{
	ScenePickingController_t1946337528::get_offset_of_Tracker_2(),
	ScenePickingController_t1946337528::get_offset_of_Augmentation_3(),
	ScenePickingController_t1946337528::get_offset_of_ToggleStateButton_4(),
	ScenePickingController_t1946337528::get_offset_of_ToggleStateButtonText_5(),
	ScenePickingController_t1946337528::get_offset_of__currentTrackerState_6(),
	ScenePickingController_t1946337528::get_offset_of__changingState_7(),
	ScenePickingController_t1946337528::get_offset_of__gridRenderer_8(),
	ScenePickingController_t1946337528::get_offset_of__augmentations_9(),
	ScenePickingController_t1946337528::get_offset_of__trackable_10(),
	ScenePickingController_t1946337528::get_offset_of__isTracking_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2322 = { sizeof (MenuControllerWikitude_t2135746466), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2322[6] = 
{
	MenuControllerWikitude_t2135746466::get_offset_of_InfoPanel_2(),
	MenuControllerWikitude_t2135746466::get_offset_of_VersionNumberText_3(),
	MenuControllerWikitude_t2135746466::get_offset_of_BuildDateText_4(),
	MenuControllerWikitude_t2135746466::get_offset_of_BuildNumberText_5(),
	MenuControllerWikitude_t2135746466::get_offset_of_BuildConfigurationText_6(),
	MenuControllerWikitude_t2135746466::get_offset_of_UnityVersionText_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2323 = { sizeof (Dinosaur_t1302057126), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2323[17] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	Dinosaur_t1302057126::get_offset_of__sequenceCoroutine_10(),
	Dinosaur_t1302057126::get_offset_of__walkingSpeedCoroutine_11(),
	Dinosaur_t1302057126::get_offset_of_U3CTargetDinosaurU3Ek__BackingField_12(),
	Dinosaur_t1302057126::get_offset_of_U3CAttackingDinosaurU3Ek__BackingField_13(),
	Dinosaur_t1302057126::get_offset_of__animator_14(),
	Dinosaur_t1302057126::get_offset_of_RotationSpeed_15(),
	Dinosaur_t1302057126::get_offset_of_MovementSpeed_16(),
	Dinosaur_t1302057126::get_offset_of_U3CInBattleU3Ek__BackingField_17(),
	Dinosaur_t1302057126::get_offset_of__currentState_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2324 = { sizeof (State_t4260191658)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2324[9] = 
{
	State_t4260191658::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2325 = { sizeof (U3CStartAttackSequenceU3Ec__Iterator0_t3157997599), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2325[4] = 
{
	U3CStartAttackSequenceU3Ec__Iterator0_t3157997599::get_offset_of_U24this_0(),
	U3CStartAttackSequenceU3Ec__Iterator0_t3157997599::get_offset_of_U24current_1(),
	U3CStartAttackSequenceU3Ec__Iterator0_t3157997599::get_offset_of_U24disposing_2(),
	U3CStartAttackSequenceU3Ec__Iterator0_t3157997599::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2326 = { sizeof (U3CWalkBackSequenceU3Ec__Iterator1_t3959348746), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2326[4] = 
{
	U3CWalkBackSequenceU3Ec__Iterator1_t3959348746::get_offset_of_U24this_0(),
	U3CWalkBackSequenceU3Ec__Iterator1_t3959348746::get_offset_of_U24current_1(),
	U3CWalkBackSequenceU3Ec__Iterator1_t3959348746::get_offset_of_U24disposing_2(),
	U3CWalkBackSequenceU3Ec__Iterator1_t3959348746::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2327 = { sizeof (U3CStartDefendSequenceU3Ec__Iterator2_t3400060597), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2327[4] = 
{
	U3CStartDefendSequenceU3Ec__Iterator2_t3400060597::get_offset_of_U24this_0(),
	U3CStartDefendSequenceU3Ec__Iterator2_t3400060597::get_offset_of_U24current_1(),
	U3CStartDefendSequenceU3Ec__Iterator2_t3400060597::get_offset_of_U24disposing_2(),
	U3CStartDefendSequenceU3Ec__Iterator2_t3400060597::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2328 = { sizeof (U3CRotateTowardsU3Ec__Iterator3_t1999157622), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2328[9] = 
{
	U3CRotateTowardsU3Ec__Iterator3_t1999157622::get_offset_of_rotationTarget_0(),
	U3CRotateTowardsU3Ec__Iterator3_t1999157622::get_offset_of_U3CtargetRotationU3E__0_1(),
	U3CRotateTowardsU3Ec__Iterator3_t1999157622::get_offset_of_U3CangleToTargetU3E__0_2(),
	U3CRotateTowardsU3Ec__Iterator3_t1999157622::get_offset_of_U3CmaxAngleU3E__1_3(),
	U3CRotateTowardsU3Ec__Iterator3_t1999157622::get_offset_of_U3CmaxTU3E__1_4(),
	U3CRotateTowardsU3Ec__Iterator3_t1999157622::get_offset_of_U24this_5(),
	U3CRotateTowardsU3Ec__Iterator3_t1999157622::get_offset_of_U24current_6(),
	U3CRotateTowardsU3Ec__Iterator3_t1999157622::get_offset_of_U24disposing_7(),
	U3CRotateTowardsU3Ec__Iterator3_t1999157622::get_offset_of_U24PC_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2329 = { sizeof (U3CMoveTowardsU3Ec__Iterator4_t3738932793), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2329[8] = 
{
	U3CMoveTowardsU3Ec__Iterator4_t3738932793::get_offset_of_moveTarget_0(),
	U3CMoveTowardsU3Ec__Iterator4_t3738932793::get_offset_of_U3CdistanceToTargetU3E__0_1(),
	U3CMoveTowardsU3Ec__Iterator4_t3738932793::get_offset_of_distanceThreshold_2(),
	U3CMoveTowardsU3Ec__Iterator4_t3738932793::get_offset_of_U3CdirectionU3E__1_3(),
	U3CMoveTowardsU3Ec__Iterator4_t3738932793::get_offset_of_U24this_4(),
	U3CMoveTowardsU3Ec__Iterator4_t3738932793::get_offset_of_U24current_5(),
	U3CMoveTowardsU3Ec__Iterator4_t3738932793::get_offset_of_U24disposing_6(),
	U3CMoveTowardsU3Ec__Iterator4_t3738932793::get_offset_of_U24PC_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2330 = { sizeof (U3CSetWalkingSpeedCoroutineU3Ec__Iterator5_t798448162), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2330[8] = 
{
	U3CSetWalkingSpeedCoroutineU3Ec__Iterator5_t798448162::get_offset_of_U3CstartingSpeedU3E__0_0(),
	U3CSetWalkingSpeedCoroutineU3Ec__Iterator5_t798448162::get_offset_of_U3CcurrentTimeU3E__0_1(),
	U3CSetWalkingSpeedCoroutineU3Ec__Iterator5_t798448162::get_offset_of_transitionTime_2(),
	U3CSetWalkingSpeedCoroutineU3Ec__Iterator5_t798448162::get_offset_of_walkingSpeed_3(),
	U3CSetWalkingSpeedCoroutineU3Ec__Iterator5_t798448162::get_offset_of_U24this_4(),
	U3CSetWalkingSpeedCoroutineU3Ec__Iterator5_t798448162::get_offset_of_U24current_5(),
	U3CSetWalkingSpeedCoroutineU3Ec__Iterator5_t798448162::get_offset_of_U24disposing_6(),
	U3CSetWalkingSpeedCoroutineU3Ec__Iterator5_t798448162::get_offset_of_U24PC_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2331 = { sizeof (MultipleTargetsController_t3623068400), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2331[1] = 
{
	MultipleTargetsController_t3623068400::get_offset_of__visibleDinosaurs_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2332 = { sizeof (MultipleTrackersController_t1631955839), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2332[5] = 
{
	MultipleTrackersController_t1631955839::get_offset_of_CarTracker_2(),
	MultipleTrackersController_t1631955839::get_offset_of_MagazineTracker_3(),
	MultipleTrackersController_t1631955839::get_offset_of_CarInstructions_4(),
	MultipleTrackersController_t1631955839::get_offset_of_MagazineInstructions_5(),
	MultipleTrackersController_t1631955839::get_offset_of__waitingForTrackerToLoad_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2333 = { sizeof (ObjectTrackingController_t3583561264), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2333[7] = 
{
	0,
	0,
	0,
	0,
	0,
	ObjectTrackingController_t3583561264::get_offset_of__isInstructionsAnimationPlaying_7(),
	ObjectTrackingController_t3583561264::get_offset_of__isSirenAnimationPlaying_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2334 = { sizeof (PluginController_t2790031014), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2334[3] = 
{
	PluginController_t2790031014::get_offset_of_ResultText_2(),
	PluginController_t2790031014::get_offset_of__plugin_3(),
	PluginController_t2790031014::get_offset_of__initialized_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2335 = { sizeof (RuntimeTrackerController_t400852104), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2335[5] = 
{
	RuntimeTrackerController_t400852104::get_offset_of_Url_2(),
	RuntimeTrackerController_t400852104::get_offset_of_TrackablePrefab_3(),
	RuntimeTrackerController_t400852104::get_offset_of_CarInstructions_4(),
	RuntimeTrackerController_t400852104::get_offset_of__currentTracker_5(),
	RuntimeTrackerController_t400852104::get_offset_of__isLoadingTracker_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2336 = { sizeof (SampleController_t2879308770), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2337 = { sizeof (SingleRecognitionController_t1501371400), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2337[4] = 
{
	SingleRecognitionController_t1501371400::get_offset_of_Tracker_2(),
	SingleRecognitionController_t1501371400::get_offset_of_InfoText_3(),
	SingleRecognitionController_t1501371400::get_offset_of_RecognizeButton_4(),
	SingleRecognitionController_t1501371400::get_offset_of_ButtonText_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2338 = { sizeof (descriptionCloseManager_t1451768383), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2338[3] = 
{
	descriptionCloseManager_t1451768383::get_offset_of_description_2(),
	descriptionCloseManager_t1451768383::get_offset_of_verticalLayoutGroup_3(),
	descriptionCloseManager_t1451768383::get_offset_of_LeftNav_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2339 = { sizeof (descriptionmanager_t4101791333), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2339[4] = 
{
	descriptionmanager_t4101791333::get_offset_of_targetTitle_2(),
	descriptionmanager_t4101791333::get_offset_of_targetDescription_3(),
	descriptionmanager_t4101791333::get_offset_of_sourceTitle_4(),
	descriptionmanager_t4101791333::get_offset_of_sourceDescription_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2340 = { sizeof (landscapeTransformManager_t1971019274), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2340[4] = 
{
	landscapeTransformManager_t1971019274::get_offset_of_rectTransform_2(),
	landscapeTransformManager_t1971019274::get_offset_of_offsetMin_3(),
	landscapeTransformManager_t1971019274::get_offset_of_offsetMax_4(),
	landscapeTransformManager_t1971019274::get_offset_of_target_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2341 = { sizeof (layoutClickManager_t3446951142), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2341[1] = 
{
	layoutClickManager_t3446951142::get_offset_of_leftNav_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2342 = { sizeof (Loading_t2310272025), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2342[6] = 
{
	Loading_t2310272025::get_offset_of_rectComponent_2(),
	Loading_t2310272025::get_offset_of_imageComp_3(),
	Loading_t2310272025::get_offset_of_up_4(),
	Loading_t2310272025::get_offset_of_rotateSpeed_5(),
	Loading_t2310272025::get_offset_of_openSpeed_6(),
	Loading_t2310272025::get_offset_of_closeSpeed_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2343 = { sizeof (SimpleLoading_t205988226), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2343[3] = 
{
	SimpleLoading_t205988226::get_offset_of_rectComponent_2(),
	SimpleLoading_t205988226::get_offset_of_imageComp_3(),
	SimpleLoading_t205988226::get_offset_of_rotateSpeed_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2344 = { sizeof (loadingbar_t3045447973), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2344[3] = 
{
	loadingbar_t3045447973::get_offset_of_rectComponent_2(),
	loadingbar_t3045447973::get_offset_of_imageComp_3(),
	loadingbar_t3045447973::get_offset_of_speed_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2345 = { sizeof (loadingcolorful_t4083442456), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2345[3] = 
{
	loadingcolorful_t4083442456::get_offset_of_rectComponent_2(),
	loadingcolorful_t4083442456::get_offset_of_imageComp_3(),
	loadingcolorful_t4083442456::get_offset_of_speed_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2346 = { sizeof (loadingtext_t2909752294), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2346[5] = 
{
	loadingtext_t2909752294::get_offset_of_rectComponent_2(),
	loadingtext_t2909752294::get_offset_of_imageComp_3(),
	loadingtext_t2909752294::get_offset_of_speed_4(),
	loadingtext_t2909752294::get_offset_of_text_5(),
	loadingtext_t2909752294::get_offset_of_textNormal_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2347 = { sizeof (rotatetotate_t3724567416), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2347[4] = 
{
	rotatetotate_t3724567416::get_offset_of_rectComponent_2(),
	rotatetotate_t3724567416::get_offset_of_imageComp_3(),
	rotatetotate_t3724567416::get_offset_of_up_4(),
	rotatetotate_t3724567416::get_offset_of_rotateSpeed_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2348 = { sizeof (simplerotate_t361467861), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2348[4] = 
{
	simplerotate_t361467861::get_offset_of_rectComponent_2(),
	simplerotate_t361467861::get_offset_of_imageComp_3(),
	simplerotate_t361467861::get_offset_of_rotateSpeed_4(),
	simplerotate_t361467861::get_offset_of_currentvalue_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2349 = { sizeof (lookAtCamera_t384050562), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2349[1] = 
{
	lookAtCamera_t384050562::get_offset_of_target_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2350 = { sizeof (U3CPrivateImplementationDetailsU3E_t3057255369), -1, sizeof(U3CPrivateImplementationDetailsU3E_t3057255369_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2350[5] = 
{
	U3CPrivateImplementationDetailsU3E_t3057255369_StaticFields::get_offset_of_U24fieldU2DD3F8EE438CC4A24A7794DA2CFD1C8099BB50DF7B_0(),
	U3CPrivateImplementationDetailsU3E_t3057255369_StaticFields::get_offset_of_U24fieldU2DD672C98E25086D1ED459CD4D367DECA22DF62D73_1(),
	U3CPrivateImplementationDetailsU3E_t3057255369_StaticFields::get_offset_of_U24fieldU2D23856485AFE3E3A5EF3795D75EBC0D059E42799C_2(),
	U3CPrivateImplementationDetailsU3E_t3057255369_StaticFields::get_offset_of_U24fieldU2D60A341F3CDF7A37F49437E5072E71879F279C32A_3(),
	U3CPrivateImplementationDetailsU3E_t3057255369_StaticFields::get_offset_of_U24fieldU2D87137E630C3837BC71D4E9D0A4EB1E7CF85C9909_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2351 = { sizeof (U24ArrayTypeU3D16_t3253128244)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU3D16_t3253128244 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2352 = { sizeof (U24ArrayTypeU3D36_t120960362)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU3D36_t120960362 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2353 = { sizeof (U24ArrayTypeU3D12_t2488454198)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU3D12_t2488454198 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2354 = { sizeof (U3CModuleU3E_t692745535), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2355 = { sizeof (JSHighlighterController_t967495899), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2355[1] = 
{
	JSHighlighterController_t967495899::get_offset_of_h_2(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
