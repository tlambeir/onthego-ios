﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour3962482529.h"

// ToggleController
struct ToggleController_t3012097607;
// UnityEngine.UI.Toggle
struct Toggle_t2735377061;
// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ToggleManager
struct  ToggleManager_t268657515  : public MonoBehaviour_t3962482529
{
public:
	// ToggleController ToggleManager::toggleController
	ToggleController_t3012097607 * ___toggleController_2;
	// UnityEngine.UI.Toggle ToggleManager::toggle
	Toggle_t2735377061 * ___toggle_3;
	// System.String ToggleManager::type
	String_t* ___type_4;

public:
	inline static int32_t get_offset_of_toggleController_2() { return static_cast<int32_t>(offsetof(ToggleManager_t268657515, ___toggleController_2)); }
	inline ToggleController_t3012097607 * get_toggleController_2() const { return ___toggleController_2; }
	inline ToggleController_t3012097607 ** get_address_of_toggleController_2() { return &___toggleController_2; }
	inline void set_toggleController_2(ToggleController_t3012097607 * value)
	{
		___toggleController_2 = value;
		Il2CppCodeGenWriteBarrier(&___toggleController_2, value);
	}

	inline static int32_t get_offset_of_toggle_3() { return static_cast<int32_t>(offsetof(ToggleManager_t268657515, ___toggle_3)); }
	inline Toggle_t2735377061 * get_toggle_3() const { return ___toggle_3; }
	inline Toggle_t2735377061 ** get_address_of_toggle_3() { return &___toggle_3; }
	inline void set_toggle_3(Toggle_t2735377061 * value)
	{
		___toggle_3 = value;
		Il2CppCodeGenWriteBarrier(&___toggle_3, value);
	}

	inline static int32_t get_offset_of_type_4() { return static_cast<int32_t>(offsetof(ToggleManager_t268657515, ___type_4)); }
	inline String_t* get_type_4() const { return ___type_4; }
	inline String_t** get_address_of_type_4() { return &___type_4; }
	inline void set_type_4(String_t* value)
	{
		___type_4 = value;
		Il2CppCodeGenWriteBarrier(&___type_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
