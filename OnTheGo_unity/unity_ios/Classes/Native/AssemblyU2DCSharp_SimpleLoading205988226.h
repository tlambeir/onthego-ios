﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour3962482529.h"

// UnityEngine.RectTransform
struct RectTransform_t3704657025;
// UnityEngine.UI.Image
struct Image_t2670269651;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SimpleLoading
struct  SimpleLoading_t205988226  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.RectTransform SimpleLoading::rectComponent
	RectTransform_t3704657025 * ___rectComponent_2;
	// UnityEngine.UI.Image SimpleLoading::imageComp
	Image_t2670269651 * ___imageComp_3;
	// System.Single SimpleLoading::rotateSpeed
	float ___rotateSpeed_4;

public:
	inline static int32_t get_offset_of_rectComponent_2() { return static_cast<int32_t>(offsetof(SimpleLoading_t205988226, ___rectComponent_2)); }
	inline RectTransform_t3704657025 * get_rectComponent_2() const { return ___rectComponent_2; }
	inline RectTransform_t3704657025 ** get_address_of_rectComponent_2() { return &___rectComponent_2; }
	inline void set_rectComponent_2(RectTransform_t3704657025 * value)
	{
		___rectComponent_2 = value;
		Il2CppCodeGenWriteBarrier(&___rectComponent_2, value);
	}

	inline static int32_t get_offset_of_imageComp_3() { return static_cast<int32_t>(offsetof(SimpleLoading_t205988226, ___imageComp_3)); }
	inline Image_t2670269651 * get_imageComp_3() const { return ___imageComp_3; }
	inline Image_t2670269651 ** get_address_of_imageComp_3() { return &___imageComp_3; }
	inline void set_imageComp_3(Image_t2670269651 * value)
	{
		___imageComp_3 = value;
		Il2CppCodeGenWriteBarrier(&___imageComp_3, value);
	}

	inline static int32_t get_offset_of_rotateSpeed_4() { return static_cast<int32_t>(offsetof(SimpleLoading_t205988226, ___rotateSpeed_4)); }
	inline float get_rotateSpeed_4() const { return ___rotateSpeed_4; }
	inline float* get_address_of_rotateSpeed_4() { return &___rotateSpeed_4; }
	inline void set_rotateSpeed_4(float value)
	{
		___rotateSpeed_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
