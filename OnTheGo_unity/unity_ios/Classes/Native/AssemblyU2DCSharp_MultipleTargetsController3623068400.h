﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_SampleController2879308770.h"

// System.Collections.Generic.HashSet`1<Dinosaur>
struct HashSet_1_t4161973896;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MultipleTargetsController
struct  MultipleTargetsController_t3623068400  : public SampleController_t2879308770
{
public:
	// System.Collections.Generic.HashSet`1<Dinosaur> MultipleTargetsController::_visibleDinosaurs
	HashSet_1_t4161973896 * ____visibleDinosaurs_2;

public:
	inline static int32_t get_offset_of__visibleDinosaurs_2() { return static_cast<int32_t>(offsetof(MultipleTargetsController_t3623068400, ____visibleDinosaurs_2)); }
	inline HashSet_1_t4161973896 * get__visibleDinosaurs_2() const { return ____visibleDinosaurs_2; }
	inline HashSet_1_t4161973896 ** get_address_of__visibleDinosaurs_2() { return &____visibleDinosaurs_2; }
	inline void set__visibleDinosaurs_2(HashSet_1_t4161973896 * value)
	{
		____visibleDinosaurs_2 = value;
		Il2CppCodeGenWriteBarrier(&____visibleDinosaurs_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
