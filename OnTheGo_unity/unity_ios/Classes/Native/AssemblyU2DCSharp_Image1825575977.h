﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour3962482529.h"

// UnityEngine.Texture2D
struct Texture2D_t3840446185;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Image
struct  Image_t1825575977  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Texture2D Image::image
	Texture2D_t3840446185 * ___image_2;

public:
	inline static int32_t get_offset_of_image_2() { return static_cast<int32_t>(offsetof(Image_t1825575977, ___image_2)); }
	inline Texture2D_t3840446185 * get_image_2() const { return ___image_2; }
	inline Texture2D_t3840446185 ** get_address_of_image_2() { return &___image_2; }
	inline void set_image_2(Texture2D_t3840446185 * value)
	{
		___image_2 = value;
		Il2CppCodeGenWriteBarrier(&___image_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
