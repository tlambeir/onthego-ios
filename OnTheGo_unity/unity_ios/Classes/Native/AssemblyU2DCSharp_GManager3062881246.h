﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour3962482529.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GManager
struct  GManager_t3062881246  : public MonoBehaviour_t3962482529
{
public:
	// System.Int32 GManager::Chapter
	int32_t ___Chapter_2;
	// System.Boolean GManager::Locking_state
	bool ___Locking_state_3;

public:
	inline static int32_t get_offset_of_Chapter_2() { return static_cast<int32_t>(offsetof(GManager_t3062881246, ___Chapter_2)); }
	inline int32_t get_Chapter_2() const { return ___Chapter_2; }
	inline int32_t* get_address_of_Chapter_2() { return &___Chapter_2; }
	inline void set_Chapter_2(int32_t value)
	{
		___Chapter_2 = value;
	}

	inline static int32_t get_offset_of_Locking_state_3() { return static_cast<int32_t>(offsetof(GManager_t3062881246, ___Locking_state_3)); }
	inline bool get_Locking_state_3() const { return ___Locking_state_3; }
	inline bool* get_address_of_Locking_state_3() { return &___Locking_state_3; }
	inline void set_Locking_state_3(bool value)
	{
		___Locking_state_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
