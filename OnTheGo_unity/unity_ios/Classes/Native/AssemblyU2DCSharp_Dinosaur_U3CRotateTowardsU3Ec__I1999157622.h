﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"
#include "UnityEngine_UnityEngine_Quaternion2301928331.h"

// UnityEngine.Transform
struct Transform_t3600365921;
// Dinosaur
struct Dinosaur_t1302057126;
// System.Object
struct Il2CppObject;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Dinosaur/<RotateTowards>c__Iterator3
struct  U3CRotateTowardsU3Ec__Iterator3_t1999157622  : public Il2CppObject
{
public:
	// UnityEngine.Transform Dinosaur/<RotateTowards>c__Iterator3::rotationTarget
	Transform_t3600365921 * ___rotationTarget_0;
	// UnityEngine.Quaternion Dinosaur/<RotateTowards>c__Iterator3::<targetRotation>__0
	Quaternion_t2301928331  ___U3CtargetRotationU3E__0_1;
	// System.Single Dinosaur/<RotateTowards>c__Iterator3::<angleToTarget>__0
	float ___U3CangleToTargetU3E__0_2;
	// System.Single Dinosaur/<RotateTowards>c__Iterator3::<maxAngle>__1
	float ___U3CmaxAngleU3E__1_3;
	// System.Single Dinosaur/<RotateTowards>c__Iterator3::<maxT>__1
	float ___U3CmaxTU3E__1_4;
	// Dinosaur Dinosaur/<RotateTowards>c__Iterator3::$this
	Dinosaur_t1302057126 * ___U24this_5;
	// System.Object Dinosaur/<RotateTowards>c__Iterator3::$current
	Il2CppObject * ___U24current_6;
	// System.Boolean Dinosaur/<RotateTowards>c__Iterator3::$disposing
	bool ___U24disposing_7;
	// System.Int32 Dinosaur/<RotateTowards>c__Iterator3::$PC
	int32_t ___U24PC_8;

public:
	inline static int32_t get_offset_of_rotationTarget_0() { return static_cast<int32_t>(offsetof(U3CRotateTowardsU3Ec__Iterator3_t1999157622, ___rotationTarget_0)); }
	inline Transform_t3600365921 * get_rotationTarget_0() const { return ___rotationTarget_0; }
	inline Transform_t3600365921 ** get_address_of_rotationTarget_0() { return &___rotationTarget_0; }
	inline void set_rotationTarget_0(Transform_t3600365921 * value)
	{
		___rotationTarget_0 = value;
		Il2CppCodeGenWriteBarrier(&___rotationTarget_0, value);
	}

	inline static int32_t get_offset_of_U3CtargetRotationU3E__0_1() { return static_cast<int32_t>(offsetof(U3CRotateTowardsU3Ec__Iterator3_t1999157622, ___U3CtargetRotationU3E__0_1)); }
	inline Quaternion_t2301928331  get_U3CtargetRotationU3E__0_1() const { return ___U3CtargetRotationU3E__0_1; }
	inline Quaternion_t2301928331 * get_address_of_U3CtargetRotationU3E__0_1() { return &___U3CtargetRotationU3E__0_1; }
	inline void set_U3CtargetRotationU3E__0_1(Quaternion_t2301928331  value)
	{
		___U3CtargetRotationU3E__0_1 = value;
	}

	inline static int32_t get_offset_of_U3CangleToTargetU3E__0_2() { return static_cast<int32_t>(offsetof(U3CRotateTowardsU3Ec__Iterator3_t1999157622, ___U3CangleToTargetU3E__0_2)); }
	inline float get_U3CangleToTargetU3E__0_2() const { return ___U3CangleToTargetU3E__0_2; }
	inline float* get_address_of_U3CangleToTargetU3E__0_2() { return &___U3CangleToTargetU3E__0_2; }
	inline void set_U3CangleToTargetU3E__0_2(float value)
	{
		___U3CangleToTargetU3E__0_2 = value;
	}

	inline static int32_t get_offset_of_U3CmaxAngleU3E__1_3() { return static_cast<int32_t>(offsetof(U3CRotateTowardsU3Ec__Iterator3_t1999157622, ___U3CmaxAngleU3E__1_3)); }
	inline float get_U3CmaxAngleU3E__1_3() const { return ___U3CmaxAngleU3E__1_3; }
	inline float* get_address_of_U3CmaxAngleU3E__1_3() { return &___U3CmaxAngleU3E__1_3; }
	inline void set_U3CmaxAngleU3E__1_3(float value)
	{
		___U3CmaxAngleU3E__1_3 = value;
	}

	inline static int32_t get_offset_of_U3CmaxTU3E__1_4() { return static_cast<int32_t>(offsetof(U3CRotateTowardsU3Ec__Iterator3_t1999157622, ___U3CmaxTU3E__1_4)); }
	inline float get_U3CmaxTU3E__1_4() const { return ___U3CmaxTU3E__1_4; }
	inline float* get_address_of_U3CmaxTU3E__1_4() { return &___U3CmaxTU3E__1_4; }
	inline void set_U3CmaxTU3E__1_4(float value)
	{
		___U3CmaxTU3E__1_4 = value;
	}

	inline static int32_t get_offset_of_U24this_5() { return static_cast<int32_t>(offsetof(U3CRotateTowardsU3Ec__Iterator3_t1999157622, ___U24this_5)); }
	inline Dinosaur_t1302057126 * get_U24this_5() const { return ___U24this_5; }
	inline Dinosaur_t1302057126 ** get_address_of_U24this_5() { return &___U24this_5; }
	inline void set_U24this_5(Dinosaur_t1302057126 * value)
	{
		___U24this_5 = value;
		Il2CppCodeGenWriteBarrier(&___U24this_5, value);
	}

	inline static int32_t get_offset_of_U24current_6() { return static_cast<int32_t>(offsetof(U3CRotateTowardsU3Ec__Iterator3_t1999157622, ___U24current_6)); }
	inline Il2CppObject * get_U24current_6() const { return ___U24current_6; }
	inline Il2CppObject ** get_address_of_U24current_6() { return &___U24current_6; }
	inline void set_U24current_6(Il2CppObject * value)
	{
		___U24current_6 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_6, value);
	}

	inline static int32_t get_offset_of_U24disposing_7() { return static_cast<int32_t>(offsetof(U3CRotateTowardsU3Ec__Iterator3_t1999157622, ___U24disposing_7)); }
	inline bool get_U24disposing_7() const { return ___U24disposing_7; }
	inline bool* get_address_of_U24disposing_7() { return &___U24disposing_7; }
	inline void set_U24disposing_7(bool value)
	{
		___U24disposing_7 = value;
	}

	inline static int32_t get_offset_of_U24PC_8() { return static_cast<int32_t>(offsetof(U3CRotateTowardsU3Ec__Iterator3_t1999157622, ___U24PC_8)); }
	inline int32_t get_U24PC_8() const { return ___U24PC_8; }
	inline int32_t* get_address_of_U24PC_8() { return &___U24PC_8; }
	inline void set_U24PC_8(int32_t value)
	{
		___U24PC_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
