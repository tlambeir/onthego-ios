﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// Dinosaur
struct Dinosaur_t1302057126;
// System.Object
struct Il2CppObject;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Dinosaur/<SetWalkingSpeedCoroutine>c__Iterator5
struct  U3CSetWalkingSpeedCoroutineU3Ec__Iterator5_t798448162  : public Il2CppObject
{
public:
	// System.Single Dinosaur/<SetWalkingSpeedCoroutine>c__Iterator5::<startingSpeed>__0
	float ___U3CstartingSpeedU3E__0_0;
	// System.Single Dinosaur/<SetWalkingSpeedCoroutine>c__Iterator5::<currentTime>__0
	float ___U3CcurrentTimeU3E__0_1;
	// System.Single Dinosaur/<SetWalkingSpeedCoroutine>c__Iterator5::transitionTime
	float ___transitionTime_2;
	// System.Single Dinosaur/<SetWalkingSpeedCoroutine>c__Iterator5::walkingSpeed
	float ___walkingSpeed_3;
	// Dinosaur Dinosaur/<SetWalkingSpeedCoroutine>c__Iterator5::$this
	Dinosaur_t1302057126 * ___U24this_4;
	// System.Object Dinosaur/<SetWalkingSpeedCoroutine>c__Iterator5::$current
	Il2CppObject * ___U24current_5;
	// System.Boolean Dinosaur/<SetWalkingSpeedCoroutine>c__Iterator5::$disposing
	bool ___U24disposing_6;
	// System.Int32 Dinosaur/<SetWalkingSpeedCoroutine>c__Iterator5::$PC
	int32_t ___U24PC_7;

public:
	inline static int32_t get_offset_of_U3CstartingSpeedU3E__0_0() { return static_cast<int32_t>(offsetof(U3CSetWalkingSpeedCoroutineU3Ec__Iterator5_t798448162, ___U3CstartingSpeedU3E__0_0)); }
	inline float get_U3CstartingSpeedU3E__0_0() const { return ___U3CstartingSpeedU3E__0_0; }
	inline float* get_address_of_U3CstartingSpeedU3E__0_0() { return &___U3CstartingSpeedU3E__0_0; }
	inline void set_U3CstartingSpeedU3E__0_0(float value)
	{
		___U3CstartingSpeedU3E__0_0 = value;
	}

	inline static int32_t get_offset_of_U3CcurrentTimeU3E__0_1() { return static_cast<int32_t>(offsetof(U3CSetWalkingSpeedCoroutineU3Ec__Iterator5_t798448162, ___U3CcurrentTimeU3E__0_1)); }
	inline float get_U3CcurrentTimeU3E__0_1() const { return ___U3CcurrentTimeU3E__0_1; }
	inline float* get_address_of_U3CcurrentTimeU3E__0_1() { return &___U3CcurrentTimeU3E__0_1; }
	inline void set_U3CcurrentTimeU3E__0_1(float value)
	{
		___U3CcurrentTimeU3E__0_1 = value;
	}

	inline static int32_t get_offset_of_transitionTime_2() { return static_cast<int32_t>(offsetof(U3CSetWalkingSpeedCoroutineU3Ec__Iterator5_t798448162, ___transitionTime_2)); }
	inline float get_transitionTime_2() const { return ___transitionTime_2; }
	inline float* get_address_of_transitionTime_2() { return &___transitionTime_2; }
	inline void set_transitionTime_2(float value)
	{
		___transitionTime_2 = value;
	}

	inline static int32_t get_offset_of_walkingSpeed_3() { return static_cast<int32_t>(offsetof(U3CSetWalkingSpeedCoroutineU3Ec__Iterator5_t798448162, ___walkingSpeed_3)); }
	inline float get_walkingSpeed_3() const { return ___walkingSpeed_3; }
	inline float* get_address_of_walkingSpeed_3() { return &___walkingSpeed_3; }
	inline void set_walkingSpeed_3(float value)
	{
		___walkingSpeed_3 = value;
	}

	inline static int32_t get_offset_of_U24this_4() { return static_cast<int32_t>(offsetof(U3CSetWalkingSpeedCoroutineU3Ec__Iterator5_t798448162, ___U24this_4)); }
	inline Dinosaur_t1302057126 * get_U24this_4() const { return ___U24this_4; }
	inline Dinosaur_t1302057126 ** get_address_of_U24this_4() { return &___U24this_4; }
	inline void set_U24this_4(Dinosaur_t1302057126 * value)
	{
		___U24this_4 = value;
		Il2CppCodeGenWriteBarrier(&___U24this_4, value);
	}

	inline static int32_t get_offset_of_U24current_5() { return static_cast<int32_t>(offsetof(U3CSetWalkingSpeedCoroutineU3Ec__Iterator5_t798448162, ___U24current_5)); }
	inline Il2CppObject * get_U24current_5() const { return ___U24current_5; }
	inline Il2CppObject ** get_address_of_U24current_5() { return &___U24current_5; }
	inline void set_U24current_5(Il2CppObject * value)
	{
		___U24current_5 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_5, value);
	}

	inline static int32_t get_offset_of_U24disposing_6() { return static_cast<int32_t>(offsetof(U3CSetWalkingSpeedCoroutineU3Ec__Iterator5_t798448162, ___U24disposing_6)); }
	inline bool get_U24disposing_6() const { return ___U24disposing_6; }
	inline bool* get_address_of_U24disposing_6() { return &___U24disposing_6; }
	inline void set_U24disposing_6(bool value)
	{
		___U24disposing_6 = value;
	}

	inline static int32_t get_offset_of_U24PC_7() { return static_cast<int32_t>(offsetof(U3CSetWalkingSpeedCoroutineU3Ec__Iterator5_t798448162, ___U24PC_7)); }
	inline int32_t get_U24PC_7() const { return ___U24PC_7; }
	inline int32_t* get_address_of_U24PC_7() { return &___U24PC_7; }
	inline void set_U24PC_7(int32_t value)
	{
		___U24PC_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
