﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour3962482529.h"

// System.String
struct String_t;
// System.Text.RegularExpressions.Regex
struct Regex_t3657309853;
// System.String[]
struct StringU5BU5D_t1281789340;
// Wikitude.TrackableBehaviour/OnEnterFieldOfVisionEvent
struct OnEnterFieldOfVisionEvent_t2975486640;
// Wikitude.TrackableBehaviour/OnExitFieldOfVisionEvent
struct OnExitFieldOfVisionEvent_t2776302756;
// UnityEngine.Texture2D
struct Texture2D_t3840446185;
// UnityEngine.Material
struct Material_t340375123;
// UnityEngine.Mesh
struct Mesh_t3648964284;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Wikitude.TrackableBehaviour
struct  TrackableBehaviour_t2469814643  : public MonoBehaviour_t3962482529
{
public:
	// System.String Wikitude.TrackableBehaviour::_targetPattern
	String_t* ____targetPattern_2;
	// System.Text.RegularExpressions.Regex Wikitude.TrackableBehaviour::_targetPatternRegex
	Regex_t3657309853 * ____targetPatternRegex_3;
	// System.Boolean Wikitude.TrackableBehaviour::_isKnown
	bool ____isKnown_4;
	// System.String Wikitude.TrackableBehaviour::_currentTargetName
	String_t* ____currentTargetName_5;
	// System.Boolean Wikitude.TrackableBehaviour::_extendedTracking
	bool ____extendedTracking_6;
	// System.String[] Wikitude.TrackableBehaviour::_targetsForExtendedTracking
	StringU5BU5D_t1281789340* ____targetsForExtendedTracking_7;
	// System.Boolean Wikitude.TrackableBehaviour::_autoToggleVisibility
	bool ____autoToggleVisibility_8;
	// Wikitude.TrackableBehaviour/OnEnterFieldOfVisionEvent Wikitude.TrackableBehaviour::OnEnterFieldOfVision
	OnEnterFieldOfVisionEvent_t2975486640 * ___OnEnterFieldOfVision_9;
	// Wikitude.TrackableBehaviour/OnExitFieldOfVisionEvent Wikitude.TrackableBehaviour::OnExitFieldOfVision
	OnExitFieldOfVisionEvent_t2776302756 * ___OnExitFieldOfVision_10;
	// System.Boolean Wikitude.TrackableBehaviour::_eventsFoldout
	bool ____eventsFoldout_11;
	// System.Boolean Wikitude.TrackableBehaviour::_registeredToTracker
	bool ____registeredToTracker_12;
	// UnityEngine.Texture2D Wikitude.TrackableBehaviour::_preview
	Texture2D_t3840446185 * ____preview_13;
	// UnityEngine.Material Wikitude.TrackableBehaviour::_previewMaterial
	Material_t340375123 * ____previewMaterial_14;
	// UnityEngine.Mesh Wikitude.TrackableBehaviour::_previewMesh
	Mesh_t3648964284 * ____previewMesh_15;

public:
	inline static int32_t get_offset_of__targetPattern_2() { return static_cast<int32_t>(offsetof(TrackableBehaviour_t2469814643, ____targetPattern_2)); }
	inline String_t* get__targetPattern_2() const { return ____targetPattern_2; }
	inline String_t** get_address_of__targetPattern_2() { return &____targetPattern_2; }
	inline void set__targetPattern_2(String_t* value)
	{
		____targetPattern_2 = value;
		Il2CppCodeGenWriteBarrier(&____targetPattern_2, value);
	}

	inline static int32_t get_offset_of__targetPatternRegex_3() { return static_cast<int32_t>(offsetof(TrackableBehaviour_t2469814643, ____targetPatternRegex_3)); }
	inline Regex_t3657309853 * get__targetPatternRegex_3() const { return ____targetPatternRegex_3; }
	inline Regex_t3657309853 ** get_address_of__targetPatternRegex_3() { return &____targetPatternRegex_3; }
	inline void set__targetPatternRegex_3(Regex_t3657309853 * value)
	{
		____targetPatternRegex_3 = value;
		Il2CppCodeGenWriteBarrier(&____targetPatternRegex_3, value);
	}

	inline static int32_t get_offset_of__isKnown_4() { return static_cast<int32_t>(offsetof(TrackableBehaviour_t2469814643, ____isKnown_4)); }
	inline bool get__isKnown_4() const { return ____isKnown_4; }
	inline bool* get_address_of__isKnown_4() { return &____isKnown_4; }
	inline void set__isKnown_4(bool value)
	{
		____isKnown_4 = value;
	}

	inline static int32_t get_offset_of__currentTargetName_5() { return static_cast<int32_t>(offsetof(TrackableBehaviour_t2469814643, ____currentTargetName_5)); }
	inline String_t* get__currentTargetName_5() const { return ____currentTargetName_5; }
	inline String_t** get_address_of__currentTargetName_5() { return &____currentTargetName_5; }
	inline void set__currentTargetName_5(String_t* value)
	{
		____currentTargetName_5 = value;
		Il2CppCodeGenWriteBarrier(&____currentTargetName_5, value);
	}

	inline static int32_t get_offset_of__extendedTracking_6() { return static_cast<int32_t>(offsetof(TrackableBehaviour_t2469814643, ____extendedTracking_6)); }
	inline bool get__extendedTracking_6() const { return ____extendedTracking_6; }
	inline bool* get_address_of__extendedTracking_6() { return &____extendedTracking_6; }
	inline void set__extendedTracking_6(bool value)
	{
		____extendedTracking_6 = value;
	}

	inline static int32_t get_offset_of__targetsForExtendedTracking_7() { return static_cast<int32_t>(offsetof(TrackableBehaviour_t2469814643, ____targetsForExtendedTracking_7)); }
	inline StringU5BU5D_t1281789340* get__targetsForExtendedTracking_7() const { return ____targetsForExtendedTracking_7; }
	inline StringU5BU5D_t1281789340** get_address_of__targetsForExtendedTracking_7() { return &____targetsForExtendedTracking_7; }
	inline void set__targetsForExtendedTracking_7(StringU5BU5D_t1281789340* value)
	{
		____targetsForExtendedTracking_7 = value;
		Il2CppCodeGenWriteBarrier(&____targetsForExtendedTracking_7, value);
	}

	inline static int32_t get_offset_of__autoToggleVisibility_8() { return static_cast<int32_t>(offsetof(TrackableBehaviour_t2469814643, ____autoToggleVisibility_8)); }
	inline bool get__autoToggleVisibility_8() const { return ____autoToggleVisibility_8; }
	inline bool* get_address_of__autoToggleVisibility_8() { return &____autoToggleVisibility_8; }
	inline void set__autoToggleVisibility_8(bool value)
	{
		____autoToggleVisibility_8 = value;
	}

	inline static int32_t get_offset_of_OnEnterFieldOfVision_9() { return static_cast<int32_t>(offsetof(TrackableBehaviour_t2469814643, ___OnEnterFieldOfVision_9)); }
	inline OnEnterFieldOfVisionEvent_t2975486640 * get_OnEnterFieldOfVision_9() const { return ___OnEnterFieldOfVision_9; }
	inline OnEnterFieldOfVisionEvent_t2975486640 ** get_address_of_OnEnterFieldOfVision_9() { return &___OnEnterFieldOfVision_9; }
	inline void set_OnEnterFieldOfVision_9(OnEnterFieldOfVisionEvent_t2975486640 * value)
	{
		___OnEnterFieldOfVision_9 = value;
		Il2CppCodeGenWriteBarrier(&___OnEnterFieldOfVision_9, value);
	}

	inline static int32_t get_offset_of_OnExitFieldOfVision_10() { return static_cast<int32_t>(offsetof(TrackableBehaviour_t2469814643, ___OnExitFieldOfVision_10)); }
	inline OnExitFieldOfVisionEvent_t2776302756 * get_OnExitFieldOfVision_10() const { return ___OnExitFieldOfVision_10; }
	inline OnExitFieldOfVisionEvent_t2776302756 ** get_address_of_OnExitFieldOfVision_10() { return &___OnExitFieldOfVision_10; }
	inline void set_OnExitFieldOfVision_10(OnExitFieldOfVisionEvent_t2776302756 * value)
	{
		___OnExitFieldOfVision_10 = value;
		Il2CppCodeGenWriteBarrier(&___OnExitFieldOfVision_10, value);
	}

	inline static int32_t get_offset_of__eventsFoldout_11() { return static_cast<int32_t>(offsetof(TrackableBehaviour_t2469814643, ____eventsFoldout_11)); }
	inline bool get__eventsFoldout_11() const { return ____eventsFoldout_11; }
	inline bool* get_address_of__eventsFoldout_11() { return &____eventsFoldout_11; }
	inline void set__eventsFoldout_11(bool value)
	{
		____eventsFoldout_11 = value;
	}

	inline static int32_t get_offset_of__registeredToTracker_12() { return static_cast<int32_t>(offsetof(TrackableBehaviour_t2469814643, ____registeredToTracker_12)); }
	inline bool get__registeredToTracker_12() const { return ____registeredToTracker_12; }
	inline bool* get_address_of__registeredToTracker_12() { return &____registeredToTracker_12; }
	inline void set__registeredToTracker_12(bool value)
	{
		____registeredToTracker_12 = value;
	}

	inline static int32_t get_offset_of__preview_13() { return static_cast<int32_t>(offsetof(TrackableBehaviour_t2469814643, ____preview_13)); }
	inline Texture2D_t3840446185 * get__preview_13() const { return ____preview_13; }
	inline Texture2D_t3840446185 ** get_address_of__preview_13() { return &____preview_13; }
	inline void set__preview_13(Texture2D_t3840446185 * value)
	{
		____preview_13 = value;
		Il2CppCodeGenWriteBarrier(&____preview_13, value);
	}

	inline static int32_t get_offset_of__previewMaterial_14() { return static_cast<int32_t>(offsetof(TrackableBehaviour_t2469814643, ____previewMaterial_14)); }
	inline Material_t340375123 * get__previewMaterial_14() const { return ____previewMaterial_14; }
	inline Material_t340375123 ** get_address_of__previewMaterial_14() { return &____previewMaterial_14; }
	inline void set__previewMaterial_14(Material_t340375123 * value)
	{
		____previewMaterial_14 = value;
		Il2CppCodeGenWriteBarrier(&____previewMaterial_14, value);
	}

	inline static int32_t get_offset_of__previewMesh_15() { return static_cast<int32_t>(offsetof(TrackableBehaviour_t2469814643, ____previewMesh_15)); }
	inline Mesh_t3648964284 * get__previewMesh_15() const { return ____previewMesh_15; }
	inline Mesh_t3648964284 ** get_address_of__previewMesh_15() { return &____previewMesh_15; }
	inline void set__previewMesh_15(Mesh_t3648964284 * value)
	{
		____previewMesh_15 = value;
		Il2CppCodeGenWriteBarrier(&____previewMesh_15, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
