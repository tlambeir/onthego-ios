﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_Lean_Touch_LeanSelectableBehavio3156997792.h"

// UnityEngine.Rigidbody
struct Rigidbody_t3916780224;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Lean.Touch.LeanSelectableTranslateInertia3D
struct  LeanSelectableTranslateInertia3D_t4040704945  : public LeanSelectableBehaviour_t3156997792
{
public:
	// UnityEngine.Rigidbody Lean.Touch.LeanSelectableTranslateInertia3D::body
	Rigidbody_t3916780224 * ___body_3;

public:
	inline static int32_t get_offset_of_body_3() { return static_cast<int32_t>(offsetof(LeanSelectableTranslateInertia3D_t4040704945, ___body_3)); }
	inline Rigidbody_t3916780224 * get_body_3() const { return ___body_3; }
	inline Rigidbody_t3916780224 ** get_address_of_body_3() { return &___body_3; }
	inline void set_body_3(Rigidbody_t3916780224 * value)
	{
		___body_3 = value;
		Il2CppCodeGenWriteBarrier(&___body_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
