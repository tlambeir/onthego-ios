﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour3962482529.h"
#include "UnityEngine_UnityEngine_Color2555686324.h"

// UnityEngine.Sprite
struct Sprite_t280657092;
// UnityEngine.UI.Text
struct Text_t1901882714;
// TMPro.TextMeshProUGUI
struct TextMeshProUGUI_t529313277;
// UnityEngine.GameObject
struct GameObject_t1113636619;
// Branch
struct Branch_t1428165248;
// LeftNav
struct LeftNav_t2375583198;
// UnityEngine.Transform
struct Transform_t3600365921;
// System.Collections.Generic.List`1<UnityEngine.GameObject>
struct List_1_t2585711361;
// Launch_Animation
struct Launch_Animation_t763963988;
// CenterScript
struct CenterScript_t2028411258;
// StepNavigation
struct StepNavigation_t1887272446;
// layoutClickManager
struct layoutClickManager_t3446951142;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MenuController
struct  MenuController_t3930949237  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Sprite MenuController::tutorialSprite
	Sprite_t280657092 * ___tutorialSprite_2;
	// UnityEngine.Sprite MenuController::componentsSprite
	Sprite_t280657092 * ___componentsSprite_3;
	// UnityEngine.Sprite MenuController::backSprite
	Sprite_t280657092 * ___backSprite_4;
	// UnityEngine.UI.Text MenuController::Title
	Text_t1901882714 * ___Title_5;
	// TMPro.TextMeshProUGUI MenuController::Description
	TextMeshProUGUI_t529313277 * ___Description_6;
	// UnityEngine.GameObject MenuController::DescriptionGO
	GameObject_t1113636619 * ___DescriptionGO_7;
	// Branch MenuController::root
	Branch_t1428165248 * ___root_8;
	// UnityEngine.GameObject MenuController::txtBtn
	GameObject_t1113636619 * ___txtBtn_9;
	// UnityEngine.GameObject MenuController::MenuItemDownBtn
	GameObject_t1113636619 * ___MenuItemDownBtn_10;
	// LeftNav MenuController::LeftNav
	LeftNav_t2375583198 * ___LeftNav_11;
	// UnityEngine.Transform MenuController::verticalLayoutGroup
	Transform_t3600365921 * ___verticalLayoutGroup_12;
	// System.Collections.Generic.List`1<UnityEngine.GameObject> MenuController::activeMenuItems
	List_1_t2585711361 * ___activeMenuItems_13;
	// Launch_Animation MenuController::animationLauncher
	Launch_Animation_t763963988 * ___animationLauncher_14;
	// CenterScript MenuController::CenterScript
	CenterScript_t2028411258 * ___CenterScript_15;
	// UnityEngine.GameObject MenuController::labelPreFab
	GameObject_t1113636619 * ___labelPreFab_16;
	// UnityEngine.Color MenuController::highlightColor
	Color_t2555686324  ___highlightColor_17;
	// StepNavigation MenuController::StepNavigation
	StepNavigation_t1887272446 * ___StepNavigation_18;
	// UnityEngine.GameObject MenuController::contentRenderer
	GameObject_t1113636619 * ___contentRenderer_19;
	// TMPro.TextMeshProUGUI MenuController::txtModelName
	TextMeshProUGUI_t529313277 * ___txtModelName_20;
	// UnityEngine.GameObject MenuController::backToTopMenuItem
	GameObject_t1113636619 * ___backToTopMenuItem_21;
	// layoutClickManager MenuController::layoutClickManager
	layoutClickManager_t3446951142 * ___layoutClickManager_22;
	// System.Collections.Generic.List`1<UnityEngine.GameObject> MenuController::hiddenGos
	List_1_t2585711361 * ___hiddenGos_23;
	// System.Boolean MenuController::isDefaultVidInit
	bool ___isDefaultVidInit_24;

public:
	inline static int32_t get_offset_of_tutorialSprite_2() { return static_cast<int32_t>(offsetof(MenuController_t3930949237, ___tutorialSprite_2)); }
	inline Sprite_t280657092 * get_tutorialSprite_2() const { return ___tutorialSprite_2; }
	inline Sprite_t280657092 ** get_address_of_tutorialSprite_2() { return &___tutorialSprite_2; }
	inline void set_tutorialSprite_2(Sprite_t280657092 * value)
	{
		___tutorialSprite_2 = value;
		Il2CppCodeGenWriteBarrier(&___tutorialSprite_2, value);
	}

	inline static int32_t get_offset_of_componentsSprite_3() { return static_cast<int32_t>(offsetof(MenuController_t3930949237, ___componentsSprite_3)); }
	inline Sprite_t280657092 * get_componentsSprite_3() const { return ___componentsSprite_3; }
	inline Sprite_t280657092 ** get_address_of_componentsSprite_3() { return &___componentsSprite_3; }
	inline void set_componentsSprite_3(Sprite_t280657092 * value)
	{
		___componentsSprite_3 = value;
		Il2CppCodeGenWriteBarrier(&___componentsSprite_3, value);
	}

	inline static int32_t get_offset_of_backSprite_4() { return static_cast<int32_t>(offsetof(MenuController_t3930949237, ___backSprite_4)); }
	inline Sprite_t280657092 * get_backSprite_4() const { return ___backSprite_4; }
	inline Sprite_t280657092 ** get_address_of_backSprite_4() { return &___backSprite_4; }
	inline void set_backSprite_4(Sprite_t280657092 * value)
	{
		___backSprite_4 = value;
		Il2CppCodeGenWriteBarrier(&___backSprite_4, value);
	}

	inline static int32_t get_offset_of_Title_5() { return static_cast<int32_t>(offsetof(MenuController_t3930949237, ___Title_5)); }
	inline Text_t1901882714 * get_Title_5() const { return ___Title_5; }
	inline Text_t1901882714 ** get_address_of_Title_5() { return &___Title_5; }
	inline void set_Title_5(Text_t1901882714 * value)
	{
		___Title_5 = value;
		Il2CppCodeGenWriteBarrier(&___Title_5, value);
	}

	inline static int32_t get_offset_of_Description_6() { return static_cast<int32_t>(offsetof(MenuController_t3930949237, ___Description_6)); }
	inline TextMeshProUGUI_t529313277 * get_Description_6() const { return ___Description_6; }
	inline TextMeshProUGUI_t529313277 ** get_address_of_Description_6() { return &___Description_6; }
	inline void set_Description_6(TextMeshProUGUI_t529313277 * value)
	{
		___Description_6 = value;
		Il2CppCodeGenWriteBarrier(&___Description_6, value);
	}

	inline static int32_t get_offset_of_DescriptionGO_7() { return static_cast<int32_t>(offsetof(MenuController_t3930949237, ___DescriptionGO_7)); }
	inline GameObject_t1113636619 * get_DescriptionGO_7() const { return ___DescriptionGO_7; }
	inline GameObject_t1113636619 ** get_address_of_DescriptionGO_7() { return &___DescriptionGO_7; }
	inline void set_DescriptionGO_7(GameObject_t1113636619 * value)
	{
		___DescriptionGO_7 = value;
		Il2CppCodeGenWriteBarrier(&___DescriptionGO_7, value);
	}

	inline static int32_t get_offset_of_root_8() { return static_cast<int32_t>(offsetof(MenuController_t3930949237, ___root_8)); }
	inline Branch_t1428165248 * get_root_8() const { return ___root_8; }
	inline Branch_t1428165248 ** get_address_of_root_8() { return &___root_8; }
	inline void set_root_8(Branch_t1428165248 * value)
	{
		___root_8 = value;
		Il2CppCodeGenWriteBarrier(&___root_8, value);
	}

	inline static int32_t get_offset_of_txtBtn_9() { return static_cast<int32_t>(offsetof(MenuController_t3930949237, ___txtBtn_9)); }
	inline GameObject_t1113636619 * get_txtBtn_9() const { return ___txtBtn_9; }
	inline GameObject_t1113636619 ** get_address_of_txtBtn_9() { return &___txtBtn_9; }
	inline void set_txtBtn_9(GameObject_t1113636619 * value)
	{
		___txtBtn_9 = value;
		Il2CppCodeGenWriteBarrier(&___txtBtn_9, value);
	}

	inline static int32_t get_offset_of_MenuItemDownBtn_10() { return static_cast<int32_t>(offsetof(MenuController_t3930949237, ___MenuItemDownBtn_10)); }
	inline GameObject_t1113636619 * get_MenuItemDownBtn_10() const { return ___MenuItemDownBtn_10; }
	inline GameObject_t1113636619 ** get_address_of_MenuItemDownBtn_10() { return &___MenuItemDownBtn_10; }
	inline void set_MenuItemDownBtn_10(GameObject_t1113636619 * value)
	{
		___MenuItemDownBtn_10 = value;
		Il2CppCodeGenWriteBarrier(&___MenuItemDownBtn_10, value);
	}

	inline static int32_t get_offset_of_LeftNav_11() { return static_cast<int32_t>(offsetof(MenuController_t3930949237, ___LeftNav_11)); }
	inline LeftNav_t2375583198 * get_LeftNav_11() const { return ___LeftNav_11; }
	inline LeftNav_t2375583198 ** get_address_of_LeftNav_11() { return &___LeftNav_11; }
	inline void set_LeftNav_11(LeftNav_t2375583198 * value)
	{
		___LeftNav_11 = value;
		Il2CppCodeGenWriteBarrier(&___LeftNav_11, value);
	}

	inline static int32_t get_offset_of_verticalLayoutGroup_12() { return static_cast<int32_t>(offsetof(MenuController_t3930949237, ___verticalLayoutGroup_12)); }
	inline Transform_t3600365921 * get_verticalLayoutGroup_12() const { return ___verticalLayoutGroup_12; }
	inline Transform_t3600365921 ** get_address_of_verticalLayoutGroup_12() { return &___verticalLayoutGroup_12; }
	inline void set_verticalLayoutGroup_12(Transform_t3600365921 * value)
	{
		___verticalLayoutGroup_12 = value;
		Il2CppCodeGenWriteBarrier(&___verticalLayoutGroup_12, value);
	}

	inline static int32_t get_offset_of_activeMenuItems_13() { return static_cast<int32_t>(offsetof(MenuController_t3930949237, ___activeMenuItems_13)); }
	inline List_1_t2585711361 * get_activeMenuItems_13() const { return ___activeMenuItems_13; }
	inline List_1_t2585711361 ** get_address_of_activeMenuItems_13() { return &___activeMenuItems_13; }
	inline void set_activeMenuItems_13(List_1_t2585711361 * value)
	{
		___activeMenuItems_13 = value;
		Il2CppCodeGenWriteBarrier(&___activeMenuItems_13, value);
	}

	inline static int32_t get_offset_of_animationLauncher_14() { return static_cast<int32_t>(offsetof(MenuController_t3930949237, ___animationLauncher_14)); }
	inline Launch_Animation_t763963988 * get_animationLauncher_14() const { return ___animationLauncher_14; }
	inline Launch_Animation_t763963988 ** get_address_of_animationLauncher_14() { return &___animationLauncher_14; }
	inline void set_animationLauncher_14(Launch_Animation_t763963988 * value)
	{
		___animationLauncher_14 = value;
		Il2CppCodeGenWriteBarrier(&___animationLauncher_14, value);
	}

	inline static int32_t get_offset_of_CenterScript_15() { return static_cast<int32_t>(offsetof(MenuController_t3930949237, ___CenterScript_15)); }
	inline CenterScript_t2028411258 * get_CenterScript_15() const { return ___CenterScript_15; }
	inline CenterScript_t2028411258 ** get_address_of_CenterScript_15() { return &___CenterScript_15; }
	inline void set_CenterScript_15(CenterScript_t2028411258 * value)
	{
		___CenterScript_15 = value;
		Il2CppCodeGenWriteBarrier(&___CenterScript_15, value);
	}

	inline static int32_t get_offset_of_labelPreFab_16() { return static_cast<int32_t>(offsetof(MenuController_t3930949237, ___labelPreFab_16)); }
	inline GameObject_t1113636619 * get_labelPreFab_16() const { return ___labelPreFab_16; }
	inline GameObject_t1113636619 ** get_address_of_labelPreFab_16() { return &___labelPreFab_16; }
	inline void set_labelPreFab_16(GameObject_t1113636619 * value)
	{
		___labelPreFab_16 = value;
		Il2CppCodeGenWriteBarrier(&___labelPreFab_16, value);
	}

	inline static int32_t get_offset_of_highlightColor_17() { return static_cast<int32_t>(offsetof(MenuController_t3930949237, ___highlightColor_17)); }
	inline Color_t2555686324  get_highlightColor_17() const { return ___highlightColor_17; }
	inline Color_t2555686324 * get_address_of_highlightColor_17() { return &___highlightColor_17; }
	inline void set_highlightColor_17(Color_t2555686324  value)
	{
		___highlightColor_17 = value;
	}

	inline static int32_t get_offset_of_StepNavigation_18() { return static_cast<int32_t>(offsetof(MenuController_t3930949237, ___StepNavigation_18)); }
	inline StepNavigation_t1887272446 * get_StepNavigation_18() const { return ___StepNavigation_18; }
	inline StepNavigation_t1887272446 ** get_address_of_StepNavigation_18() { return &___StepNavigation_18; }
	inline void set_StepNavigation_18(StepNavigation_t1887272446 * value)
	{
		___StepNavigation_18 = value;
		Il2CppCodeGenWriteBarrier(&___StepNavigation_18, value);
	}

	inline static int32_t get_offset_of_contentRenderer_19() { return static_cast<int32_t>(offsetof(MenuController_t3930949237, ___contentRenderer_19)); }
	inline GameObject_t1113636619 * get_contentRenderer_19() const { return ___contentRenderer_19; }
	inline GameObject_t1113636619 ** get_address_of_contentRenderer_19() { return &___contentRenderer_19; }
	inline void set_contentRenderer_19(GameObject_t1113636619 * value)
	{
		___contentRenderer_19 = value;
		Il2CppCodeGenWriteBarrier(&___contentRenderer_19, value);
	}

	inline static int32_t get_offset_of_txtModelName_20() { return static_cast<int32_t>(offsetof(MenuController_t3930949237, ___txtModelName_20)); }
	inline TextMeshProUGUI_t529313277 * get_txtModelName_20() const { return ___txtModelName_20; }
	inline TextMeshProUGUI_t529313277 ** get_address_of_txtModelName_20() { return &___txtModelName_20; }
	inline void set_txtModelName_20(TextMeshProUGUI_t529313277 * value)
	{
		___txtModelName_20 = value;
		Il2CppCodeGenWriteBarrier(&___txtModelName_20, value);
	}

	inline static int32_t get_offset_of_backToTopMenuItem_21() { return static_cast<int32_t>(offsetof(MenuController_t3930949237, ___backToTopMenuItem_21)); }
	inline GameObject_t1113636619 * get_backToTopMenuItem_21() const { return ___backToTopMenuItem_21; }
	inline GameObject_t1113636619 ** get_address_of_backToTopMenuItem_21() { return &___backToTopMenuItem_21; }
	inline void set_backToTopMenuItem_21(GameObject_t1113636619 * value)
	{
		___backToTopMenuItem_21 = value;
		Il2CppCodeGenWriteBarrier(&___backToTopMenuItem_21, value);
	}

	inline static int32_t get_offset_of_layoutClickManager_22() { return static_cast<int32_t>(offsetof(MenuController_t3930949237, ___layoutClickManager_22)); }
	inline layoutClickManager_t3446951142 * get_layoutClickManager_22() const { return ___layoutClickManager_22; }
	inline layoutClickManager_t3446951142 ** get_address_of_layoutClickManager_22() { return &___layoutClickManager_22; }
	inline void set_layoutClickManager_22(layoutClickManager_t3446951142 * value)
	{
		___layoutClickManager_22 = value;
		Il2CppCodeGenWriteBarrier(&___layoutClickManager_22, value);
	}

	inline static int32_t get_offset_of_hiddenGos_23() { return static_cast<int32_t>(offsetof(MenuController_t3930949237, ___hiddenGos_23)); }
	inline List_1_t2585711361 * get_hiddenGos_23() const { return ___hiddenGos_23; }
	inline List_1_t2585711361 ** get_address_of_hiddenGos_23() { return &___hiddenGos_23; }
	inline void set_hiddenGos_23(List_1_t2585711361 * value)
	{
		___hiddenGos_23 = value;
		Il2CppCodeGenWriteBarrier(&___hiddenGos_23, value);
	}

	inline static int32_t get_offset_of_isDefaultVidInit_24() { return static_cast<int32_t>(offsetof(MenuController_t3930949237, ___isDefaultVidInit_24)); }
	inline bool get_isDefaultVidInit_24() const { return ___isDefaultVidInit_24; }
	inline bool* get_address_of_isDefaultVidInit_24() { return &___isDefaultVidInit_24; }
	inline void set_isDefaultVidInit_24(bool value)
	{
		___isDefaultVidInit_24 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
