﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "WikitudeUnityPlugin_Wikitude_TargetSource2046190744.h"
#include "WikitudeUnityPlugin_Wikitude_TrackerManager_CloudR2146108836.h"

// System.String
struct String_t;
// Wikitude.CloudRecognitionService/OnInitializedEvent
struct OnInitializedEvent_t1242699385;
// Wikitude.CloudRecognitionService/OnInitializationErrorEvent
struct OnInitializationErrorEvent_t511199451;
// Wikitude.CloudRecognitionService/OnRecognitionResponseEvent
struct OnRecognitionResponseEvent_t3712700845;
// Wikitude.CloudRecognitionService/OnRecognitionErrorEvent
struct OnRecognitionErrorEvent_t2212464566;
// Wikitude.CloudRecognitionService/OnInterruptionEvent
struct OnInterruptionEvent_t1530367979;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Wikitude.CloudRecognitionService
struct  CloudRecognitionService_t3866570042  : public TargetSource_t2046190744
{
public:
	// System.String Wikitude.CloudRecognitionService::_clientToken
	String_t* ____clientToken_2;
	// System.String Wikitude.CloudRecognitionService::_targetCollectionId
	String_t* ____targetCollectionId_3;
	// Wikitude.TrackerManager/CloudRecognitionServerRegion Wikitude.CloudRecognitionService::_serverRegion
	int32_t ____serverRegion_4;
	// System.String Wikitude.CloudRecognitionService::_customServerURL
	String_t* ____customServerURL_5;
	// Wikitude.CloudRecognitionService/OnInitializedEvent Wikitude.CloudRecognitionService::OnInitialized
	OnInitializedEvent_t1242699385 * ___OnInitialized_6;
	// Wikitude.CloudRecognitionService/OnInitializationErrorEvent Wikitude.CloudRecognitionService::OnInitializationError
	OnInitializationErrorEvent_t511199451 * ___OnInitializationError_7;
	// Wikitude.CloudRecognitionService/OnRecognitionResponseEvent Wikitude.CloudRecognitionService::OnRecognitionResponse
	OnRecognitionResponseEvent_t3712700845 * ___OnRecognitionResponse_8;
	// Wikitude.CloudRecognitionService/OnRecognitionErrorEvent Wikitude.CloudRecognitionService::OnRecognitionError
	OnRecognitionErrorEvent_t2212464566 * ___OnRecognitionError_9;
	// Wikitude.CloudRecognitionService/OnInterruptionEvent Wikitude.CloudRecognitionService::OnInterruption
	OnInterruptionEvent_t1530367979 * ___OnInterruption_10;
	// System.Boolean Wikitude.CloudRecognitionService::<IsContinuousRecognitionRunning>k__BackingField
	bool ___U3CIsContinuousRecognitionRunningU3Ek__BackingField_11;

public:
	inline static int32_t get_offset_of__clientToken_2() { return static_cast<int32_t>(offsetof(CloudRecognitionService_t3866570042, ____clientToken_2)); }
	inline String_t* get__clientToken_2() const { return ____clientToken_2; }
	inline String_t** get_address_of__clientToken_2() { return &____clientToken_2; }
	inline void set__clientToken_2(String_t* value)
	{
		____clientToken_2 = value;
		Il2CppCodeGenWriteBarrier(&____clientToken_2, value);
	}

	inline static int32_t get_offset_of__targetCollectionId_3() { return static_cast<int32_t>(offsetof(CloudRecognitionService_t3866570042, ____targetCollectionId_3)); }
	inline String_t* get__targetCollectionId_3() const { return ____targetCollectionId_3; }
	inline String_t** get_address_of__targetCollectionId_3() { return &____targetCollectionId_3; }
	inline void set__targetCollectionId_3(String_t* value)
	{
		____targetCollectionId_3 = value;
		Il2CppCodeGenWriteBarrier(&____targetCollectionId_3, value);
	}

	inline static int32_t get_offset_of__serverRegion_4() { return static_cast<int32_t>(offsetof(CloudRecognitionService_t3866570042, ____serverRegion_4)); }
	inline int32_t get__serverRegion_4() const { return ____serverRegion_4; }
	inline int32_t* get_address_of__serverRegion_4() { return &____serverRegion_4; }
	inline void set__serverRegion_4(int32_t value)
	{
		____serverRegion_4 = value;
	}

	inline static int32_t get_offset_of__customServerURL_5() { return static_cast<int32_t>(offsetof(CloudRecognitionService_t3866570042, ____customServerURL_5)); }
	inline String_t* get__customServerURL_5() const { return ____customServerURL_5; }
	inline String_t** get_address_of__customServerURL_5() { return &____customServerURL_5; }
	inline void set__customServerURL_5(String_t* value)
	{
		____customServerURL_5 = value;
		Il2CppCodeGenWriteBarrier(&____customServerURL_5, value);
	}

	inline static int32_t get_offset_of_OnInitialized_6() { return static_cast<int32_t>(offsetof(CloudRecognitionService_t3866570042, ___OnInitialized_6)); }
	inline OnInitializedEvent_t1242699385 * get_OnInitialized_6() const { return ___OnInitialized_6; }
	inline OnInitializedEvent_t1242699385 ** get_address_of_OnInitialized_6() { return &___OnInitialized_6; }
	inline void set_OnInitialized_6(OnInitializedEvent_t1242699385 * value)
	{
		___OnInitialized_6 = value;
		Il2CppCodeGenWriteBarrier(&___OnInitialized_6, value);
	}

	inline static int32_t get_offset_of_OnInitializationError_7() { return static_cast<int32_t>(offsetof(CloudRecognitionService_t3866570042, ___OnInitializationError_7)); }
	inline OnInitializationErrorEvent_t511199451 * get_OnInitializationError_7() const { return ___OnInitializationError_7; }
	inline OnInitializationErrorEvent_t511199451 ** get_address_of_OnInitializationError_7() { return &___OnInitializationError_7; }
	inline void set_OnInitializationError_7(OnInitializationErrorEvent_t511199451 * value)
	{
		___OnInitializationError_7 = value;
		Il2CppCodeGenWriteBarrier(&___OnInitializationError_7, value);
	}

	inline static int32_t get_offset_of_OnRecognitionResponse_8() { return static_cast<int32_t>(offsetof(CloudRecognitionService_t3866570042, ___OnRecognitionResponse_8)); }
	inline OnRecognitionResponseEvent_t3712700845 * get_OnRecognitionResponse_8() const { return ___OnRecognitionResponse_8; }
	inline OnRecognitionResponseEvent_t3712700845 ** get_address_of_OnRecognitionResponse_8() { return &___OnRecognitionResponse_8; }
	inline void set_OnRecognitionResponse_8(OnRecognitionResponseEvent_t3712700845 * value)
	{
		___OnRecognitionResponse_8 = value;
		Il2CppCodeGenWriteBarrier(&___OnRecognitionResponse_8, value);
	}

	inline static int32_t get_offset_of_OnRecognitionError_9() { return static_cast<int32_t>(offsetof(CloudRecognitionService_t3866570042, ___OnRecognitionError_9)); }
	inline OnRecognitionErrorEvent_t2212464566 * get_OnRecognitionError_9() const { return ___OnRecognitionError_9; }
	inline OnRecognitionErrorEvent_t2212464566 ** get_address_of_OnRecognitionError_9() { return &___OnRecognitionError_9; }
	inline void set_OnRecognitionError_9(OnRecognitionErrorEvent_t2212464566 * value)
	{
		___OnRecognitionError_9 = value;
		Il2CppCodeGenWriteBarrier(&___OnRecognitionError_9, value);
	}

	inline static int32_t get_offset_of_OnInterruption_10() { return static_cast<int32_t>(offsetof(CloudRecognitionService_t3866570042, ___OnInterruption_10)); }
	inline OnInterruptionEvent_t1530367979 * get_OnInterruption_10() const { return ___OnInterruption_10; }
	inline OnInterruptionEvent_t1530367979 ** get_address_of_OnInterruption_10() { return &___OnInterruption_10; }
	inline void set_OnInterruption_10(OnInterruptionEvent_t1530367979 * value)
	{
		___OnInterruption_10 = value;
		Il2CppCodeGenWriteBarrier(&___OnInterruption_10, value);
	}

	inline static int32_t get_offset_of_U3CIsContinuousRecognitionRunningU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(CloudRecognitionService_t3866570042, ___U3CIsContinuousRecognitionRunningU3Ek__BackingField_11)); }
	inline bool get_U3CIsContinuousRecognitionRunningU3Ek__BackingField_11() const { return ___U3CIsContinuousRecognitionRunningU3Ek__BackingField_11; }
	inline bool* get_address_of_U3CIsContinuousRecognitionRunningU3Ek__BackingField_11() { return &___U3CIsContinuousRecognitionRunningU3Ek__BackingField_11; }
	inline void set_U3CIsContinuousRecognitionRunningU3Ek__BackingField_11(bool value)
	{
		___U3CIsContinuousRecognitionRunningU3Ek__BackingField_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
