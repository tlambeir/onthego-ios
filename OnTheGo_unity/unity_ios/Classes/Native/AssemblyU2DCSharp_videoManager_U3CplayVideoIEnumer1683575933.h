﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.String
struct String_t;
// videoManager
struct videoManager_t104452510;
// System.Object
struct Il2CppObject;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// videoManager/<playVideoIEnumerator>c__Iterator0
struct  U3CplayVideoIEnumeratorU3Ec__Iterator0_t1683575933  : public Il2CppObject
{
public:
	// System.String videoManager/<playVideoIEnumerator>c__Iterator0::path
	String_t* ___path_0;
	// System.String videoManager/<playVideoIEnumerator>c__Iterator0::<url>__0
	String_t* ___U3CurlU3E__0_1;
	// videoManager videoManager/<playVideoIEnumerator>c__Iterator0::$this
	videoManager_t104452510 * ___U24this_2;
	// System.Object videoManager/<playVideoIEnumerator>c__Iterator0::$current
	Il2CppObject * ___U24current_3;
	// System.Boolean videoManager/<playVideoIEnumerator>c__Iterator0::$disposing
	bool ___U24disposing_4;
	// System.Int32 videoManager/<playVideoIEnumerator>c__Iterator0::$PC
	int32_t ___U24PC_5;

public:
	inline static int32_t get_offset_of_path_0() { return static_cast<int32_t>(offsetof(U3CplayVideoIEnumeratorU3Ec__Iterator0_t1683575933, ___path_0)); }
	inline String_t* get_path_0() const { return ___path_0; }
	inline String_t** get_address_of_path_0() { return &___path_0; }
	inline void set_path_0(String_t* value)
	{
		___path_0 = value;
		Il2CppCodeGenWriteBarrier(&___path_0, value);
	}

	inline static int32_t get_offset_of_U3CurlU3E__0_1() { return static_cast<int32_t>(offsetof(U3CplayVideoIEnumeratorU3Ec__Iterator0_t1683575933, ___U3CurlU3E__0_1)); }
	inline String_t* get_U3CurlU3E__0_1() const { return ___U3CurlU3E__0_1; }
	inline String_t** get_address_of_U3CurlU3E__0_1() { return &___U3CurlU3E__0_1; }
	inline void set_U3CurlU3E__0_1(String_t* value)
	{
		___U3CurlU3E__0_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CurlU3E__0_1, value);
	}

	inline static int32_t get_offset_of_U24this_2() { return static_cast<int32_t>(offsetof(U3CplayVideoIEnumeratorU3Ec__Iterator0_t1683575933, ___U24this_2)); }
	inline videoManager_t104452510 * get_U24this_2() const { return ___U24this_2; }
	inline videoManager_t104452510 ** get_address_of_U24this_2() { return &___U24this_2; }
	inline void set_U24this_2(videoManager_t104452510 * value)
	{
		___U24this_2 = value;
		Il2CppCodeGenWriteBarrier(&___U24this_2, value);
	}

	inline static int32_t get_offset_of_U24current_3() { return static_cast<int32_t>(offsetof(U3CplayVideoIEnumeratorU3Ec__Iterator0_t1683575933, ___U24current_3)); }
	inline Il2CppObject * get_U24current_3() const { return ___U24current_3; }
	inline Il2CppObject ** get_address_of_U24current_3() { return &___U24current_3; }
	inline void set_U24current_3(Il2CppObject * value)
	{
		___U24current_3 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_3, value);
	}

	inline static int32_t get_offset_of_U24disposing_4() { return static_cast<int32_t>(offsetof(U3CplayVideoIEnumeratorU3Ec__Iterator0_t1683575933, ___U24disposing_4)); }
	inline bool get_U24disposing_4() const { return ___U24disposing_4; }
	inline bool* get_address_of_U24disposing_4() { return &___U24disposing_4; }
	inline void set_U24disposing_4(bool value)
	{
		___U24disposing_4 = value;
	}

	inline static int32_t get_offset_of_U24PC_5() { return static_cast<int32_t>(offsetof(U3CplayVideoIEnumeratorU3Ec__Iterator0_t1683575933, ___U24PC_5)); }
	inline int32_t get_U24PC_5() const { return ___U24PC_5; }
	inline int32_t* get_address_of_U24PC_5() { return &___U24PC_5; }
	inline void set_U24PC_5(int32_t value)
	{
		___U24PC_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
