﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.Single[]
struct SingleU5BU5D_t1444911251;
// System.Collections.Generic.List`1<Wikitude.UnityEditorSimulator/SimulatedTarget>
struct List_1_t3036245690;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Wikitude.UnityEditorSimulator
struct  UnityEditorSimulator_t2033215626  : public Il2CppObject
{
public:

public:
};

struct UnityEditorSimulator_t2033215626_StaticFields
{
public:
	// System.Single[] Wikitude.UnityEditorSimulator::_defaultTrackingMatrix
	SingleU5BU5D_t1444911251* ____defaultTrackingMatrix_0;
	// System.Collections.Generic.List`1<Wikitude.UnityEditorSimulator/SimulatedTarget> Wikitude.UnityEditorSimulator::_currentTargets
	List_1_t3036245690 * ____currentTargets_1;

public:
	inline static int32_t get_offset_of__defaultTrackingMatrix_0() { return static_cast<int32_t>(offsetof(UnityEditorSimulator_t2033215626_StaticFields, ____defaultTrackingMatrix_0)); }
	inline SingleU5BU5D_t1444911251* get__defaultTrackingMatrix_0() const { return ____defaultTrackingMatrix_0; }
	inline SingleU5BU5D_t1444911251** get_address_of__defaultTrackingMatrix_0() { return &____defaultTrackingMatrix_0; }
	inline void set__defaultTrackingMatrix_0(SingleU5BU5D_t1444911251* value)
	{
		____defaultTrackingMatrix_0 = value;
		Il2CppCodeGenWriteBarrier(&____defaultTrackingMatrix_0, value);
	}

	inline static int32_t get_offset_of__currentTargets_1() { return static_cast<int32_t>(offsetof(UnityEditorSimulator_t2033215626_StaticFields, ____currentTargets_1)); }
	inline List_1_t3036245690 * get__currentTargets_1() const { return ____currentTargets_1; }
	inline List_1_t3036245690 ** get_address_of__currentTargets_1() { return &____currentTargets_1; }
	inline void set__currentTargets_1(List_1_t3036245690 * value)
	{
		____currentTargets_1 = value;
		Il2CppCodeGenWriteBarrier(&____currentTargets_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
