﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_RuntimeAnimatorController2933699135.h"
#include "UnityEngine_UnityEngine_Experimental_Director_Anim1397024487.h"
#include "UnityEngine_UnityEngine_Experimental_Director_Anim2837525843.h"
#include "UnityEngine_UnityEngine_Experimental_Director_Anim1193230317.h"
#include "UnityEngine_UnityEngine_Experimental_Director_Anim2591841208.h"
#include "UnityEngine_UnityEngine_Experimental_Director_Anim2436992687.h"
#include "UnityEngine_UnityEngine_Experimental_Director_Anim3136944237.h"
#include "UnityEngine_UnityEngine_Motion1110556653.h"
#include "UnityEngine_UnityEngine_TerrainData657004131.h"
#include "UnityEngine_UnityEngine_Terrain3055443660.h"
#include "UnityEngine_UnityEngine_FontStyle82229486.h"
#include "UnityEngine_UnityEngine_TextGenerationError3604799999.h"
#include "UnityEngine_UnityEngine_TextGenerationSettings1351628751.h"
#include "UnityEngine_UnityEngine_TextGenerator3211863866.h"
#include "UnityEngine_UnityEngine_TextAnchor2035777396.h"
#include "UnityEngine_UnityEngine_HorizontalWrapMode2172737147.h"
#include "UnityEngine_UnityEngine_VerticalWrapMode2936607737.h"
#include "UnityEngine_UnityEngine_GUIText402233326.h"
#include "UnityEngine_UnityEngine_TextMesh1536577757.h"
#include "UnityEngine_UnityEngine_CharacterInfo1228754872.h"
#include "UnityEngine_UnityEngine_Font1956802104.h"
#include "UnityEngine_UnityEngine_Font_FontTextureRebuildCal2467502454.h"
#include "UnityEngine_UnityEngine_UICharInfo75501106.h"
#include "UnityEngine_UnityEngine_UILineInfo4195266810.h"
#include "UnityEngine_UnityEngine_UIVertex4057497605.h"
#include "UnityEngine_UnityEngine_RectTransformUtility1743242446.h"
#include "UnityEngine_UnityEngine_RenderMode4077056833.h"
#include "UnityEngine_UnityEngine_AdditionalCanvasShaderChan2298426082.h"
#include "UnityEngine_UnityEngine_Canvas3310196443.h"
#include "UnityEngine_UnityEngine_Canvas_WillRenderCanvases3309123499.h"
#include "UnityEngine_UnityEngine_CanvasGroup4083511760.h"
#include "UnityEngine_UnityEngine_CanvasRenderer2598313366.h"
#include "UnityEngine_UnityEngine_Event2956885303.h"
#include "UnityEngine_UnityEngine_EventType3528516131.h"
#include "UnityEngine_UnityEngine_EventModifiers2016417398.h"
#include "UnityEngine_UnityEngine_GUI1624858472.h"
#include "UnityEngine_UnityEngine_GUI_WindowFunction3146511083.h"
#include "UnityEngine_UnityEngine_GUIContent3050628031.h"
#include "UnityEngine_UnityEngine_ScaleMode2341947364.h"
#include "UnityEngine_UnityEngine_FocusType718022695.h"
#include "UnityEngine_UnityEngine_GUILayout3503650450.h"
#include "UnityEngine_UnityEngine_GUILayoutOption811797299.h"
#include "UnityEngine_UnityEngine_GUILayoutOption_Type3858932131.h"
#include "UnityEngine_UnityEngine_GUILayoutGroup2157789695.h"
#include "UnityEngine_UnityEngine_GUIScrollGroup1523329021.h"
#include "UnityEngine_UnityEngine_GUILayoutEntry3214611570.h"
#include "UnityEngine_UnityEngine_GUIWordWrapSizer2043268473.h"
#include "UnityEngine_UnityEngine_GUILayoutUtility66395690.h"
#include "UnityEngine_UnityEngine_GUILayoutUtility_LayoutCache78309876.h"
#include "UnityEngine_UnityEngine_GUISettings1774757634.h"
#include "UnityEngine_UnityEngine_GUISkin1244372282.h"
#include "UnityEngine_UnityEngine_GUISkin_SkinChangedDelegat1143955295.h"
#include "UnityEngine_UnityEngine_GUIStyleState1397964415.h"
#include "UnityEngine_UnityEngine_ImagePosition641749504.h"
#include "UnityEngine_UnityEngine_GUIStyle3956901511.h"
#include "UnityEngine_UnityEngine_TextClipping865312958.h"
#include "UnityEngine_UnityEngine_GUITargetAttribute25796337.h"
#include "UnityEngine_UnityEngine_ExitGUIException133215258.h"
#include "UnityEngine_UnityEngine_GUIUtility1868551600.h"
#include "UnityEngine_UnityEngine_ScrollViewState3797911395.h"
#include "UnityEngine_UnityEngine_SliderState2207048770.h"
#include "UnityEngine_UnityEngine_TextEditor2759855366.h"
#include "UnityEngine_UnityEngine_TextEditor_DblClickSnappin2629979741.h"
#include "UnityEngine_UnityEngine_Internal_DrawArguments407707935.h"
#include "UnityEngine_UnityEngine_Internal_DrawWithTextSelect978153917.h"
#include "UnityEngine_UnityEngineInternal_WebRequestUtils3541624225.h"
#include "UnityEngine_UnityEngine_Networking_DownloadHandler2937767557.h"
#include "UnityEngine_UnityEngine_Networking_DownloadHandler3167892435.h"
#include "UnityEngine_UnityEngine_RemoteSettings1718627291.h"
#include "UnityEngine_UnityEngine_RemoteSettings_UpdatedEven1027848393.h"
#include "UnityEngine_UnityEngine_Video_VideoSource4095631662.h"
#include "UnityEngine_UnityEngine_Video_VideoAudioOutputMode1271585647.h"
#include "UnityEngine_UnityEngine_Video_VideoPlayer1683042537.h"
#include "UnityEngine_UnityEngine_Video_VideoPlayer_EventHan3436254912.h"
#include "UnityEngine_UnityEngine_Video_VideoPlayer_ErrorEve3211687919.h"
#include "UnityEngine_UnityEngine_Video_VideoPlayer_FrameRea3848515759.h"
#include "UnityEngine_UnityEngine_Video_VideoClip1281919028.h"
#include "UnityEngine_UnityEngine_AttributeHelperEngine2735742303.h"
#include "UnityEngine_UnityEngine_DisallowMultipleComponent1422053217.h"
#include "UnityEngine_UnityEngine_RequireComponent3490506609.h"
#include "UnityEngine_UnityEngine_AddComponentMenu415040132.h"
#include "UnityEngine_UnityEngine_ContextMenu1295656858.h"
#include "UnityEngine_UnityEngine_ExecuteInEditMode3727731349.h"
#include "UnityEngine_UnityEngine_HideInInspector1216868993.h"
#include "UnityEngine_UnityEngine_DefaultExecutionOrder3059642329.h"
#include "UnityEngine_UnityEngine_IL2CPPStructAlignmentAttri1946008997.h"
#include "UnityEngine_UnityEngine_NativeClassAttribute2601352714.h"
#include "UnityEngine_UnityEngine_Scripting_GeneratedByOldBin433318409.h"
#include "UnityEngine_UnityEngine_SendMessageOptions3580193095.h"
#include "UnityEngine_UnityEngine_PrimitiveType3468579401.h"
#include "UnityEngine_UnityEngine_Space654135784.h"
#include "UnityEngine_UnityEngine_RuntimePlatform4159857903.h"
#include "UnityEngine_UnityEngine_OperatingSystemFamily1868066375.h"
#include "UnityEngine_UnityEngine_LogType73765434.h"
#include "UnityEngine_UnityEngine_Color2555686324.h"
#include "UnityEngine_UnityEngine_Color322600501292.h"
#include "UnityEngine_UnityEngine_SetupCoroutine2062820429.h"
#include "UnityEngine_UnityEngine_WritableAttribute812406054.h"







#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1400 = { sizeof (RuntimeAnimatorController_t2933699135), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1401 = { sizeof (AnimatorControllerPlayable_t1397024487), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1402 = { sizeof (AnimationMixerPlayable_t2837525843), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1403 = { sizeof (AnimationLayerMixerPlayable_t1193230317), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1404 = { sizeof (AnimationClipPlayable_t2591841208), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1405 = { sizeof (AnimationPlayable_t2436992687), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1406 = { sizeof (AnimationOffsetPlayable_t3136944237), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1407 = { sizeof (Motion_t1110556653), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1408 = { sizeof (TerrainData_t657004131), -1, sizeof(TerrainData_t657004131_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1408[8] = 
{
	TerrainData_t657004131_StaticFields::get_offset_of_kMaximumResolution_2(),
	TerrainData_t657004131_StaticFields::get_offset_of_kMinimumDetailResolutionPerPatch_3(),
	TerrainData_t657004131_StaticFields::get_offset_of_kMaximumDetailResolutionPerPatch_4(),
	TerrainData_t657004131_StaticFields::get_offset_of_kMaximumDetailPatchCount_5(),
	TerrainData_t657004131_StaticFields::get_offset_of_kMinimumAlphamapResolution_6(),
	TerrainData_t657004131_StaticFields::get_offset_of_kMaximumAlphamapResolution_7(),
	TerrainData_t657004131_StaticFields::get_offset_of_kMinimumBaseMapResolution_8(),
	TerrainData_t657004131_StaticFields::get_offset_of_kMaximumBaseMapResolution_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1409 = { sizeof (Terrain_t3055443660), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1410 = { sizeof (FontStyle_t82229486)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1410[5] = 
{
	FontStyle_t82229486::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1411 = { sizeof (TextGenerationError_t3604799999)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1411[5] = 
{
	TextGenerationError_t3604799999::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1412 = { sizeof (TextGenerationSettings_t1351628751)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1412[18] = 
{
	TextGenerationSettings_t1351628751::get_offset_of_font_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TextGenerationSettings_t1351628751::get_offset_of_color_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TextGenerationSettings_t1351628751::get_offset_of_fontSize_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TextGenerationSettings_t1351628751::get_offset_of_lineSpacing_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TextGenerationSettings_t1351628751::get_offset_of_richText_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TextGenerationSettings_t1351628751::get_offset_of_scaleFactor_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TextGenerationSettings_t1351628751::get_offset_of_fontStyle_6() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TextGenerationSettings_t1351628751::get_offset_of_textAnchor_7() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TextGenerationSettings_t1351628751::get_offset_of_alignByGeometry_8() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TextGenerationSettings_t1351628751::get_offset_of_resizeTextForBestFit_9() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TextGenerationSettings_t1351628751::get_offset_of_resizeTextMinSize_10() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TextGenerationSettings_t1351628751::get_offset_of_resizeTextMaxSize_11() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TextGenerationSettings_t1351628751::get_offset_of_updateBounds_12() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TextGenerationSettings_t1351628751::get_offset_of_verticalOverflow_13() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TextGenerationSettings_t1351628751::get_offset_of_horizontalOverflow_14() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TextGenerationSettings_t1351628751::get_offset_of_generationExtents_15() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TextGenerationSettings_t1351628751::get_offset_of_pivot_16() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TextGenerationSettings_t1351628751::get_offset_of_generateOutOfBounds_17() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1413 = { sizeof (TextGenerator_t3211863866), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1413[11] = 
{
	TextGenerator_t3211863866::get_offset_of_m_Ptr_0(),
	TextGenerator_t3211863866::get_offset_of_m_LastString_1(),
	TextGenerator_t3211863866::get_offset_of_m_LastSettings_2(),
	TextGenerator_t3211863866::get_offset_of_m_HasGenerated_3(),
	TextGenerator_t3211863866::get_offset_of_m_LastValid_4(),
	TextGenerator_t3211863866::get_offset_of_m_Verts_5(),
	TextGenerator_t3211863866::get_offset_of_m_Characters_6(),
	TextGenerator_t3211863866::get_offset_of_m_Lines_7(),
	TextGenerator_t3211863866::get_offset_of_m_CachedVerts_8(),
	TextGenerator_t3211863866::get_offset_of_m_CachedCharacters_9(),
	TextGenerator_t3211863866::get_offset_of_m_CachedLines_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1414 = { sizeof (TextAnchor_t2035777396)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1414[10] = 
{
	TextAnchor_t2035777396::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1415 = { sizeof (HorizontalWrapMode_t2172737147)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1415[3] = 
{
	HorizontalWrapMode_t2172737147::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1416 = { sizeof (VerticalWrapMode_t2936607737)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1416[3] = 
{
	VerticalWrapMode_t2936607737::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1417 = { sizeof (GUIText_t402233326), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1418 = { sizeof (TextMesh_t1536577757), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1419 = { sizeof (CharacterInfo_t1228754872)+ sizeof (Il2CppObject), sizeof(CharacterInfo_t1228754872_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1419[7] = 
{
	CharacterInfo_t1228754872::get_offset_of_index_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	CharacterInfo_t1228754872::get_offset_of_uv_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	CharacterInfo_t1228754872::get_offset_of_vert_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	CharacterInfo_t1228754872::get_offset_of_width_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	CharacterInfo_t1228754872::get_offset_of_size_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	CharacterInfo_t1228754872::get_offset_of_style_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
	CharacterInfo_t1228754872::get_offset_of_flipped_6() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1420 = { sizeof (Font_t1956802104), -1, sizeof(Font_t1956802104_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1420[2] = 
{
	Font_t1956802104_StaticFields::get_offset_of_textureRebuilt_2(),
	Font_t1956802104::get_offset_of_m_FontTextureRebuildCallback_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1421 = { sizeof (FontTextureRebuildCallback_t2467502454), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1422 = { sizeof (UICharInfo_t75501106)+ sizeof (Il2CppObject), sizeof(UICharInfo_t75501106 ), 0, 0 };
extern const int32_t g_FieldOffsetTable1422[2] = 
{
	UICharInfo_t75501106::get_offset_of_cursorPos_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	UICharInfo_t75501106::get_offset_of_charWidth_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1423 = { sizeof (UILineInfo_t4195266810)+ sizeof (Il2CppObject), sizeof(UILineInfo_t4195266810 ), 0, 0 };
extern const int32_t g_FieldOffsetTable1423[4] = 
{
	UILineInfo_t4195266810::get_offset_of_startCharIdx_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	UILineInfo_t4195266810::get_offset_of_height_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	UILineInfo_t4195266810::get_offset_of_topY_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	UILineInfo_t4195266810::get_offset_of_leading_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1424 = { sizeof (UIVertex_t4057497605)+ sizeof (Il2CppObject), sizeof(UIVertex_t4057497605 ), sizeof(UIVertex_t4057497605_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1424[11] = 
{
	UIVertex_t4057497605::get_offset_of_position_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	UIVertex_t4057497605::get_offset_of_normal_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	UIVertex_t4057497605::get_offset_of_color_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	UIVertex_t4057497605::get_offset_of_uv0_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	UIVertex_t4057497605::get_offset_of_uv1_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	UIVertex_t4057497605::get_offset_of_uv2_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
	UIVertex_t4057497605::get_offset_of_uv3_6() + static_cast<int32_t>(sizeof(Il2CppObject)),
	UIVertex_t4057497605::get_offset_of_tangent_7() + static_cast<int32_t>(sizeof(Il2CppObject)),
	UIVertex_t4057497605_StaticFields::get_offset_of_s_DefaultColor_8(),
	UIVertex_t4057497605_StaticFields::get_offset_of_s_DefaultTangent_9(),
	UIVertex_t4057497605_StaticFields::get_offset_of_simpleVert_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1425 = { sizeof (RectTransformUtility_t1743242446), -1, sizeof(RectTransformUtility_t1743242446_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1425[1] = 
{
	RectTransformUtility_t1743242446_StaticFields::get_offset_of_s_Corners_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1426 = { sizeof (RenderMode_t4077056833)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1426[4] = 
{
	RenderMode_t4077056833::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1427 = { sizeof (AdditionalCanvasShaderChannels_t2298426082)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1427[7] = 
{
	AdditionalCanvasShaderChannels_t2298426082::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1428 = { sizeof (Canvas_t3310196443), -1, sizeof(Canvas_t3310196443_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1428[1] = 
{
	Canvas_t3310196443_StaticFields::get_offset_of_willRenderCanvases_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1429 = { sizeof (WillRenderCanvases_t3309123499), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1430 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1431 = { sizeof (CanvasGroup_t4083511760), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1432 = { sizeof (CanvasRenderer_t2598313366), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1433 = { sizeof (Event_t2956885303), sizeof(Event_t2956885303_marshaled_pinvoke), sizeof(Event_t2956885303_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1433[4] = 
{
	Event_t2956885303::get_offset_of_m_Ptr_0(),
	Event_t2956885303_StaticFields::get_offset_of_s_Current_1(),
	Event_t2956885303_StaticFields::get_offset_of_s_MasterEvent_2(),
	Event_t2956885303_StaticFields::get_offset_of_U3CU3Ef__switchU24map0_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1434 = { sizeof (EventType_t3528516131)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1434[33] = 
{
	EventType_t3528516131::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1435 = { sizeof (EventModifiers_t2016417398)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1435[9] = 
{
	EventModifiers_t2016417398::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1436 = { sizeof (GUI_t1624858472), -1, sizeof(GUI_t1624858472_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1436[12] = 
{
	GUI_t1624858472_StaticFields::get_offset_of_s_ScrollStepSize_0(),
	GUI_t1624858472_StaticFields::get_offset_of_s_HotTextField_1(),
	GUI_t1624858472_StaticFields::get_offset_of_s_BoxHash_2(),
	GUI_t1624858472_StaticFields::get_offset_of_s_RepeatButtonHash_3(),
	GUI_t1624858472_StaticFields::get_offset_of_s_ToggleHash_4(),
	GUI_t1624858472_StaticFields::get_offset_of_s_ButtonGridHash_5(),
	GUI_t1624858472_StaticFields::get_offset_of_s_SliderHash_6(),
	GUI_t1624858472_StaticFields::get_offset_of_s_BeginGroupHash_7(),
	GUI_t1624858472_StaticFields::get_offset_of_s_ScrollviewHash_8(),
	GUI_t1624858472_StaticFields::get_offset_of_U3CnextScrollStepTimeU3Ek__BackingField_9(),
	GUI_t1624858472_StaticFields::get_offset_of_s_Skin_10(),
	GUI_t1624858472_StaticFields::get_offset_of_s_ScrollViewStates_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1437 = { sizeof (WindowFunction_t3146511083), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1438 = { sizeof (GUIContent_t3050628031), -1, sizeof(GUIContent_t3050628031_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1438[7] = 
{
	GUIContent_t3050628031::get_offset_of_m_Text_0(),
	GUIContent_t3050628031::get_offset_of_m_Image_1(),
	GUIContent_t3050628031::get_offset_of_m_Tooltip_2(),
	GUIContent_t3050628031_StaticFields::get_offset_of_s_Text_3(),
	GUIContent_t3050628031_StaticFields::get_offset_of_s_Image_4(),
	GUIContent_t3050628031_StaticFields::get_offset_of_s_TextImage_5(),
	GUIContent_t3050628031_StaticFields::get_offset_of_none_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1439 = { sizeof (ScaleMode_t2341947364)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1439[4] = 
{
	ScaleMode_t2341947364::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1440 = { sizeof (FocusType_t718022695)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1440[4] = 
{
	FocusType_t718022695::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1441 = { sizeof (GUILayout_t3503650450), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1442 = { sizeof (GUILayoutOption_t811797299), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1442[2] = 
{
	GUILayoutOption_t811797299::get_offset_of_type_0(),
	GUILayoutOption_t811797299::get_offset_of_value_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1443 = { sizeof (Type_t3858932131)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1443[15] = 
{
	Type_t3858932131::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1444 = { sizeof (GUILayoutGroup_t2157789695), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1444[17] = 
{
	GUILayoutGroup_t2157789695::get_offset_of_entries_10(),
	GUILayoutGroup_t2157789695::get_offset_of_isVertical_11(),
	GUILayoutGroup_t2157789695::get_offset_of_resetCoords_12(),
	GUILayoutGroup_t2157789695::get_offset_of_spacing_13(),
	GUILayoutGroup_t2157789695::get_offset_of_sameSize_14(),
	GUILayoutGroup_t2157789695::get_offset_of_isWindow_15(),
	GUILayoutGroup_t2157789695::get_offset_of_windowID_16(),
	GUILayoutGroup_t2157789695::get_offset_of_m_Cursor_17(),
	GUILayoutGroup_t2157789695::get_offset_of_m_StretchableCountX_18(),
	GUILayoutGroup_t2157789695::get_offset_of_m_StretchableCountY_19(),
	GUILayoutGroup_t2157789695::get_offset_of_m_UserSpecifiedWidth_20(),
	GUILayoutGroup_t2157789695::get_offset_of_m_UserSpecifiedHeight_21(),
	GUILayoutGroup_t2157789695::get_offset_of_m_ChildMinWidth_22(),
	GUILayoutGroup_t2157789695::get_offset_of_m_ChildMaxWidth_23(),
	GUILayoutGroup_t2157789695::get_offset_of_m_ChildMinHeight_24(),
	GUILayoutGroup_t2157789695::get_offset_of_m_ChildMaxHeight_25(),
	GUILayoutGroup_t2157789695::get_offset_of_m_Margin_26(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1445 = { sizeof (GUIScrollGroup_t1523329021), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1445[12] = 
{
	GUIScrollGroup_t1523329021::get_offset_of_calcMinWidth_27(),
	GUIScrollGroup_t1523329021::get_offset_of_calcMaxWidth_28(),
	GUIScrollGroup_t1523329021::get_offset_of_calcMinHeight_29(),
	GUIScrollGroup_t1523329021::get_offset_of_calcMaxHeight_30(),
	GUIScrollGroup_t1523329021::get_offset_of_clientWidth_31(),
	GUIScrollGroup_t1523329021::get_offset_of_clientHeight_32(),
	GUIScrollGroup_t1523329021::get_offset_of_allowHorizontalScroll_33(),
	GUIScrollGroup_t1523329021::get_offset_of_allowVerticalScroll_34(),
	GUIScrollGroup_t1523329021::get_offset_of_needsHorizontalScrollbar_35(),
	GUIScrollGroup_t1523329021::get_offset_of_needsVerticalScrollbar_36(),
	GUIScrollGroup_t1523329021::get_offset_of_horizontalScrollbar_37(),
	GUIScrollGroup_t1523329021::get_offset_of_verticalScrollbar_38(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1446 = { sizeof (GUILayoutEntry_t3214611570), -1, sizeof(GUILayoutEntry_t3214611570_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1446[10] = 
{
	GUILayoutEntry_t3214611570::get_offset_of_minWidth_0(),
	GUILayoutEntry_t3214611570::get_offset_of_maxWidth_1(),
	GUILayoutEntry_t3214611570::get_offset_of_minHeight_2(),
	GUILayoutEntry_t3214611570::get_offset_of_maxHeight_3(),
	GUILayoutEntry_t3214611570::get_offset_of_rect_4(),
	GUILayoutEntry_t3214611570::get_offset_of_stretchWidth_5(),
	GUILayoutEntry_t3214611570::get_offset_of_stretchHeight_6(),
	GUILayoutEntry_t3214611570::get_offset_of_m_Style_7(),
	GUILayoutEntry_t3214611570_StaticFields::get_offset_of_kDummyRect_8(),
	GUILayoutEntry_t3214611570_StaticFields::get_offset_of_indent_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1447 = { sizeof (GUIWordWrapSizer_t2043268473), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1447[3] = 
{
	GUIWordWrapSizer_t2043268473::get_offset_of_m_Content_10(),
	GUIWordWrapSizer_t2043268473::get_offset_of_m_ForcedMinHeight_11(),
	GUIWordWrapSizer_t2043268473::get_offset_of_m_ForcedMaxHeight_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1448 = { sizeof (GUILayoutUtility_t66395690), -1, sizeof(GUILayoutUtility_t66395690_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1448[5] = 
{
	GUILayoutUtility_t66395690_StaticFields::get_offset_of_s_StoredLayouts_0(),
	GUILayoutUtility_t66395690_StaticFields::get_offset_of_s_StoredWindows_1(),
	GUILayoutUtility_t66395690_StaticFields::get_offset_of_current_2(),
	GUILayoutUtility_t66395690_StaticFields::get_offset_of_kDummyRect_3(),
	GUILayoutUtility_t66395690_StaticFields::get_offset_of_s_SpaceStyle_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1449 = { sizeof (LayoutCache_t78309876), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1449[3] = 
{
	LayoutCache_t78309876::get_offset_of_topLevel_0(),
	LayoutCache_t78309876::get_offset_of_layoutGroups_1(),
	LayoutCache_t78309876::get_offset_of_windows_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1450 = { sizeof (GUISettings_t1774757634), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1450[5] = 
{
	GUISettings_t1774757634::get_offset_of_m_DoubleClickSelectsWord_0(),
	GUISettings_t1774757634::get_offset_of_m_TripleClickSelectsLine_1(),
	GUISettings_t1774757634::get_offset_of_m_CursorColor_2(),
	GUISettings_t1774757634::get_offset_of_m_CursorFlashSpeed_3(),
	GUISettings_t1774757634::get_offset_of_m_SelectionColor_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1451 = { sizeof (GUISkin_t1244372282), -1, sizeof(GUISkin_t1244372282_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1451[27] = 
{
	GUISkin_t1244372282::get_offset_of_m_Font_2(),
	GUISkin_t1244372282::get_offset_of_m_box_3(),
	GUISkin_t1244372282::get_offset_of_m_button_4(),
	GUISkin_t1244372282::get_offset_of_m_toggle_5(),
	GUISkin_t1244372282::get_offset_of_m_label_6(),
	GUISkin_t1244372282::get_offset_of_m_textField_7(),
	GUISkin_t1244372282::get_offset_of_m_textArea_8(),
	GUISkin_t1244372282::get_offset_of_m_window_9(),
	GUISkin_t1244372282::get_offset_of_m_horizontalSlider_10(),
	GUISkin_t1244372282::get_offset_of_m_horizontalSliderThumb_11(),
	GUISkin_t1244372282::get_offset_of_m_verticalSlider_12(),
	GUISkin_t1244372282::get_offset_of_m_verticalSliderThumb_13(),
	GUISkin_t1244372282::get_offset_of_m_horizontalScrollbar_14(),
	GUISkin_t1244372282::get_offset_of_m_horizontalScrollbarThumb_15(),
	GUISkin_t1244372282::get_offset_of_m_horizontalScrollbarLeftButton_16(),
	GUISkin_t1244372282::get_offset_of_m_horizontalScrollbarRightButton_17(),
	GUISkin_t1244372282::get_offset_of_m_verticalScrollbar_18(),
	GUISkin_t1244372282::get_offset_of_m_verticalScrollbarThumb_19(),
	GUISkin_t1244372282::get_offset_of_m_verticalScrollbarUpButton_20(),
	GUISkin_t1244372282::get_offset_of_m_verticalScrollbarDownButton_21(),
	GUISkin_t1244372282::get_offset_of_m_ScrollView_22(),
	GUISkin_t1244372282::get_offset_of_m_CustomStyles_23(),
	GUISkin_t1244372282::get_offset_of_m_Settings_24(),
	GUISkin_t1244372282_StaticFields::get_offset_of_ms_Error_25(),
	GUISkin_t1244372282::get_offset_of_m_Styles_26(),
	GUISkin_t1244372282_StaticFields::get_offset_of_m_SkinChanged_27(),
	GUISkin_t1244372282_StaticFields::get_offset_of_current_28(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1452 = { sizeof (SkinChangedDelegate_t1143955295), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1453 = { sizeof (GUIStyleState_t1397964415), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1453[3] = 
{
	GUIStyleState_t1397964415::get_offset_of_m_Ptr_0(),
	GUIStyleState_t1397964415::get_offset_of_m_SourceStyle_1(),
	GUIStyleState_t1397964415::get_offset_of_m_Background_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1454 = { sizeof (ImagePosition_t641749504)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1454[5] = 
{
	ImagePosition_t641749504::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1455 = { sizeof (GUIStyle_t3956901511), -1, sizeof(GUIStyle_t3956901511_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1455[16] = 
{
	GUIStyle_t3956901511::get_offset_of_m_Ptr_0(),
	GUIStyle_t3956901511::get_offset_of_m_Normal_1(),
	GUIStyle_t3956901511::get_offset_of_m_Hover_2(),
	GUIStyle_t3956901511::get_offset_of_m_Active_3(),
	GUIStyle_t3956901511::get_offset_of_m_Focused_4(),
	GUIStyle_t3956901511::get_offset_of_m_OnNormal_5(),
	GUIStyle_t3956901511::get_offset_of_m_OnHover_6(),
	GUIStyle_t3956901511::get_offset_of_m_OnActive_7(),
	GUIStyle_t3956901511::get_offset_of_m_OnFocused_8(),
	GUIStyle_t3956901511::get_offset_of_m_Border_9(),
	GUIStyle_t3956901511::get_offset_of_m_Padding_10(),
	GUIStyle_t3956901511::get_offset_of_m_Margin_11(),
	GUIStyle_t3956901511::get_offset_of_m_Overflow_12(),
	GUIStyle_t3956901511::get_offset_of_m_FontInternal_13(),
	GUIStyle_t3956901511_StaticFields::get_offset_of_showKeyboardFocus_14(),
	GUIStyle_t3956901511_StaticFields::get_offset_of_s_None_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1456 = { sizeof (TextClipping_t865312958)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1456[3] = 
{
	TextClipping_t865312958::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1457 = { sizeof (GUITargetAttribute_t25796337), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1457[1] = 
{
	GUITargetAttribute_t25796337::get_offset_of_displayMask_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1458 = { sizeof (ExitGUIException_t133215258), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1459 = { sizeof (GUIUtility_t1868551600), -1, sizeof(GUIUtility_t1868551600_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1459[4] = 
{
	GUIUtility_t1868551600_StaticFields::get_offset_of_s_SkinMode_0(),
	GUIUtility_t1868551600_StaticFields::get_offset_of_s_OriginalID_1(),
	GUIUtility_t1868551600_StaticFields::get_offset_of_U3CguiIsExitingU3Ek__BackingField_2(),
	GUIUtility_t1868551600_StaticFields::get_offset_of_s_EditorScreenPointOffset_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1460 = { sizeof (ScrollViewState_t3797911395), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1461 = { sizeof (SliderState_t2207048770), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1461[3] = 
{
	SliderState_t2207048770::get_offset_of_dragStartPos_0(),
	SliderState_t2207048770::get_offset_of_dragStartValue_1(),
	SliderState_t2207048770::get_offset_of_isDragging_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1462 = { sizeof (TextEditor_t2759855366), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1462[16] = 
{
	TextEditor_t2759855366::get_offset_of_keyboardOnScreen_0(),
	TextEditor_t2759855366::get_offset_of_controlID_1(),
	TextEditor_t2759855366::get_offset_of_style_2(),
	TextEditor_t2759855366::get_offset_of_multiline_3(),
	TextEditor_t2759855366::get_offset_of_hasHorizontalCursorPos_4(),
	TextEditor_t2759855366::get_offset_of_isPasswordField_5(),
	TextEditor_t2759855366::get_offset_of_scrollOffset_6(),
	TextEditor_t2759855366::get_offset_of_m_Content_7(),
	TextEditor_t2759855366::get_offset_of_m_CursorIndex_8(),
	TextEditor_t2759855366::get_offset_of_m_SelectIndex_9(),
	TextEditor_t2759855366::get_offset_of_m_RevealCursor_10(),
	TextEditor_t2759855366::get_offset_of_m_MouseDragSelectsWholeWords_11(),
	TextEditor_t2759855366::get_offset_of_m_DblClickInitPos_12(),
	TextEditor_t2759855366::get_offset_of_m_DblClickSnap_13(),
	TextEditor_t2759855366::get_offset_of_m_bJustSelected_14(),
	TextEditor_t2759855366::get_offset_of_m_iAltCursorPos_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1463 = { sizeof (DblClickSnapping_t2629979741)+ sizeof (Il2CppObject), sizeof(uint8_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1463[3] = 
{
	DblClickSnapping_t2629979741::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1464 = { sizeof (Internal_DrawArguments_t407707935)+ sizeof (Il2CppObject), sizeof(Internal_DrawArguments_t407707935 ), 0, 0 };
extern const int32_t g_FieldOffsetTable1464[6] = 
{
	Internal_DrawArguments_t407707935::get_offset_of_target_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Internal_DrawArguments_t407707935::get_offset_of_position_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Internal_DrawArguments_t407707935::get_offset_of_isHover_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Internal_DrawArguments_t407707935::get_offset_of_isActive_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Internal_DrawArguments_t407707935::get_offset_of_on_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Internal_DrawArguments_t407707935::get_offset_of_hasKeyboardFocus_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1465 = { sizeof (Internal_DrawWithTextSelectionArguments_t978153917)+ sizeof (Il2CppObject), sizeof(Internal_DrawWithTextSelectionArguments_t978153917 ), 0, 0 };
extern const int32_t g_FieldOffsetTable1465[11] = 
{
	Internal_DrawWithTextSelectionArguments_t978153917::get_offset_of_target_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Internal_DrawWithTextSelectionArguments_t978153917::get_offset_of_position_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Internal_DrawWithTextSelectionArguments_t978153917::get_offset_of_firstPos_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Internal_DrawWithTextSelectionArguments_t978153917::get_offset_of_lastPos_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Internal_DrawWithTextSelectionArguments_t978153917::get_offset_of_cursorColor_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Internal_DrawWithTextSelectionArguments_t978153917::get_offset_of_selectionColor_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Internal_DrawWithTextSelectionArguments_t978153917::get_offset_of_isHover_6() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Internal_DrawWithTextSelectionArguments_t978153917::get_offset_of_isActive_7() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Internal_DrawWithTextSelectionArguments_t978153917::get_offset_of_on_8() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Internal_DrawWithTextSelectionArguments_t978153917::get_offset_of_hasKeyboardFocus_9() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Internal_DrawWithTextSelectionArguments_t978153917::get_offset_of_drawSelectionAsComposition_10() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1466 = { sizeof (WebRequestUtils_t3541624225), -1, sizeof(WebRequestUtils_t3541624225_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1466[1] = 
{
	WebRequestUtils_t3541624225_StaticFields::get_offset_of_domainRegex_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1467 = { sizeof (DownloadHandler_t2937767557), sizeof(DownloadHandler_t2937767557_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1467[1] = 
{
	DownloadHandler_t2937767557::get_offset_of_m_Ptr_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1468 = { sizeof (DownloadHandlerAudioClip_t3167892435), sizeof(DownloadHandlerAudioClip_t3167892435_marshaled_pinvoke), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1469 = { sizeof (RemoteSettings_t1718627291), -1, sizeof(RemoteSettings_t1718627291_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1469[1] = 
{
	RemoteSettings_t1718627291_StaticFields::get_offset_of_Updated_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1470 = { sizeof (UpdatedEventHandler_t1027848393), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1471 = { sizeof (VideoSource_t4095631662)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1471[3] = 
{
	VideoSource_t4095631662::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1472 = { sizeof (VideoAudioOutputMode_t1271585647)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1472[4] = 
{
	VideoAudioOutputMode_t1271585647::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1473 = { sizeof (VideoPlayer_t1683042537), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1473[7] = 
{
	VideoPlayer_t1683042537::get_offset_of_prepareCompleted_2(),
	VideoPlayer_t1683042537::get_offset_of_loopPointReached_3(),
	VideoPlayer_t1683042537::get_offset_of_started_4(),
	VideoPlayer_t1683042537::get_offset_of_frameDropped_5(),
	VideoPlayer_t1683042537::get_offset_of_errorReceived_6(),
	VideoPlayer_t1683042537::get_offset_of_seekCompleted_7(),
	VideoPlayer_t1683042537::get_offset_of_frameReady_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1474 = { sizeof (EventHandler_t3436254912), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1475 = { sizeof (ErrorEventHandler_t3211687919), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1476 = { sizeof (FrameReadyEventHandler_t3848515759), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1477 = { sizeof (VideoClip_t1281919028), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1478 = { sizeof (AttributeHelperEngine_t2735742303), -1, sizeof(AttributeHelperEngine_t2735742303_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1478[3] = 
{
	AttributeHelperEngine_t2735742303_StaticFields::get_offset_of__disallowMultipleComponentArray_0(),
	AttributeHelperEngine_t2735742303_StaticFields::get_offset_of__executeInEditModeArray_1(),
	AttributeHelperEngine_t2735742303_StaticFields::get_offset_of__requireComponentArray_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1479 = { sizeof (DisallowMultipleComponent_t1422053217), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1480 = { sizeof (RequireComponent_t3490506609), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1480[3] = 
{
	RequireComponent_t3490506609::get_offset_of_m_Type0_0(),
	RequireComponent_t3490506609::get_offset_of_m_Type1_1(),
	RequireComponent_t3490506609::get_offset_of_m_Type2_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1481 = { sizeof (AddComponentMenu_t415040132), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1481[2] = 
{
	AddComponentMenu_t415040132::get_offset_of_m_AddComponentMenu_0(),
	AddComponentMenu_t415040132::get_offset_of_m_Ordering_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1482 = { sizeof (ContextMenu_t1295656858), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1482[3] = 
{
	ContextMenu_t1295656858::get_offset_of_menuItem_0(),
	ContextMenu_t1295656858::get_offset_of_validate_1(),
	ContextMenu_t1295656858::get_offset_of_priority_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1483 = { sizeof (ExecuteInEditMode_t3727731349), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1484 = { sizeof (HideInInspector_t1216868993), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1485 = { sizeof (DefaultExecutionOrder_t3059642329), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1485[1] = 
{
	DefaultExecutionOrder_t3059642329::get_offset_of_U3CorderU3Ek__BackingField_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1486 = { sizeof (IL2CPPStructAlignmentAttribute_t1946008997), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1486[1] = 
{
	IL2CPPStructAlignmentAttribute_t1946008997::get_offset_of_Align_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1487 = { sizeof (NativeClassAttribute_t2601352714), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1487[1] = 
{
	NativeClassAttribute_t2601352714::get_offset_of_U3CQualifiedNativeNameU3Ek__BackingField_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1488 = { sizeof (GeneratedByOldBindingsGeneratorAttribute_t433318409), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1489 = { sizeof (SendMessageOptions_t3580193095)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1489[3] = 
{
	SendMessageOptions_t3580193095::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1490 = { sizeof (PrimitiveType_t3468579401)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1490[7] = 
{
	PrimitiveType_t3468579401::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1491 = { sizeof (Space_t654135784)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1491[3] = 
{
	Space_t654135784::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1492 = { sizeof (RuntimePlatform_t4159857903)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1492[34] = 
{
	RuntimePlatform_t4159857903::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1493 = { sizeof (OperatingSystemFamily_t1868066375)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1493[5] = 
{
	OperatingSystemFamily_t1868066375::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1494 = { sizeof (LogType_t73765434)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1494[6] = 
{
	LogType_t73765434::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1495 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1495[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1496 = { sizeof (Color_t2555686324)+ sizeof (Il2CppObject), sizeof(Color_t2555686324 ), 0, 0 };
extern const int32_t g_FieldOffsetTable1496[4] = 
{
	Color_t2555686324::get_offset_of_r_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Color_t2555686324::get_offset_of_g_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Color_t2555686324::get_offset_of_b_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Color_t2555686324::get_offset_of_a_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1497 = { sizeof (Color32_t2600501292)+ sizeof (Il2CppObject), sizeof(Color32_t2600501292 ), 0, 0 };
extern const int32_t g_FieldOffsetTable1497[4] = 
{
	Color32_t2600501292::get_offset_of_r_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Color32_t2600501292::get_offset_of_g_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Color32_t2600501292::get_offset_of_b_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Color32_t2600501292::get_offset_of_a_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1498 = { sizeof (SetupCoroutine_t2062820429), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1499 = { sizeof (WritableAttribute_t812406054), -1, 0, 0 };
#ifdef __clang__
#pragma clang diagnostic pop
#endif
