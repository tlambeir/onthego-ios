﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour3962482529.h"

// UnityEngine.UI.Text
struct Text_t1901882714;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Lean.Touch.LeanSwipeDirection8
struct  LeanSwipeDirection8_t1958227876  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.UI.Text Lean.Touch.LeanSwipeDirection8::InfoText
	Text_t1901882714 * ___InfoText_2;

public:
	inline static int32_t get_offset_of_InfoText_2() { return static_cast<int32_t>(offsetof(LeanSwipeDirection8_t1958227876, ___InfoText_2)); }
	inline Text_t1901882714 * get_InfoText_2() const { return ___InfoText_2; }
	inline Text_t1901882714 ** get_address_of_InfoText_2() { return &___InfoText_2; }
	inline void set_InfoText_2(Text_t1901882714 * value)
	{
		___InfoText_2 = value;
		Il2CppCodeGenWriteBarrier(&___InfoText_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
