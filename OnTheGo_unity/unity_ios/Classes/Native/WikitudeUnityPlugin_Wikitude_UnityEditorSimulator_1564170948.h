﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_ValueType3640485471.h"
#include "UnityEngine_UnityEngine_Vector33722313464.h"

// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Wikitude.UnityEditorSimulator/SimulatedTarget
struct  SimulatedTarget_t1564170948 
{
public:
	// System.String Wikitude.UnityEditorSimulator/SimulatedTarget::Name
	String_t* ___Name_0;
	// System.Int32 Wikitude.UnityEditorSimulator/SimulatedTarget::UniqueId
	int32_t ___UniqueId_1;
	// UnityEngine.Vector3 Wikitude.UnityEditorSimulator/SimulatedTarget::Offset
	Vector3_t3722313464  ___Offset_2;

public:
	inline static int32_t get_offset_of_Name_0() { return static_cast<int32_t>(offsetof(SimulatedTarget_t1564170948, ___Name_0)); }
	inline String_t* get_Name_0() const { return ___Name_0; }
	inline String_t** get_address_of_Name_0() { return &___Name_0; }
	inline void set_Name_0(String_t* value)
	{
		___Name_0 = value;
		Il2CppCodeGenWriteBarrier(&___Name_0, value);
	}

	inline static int32_t get_offset_of_UniqueId_1() { return static_cast<int32_t>(offsetof(SimulatedTarget_t1564170948, ___UniqueId_1)); }
	inline int32_t get_UniqueId_1() const { return ___UniqueId_1; }
	inline int32_t* get_address_of_UniqueId_1() { return &___UniqueId_1; }
	inline void set_UniqueId_1(int32_t value)
	{
		___UniqueId_1 = value;
	}

	inline static int32_t get_offset_of_Offset_2() { return static_cast<int32_t>(offsetof(SimulatedTarget_t1564170948, ___Offset_2)); }
	inline Vector3_t3722313464  get_Offset_2() const { return ___Offset_2; }
	inline Vector3_t3722313464 * get_address_of_Offset_2() { return &___Offset_2; }
	inline void set_Offset_2(Vector3_t3722313464  value)
	{
		___Offset_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of Wikitude.UnityEditorSimulator/SimulatedTarget
struct SimulatedTarget_t1564170948_marshaled_pinvoke
{
	char* ___Name_0;
	int32_t ___UniqueId_1;
	Vector3_t3722313464  ___Offset_2;
};
// Native definition for COM marshalling of Wikitude.UnityEditorSimulator/SimulatedTarget
struct SimulatedTarget_t1564170948_marshaled_com
{
	Il2CppChar* ___Name_0;
	int32_t ___UniqueId_1;
	Vector3_t3722313464  ___Offset_2;
};
