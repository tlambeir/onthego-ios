﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour3962482529.h"

// UnityEngine.UI.CanvasScaler
struct CanvasScaler_t2767979955;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ascpectRatioManager
struct  ascpectRatioManager_t2479582443  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.UI.CanvasScaler ascpectRatioManager::canvasScaler
	CanvasScaler_t2767979955 * ___canvasScaler_2;

public:
	inline static int32_t get_offset_of_canvasScaler_2() { return static_cast<int32_t>(offsetof(ascpectRatioManager_t2479582443, ___canvasScaler_2)); }
	inline CanvasScaler_t2767979955 * get_canvasScaler_2() const { return ___canvasScaler_2; }
	inline CanvasScaler_t2767979955 ** get_address_of_canvasScaler_2() { return &___canvasScaler_2; }
	inline void set_canvasScaler_2(CanvasScaler_t2767979955 * value)
	{
		___canvasScaler_2 = value;
		Il2CppCodeGenWriteBarrier(&___canvasScaler_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
