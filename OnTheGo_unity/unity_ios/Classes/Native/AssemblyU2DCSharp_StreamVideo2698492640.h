﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour3962482529.h"
#include "UnityEngine_UnityEngine_Video_VideoSource4095631662.h"

// UnityEngine.UI.RawImage
struct RawImage_t3182918964;
// UnityEngine.Video.VideoClip
struct VideoClip_t1281919028;
// UnityEngine.Video.VideoPlayer
struct VideoPlayer_t1683042537;
// UnityEngine.AudioSource
struct AudioSource_t3935305588;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// StreamVideo
struct  StreamVideo_t2698492640  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.UI.RawImage StreamVideo::image
	RawImage_t3182918964 * ___image_2;
	// UnityEngine.Video.VideoClip StreamVideo::clip
	VideoClip_t1281919028 * ___clip_3;
	// UnityEngine.Video.VideoPlayer StreamVideo::videoPlayer
	VideoPlayer_t1683042537 * ___videoPlayer_4;
	// UnityEngine.Video.VideoSource StreamVideo::videoSource
	int32_t ___videoSource_5;
	// UnityEngine.AudioSource StreamVideo::audioSource
	AudioSource_t3935305588 * ___audioSource_6;

public:
	inline static int32_t get_offset_of_image_2() { return static_cast<int32_t>(offsetof(StreamVideo_t2698492640, ___image_2)); }
	inline RawImage_t3182918964 * get_image_2() const { return ___image_2; }
	inline RawImage_t3182918964 ** get_address_of_image_2() { return &___image_2; }
	inline void set_image_2(RawImage_t3182918964 * value)
	{
		___image_2 = value;
		Il2CppCodeGenWriteBarrier(&___image_2, value);
	}

	inline static int32_t get_offset_of_clip_3() { return static_cast<int32_t>(offsetof(StreamVideo_t2698492640, ___clip_3)); }
	inline VideoClip_t1281919028 * get_clip_3() const { return ___clip_3; }
	inline VideoClip_t1281919028 ** get_address_of_clip_3() { return &___clip_3; }
	inline void set_clip_3(VideoClip_t1281919028 * value)
	{
		___clip_3 = value;
		Il2CppCodeGenWriteBarrier(&___clip_3, value);
	}

	inline static int32_t get_offset_of_videoPlayer_4() { return static_cast<int32_t>(offsetof(StreamVideo_t2698492640, ___videoPlayer_4)); }
	inline VideoPlayer_t1683042537 * get_videoPlayer_4() const { return ___videoPlayer_4; }
	inline VideoPlayer_t1683042537 ** get_address_of_videoPlayer_4() { return &___videoPlayer_4; }
	inline void set_videoPlayer_4(VideoPlayer_t1683042537 * value)
	{
		___videoPlayer_4 = value;
		Il2CppCodeGenWriteBarrier(&___videoPlayer_4, value);
	}

	inline static int32_t get_offset_of_videoSource_5() { return static_cast<int32_t>(offsetof(StreamVideo_t2698492640, ___videoSource_5)); }
	inline int32_t get_videoSource_5() const { return ___videoSource_5; }
	inline int32_t* get_address_of_videoSource_5() { return &___videoSource_5; }
	inline void set_videoSource_5(int32_t value)
	{
		___videoSource_5 = value;
	}

	inline static int32_t get_offset_of_audioSource_6() { return static_cast<int32_t>(offsetof(StreamVideo_t2698492640, ___audioSource_6)); }
	inline AudioSource_t3935305588 * get_audioSource_6() const { return ___audioSource_6; }
	inline AudioSource_t3935305588 ** get_address_of_audioSource_6() { return &___audioSource_6; }
	inline void set_audioSource_6(AudioSource_t3935305588 * value)
	{
		___audioSource_6 = value;
		Il2CppCodeGenWriteBarrier(&___audioSource_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
