﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_BehaviourToggle3110580710.h"
#include "AssemblyU2DCSharp_BehaviourToggle_U3CToggleRoutine2050567850.h"
#include "AssemblyU2DCSharp_CompoundObjectController273631460.h"
#include "AssemblyU2DCSharp_GameObjectToggle1685098097.h"
#include "AssemblyU2DCSharp_GameObjectToggle_U3CToggleRoutin3623893615.h"
#include "AssemblyU2DCSharp_MovementController627264377.h"
#include "AssemblyU2DCSharp_RotationController3700778844.h"
#include "AssemblyU2DCSharp_CameraFlyController109453238.h"
#include "AssemblyU2DCSharp_InputHelper1816768171.h"
#include "AssemblyU2DCSharp_ColorTool2360959593.h"
#include "AssemblyU2DCSharp_FPSCounter4258725281.h"
#include "AssemblyU2DCSharp_PresetSelector2134829519.h"
#include "AssemblyU2DCSharp_SceneLoader4130533360.h"
#include "AssemblyU2DCSharp_ScreenSpaceCanvas2254845544.h"
#include "AssemblyU2DCSharp_WorldSpaceCanvas1638530760.h"
#include "AssemblyU2DCSharp_JSONObject1339445639.h"
#include "AssemblyU2DCSharp_JSONObject_Type1992804461.h"
#include "AssemblyU2DCSharp_JSONObject_AddJSONContents64304480.h"
#include "AssemblyU2DCSharp_JSONObject_FieldNotFound2620845099.h"
#include "AssemblyU2DCSharp_JSONObject_GetFieldResponse4099938239.h"
#include "AssemblyU2DCSharp_JSONObject_U3CBakeAsyncU3Ec__Ite4027967272.h"
#include "AssemblyU2DCSharp_JSONObject_U3CPrintAsyncU3Ec__It4186082390.h"
#include "AssemblyU2DCSharp_JSONObject_U3CStringifyAsyncU3Ec3547434334.h"
#include "AssemblyU2DCSharp_JSONTemplates2141175947.h"
#include "AssemblyU2DCSharp_Lean_Touch_LeanCameraMove2733027855.h"
#include "AssemblyU2DCSharp_Lean_Touch_LeanCameraMoveSmooth3552812877.h"
#include "AssemblyU2DCSharp_Lean_Touch_LeanCameraZoom533377559.h"
#include "AssemblyU2DCSharp_Lean_Touch_LeanCameraZoomSmooth1926209604.h"
#include "AssemblyU2DCSharp_Lean_Touch_LeanDestroy2012376690.h"
#include "AssemblyU2DCSharp_Lean_Touch_LeanFingerDown827018743.h"
#include "AssemblyU2DCSharp_Lean_Touch_LeanFingerDown_LeanFi2839702054.h"
#include "AssemblyU2DCSharp_Lean_Touch_LeanFingerHeld1927732235.h"
#include "AssemblyU2DCSharp_Lean_Touch_LeanFingerHeld_FingerEv27707137.h"
#include "AssemblyU2DCSharp_Lean_Touch_LeanFingerHeld_Link1401011957.h"
#include "AssemblyU2DCSharp_Lean_Touch_LeanFingerLine3523414432.h"
#include "AssemblyU2DCSharp_Lean_Touch_LeanFingerSet4269474588.h"
#include "AssemblyU2DCSharp_Lean_Touch_LeanFingerSet_LeanFin2859433896.h"
#include "AssemblyU2DCSharp_Lean_Touch_LeanFingerSwipe3060022393.h"
#include "AssemblyU2DCSharp_Lean_Touch_LeanFingerSwipe_Finge2652301007.h"
#include "AssemblyU2DCSharp_Lean_Touch_LeanFingerTap1535759752.h"
#include "AssemblyU2DCSharp_Lean_Touch_LeanFingerTap_LeanFin1763708065.h"
#include "AssemblyU2DCSharp_Lean_Touch_LeanFingerTrail1074103177.h"
#include "AssemblyU2DCSharp_Lean_Touch_LeanFingerTrail_LeanF1250509032.h"
#include "AssemblyU2DCSharp_Lean_Touch_LeanFingerTrail_Link4191133801.h"
#include "AssemblyU2DCSharp_Lean_Touch_LeanFingerUp1918473171.h"
#include "AssemblyU2DCSharp_Lean_Touch_LeanFingerUp_LeanFinge446569333.h"
#include "AssemblyU2DCSharp_Lean_Touch_LeanMultiTap1614097430.h"
#include "AssemblyU2DCSharp_Lean_Touch_LeanMultiTap_IntEvent3520185602.h"
#include "AssemblyU2DCSharp_Lean_Touch_LeanMultiTapInfo2117714181.h"
#include "AssemblyU2DCSharp_Lean_Touch_LeanOpenUrl3078677178.h"
#include "AssemblyU2DCSharp_Lean_Touch_LeanPitchYaw3305105563.h"
#include "AssemblyU2DCSharp_Lean_Touch_LeanPitchYawSmooth1472675816.h"
#include "AssemblyU2DCSharp_Lean_Touch_LeanPressSelect1072818198.h"
#include "AssemblyU2DCSharp_Lean_Touch_LeanPressSelect_Selec1719826621.h"
#include "AssemblyU2DCSharp_Lean_Touch_LeanPressSelect_Searc3977590902.h"
#include "AssemblyU2DCSharp_Lean_Touch_LeanRotate34594455.h"
#include "AssemblyU2DCSharp_Lean_Touch_LeanScale3637880192.h"
#include "AssemblyU2DCSharp_Lean_Touch_LeanSelect1468903490.h"
#include "AssemblyU2DCSharp_Lean_Touch_LeanSelect_SelectType3826004921.h"
#include "AssemblyU2DCSharp_Lean_Touch_LeanSelect_SearchType1066087934.h"
#include "AssemblyU2DCSharp_Lean_Touch_LeanSelect_ReselectTyp452019290.h"
#include "AssemblyU2DCSharp_Lean_Touch_LeanSelectableBehavio3156997792.h"
#include "AssemblyU2DCSharp_Lean_Touch_LeanSelectableRenderer156123418.h"
#include "AssemblyU2DCSharp_Lean_Touch_LeanSelectableSpriteRen35475258.h"
#include "AssemblyU2DCSharp_Lean_Touch_LeanSelectableTransla4040639409.h"
#include "AssemblyU2DCSharp_Lean_Touch_LeanSelectableTransla4040704945.h"
#include "AssemblyU2DCSharp_Lean_Touch_LeanSpawn529394838.h"
#include "AssemblyU2DCSharp_Lean_Touch_LeanSwipeDirection41957965732.h"
#include "AssemblyU2DCSharp_Lean_Touch_LeanSwipeDirection81958227876.h"
#include "AssemblyU2DCSharp_Lean_Touch_LeanSwipeRigidbody2D3111785101.h"
#include "AssemblyU2DCSharp_Lean_Touch_LeanSwipeRigidbody2DNo899216836.h"
#include "AssemblyU2DCSharp_Lean_Touch_LeanSwipeRigidbody3D3111719565.h"
#include "AssemblyU2DCSharp_Lean_Touch_LeanSwipeRigidbody3DN1104278980.h"
#include "AssemblyU2DCSharp_Lean_Touch_LeanTapSelect839112588.h"
#include "AssemblyU2DCSharp_Lean_Touch_LeanTouchEvents1822248729.h"
#include "AssemblyU2DCSharp_Lean_Touch_LeanTranslate455865715.h"
#include "AssemblyU2DCSharp_Lean_Touch_LeanTranslateSmooth227688578.h"
#include "AssemblyU2DCSharp_Lean_Touch_LeanFinger3506292858.h"
#include "AssemblyU2DCSharp_Lean_Touch_LeanGesture1335127695.h"
#include "AssemblyU2DCSharp_Lean_Touch_LeanSelectable2178850769.h"
#include "AssemblyU2DCSharp_Lean_Touch_LeanSelectable_LeanFin821904700.h"
#include "AssemblyU2DCSharp_Lean_Touch_LeanSnapshot4136513957.h"
#include "AssemblyU2DCSharp_Lean_Touch_LeanTouch2951860335.h"
#include "AssemblyU2DCSharp_iTweenPath76106739.h"
#include "AssemblyU2DCSharp_RotationAdjuster35632371.h"
#include "AssemblyU2DCSharp_AMIA4201455126.h"
#include "AssemblyU2DCSharp_AndroidData2576415101.h"
#include "AssemblyU2DCSharp_ApplicationModel3549425075.h"
#include "AssemblyU2DCSharp_BottomNav1719165380.h"
#include "AssemblyU2DCSharp_Branch1428165248.h"
#include "AssemblyU2DCSharp_CenterScript2028411258.h"
#include "AssemblyU2DCSharp_CenterScript_U3CHandleItU3Ec__It3758345359.h"
#include "AssemblyU2DCSharp_FakeApplicationModel3022998755.h"
#include "AssemblyU2DCSharp_GManager3062881246.h"
#include "AssemblyU2DCSharp_Board_Manager347073597.h"
#include "AssemblyU2DCSharp_GameManager1536523654.h"
#include "AssemblyU2DCSharp_GetOutCard3730830718.h"
#include "AssemblyU2DCSharp_Image1825575977.h"
#include "AssemblyU2DCSharp_InitSceneController315947004.h"
#include "AssemblyU2DCSharp_InitSceneController_U3CloadAsset4011785967.h"







#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2100 = { sizeof (BehaviourToggle_t3110580710), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2100[3] = 
{
	BehaviourToggle_t3110580710::get_offset_of_target_2(),
	BehaviourToggle_t3110580710::get_offset_of_delayMin_3(),
	BehaviourToggle_t3110580710::get_offset_of_delayMax_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2101 = { sizeof (U3CToggleRoutineU3Ec__Iterator0_t2050567850), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2101[4] = 
{
	U3CToggleRoutineU3Ec__Iterator0_t2050567850::get_offset_of_U24this_0(),
	U3CToggleRoutineU3Ec__Iterator0_t2050567850::get_offset_of_U24current_1(),
	U3CToggleRoutineU3Ec__Iterator0_t2050567850::get_offset_of_U24disposing_2(),
	U3CToggleRoutineU3Ec__Iterator0_t2050567850::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2102 = { sizeof (CompoundObjectController_t273631460), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2102[6] = 
{
	CompoundObjectController_t273631460::get_offset_of_meshes_2(),
	CompoundObjectController_t273631460::get_offset_of_shaders_3(),
	CompoundObjectController_t273631460::get_offset_of_tr_4(),
	CompoundObjectController_t273631460::get_offset_of_objects_5(),
	CompoundObjectController_t273631460::get_offset_of_shaderIndex_6(),
	CompoundObjectController_t273631460::get_offset_of_h_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2103 = { sizeof (GameObjectToggle_t1685098097), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2103[3] = 
{
	GameObjectToggle_t1685098097::get_offset_of_target_2(),
	GameObjectToggle_t1685098097::get_offset_of_delayMin_3(),
	GameObjectToggle_t1685098097::get_offset_of_delayMax_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2104 = { sizeof (U3CToggleRoutineU3Ec__Iterator0_t3623893615), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2104[4] = 
{
	U3CToggleRoutineU3Ec__Iterator0_t3623893615::get_offset_of_U24this_0(),
	U3CToggleRoutineU3Ec__Iterator0_t3623893615::get_offset_of_U24current_1(),
	U3CToggleRoutineU3Ec__Iterator0_t3623893615::get_offset_of_U24disposing_2(),
	U3CToggleRoutineU3Ec__Iterator0_t3623893615::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2105 = { sizeof (MovementController_t627264377), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2105[8] = 
{
	MovementController_t627264377::get_offset_of_moveX_2(),
	MovementController_t627264377::get_offset_of_moveY_3(),
	MovementController_t627264377::get_offset_of_moveZ_4(),
	MovementController_t627264377::get_offset_of_speed_5(),
	MovementController_t627264377::get_offset_of_amplitude_6(),
	MovementController_t627264377::get_offset_of_tr_7(),
	MovementController_t627264377::get_offset_of_counter_8(),
	MovementController_t627264377::get_offset_of_initialOffsets_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2106 = { sizeof (RotationController_t3700778844), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2106[4] = 
{
	RotationController_t3700778844::get_offset_of_speedX_2(),
	RotationController_t3700778844::get_offset_of_speedY_3(),
	RotationController_t3700778844::get_offset_of_speedZ_4(),
	RotationController_t3700778844::get_offset_of_tr_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2107 = { sizeof (CameraFlyController_t109453238), -1, sizeof(CameraFlyController_t109453238_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2107[24] = 
{
	CameraFlyController_t109453238::get_offset_of_keyboardMoveSpeed_2(),
	CameraFlyController_t109453238::get_offset_of_joystickAxisThresholdMin_3(),
	CameraFlyController_t109453238::get_offset_of_joystickAxisThresholdMax_4(),
	CameraFlyController_t109453238::get_offset_of_joystickMoveSpeedXMin_5(),
	CameraFlyController_t109453238::get_offset_of_joystickMoveSpeedXMax_6(),
	CameraFlyController_t109453238::get_offset_of_joystickMoveSpeedY_7(),
	CameraFlyController_t109453238::get_offset_of_joystickMoveSpeedZMin_8(),
	CameraFlyController_t109453238::get_offset_of_joystickMoveSpeedZMax_9(),
	CameraFlyController_t109453238::get_offset_of_joystickRotationSpeedMin_10(),
	CameraFlyController_t109453238::get_offset_of_joystickRotationSpeedMax_11(),
	CameraFlyController_t109453238_StaticFields::get_offset_of_keysForward_12(),
	CameraFlyController_t109453238_StaticFields::get_offset_of_keysBackward_13(),
	CameraFlyController_t109453238_StaticFields::get_offset_of_keysLeft_14(),
	CameraFlyController_t109453238_StaticFields::get_offset_of_keysRight_15(),
	CameraFlyController_t109453238_StaticFields::get_offset_of_keysUp_16(),
	CameraFlyController_t109453238_StaticFields::get_offset_of_keysDown_17(),
	CameraFlyController_t109453238_StaticFields::get_offset_of_keysRun_18(),
	CameraFlyController_t109453238_StaticFields::get_offset_of_joystickUp_19(),
	CameraFlyController_t109453238_StaticFields::get_offset_of_joystickDown_20(),
	CameraFlyController_t109453238::get_offset_of_tr_21(),
	CameraFlyController_t109453238::get_offset_of_rmbDownInRect_22(),
	CameraFlyController_t109453238::get_offset_of_mpStart_23(),
	CameraFlyController_t109453238::get_offset_of_originalRotation_24(),
	CameraFlyController_t109453238::get_offset_of_t_25(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2108 = { sizeof (InputHelper_t1816768171), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2109 = { sizeof (ColorTool_t2360959593), -1, sizeof(ColorTool_t2360959593_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2109[4] = 
{
	0,
	ColorTool_t2360959593_StaticFields::get_offset_of_colorComponentOffsets_1(),
	ColorTool_t2360959593_StaticFields::get_offset_of_colorComponents_2(),
	ColorTool_t2360959593_StaticFields::get_offset_of_colorResult_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2110 = { sizeof (FPSCounter_t4258725281), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2110[7] = 
{
	0,
	FPSCounter_t4258725281::get_offset_of_frameTimes_3(),
	FPSCounter_t4258725281::get_offset_of_sum_4(),
	FPSCounter_t4258725281::get_offset_of_i_5(),
	FPSCounter_t4258725281::get_offset_of_avgAcc_6(),
	FPSCounter_t4258725281::get_offset_of_medAcc_7(),
	FPSCounter_t4258725281::get_offset_of_text_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2111 = { sizeof (PresetSelector_t2134829519), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2111[2] = 
{
	PresetSelector_t2134829519::get_offset_of_dropdown_2(),
	PresetSelector_t2134829519::get_offset_of_hr_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2112 = { sizeof (SceneLoader_t4130533360), -1, sizeof(SceneLoader_t4130533360_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2112[11] = 
{
	SceneLoader_t4130533360_StaticFields::get_offset_of_keysNext_2(),
	SceneLoader_t4130533360_StaticFields::get_offset_of_keysPrev_3(),
	SceneLoader_t4130533360_StaticFields::get_offset_of_descriptionDefault_4(),
	SceneLoader_t4130533360_StaticFields::get_offset_of_descriptionConstant_5(),
	SceneLoader_t4130533360_StaticFields::get_offset_of_descriptions_6(),
	SceneLoader_t4130533360::get_offset_of_sceneNames_7(),
	SceneLoader_t4130533360::get_offset_of_title_8(),
	SceneLoader_t4130533360::get_offset_of_description_9(),
	SceneLoader_t4130533360::get_offset_of_dropdown_10(),
	SceneLoader_t4130533360::get_offset_of_previous_11(),
	SceneLoader_t4130533360::get_offset_of_next_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2113 = { sizeof (ScreenSpaceCanvas_t2254845544), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2113[12] = 
{
	0,
	ScreenSpaceCanvas_t2254845544::get_offset_of_matchWidthOrHeight_3(),
	ScreenSpaceCanvas_t2254845544::get_offset_of_distance_4(),
	ScreenSpaceCanvas_t2254845544::get_offset_of_canvas_5(),
	ScreenSpaceCanvas_t2254845544::get_offset_of_rt_6(),
	ScreenSpaceCanvas_t2254845544::get_offset_of_referenceResolution_7(),
	ScreenSpaceCanvas_t2254845544::get_offset_of__scaleX_8(),
	ScreenSpaceCanvas_t2254845544::get_offset_of__scaleY_9(),
	ScreenSpaceCanvas_t2254845544::get_offset_of__width_10(),
	ScreenSpaceCanvas_t2254845544::get_offset_of__height_11(),
	ScreenSpaceCanvas_t2254845544::get_offset_of__vr_12(),
	ScreenSpaceCanvas_t2254845544::get_offset_of__scaleFactor_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2114 = { sizeof (WorldSpaceCanvas_t1638530760), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2114[3] = 
{
	WorldSpaceCanvas_t1638530760::get_offset_of_scale_2(),
	WorldSpaceCanvas_t1638530760::get_offset_of_tr_3(),
	WorldSpaceCanvas_t1638530760::get_offset_of__material_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2115 = { sizeof (JSONObject_t1339445639), -1, sizeof(JSONObject_t1339445639_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2115[16] = 
{
	0,
	0,
	0,
	0,
	0,
	JSONObject_t1339445639_StaticFields::get_offset_of_WHITESPACE_5(),
	JSONObject_t1339445639::get_offset_of_type_6(),
	JSONObject_t1339445639::get_offset_of_list_7(),
	JSONObject_t1339445639::get_offset_of_keys_8(),
	JSONObject_t1339445639::get_offset_of_str_9(),
	JSONObject_t1339445639::get_offset_of_n_10(),
	JSONObject_t1339445639::get_offset_of_useInt_11(),
	JSONObject_t1339445639::get_offset_of_i_12(),
	JSONObject_t1339445639::get_offset_of_b_13(),
	0,
	JSONObject_t1339445639_StaticFields::get_offset_of_printWatch_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2116 = { sizeof (Type_t1992804461)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2116[8] = 
{
	Type_t1992804461::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2117 = { sizeof (AddJSONContents_t64304480), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2118 = { sizeof (FieldNotFound_t2620845099), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2119 = { sizeof (GetFieldResponse_t4099938239), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2120 = { sizeof (U3CBakeAsyncU3Ec__Iterator0_t4027967272), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2120[6] = 
{
	U3CBakeAsyncU3Ec__Iterator0_t4027967272::get_offset_of_U24locvar0_0(),
	U3CBakeAsyncU3Ec__Iterator0_t4027967272::get_offset_of_U3CsU3E__1_1(),
	U3CBakeAsyncU3Ec__Iterator0_t4027967272::get_offset_of_U24this_2(),
	U3CBakeAsyncU3Ec__Iterator0_t4027967272::get_offset_of_U24current_3(),
	U3CBakeAsyncU3Ec__Iterator0_t4027967272::get_offset_of_U24disposing_4(),
	U3CBakeAsyncU3Ec__Iterator0_t4027967272::get_offset_of_U24PC_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2121 = { sizeof (U3CPrintAsyncU3Ec__Iterator1_t4186082390), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2121[9] = 
{
	U3CPrintAsyncU3Ec__Iterator1_t4186082390::get_offset_of_U3CbuilderU3E__0_0(),
	U3CPrintAsyncU3Ec__Iterator1_t4186082390::get_offset_of_pretty_1(),
	U3CPrintAsyncU3Ec__Iterator1_t4186082390::get_offset_of_U24locvar0_2(),
	U3CPrintAsyncU3Ec__Iterator1_t4186082390::get_offset_of_U3CeU3E__1_3(),
	U3CPrintAsyncU3Ec__Iterator1_t4186082390::get_offset_of_U24locvar1_4(),
	U3CPrintAsyncU3Ec__Iterator1_t4186082390::get_offset_of_U24this_5(),
	U3CPrintAsyncU3Ec__Iterator1_t4186082390::get_offset_of_U24current_6(),
	U3CPrintAsyncU3Ec__Iterator1_t4186082390::get_offset_of_U24disposing_7(),
	U3CPrintAsyncU3Ec__Iterator1_t4186082390::get_offset_of_U24PC_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2122 = { sizeof (U3CStringifyAsyncU3Ec__Iterator2_t3547434334), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2122[19] = 
{
	U3CStringifyAsyncU3Ec__Iterator2_t3547434334::get_offset_of_depth_0(),
	U3CStringifyAsyncU3Ec__Iterator2_t3547434334::get_offset_of_U24locvar0_1(),
	U3CStringifyAsyncU3Ec__Iterator2_t3547434334::get_offset_of_builder_2(),
	U3CStringifyAsyncU3Ec__Iterator2_t3547434334::get_offset_of_pretty_3(),
	U3CStringifyAsyncU3Ec__Iterator2_t3547434334::get_offset_of_U3CiU3E__1_4(),
	U3CStringifyAsyncU3Ec__Iterator2_t3547434334::get_offset_of_U3CkeyU3E__2_5(),
	U3CStringifyAsyncU3Ec__Iterator2_t3547434334::get_offset_of_U3CobjU3E__2_6(),
	U3CStringifyAsyncU3Ec__Iterator2_t3547434334::get_offset_of_U24locvar1_7(),
	U3CStringifyAsyncU3Ec__Iterator2_t3547434334::get_offset_of_U3CeU3E__3_8(),
	U3CStringifyAsyncU3Ec__Iterator2_t3547434334::get_offset_of_U24locvar2_9(),
	U3CStringifyAsyncU3Ec__Iterator2_t3547434334::get_offset_of_U3CiU3E__4_10(),
	U3CStringifyAsyncU3Ec__Iterator2_t3547434334::get_offset_of_U24locvar3_11(),
	U3CStringifyAsyncU3Ec__Iterator2_t3547434334::get_offset_of_U3CeU3E__5_12(),
	U3CStringifyAsyncU3Ec__Iterator2_t3547434334::get_offset_of_U24locvar4_13(),
	U3CStringifyAsyncU3Ec__Iterator2_t3547434334::get_offset_of_U24this_14(),
	U3CStringifyAsyncU3Ec__Iterator2_t3547434334::get_offset_of_U24current_15(),
	U3CStringifyAsyncU3Ec__Iterator2_t3547434334::get_offset_of_U24disposing_16(),
	U3CStringifyAsyncU3Ec__Iterator2_t3547434334::get_offset_of_U3CU24U3Edepth_17(),
	U3CStringifyAsyncU3Ec__Iterator2_t3547434334::get_offset_of_U24PC_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2123 = { sizeof (JSONTemplates_t2141175947), -1, sizeof(JSONTemplates_t2141175947_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2123[1] = 
{
	JSONTemplates_t2141175947_StaticFields::get_offset_of_touched_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2124 = { sizeof (LeanCameraMove_t2733027855), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2124[5] = 
{
	LeanCameraMove_t2733027855::get_offset_of_Camera_2(),
	LeanCameraMove_t2733027855::get_offset_of_IgnoreGuiFingers_3(),
	LeanCameraMove_t2733027855::get_offset_of_RequiredFingerCount_4(),
	LeanCameraMove_t2733027855::get_offset_of_Distance_5(),
	LeanCameraMove_t2733027855::get_offset_of_Sensitivity_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2125 = { sizeof (LeanCameraMoveSmooth_t3552812877), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2125[2] = 
{
	LeanCameraMoveSmooth_t3552812877::get_offset_of_Dampening_7(),
	LeanCameraMoveSmooth_t3552812877::get_offset_of_remainingDelta_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2126 = { sizeof (LeanCameraZoom_t533377559), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2126[8] = 
{
	LeanCameraZoom_t533377559::get_offset_of_Camera_2(),
	LeanCameraZoom_t533377559::get_offset_of_IgnoreGuiFingers_3(),
	LeanCameraZoom_t533377559::get_offset_of_RequiredFingerCount_4(),
	LeanCameraZoom_t533377559::get_offset_of_WheelSensitivity_5(),
	LeanCameraZoom_t533377559::get_offset_of_Zoom_6(),
	LeanCameraZoom_t533377559::get_offset_of_ZoomClamp_7(),
	LeanCameraZoom_t533377559::get_offset_of_ZoomMin_8(),
	LeanCameraZoom_t533377559::get_offset_of_ZoomMax_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2127 = { sizeof (LeanCameraZoomSmooth_t1926209604), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2127[2] = 
{
	LeanCameraZoomSmooth_t1926209604::get_offset_of_Dampening_10(),
	LeanCameraZoomSmooth_t1926209604::get_offset_of_currentZoom_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2128 = { sizeof (LeanDestroy_t2012376690), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2128[1] = 
{
	LeanDestroy_t2012376690::get_offset_of_Seconds_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2129 = { sizeof (LeanFingerDown_t827018743), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2129[2] = 
{
	LeanFingerDown_t827018743::get_offset_of_IgnoreIfOverGui_2(),
	LeanFingerDown_t827018743::get_offset_of_OnFingerDown_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2130 = { sizeof (LeanFingerEvent_t2839702054), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2131 = { sizeof (LeanFingerHeld_t1927732235), -1, sizeof(LeanFingerHeld_t1927732235_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2131[11] = 
{
	LeanFingerHeld_t1927732235::get_offset_of_IgnoreIfStartedOverGui_2(),
	LeanFingerHeld_t1927732235::get_offset_of_MinimumAge_3(),
	LeanFingerHeld_t1927732235::get_offset_of_MaximumMovement_4(),
	LeanFingerHeld_t1927732235::get_offset_of_onFingerHeldDown_5(),
	LeanFingerHeld_t1927732235::get_offset_of_onFingerHeldSet_6(),
	LeanFingerHeld_t1927732235::get_offset_of_onFingerHeldUp_7(),
	LeanFingerHeld_t1927732235_StaticFields::get_offset_of_Instances_8(),
	LeanFingerHeld_t1927732235_StaticFields::get_offset_of_OnFingerHeldDown_9(),
	LeanFingerHeld_t1927732235_StaticFields::get_offset_of_OnFingerHeldSet_10(),
	LeanFingerHeld_t1927732235_StaticFields::get_offset_of_OnFingerHeldUp_11(),
	LeanFingerHeld_t1927732235::get_offset_of_links_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2132 = { sizeof (FingerEvent_t27707137), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2133 = { sizeof (Link_t1401011957), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2133[3] = 
{
	Link_t1401011957::get_offset_of_Finger_0(),
	Link_t1401011957::get_offset_of_LastSet_1(),
	Link_t1401011957::get_offset_of_TotalScaledDelta_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2134 = { sizeof (LeanFingerLine_t3523414432), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2135 = { sizeof (LeanFingerSet_t4269474588), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2135[3] = 
{
	LeanFingerSet_t4269474588::get_offset_of_IgnoreIfOverGui_2(),
	LeanFingerSet_t4269474588::get_offset_of_IgnoreIfStartedOverGui_3(),
	LeanFingerSet_t4269474588::get_offset_of_OnFingerSet_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2136 = { sizeof (LeanFingerEvent_t2859433896), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2137 = { sizeof (LeanFingerSwipe_t3060022393), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2137[2] = 
{
	LeanFingerSwipe_t3060022393::get_offset_of_IgnoreGuiFingers_2(),
	LeanFingerSwipe_t3060022393::get_offset_of_OnFingerSwipe_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2138 = { sizeof (FingerEvent_t2652301007), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2139 = { sizeof (LeanFingerTap_t1535759752), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2139[5] = 
{
	LeanFingerTap_t1535759752::get_offset_of_IgnoreIfOverGui_2(),
	LeanFingerTap_t1535759752::get_offset_of_IgnoreIfStartedOverGui_3(),
	LeanFingerTap_t1535759752::get_offset_of_RequiredTapCount_4(),
	LeanFingerTap_t1535759752::get_offset_of_RequiredTapInterval_5(),
	LeanFingerTap_t1535759752::get_offset_of_OnFingerTap_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2140 = { sizeof (LeanFingerEvent_t1763708065), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2141 = { sizeof (LeanFingerTrail_t1074103177), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2141[6] = 
{
	LeanFingerTrail_t1074103177::get_offset_of_IgnoreGuiFingers_2(),
	LeanFingerTrail_t1074103177::get_offset_of_Prefab_3(),
	LeanFingerTrail_t1074103177::get_offset_of_Distance_4(),
	LeanFingerTrail_t1074103177::get_offset_of_MaxLines_5(),
	LeanFingerTrail_t1074103177::get_offset_of_OnFingerUp_6(),
	LeanFingerTrail_t1074103177::get_offset_of_links_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2142 = { sizeof (LeanFingerEvent_t1250509032), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2143 = { sizeof (Link_t4191133801), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2143[2] = 
{
	Link_t4191133801::get_offset_of_Finger_0(),
	Link_t4191133801::get_offset_of_Line_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2144 = { sizeof (LeanFingerUp_t1918473171), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2144[3] = 
{
	LeanFingerUp_t1918473171::get_offset_of_IgnoreIfOverGui_2(),
	LeanFingerUp_t1918473171::get_offset_of_IgnoreIfStartedOverGui_3(),
	LeanFingerUp_t1918473171::get_offset_of_OnFingerUp_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2145 = { sizeof (LeanFingerEvent_t446569333), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2146 = { sizeof (LeanMultiTap_t1614097430), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2146[7] = 
{
	LeanMultiTap_t1614097430::get_offset_of_IgnoreGuiFingers_2(),
	LeanMultiTap_t1614097430::get_offset_of_MultiTap_3(),
	LeanMultiTap_t1614097430::get_offset_of_MultiTapCount_4(),
	LeanMultiTap_t1614097430::get_offset_of_HighestFingerCount_5(),
	LeanMultiTap_t1614097430::get_offset_of_OnMultiTap_6(),
	LeanMultiTap_t1614097430::get_offset_of_age_7(),
	LeanMultiTap_t1614097430::get_offset_of_lastFingerCount_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2147 = { sizeof (IntEvent_t3520185602), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2148 = { sizeof (LeanMultiTapInfo_t2117714181), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2148[1] = 
{
	LeanMultiTapInfo_t2117714181::get_offset_of_Text_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2149 = { sizeof (LeanOpenUrl_t3078677178), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2150 = { sizeof (LeanPitchYaw_t3305105563), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2150[13] = 
{
	LeanPitchYaw_t3305105563::get_offset_of_IgnoreGuiFingers_2(),
	LeanPitchYaw_t3305105563::get_offset_of_RequiredFingerCount_3(),
	LeanPitchYaw_t3305105563::get_offset_of_Camera_4(),
	LeanPitchYaw_t3305105563::get_offset_of_Pitch_5(),
	LeanPitchYaw_t3305105563::get_offset_of_PitchSensitivity_6(),
	LeanPitchYaw_t3305105563::get_offset_of_PitchClamp_7(),
	LeanPitchYaw_t3305105563::get_offset_of_PitchMin_8(),
	LeanPitchYaw_t3305105563::get_offset_of_PitchMax_9(),
	LeanPitchYaw_t3305105563::get_offset_of_Yaw_10(),
	LeanPitchYaw_t3305105563::get_offset_of_YawSensitivity_11(),
	LeanPitchYaw_t3305105563::get_offset_of_YawClamp_12(),
	LeanPitchYaw_t3305105563::get_offset_of_YawMin_13(),
	LeanPitchYaw_t3305105563::get_offset_of_YawMax_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2151 = { sizeof (LeanPitchYawSmooth_t1472675816), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2151[3] = 
{
	LeanPitchYawSmooth_t1472675816::get_offset_of_Dampening_15(),
	LeanPitchYawSmooth_t1472675816::get_offset_of_currentPitch_16(),
	LeanPitchYawSmooth_t1472675816::get_offset_of_currentYaw_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2152 = { sizeof (LeanPressSelect_t1072818198), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2152[6] = 
{
	LeanPressSelect_t1072818198::get_offset_of_IgnoreGuiFingers_2(),
	LeanPressSelect_t1072818198::get_offset_of_DeselectOnExit_3(),
	LeanPressSelect_t1072818198::get_offset_of_SelectUsing_4(),
	LeanPressSelect_t1072818198::get_offset_of_LayerMask_5(),
	LeanPressSelect_t1072818198::get_offset_of_Search_6(),
	LeanPressSelect_t1072818198::get_offset_of_CurrentSelectables_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2153 = { sizeof (SelectType_t1719826621)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2153[3] = 
{
	SelectType_t1719826621::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2154 = { sizeof (SearchType_t3977590902)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2154[4] = 
{
	SearchType_t3977590902::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2155 = { sizeof (LeanRotate_t34594455), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2155[6] = 
{
	LeanRotate_t34594455::get_offset_of_IgnoreGuiFingers_2(),
	LeanRotate_t34594455::get_offset_of_RequiredFingerCount_3(),
	LeanRotate_t34594455::get_offset_of_RequiredSelectable_4(),
	LeanRotate_t34594455::get_offset_of_Camera_5(),
	LeanRotate_t34594455::get_offset_of_RotateAxis_6(),
	LeanRotate_t34594455::get_offset_of_Relative_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2156 = { sizeof (LeanScale_t3637880192), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2156[6] = 
{
	LeanScale_t3637880192::get_offset_of_IgnoreGuiFingers_2(),
	LeanScale_t3637880192::get_offset_of_RequiredFingerCount_3(),
	LeanScale_t3637880192::get_offset_of_RequiredSelectable_4(),
	LeanScale_t3637880192::get_offset_of_WheelSensitivity_5(),
	LeanScale_t3637880192::get_offset_of_Camera_6(),
	LeanScale_t3637880192::get_offset_of_Relative_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2157 = { sizeof (LeanSelect_t1468903490), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2157[6] = 
{
	LeanSelect_t1468903490::get_offset_of_SelectUsing_2(),
	LeanSelect_t1468903490::get_offset_of_LayerMask_3(),
	LeanSelect_t1468903490::get_offset_of_Search_4(),
	LeanSelect_t1468903490::get_offset_of_CurrentSelectable_5(),
	LeanSelect_t1468903490::get_offset_of_Reselect_6(),
	LeanSelect_t1468903490::get_offset_of_AutoDeselect_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2158 = { sizeof (SelectType_t3826004921)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2158[3] = 
{
	SelectType_t3826004921::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2159 = { sizeof (SearchType_t1066087934)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2159[4] = 
{
	SearchType_t1066087934::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2160 = { sizeof (ReselectType_t452019290)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2160[5] = 
{
	ReselectType_t452019290::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2161 = { sizeof (LeanSelectableBehaviour_t3156997792), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2161[1] = 
{
	LeanSelectableBehaviour_t3156997792::get_offset_of_selectable_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2162 = { sizeof (LeanSelectableRendererColor_t156123418), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2162[3] = 
{
	LeanSelectableRendererColor_t156123418::get_offset_of_AutoGetDefaultColor_3(),
	LeanSelectableRendererColor_t156123418::get_offset_of_DefaultColor_4(),
	LeanSelectableRendererColor_t156123418::get_offset_of_SelectedColor_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2163 = { sizeof (LeanSelectableSpriteRendererColor_t35475258), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2163[3] = 
{
	LeanSelectableSpriteRendererColor_t35475258::get_offset_of_AutoGetDefaultColor_3(),
	LeanSelectableSpriteRendererColor_t35475258::get_offset_of_DefaultColor_4(),
	LeanSelectableSpriteRendererColor_t35475258::get_offset_of_SelectedColor_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2164 = { sizeof (LeanSelectableTranslateInertia2D_t4040639409), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2165 = { sizeof (LeanSelectableTranslateInertia3D_t4040704945), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2165[1] = 
{
	LeanSelectableTranslateInertia3D_t4040704945::get_offset_of_body_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2166 = { sizeof (LeanSpawn_t529394838), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2166[2] = 
{
	LeanSpawn_t529394838::get_offset_of_Prefab_2(),
	LeanSpawn_t529394838::get_offset_of_Distance_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2167 = { sizeof (LeanSwipeDirection4_t1957965732), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2167[1] = 
{
	LeanSwipeDirection4_t1957965732::get_offset_of_InfoText_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2168 = { sizeof (LeanSwipeDirection8_t1958227876), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2168[1] = 
{
	LeanSwipeDirection8_t1958227876::get_offset_of_InfoText_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2169 = { sizeof (LeanSwipeRigidbody2D_t3111785101), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2169[2] = 
{
	LeanSwipeRigidbody2D_t3111785101::get_offset_of_LayerMask_2(),
	LeanSwipeRigidbody2D_t3111785101::get_offset_of_ForceMultiplier_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2170 = { sizeof (LeanSwipeRigidbody2DNoRelease_t899216836), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2170[3] = 
{
	LeanSwipeRigidbody2DNoRelease_t899216836::get_offset_of_LayerMask_2(),
	LeanSwipeRigidbody2DNoRelease_t899216836::get_offset_of_ImpulseForce_3(),
	LeanSwipeRigidbody2DNoRelease_t899216836::get_offset_of_swipingFinger_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2171 = { sizeof (LeanSwipeRigidbody3D_t3111719565), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2171[2] = 
{
	LeanSwipeRigidbody3D_t3111719565::get_offset_of_LayerMask_2(),
	LeanSwipeRigidbody3D_t3111719565::get_offset_of_ForceMultiplier_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2172 = { sizeof (LeanSwipeRigidbody3DNoRelease_t1104278980), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2172[3] = 
{
	LeanSwipeRigidbody3DNoRelease_t1104278980::get_offset_of_LayerMask_2(),
	LeanSwipeRigidbody3DNoRelease_t1104278980::get_offset_of_ImpulseForce_3(),
	LeanSwipeRigidbody3DNoRelease_t1104278980::get_offset_of_swipingFinger_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2173 = { sizeof (LeanTapSelect_t839112588), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2173[3] = 
{
	LeanTapSelect_t839112588::get_offset_of_IgnoreGuiFingers_8(),
	LeanTapSelect_t839112588::get_offset_of_RequiredTapCount_9(),
	LeanTapSelect_t839112588::get_offset_of_RequiredTapInterval_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2174 = { sizeof (LeanTouchEvents_t1822248729), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2175 = { sizeof (LeanTranslate_t455865715), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2175[4] = 
{
	LeanTranslate_t455865715::get_offset_of_IgnoreGuiFingers_2(),
	LeanTranslate_t455865715::get_offset_of_RequiredFingerCount_3(),
	LeanTranslate_t455865715::get_offset_of_RequiredSelectable_4(),
	LeanTranslate_t455865715::get_offset_of_Camera_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2176 = { sizeof (LeanTranslateSmooth_t227688578), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2176[6] = 
{
	LeanTranslateSmooth_t227688578::get_offset_of_IgnoreGuiFingers_2(),
	LeanTranslateSmooth_t227688578::get_offset_of_RequiredFingerCount_3(),
	LeanTranslateSmooth_t227688578::get_offset_of_RequiredSelectable_4(),
	LeanTranslateSmooth_t227688578::get_offset_of_Camera_5(),
	LeanTranslateSmooth_t227688578::get_offset_of_Dampening_6(),
	LeanTranslateSmooth_t227688578::get_offset_of_RemainingDelta_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2177 = { sizeof (LeanFinger_t3506292858), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2177[12] = 
{
	LeanFinger_t3506292858::get_offset_of_Index_0(),
	LeanFinger_t3506292858::get_offset_of_Age_1(),
	LeanFinger_t3506292858::get_offset_of_Set_2(),
	LeanFinger_t3506292858::get_offset_of_LastSet_3(),
	LeanFinger_t3506292858::get_offset_of_Tap_4(),
	LeanFinger_t3506292858::get_offset_of_TapCount_5(),
	LeanFinger_t3506292858::get_offset_of_Swipe_6(),
	LeanFinger_t3506292858::get_offset_of_StartScreenPosition_7(),
	LeanFinger_t3506292858::get_offset_of_LastScreenPosition_8(),
	LeanFinger_t3506292858::get_offset_of_ScreenPosition_9(),
	LeanFinger_t3506292858::get_offset_of_StartedOverGui_10(),
	LeanFinger_t3506292858::get_offset_of_Snapshots_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2178 = { sizeof (LeanGesture_t1335127695), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2179 = { sizeof (LeanSelectable_t2178850769), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2179[6] = 
{
	LeanSelectable_t2178850769::get_offset_of_HideWithFinger_2(),
	LeanSelectable_t2178850769::get_offset_of_SelectingFinger_3(),
	LeanSelectable_t2178850769::get_offset_of_OnSelect_4(),
	LeanSelectable_t2178850769::get_offset_of_OnSelectUp_5(),
	LeanSelectable_t2178850769::get_offset_of_OnDeselect_6(),
	LeanSelectable_t2178850769::get_offset_of_isSelected_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2180 = { sizeof (LeanFingerEvent_t821904700), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2181 = { sizeof (LeanSnapshot_t4136513957), -1, sizeof(LeanSnapshot_t4136513957_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2181[3] = 
{
	LeanSnapshot_t4136513957::get_offset_of_Age_0(),
	LeanSnapshot_t4136513957::get_offset_of_ScreenPosition_1(),
	LeanSnapshot_t4136513957_StaticFields::get_offset_of_InactiveSnapshots_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2182 = { sizeof (LeanTouch_t2951860335), -1, sizeof(LeanTouch_t2951860335_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2182[28] = 
{
	LeanTouch_t2951860335_StaticFields::get_offset_of_Instances_2(),
	LeanTouch_t2951860335_StaticFields::get_offset_of_Fingers_3(),
	LeanTouch_t2951860335_StaticFields::get_offset_of_InactiveFingers_4(),
	LeanTouch_t2951860335_StaticFields::get_offset_of_OnFingerDown_5(),
	LeanTouch_t2951860335_StaticFields::get_offset_of_OnFingerSet_6(),
	LeanTouch_t2951860335_StaticFields::get_offset_of_OnFingerUp_7(),
	LeanTouch_t2951860335_StaticFields::get_offset_of_OnFingerTap_8(),
	LeanTouch_t2951860335_StaticFields::get_offset_of_OnFingerSwipe_9(),
	LeanTouch_t2951860335_StaticFields::get_offset_of_OnGesture_10(),
	LeanTouch_t2951860335::get_offset_of_TapThreshold_11(),
	0,
	LeanTouch_t2951860335::get_offset_of_SwipeThreshold_13(),
	0,
	LeanTouch_t2951860335::get_offset_of_ReferenceDpi_15(),
	0,
	LeanTouch_t2951860335::get_offset_of_GuiLayers_17(),
	LeanTouch_t2951860335::get_offset_of_RecordFingers_18(),
	LeanTouch_t2951860335::get_offset_of_RecordThreshold_19(),
	LeanTouch_t2951860335::get_offset_of_RecordLimit_20(),
	LeanTouch_t2951860335::get_offset_of_SimulateMultiFingers_21(),
	LeanTouch_t2951860335::get_offset_of_PinchTwistKey_22(),
	LeanTouch_t2951860335::get_offset_of_MultiDragKey_23(),
	LeanTouch_t2951860335::get_offset_of_FingerTexture_24(),
	LeanTouch_t2951860335_StaticFields::get_offset_of_highestMouseButton_25(),
	LeanTouch_t2951860335_StaticFields::get_offset_of_tempRaycastResults_26(),
	LeanTouch_t2951860335_StaticFields::get_offset_of_filteredFingers_27(),
	LeanTouch_t2951860335_StaticFields::get_offset_of_tempPointerEventData_28(),
	LeanTouch_t2951860335_StaticFields::get_offset_of_tempEventSystem_29(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2183 = { sizeof (iTweenPath_t76106739), -1, sizeof(iTweenPath_t76106739_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2183[8] = 
{
	iTweenPath_t76106739::get_offset_of_pathName_2(),
	iTweenPath_t76106739::get_offset_of_pathColor_3(),
	iTweenPath_t76106739::get_offset_of_nodes_4(),
	iTweenPath_t76106739::get_offset_of_nodeCount_5(),
	iTweenPath_t76106739_StaticFields::get_offset_of_paths_6(),
	iTweenPath_t76106739::get_offset_of_initialized_7(),
	iTweenPath_t76106739::get_offset_of_initialName_8(),
	iTweenPath_t76106739::get_offset_of_pathVisible_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2184 = { sizeof (RotationAdjuster_t35632371), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2185 = { sizeof (AMIA_t4201455126), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2186 = { sizeof (AndroidData_t2576415101), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2186[8] = 
{
	AndroidData_t2576415101::get_offset_of_id_0(),
	AndroidData_t2576415101::get_offset_of_url_1(),
	AndroidData_t2576415101::get_offset_of_model_2(),
	AndroidData_t2576415101::get_offset_of_scene_3(),
	AndroidData_t2576415101::get_offset_of_thumb_4(),
	AndroidData_t2576415101::get_offset_of_title_5(),
	AndroidData_t2576415101::get_offset_of_description_6(),
	AndroidData_t2576415101::get_offset_of_structure_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2187 = { sizeof (ApplicationModel_t3549425075), -1, sizeof(ApplicationModel_t3549425075_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2187[15] = 
{
	ApplicationModel_t3549425075_StaticFields::get_offset_of_url_0(),
	ApplicationModel_t3549425075_StaticFields::get_offset_of_model_1(),
	ApplicationModel_t3549425075_StaticFields::get_offset_of_mapFilePath_2(),
	ApplicationModel_t3549425075_StaticFields::get_offset_of_mapFileName_3(),
	ApplicationModel_t3549425075_StaticFields::get_offset_of_structure_4(),
	ApplicationModel_t3549425075_StaticFields::get_offset_of_bundle_5(),
	ApplicationModel_t3549425075_StaticFields::get_offset_of_gameobjects_6(),
	ApplicationModel_t3549425075_StaticFields::get_offset_of_gameObjectToLoad_7(),
	ApplicationModel_t3549425075_StaticFields::get_offset_of_oldx_8(),
	ApplicationModel_t3549425075_StaticFields::get_offset_of_oldy_9(),
	ApplicationModel_t3549425075_StaticFields::get_offset_of_oldz_10(),
	ApplicationModel_t3549425075_StaticFields::get_offset_of_oldEulerAngles_11(),
	ApplicationModel_t3549425075_StaticFields::get_offset_of_arAnimation_12(),
	ApplicationModel_t3549425075_StaticFields::get_offset_of_clips_13(),
	ApplicationModel_t3549425075_StaticFields::get_offset_of_images_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2188 = { sizeof (BottomNav_t1719165380), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2188[3] = 
{
	BottomNav_t1719165380::get_offset_of_panel_2(),
	BottomNav_t1719165380::get_offset_of_anim_3(),
	BottomNav_t1719165380::get_offset_of_isPaused_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2189 = { sizeof (Branch_t1428165248), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2189[19] = 
{
	Branch_t1428165248::get_offset_of_U3CNameU3Ek__BackingField_0(),
	Branch_t1428165248::get_offset_of_U3CDescriptionU3Ek__BackingField_1(),
	Branch_t1428165248::get_offset_of_U3CTypeU3Ek__BackingField_2(),
	Branch_t1428165248::get_offset_of_U3CAssetU3Ek__BackingField_3(),
	Branch_t1428165248::get_offset_of_U3CParentU3Ek__BackingField_4(),
	Branch_t1428165248::get_offset_of_U3CChildrenU3Ek__BackingField_5(),
	Branch_t1428165248::get_offset_of_isRoot_6(),
	Branch_t1428165248::get_offset_of_iconSprite_7(),
	Branch_t1428165248::get_offset_of_isAnimation_8(),
	Branch_t1428165248::get_offset_of_isHide_9(),
	Branch_t1428165248::get_offset_of_animationTrigger_10(),
	Branch_t1428165248::get_offset_of_path_11(),
	Branch_t1428165248::get_offset_of_pdate_12(),
	Branch_t1428165248::get_offset_of_mesh_13(),
	Branch_t1428165248::get_offset_of_showLabel_14(),
	Branch_t1428165248::get_offset_of_steps_15(),
	Branch_t1428165248::get_offset_of_hides_16(),
	Branch_t1428165248::get_offset_of_isStepAnimation_17(),
	Branch_t1428165248::get_offset_of_isActive_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2190 = { sizeof (CenterScript_t2028411258), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2190[9] = 
{
	CenterScript_t2028411258::get_offset_of_cameraZoom_2(),
	CenterScript_t2028411258::get_offset_of_cameraZoomUI_3(),
	CenterScript_t2028411258::get_offset_of_SmoothOrbitCam_4(),
	CenterScript_t2028411258::get_offset_of_mainCameraTransform_5(),
	CenterScript_t2028411258::get_offset_of_leanCameraMoveSmooth_6(),
	CenterScript_t2028411258::get_offset_of_originalZoom_7(),
	CenterScript_t2028411258::get_offset_of_originalZoomUI_8(),
	CenterScript_t2028411258::get_offset_of_originalposition_9(),
	CenterScript_t2028411258::get_offset_of_animationLauncher_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2191 = { sizeof (U3CHandleItU3Ec__Iterator0_t3758345359), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2191[4] = 
{
	U3CHandleItU3Ec__Iterator0_t3758345359::get_offset_of_U24this_0(),
	U3CHandleItU3Ec__Iterator0_t3758345359::get_offset_of_U24current_1(),
	U3CHandleItU3Ec__Iterator0_t3758345359::get_offset_of_U24disposing_2(),
	U3CHandleItU3Ec__Iterator0_t3758345359::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2192 = { sizeof (FakeApplicationModel_t3022998755), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2192[2] = 
{
	FakeApplicationModel_t3022998755::get_offset_of_url_2(),
	FakeApplicationModel_t3022998755::get_offset_of_model_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2193 = { sizeof (GManager_t3062881246), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2193[2] = 
{
	GManager_t3062881246::get_offset_of_Chapter_2(),
	GManager_t3062881246::get_offset_of_Locking_state_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2194 = { sizeof (Board_Manager_t347073597), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2195 = { sizeof (GameManager_t1536523654), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2196 = { sizeof (GetOutCard_t3730830718), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2197 = { sizeof (Image_t1825575977), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2197[1] = 
{
	Image_t1825575977::get_offset_of_image_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2198 = { sizeof (InitSceneController_t315947004), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2198[5] = 
{
	InitSceneController_t315947004::get_offset_of_imageComp_2(),
	InitSceneController_t315947004::get_offset_of_text_3(),
	InitSceneController_t315947004::get_offset_of_json_4(),
	InitSceneController_t315947004::get_offset_of_readyToLoadScene_5(),
	InitSceneController_t315947004::get_offset_of_www_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2199 = { sizeof (U3CloadAssetBundleU3Ec__Iterator0_t4011785967), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2199[9] = 
{
	U3CloadAssetBundleU3Ec__Iterator0_t4011785967::get_offset_of_U3CurlU3E__0_0(),
	U3CloadAssetBundleU3Ec__Iterator0_t4011785967::get_offset_of_U3CbundleGameObjectRequestU3E__0_1(),
	U3CloadAssetBundleU3Ec__Iterator0_t4011785967::get_offset_of_U3CbundleVideoClipRequestU3E__0_2(),
	U3CloadAssetBundleU3Ec__Iterator0_t4011785967::get_offset_of_U3CbundleImagesRequestU3E__0_3(),
	U3CloadAssetBundleU3Ec__Iterator0_t4011785967::get_offset_of_jsonObj_4(),
	U3CloadAssetBundleU3Ec__Iterator0_t4011785967::get_offset_of_U24this_5(),
	U3CloadAssetBundleU3Ec__Iterator0_t4011785967::get_offset_of_U24current_6(),
	U3CloadAssetBundleU3Ec__Iterator0_t4011785967::get_offset_of_U24disposing_7(),
	U3CloadAssetBundleU3Ec__Iterator0_t4011785967::get_offset_of_U24PC_8(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
