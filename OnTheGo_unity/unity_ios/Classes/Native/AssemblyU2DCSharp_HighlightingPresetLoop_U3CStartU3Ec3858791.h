﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"
#include "AssemblyU2DCSharpU2Dfirstpass_HighlightingSystem_Hi635619791.h"

// System.Collections.ObjectModel.ReadOnlyCollection`1<HighlightingSystem.HighlightingPreset>
struct ReadOnlyCollection_1_t1848196078;
// HighlightingPresetLoop
struct HighlightingPresetLoop_t1609534497;
// System.Object
struct Il2CppObject;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HighlightingPresetLoop/<Start>c__Iterator0
struct  U3CStartU3Ec__Iterator0_t3858791  : public Il2CppObject
{
public:
	// System.Int32 HighlightingPresetLoop/<Start>c__Iterator0::<index>__0
	int32_t ___U3CindexU3E__0_0;
	// System.Collections.ObjectModel.ReadOnlyCollection`1<HighlightingSystem.HighlightingPreset> HighlightingPresetLoop/<Start>c__Iterator0::<presets>__1
	ReadOnlyCollection_1_t1848196078 * ___U3CpresetsU3E__1_1;
	// HighlightingSystem.HighlightingPreset HighlightingPresetLoop/<Start>c__Iterator0::<preset>__1
	HighlightingPreset_t635619791  ___U3CpresetU3E__1_2;
	// HighlightingPresetLoop HighlightingPresetLoop/<Start>c__Iterator0::$this
	HighlightingPresetLoop_t1609534497 * ___U24this_3;
	// System.Object HighlightingPresetLoop/<Start>c__Iterator0::$current
	Il2CppObject * ___U24current_4;
	// System.Boolean HighlightingPresetLoop/<Start>c__Iterator0::$disposing
	bool ___U24disposing_5;
	// System.Int32 HighlightingPresetLoop/<Start>c__Iterator0::$PC
	int32_t ___U24PC_6;

public:
	inline static int32_t get_offset_of_U3CindexU3E__0_0() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t3858791, ___U3CindexU3E__0_0)); }
	inline int32_t get_U3CindexU3E__0_0() const { return ___U3CindexU3E__0_0; }
	inline int32_t* get_address_of_U3CindexU3E__0_0() { return &___U3CindexU3E__0_0; }
	inline void set_U3CindexU3E__0_0(int32_t value)
	{
		___U3CindexU3E__0_0 = value;
	}

	inline static int32_t get_offset_of_U3CpresetsU3E__1_1() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t3858791, ___U3CpresetsU3E__1_1)); }
	inline ReadOnlyCollection_1_t1848196078 * get_U3CpresetsU3E__1_1() const { return ___U3CpresetsU3E__1_1; }
	inline ReadOnlyCollection_1_t1848196078 ** get_address_of_U3CpresetsU3E__1_1() { return &___U3CpresetsU3E__1_1; }
	inline void set_U3CpresetsU3E__1_1(ReadOnlyCollection_1_t1848196078 * value)
	{
		___U3CpresetsU3E__1_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CpresetsU3E__1_1, value);
	}

	inline static int32_t get_offset_of_U3CpresetU3E__1_2() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t3858791, ___U3CpresetU3E__1_2)); }
	inline HighlightingPreset_t635619791  get_U3CpresetU3E__1_2() const { return ___U3CpresetU3E__1_2; }
	inline HighlightingPreset_t635619791 * get_address_of_U3CpresetU3E__1_2() { return &___U3CpresetU3E__1_2; }
	inline void set_U3CpresetU3E__1_2(HighlightingPreset_t635619791  value)
	{
		___U3CpresetU3E__1_2 = value;
	}

	inline static int32_t get_offset_of_U24this_3() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t3858791, ___U24this_3)); }
	inline HighlightingPresetLoop_t1609534497 * get_U24this_3() const { return ___U24this_3; }
	inline HighlightingPresetLoop_t1609534497 ** get_address_of_U24this_3() { return &___U24this_3; }
	inline void set_U24this_3(HighlightingPresetLoop_t1609534497 * value)
	{
		___U24this_3 = value;
		Il2CppCodeGenWriteBarrier(&___U24this_3, value);
	}

	inline static int32_t get_offset_of_U24current_4() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t3858791, ___U24current_4)); }
	inline Il2CppObject * get_U24current_4() const { return ___U24current_4; }
	inline Il2CppObject ** get_address_of_U24current_4() { return &___U24current_4; }
	inline void set_U24current_4(Il2CppObject * value)
	{
		___U24current_4 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_4, value);
	}

	inline static int32_t get_offset_of_U24disposing_5() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t3858791, ___U24disposing_5)); }
	inline bool get_U24disposing_5() const { return ___U24disposing_5; }
	inline bool* get_address_of_U24disposing_5() { return &___U24disposing_5; }
	inline void set_U24disposing_5(bool value)
	{
		___U24disposing_5 = value;
	}

	inline static int32_t get_offset_of_U24PC_6() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t3858791, ___U24PC_6)); }
	inline int32_t get_U24PC_6() const { return ___U24PC_6; }
	inline int32_t* get_address_of_U24PC_6() { return &___U24PC_6; }
	inline void set_U24PC_6(int32_t value)
	{
		___U24PC_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
