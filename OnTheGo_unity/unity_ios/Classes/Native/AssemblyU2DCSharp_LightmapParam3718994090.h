﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour3962482529.h"
#include "UnityEngine_UnityEngine_Vector43319028937.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LightmapParam
struct  LightmapParam_t3718994090  : public MonoBehaviour_t3962482529
{
public:
	// System.Int32 LightmapParam::lightmapIndex
	int32_t ___lightmapIndex_2;
	// UnityEngine.Vector4 LightmapParam::lightmapScaleOffset
	Vector4_t3319028937  ___lightmapScaleOffset_3;

public:
	inline static int32_t get_offset_of_lightmapIndex_2() { return static_cast<int32_t>(offsetof(LightmapParam_t3718994090, ___lightmapIndex_2)); }
	inline int32_t get_lightmapIndex_2() const { return ___lightmapIndex_2; }
	inline int32_t* get_address_of_lightmapIndex_2() { return &___lightmapIndex_2; }
	inline void set_lightmapIndex_2(int32_t value)
	{
		___lightmapIndex_2 = value;
	}

	inline static int32_t get_offset_of_lightmapScaleOffset_3() { return static_cast<int32_t>(offsetof(LightmapParam_t3718994090, ___lightmapScaleOffset_3)); }
	inline Vector4_t3319028937  get_lightmapScaleOffset_3() const { return ___lightmapScaleOffset_3; }
	inline Vector4_t3319028937 * get_address_of_lightmapScaleOffset_3() { return &___lightmapScaleOffset_3; }
	inline void set_lightmapScaleOffset_3(Vector4_t3319028937  value)
	{
		___lightmapScaleOffset_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
