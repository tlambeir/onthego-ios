﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour3962482529.h"

// UnityEngine.UI.Dropdown
struct Dropdown_t2274391225;
// HighlightingSystem.HighlightingRenderer
struct HighlightingRenderer_t1923179915;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PresetSelector
struct  PresetSelector_t2134829519  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.UI.Dropdown PresetSelector::dropdown
	Dropdown_t2274391225 * ___dropdown_2;
	// HighlightingSystem.HighlightingRenderer PresetSelector::hr
	HighlightingRenderer_t1923179915 * ___hr_3;

public:
	inline static int32_t get_offset_of_dropdown_2() { return static_cast<int32_t>(offsetof(PresetSelector_t2134829519, ___dropdown_2)); }
	inline Dropdown_t2274391225 * get_dropdown_2() const { return ___dropdown_2; }
	inline Dropdown_t2274391225 ** get_address_of_dropdown_2() { return &___dropdown_2; }
	inline void set_dropdown_2(Dropdown_t2274391225 * value)
	{
		___dropdown_2 = value;
		Il2CppCodeGenWriteBarrier(&___dropdown_2, value);
	}

	inline static int32_t get_offset_of_hr_3() { return static_cast<int32_t>(offsetof(PresetSelector_t2134829519, ___hr_3)); }
	inline HighlightingRenderer_t1923179915 * get_hr_3() const { return ___hr_3; }
	inline HighlightingRenderer_t1923179915 ** get_address_of_hr_3() { return &___hr_3; }
	inline void set_hr_3(HighlightingRenderer_t1923179915 * value)
	{
		___hr_3 = value;
		Il2CppCodeGenWriteBarrier(&___hr_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
