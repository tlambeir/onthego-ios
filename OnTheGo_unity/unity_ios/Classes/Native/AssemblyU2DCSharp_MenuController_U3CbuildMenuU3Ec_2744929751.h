﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// UnityEngine.GameObject
struct GameObject_t1113636619;
// MenuController/<buildMenu>c__AnonStorey1
struct U3CbuildMenuU3Ec__AnonStorey1_t23940567;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MenuController/<buildMenu>c__AnonStorey2
struct  U3CbuildMenuU3Ec__AnonStorey2_t2744929751  : public Il2CppObject
{
public:
	// UnityEngine.GameObject MenuController/<buildMenu>c__AnonStorey2::backToTopmesh
	GameObject_t1113636619 * ___backToTopmesh_0;
	// MenuController/<buildMenu>c__AnonStorey1 MenuController/<buildMenu>c__AnonStorey2::<>f__ref$1
	U3CbuildMenuU3Ec__AnonStorey1_t23940567 * ___U3CU3Ef__refU241_1;

public:
	inline static int32_t get_offset_of_backToTopmesh_0() { return static_cast<int32_t>(offsetof(U3CbuildMenuU3Ec__AnonStorey2_t2744929751, ___backToTopmesh_0)); }
	inline GameObject_t1113636619 * get_backToTopmesh_0() const { return ___backToTopmesh_0; }
	inline GameObject_t1113636619 ** get_address_of_backToTopmesh_0() { return &___backToTopmesh_0; }
	inline void set_backToTopmesh_0(GameObject_t1113636619 * value)
	{
		___backToTopmesh_0 = value;
		Il2CppCodeGenWriteBarrier(&___backToTopmesh_0, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__refU241_1() { return static_cast<int32_t>(offsetof(U3CbuildMenuU3Ec__AnonStorey2_t2744929751, ___U3CU3Ef__refU241_1)); }
	inline U3CbuildMenuU3Ec__AnonStorey1_t23940567 * get_U3CU3Ef__refU241_1() const { return ___U3CU3Ef__refU241_1; }
	inline U3CbuildMenuU3Ec__AnonStorey1_t23940567 ** get_address_of_U3CU3Ef__refU241_1() { return &___U3CU3Ef__refU241_1; }
	inline void set_U3CU3Ef__refU241_1(U3CbuildMenuU3Ec__AnonStorey1_t23940567 * value)
	{
		___U3CU3Ef__refU241_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__refU241_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
