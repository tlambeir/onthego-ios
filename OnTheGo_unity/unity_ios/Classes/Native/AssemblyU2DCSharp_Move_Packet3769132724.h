﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour3962482529.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Move_Packet
struct  Move_Packet_t3769132724  : public MonoBehaviour_t3962482529
{
public:
	// System.Int32 Move_Packet::cycles
	int32_t ___cycles_2;

public:
	inline static int32_t get_offset_of_cycles_2() { return static_cast<int32_t>(offsetof(Move_Packet_t3769132724, ___cycles_2)); }
	inline int32_t get_cycles_2() const { return ___cycles_2; }
	inline int32_t* get_address_of_cycles_2() { return &___cycles_2; }
	inline void set_cycles_2(int32_t value)
	{
		___cycles_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
