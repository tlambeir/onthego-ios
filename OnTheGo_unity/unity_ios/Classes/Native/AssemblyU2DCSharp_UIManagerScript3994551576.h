﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour3962482529.h"

// UnityEngine.Animator
struct Animator_t434523843;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIManagerScript
struct  UIManagerScript_t3994551576  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Animator UIManagerScript::contentPanel
	Animator_t434523843 * ___contentPanel_2;

public:
	inline static int32_t get_offset_of_contentPanel_2() { return static_cast<int32_t>(offsetof(UIManagerScript_t3994551576, ___contentPanel_2)); }
	inline Animator_t434523843 * get_contentPanel_2() const { return ___contentPanel_2; }
	inline Animator_t434523843 ** get_address_of_contentPanel_2() { return &___contentPanel_2; }
	inline void set_contentPanel_2(Animator_t434523843 * value)
	{
		___contentPanel_2 = value;
		Il2CppCodeGenWriteBarrier(&___contentPanel_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
