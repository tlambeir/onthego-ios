﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HighlightingSystem.ShaderPropertyID
struct  ShaderPropertyID_t567755140  : public Il2CppObject
{
public:
	// System.Int32 HighlightingSystem.ShaderPropertyID::_MainTex
	int32_t ____MainTex_0;
	// System.Int32 HighlightingSystem.ShaderPropertyID::_Color
	int32_t ____Color_1;
	// System.Int32 HighlightingSystem.ShaderPropertyID::_Cutoff
	int32_t ____Cutoff_2;
	// System.Int32 HighlightingSystem.ShaderPropertyID::_Intensity
	int32_t ____Intensity_3;
	// System.Int32 HighlightingSystem.ShaderPropertyID::_Cull
	int32_t ____Cull_4;
	// System.Int32 HighlightingSystem.ShaderPropertyID::_HighlightingBlur1
	int32_t ____HighlightingBlur1_5;
	// System.Int32 HighlightingSystem.ShaderPropertyID::_HighlightingBlur2
	int32_t ____HighlightingBlur2_6;
	// System.Int32 HighlightingSystem.ShaderPropertyID::_HighlightingBuffer
	int32_t ____HighlightingBuffer_7;
	// System.Int32 HighlightingSystem.ShaderPropertyID::_HighlightingBlurOffset
	int32_t ____HighlightingBlurOffset_8;

public:
	inline static int32_t get_offset_of__MainTex_0() { return static_cast<int32_t>(offsetof(ShaderPropertyID_t567755140, ____MainTex_0)); }
	inline int32_t get__MainTex_0() const { return ____MainTex_0; }
	inline int32_t* get_address_of__MainTex_0() { return &____MainTex_0; }
	inline void set__MainTex_0(int32_t value)
	{
		____MainTex_0 = value;
	}

	inline static int32_t get_offset_of__Color_1() { return static_cast<int32_t>(offsetof(ShaderPropertyID_t567755140, ____Color_1)); }
	inline int32_t get__Color_1() const { return ____Color_1; }
	inline int32_t* get_address_of__Color_1() { return &____Color_1; }
	inline void set__Color_1(int32_t value)
	{
		____Color_1 = value;
	}

	inline static int32_t get_offset_of__Cutoff_2() { return static_cast<int32_t>(offsetof(ShaderPropertyID_t567755140, ____Cutoff_2)); }
	inline int32_t get__Cutoff_2() const { return ____Cutoff_2; }
	inline int32_t* get_address_of__Cutoff_2() { return &____Cutoff_2; }
	inline void set__Cutoff_2(int32_t value)
	{
		____Cutoff_2 = value;
	}

	inline static int32_t get_offset_of__Intensity_3() { return static_cast<int32_t>(offsetof(ShaderPropertyID_t567755140, ____Intensity_3)); }
	inline int32_t get__Intensity_3() const { return ____Intensity_3; }
	inline int32_t* get_address_of__Intensity_3() { return &____Intensity_3; }
	inline void set__Intensity_3(int32_t value)
	{
		____Intensity_3 = value;
	}

	inline static int32_t get_offset_of__Cull_4() { return static_cast<int32_t>(offsetof(ShaderPropertyID_t567755140, ____Cull_4)); }
	inline int32_t get__Cull_4() const { return ____Cull_4; }
	inline int32_t* get_address_of__Cull_4() { return &____Cull_4; }
	inline void set__Cull_4(int32_t value)
	{
		____Cull_4 = value;
	}

	inline static int32_t get_offset_of__HighlightingBlur1_5() { return static_cast<int32_t>(offsetof(ShaderPropertyID_t567755140, ____HighlightingBlur1_5)); }
	inline int32_t get__HighlightingBlur1_5() const { return ____HighlightingBlur1_5; }
	inline int32_t* get_address_of__HighlightingBlur1_5() { return &____HighlightingBlur1_5; }
	inline void set__HighlightingBlur1_5(int32_t value)
	{
		____HighlightingBlur1_5 = value;
	}

	inline static int32_t get_offset_of__HighlightingBlur2_6() { return static_cast<int32_t>(offsetof(ShaderPropertyID_t567755140, ____HighlightingBlur2_6)); }
	inline int32_t get__HighlightingBlur2_6() const { return ____HighlightingBlur2_6; }
	inline int32_t* get_address_of__HighlightingBlur2_6() { return &____HighlightingBlur2_6; }
	inline void set__HighlightingBlur2_6(int32_t value)
	{
		____HighlightingBlur2_6 = value;
	}

	inline static int32_t get_offset_of__HighlightingBuffer_7() { return static_cast<int32_t>(offsetof(ShaderPropertyID_t567755140, ____HighlightingBuffer_7)); }
	inline int32_t get__HighlightingBuffer_7() const { return ____HighlightingBuffer_7; }
	inline int32_t* get_address_of__HighlightingBuffer_7() { return &____HighlightingBuffer_7; }
	inline void set__HighlightingBuffer_7(int32_t value)
	{
		____HighlightingBuffer_7 = value;
	}

	inline static int32_t get_offset_of__HighlightingBlurOffset_8() { return static_cast<int32_t>(offsetof(ShaderPropertyID_t567755140, ____HighlightingBlurOffset_8)); }
	inline int32_t get__HighlightingBlurOffset_8() const { return ____HighlightingBlurOffset_8; }
	inline int32_t* get_address_of__HighlightingBlurOffset_8() { return &____HighlightingBlurOffset_8; }
	inline void set__HighlightingBlurOffset_8(int32_t value)
	{
		____HighlightingBlurOffset_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
