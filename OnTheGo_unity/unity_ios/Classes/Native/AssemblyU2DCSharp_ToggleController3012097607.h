﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour3962482529.h"

// System.Collections.Generic.List`1<TransparancyManager>
struct List_1_t1790930258;
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t3328599146;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ToggleController
struct  ToggleController_t3012097607  : public MonoBehaviour_t3962482529
{
public:
	// System.Collections.Generic.List`1<TransparancyManager> ToggleController::components
	List_1_t1790930258 * ___components_2;
	// UnityEngine.GameObject[] ToggleController::RackList
	GameObjectU5BU5D_t3328599146* ___RackList_3;
	// UnityEngine.GameObject[] ToggleController::AMIAList
	GameObjectU5BU5D_t3328599146* ___AMIAList_4;
	// UnityEngine.GameObject[] ToggleController::ABIAList
	GameObjectU5BU5D_t3328599146* ___ABIAList_5;
	// UnityEngine.GameObject[] ToggleController::ASIAList
	GameObjectU5BU5D_t3328599146* ___ASIAList_6;

public:
	inline static int32_t get_offset_of_components_2() { return static_cast<int32_t>(offsetof(ToggleController_t3012097607, ___components_2)); }
	inline List_1_t1790930258 * get_components_2() const { return ___components_2; }
	inline List_1_t1790930258 ** get_address_of_components_2() { return &___components_2; }
	inline void set_components_2(List_1_t1790930258 * value)
	{
		___components_2 = value;
		Il2CppCodeGenWriteBarrier(&___components_2, value);
	}

	inline static int32_t get_offset_of_RackList_3() { return static_cast<int32_t>(offsetof(ToggleController_t3012097607, ___RackList_3)); }
	inline GameObjectU5BU5D_t3328599146* get_RackList_3() const { return ___RackList_3; }
	inline GameObjectU5BU5D_t3328599146** get_address_of_RackList_3() { return &___RackList_3; }
	inline void set_RackList_3(GameObjectU5BU5D_t3328599146* value)
	{
		___RackList_3 = value;
		Il2CppCodeGenWriteBarrier(&___RackList_3, value);
	}

	inline static int32_t get_offset_of_AMIAList_4() { return static_cast<int32_t>(offsetof(ToggleController_t3012097607, ___AMIAList_4)); }
	inline GameObjectU5BU5D_t3328599146* get_AMIAList_4() const { return ___AMIAList_4; }
	inline GameObjectU5BU5D_t3328599146** get_address_of_AMIAList_4() { return &___AMIAList_4; }
	inline void set_AMIAList_4(GameObjectU5BU5D_t3328599146* value)
	{
		___AMIAList_4 = value;
		Il2CppCodeGenWriteBarrier(&___AMIAList_4, value);
	}

	inline static int32_t get_offset_of_ABIAList_5() { return static_cast<int32_t>(offsetof(ToggleController_t3012097607, ___ABIAList_5)); }
	inline GameObjectU5BU5D_t3328599146* get_ABIAList_5() const { return ___ABIAList_5; }
	inline GameObjectU5BU5D_t3328599146** get_address_of_ABIAList_5() { return &___ABIAList_5; }
	inline void set_ABIAList_5(GameObjectU5BU5D_t3328599146* value)
	{
		___ABIAList_5 = value;
		Il2CppCodeGenWriteBarrier(&___ABIAList_5, value);
	}

	inline static int32_t get_offset_of_ASIAList_6() { return static_cast<int32_t>(offsetof(ToggleController_t3012097607, ___ASIAList_6)); }
	inline GameObjectU5BU5D_t3328599146* get_ASIAList_6() const { return ___ASIAList_6; }
	inline GameObjectU5BU5D_t3328599146** get_address_of_ASIAList_6() { return &___ASIAList_6; }
	inline void set_ASIAList_6(GameObjectU5BU5D_t3328599146* value)
	{
		___ASIAList_6 = value;
		Il2CppCodeGenWriteBarrier(&___ASIAList_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
