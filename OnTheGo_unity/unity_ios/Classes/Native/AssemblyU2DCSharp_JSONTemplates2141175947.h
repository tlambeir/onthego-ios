﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.Collections.Generic.HashSet`1<System.Object>
struct HashSet_1_t1645055638;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// JSONTemplates
struct  JSONTemplates_t2141175947  : public Il2CppObject
{
public:

public:
};

struct JSONTemplates_t2141175947_StaticFields
{
public:
	// System.Collections.Generic.HashSet`1<System.Object> JSONTemplates::touched
	HashSet_1_t1645055638 * ___touched_0;

public:
	inline static int32_t get_offset_of_touched_0() { return static_cast<int32_t>(offsetof(JSONTemplates_t2141175947_StaticFields, ___touched_0)); }
	inline HashSet_1_t1645055638 * get_touched_0() const { return ___touched_0; }
	inline HashSet_1_t1645055638 ** get_address_of_touched_0() { return &___touched_0; }
	inline void set_touched_0(HashSet_1_t1645055638 * value)
	{
		___touched_0 = value;
		Il2CppCodeGenWriteBarrier(&___touched_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
