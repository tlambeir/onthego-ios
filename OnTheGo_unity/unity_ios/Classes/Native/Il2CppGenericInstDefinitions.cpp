﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"





extern const Il2CppType Il2CppObject_0_0_0;
static const Il2CppType* GenInst_Il2CppObject_0_0_0_Types[] = { &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0 = { 1, GenInst_Il2CppObject_0_0_0_Types };
extern const Il2CppType Int32_t2950945753_0_0_0;
static const Il2CppType* GenInst_Int32_t2950945753_0_0_0_Types[] = { &Int32_t2950945753_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2950945753_0_0_0 = { 1, GenInst_Int32_t2950945753_0_0_0_Types };
extern const Il2CppType Char_t3634460470_0_0_0;
static const Il2CppType* GenInst_Char_t3634460470_0_0_0_Types[] = { &Char_t3634460470_0_0_0 };
extern const Il2CppGenericInst GenInst_Char_t3634460470_0_0_0 = { 1, GenInst_Char_t3634460470_0_0_0_Types };
extern const Il2CppType Int64_t3736567304_0_0_0;
static const Il2CppType* GenInst_Int64_t3736567304_0_0_0_Types[] = { &Int64_t3736567304_0_0_0 };
extern const Il2CppGenericInst GenInst_Int64_t3736567304_0_0_0 = { 1, GenInst_Int64_t3736567304_0_0_0_Types };
extern const Il2CppType UInt32_t2560061978_0_0_0;
static const Il2CppType* GenInst_UInt32_t2560061978_0_0_0_Types[] = { &UInt32_t2560061978_0_0_0 };
extern const Il2CppGenericInst GenInst_UInt32_t2560061978_0_0_0 = { 1, GenInst_UInt32_t2560061978_0_0_0_Types };
extern const Il2CppType UInt64_t4134040092_0_0_0;
static const Il2CppType* GenInst_UInt64_t4134040092_0_0_0_Types[] = { &UInt64_t4134040092_0_0_0 };
extern const Il2CppGenericInst GenInst_UInt64_t4134040092_0_0_0 = { 1, GenInst_UInt64_t4134040092_0_0_0_Types };
extern const Il2CppType Byte_t1134296376_0_0_0;
static const Il2CppType* GenInst_Byte_t1134296376_0_0_0_Types[] = { &Byte_t1134296376_0_0_0 };
extern const Il2CppGenericInst GenInst_Byte_t1134296376_0_0_0 = { 1, GenInst_Byte_t1134296376_0_0_0_Types };
extern const Il2CppType SByte_t1669577662_0_0_0;
static const Il2CppType* GenInst_SByte_t1669577662_0_0_0_Types[] = { &SByte_t1669577662_0_0_0 };
extern const Il2CppGenericInst GenInst_SByte_t1669577662_0_0_0 = { 1, GenInst_SByte_t1669577662_0_0_0_Types };
extern const Il2CppType Int16_t2552820387_0_0_0;
static const Il2CppType* GenInst_Int16_t2552820387_0_0_0_Types[] = { &Int16_t2552820387_0_0_0 };
extern const Il2CppGenericInst GenInst_Int16_t2552820387_0_0_0 = { 1, GenInst_Int16_t2552820387_0_0_0_Types };
extern const Il2CppType UInt16_t2177724958_0_0_0;
static const Il2CppType* GenInst_UInt16_t2177724958_0_0_0_Types[] = { &UInt16_t2177724958_0_0_0 };
extern const Il2CppGenericInst GenInst_UInt16_t2177724958_0_0_0 = { 1, GenInst_UInt16_t2177724958_0_0_0_Types };
extern const Il2CppType String_t_0_0_0;
static const Il2CppType* GenInst_String_t_0_0_0_Types[] = { &String_t_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0 = { 1, GenInst_String_t_0_0_0_Types };
extern const Il2CppType IConvertible_t2977365677_0_0_0;
static const Il2CppType* GenInst_IConvertible_t2977365677_0_0_0_Types[] = { &IConvertible_t2977365677_0_0_0 };
extern const Il2CppGenericInst GenInst_IConvertible_t2977365677_0_0_0 = { 1, GenInst_IConvertible_t2977365677_0_0_0_Types };
extern const Il2CppType IComparable_t36111218_0_0_0;
static const Il2CppType* GenInst_IComparable_t36111218_0_0_0_Types[] = { &IComparable_t36111218_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_t36111218_0_0_0 = { 1, GenInst_IComparable_t36111218_0_0_0_Types };
extern const Il2CppType IEnumerable_t1941168011_0_0_0;
static const Il2CppType* GenInst_IEnumerable_t1941168011_0_0_0_Types[] = { &IEnumerable_t1941168011_0_0_0 };
extern const Il2CppGenericInst GenInst_IEnumerable_t1941168011_0_0_0 = { 1, GenInst_IEnumerable_t1941168011_0_0_0_Types };
extern const Il2CppType ICloneable_t724424198_0_0_0;
static const Il2CppType* GenInst_ICloneable_t724424198_0_0_0_Types[] = { &ICloneable_t724424198_0_0_0 };
extern const Il2CppGenericInst GenInst_ICloneable_t724424198_0_0_0 = { 1, GenInst_ICloneable_t724424198_0_0_0_Types };
extern const Il2CppType IComparable_1_t1216115102_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t1216115102_0_0_0_Types[] = { &IComparable_1_t1216115102_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t1216115102_0_0_0 = { 1, GenInst_IComparable_1_t1216115102_0_0_0_Types };
extern const Il2CppType IEquatable_1_t2738596416_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t2738596416_0_0_0_Types[] = { &IEquatable_1_t2738596416_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t2738596416_0_0_0 = { 1, GenInst_IEquatable_1_t2738596416_0_0_0_Types };
extern const Il2CppType Type_t_0_0_0;
static const Il2CppType* GenInst_Type_t_0_0_0_Types[] = { &Type_t_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0 = { 1, GenInst_Type_t_0_0_0_Types };
extern const Il2CppType IReflect_t2554276939_0_0_0;
static const Il2CppType* GenInst_IReflect_t2554276939_0_0_0_Types[] = { &IReflect_t2554276939_0_0_0 };
extern const Il2CppGenericInst GenInst_IReflect_t2554276939_0_0_0 = { 1, GenInst_IReflect_t2554276939_0_0_0_Types };
extern const Il2CppType _Type_t3588564251_0_0_0;
static const Il2CppType* GenInst__Type_t3588564251_0_0_0_Types[] = { &_Type_t3588564251_0_0_0 };
extern const Il2CppGenericInst GenInst__Type_t3588564251_0_0_0 = { 1, GenInst__Type_t3588564251_0_0_0_Types };
extern const Il2CppType MemberInfo_t_0_0_0;
static const Il2CppType* GenInst_MemberInfo_t_0_0_0_Types[] = { &MemberInfo_t_0_0_0 };
extern const Il2CppGenericInst GenInst_MemberInfo_t_0_0_0 = { 1, GenInst_MemberInfo_t_0_0_0_Types };
extern const Il2CppType ICustomAttributeProvider_t1530824137_0_0_0;
static const Il2CppType* GenInst_ICustomAttributeProvider_t1530824137_0_0_0_Types[] = { &ICustomAttributeProvider_t1530824137_0_0_0 };
extern const Il2CppGenericInst GenInst_ICustomAttributeProvider_t1530824137_0_0_0 = { 1, GenInst_ICustomAttributeProvider_t1530824137_0_0_0_Types };
extern const Il2CppType _MemberInfo_t3922476713_0_0_0;
static const Il2CppType* GenInst__MemberInfo_t3922476713_0_0_0_Types[] = { &_MemberInfo_t3922476713_0_0_0 };
extern const Il2CppGenericInst GenInst__MemberInfo_t3922476713_0_0_0 = { 1, GenInst__MemberInfo_t3922476713_0_0_0_Types };
extern const Il2CppType Double_t594665363_0_0_0;
static const Il2CppType* GenInst_Double_t594665363_0_0_0_Types[] = { &Double_t594665363_0_0_0 };
extern const Il2CppGenericInst GenInst_Double_t594665363_0_0_0 = { 1, GenInst_Double_t594665363_0_0_0_Types };
extern const Il2CppType Single_t1397266774_0_0_0;
static const Il2CppType* GenInst_Single_t1397266774_0_0_0_Types[] = { &Single_t1397266774_0_0_0 };
extern const Il2CppGenericInst GenInst_Single_t1397266774_0_0_0 = { 1, GenInst_Single_t1397266774_0_0_0_Types };
extern const Il2CppType Decimal_t2948259380_0_0_0;
static const Il2CppType* GenInst_Decimal_t2948259380_0_0_0_Types[] = { &Decimal_t2948259380_0_0_0 };
extern const Il2CppGenericInst GenInst_Decimal_t2948259380_0_0_0 = { 1, GenInst_Decimal_t2948259380_0_0_0_Types };
extern const Il2CppType Boolean_t97287965_0_0_0;
static const Il2CppType* GenInst_Boolean_t97287965_0_0_0_Types[] = { &Boolean_t97287965_0_0_0 };
extern const Il2CppGenericInst GenInst_Boolean_t97287965_0_0_0 = { 1, GenInst_Boolean_t97287965_0_0_0_Types };
extern const Il2CppType Delegate_t1188392813_0_0_0;
static const Il2CppType* GenInst_Delegate_t1188392813_0_0_0_Types[] = { &Delegate_t1188392813_0_0_0 };
extern const Il2CppGenericInst GenInst_Delegate_t1188392813_0_0_0 = { 1, GenInst_Delegate_t1188392813_0_0_0_Types };
extern const Il2CppType ISerializable_t3375760802_0_0_0;
static const Il2CppType* GenInst_ISerializable_t3375760802_0_0_0_Types[] = { &ISerializable_t3375760802_0_0_0 };
extern const Il2CppGenericInst GenInst_ISerializable_t3375760802_0_0_0 = { 1, GenInst_ISerializable_t3375760802_0_0_0_Types };
extern const Il2CppType ParameterInfo_t1861056598_0_0_0;
static const Il2CppType* GenInst_ParameterInfo_t1861056598_0_0_0_Types[] = { &ParameterInfo_t1861056598_0_0_0 };
extern const Il2CppGenericInst GenInst_ParameterInfo_t1861056598_0_0_0 = { 1, GenInst_ParameterInfo_t1861056598_0_0_0_Types };
extern const Il2CppType _ParameterInfo_t489405856_0_0_0;
static const Il2CppType* GenInst__ParameterInfo_t489405856_0_0_0_Types[] = { &_ParameterInfo_t489405856_0_0_0 };
extern const Il2CppGenericInst GenInst__ParameterInfo_t489405856_0_0_0 = { 1, GenInst__ParameterInfo_t489405856_0_0_0_Types };
extern const Il2CppType ParameterModifier_t1461694466_0_0_0;
static const Il2CppType* GenInst_ParameterModifier_t1461694466_0_0_0_Types[] = { &ParameterModifier_t1461694466_0_0_0 };
extern const Il2CppGenericInst GenInst_ParameterModifier_t1461694466_0_0_0 = { 1, GenInst_ParameterModifier_t1461694466_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Types[] = { &Il2CppObject_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Types };
extern const Il2CppType FieldInfo_t_0_0_0;
static const Il2CppType* GenInst_FieldInfo_t_0_0_0_Types[] = { &FieldInfo_t_0_0_0 };
extern const Il2CppGenericInst GenInst_FieldInfo_t_0_0_0 = { 1, GenInst_FieldInfo_t_0_0_0_Types };
extern const Il2CppType _FieldInfo_t2781946373_0_0_0;
static const Il2CppType* GenInst__FieldInfo_t2781946373_0_0_0_Types[] = { &_FieldInfo_t2781946373_0_0_0 };
extern const Il2CppGenericInst GenInst__FieldInfo_t2781946373_0_0_0 = { 1, GenInst__FieldInfo_t2781946373_0_0_0_Types };
extern const Il2CppType MethodInfo_t_0_0_0;
static const Il2CppType* GenInst_MethodInfo_t_0_0_0_Types[] = { &MethodInfo_t_0_0_0 };
extern const Il2CppGenericInst GenInst_MethodInfo_t_0_0_0 = { 1, GenInst_MethodInfo_t_0_0_0_Types };
extern const Il2CppType _MethodInfo_t3550065504_0_0_0;
static const Il2CppType* GenInst__MethodInfo_t3550065504_0_0_0_Types[] = { &_MethodInfo_t3550065504_0_0_0 };
extern const Il2CppGenericInst GenInst__MethodInfo_t3550065504_0_0_0 = { 1, GenInst__MethodInfo_t3550065504_0_0_0_Types };
extern const Il2CppType MethodBase_t609368412_0_0_0;
static const Il2CppType* GenInst_MethodBase_t609368412_0_0_0_Types[] = { &MethodBase_t609368412_0_0_0 };
extern const Il2CppGenericInst GenInst_MethodBase_t609368412_0_0_0 = { 1, GenInst_MethodBase_t609368412_0_0_0_Types };
extern const Il2CppType _MethodBase_t1657248248_0_0_0;
static const Il2CppType* GenInst__MethodBase_t1657248248_0_0_0_Types[] = { &_MethodBase_t1657248248_0_0_0 };
extern const Il2CppGenericInst GenInst__MethodBase_t1657248248_0_0_0 = { 1, GenInst__MethodBase_t1657248248_0_0_0_Types };
extern const Il2CppType PropertyInfo_t_0_0_0;
static const Il2CppType* GenInst_PropertyInfo_t_0_0_0_Types[] = { &PropertyInfo_t_0_0_0 };
extern const Il2CppGenericInst GenInst_PropertyInfo_t_0_0_0 = { 1, GenInst_PropertyInfo_t_0_0_0_Types };
extern const Il2CppType _PropertyInfo_t4070324388_0_0_0;
static const Il2CppType* GenInst__PropertyInfo_t4070324388_0_0_0_Types[] = { &_PropertyInfo_t4070324388_0_0_0 };
extern const Il2CppGenericInst GenInst__PropertyInfo_t4070324388_0_0_0 = { 1, GenInst__PropertyInfo_t4070324388_0_0_0_Types };
extern const Il2CppType ConstructorInfo_t5769829_0_0_0;
static const Il2CppType* GenInst_ConstructorInfo_t5769829_0_0_0_Types[] = { &ConstructorInfo_t5769829_0_0_0 };
extern const Il2CppGenericInst GenInst_ConstructorInfo_t5769829_0_0_0 = { 1, GenInst_ConstructorInfo_t5769829_0_0_0_Types };
extern const Il2CppType _ConstructorInfo_t3357543833_0_0_0;
static const Il2CppType* GenInst__ConstructorInfo_t3357543833_0_0_0_Types[] = { &_ConstructorInfo_t3357543833_0_0_0 };
extern const Il2CppGenericInst GenInst__ConstructorInfo_t3357543833_0_0_0 = { 1, GenInst__ConstructorInfo_t3357543833_0_0_0_Types };
extern const Il2CppType IntPtr_t_0_0_0;
static const Il2CppType* GenInst_IntPtr_t_0_0_0_Types[] = { &IntPtr_t_0_0_0 };
extern const Il2CppGenericInst GenInst_IntPtr_t_0_0_0 = { 1, GenInst_IntPtr_t_0_0_0_Types };
extern const Il2CppType TableRange_t3332867892_0_0_0;
static const Il2CppType* GenInst_TableRange_t3332867892_0_0_0_Types[] = { &TableRange_t3332867892_0_0_0 };
extern const Il2CppGenericInst GenInst_TableRange_t3332867892_0_0_0 = { 1, GenInst_TableRange_t3332867892_0_0_0_Types };
extern const Il2CppType TailoringInfo_t866433654_0_0_0;
static const Il2CppType* GenInst_TailoringInfo_t866433654_0_0_0_Types[] = { &TailoringInfo_t866433654_0_0_0 };
extern const Il2CppGenericInst GenInst_TailoringInfo_t866433654_0_0_0 = { 1, GenInst_TailoringInfo_t866433654_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_Int32_t2950945753_0_0_0_Types[] = { &String_t_0_0_0, &Int32_t2950945753_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Int32_t2950945753_0_0_0 = { 2, GenInst_String_t_0_0_0_Int32_t2950945753_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_Int32_t2950945753_0_0_0_Types[] = { &Il2CppObject_0_0_0, &Int32_t2950945753_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_Int32_t2950945753_0_0_0 = { 2, GenInst_Il2CppObject_0_0_0_Int32_t2950945753_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2401056908_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2401056908_0_0_0_Types[] = { &KeyValuePair_2_t2401056908_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2401056908_0_0_0 = { 1, GenInst_KeyValuePair_2_t2401056908_0_0_0_Types };
extern const Il2CppType Link_t544317964_0_0_0;
static const Il2CppType* GenInst_Link_t544317964_0_0_0_Types[] = { &Link_t544317964_0_0_0 };
extern const Il2CppGenericInst GenInst_Link_t544317964_0_0_0 = { 1, GenInst_Link_t544317964_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_Int32_t2950945753_0_0_0_Int32_t2950945753_0_0_0_Types[] = { &Il2CppObject_0_0_0, &Int32_t2950945753_0_0_0, &Int32_t2950945753_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_Int32_t2950945753_0_0_0_Int32_t2950945753_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_Int32_t2950945753_0_0_0_Int32_t2950945753_0_0_0_Types };
extern const Il2CppType DictionaryEntry_t3123975638_0_0_0;
static const Il2CppType* GenInst_Il2CppObject_0_0_0_Int32_t2950945753_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types[] = { &Il2CppObject_0_0_0, &Int32_t2950945753_0_0_0, &DictionaryEntry_t3123975638_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_Int32_t2950945753_0_0_0_DictionaryEntry_t3123975638_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_Int32_t2950945753_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types };
static const Il2CppType* GenInst_DictionaryEntry_t3123975638_0_0_0_Types[] = { &DictionaryEntry_t3123975638_0_0_0 };
extern const Il2CppGenericInst GenInst_DictionaryEntry_t3123975638_0_0_0 = { 1, GenInst_DictionaryEntry_t3123975638_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_Int32_t2950945753_0_0_0_KeyValuePair_2_t2401056908_0_0_0_Types[] = { &Il2CppObject_0_0_0, &Int32_t2950945753_0_0_0, &KeyValuePair_2_t2401056908_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_Int32_t2950945753_0_0_0_KeyValuePair_2_t2401056908_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_Int32_t2950945753_0_0_0_KeyValuePair_2_t2401056908_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_Int32_t2950945753_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types[] = { &String_t_0_0_0, &Int32_t2950945753_0_0_0, &DictionaryEntry_t3123975638_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Int32_t2950945753_0_0_0_DictionaryEntry_t3123975638_0_0_0 = { 3, GenInst_String_t_0_0_0_Int32_t2950945753_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t838906923_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t838906923_0_0_0_Types[] = { &KeyValuePair_2_t838906923_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t838906923_0_0_0 = { 1, GenInst_KeyValuePair_2_t838906923_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_Int32_t2950945753_0_0_0_KeyValuePair_2_t838906923_0_0_0_Types[] = { &String_t_0_0_0, &Int32_t2950945753_0_0_0, &KeyValuePair_2_t838906923_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Int32_t2950945753_0_0_0_KeyValuePair_2_t838906923_0_0_0 = { 3, GenInst_String_t_0_0_0_Int32_t2950945753_0_0_0_KeyValuePair_2_t838906923_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_Int32_t2950945753_0_0_0_Int32_t2950945753_0_0_0_Types[] = { &String_t_0_0_0, &Int32_t2950945753_0_0_0, &Int32_t2950945753_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Int32_t2950945753_0_0_0_Int32_t2950945753_0_0_0 = { 3, GenInst_String_t_0_0_0_Int32_t2950945753_0_0_0_Int32_t2950945753_0_0_0_Types };
extern const Il2CppType Contraction_t1589275354_0_0_0;
static const Il2CppType* GenInst_Contraction_t1589275354_0_0_0_Types[] = { &Contraction_t1589275354_0_0_0 };
extern const Il2CppGenericInst GenInst_Contraction_t1589275354_0_0_0 = { 1, GenInst_Contraction_t1589275354_0_0_0_Types };
extern const Il2CppType Level2Map_t3640798870_0_0_0;
static const Il2CppType* GenInst_Level2Map_t3640798870_0_0_0_Types[] = { &Level2Map_t3640798870_0_0_0 };
extern const Il2CppGenericInst GenInst_Level2Map_t3640798870_0_0_0 = { 1, GenInst_Level2Map_t3640798870_0_0_0_Types };
extern const Il2CppType BigInteger_t2902905089_0_0_0;
static const Il2CppType* GenInst_BigInteger_t2902905089_0_0_0_Types[] = { &BigInteger_t2902905089_0_0_0 };
extern const Il2CppGenericInst GenInst_BigInteger_t2902905089_0_0_0 = { 1, GenInst_BigInteger_t2902905089_0_0_0_Types };
extern const Il2CppType KeySizes_t85027896_0_0_0;
static const Il2CppType* GenInst_KeySizes_t85027896_0_0_0_Types[] = { &KeySizes_t85027896_0_0_0 };
extern const Il2CppGenericInst GenInst_KeySizes_t85027896_0_0_0 = { 1, GenInst_KeySizes_t85027896_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2530217319_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2530217319_0_0_0_Types[] = { &KeyValuePair_2_t2530217319_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2530217319_0_0_0 = { 1, GenInst_KeyValuePair_2_t2530217319_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Types[] = { &Il2CppObject_0_0_0, &Il2CppObject_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types[] = { &Il2CppObject_0_0_0, &Il2CppObject_0_0_0, &DictionaryEntry_t3123975638_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t3123975638_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0_KeyValuePair_2_t2530217319_0_0_0_Types[] = { &Il2CppObject_0_0_0, &Il2CppObject_0_0_0, &KeyValuePair_2_t2530217319_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0_KeyValuePair_2_t2530217319_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0_KeyValuePair_2_t2530217319_0_0_0_Types };
extern const Il2CppType Slot_t3975888750_0_0_0;
static const Il2CppType* GenInst_Slot_t3975888750_0_0_0_Types[] = { &Slot_t3975888750_0_0_0 };
extern const Il2CppGenericInst GenInst_Slot_t3975888750_0_0_0 = { 1, GenInst_Slot_t3975888750_0_0_0_Types };
extern const Il2CppType Slot_t384495010_0_0_0;
static const Il2CppType* GenInst_Slot_t384495010_0_0_0_Types[] = { &Slot_t384495010_0_0_0 };
extern const Il2CppGenericInst GenInst_Slot_t384495010_0_0_0 = { 1, GenInst_Slot_t384495010_0_0_0_Types };
extern const Il2CppType StackFrame_t3217253059_0_0_0;
static const Il2CppType* GenInst_StackFrame_t3217253059_0_0_0_Types[] = { &StackFrame_t3217253059_0_0_0 };
extern const Il2CppGenericInst GenInst_StackFrame_t3217253059_0_0_0 = { 1, GenInst_StackFrame_t3217253059_0_0_0_Types };
extern const Il2CppType Calendar_t1661121569_0_0_0;
static const Il2CppType* GenInst_Calendar_t1661121569_0_0_0_Types[] = { &Calendar_t1661121569_0_0_0 };
extern const Il2CppGenericInst GenInst_Calendar_t1661121569_0_0_0 = { 1, GenInst_Calendar_t1661121569_0_0_0_Types };
extern const Il2CppType ModuleBuilder_t731887691_0_0_0;
static const Il2CppType* GenInst_ModuleBuilder_t731887691_0_0_0_Types[] = { &ModuleBuilder_t731887691_0_0_0 };
extern const Il2CppGenericInst GenInst_ModuleBuilder_t731887691_0_0_0 = { 1, GenInst_ModuleBuilder_t731887691_0_0_0_Types };
extern const Il2CppType _ModuleBuilder_t3217089703_0_0_0;
static const Il2CppType* GenInst__ModuleBuilder_t3217089703_0_0_0_Types[] = { &_ModuleBuilder_t3217089703_0_0_0 };
extern const Il2CppGenericInst GenInst__ModuleBuilder_t3217089703_0_0_0 = { 1, GenInst__ModuleBuilder_t3217089703_0_0_0_Types };
extern const Il2CppType Module_t2987026101_0_0_0;
static const Il2CppType* GenInst_Module_t2987026101_0_0_0_Types[] = { &Module_t2987026101_0_0_0 };
extern const Il2CppGenericInst GenInst_Module_t2987026101_0_0_0 = { 1, GenInst_Module_t2987026101_0_0_0_Types };
extern const Il2CppType _Module_t135161706_0_0_0;
static const Il2CppType* GenInst__Module_t135161706_0_0_0_Types[] = { &_Module_t135161706_0_0_0 };
extern const Il2CppGenericInst GenInst__Module_t135161706_0_0_0 = { 1, GenInst__Module_t135161706_0_0_0_Types };
extern const Il2CppType ParameterBuilder_t1137139675_0_0_0;
static const Il2CppType* GenInst_ParameterBuilder_t1137139675_0_0_0_Types[] = { &ParameterBuilder_t1137139675_0_0_0 };
extern const Il2CppGenericInst GenInst_ParameterBuilder_t1137139675_0_0_0 = { 1, GenInst_ParameterBuilder_t1137139675_0_0_0_Types };
extern const Il2CppType _ParameterBuilder_t3901898075_0_0_0;
static const Il2CppType* GenInst__ParameterBuilder_t3901898075_0_0_0_Types[] = { &_ParameterBuilder_t3901898075_0_0_0 };
extern const Il2CppGenericInst GenInst__ParameterBuilder_t3901898075_0_0_0 = { 1, GenInst__ParameterBuilder_t3901898075_0_0_0_Types };
extern const Il2CppType TypeU5BU5D_t3940880105_0_0_0;
static const Il2CppType* GenInst_TypeU5BU5D_t3940880105_0_0_0_Types[] = { &TypeU5BU5D_t3940880105_0_0_0 };
extern const Il2CppGenericInst GenInst_TypeU5BU5D_t3940880105_0_0_0 = { 1, GenInst_TypeU5BU5D_t3940880105_0_0_0_Types };
extern const Il2CppType Il2CppArray_0_0_0;
static const Il2CppType* GenInst_Il2CppArray_0_0_0_Types[] = { &Il2CppArray_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppArray_0_0_0 = { 1, GenInst_Il2CppArray_0_0_0_Types };
extern const Il2CppType ICollection_t3904884886_0_0_0;
static const Il2CppType* GenInst_ICollection_t3904884886_0_0_0_Types[] = { &ICollection_t3904884886_0_0_0 };
extern const Il2CppGenericInst GenInst_ICollection_t3904884886_0_0_0 = { 1, GenInst_ICollection_t3904884886_0_0_0_Types };
extern const Il2CppType IList_t2094931216_0_0_0;
static const Il2CppType* GenInst_IList_t2094931216_0_0_0_Types[] = { &IList_t2094931216_0_0_0 };
extern const Il2CppGenericInst GenInst_IList_t2094931216_0_0_0 = { 1, GenInst_IList_t2094931216_0_0_0_Types };
extern const Il2CppType IList_1_t4297247_0_0_0;
static const Il2CppType* GenInst_IList_1_t4297247_0_0_0_Types[] = { &IList_1_t4297247_0_0_0 };
extern const Il2CppGenericInst GenInst_IList_1_t4297247_0_0_0 = { 1, GenInst_IList_1_t4297247_0_0_0_Types };
extern const Il2CppType ICollection_1_t1017129698_0_0_0;
static const Il2CppType* GenInst_ICollection_1_t1017129698_0_0_0_Types[] = { &ICollection_1_t1017129698_0_0_0 };
extern const Il2CppGenericInst GenInst_ICollection_1_t1017129698_0_0_0 = { 1, GenInst_ICollection_1_t1017129698_0_0_0_Types };
extern const Il2CppType IEnumerable_1_t1463797649_0_0_0;
static const Il2CppType* GenInst_IEnumerable_1_t1463797649_0_0_0_Types[] = { &IEnumerable_1_t1463797649_0_0_0 };
extern const Il2CppGenericInst GenInst_IEnumerable_1_t1463797649_0_0_0 = { 1, GenInst_IEnumerable_1_t1463797649_0_0_0_Types };
extern const Il2CppType IList_1_t74629426_0_0_0;
static const Il2CppType* GenInst_IList_1_t74629426_0_0_0_Types[] = { &IList_1_t74629426_0_0_0 };
extern const Il2CppGenericInst GenInst_IList_1_t74629426_0_0_0 = { 1, GenInst_IList_1_t74629426_0_0_0_Types };
extern const Il2CppType ICollection_1_t1087461877_0_0_0;
static const Il2CppType* GenInst_ICollection_1_t1087461877_0_0_0_Types[] = { &ICollection_1_t1087461877_0_0_0 };
extern const Il2CppGenericInst GenInst_ICollection_1_t1087461877_0_0_0 = { 1, GenInst_ICollection_1_t1087461877_0_0_0_Types };
extern const Il2CppType IEnumerable_1_t1534129828_0_0_0;
static const Il2CppType* GenInst_IEnumerable_1_t1534129828_0_0_0_Types[] = { &IEnumerable_1_t1534129828_0_0_0 };
extern const Il2CppGenericInst GenInst_IEnumerable_1_t1534129828_0_0_0 = { 1, GenInst_IEnumerable_1_t1534129828_0_0_0_Types };
extern const Il2CppType IList_1_t1108916738_0_0_0;
static const Il2CppType* GenInst_IList_1_t1108916738_0_0_0_Types[] = { &IList_1_t1108916738_0_0_0 };
extern const Il2CppGenericInst GenInst_IList_1_t1108916738_0_0_0 = { 1, GenInst_IList_1_t1108916738_0_0_0_Types };
extern const Il2CppType ICollection_1_t2121749189_0_0_0;
static const Il2CppType* GenInst_ICollection_1_t2121749189_0_0_0_Types[] = { &ICollection_1_t2121749189_0_0_0 };
extern const Il2CppGenericInst GenInst_ICollection_1_t2121749189_0_0_0 = { 1, GenInst_ICollection_1_t2121749189_0_0_0_Types };
extern const Il2CppType IEnumerable_1_t2568417140_0_0_0;
static const Il2CppType* GenInst_IEnumerable_1_t2568417140_0_0_0_Types[] = { &IEnumerable_1_t2568417140_0_0_0 };
extern const Il2CppGenericInst GenInst_IEnumerable_1_t2568417140_0_0_0 = { 1, GenInst_IEnumerable_1_t2568417140_0_0_0_Types };
extern const Il2CppType IList_1_t900354228_0_0_0;
static const Il2CppType* GenInst_IList_1_t900354228_0_0_0_Types[] = { &IList_1_t900354228_0_0_0 };
extern const Il2CppGenericInst GenInst_IList_1_t900354228_0_0_0 = { 1, GenInst_IList_1_t900354228_0_0_0_Types };
extern const Il2CppType ICollection_1_t1913186679_0_0_0;
static const Il2CppType* GenInst_ICollection_1_t1913186679_0_0_0_Types[] = { &ICollection_1_t1913186679_0_0_0 };
extern const Il2CppGenericInst GenInst_ICollection_1_t1913186679_0_0_0 = { 1, GenInst_ICollection_1_t1913186679_0_0_0_Types };
extern const Il2CppType IEnumerable_1_t2359854630_0_0_0;
static const Il2CppType* GenInst_IEnumerable_1_t2359854630_0_0_0_Types[] = { &IEnumerable_1_t2359854630_0_0_0 };
extern const Il2CppGenericInst GenInst_IEnumerable_1_t2359854630_0_0_0 = { 1, GenInst_IEnumerable_1_t2359854630_0_0_0_Types };
extern const Il2CppType IList_1_t3346143920_0_0_0;
static const Il2CppType* GenInst_IList_1_t3346143920_0_0_0_Types[] = { &IList_1_t3346143920_0_0_0 };
extern const Il2CppGenericInst GenInst_IList_1_t3346143920_0_0_0 = { 1, GenInst_IList_1_t3346143920_0_0_0_Types };
extern const Il2CppType ICollection_1_t64009075_0_0_0;
static const Il2CppType* GenInst_ICollection_1_t64009075_0_0_0_Types[] = { &ICollection_1_t64009075_0_0_0 };
extern const Il2CppGenericInst GenInst_ICollection_1_t64009075_0_0_0 = { 1, GenInst_ICollection_1_t64009075_0_0_0_Types };
extern const Il2CppType IEnumerable_1_t510677026_0_0_0;
static const Il2CppType* GenInst_IEnumerable_1_t510677026_0_0_0_Types[] = { &IEnumerable_1_t510677026_0_0_0 };
extern const Il2CppGenericInst GenInst_IEnumerable_1_t510677026_0_0_0 = { 1, GenInst_IEnumerable_1_t510677026_0_0_0_Types };
extern const Il2CppType IList_1_t1442829200_0_0_0;
static const Il2CppType* GenInst_IList_1_t1442829200_0_0_0_Types[] = { &IList_1_t1442829200_0_0_0 };
extern const Il2CppGenericInst GenInst_IList_1_t1442829200_0_0_0 = { 1, GenInst_IList_1_t1442829200_0_0_0_Types };
extern const Il2CppType ICollection_1_t2455661651_0_0_0;
static const Il2CppType* GenInst_ICollection_1_t2455661651_0_0_0_Types[] = { &ICollection_1_t2455661651_0_0_0 };
extern const Il2CppGenericInst GenInst_ICollection_1_t2455661651_0_0_0 = { 1, GenInst_ICollection_1_t2455661651_0_0_0_Types };
extern const Il2CppType IEnumerable_1_t2902329602_0_0_0;
static const Il2CppType* GenInst_IEnumerable_1_t2902329602_0_0_0_Types[] = { &IEnumerable_1_t2902329602_0_0_0 };
extern const Il2CppGenericInst GenInst_IEnumerable_1_t2902329602_0_0_0 = { 1, GenInst_IEnumerable_1_t2902329602_0_0_0_Types };
extern const Il2CppType IList_1_t600458651_0_0_0;
static const Il2CppType* GenInst_IList_1_t600458651_0_0_0_Types[] = { &IList_1_t600458651_0_0_0 };
extern const Il2CppGenericInst GenInst_IList_1_t600458651_0_0_0 = { 1, GenInst_IList_1_t600458651_0_0_0_Types };
extern const Il2CppType ICollection_1_t1613291102_0_0_0;
static const Il2CppType* GenInst_ICollection_1_t1613291102_0_0_0_Types[] = { &ICollection_1_t1613291102_0_0_0 };
extern const Il2CppGenericInst GenInst_ICollection_1_t1613291102_0_0_0 = { 1, GenInst_ICollection_1_t1613291102_0_0_0_Types };
extern const Il2CppType IEnumerable_1_t2059959053_0_0_0;
static const Il2CppType* GenInst_IEnumerable_1_t2059959053_0_0_0_Types[] = { &IEnumerable_1_t2059959053_0_0_0 };
extern const Il2CppGenericInst GenInst_IEnumerable_1_t2059959053_0_0_0 = { 1, GenInst_IEnumerable_1_t2059959053_0_0_0_Types };
extern const Il2CppType ILTokenInfo_t2325775114_0_0_0;
static const Il2CppType* GenInst_ILTokenInfo_t2325775114_0_0_0_Types[] = { &ILTokenInfo_t2325775114_0_0_0 };
extern const Il2CppGenericInst GenInst_ILTokenInfo_t2325775114_0_0_0 = { 1, GenInst_ILTokenInfo_t2325775114_0_0_0_Types };
extern const Il2CppType LabelData_t360167391_0_0_0;
static const Il2CppType* GenInst_LabelData_t360167391_0_0_0_Types[] = { &LabelData_t360167391_0_0_0 };
extern const Il2CppGenericInst GenInst_LabelData_t360167391_0_0_0 = { 1, GenInst_LabelData_t360167391_0_0_0_Types };
extern const Il2CppType LabelFixup_t858502054_0_0_0;
static const Il2CppType* GenInst_LabelFixup_t858502054_0_0_0_Types[] = { &LabelFixup_t858502054_0_0_0 };
extern const Il2CppGenericInst GenInst_LabelFixup_t858502054_0_0_0 = { 1, GenInst_LabelFixup_t858502054_0_0_0_Types };
extern const Il2CppType GenericTypeParameterBuilder_t1988827940_0_0_0;
static const Il2CppType* GenInst_GenericTypeParameterBuilder_t1988827940_0_0_0_Types[] = { &GenericTypeParameterBuilder_t1988827940_0_0_0 };
extern const Il2CppGenericInst GenInst_GenericTypeParameterBuilder_t1988827940_0_0_0 = { 1, GenInst_GenericTypeParameterBuilder_t1988827940_0_0_0_Types };
extern const Il2CppType TypeBuilder_t1073948154_0_0_0;
static const Il2CppType* GenInst_TypeBuilder_t1073948154_0_0_0_Types[] = { &TypeBuilder_t1073948154_0_0_0 };
extern const Il2CppGenericInst GenInst_TypeBuilder_t1073948154_0_0_0 = { 1, GenInst_TypeBuilder_t1073948154_0_0_0_Types };
extern const Il2CppType _TypeBuilder_t2501637272_0_0_0;
static const Il2CppType* GenInst__TypeBuilder_t2501637272_0_0_0_Types[] = { &_TypeBuilder_t2501637272_0_0_0 };
extern const Il2CppGenericInst GenInst__TypeBuilder_t2501637272_0_0_0 = { 1, GenInst__TypeBuilder_t2501637272_0_0_0_Types };
extern const Il2CppType MethodBuilder_t2807316753_0_0_0;
static const Il2CppType* GenInst_MethodBuilder_t2807316753_0_0_0_Types[] = { &MethodBuilder_t2807316753_0_0_0 };
extern const Il2CppGenericInst GenInst_MethodBuilder_t2807316753_0_0_0 = { 1, GenInst_MethodBuilder_t2807316753_0_0_0_Types };
extern const Il2CppType _MethodBuilder_t600455149_0_0_0;
static const Il2CppType* GenInst__MethodBuilder_t600455149_0_0_0_Types[] = { &_MethodBuilder_t600455149_0_0_0 };
extern const Il2CppGenericInst GenInst__MethodBuilder_t600455149_0_0_0 = { 1, GenInst__MethodBuilder_t600455149_0_0_0_Types };
extern const Il2CppType ConstructorBuilder_t2813524108_0_0_0;
static const Il2CppType* GenInst_ConstructorBuilder_t2813524108_0_0_0_Types[] = { &ConstructorBuilder_t2813524108_0_0_0 };
extern const Il2CppGenericInst GenInst_ConstructorBuilder_t2813524108_0_0_0 = { 1, GenInst_ConstructorBuilder_t2813524108_0_0_0_Types };
extern const Il2CppType _ConstructorBuilder_t2416550571_0_0_0;
static const Il2CppType* GenInst__ConstructorBuilder_t2416550571_0_0_0_Types[] = { &_ConstructorBuilder_t2416550571_0_0_0 };
extern const Il2CppGenericInst GenInst__ConstructorBuilder_t2416550571_0_0_0 = { 1, GenInst__ConstructorBuilder_t2416550571_0_0_0_Types };
extern const Il2CppType PropertyBuilder_t314297007_0_0_0;
static const Il2CppType* GenInst_PropertyBuilder_t314297007_0_0_0_Types[] = { &PropertyBuilder_t314297007_0_0_0 };
extern const Il2CppGenericInst GenInst_PropertyBuilder_t314297007_0_0_0 = { 1, GenInst_PropertyBuilder_t314297007_0_0_0_Types };
extern const Il2CppType _PropertyBuilder_t1366136710_0_0_0;
static const Il2CppType* GenInst__PropertyBuilder_t1366136710_0_0_0_Types[] = { &_PropertyBuilder_t1366136710_0_0_0 };
extern const Il2CppGenericInst GenInst__PropertyBuilder_t1366136710_0_0_0 = { 1, GenInst__PropertyBuilder_t1366136710_0_0_0_Types };
extern const Il2CppType FieldBuilder_t2627049993_0_0_0;
static const Il2CppType* GenInst_FieldBuilder_t2627049993_0_0_0_Types[] = { &FieldBuilder_t2627049993_0_0_0 };
extern const Il2CppGenericInst GenInst_FieldBuilder_t2627049993_0_0_0 = { 1, GenInst_FieldBuilder_t2627049993_0_0_0_Types };
extern const Il2CppType _FieldBuilder_t2615792726_0_0_0;
static const Il2CppType* GenInst__FieldBuilder_t2615792726_0_0_0_Types[] = { &_FieldBuilder_t2615792726_0_0_0 };
extern const Il2CppGenericInst GenInst__FieldBuilder_t2615792726_0_0_0 = { 1, GenInst__FieldBuilder_t2615792726_0_0_0_Types };
extern const Il2CppType CustomAttributeTypedArgument_t2723150157_0_0_0;
static const Il2CppType* GenInst_CustomAttributeTypedArgument_t2723150157_0_0_0_Types[] = { &CustomAttributeTypedArgument_t2723150157_0_0_0 };
extern const Il2CppGenericInst GenInst_CustomAttributeTypedArgument_t2723150157_0_0_0 = { 1, GenInst_CustomAttributeTypedArgument_t2723150157_0_0_0_Types };
extern const Il2CppType CustomAttributeNamedArgument_t287865710_0_0_0;
static const Il2CppType* GenInst_CustomAttributeNamedArgument_t287865710_0_0_0_Types[] = { &CustomAttributeNamedArgument_t287865710_0_0_0 };
extern const Il2CppGenericInst GenInst_CustomAttributeNamedArgument_t287865710_0_0_0 = { 1, GenInst_CustomAttributeNamedArgument_t287865710_0_0_0_Types };
extern const Il2CppType CustomAttributeData_t1084486650_0_0_0;
static const Il2CppType* GenInst_CustomAttributeData_t1084486650_0_0_0_Types[] = { &CustomAttributeData_t1084486650_0_0_0 };
extern const Il2CppGenericInst GenInst_CustomAttributeData_t1084486650_0_0_0 = { 1, GenInst_CustomAttributeData_t1084486650_0_0_0_Types };
extern const Il2CppType ResourceInfo_t2872965302_0_0_0;
static const Il2CppType* GenInst_ResourceInfo_t2872965302_0_0_0_Types[] = { &ResourceInfo_t2872965302_0_0_0 };
extern const Il2CppGenericInst GenInst_ResourceInfo_t2872965302_0_0_0 = { 1, GenInst_ResourceInfo_t2872965302_0_0_0_Types };
extern const Il2CppType ResourceCacheItem_t51292791_0_0_0;
static const Il2CppType* GenInst_ResourceCacheItem_t51292791_0_0_0_Types[] = { &ResourceCacheItem_t51292791_0_0_0 };
extern const Il2CppGenericInst GenInst_ResourceCacheItem_t51292791_0_0_0 = { 1, GenInst_ResourceCacheItem_t51292791_0_0_0_Types };
extern const Il2CppType IContextProperty_t840037424_0_0_0;
static const Il2CppType* GenInst_IContextProperty_t840037424_0_0_0_Types[] = { &IContextProperty_t840037424_0_0_0 };
extern const Il2CppGenericInst GenInst_IContextProperty_t840037424_0_0_0 = { 1, GenInst_IContextProperty_t840037424_0_0_0_Types };
extern const Il2CppType Header_t549724581_0_0_0;
static const Il2CppType* GenInst_Header_t549724581_0_0_0_Types[] = { &Header_t549724581_0_0_0 };
extern const Il2CppGenericInst GenInst_Header_t549724581_0_0_0 = { 1, GenInst_Header_t549724581_0_0_0_Types };
extern const Il2CppType ITrackingHandler_t1244553475_0_0_0;
static const Il2CppType* GenInst_ITrackingHandler_t1244553475_0_0_0_Types[] = { &ITrackingHandler_t1244553475_0_0_0 };
extern const Il2CppGenericInst GenInst_ITrackingHandler_t1244553475_0_0_0 = { 1, GenInst_ITrackingHandler_t1244553475_0_0_0_Types };
extern const Il2CppType IContextAttribute_t176678928_0_0_0;
static const Il2CppType* GenInst_IContextAttribute_t176678928_0_0_0_Types[] = { &IContextAttribute_t176678928_0_0_0 };
extern const Il2CppGenericInst GenInst_IContextAttribute_t176678928_0_0_0 = { 1, GenInst_IContextAttribute_t176678928_0_0_0_Types };
extern const Il2CppType DateTime_t3738529785_0_0_0;
static const Il2CppType* GenInst_DateTime_t3738529785_0_0_0_Types[] = { &DateTime_t3738529785_0_0_0 };
extern const Il2CppGenericInst GenInst_DateTime_t3738529785_0_0_0 = { 1, GenInst_DateTime_t3738529785_0_0_0_Types };
extern const Il2CppType TimeSpan_t881159249_0_0_0;
static const Il2CppType* GenInst_TimeSpan_t881159249_0_0_0_Types[] = { &TimeSpan_t881159249_0_0_0 };
extern const Il2CppGenericInst GenInst_TimeSpan_t881159249_0_0_0 = { 1, GenInst_TimeSpan_t881159249_0_0_0_Types };
extern const Il2CppType TypeTag_t3541821701_0_0_0;
static const Il2CppType* GenInst_TypeTag_t3541821701_0_0_0_Types[] = { &TypeTag_t3541821701_0_0_0 };
extern const Il2CppGenericInst GenInst_TypeTag_t3541821701_0_0_0 = { 1, GenInst_TypeTag_t3541821701_0_0_0_Types };
extern const Il2CppType MonoType_t_0_0_0;
static const Il2CppType* GenInst_MonoType_t_0_0_0_Types[] = { &MonoType_t_0_0_0 };
extern const Il2CppGenericInst GenInst_MonoType_t_0_0_0 = { 1, GenInst_MonoType_t_0_0_0_Types };
extern const Il2CppType StrongName_t3675724614_0_0_0;
static const Il2CppType* GenInst_StrongName_t3675724614_0_0_0_Types[] = { &StrongName_t3675724614_0_0_0 };
extern const Il2CppGenericInst GenInst_StrongName_t3675724614_0_0_0 = { 1, GenInst_StrongName_t3675724614_0_0_0_Types };
extern const Il2CppType IBuiltInEvidence_t554693121_0_0_0;
static const Il2CppType* GenInst_IBuiltInEvidence_t554693121_0_0_0_Types[] = { &IBuiltInEvidence_t554693121_0_0_0 };
extern const Il2CppGenericInst GenInst_IBuiltInEvidence_t554693121_0_0_0 = { 1, GenInst_IBuiltInEvidence_t554693121_0_0_0_Types };
extern const Il2CppType IIdentityPermissionFactory_t3268650966_0_0_0;
static const Il2CppType* GenInst_IIdentityPermissionFactory_t3268650966_0_0_0_Types[] = { &IIdentityPermissionFactory_t3268650966_0_0_0 };
extern const Il2CppGenericInst GenInst_IIdentityPermissionFactory_t3268650966_0_0_0 = { 1, GenInst_IIdentityPermissionFactory_t3268650966_0_0_0_Types };
extern const Il2CppType DateTimeOffset_t3229287507_0_0_0;
static const Il2CppType* GenInst_DateTimeOffset_t3229287507_0_0_0_Types[] = { &DateTimeOffset_t3229287507_0_0_0 };
extern const Il2CppGenericInst GenInst_DateTimeOffset_t3229287507_0_0_0 = { 1, GenInst_DateTimeOffset_t3229287507_0_0_0_Types };
extern const Il2CppType Guid_t_0_0_0;
static const Il2CppType* GenInst_Guid_t_0_0_0_Types[] = { &Guid_t_0_0_0 };
extern const Il2CppGenericInst GenInst_Guid_t_0_0_0 = { 1, GenInst_Guid_t_0_0_0_Types };
extern const Il2CppType Version_t3456873960_0_0_0;
static const Il2CppType* GenInst_Version_t3456873960_0_0_0_Types[] = { &Version_t3456873960_0_0_0 };
extern const Il2CppGenericInst GenInst_Version_t3456873960_0_0_0 = { 1, GenInst_Version_t3456873960_0_0_0_Types };
extern const Il2CppType BigInteger_t2902905090_0_0_0;
static const Il2CppType* GenInst_BigInteger_t2902905090_0_0_0_Types[] = { &BigInteger_t2902905090_0_0_0 };
extern const Il2CppGenericInst GenInst_BigInteger_t2902905090_0_0_0 = { 1, GenInst_BigInteger_t2902905090_0_0_0_Types };
extern const Il2CppType ByteU5BU5D_t4116647657_0_0_0;
static const Il2CppType* GenInst_ByteU5BU5D_t4116647657_0_0_0_Types[] = { &ByteU5BU5D_t4116647657_0_0_0 };
extern const Il2CppGenericInst GenInst_ByteU5BU5D_t4116647657_0_0_0 = { 1, GenInst_ByteU5BU5D_t4116647657_0_0_0_Types };
extern const Il2CppType IList_1_t2949616159_0_0_0;
static const Il2CppType* GenInst_IList_1_t2949616159_0_0_0_Types[] = { &IList_1_t2949616159_0_0_0 };
extern const Il2CppGenericInst GenInst_IList_1_t2949616159_0_0_0 = { 1, GenInst_IList_1_t2949616159_0_0_0_Types };
extern const Il2CppType ICollection_1_t3962448610_0_0_0;
static const Il2CppType* GenInst_ICollection_1_t3962448610_0_0_0_Types[] = { &ICollection_1_t3962448610_0_0_0 };
extern const Il2CppGenericInst GenInst_ICollection_1_t3962448610_0_0_0 = { 1, GenInst_ICollection_1_t3962448610_0_0_0_Types };
extern const Il2CppType IEnumerable_1_t114149265_0_0_0;
static const Il2CppType* GenInst_IEnumerable_1_t114149265_0_0_0_Types[] = { &IEnumerable_1_t114149265_0_0_0 };
extern const Il2CppGenericInst GenInst_IEnumerable_1_t114149265_0_0_0 = { 1, GenInst_IEnumerable_1_t114149265_0_0_0_Types };
extern const Il2CppType X509Certificate_t713131622_0_0_0;
static const Il2CppType* GenInst_X509Certificate_t713131622_0_0_0_Types[] = { &X509Certificate_t713131622_0_0_0 };
extern const Il2CppGenericInst GenInst_X509Certificate_t713131622_0_0_0 = { 1, GenInst_X509Certificate_t713131622_0_0_0_Types };
extern const Il2CppType IDeserializationCallback_t4220500054_0_0_0;
static const Il2CppType* GenInst_IDeserializationCallback_t4220500054_0_0_0_Types[] = { &IDeserializationCallback_t4220500054_0_0_0 };
extern const Il2CppGenericInst GenInst_IDeserializationCallback_t4220500054_0_0_0 = { 1, GenInst_IDeserializationCallback_t4220500054_0_0_0_Types };
extern const Il2CppType ClientCertificateType_t1004704908_0_0_0;
static const Il2CppType* GenInst_ClientCertificateType_t1004704908_0_0_0_Types[] = { &ClientCertificateType_t1004704908_0_0_0 };
extern const Il2CppGenericInst GenInst_ClientCertificateType_t1004704908_0_0_0 = { 1, GenInst_ClientCertificateType_t1004704908_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_Boolean_t97287965_0_0_0_Types[] = { &String_t_0_0_0, &Boolean_t97287965_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Boolean_t97287965_0_0_0 = { 2, GenInst_String_t_0_0_0_Boolean_t97287965_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_Boolean_t97287965_0_0_0_Types[] = { &Il2CppObject_0_0_0, &Boolean_t97287965_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_Boolean_t97287965_0_0_0 = { 2, GenInst_Il2CppObject_0_0_0_Boolean_t97287965_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t3842366416_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t3842366416_0_0_0_Types[] = { &KeyValuePair_2_t3842366416_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3842366416_0_0_0 = { 1, GenInst_KeyValuePair_2_t3842366416_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_Boolean_t97287965_0_0_0_Boolean_t97287965_0_0_0_Types[] = { &Il2CppObject_0_0_0, &Boolean_t97287965_0_0_0, &Boolean_t97287965_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_Boolean_t97287965_0_0_0_Boolean_t97287965_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_Boolean_t97287965_0_0_0_Boolean_t97287965_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_Boolean_t97287965_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types[] = { &Il2CppObject_0_0_0, &Boolean_t97287965_0_0_0, &DictionaryEntry_t3123975638_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_Boolean_t97287965_0_0_0_DictionaryEntry_t3123975638_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_Boolean_t97287965_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_Boolean_t97287965_0_0_0_KeyValuePair_2_t3842366416_0_0_0_Types[] = { &Il2CppObject_0_0_0, &Boolean_t97287965_0_0_0, &KeyValuePair_2_t3842366416_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_Boolean_t97287965_0_0_0_KeyValuePair_2_t3842366416_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_Boolean_t97287965_0_0_0_KeyValuePair_2_t3842366416_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_Boolean_t97287965_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types[] = { &String_t_0_0_0, &Boolean_t97287965_0_0_0, &DictionaryEntry_t3123975638_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Boolean_t97287965_0_0_0_DictionaryEntry_t3123975638_0_0_0 = { 3, GenInst_String_t_0_0_0_Boolean_t97287965_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2280216431_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2280216431_0_0_0_Types[] = { &KeyValuePair_2_t2280216431_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2280216431_0_0_0 = { 1, GenInst_KeyValuePair_2_t2280216431_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_Boolean_t97287965_0_0_0_KeyValuePair_2_t2280216431_0_0_0_Types[] = { &String_t_0_0_0, &Boolean_t97287965_0_0_0, &KeyValuePair_2_t2280216431_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Boolean_t97287965_0_0_0_KeyValuePair_2_t2280216431_0_0_0 = { 3, GenInst_String_t_0_0_0_Boolean_t97287965_0_0_0_KeyValuePair_2_t2280216431_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_Boolean_t97287965_0_0_0_Boolean_t97287965_0_0_0_Types[] = { &String_t_0_0_0, &Boolean_t97287965_0_0_0, &Boolean_t97287965_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Boolean_t97287965_0_0_0_Boolean_t97287965_0_0_0 = { 3, GenInst_String_t_0_0_0_Boolean_t97287965_0_0_0_Boolean_t97287965_0_0_0_Types };
extern const Il2CppType X509ChainStatus_t133602714_0_0_0;
static const Il2CppType* GenInst_X509ChainStatus_t133602714_0_0_0_Types[] = { &X509ChainStatus_t133602714_0_0_0 };
extern const Il2CppGenericInst GenInst_X509ChainStatus_t133602714_0_0_0 = { 1, GenInst_X509ChainStatus_t133602714_0_0_0_Types };
extern const Il2CppType Capture_t2232016050_0_0_0;
static const Il2CppType* GenInst_Capture_t2232016050_0_0_0_Types[] = { &Capture_t2232016050_0_0_0 };
extern const Il2CppGenericInst GenInst_Capture_t2232016050_0_0_0 = { 1, GenInst_Capture_t2232016050_0_0_0_Types };
extern const Il2CppType Group_t2468205786_0_0_0;
static const Il2CppType* GenInst_Group_t2468205786_0_0_0_Types[] = { &Group_t2468205786_0_0_0 };
extern const Il2CppGenericInst GenInst_Group_t2468205786_0_0_0 = { 1, GenInst_Group_t2468205786_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2950945753_0_0_0_Int32_t2950945753_0_0_0_Types[] = { &Int32_t2950945753_0_0_0, &Int32_t2950945753_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2950945753_0_0_0_Int32_t2950945753_0_0_0 = { 2, GenInst_Int32_t2950945753_0_0_0_Int32_t2950945753_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t4237331251_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t4237331251_0_0_0_Types[] = { &KeyValuePair_2_t4237331251_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t4237331251_0_0_0 = { 1, GenInst_KeyValuePair_2_t4237331251_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2950945753_0_0_0_Int32_t2950945753_0_0_0_Int32_t2950945753_0_0_0_Types[] = { &Int32_t2950945753_0_0_0, &Int32_t2950945753_0_0_0, &Int32_t2950945753_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2950945753_0_0_0_Int32_t2950945753_0_0_0_Int32_t2950945753_0_0_0 = { 3, GenInst_Int32_t2950945753_0_0_0_Int32_t2950945753_0_0_0_Int32_t2950945753_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2950945753_0_0_0_Int32_t2950945753_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types[] = { &Int32_t2950945753_0_0_0, &Int32_t2950945753_0_0_0, &DictionaryEntry_t3123975638_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2950945753_0_0_0_Int32_t2950945753_0_0_0_DictionaryEntry_t3123975638_0_0_0 = { 3, GenInst_Int32_t2950945753_0_0_0_Int32_t2950945753_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2950945753_0_0_0_Int32_t2950945753_0_0_0_KeyValuePair_2_t4237331251_0_0_0_Types[] = { &Int32_t2950945753_0_0_0, &Int32_t2950945753_0_0_0, &KeyValuePair_2_t4237331251_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2950945753_0_0_0_Int32_t2950945753_0_0_0_KeyValuePair_2_t4237331251_0_0_0 = { 3, GenInst_Int32_t2950945753_0_0_0_Int32_t2950945753_0_0_0_KeyValuePair_2_t4237331251_0_0_0_Types };
extern const Il2CppType Mark_t3471605523_0_0_0;
static const Il2CppType* GenInst_Mark_t3471605523_0_0_0_Types[] = { &Mark_t3471605523_0_0_0 };
extern const Il2CppGenericInst GenInst_Mark_t3471605523_0_0_0 = { 1, GenInst_Mark_t3471605523_0_0_0_Types };
extern const Il2CppType UriScheme_t722425697_0_0_0;
static const Il2CppType* GenInst_UriScheme_t722425697_0_0_0_Types[] = { &UriScheme_t722425697_0_0_0 };
extern const Il2CppGenericInst GenInst_UriScheme_t722425697_0_0_0 = { 1, GenInst_UriScheme_t722425697_0_0_0_Types };
extern const Il2CppType Link_t3209266973_0_0_0;
static const Il2CppType* GenInst_Link_t3209266973_0_0_0_Types[] = { &Link_t3209266973_0_0_0 };
extern const Il2CppGenericInst GenInst_Link_t3209266973_0_0_0 = { 1, GenInst_Link_t3209266973_0_0_0_Types };
extern const Il2CppType Object_t631007953_0_0_0;
static const Il2CppType* GenInst_Object_t631007953_0_0_0_Types[] = { &Object_t631007953_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_t631007953_0_0_0 = { 1, GenInst_Object_t631007953_0_0_0_Types };
extern const Il2CppType Camera_t4157153871_0_0_0;
static const Il2CppType* GenInst_Camera_t4157153871_0_0_0_Types[] = { &Camera_t4157153871_0_0_0 };
extern const Il2CppGenericInst GenInst_Camera_t4157153871_0_0_0 = { 1, GenInst_Camera_t4157153871_0_0_0_Types };
extern const Il2CppType Behaviour_t1437897464_0_0_0;
static const Il2CppType* GenInst_Behaviour_t1437897464_0_0_0_Types[] = { &Behaviour_t1437897464_0_0_0 };
extern const Il2CppGenericInst GenInst_Behaviour_t1437897464_0_0_0 = { 1, GenInst_Behaviour_t1437897464_0_0_0_Types };
extern const Il2CppType Component_t1923634451_0_0_0;
static const Il2CppType* GenInst_Component_t1923634451_0_0_0_Types[] = { &Component_t1923634451_0_0_0 };
extern const Il2CppGenericInst GenInst_Component_t1923634451_0_0_0 = { 1, GenInst_Component_t1923634451_0_0_0_Types };
extern const Il2CppType Display_t1387065949_0_0_0;
static const Il2CppType* GenInst_Display_t1387065949_0_0_0_Types[] = { &Display_t1387065949_0_0_0 };
extern const Il2CppGenericInst GenInst_Display_t1387065949_0_0_0 = { 1, GenInst_Display_t1387065949_0_0_0_Types };
static const Il2CppType* GenInst_Boolean_t97287965_0_0_0_String_t_0_0_0_Types[] = { &Boolean_t97287965_0_0_0, &String_t_0_0_0 };
extern const Il2CppGenericInst GenInst_Boolean_t97287965_0_0_0_String_t_0_0_0 = { 2, GenInst_Boolean_t97287965_0_0_0_String_t_0_0_0_Types };
static const Il2CppType* GenInst_Boolean_t97287965_0_0_0_Il2CppObject_0_0_0_Types[] = { &Boolean_t97287965_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_Boolean_t97287965_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_Boolean_t97287965_0_0_0_Il2CppObject_0_0_0_Types };
extern const Il2CppType AchievementDescription_t3217594527_0_0_0;
static const Il2CppType* GenInst_AchievementDescription_t3217594527_0_0_0_Types[] = { &AchievementDescription_t3217594527_0_0_0 };
extern const Il2CppGenericInst GenInst_AchievementDescription_t3217594527_0_0_0 = { 1, GenInst_AchievementDescription_t3217594527_0_0_0_Types };
extern const Il2CppType IAchievementDescription_t2514275728_0_0_0;
static const Il2CppType* GenInst_IAchievementDescription_t2514275728_0_0_0_Types[] = { &IAchievementDescription_t2514275728_0_0_0 };
extern const Il2CppGenericInst GenInst_IAchievementDescription_t2514275728_0_0_0 = { 1, GenInst_IAchievementDescription_t2514275728_0_0_0_Types };
extern const Il2CppType UserProfile_t3137328177_0_0_0;
static const Il2CppType* GenInst_UserProfile_t3137328177_0_0_0_Types[] = { &UserProfile_t3137328177_0_0_0 };
extern const Il2CppGenericInst GenInst_UserProfile_t3137328177_0_0_0 = { 1, GenInst_UserProfile_t3137328177_0_0_0_Types };
extern const Il2CppType IUserProfile_t360500636_0_0_0;
static const Il2CppType* GenInst_IUserProfile_t360500636_0_0_0_Types[] = { &IUserProfile_t360500636_0_0_0 };
extern const Il2CppGenericInst GenInst_IUserProfile_t360500636_0_0_0 = { 1, GenInst_IUserProfile_t360500636_0_0_0_Types };
extern const Il2CppType GcLeaderboard_t4132273028_0_0_0;
static const Il2CppType* GenInst_GcLeaderboard_t4132273028_0_0_0_Types[] = { &GcLeaderboard_t4132273028_0_0_0 };
extern const Il2CppGenericInst GenInst_GcLeaderboard_t4132273028_0_0_0 = { 1, GenInst_GcLeaderboard_t4132273028_0_0_0_Types };
extern const Il2CppType IAchievementDescriptionU5BU5D_t1821964849_0_0_0;
static const Il2CppType* GenInst_IAchievementDescriptionU5BU5D_t1821964849_0_0_0_Types[] = { &IAchievementDescriptionU5BU5D_t1821964849_0_0_0 };
extern const Il2CppGenericInst GenInst_IAchievementDescriptionU5BU5D_t1821964849_0_0_0 = { 1, GenInst_IAchievementDescriptionU5BU5D_t1821964849_0_0_0_Types };
extern const Il2CppType IAchievementU5BU5D_t1892338339_0_0_0;
static const Il2CppType* GenInst_IAchievementU5BU5D_t1892338339_0_0_0_Types[] = { &IAchievementU5BU5D_t1892338339_0_0_0 };
extern const Il2CppGenericInst GenInst_IAchievementU5BU5D_t1892338339_0_0_0 = { 1, GenInst_IAchievementU5BU5D_t1892338339_0_0_0_Types };
extern const Il2CppType IAchievement_t1421108358_0_0_0;
static const Il2CppType* GenInst_IAchievement_t1421108358_0_0_0_Types[] = { &IAchievement_t1421108358_0_0_0 };
extern const Il2CppGenericInst GenInst_IAchievement_t1421108358_0_0_0 = { 1, GenInst_IAchievement_t1421108358_0_0_0_Types };
extern const Il2CppType GcAchievementData_t675222246_0_0_0;
static const Il2CppType* GenInst_GcAchievementData_t675222246_0_0_0_Types[] = { &GcAchievementData_t675222246_0_0_0 };
extern const Il2CppGenericInst GenInst_GcAchievementData_t675222246_0_0_0 = { 1, GenInst_GcAchievementData_t675222246_0_0_0_Types };
extern const Il2CppType Achievement_t565359984_0_0_0;
static const Il2CppType* GenInst_Achievement_t565359984_0_0_0_Types[] = { &Achievement_t565359984_0_0_0 };
extern const Il2CppGenericInst GenInst_Achievement_t565359984_0_0_0 = { 1, GenInst_Achievement_t565359984_0_0_0_Types };
extern const Il2CppType IScoreU5BU5D_t527871248_0_0_0;
static const Il2CppType* GenInst_IScoreU5BU5D_t527871248_0_0_0_Types[] = { &IScoreU5BU5D_t527871248_0_0_0 };
extern const Il2CppGenericInst GenInst_IScoreU5BU5D_t527871248_0_0_0 = { 1, GenInst_IScoreU5BU5D_t527871248_0_0_0_Types };
extern const Il2CppType IScore_t2559910621_0_0_0;
static const Il2CppType* GenInst_IScore_t2559910621_0_0_0_Types[] = { &IScore_t2559910621_0_0_0 };
extern const Il2CppGenericInst GenInst_IScore_t2559910621_0_0_0 = { 1, GenInst_IScore_t2559910621_0_0_0_Types };
extern const Il2CppType GcScoreData_t2125309831_0_0_0;
static const Il2CppType* GenInst_GcScoreData_t2125309831_0_0_0_Types[] = { &GcScoreData_t2125309831_0_0_0 };
extern const Il2CppGenericInst GenInst_GcScoreData_t2125309831_0_0_0 = { 1, GenInst_GcScoreData_t2125309831_0_0_0_Types };
extern const Il2CppType Score_t1968645328_0_0_0;
static const Il2CppType* GenInst_Score_t1968645328_0_0_0_Types[] = { &Score_t1968645328_0_0_0 };
extern const Il2CppGenericInst GenInst_Score_t1968645328_0_0_0 = { 1, GenInst_Score_t1968645328_0_0_0_Types };
extern const Il2CppType IUserProfileU5BU5D_t909679733_0_0_0;
static const Il2CppType* GenInst_IUserProfileU5BU5D_t909679733_0_0_0_Types[] = { &IUserProfileU5BU5D_t909679733_0_0_0 };
extern const Il2CppGenericInst GenInst_IUserProfileU5BU5D_t909679733_0_0_0 = { 1, GenInst_IUserProfileU5BU5D_t909679733_0_0_0_Types };
extern const Il2CppType GameObject_t1113636619_0_0_0;
static const Il2CppType* GenInst_GameObject_t1113636619_0_0_0_Types[] = { &GameObject_t1113636619_0_0_0 };
extern const Il2CppGenericInst GenInst_GameObject_t1113636619_0_0_0 = { 1, GenInst_GameObject_t1113636619_0_0_0_Types };
extern const Il2CppType Material_t340375123_0_0_0;
static const Il2CppType* GenInst_Material_t340375123_0_0_0_Types[] = { &Material_t340375123_0_0_0 };
extern const Il2CppGenericInst GenInst_Material_t340375123_0_0_0 = { 1, GenInst_Material_t340375123_0_0_0_Types };
extern const Il2CppType LightmapData_t2568046604_0_0_0;
static const Il2CppType* GenInst_LightmapData_t2568046604_0_0_0_Types[] = { &LightmapData_t2568046604_0_0_0 };
extern const Il2CppGenericInst GenInst_LightmapData_t2568046604_0_0_0 = { 1, GenInst_LightmapData_t2568046604_0_0_0_Types };
extern const Il2CppType Keyframe_t4206410242_0_0_0;
static const Il2CppType* GenInst_Keyframe_t4206410242_0_0_0_Types[] = { &Keyframe_t4206410242_0_0_0 };
extern const Il2CppGenericInst GenInst_Keyframe_t4206410242_0_0_0 = { 1, GenInst_Keyframe_t4206410242_0_0_0_Types };
extern const Il2CppType Vector3_t3722313464_0_0_0;
static const Il2CppType* GenInst_Vector3_t3722313464_0_0_0_Types[] = { &Vector3_t3722313464_0_0_0 };
extern const Il2CppGenericInst GenInst_Vector3_t3722313464_0_0_0 = { 1, GenInst_Vector3_t3722313464_0_0_0_Types };
extern const Il2CppType Vector4_t3319028937_0_0_0;
static const Il2CppType* GenInst_Vector4_t3319028937_0_0_0_Types[] = { &Vector4_t3319028937_0_0_0 };
extern const Il2CppGenericInst GenInst_Vector4_t3319028937_0_0_0 = { 1, GenInst_Vector4_t3319028937_0_0_0_Types };
extern const Il2CppType Vector2_t2156229523_0_0_0;
static const Il2CppType* GenInst_Vector2_t2156229523_0_0_0_Types[] = { &Vector2_t2156229523_0_0_0 };
extern const Il2CppGenericInst GenInst_Vector2_t2156229523_0_0_0 = { 1, GenInst_Vector2_t2156229523_0_0_0_Types };
extern const Il2CppType Color_t2555686324_0_0_0;
static const Il2CppType* GenInst_Color_t2555686324_0_0_0_Types[] = { &Color_t2555686324_0_0_0 };
extern const Il2CppGenericInst GenInst_Color_t2555686324_0_0_0 = { 1, GenInst_Color_t2555686324_0_0_0_Types };
extern const Il2CppType Color32_t2600501292_0_0_0;
static const Il2CppType* GenInst_Color32_t2600501292_0_0_0_Types[] = { &Color32_t2600501292_0_0_0 };
extern const Il2CppGenericInst GenInst_Color32_t2600501292_0_0_0 = { 1, GenInst_Color32_t2600501292_0_0_0_Types };
extern const Il2CppType Scene_t2348375561_0_0_0;
extern const Il2CppType LoadSceneMode_t3251202195_0_0_0;
static const Il2CppType* GenInst_Scene_t2348375561_0_0_0_LoadSceneMode_t3251202195_0_0_0_Types[] = { &Scene_t2348375561_0_0_0, &LoadSceneMode_t3251202195_0_0_0 };
extern const Il2CppGenericInst GenInst_Scene_t2348375561_0_0_0_LoadSceneMode_t3251202195_0_0_0 = { 2, GenInst_Scene_t2348375561_0_0_0_LoadSceneMode_t3251202195_0_0_0_Types };
static const Il2CppType* GenInst_Scene_t2348375561_0_0_0_Types[] = { &Scene_t2348375561_0_0_0 };
extern const Il2CppGenericInst GenInst_Scene_t2348375561_0_0_0 = { 1, GenInst_Scene_t2348375561_0_0_0_Types };
static const Il2CppType* GenInst_Scene_t2348375561_0_0_0_Scene_t2348375561_0_0_0_Types[] = { &Scene_t2348375561_0_0_0, &Scene_t2348375561_0_0_0 };
extern const Il2CppGenericInst GenInst_Scene_t2348375561_0_0_0_Scene_t2348375561_0_0_0 = { 2, GenInst_Scene_t2348375561_0_0_0_Scene_t2348375561_0_0_0_Types };
extern const Il2CppType Particle_t1882894987_0_0_0;
static const Il2CppType* GenInst_Particle_t1882894987_0_0_0_Types[] = { &Particle_t1882894987_0_0_0 };
extern const Il2CppGenericInst GenInst_Particle_t1882894987_0_0_0 = { 1, GenInst_Particle_t1882894987_0_0_0_Types };
extern const Il2CppType ContactPoint_t3758755253_0_0_0;
static const Il2CppType* GenInst_ContactPoint_t3758755253_0_0_0_Types[] = { &ContactPoint_t3758755253_0_0_0 };
extern const Il2CppGenericInst GenInst_ContactPoint_t3758755253_0_0_0 = { 1, GenInst_ContactPoint_t3758755253_0_0_0_Types };
extern const Il2CppType RaycastHit_t1056001966_0_0_0;
static const Il2CppType* GenInst_RaycastHit_t1056001966_0_0_0_Types[] = { &RaycastHit_t1056001966_0_0_0 };
extern const Il2CppGenericInst GenInst_RaycastHit_t1056001966_0_0_0 = { 1, GenInst_RaycastHit_t1056001966_0_0_0_Types };
extern const Il2CppType Collider_t1773347010_0_0_0;
static const Il2CppType* GenInst_Collider_t1773347010_0_0_0_Types[] = { &Collider_t1773347010_0_0_0 };
extern const Il2CppGenericInst GenInst_Collider_t1773347010_0_0_0 = { 1, GenInst_Collider_t1773347010_0_0_0_Types };
extern const Il2CppType Rigidbody2D_t939494601_0_0_0;
static const Il2CppType* GenInst_Rigidbody2D_t939494601_0_0_0_Types[] = { &Rigidbody2D_t939494601_0_0_0 };
extern const Il2CppGenericInst GenInst_Rigidbody2D_t939494601_0_0_0 = { 1, GenInst_Rigidbody2D_t939494601_0_0_0_Types };
extern const Il2CppType RaycastHit2D_t2279581989_0_0_0;
static const Il2CppType* GenInst_RaycastHit2D_t2279581989_0_0_0_Types[] = { &RaycastHit2D_t2279581989_0_0_0 };
extern const Il2CppGenericInst GenInst_RaycastHit2D_t2279581989_0_0_0 = { 1, GenInst_RaycastHit2D_t2279581989_0_0_0_Types };
extern const Il2CppType ContactPoint2D_t3390240644_0_0_0;
static const Il2CppType* GenInst_ContactPoint2D_t3390240644_0_0_0_Types[] = { &ContactPoint2D_t3390240644_0_0_0 };
extern const Il2CppGenericInst GenInst_ContactPoint2D_t3390240644_0_0_0 = { 1, GenInst_ContactPoint2D_t3390240644_0_0_0_Types };
extern const Il2CppType WebCamDevice_t1322781432_0_0_0;
static const Il2CppType* GenInst_WebCamDevice_t1322781432_0_0_0_Types[] = { &WebCamDevice_t1322781432_0_0_0 };
extern const Il2CppGenericInst GenInst_WebCamDevice_t1322781432_0_0_0 = { 1, GenInst_WebCamDevice_t1322781432_0_0_0_Types };
extern const Il2CppType AnimatorClipInfo_t3156717155_0_0_0;
static const Il2CppType* GenInst_AnimatorClipInfo_t3156717155_0_0_0_Types[] = { &AnimatorClipInfo_t3156717155_0_0_0 };
extern const Il2CppGenericInst GenInst_AnimatorClipInfo_t3156717155_0_0_0 = { 1, GenInst_AnimatorClipInfo_t3156717155_0_0_0_Types };
extern const Il2CppType AnimatorControllerParameter_t1758260042_0_0_0;
static const Il2CppType* GenInst_AnimatorControllerParameter_t1758260042_0_0_0_Types[] = { &AnimatorControllerParameter_t1758260042_0_0_0 };
extern const Il2CppGenericInst GenInst_AnimatorControllerParameter_t1758260042_0_0_0 = { 1, GenInst_AnimatorControllerParameter_t1758260042_0_0_0_Types };
extern const Il2CppType UIVertex_t4057497605_0_0_0;
static const Il2CppType* GenInst_UIVertex_t4057497605_0_0_0_Types[] = { &UIVertex_t4057497605_0_0_0 };
extern const Il2CppGenericInst GenInst_UIVertex_t4057497605_0_0_0 = { 1, GenInst_UIVertex_t4057497605_0_0_0_Types };
extern const Il2CppType UICharInfo_t75501106_0_0_0;
static const Il2CppType* GenInst_UICharInfo_t75501106_0_0_0_Types[] = { &UICharInfo_t75501106_0_0_0 };
extern const Il2CppGenericInst GenInst_UICharInfo_t75501106_0_0_0 = { 1, GenInst_UICharInfo_t75501106_0_0_0_Types };
extern const Il2CppType UILineInfo_t4195266810_0_0_0;
static const Il2CppType* GenInst_UILineInfo_t4195266810_0_0_0_Types[] = { &UILineInfo_t4195266810_0_0_0 };
extern const Il2CppGenericInst GenInst_UILineInfo_t4195266810_0_0_0 = { 1, GenInst_UILineInfo_t4195266810_0_0_0_Types };
extern const Il2CppType Font_t1956802104_0_0_0;
static const Il2CppType* GenInst_Font_t1956802104_0_0_0_Types[] = { &Font_t1956802104_0_0_0 };
extern const Il2CppGenericInst GenInst_Font_t1956802104_0_0_0 = { 1, GenInst_Font_t1956802104_0_0_0_Types };
extern const Il2CppType GUILayoutOption_t811797299_0_0_0;
static const Il2CppType* GenInst_GUILayoutOption_t811797299_0_0_0_Types[] = { &GUILayoutOption_t811797299_0_0_0 };
extern const Il2CppGenericInst GenInst_GUILayoutOption_t811797299_0_0_0 = { 1, GenInst_GUILayoutOption_t811797299_0_0_0_Types };
extern const Il2CppType GUILayoutEntry_t3214611570_0_0_0;
static const Il2CppType* GenInst_GUILayoutEntry_t3214611570_0_0_0_Types[] = { &GUILayoutEntry_t3214611570_0_0_0 };
extern const Il2CppGenericInst GenInst_GUILayoutEntry_t3214611570_0_0_0 = { 1, GenInst_GUILayoutEntry_t3214611570_0_0_0_Types };
extern const Il2CppType LayoutCache_t78309876_0_0_0;
static const Il2CppType* GenInst_Int32_t2950945753_0_0_0_LayoutCache_t78309876_0_0_0_Types[] = { &Int32_t2950945753_0_0_0, &LayoutCache_t78309876_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2950945753_0_0_0_LayoutCache_t78309876_0_0_0 = { 2, GenInst_Int32_t2950945753_0_0_0_LayoutCache_t78309876_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2950945753_0_0_0_Il2CppObject_0_0_0_Types[] = { &Int32_t2950945753_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2950945753_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_Int32_t2950945753_0_0_0_Il2CppObject_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t71524366_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t71524366_0_0_0_Types[] = { &KeyValuePair_2_t71524366_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t71524366_0_0_0 = { 1, GenInst_KeyValuePair_2_t71524366_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2950945753_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Types[] = { &Int32_t2950945753_0_0_0, &Il2CppObject_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2950945753_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0 = { 3, GenInst_Int32_t2950945753_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2950945753_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types[] = { &Int32_t2950945753_0_0_0, &Il2CppObject_0_0_0, &DictionaryEntry_t3123975638_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2950945753_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t3123975638_0_0_0 = { 3, GenInst_Int32_t2950945753_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2950945753_0_0_0_Il2CppObject_0_0_0_KeyValuePair_2_t71524366_0_0_0_Types[] = { &Int32_t2950945753_0_0_0, &Il2CppObject_0_0_0, &KeyValuePair_2_t71524366_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2950945753_0_0_0_Il2CppObject_0_0_0_KeyValuePair_2_t71524366_0_0_0 = { 3, GenInst_Int32_t2950945753_0_0_0_Il2CppObject_0_0_0_KeyValuePair_2_t71524366_0_0_0_Types };
static const Il2CppType* GenInst_LayoutCache_t78309876_0_0_0_Types[] = { &LayoutCache_t78309876_0_0_0 };
extern const Il2CppGenericInst GenInst_LayoutCache_t78309876_0_0_0 = { 1, GenInst_LayoutCache_t78309876_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2950945753_0_0_0_LayoutCache_t78309876_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types[] = { &Int32_t2950945753_0_0_0, &LayoutCache_t78309876_0_0_0, &DictionaryEntry_t3123975638_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2950945753_0_0_0_LayoutCache_t78309876_0_0_0_DictionaryEntry_t3123975638_0_0_0 = { 3, GenInst_Int32_t2950945753_0_0_0_LayoutCache_t78309876_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1364695374_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1364695374_0_0_0_Types[] = { &KeyValuePair_2_t1364695374_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1364695374_0_0_0 = { 1, GenInst_KeyValuePair_2_t1364695374_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2950945753_0_0_0_LayoutCache_t78309876_0_0_0_KeyValuePair_2_t1364695374_0_0_0_Types[] = { &Int32_t2950945753_0_0_0, &LayoutCache_t78309876_0_0_0, &KeyValuePair_2_t1364695374_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2950945753_0_0_0_LayoutCache_t78309876_0_0_0_KeyValuePair_2_t1364695374_0_0_0 = { 3, GenInst_Int32_t2950945753_0_0_0_LayoutCache_t78309876_0_0_0_KeyValuePair_2_t1364695374_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2950945753_0_0_0_LayoutCache_t78309876_0_0_0_LayoutCache_t78309876_0_0_0_Types[] = { &Int32_t2950945753_0_0_0, &LayoutCache_t78309876_0_0_0, &LayoutCache_t78309876_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2950945753_0_0_0_LayoutCache_t78309876_0_0_0_LayoutCache_t78309876_0_0_0 = { 3, GenInst_Int32_t2950945753_0_0_0_LayoutCache_t78309876_0_0_0_LayoutCache_t78309876_0_0_0_Types };
extern const Il2CppType GUIStyle_t3956901511_0_0_0;
static const Il2CppType* GenInst_GUIStyle_t3956901511_0_0_0_Types[] = { &GUIStyle_t3956901511_0_0_0 };
extern const Il2CppGenericInst GenInst_GUIStyle_t3956901511_0_0_0 = { 1, GenInst_GUIStyle_t3956901511_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_GUIStyle_t3956901511_0_0_0_Types[] = { &String_t_0_0_0, &GUIStyle_t3956901511_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_GUIStyle_t3956901511_0_0_0 = { 2, GenInst_String_t_0_0_0_GUIStyle_t3956901511_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_GUIStyle_t3956901511_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types[] = { &String_t_0_0_0, &GUIStyle_t3956901511_0_0_0, &DictionaryEntry_t3123975638_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_GUIStyle_t3956901511_0_0_0_DictionaryEntry_t3123975638_0_0_0 = { 3, GenInst_String_t_0_0_0_GUIStyle_t3956901511_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1844862681_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1844862681_0_0_0_Types[] = { &KeyValuePair_2_t1844862681_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1844862681_0_0_0 = { 1, GenInst_KeyValuePair_2_t1844862681_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_GUIStyle_t3956901511_0_0_0_KeyValuePair_2_t1844862681_0_0_0_Types[] = { &String_t_0_0_0, &GUIStyle_t3956901511_0_0_0, &KeyValuePair_2_t1844862681_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_GUIStyle_t3956901511_0_0_0_KeyValuePair_2_t1844862681_0_0_0 = { 3, GenInst_String_t_0_0_0_GUIStyle_t3956901511_0_0_0_KeyValuePair_2_t1844862681_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_GUIStyle_t3956901511_0_0_0_GUIStyle_t3956901511_0_0_0_Types[] = { &String_t_0_0_0, &GUIStyle_t3956901511_0_0_0, &GUIStyle_t3956901511_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_GUIStyle_t3956901511_0_0_0_GUIStyle_t3956901511_0_0_0 = { 3, GenInst_String_t_0_0_0_GUIStyle_t3956901511_0_0_0_GUIStyle_t3956901511_0_0_0_Types };
extern const Il2CppType DisallowMultipleComponent_t1422053217_0_0_0;
static const Il2CppType* GenInst_DisallowMultipleComponent_t1422053217_0_0_0_Types[] = { &DisallowMultipleComponent_t1422053217_0_0_0 };
extern const Il2CppGenericInst GenInst_DisallowMultipleComponent_t1422053217_0_0_0 = { 1, GenInst_DisallowMultipleComponent_t1422053217_0_0_0_Types };
extern const Il2CppType Attribute_t861562559_0_0_0;
static const Il2CppType* GenInst_Attribute_t861562559_0_0_0_Types[] = { &Attribute_t861562559_0_0_0 };
extern const Il2CppGenericInst GenInst_Attribute_t861562559_0_0_0 = { 1, GenInst_Attribute_t861562559_0_0_0_Types };
extern const Il2CppType _Attribute_t122494719_0_0_0;
static const Il2CppType* GenInst__Attribute_t122494719_0_0_0_Types[] = { &_Attribute_t122494719_0_0_0 };
extern const Il2CppGenericInst GenInst__Attribute_t122494719_0_0_0 = { 1, GenInst__Attribute_t122494719_0_0_0_Types };
extern const Il2CppType ExecuteInEditMode_t3727731349_0_0_0;
static const Il2CppType* GenInst_ExecuteInEditMode_t3727731349_0_0_0_Types[] = { &ExecuteInEditMode_t3727731349_0_0_0 };
extern const Il2CppGenericInst GenInst_ExecuteInEditMode_t3727731349_0_0_0 = { 1, GenInst_ExecuteInEditMode_t3727731349_0_0_0_Types };
extern const Il2CppType RequireComponent_t3490506609_0_0_0;
static const Il2CppType* GenInst_RequireComponent_t3490506609_0_0_0_Types[] = { &RequireComponent_t3490506609_0_0_0 };
extern const Il2CppGenericInst GenInst_RequireComponent_t3490506609_0_0_0 = { 1, GenInst_RequireComponent_t3490506609_0_0_0_Types };
extern const Il2CppType HitInfo_t3229609740_0_0_0;
static const Il2CppType* GenInst_HitInfo_t3229609740_0_0_0_Types[] = { &HitInfo_t3229609740_0_0_0 };
extern const Il2CppGenericInst GenInst_HitInfo_t3229609740_0_0_0 = { 1, GenInst_HitInfo_t3229609740_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Types[] = { &Il2CppObject_0_0_0, &Il2CppObject_0_0_0, &Il2CppObject_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0 = { 4, GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Types };
extern const Il2CppType PersistentCall_t3407714124_0_0_0;
static const Il2CppType* GenInst_PersistentCall_t3407714124_0_0_0_Types[] = { &PersistentCall_t3407714124_0_0_0 };
extern const Il2CppGenericInst GenInst_PersistentCall_t3407714124_0_0_0 = { 1, GenInst_PersistentCall_t3407714124_0_0_0_Types };
extern const Il2CppType BaseInvokableCall_t2703961024_0_0_0;
static const Il2CppType* GenInst_BaseInvokableCall_t2703961024_0_0_0_Types[] = { &BaseInvokableCall_t2703961024_0_0_0 };
extern const Il2CppGenericInst GenInst_BaseInvokableCall_t2703961024_0_0_0 = { 1, GenInst_BaseInvokableCall_t2703961024_0_0_0_Types };
extern const Il2CppType MessageTypeSubscribers_t1684935770_0_0_0;
static const Il2CppType* GenInst_MessageTypeSubscribers_t1684935770_0_0_0_Types[] = { &MessageTypeSubscribers_t1684935770_0_0_0 };
extern const Il2CppGenericInst GenInst_MessageTypeSubscribers_t1684935770_0_0_0 = { 1, GenInst_MessageTypeSubscribers_t1684935770_0_0_0_Types };
static const Il2CppType* GenInst_MessageTypeSubscribers_t1684935770_0_0_0_Boolean_t97287965_0_0_0_Types[] = { &MessageTypeSubscribers_t1684935770_0_0_0, &Boolean_t97287965_0_0_0 };
extern const Il2CppGenericInst GenInst_MessageTypeSubscribers_t1684935770_0_0_0_Boolean_t97287965_0_0_0 = { 2, GenInst_MessageTypeSubscribers_t1684935770_0_0_0_Boolean_t97287965_0_0_0_Types };
extern const Il2CppType MessageEventArgs_t1170575784_0_0_0;
static const Il2CppType* GenInst_MessageEventArgs_t1170575784_0_0_0_Types[] = { &MessageEventArgs_t1170575784_0_0_0 };
extern const Il2CppGenericInst GenInst_MessageEventArgs_t1170575784_0_0_0 = { 1, GenInst_MessageEventArgs_t1170575784_0_0_0_Types };
extern const Il2CppType BaseInputModule_t2019268878_0_0_0;
static const Il2CppType* GenInst_BaseInputModule_t2019268878_0_0_0_Types[] = { &BaseInputModule_t2019268878_0_0_0 };
extern const Il2CppGenericInst GenInst_BaseInputModule_t2019268878_0_0_0 = { 1, GenInst_BaseInputModule_t2019268878_0_0_0_Types };
extern const Il2CppType UIBehaviour_t3495933518_0_0_0;
static const Il2CppType* GenInst_UIBehaviour_t3495933518_0_0_0_Types[] = { &UIBehaviour_t3495933518_0_0_0 };
extern const Il2CppGenericInst GenInst_UIBehaviour_t3495933518_0_0_0 = { 1, GenInst_UIBehaviour_t3495933518_0_0_0_Types };
extern const Il2CppType MonoBehaviour_t3962482529_0_0_0;
static const Il2CppType* GenInst_MonoBehaviour_t3962482529_0_0_0_Types[] = { &MonoBehaviour_t3962482529_0_0_0 };
extern const Il2CppGenericInst GenInst_MonoBehaviour_t3962482529_0_0_0 = { 1, GenInst_MonoBehaviour_t3962482529_0_0_0_Types };
extern const Il2CppType RaycastResult_t3360306849_0_0_0;
static const Il2CppType* GenInst_RaycastResult_t3360306849_0_0_0_Types[] = { &RaycastResult_t3360306849_0_0_0 };
extern const Il2CppGenericInst GenInst_RaycastResult_t3360306849_0_0_0 = { 1, GenInst_RaycastResult_t3360306849_0_0_0_Types };
extern const Il2CppType IDeselectHandler_t393712923_0_0_0;
static const Il2CppType* GenInst_IDeselectHandler_t393712923_0_0_0_Types[] = { &IDeselectHandler_t393712923_0_0_0 };
extern const Il2CppGenericInst GenInst_IDeselectHandler_t393712923_0_0_0 = { 1, GenInst_IDeselectHandler_t393712923_0_0_0_Types };
extern const Il2CppType IEventSystemHandler_t3354683850_0_0_0;
static const Il2CppType* GenInst_IEventSystemHandler_t3354683850_0_0_0_Types[] = { &IEventSystemHandler_t3354683850_0_0_0 };
extern const Il2CppGenericInst GenInst_IEventSystemHandler_t3354683850_0_0_0 = { 1, GenInst_IEventSystemHandler_t3354683850_0_0_0_Types };
extern const Il2CppType List_1_t531791296_0_0_0;
static const Il2CppType* GenInst_List_1_t531791296_0_0_0_Types[] = { &List_1_t531791296_0_0_0 };
extern const Il2CppGenericInst GenInst_List_1_t531791296_0_0_0 = { 1, GenInst_List_1_t531791296_0_0_0_Types };
extern const Il2CppType List_1_t257213610_0_0_0;
static const Il2CppType* GenInst_List_1_t257213610_0_0_0_Types[] = { &List_1_t257213610_0_0_0 };
extern const Il2CppGenericInst GenInst_List_1_t257213610_0_0_0 = { 1, GenInst_List_1_t257213610_0_0_0_Types };
extern const Il2CppType List_1_t3395709193_0_0_0;
static const Il2CppType* GenInst_List_1_t3395709193_0_0_0_Types[] = { &List_1_t3395709193_0_0_0 };
extern const Il2CppGenericInst GenInst_List_1_t3395709193_0_0_0 = { 1, GenInst_List_1_t3395709193_0_0_0_Types };
extern const Il2CppType ISelectHandler_t2271418839_0_0_0;
static const Il2CppType* GenInst_ISelectHandler_t2271418839_0_0_0_Types[] = { &ISelectHandler_t2271418839_0_0_0 };
extern const Il2CppGenericInst GenInst_ISelectHandler_t2271418839_0_0_0 = { 1, GenInst_ISelectHandler_t2271418839_0_0_0_Types };
extern const Il2CppType BaseRaycaster_t4150874583_0_0_0;
static const Il2CppType* GenInst_BaseRaycaster_t4150874583_0_0_0_Types[] = { &BaseRaycaster_t4150874583_0_0_0 };
extern const Il2CppGenericInst GenInst_BaseRaycaster_t4150874583_0_0_0 = { 1, GenInst_BaseRaycaster_t4150874583_0_0_0_Types };
extern const Il2CppType Entry_t3344766165_0_0_0;
static const Il2CppType* GenInst_Entry_t3344766165_0_0_0_Types[] = { &Entry_t3344766165_0_0_0 };
extern const Il2CppGenericInst GenInst_Entry_t3344766165_0_0_0 = { 1, GenInst_Entry_t3344766165_0_0_0_Types };
extern const Il2CppType BaseEventData_t3903027533_0_0_0;
static const Il2CppType* GenInst_BaseEventData_t3903027533_0_0_0_Types[] = { &BaseEventData_t3903027533_0_0_0 };
extern const Il2CppGenericInst GenInst_BaseEventData_t3903027533_0_0_0 = { 1, GenInst_BaseEventData_t3903027533_0_0_0_Types };
extern const Il2CppType IPointerEnterHandler_t1016128679_0_0_0;
static const Il2CppType* GenInst_IPointerEnterHandler_t1016128679_0_0_0_Types[] = { &IPointerEnterHandler_t1016128679_0_0_0 };
extern const Il2CppGenericInst GenInst_IPointerEnterHandler_t1016128679_0_0_0 = { 1, GenInst_IPointerEnterHandler_t1016128679_0_0_0_Types };
extern const Il2CppType IPointerExitHandler_t4182793654_0_0_0;
static const Il2CppType* GenInst_IPointerExitHandler_t4182793654_0_0_0_Types[] = { &IPointerExitHandler_t4182793654_0_0_0 };
extern const Il2CppGenericInst GenInst_IPointerExitHandler_t4182793654_0_0_0 = { 1, GenInst_IPointerExitHandler_t4182793654_0_0_0_Types };
extern const Il2CppType IPointerDownHandler_t1380080529_0_0_0;
static const Il2CppType* GenInst_IPointerDownHandler_t1380080529_0_0_0_Types[] = { &IPointerDownHandler_t1380080529_0_0_0 };
extern const Il2CppGenericInst GenInst_IPointerDownHandler_t1380080529_0_0_0 = { 1, GenInst_IPointerDownHandler_t1380080529_0_0_0_Types };
extern const Il2CppType IPointerUpHandler_t277099170_0_0_0;
static const Il2CppType* GenInst_IPointerUpHandler_t277099170_0_0_0_Types[] = { &IPointerUpHandler_t277099170_0_0_0 };
extern const Il2CppGenericInst GenInst_IPointerUpHandler_t277099170_0_0_0 = { 1, GenInst_IPointerUpHandler_t277099170_0_0_0_Types };
extern const Il2CppType IPointerClickHandler_t132471142_0_0_0;
static const Il2CppType* GenInst_IPointerClickHandler_t132471142_0_0_0_Types[] = { &IPointerClickHandler_t132471142_0_0_0 };
extern const Il2CppGenericInst GenInst_IPointerClickHandler_t132471142_0_0_0 = { 1, GenInst_IPointerClickHandler_t132471142_0_0_0_Types };
extern const Il2CppType IInitializePotentialDragHandler_t608041180_0_0_0;
static const Il2CppType* GenInst_IInitializePotentialDragHandler_t608041180_0_0_0_Types[] = { &IInitializePotentialDragHandler_t608041180_0_0_0 };
extern const Il2CppGenericInst GenInst_IInitializePotentialDragHandler_t608041180_0_0_0 = { 1, GenInst_IInitializePotentialDragHandler_t608041180_0_0_0_Types };
extern const Il2CppType IBeginDragHandler_t3293314358_0_0_0;
static const Il2CppType* GenInst_IBeginDragHandler_t3293314358_0_0_0_Types[] = { &IBeginDragHandler_t3293314358_0_0_0 };
extern const Il2CppGenericInst GenInst_IBeginDragHandler_t3293314358_0_0_0 = { 1, GenInst_IBeginDragHandler_t3293314358_0_0_0_Types };
extern const Il2CppType IDragHandler_t2288426503_0_0_0;
static const Il2CppType* GenInst_IDragHandler_t2288426503_0_0_0_Types[] = { &IDragHandler_t2288426503_0_0_0 };
extern const Il2CppGenericInst GenInst_IDragHandler_t2288426503_0_0_0 = { 1, GenInst_IDragHandler_t2288426503_0_0_0_Types };
extern const Il2CppType IEndDragHandler_t297508562_0_0_0;
static const Il2CppType* GenInst_IEndDragHandler_t297508562_0_0_0_Types[] = { &IEndDragHandler_t297508562_0_0_0 };
extern const Il2CppGenericInst GenInst_IEndDragHandler_t297508562_0_0_0 = { 1, GenInst_IEndDragHandler_t297508562_0_0_0_Types };
extern const Il2CppType IDropHandler_t3627139509_0_0_0;
static const Il2CppType* GenInst_IDropHandler_t3627139509_0_0_0_Types[] = { &IDropHandler_t3627139509_0_0_0 };
extern const Il2CppGenericInst GenInst_IDropHandler_t3627139509_0_0_0 = { 1, GenInst_IDropHandler_t3627139509_0_0_0_Types };
extern const Il2CppType IScrollHandler_t4201797704_0_0_0;
static const Il2CppType* GenInst_IScrollHandler_t4201797704_0_0_0_Types[] = { &IScrollHandler_t4201797704_0_0_0 };
extern const Il2CppGenericInst GenInst_IScrollHandler_t4201797704_0_0_0 = { 1, GenInst_IScrollHandler_t4201797704_0_0_0_Types };
extern const Il2CppType IUpdateSelectedHandler_t4266291469_0_0_0;
static const Il2CppType* GenInst_IUpdateSelectedHandler_t4266291469_0_0_0_Types[] = { &IUpdateSelectedHandler_t4266291469_0_0_0 };
extern const Il2CppGenericInst GenInst_IUpdateSelectedHandler_t4266291469_0_0_0 = { 1, GenInst_IUpdateSelectedHandler_t4266291469_0_0_0_Types };
extern const Il2CppType IMoveHandler_t933334182_0_0_0;
static const Il2CppType* GenInst_IMoveHandler_t933334182_0_0_0_Types[] = { &IMoveHandler_t933334182_0_0_0 };
extern const Il2CppGenericInst GenInst_IMoveHandler_t933334182_0_0_0 = { 1, GenInst_IMoveHandler_t933334182_0_0_0_Types };
extern const Il2CppType ISubmitHandler_t2790798304_0_0_0;
static const Il2CppType* GenInst_ISubmitHandler_t2790798304_0_0_0_Types[] = { &ISubmitHandler_t2790798304_0_0_0 };
extern const Il2CppGenericInst GenInst_ISubmitHandler_t2790798304_0_0_0 = { 1, GenInst_ISubmitHandler_t2790798304_0_0_0_Types };
extern const Il2CppType ICancelHandler_t3974364820_0_0_0;
static const Il2CppType* GenInst_ICancelHandler_t3974364820_0_0_0_Types[] = { &ICancelHandler_t3974364820_0_0_0 };
extern const Il2CppGenericInst GenInst_ICancelHandler_t3974364820_0_0_0 = { 1, GenInst_ICancelHandler_t3974364820_0_0_0_Types };
extern const Il2CppType Transform_t3600365921_0_0_0;
static const Il2CppType* GenInst_Transform_t3600365921_0_0_0_Types[] = { &Transform_t3600365921_0_0_0 };
extern const Il2CppGenericInst GenInst_Transform_t3600365921_0_0_0 = { 1, GenInst_Transform_t3600365921_0_0_0_Types };
extern const Il2CppType BaseInput_t3630163547_0_0_0;
static const Il2CppType* GenInst_BaseInput_t3630163547_0_0_0_Types[] = { &BaseInput_t3630163547_0_0_0 };
extern const Il2CppGenericInst GenInst_BaseInput_t3630163547_0_0_0 = { 1, GenInst_BaseInput_t3630163547_0_0_0_Types };
extern const Il2CppType PointerEventData_t3807901092_0_0_0;
static const Il2CppType* GenInst_Int32_t2950945753_0_0_0_PointerEventData_t3807901092_0_0_0_Types[] = { &Int32_t2950945753_0_0_0, &PointerEventData_t3807901092_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2950945753_0_0_0_PointerEventData_t3807901092_0_0_0 = { 2, GenInst_Int32_t2950945753_0_0_0_PointerEventData_t3807901092_0_0_0_Types };
static const Il2CppType* GenInst_PointerEventData_t3807901092_0_0_0_Types[] = { &PointerEventData_t3807901092_0_0_0 };
extern const Il2CppGenericInst GenInst_PointerEventData_t3807901092_0_0_0 = { 1, GenInst_PointerEventData_t3807901092_0_0_0_Types };
extern const Il2CppType AbstractEventData_t4171500731_0_0_0;
static const Il2CppType* GenInst_AbstractEventData_t4171500731_0_0_0_Types[] = { &AbstractEventData_t4171500731_0_0_0 };
extern const Il2CppGenericInst GenInst_AbstractEventData_t4171500731_0_0_0 = { 1, GenInst_AbstractEventData_t4171500731_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2950945753_0_0_0_PointerEventData_t3807901092_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types[] = { &Int32_t2950945753_0_0_0, &PointerEventData_t3807901092_0_0_0, &DictionaryEntry_t3123975638_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2950945753_0_0_0_PointerEventData_t3807901092_0_0_0_DictionaryEntry_t3123975638_0_0_0 = { 3, GenInst_Int32_t2950945753_0_0_0_PointerEventData_t3807901092_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t799319294_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t799319294_0_0_0_Types[] = { &KeyValuePair_2_t799319294_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t799319294_0_0_0 = { 1, GenInst_KeyValuePair_2_t799319294_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2950945753_0_0_0_PointerEventData_t3807901092_0_0_0_KeyValuePair_2_t799319294_0_0_0_Types[] = { &Int32_t2950945753_0_0_0, &PointerEventData_t3807901092_0_0_0, &KeyValuePair_2_t799319294_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2950945753_0_0_0_PointerEventData_t3807901092_0_0_0_KeyValuePair_2_t799319294_0_0_0 = { 3, GenInst_Int32_t2950945753_0_0_0_PointerEventData_t3807901092_0_0_0_KeyValuePair_2_t799319294_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2950945753_0_0_0_PointerEventData_t3807901092_0_0_0_PointerEventData_t3807901092_0_0_0_Types[] = { &Int32_t2950945753_0_0_0, &PointerEventData_t3807901092_0_0_0, &PointerEventData_t3807901092_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2950945753_0_0_0_PointerEventData_t3807901092_0_0_0_PointerEventData_t3807901092_0_0_0 = { 3, GenInst_Int32_t2950945753_0_0_0_PointerEventData_t3807901092_0_0_0_PointerEventData_t3807901092_0_0_0_Types };
extern const Il2CppType ButtonState_t857139936_0_0_0;
static const Il2CppType* GenInst_ButtonState_t857139936_0_0_0_Types[] = { &ButtonState_t857139936_0_0_0 };
extern const Il2CppGenericInst GenInst_ButtonState_t857139936_0_0_0 = { 1, GenInst_ButtonState_t857139936_0_0_0_Types };
extern const Il2CppType ICanvasElement_t2121898866_0_0_0;
static const Il2CppType* GenInst_ICanvasElement_t2121898866_0_0_0_Types[] = { &ICanvasElement_t2121898866_0_0_0 };
extern const Il2CppGenericInst GenInst_ICanvasElement_t2121898866_0_0_0 = { 1, GenInst_ICanvasElement_t2121898866_0_0_0_Types };
static const Il2CppType* GenInst_ICanvasElement_t2121898866_0_0_0_Int32_t2950945753_0_0_0_Types[] = { &ICanvasElement_t2121898866_0_0_0, &Int32_t2950945753_0_0_0 };
extern const Il2CppGenericInst GenInst_ICanvasElement_t2121898866_0_0_0_Int32_t2950945753_0_0_0 = { 2, GenInst_ICanvasElement_t2121898866_0_0_0_Int32_t2950945753_0_0_0_Types };
static const Il2CppType* GenInst_ICanvasElement_t2121898866_0_0_0_Int32_t2950945753_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types[] = { &ICanvasElement_t2121898866_0_0_0, &Int32_t2950945753_0_0_0, &DictionaryEntry_t3123975638_0_0_0 };
extern const Il2CppGenericInst GenInst_ICanvasElement_t2121898866_0_0_0_Int32_t2950945753_0_0_0_DictionaryEntry_t3123975638_0_0_0 = { 3, GenInst_ICanvasElement_t2121898866_0_0_0_Int32_t2950945753_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1364477206_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1364477206_0_0_0_Types[] = { &KeyValuePair_2_t1364477206_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1364477206_0_0_0 = { 1, GenInst_KeyValuePair_2_t1364477206_0_0_0_Types };
static const Il2CppType* GenInst_ICanvasElement_t2121898866_0_0_0_Int32_t2950945753_0_0_0_KeyValuePair_2_t1364477206_0_0_0_Types[] = { &ICanvasElement_t2121898866_0_0_0, &Int32_t2950945753_0_0_0, &KeyValuePair_2_t1364477206_0_0_0 };
extern const Il2CppGenericInst GenInst_ICanvasElement_t2121898866_0_0_0_Int32_t2950945753_0_0_0_KeyValuePair_2_t1364477206_0_0_0 = { 3, GenInst_ICanvasElement_t2121898866_0_0_0_Int32_t2950945753_0_0_0_KeyValuePair_2_t1364477206_0_0_0_Types };
static const Il2CppType* GenInst_ICanvasElement_t2121898866_0_0_0_Int32_t2950945753_0_0_0_Int32_t2950945753_0_0_0_Types[] = { &ICanvasElement_t2121898866_0_0_0, &Int32_t2950945753_0_0_0, &Int32_t2950945753_0_0_0 };
extern const Il2CppGenericInst GenInst_ICanvasElement_t2121898866_0_0_0_Int32_t2950945753_0_0_0_Int32_t2950945753_0_0_0 = { 3, GenInst_ICanvasElement_t2121898866_0_0_0_Int32_t2950945753_0_0_0_Int32_t2950945753_0_0_0_Types };
extern const Il2CppType ColorBlock_t2139031574_0_0_0;
static const Il2CppType* GenInst_ColorBlock_t2139031574_0_0_0_Types[] = { &ColorBlock_t2139031574_0_0_0 };
extern const Il2CppGenericInst GenInst_ColorBlock_t2139031574_0_0_0 = { 1, GenInst_ColorBlock_t2139031574_0_0_0_Types };
extern const Il2CppType OptionData_t3270282352_0_0_0;
static const Il2CppType* GenInst_OptionData_t3270282352_0_0_0_Types[] = { &OptionData_t3270282352_0_0_0 };
extern const Il2CppGenericInst GenInst_OptionData_t3270282352_0_0_0 = { 1, GenInst_OptionData_t3270282352_0_0_0_Types };
extern const Il2CppType DropdownItem_t1451952895_0_0_0;
static const Il2CppType* GenInst_DropdownItem_t1451952895_0_0_0_Types[] = { &DropdownItem_t1451952895_0_0_0 };
extern const Il2CppGenericInst GenInst_DropdownItem_t1451952895_0_0_0 = { 1, GenInst_DropdownItem_t1451952895_0_0_0_Types };
extern const Il2CppType FloatTween_t1274330004_0_0_0;
static const Il2CppType* GenInst_FloatTween_t1274330004_0_0_0_Types[] = { &FloatTween_t1274330004_0_0_0 };
extern const Il2CppGenericInst GenInst_FloatTween_t1274330004_0_0_0 = { 1, GenInst_FloatTween_t1274330004_0_0_0_Types };
extern const Il2CppType Sprite_t280657092_0_0_0;
static const Il2CppType* GenInst_Sprite_t280657092_0_0_0_Types[] = { &Sprite_t280657092_0_0_0 };
extern const Il2CppGenericInst GenInst_Sprite_t280657092_0_0_0 = { 1, GenInst_Sprite_t280657092_0_0_0_Types };
extern const Il2CppType Canvas_t3310196443_0_0_0;
static const Il2CppType* GenInst_Canvas_t3310196443_0_0_0_Types[] = { &Canvas_t3310196443_0_0_0 };
extern const Il2CppGenericInst GenInst_Canvas_t3310196443_0_0_0 = { 1, GenInst_Canvas_t3310196443_0_0_0_Types };
extern const Il2CppType List_1_t487303889_0_0_0;
static const Il2CppType* GenInst_List_1_t487303889_0_0_0_Types[] = { &List_1_t487303889_0_0_0 };
extern const Il2CppGenericInst GenInst_List_1_t487303889_0_0_0 = { 1, GenInst_List_1_t487303889_0_0_0_Types };
extern const Il2CppType HashSet_1_t466832188_0_0_0;
static const Il2CppType* GenInst_Font_t1956802104_0_0_0_HashSet_1_t466832188_0_0_0_Types[] = { &Font_t1956802104_0_0_0, &HashSet_1_t466832188_0_0_0 };
extern const Il2CppGenericInst GenInst_Font_t1956802104_0_0_0_HashSet_1_t466832188_0_0_0 = { 2, GenInst_Font_t1956802104_0_0_0_HashSet_1_t466832188_0_0_0_Types };
extern const Il2CppType Text_t1901882714_0_0_0;
static const Il2CppType* GenInst_Text_t1901882714_0_0_0_Types[] = { &Text_t1901882714_0_0_0 };
extern const Il2CppGenericInst GenInst_Text_t1901882714_0_0_0 = { 1, GenInst_Text_t1901882714_0_0_0_Types };
extern const Il2CppType Link_t2031043523_0_0_0;
static const Il2CppType* GenInst_Link_t2031043523_0_0_0_Types[] = { &Link_t2031043523_0_0_0 };
extern const Il2CppGenericInst GenInst_Link_t2031043523_0_0_0 = { 1, GenInst_Link_t2031043523_0_0_0_Types };
extern const Il2CppType ILayoutElement_t4082016710_0_0_0;
static const Il2CppType* GenInst_ILayoutElement_t4082016710_0_0_0_Types[] = { &ILayoutElement_t4082016710_0_0_0 };
extern const Il2CppGenericInst GenInst_ILayoutElement_t4082016710_0_0_0 = { 1, GenInst_ILayoutElement_t4082016710_0_0_0_Types };
extern const Il2CppType MaskableGraphic_t3839221559_0_0_0;
static const Il2CppType* GenInst_MaskableGraphic_t3839221559_0_0_0_Types[] = { &MaskableGraphic_t3839221559_0_0_0 };
extern const Il2CppGenericInst GenInst_MaskableGraphic_t3839221559_0_0_0 = { 1, GenInst_MaskableGraphic_t3839221559_0_0_0_Types };
extern const Il2CppType IClippable_t1239629351_0_0_0;
static const Il2CppType* GenInst_IClippable_t1239629351_0_0_0_Types[] = { &IClippable_t1239629351_0_0_0 };
extern const Il2CppGenericInst GenInst_IClippable_t1239629351_0_0_0 = { 1, GenInst_IClippable_t1239629351_0_0_0_Types };
extern const Il2CppType IMaskable_t433386433_0_0_0;
static const Il2CppType* GenInst_IMaskable_t433386433_0_0_0_Types[] = { &IMaskable_t433386433_0_0_0 };
extern const Il2CppGenericInst GenInst_IMaskable_t433386433_0_0_0 = { 1, GenInst_IMaskable_t433386433_0_0_0_Types };
extern const Il2CppType IMaterialModifier_t1975025690_0_0_0;
static const Il2CppType* GenInst_IMaterialModifier_t1975025690_0_0_0_Types[] = { &IMaterialModifier_t1975025690_0_0_0 };
extern const Il2CppGenericInst GenInst_IMaterialModifier_t1975025690_0_0_0 = { 1, GenInst_IMaterialModifier_t1975025690_0_0_0_Types };
extern const Il2CppType Graphic_t1660335611_0_0_0;
static const Il2CppType* GenInst_Graphic_t1660335611_0_0_0_Types[] = { &Graphic_t1660335611_0_0_0 };
extern const Il2CppGenericInst GenInst_Graphic_t1660335611_0_0_0 = { 1, GenInst_Graphic_t1660335611_0_0_0_Types };
static const Il2CppType* GenInst_HashSet_1_t466832188_0_0_0_Types[] = { &HashSet_1_t466832188_0_0_0 };
extern const Il2CppGenericInst GenInst_HashSet_1_t466832188_0_0_0 = { 1, GenInst_HashSet_1_t466832188_0_0_0_Types };
static const Il2CppType* GenInst_Font_t1956802104_0_0_0_HashSet_1_t466832188_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types[] = { &Font_t1956802104_0_0_0, &HashSet_1_t466832188_0_0_0, &DictionaryEntry_t3123975638_0_0_0 };
extern const Il2CppGenericInst GenInst_Font_t1956802104_0_0_0_HashSet_1_t466832188_0_0_0_DictionaryEntry_t3123975638_0_0_0 = { 3, GenInst_Font_t1956802104_0_0_0_HashSet_1_t466832188_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2767015899_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2767015899_0_0_0_Types[] = { &KeyValuePair_2_t2767015899_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2767015899_0_0_0 = { 1, GenInst_KeyValuePair_2_t2767015899_0_0_0_Types };
static const Il2CppType* GenInst_Font_t1956802104_0_0_0_HashSet_1_t466832188_0_0_0_KeyValuePair_2_t2767015899_0_0_0_Types[] = { &Font_t1956802104_0_0_0, &HashSet_1_t466832188_0_0_0, &KeyValuePair_2_t2767015899_0_0_0 };
extern const Il2CppGenericInst GenInst_Font_t1956802104_0_0_0_HashSet_1_t466832188_0_0_0_KeyValuePair_2_t2767015899_0_0_0 = { 3, GenInst_Font_t1956802104_0_0_0_HashSet_1_t466832188_0_0_0_KeyValuePair_2_t2767015899_0_0_0_Types };
static const Il2CppType* GenInst_Font_t1956802104_0_0_0_HashSet_1_t466832188_0_0_0_HashSet_1_t466832188_0_0_0_Types[] = { &Font_t1956802104_0_0_0, &HashSet_1_t466832188_0_0_0, &HashSet_1_t466832188_0_0_0 };
extern const Il2CppGenericInst GenInst_Font_t1956802104_0_0_0_HashSet_1_t466832188_0_0_0_HashSet_1_t466832188_0_0_0 = { 3, GenInst_Font_t1956802104_0_0_0_HashSet_1_t466832188_0_0_0_HashSet_1_t466832188_0_0_0_Types };
extern const Il2CppType ColorTween_t809614380_0_0_0;
static const Il2CppType* GenInst_ColorTween_t809614380_0_0_0_Types[] = { &ColorTween_t809614380_0_0_0 };
extern const Il2CppGenericInst GenInst_ColorTween_t809614380_0_0_0 = { 1, GenInst_ColorTween_t809614380_0_0_0_Types };
extern const Il2CppType IndexedSet_1_t3109723551_0_0_0;
static const Il2CppType* GenInst_Canvas_t3310196443_0_0_0_IndexedSet_1_t3109723551_0_0_0_Types[] = { &Canvas_t3310196443_0_0_0, &IndexedSet_1_t3109723551_0_0_0 };
extern const Il2CppGenericInst GenInst_Canvas_t3310196443_0_0_0_IndexedSet_1_t3109723551_0_0_0 = { 2, GenInst_Canvas_t3310196443_0_0_0_IndexedSet_1_t3109723551_0_0_0_Types };
static const Il2CppType* GenInst_Graphic_t1660335611_0_0_0_Int32_t2950945753_0_0_0_Types[] = { &Graphic_t1660335611_0_0_0, &Int32_t2950945753_0_0_0 };
extern const Il2CppGenericInst GenInst_Graphic_t1660335611_0_0_0_Int32_t2950945753_0_0_0 = { 2, GenInst_Graphic_t1660335611_0_0_0_Int32_t2950945753_0_0_0_Types };
static const Il2CppType* GenInst_Graphic_t1660335611_0_0_0_Int32_t2950945753_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types[] = { &Graphic_t1660335611_0_0_0, &Int32_t2950945753_0_0_0, &DictionaryEntry_t3123975638_0_0_0 };
extern const Il2CppGenericInst GenInst_Graphic_t1660335611_0_0_0_Int32_t2950945753_0_0_0_DictionaryEntry_t3123975638_0_0_0 = { 3, GenInst_Graphic_t1660335611_0_0_0_Int32_t2950945753_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t3639519817_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t3639519817_0_0_0_Types[] = { &KeyValuePair_2_t3639519817_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3639519817_0_0_0 = { 1, GenInst_KeyValuePair_2_t3639519817_0_0_0_Types };
static const Il2CppType* GenInst_Graphic_t1660335611_0_0_0_Int32_t2950945753_0_0_0_KeyValuePair_2_t3639519817_0_0_0_Types[] = { &Graphic_t1660335611_0_0_0, &Int32_t2950945753_0_0_0, &KeyValuePair_2_t3639519817_0_0_0 };
extern const Il2CppGenericInst GenInst_Graphic_t1660335611_0_0_0_Int32_t2950945753_0_0_0_KeyValuePair_2_t3639519817_0_0_0 = { 3, GenInst_Graphic_t1660335611_0_0_0_Int32_t2950945753_0_0_0_KeyValuePair_2_t3639519817_0_0_0_Types };
static const Il2CppType* GenInst_Graphic_t1660335611_0_0_0_Int32_t2950945753_0_0_0_Int32_t2950945753_0_0_0_Types[] = { &Graphic_t1660335611_0_0_0, &Int32_t2950945753_0_0_0, &Int32_t2950945753_0_0_0 };
extern const Il2CppGenericInst GenInst_Graphic_t1660335611_0_0_0_Int32_t2950945753_0_0_0_Int32_t2950945753_0_0_0 = { 3, GenInst_Graphic_t1660335611_0_0_0_Int32_t2950945753_0_0_0_Int32_t2950945753_0_0_0_Types };
static const Il2CppType* GenInst_IndexedSet_1_t3109723551_0_0_0_Types[] = { &IndexedSet_1_t3109723551_0_0_0 };
extern const Il2CppGenericInst GenInst_IndexedSet_1_t3109723551_0_0_0 = { 1, GenInst_IndexedSet_1_t3109723551_0_0_0_Types };
static const Il2CppType* GenInst_Canvas_t3310196443_0_0_0_IndexedSet_1_t3109723551_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types[] = { &Canvas_t3310196443_0_0_0, &IndexedSet_1_t3109723551_0_0_0, &DictionaryEntry_t3123975638_0_0_0 };
extern const Il2CppGenericInst GenInst_Canvas_t3310196443_0_0_0_IndexedSet_1_t3109723551_0_0_0_DictionaryEntry_t3123975638_0_0_0 = { 3, GenInst_Canvas_t3310196443_0_0_0_IndexedSet_1_t3109723551_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t398822319_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t398822319_0_0_0_Types[] = { &KeyValuePair_2_t398822319_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t398822319_0_0_0 = { 1, GenInst_KeyValuePair_2_t398822319_0_0_0_Types };
static const Il2CppType* GenInst_Canvas_t3310196443_0_0_0_IndexedSet_1_t3109723551_0_0_0_KeyValuePair_2_t398822319_0_0_0_Types[] = { &Canvas_t3310196443_0_0_0, &IndexedSet_1_t3109723551_0_0_0, &KeyValuePair_2_t398822319_0_0_0 };
extern const Il2CppGenericInst GenInst_Canvas_t3310196443_0_0_0_IndexedSet_1_t3109723551_0_0_0_KeyValuePair_2_t398822319_0_0_0 = { 3, GenInst_Canvas_t3310196443_0_0_0_IndexedSet_1_t3109723551_0_0_0_KeyValuePair_2_t398822319_0_0_0_Types };
static const Il2CppType* GenInst_Canvas_t3310196443_0_0_0_IndexedSet_1_t3109723551_0_0_0_IndexedSet_1_t3109723551_0_0_0_Types[] = { &Canvas_t3310196443_0_0_0, &IndexedSet_1_t3109723551_0_0_0, &IndexedSet_1_t3109723551_0_0_0 };
extern const Il2CppGenericInst GenInst_Canvas_t3310196443_0_0_0_IndexedSet_1_t3109723551_0_0_0_IndexedSet_1_t3109723551_0_0_0 = { 3, GenInst_Canvas_t3310196443_0_0_0_IndexedSet_1_t3109723551_0_0_0_IndexedSet_1_t3109723551_0_0_0_Types };
extern const Il2CppType Type_t1152881528_0_0_0;
static const Il2CppType* GenInst_Type_t1152881528_0_0_0_Types[] = { &Type_t1152881528_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t1152881528_0_0_0 = { 1, GenInst_Type_t1152881528_0_0_0_Types };
extern const Il2CppType FillMethod_t1167457570_0_0_0;
static const Il2CppType* GenInst_FillMethod_t1167457570_0_0_0_Types[] = { &FillMethod_t1167457570_0_0_0 };
extern const Il2CppGenericInst GenInst_FillMethod_t1167457570_0_0_0 = { 1, GenInst_FillMethod_t1167457570_0_0_0_Types };
extern const Il2CppType ContentType_t1787303396_0_0_0;
static const Il2CppType* GenInst_ContentType_t1787303396_0_0_0_Types[] = { &ContentType_t1787303396_0_0_0 };
extern const Il2CppGenericInst GenInst_ContentType_t1787303396_0_0_0 = { 1, GenInst_ContentType_t1787303396_0_0_0_Types };
extern const Il2CppType LineType_t4214648469_0_0_0;
static const Il2CppType* GenInst_LineType_t4214648469_0_0_0_Types[] = { &LineType_t4214648469_0_0_0 };
extern const Il2CppGenericInst GenInst_LineType_t4214648469_0_0_0 = { 1, GenInst_LineType_t4214648469_0_0_0_Types };
extern const Il2CppType InputType_t1770400679_0_0_0;
static const Il2CppType* GenInst_InputType_t1770400679_0_0_0_Types[] = { &InputType_t1770400679_0_0_0 };
extern const Il2CppGenericInst GenInst_InputType_t1770400679_0_0_0 = { 1, GenInst_InputType_t1770400679_0_0_0_Types };
extern const Il2CppType TouchScreenKeyboardType_t1530597702_0_0_0;
static const Il2CppType* GenInst_TouchScreenKeyboardType_t1530597702_0_0_0_Types[] = { &TouchScreenKeyboardType_t1530597702_0_0_0 };
extern const Il2CppGenericInst GenInst_TouchScreenKeyboardType_t1530597702_0_0_0 = { 1, GenInst_TouchScreenKeyboardType_t1530597702_0_0_0_Types };
extern const Il2CppType CharacterValidation_t4051914437_0_0_0;
static const Il2CppType* GenInst_CharacterValidation_t4051914437_0_0_0_Types[] = { &CharacterValidation_t4051914437_0_0_0 };
extern const Il2CppGenericInst GenInst_CharacterValidation_t4051914437_0_0_0 = { 1, GenInst_CharacterValidation_t4051914437_0_0_0_Types };
extern const Il2CppType Mask_t1803652131_0_0_0;
static const Il2CppType* GenInst_Mask_t1803652131_0_0_0_Types[] = { &Mask_t1803652131_0_0_0 };
extern const Il2CppGenericInst GenInst_Mask_t1803652131_0_0_0 = { 1, GenInst_Mask_t1803652131_0_0_0_Types };
extern const Il2CppType ICanvasRaycastFilter_t2454702837_0_0_0;
static const Il2CppType* GenInst_ICanvasRaycastFilter_t2454702837_0_0_0_Types[] = { &ICanvasRaycastFilter_t2454702837_0_0_0 };
extern const Il2CppGenericInst GenInst_ICanvasRaycastFilter_t2454702837_0_0_0 = { 1, GenInst_ICanvasRaycastFilter_t2454702837_0_0_0_Types };
extern const Il2CppType List_1_t3275726873_0_0_0;
static const Il2CppType* GenInst_List_1_t3275726873_0_0_0_Types[] = { &List_1_t3275726873_0_0_0 };
extern const Il2CppGenericInst GenInst_List_1_t3275726873_0_0_0 = { 1, GenInst_List_1_t3275726873_0_0_0_Types };
extern const Il2CppType RectMask2D_t3474889437_0_0_0;
static const Il2CppType* GenInst_RectMask2D_t3474889437_0_0_0_Types[] = { &RectMask2D_t3474889437_0_0_0 };
extern const Il2CppGenericInst GenInst_RectMask2D_t3474889437_0_0_0 = { 1, GenInst_RectMask2D_t3474889437_0_0_0_Types };
extern const Il2CppType IClipper_t1224123152_0_0_0;
static const Il2CppType* GenInst_IClipper_t1224123152_0_0_0_Types[] = { &IClipper_t1224123152_0_0_0 };
extern const Il2CppGenericInst GenInst_IClipper_t1224123152_0_0_0 = { 1, GenInst_IClipper_t1224123152_0_0_0_Types };
extern const Il2CppType List_1_t651996883_0_0_0;
static const Il2CppType* GenInst_List_1_t651996883_0_0_0_Types[] = { &List_1_t651996883_0_0_0 };
extern const Il2CppGenericInst GenInst_List_1_t651996883_0_0_0 = { 1, GenInst_List_1_t651996883_0_0_0_Types };
extern const Il2CppType Navigation_t3049316579_0_0_0;
static const Il2CppType* GenInst_Navigation_t3049316579_0_0_0_Types[] = { &Navigation_t3049316579_0_0_0 };
extern const Il2CppGenericInst GenInst_Navigation_t3049316579_0_0_0 = { 1, GenInst_Navigation_t3049316579_0_0_0_Types };
extern const Il2CppType Link_t1368790160_0_0_0;
static const Il2CppType* GenInst_Link_t1368790160_0_0_0_Types[] = { &Link_t1368790160_0_0_0 };
extern const Il2CppGenericInst GenInst_Link_t1368790160_0_0_0 = { 1, GenInst_Link_t1368790160_0_0_0_Types };
extern const Il2CppType Direction_t3470714353_0_0_0;
static const Il2CppType* GenInst_Direction_t3470714353_0_0_0_Types[] = { &Direction_t3470714353_0_0_0 };
extern const Il2CppGenericInst GenInst_Direction_t3470714353_0_0_0 = { 1, GenInst_Direction_t3470714353_0_0_0_Types };
extern const Il2CppType Selectable_t3250028441_0_0_0;
static const Il2CppType* GenInst_Selectable_t3250028441_0_0_0_Types[] = { &Selectable_t3250028441_0_0_0 };
extern const Il2CppGenericInst GenInst_Selectable_t3250028441_0_0_0 = { 1, GenInst_Selectable_t3250028441_0_0_0_Types };
extern const Il2CppType Transition_t1769908631_0_0_0;
static const Il2CppType* GenInst_Transition_t1769908631_0_0_0_Types[] = { &Transition_t1769908631_0_0_0 };
extern const Il2CppGenericInst GenInst_Transition_t1769908631_0_0_0 = { 1, GenInst_Transition_t1769908631_0_0_0_Types };
extern const Il2CppType SpriteState_t1362986479_0_0_0;
static const Il2CppType* GenInst_SpriteState_t1362986479_0_0_0_Types[] = { &SpriteState_t1362986479_0_0_0 };
extern const Il2CppGenericInst GenInst_SpriteState_t1362986479_0_0_0 = { 1, GenInst_SpriteState_t1362986479_0_0_0_Types };
extern const Il2CppType CanvasGroup_t4083511760_0_0_0;
static const Il2CppType* GenInst_CanvasGroup_t4083511760_0_0_0_Types[] = { &CanvasGroup_t4083511760_0_0_0 };
extern const Il2CppGenericInst GenInst_CanvasGroup_t4083511760_0_0_0 = { 1, GenInst_CanvasGroup_t4083511760_0_0_0_Types };
extern const Il2CppType Direction_t337909235_0_0_0;
static const Il2CppType* GenInst_Direction_t337909235_0_0_0_Types[] = { &Direction_t337909235_0_0_0 };
extern const Il2CppGenericInst GenInst_Direction_t337909235_0_0_0 = { 1, GenInst_Direction_t337909235_0_0_0_Types };
extern const Il2CppType MatEntry_t2957107092_0_0_0;
static const Il2CppType* GenInst_MatEntry_t2957107092_0_0_0_Types[] = { &MatEntry_t2957107092_0_0_0 };
extern const Il2CppGenericInst GenInst_MatEntry_t2957107092_0_0_0 = { 1, GenInst_MatEntry_t2957107092_0_0_0_Types };
extern const Il2CppType Toggle_t2735377061_0_0_0;
static const Il2CppType* GenInst_Toggle_t2735377061_0_0_0_Types[] = { &Toggle_t2735377061_0_0_0 };
extern const Il2CppGenericInst GenInst_Toggle_t2735377061_0_0_0 = { 1, GenInst_Toggle_t2735377061_0_0_0_Types };
static const Il2CppType* GenInst_Toggle_t2735377061_0_0_0_Boolean_t97287965_0_0_0_Types[] = { &Toggle_t2735377061_0_0_0, &Boolean_t97287965_0_0_0 };
extern const Il2CppGenericInst GenInst_Toggle_t2735377061_0_0_0_Boolean_t97287965_0_0_0 = { 2, GenInst_Toggle_t2735377061_0_0_0_Boolean_t97287965_0_0_0_Types };
static const Il2CppType* GenInst_IClipper_t1224123152_0_0_0_Int32_t2950945753_0_0_0_Types[] = { &IClipper_t1224123152_0_0_0, &Int32_t2950945753_0_0_0 };
extern const Il2CppGenericInst GenInst_IClipper_t1224123152_0_0_0_Int32_t2950945753_0_0_0 = { 2, GenInst_IClipper_t1224123152_0_0_0_Int32_t2950945753_0_0_0_Types };
static const Il2CppType* GenInst_IClipper_t1224123152_0_0_0_Int32_t2950945753_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types[] = { &IClipper_t1224123152_0_0_0, &Int32_t2950945753_0_0_0, &DictionaryEntry_t3123975638_0_0_0 };
extern const Il2CppGenericInst GenInst_IClipper_t1224123152_0_0_0_Int32_t2950945753_0_0_0_DictionaryEntry_t3123975638_0_0_0 = { 3, GenInst_IClipper_t1224123152_0_0_0_Int32_t2950945753_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1634190656_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1634190656_0_0_0_Types[] = { &KeyValuePair_2_t1634190656_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1634190656_0_0_0 = { 1, GenInst_KeyValuePair_2_t1634190656_0_0_0_Types };
static const Il2CppType* GenInst_IClipper_t1224123152_0_0_0_Int32_t2950945753_0_0_0_KeyValuePair_2_t1634190656_0_0_0_Types[] = { &IClipper_t1224123152_0_0_0, &Int32_t2950945753_0_0_0, &KeyValuePair_2_t1634190656_0_0_0 };
extern const Il2CppGenericInst GenInst_IClipper_t1224123152_0_0_0_Int32_t2950945753_0_0_0_KeyValuePair_2_t1634190656_0_0_0 = { 3, GenInst_IClipper_t1224123152_0_0_0_Int32_t2950945753_0_0_0_KeyValuePair_2_t1634190656_0_0_0_Types };
static const Il2CppType* GenInst_IClipper_t1224123152_0_0_0_Int32_t2950945753_0_0_0_Int32_t2950945753_0_0_0_Types[] = { &IClipper_t1224123152_0_0_0, &Int32_t2950945753_0_0_0, &Int32_t2950945753_0_0_0 };
extern const Il2CppGenericInst GenInst_IClipper_t1224123152_0_0_0_Int32_t2950945753_0_0_0_Int32_t2950945753_0_0_0 = { 3, GenInst_IClipper_t1224123152_0_0_0_Int32_t2950945753_0_0_0_Int32_t2950945753_0_0_0_Types };
extern const Il2CppType AspectMode_t3417192999_0_0_0;
static const Il2CppType* GenInst_AspectMode_t3417192999_0_0_0_Types[] = { &AspectMode_t3417192999_0_0_0 };
extern const Il2CppGenericInst GenInst_AspectMode_t3417192999_0_0_0 = { 1, GenInst_AspectMode_t3417192999_0_0_0_Types };
extern const Il2CppType FitMode_t3267881214_0_0_0;
static const Il2CppType* GenInst_FitMode_t3267881214_0_0_0_Types[] = { &FitMode_t3267881214_0_0_0 };
extern const Il2CppGenericInst GenInst_FitMode_t3267881214_0_0_0 = { 1, GenInst_FitMode_t3267881214_0_0_0_Types };
extern const Il2CppType RectTransform_t3704657025_0_0_0;
static const Il2CppType* GenInst_RectTransform_t3704657025_0_0_0_Types[] = { &RectTransform_t3704657025_0_0_0 };
extern const Il2CppGenericInst GenInst_RectTransform_t3704657025_0_0_0 = { 1, GenInst_RectTransform_t3704657025_0_0_0_Types };
extern const Il2CppType LayoutRebuilder_t541313304_0_0_0;
static const Il2CppType* GenInst_LayoutRebuilder_t541313304_0_0_0_Types[] = { &LayoutRebuilder_t541313304_0_0_0 };
extern const Il2CppGenericInst GenInst_LayoutRebuilder_t541313304_0_0_0 = { 1, GenInst_LayoutRebuilder_t541313304_0_0_0_Types };
static const Il2CppType* GenInst_ILayoutElement_t4082016710_0_0_0_Single_t1397266774_0_0_0_Types[] = { &ILayoutElement_t4082016710_0_0_0, &Single_t1397266774_0_0_0 };
extern const Il2CppGenericInst GenInst_ILayoutElement_t4082016710_0_0_0_Single_t1397266774_0_0_0 = { 2, GenInst_ILayoutElement_t4082016710_0_0_0_Single_t1397266774_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_Single_t1397266774_0_0_0_Types[] = { &Il2CppObject_0_0_0, &Single_t1397266774_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_Single_t1397266774_0_0_0 = { 2, GenInst_Il2CppObject_0_0_0_Single_t1397266774_0_0_0_Types };
extern const Il2CppType List_1_t899420910_0_0_0;
static const Il2CppType* GenInst_List_1_t899420910_0_0_0_Types[] = { &List_1_t899420910_0_0_0 };
extern const Il2CppGenericInst GenInst_List_1_t899420910_0_0_0 = { 1, GenInst_List_1_t899420910_0_0_0_Types };
extern const Il2CppType List_1_t4072576034_0_0_0;
static const Il2CppType* GenInst_List_1_t4072576034_0_0_0_Types[] = { &List_1_t4072576034_0_0_0 };
extern const Il2CppGenericInst GenInst_List_1_t4072576034_0_0_0 = { 1, GenInst_List_1_t4072576034_0_0_0_Types };
extern const Il2CppType List_1_t3628304265_0_0_0;
static const Il2CppType* GenInst_List_1_t3628304265_0_0_0_Types[] = { &List_1_t3628304265_0_0_0 };
extern const Il2CppGenericInst GenInst_List_1_t3628304265_0_0_0 = { 1, GenInst_List_1_t3628304265_0_0_0_Types };
extern const Il2CppType List_1_t496136383_0_0_0;
static const Il2CppType* GenInst_List_1_t496136383_0_0_0_Types[] = { &List_1_t496136383_0_0_0 };
extern const Il2CppGenericInst GenInst_List_1_t496136383_0_0_0 = { 1, GenInst_List_1_t496136383_0_0_0_Types };
extern const Il2CppType List_1_t128053199_0_0_0;
static const Il2CppType* GenInst_List_1_t128053199_0_0_0_Types[] = { &List_1_t128053199_0_0_0 };
extern const Il2CppGenericInst GenInst_List_1_t128053199_0_0_0 = { 1, GenInst_List_1_t128053199_0_0_0_Types };
extern const Il2CppType List_1_t1234605051_0_0_0;
static const Il2CppType* GenInst_List_1_t1234605051_0_0_0_Types[] = { &List_1_t1234605051_0_0_0 };
extern const Il2CppGenericInst GenInst_List_1_t1234605051_0_0_0 = { 1, GenInst_List_1_t1234605051_0_0_0_Types };
extern const Il2CppType Action_t1264377477_0_0_0;
static const Il2CppType* GenInst_Action_t1264377477_0_0_0_Types[] = { &Action_t1264377477_0_0_0 };
extern const Il2CppGenericInst GenInst_Action_t1264377477_0_0_0 = { 1, GenInst_Action_t1264377477_0_0_0_Types };
extern const Il2CppType MulticastDelegate_t157516450_0_0_0;
static const Il2CppType* GenInst_MulticastDelegate_t157516450_0_0_0_Types[] = { &MulticastDelegate_t157516450_0_0_0 };
extern const Il2CppGenericInst GenInst_MulticastDelegate_t157516450_0_0_0 = { 1, GenInst_MulticastDelegate_t157516450_0_0_0_Types };
extern const Il2CppType LinkedListNode_1_t1009552580_0_0_0;
static const Il2CppType* GenInst_Action_t1264377477_0_0_0_LinkedListNode_1_t1009552580_0_0_0_Types[] = { &Action_t1264377477_0_0_0, &LinkedListNode_1_t1009552580_0_0_0 };
extern const Il2CppGenericInst GenInst_Action_t1264377477_0_0_0_LinkedListNode_1_t1009552580_0_0_0 = { 2, GenInst_Action_t1264377477_0_0_0_LinkedListNode_1_t1009552580_0_0_0_Types };
static const Il2CppType* GenInst_LinkedListNode_1_t1009552580_0_0_0_Types[] = { &LinkedListNode_1_t1009552580_0_0_0 };
extern const Il2CppGenericInst GenInst_LinkedListNode_1_t1009552580_0_0_0 = { 1, GenInst_LinkedListNode_1_t1009552580_0_0_0_Types };
static const Il2CppType* GenInst_Action_t1264377477_0_0_0_LinkedListNode_1_t1009552580_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types[] = { &Action_t1264377477_0_0_0, &LinkedListNode_1_t1009552580_0_0_0, &DictionaryEntry_t3123975638_0_0_0 };
extern const Il2CppGenericInst GenInst_Action_t1264377477_0_0_0_LinkedListNode_1_t1009552580_0_0_0_DictionaryEntry_t3123975638_0_0_0 = { 3, GenInst_Action_t1264377477_0_0_0_LinkedListNode_1_t1009552580_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2298961218_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2298961218_0_0_0_Types[] = { &KeyValuePair_2_t2298961218_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2298961218_0_0_0 = { 1, GenInst_KeyValuePair_2_t2298961218_0_0_0_Types };
static const Il2CppType* GenInst_Action_t1264377477_0_0_0_LinkedListNode_1_t1009552580_0_0_0_KeyValuePair_2_t2298961218_0_0_0_Types[] = { &Action_t1264377477_0_0_0, &LinkedListNode_1_t1009552580_0_0_0, &KeyValuePair_2_t2298961218_0_0_0 };
extern const Il2CppGenericInst GenInst_Action_t1264377477_0_0_0_LinkedListNode_1_t1009552580_0_0_0_KeyValuePair_2_t2298961218_0_0_0 = { 3, GenInst_Action_t1264377477_0_0_0_LinkedListNode_1_t1009552580_0_0_0_KeyValuePair_2_t2298961218_0_0_0_Types };
static const Il2CppType* GenInst_Action_t1264377477_0_0_0_LinkedListNode_1_t1009552580_0_0_0_LinkedListNode_1_t1009552580_0_0_0_Types[] = { &Action_t1264377477_0_0_0, &LinkedListNode_1_t1009552580_0_0_0, &LinkedListNode_1_t1009552580_0_0_0 };
extern const Il2CppGenericInst GenInst_Action_t1264377477_0_0_0_LinkedListNode_1_t1009552580_0_0_0_LinkedListNode_1_t1009552580_0_0_0 = { 3, GenInst_Action_t1264377477_0_0_0_LinkedListNode_1_t1009552580_0_0_0_LinkedListNode_1_t1009552580_0_0_0_Types };
extern const Il2CppType Action_1_t3252573759_0_0_0;
static const Il2CppType* GenInst_Action_1_t3252573759_0_0_0_Types[] = { &Action_1_t3252573759_0_0_0 };
extern const Il2CppGenericInst GenInst_Action_1_t3252573759_0_0_0 = { 1, GenInst_Action_1_t3252573759_0_0_0_Types };
extern const Il2CppType LinkedListNode_1_t2997748862_0_0_0;
static const Il2CppType* GenInst_Action_1_t3252573759_0_0_0_LinkedListNode_1_t2997748862_0_0_0_Types[] = { &Action_1_t3252573759_0_0_0, &LinkedListNode_1_t2997748862_0_0_0 };
extern const Il2CppGenericInst GenInst_Action_1_t3252573759_0_0_0_LinkedListNode_1_t2997748862_0_0_0 = { 2, GenInst_Action_1_t3252573759_0_0_0_LinkedListNode_1_t2997748862_0_0_0_Types };
static const Il2CppType* GenInst_LinkedListNode_1_t2997748862_0_0_0_Types[] = { &LinkedListNode_1_t2997748862_0_0_0 };
extern const Il2CppGenericInst GenInst_LinkedListNode_1_t2997748862_0_0_0 = { 1, GenInst_LinkedListNode_1_t2997748862_0_0_0_Types };
static const Il2CppType* GenInst_Action_1_t3252573759_0_0_0_LinkedListNode_1_t2997748862_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types[] = { &Action_1_t3252573759_0_0_0, &LinkedListNode_1_t2997748862_0_0_0, &DictionaryEntry_t3123975638_0_0_0 };
extern const Il2CppGenericInst GenInst_Action_1_t3252573759_0_0_0_LinkedListNode_1_t2997748862_0_0_0_DictionaryEntry_t3123975638_0_0_0 = { 3, GenInst_Action_1_t3252573759_0_0_0_LinkedListNode_1_t2997748862_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2444358938_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2444358938_0_0_0_Types[] = { &KeyValuePair_2_t2444358938_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2444358938_0_0_0 = { 1, GenInst_KeyValuePair_2_t2444358938_0_0_0_Types };
static const Il2CppType* GenInst_Action_1_t3252573759_0_0_0_LinkedListNode_1_t2997748862_0_0_0_KeyValuePair_2_t2444358938_0_0_0_Types[] = { &Action_1_t3252573759_0_0_0, &LinkedListNode_1_t2997748862_0_0_0, &KeyValuePair_2_t2444358938_0_0_0 };
extern const Il2CppGenericInst GenInst_Action_1_t3252573759_0_0_0_LinkedListNode_1_t2997748862_0_0_0_KeyValuePair_2_t2444358938_0_0_0 = { 3, GenInst_Action_1_t3252573759_0_0_0_LinkedListNode_1_t2997748862_0_0_0_KeyValuePair_2_t2444358938_0_0_0_Types };
static const Il2CppType* GenInst_Action_1_t3252573759_0_0_0_LinkedListNode_1_t2997748862_0_0_0_LinkedListNode_1_t2997748862_0_0_0_Types[] = { &Action_1_t3252573759_0_0_0, &LinkedListNode_1_t2997748862_0_0_0, &LinkedListNode_1_t2997748862_0_0_0 };
extern const Il2CppGenericInst GenInst_Action_1_t3252573759_0_0_0_LinkedListNode_1_t2997748862_0_0_0_LinkedListNode_1_t2997748862_0_0_0 = { 3, GenInst_Action_1_t3252573759_0_0_0_LinkedListNode_1_t2997748862_0_0_0_LinkedListNode_1_t2997748862_0_0_0_Types };
extern const Il2CppType Action_2_t2470008838_0_0_0;
static const Il2CppType* GenInst_Action_2_t2470008838_0_0_0_Types[] = { &Action_2_t2470008838_0_0_0 };
extern const Il2CppGenericInst GenInst_Action_2_t2470008838_0_0_0 = { 1, GenInst_Action_2_t2470008838_0_0_0_Types };
extern const Il2CppType LinkedListNode_1_t2215183941_0_0_0;
static const Il2CppType* GenInst_Action_2_t2470008838_0_0_0_LinkedListNode_1_t2215183941_0_0_0_Types[] = { &Action_2_t2470008838_0_0_0, &LinkedListNode_1_t2215183941_0_0_0 };
extern const Il2CppGenericInst GenInst_Action_2_t2470008838_0_0_0_LinkedListNode_1_t2215183941_0_0_0 = { 2, GenInst_Action_2_t2470008838_0_0_0_LinkedListNode_1_t2215183941_0_0_0_Types };
static const Il2CppType* GenInst_LinkedListNode_1_t2215183941_0_0_0_Types[] = { &LinkedListNode_1_t2215183941_0_0_0 };
extern const Il2CppGenericInst GenInst_LinkedListNode_1_t2215183941_0_0_0 = { 1, GenInst_LinkedListNode_1_t2215183941_0_0_0_Types };
static const Il2CppType* GenInst_Action_2_t2470008838_0_0_0_LinkedListNode_1_t2215183941_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types[] = { &Action_2_t2470008838_0_0_0, &LinkedListNode_1_t2215183941_0_0_0, &DictionaryEntry_t3123975638_0_0_0 };
extern const Il2CppGenericInst GenInst_Action_2_t2470008838_0_0_0_LinkedListNode_1_t2215183941_0_0_0_DictionaryEntry_t3123975638_0_0_0 = { 3, GenInst_Action_2_t2470008838_0_0_0_LinkedListNode_1_t2215183941_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2472372766_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2472372766_0_0_0_Types[] = { &KeyValuePair_2_t2472372766_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2472372766_0_0_0 = { 1, GenInst_KeyValuePair_2_t2472372766_0_0_0_Types };
static const Il2CppType* GenInst_Action_2_t2470008838_0_0_0_LinkedListNode_1_t2215183941_0_0_0_KeyValuePair_2_t2472372766_0_0_0_Types[] = { &Action_2_t2470008838_0_0_0, &LinkedListNode_1_t2215183941_0_0_0, &KeyValuePair_2_t2472372766_0_0_0 };
extern const Il2CppGenericInst GenInst_Action_2_t2470008838_0_0_0_LinkedListNode_1_t2215183941_0_0_0_KeyValuePair_2_t2472372766_0_0_0 = { 3, GenInst_Action_2_t2470008838_0_0_0_LinkedListNode_1_t2215183941_0_0_0_KeyValuePair_2_t2472372766_0_0_0_Types };
static const Il2CppType* GenInst_Action_2_t2470008838_0_0_0_LinkedListNode_1_t2215183941_0_0_0_LinkedListNode_1_t2215183941_0_0_0_Types[] = { &Action_2_t2470008838_0_0_0, &LinkedListNode_1_t2215183941_0_0_0, &LinkedListNode_1_t2215183941_0_0_0 };
extern const Il2CppGenericInst GenInst_Action_2_t2470008838_0_0_0_LinkedListNode_1_t2215183941_0_0_0_LinkedListNode_1_t2215183941_0_0_0 = { 3, GenInst_Action_2_t2470008838_0_0_0_LinkedListNode_1_t2215183941_0_0_0_LinkedListNode_1_t2215183941_0_0_0_Types };
extern const Il2CppType Action_3_t3632554945_0_0_0;
static const Il2CppType* GenInst_Action_3_t3632554945_0_0_0_Types[] = { &Action_3_t3632554945_0_0_0 };
extern const Il2CppGenericInst GenInst_Action_3_t3632554945_0_0_0 = { 1, GenInst_Action_3_t3632554945_0_0_0_Types };
extern const Il2CppType LinkedListNode_1_t3377730048_0_0_0;
static const Il2CppType* GenInst_Action_3_t3632554945_0_0_0_LinkedListNode_1_t3377730048_0_0_0_Types[] = { &Action_3_t3632554945_0_0_0, &LinkedListNode_1_t3377730048_0_0_0 };
extern const Il2CppGenericInst GenInst_Action_3_t3632554945_0_0_0_LinkedListNode_1_t3377730048_0_0_0 = { 2, GenInst_Action_3_t3632554945_0_0_0_LinkedListNode_1_t3377730048_0_0_0_Types };
static const Il2CppType* GenInst_LinkedListNode_1_t3377730048_0_0_0_Types[] = { &LinkedListNode_1_t3377730048_0_0_0 };
extern const Il2CppGenericInst GenInst_LinkedListNode_1_t3377730048_0_0_0 = { 1, GenInst_LinkedListNode_1_t3377730048_0_0_0_Types };
static const Il2CppType* GenInst_Action_3_t3632554945_0_0_0_LinkedListNode_1_t3377730048_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types[] = { &Action_3_t3632554945_0_0_0, &LinkedListNode_1_t3377730048_0_0_0, &DictionaryEntry_t3123975638_0_0_0 };
extern const Il2CppGenericInst GenInst_Action_3_t3632554945_0_0_0_LinkedListNode_1_t3377730048_0_0_0_DictionaryEntry_t3123975638_0_0_0 = { 3, GenInst_Action_3_t3632554945_0_0_0_LinkedListNode_1_t3377730048_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2246431954_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2246431954_0_0_0_Types[] = { &KeyValuePair_2_t2246431954_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2246431954_0_0_0 = { 1, GenInst_KeyValuePair_2_t2246431954_0_0_0_Types };
static const Il2CppType* GenInst_Action_3_t3632554945_0_0_0_LinkedListNode_1_t3377730048_0_0_0_KeyValuePair_2_t2246431954_0_0_0_Types[] = { &Action_3_t3632554945_0_0_0, &LinkedListNode_1_t3377730048_0_0_0, &KeyValuePair_2_t2246431954_0_0_0 };
extern const Il2CppGenericInst GenInst_Action_3_t3632554945_0_0_0_LinkedListNode_1_t3377730048_0_0_0_KeyValuePair_2_t2246431954_0_0_0 = { 3, GenInst_Action_3_t3632554945_0_0_0_LinkedListNode_1_t3377730048_0_0_0_KeyValuePair_2_t2246431954_0_0_0_Types };
static const Il2CppType* GenInst_Action_3_t3632554945_0_0_0_LinkedListNode_1_t3377730048_0_0_0_LinkedListNode_1_t3377730048_0_0_0_Types[] = { &Action_3_t3632554945_0_0_0, &LinkedListNode_1_t3377730048_0_0_0, &LinkedListNode_1_t3377730048_0_0_0 };
extern const Il2CppGenericInst GenInst_Action_3_t3632554945_0_0_0_LinkedListNode_1_t3377730048_0_0_0_LinkedListNode_1_t3377730048_0_0_0 = { 3, GenInst_Action_3_t3632554945_0_0_0_LinkedListNode_1_t3377730048_0_0_0_LinkedListNode_1_t3377730048_0_0_0_Types };
extern const Il2CppType TMP_Sprite_t554067146_0_0_0;
static const Il2CppType* GenInst_TMP_Sprite_t554067146_0_0_0_Types[] = { &TMP_Sprite_t554067146_0_0_0 };
extern const Il2CppGenericInst GenInst_TMP_Sprite_t554067146_0_0_0 = { 1, GenInst_TMP_Sprite_t554067146_0_0_0_Types };
extern const Il2CppType TMP_TextElement_t129727469_0_0_0;
static const Il2CppType* GenInst_TMP_TextElement_t129727469_0_0_0_Types[] = { &TMP_TextElement_t129727469_0_0_0 };
extern const Il2CppGenericInst GenInst_TMP_TextElement_t129727469_0_0_0 = { 1, GenInst_TMP_TextElement_t129727469_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2950945753_0_0_0_Material_t340375123_0_0_0_Types[] = { &Int32_t2950945753_0_0_0, &Material_t340375123_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2950945753_0_0_0_Material_t340375123_0_0_0 = { 2, GenInst_Int32_t2950945753_0_0_0_Material_t340375123_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2950945753_0_0_0_Material_t340375123_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types[] = { &Int32_t2950945753_0_0_0, &Material_t340375123_0_0_0, &DictionaryEntry_t3123975638_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2950945753_0_0_0_Material_t340375123_0_0_0_DictionaryEntry_t3123975638_0_0_0 = { 3, GenInst_Int32_t2950945753_0_0_0_Material_t340375123_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1626760621_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1626760621_0_0_0_Types[] = { &KeyValuePair_2_t1626760621_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1626760621_0_0_0 = { 1, GenInst_KeyValuePair_2_t1626760621_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2950945753_0_0_0_Material_t340375123_0_0_0_KeyValuePair_2_t1626760621_0_0_0_Types[] = { &Int32_t2950945753_0_0_0, &Material_t340375123_0_0_0, &KeyValuePair_2_t1626760621_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2950945753_0_0_0_Material_t340375123_0_0_0_KeyValuePair_2_t1626760621_0_0_0 = { 3, GenInst_Int32_t2950945753_0_0_0_Material_t340375123_0_0_0_KeyValuePair_2_t1626760621_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2950945753_0_0_0_Material_t340375123_0_0_0_Material_t340375123_0_0_0_Types[] = { &Int32_t2950945753_0_0_0, &Material_t340375123_0_0_0, &Material_t340375123_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2950945753_0_0_0_Material_t340375123_0_0_0_Material_t340375123_0_0_0 = { 3, GenInst_Int32_t2950945753_0_0_0_Material_t340375123_0_0_0_Material_t340375123_0_0_0_Types };
extern const Il2CppType TMP_FontAsset_t364381626_0_0_0;
static const Il2CppType* GenInst_Int32_t2950945753_0_0_0_TMP_FontAsset_t364381626_0_0_0_Types[] = { &Int32_t2950945753_0_0_0, &TMP_FontAsset_t364381626_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2950945753_0_0_0_TMP_FontAsset_t364381626_0_0_0 = { 2, GenInst_Int32_t2950945753_0_0_0_TMP_FontAsset_t364381626_0_0_0_Types };
static const Il2CppType* GenInst_TMP_FontAsset_t364381626_0_0_0_Types[] = { &TMP_FontAsset_t364381626_0_0_0 };
extern const Il2CppGenericInst GenInst_TMP_FontAsset_t364381626_0_0_0 = { 1, GenInst_TMP_FontAsset_t364381626_0_0_0_Types };
extern const Il2CppType TMP_Asset_t2469957285_0_0_0;
static const Il2CppType* GenInst_TMP_Asset_t2469957285_0_0_0_Types[] = { &TMP_Asset_t2469957285_0_0_0 };
extern const Il2CppGenericInst GenInst_TMP_Asset_t2469957285_0_0_0 = { 1, GenInst_TMP_Asset_t2469957285_0_0_0_Types };
extern const Il2CppType ScriptableObject_t2528358522_0_0_0;
static const Il2CppType* GenInst_ScriptableObject_t2528358522_0_0_0_Types[] = { &ScriptableObject_t2528358522_0_0_0 };
extern const Il2CppGenericInst GenInst_ScriptableObject_t2528358522_0_0_0 = { 1, GenInst_ScriptableObject_t2528358522_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2950945753_0_0_0_TMP_FontAsset_t364381626_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types[] = { &Int32_t2950945753_0_0_0, &TMP_FontAsset_t364381626_0_0_0, &DictionaryEntry_t3123975638_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2950945753_0_0_0_TMP_FontAsset_t364381626_0_0_0_DictionaryEntry_t3123975638_0_0_0 = { 3, GenInst_Int32_t2950945753_0_0_0_TMP_FontAsset_t364381626_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1650767124_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1650767124_0_0_0_Types[] = { &KeyValuePair_2_t1650767124_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1650767124_0_0_0 = { 1, GenInst_KeyValuePair_2_t1650767124_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2950945753_0_0_0_TMP_FontAsset_t364381626_0_0_0_KeyValuePair_2_t1650767124_0_0_0_Types[] = { &Int32_t2950945753_0_0_0, &TMP_FontAsset_t364381626_0_0_0, &KeyValuePair_2_t1650767124_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2950945753_0_0_0_TMP_FontAsset_t364381626_0_0_0_KeyValuePair_2_t1650767124_0_0_0 = { 3, GenInst_Int32_t2950945753_0_0_0_TMP_FontAsset_t364381626_0_0_0_KeyValuePair_2_t1650767124_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2950945753_0_0_0_TMP_FontAsset_t364381626_0_0_0_TMP_FontAsset_t364381626_0_0_0_Types[] = { &Int32_t2950945753_0_0_0, &TMP_FontAsset_t364381626_0_0_0, &TMP_FontAsset_t364381626_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2950945753_0_0_0_TMP_FontAsset_t364381626_0_0_0_TMP_FontAsset_t364381626_0_0_0 = { 3, GenInst_Int32_t2950945753_0_0_0_TMP_FontAsset_t364381626_0_0_0_TMP_FontAsset_t364381626_0_0_0_Types };
extern const Il2CppType TMP_SpriteAsset_t484820633_0_0_0;
static const Il2CppType* GenInst_Int32_t2950945753_0_0_0_TMP_SpriteAsset_t484820633_0_0_0_Types[] = { &Int32_t2950945753_0_0_0, &TMP_SpriteAsset_t484820633_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2950945753_0_0_0_TMP_SpriteAsset_t484820633_0_0_0 = { 2, GenInst_Int32_t2950945753_0_0_0_TMP_SpriteAsset_t484820633_0_0_0_Types };
static const Il2CppType* GenInst_TMP_SpriteAsset_t484820633_0_0_0_Types[] = { &TMP_SpriteAsset_t484820633_0_0_0 };
extern const Il2CppGenericInst GenInst_TMP_SpriteAsset_t484820633_0_0_0 = { 1, GenInst_TMP_SpriteAsset_t484820633_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2950945753_0_0_0_TMP_SpriteAsset_t484820633_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types[] = { &Int32_t2950945753_0_0_0, &TMP_SpriteAsset_t484820633_0_0_0, &DictionaryEntry_t3123975638_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2950945753_0_0_0_TMP_SpriteAsset_t484820633_0_0_0_DictionaryEntry_t3123975638_0_0_0 = { 3, GenInst_Int32_t2950945753_0_0_0_TMP_SpriteAsset_t484820633_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1771206131_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1771206131_0_0_0_Types[] = { &KeyValuePair_2_t1771206131_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1771206131_0_0_0 = { 1, GenInst_KeyValuePair_2_t1771206131_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2950945753_0_0_0_TMP_SpriteAsset_t484820633_0_0_0_KeyValuePair_2_t1771206131_0_0_0_Types[] = { &Int32_t2950945753_0_0_0, &TMP_SpriteAsset_t484820633_0_0_0, &KeyValuePair_2_t1771206131_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2950945753_0_0_0_TMP_SpriteAsset_t484820633_0_0_0_KeyValuePair_2_t1771206131_0_0_0 = { 3, GenInst_Int32_t2950945753_0_0_0_TMP_SpriteAsset_t484820633_0_0_0_KeyValuePair_2_t1771206131_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2950945753_0_0_0_TMP_SpriteAsset_t484820633_0_0_0_TMP_SpriteAsset_t484820633_0_0_0_Types[] = { &Int32_t2950945753_0_0_0, &TMP_SpriteAsset_t484820633_0_0_0, &TMP_SpriteAsset_t484820633_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2950945753_0_0_0_TMP_SpriteAsset_t484820633_0_0_0_TMP_SpriteAsset_t484820633_0_0_0 = { 3, GenInst_Int32_t2950945753_0_0_0_TMP_SpriteAsset_t484820633_0_0_0_TMP_SpriteAsset_t484820633_0_0_0_Types };
extern const Il2CppType MaterialReference_t1952344632_0_0_0;
static const Il2CppType* GenInst_MaterialReference_t1952344632_0_0_0_Types[] = { &MaterialReference_t1952344632_0_0_0 };
extern const Il2CppGenericInst GenInst_MaterialReference_t1952344632_0_0_0 = { 1, GenInst_MaterialReference_t1952344632_0_0_0_Types };
extern const Il2CppType OptionData_t1114640268_0_0_0;
static const Il2CppType* GenInst_OptionData_t1114640268_0_0_0_Types[] = { &OptionData_t1114640268_0_0_0 };
extern const Il2CppGenericInst GenInst_OptionData_t1114640268_0_0_0 = { 1, GenInst_OptionData_t1114640268_0_0_0_Types };
extern const Il2CppType DropdownItem_t1842596711_0_0_0;
static const Il2CppType* GenInst_DropdownItem_t1842596711_0_0_0_Types[] = { &DropdownItem_t1842596711_0_0_0 };
extern const Il2CppGenericInst GenInst_DropdownItem_t1842596711_0_0_0 = { 1, GenInst_DropdownItem_t1842596711_0_0_0_Types };
extern const Il2CppType FloatTween_t3783157226_0_0_0;
static const Il2CppType* GenInst_FloatTween_t3783157226_0_0_0_Types[] = { &FloatTween_t3783157226_0_0_0 };
extern const Il2CppGenericInst GenInst_FloatTween_t3783157226_0_0_0 = { 1, GenInst_FloatTween_t3783157226_0_0_0_Types };
extern const Il2CppType TMP_Glyph_t581847833_0_0_0;
static const Il2CppType* GenInst_Int32_t2950945753_0_0_0_TMP_Glyph_t581847833_0_0_0_Types[] = { &Int32_t2950945753_0_0_0, &TMP_Glyph_t581847833_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2950945753_0_0_0_TMP_Glyph_t581847833_0_0_0 = { 2, GenInst_Int32_t2950945753_0_0_0_TMP_Glyph_t581847833_0_0_0_Types };
static const Il2CppType* GenInst_TMP_Glyph_t581847833_0_0_0_Types[] = { &TMP_Glyph_t581847833_0_0_0 };
extern const Il2CppGenericInst GenInst_TMP_Glyph_t581847833_0_0_0 = { 1, GenInst_TMP_Glyph_t581847833_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2950945753_0_0_0_TMP_Glyph_t581847833_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types[] = { &Int32_t2950945753_0_0_0, &TMP_Glyph_t581847833_0_0_0, &DictionaryEntry_t3123975638_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2950945753_0_0_0_TMP_Glyph_t581847833_0_0_0_DictionaryEntry_t3123975638_0_0_0 = { 3, GenInst_Int32_t2950945753_0_0_0_TMP_Glyph_t581847833_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1868233331_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1868233331_0_0_0_Types[] = { &KeyValuePair_2_t1868233331_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1868233331_0_0_0 = { 1, GenInst_KeyValuePair_2_t1868233331_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2950945753_0_0_0_TMP_Glyph_t581847833_0_0_0_KeyValuePair_2_t1868233331_0_0_0_Types[] = { &Int32_t2950945753_0_0_0, &TMP_Glyph_t581847833_0_0_0, &KeyValuePair_2_t1868233331_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2950945753_0_0_0_TMP_Glyph_t581847833_0_0_0_KeyValuePair_2_t1868233331_0_0_0 = { 3, GenInst_Int32_t2950945753_0_0_0_TMP_Glyph_t581847833_0_0_0_KeyValuePair_2_t1868233331_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2950945753_0_0_0_TMP_Glyph_t581847833_0_0_0_TMP_Glyph_t581847833_0_0_0_Types[] = { &Int32_t2950945753_0_0_0, &TMP_Glyph_t581847833_0_0_0, &TMP_Glyph_t581847833_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2950945753_0_0_0_TMP_Glyph_t581847833_0_0_0_TMP_Glyph_t581847833_0_0_0 = { 3, GenInst_Int32_t2950945753_0_0_0_TMP_Glyph_t581847833_0_0_0_TMP_Glyph_t581847833_0_0_0_Types };
extern const Il2CppType KerningPair_t2270855589_0_0_0;
static const Il2CppType* GenInst_Int32_t2950945753_0_0_0_KerningPair_t2270855589_0_0_0_Types[] = { &Int32_t2950945753_0_0_0, &KerningPair_t2270855589_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2950945753_0_0_0_KerningPair_t2270855589_0_0_0 = { 2, GenInst_Int32_t2950945753_0_0_0_KerningPair_t2270855589_0_0_0_Types };
static const Il2CppType* GenInst_KerningPair_t2270855589_0_0_0_Types[] = { &KerningPair_t2270855589_0_0_0 };
extern const Il2CppGenericInst GenInst_KerningPair_t2270855589_0_0_0 = { 1, GenInst_KerningPair_t2270855589_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2950945753_0_0_0_KerningPair_t2270855589_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types[] = { &Int32_t2950945753_0_0_0, &KerningPair_t2270855589_0_0_0, &DictionaryEntry_t3123975638_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2950945753_0_0_0_KerningPair_t2270855589_0_0_0_DictionaryEntry_t3123975638_0_0_0 = { 3, GenInst_Int32_t2950945753_0_0_0_KerningPair_t2270855589_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t3557241087_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t3557241087_0_0_0_Types[] = { &KeyValuePair_2_t3557241087_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3557241087_0_0_0 = { 1, GenInst_KeyValuePair_2_t3557241087_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2950945753_0_0_0_KerningPair_t2270855589_0_0_0_KeyValuePair_2_t3557241087_0_0_0_Types[] = { &Int32_t2950945753_0_0_0, &KerningPair_t2270855589_0_0_0, &KeyValuePair_2_t3557241087_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2950945753_0_0_0_KerningPair_t2270855589_0_0_0_KeyValuePair_2_t3557241087_0_0_0 = { 3, GenInst_Int32_t2950945753_0_0_0_KerningPair_t2270855589_0_0_0_KeyValuePair_2_t3557241087_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2950945753_0_0_0_KerningPair_t2270855589_0_0_0_KerningPair_t2270855589_0_0_0_Types[] = { &Int32_t2950945753_0_0_0, &KerningPair_t2270855589_0_0_0, &KerningPair_t2270855589_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2950945753_0_0_0_KerningPair_t2270855589_0_0_0_KerningPair_t2270855589_0_0_0 = { 3, GenInst_Int32_t2950945753_0_0_0_KerningPair_t2270855589_0_0_0_KerningPair_t2270855589_0_0_0_Types };
extern const Il2CppType TMP_FontWeights_t916301067_0_0_0;
static const Il2CppType* GenInst_TMP_FontWeights_t916301067_0_0_0_Types[] = { &TMP_FontWeights_t916301067_0_0_0 };
extern const Il2CppGenericInst GenInst_TMP_FontWeights_t916301067_0_0_0 = { 1, GenInst_TMP_FontWeights_t916301067_0_0_0_Types };
static const Il2CppType* GenInst_TMP_Glyph_t581847833_0_0_0_Int32_t2950945753_0_0_0_Types[] = { &TMP_Glyph_t581847833_0_0_0, &Int32_t2950945753_0_0_0 };
extern const Il2CppGenericInst GenInst_TMP_Glyph_t581847833_0_0_0_Int32_t2950945753_0_0_0 = { 2, GenInst_TMP_Glyph_t581847833_0_0_0_Int32_t2950945753_0_0_0_Types };
extern const Il2CppType ContentType_t1128941285_0_0_0;
static const Il2CppType* GenInst_ContentType_t1128941285_0_0_0_Types[] = { &ContentType_t1128941285_0_0_0 };
extern const Il2CppGenericInst GenInst_ContentType_t1128941285_0_0_0 = { 1, GenInst_ContentType_t1128941285_0_0_0_Types };
extern const Il2CppType Action_1_t803475548_0_0_0;
static const Il2CppType* GenInst_Action_1_t803475548_0_0_0_Types[] = { &Action_1_t803475548_0_0_0 };
extern const Il2CppGenericInst GenInst_Action_1_t803475548_0_0_0 = { 1, GenInst_Action_1_t803475548_0_0_0_Types };
extern const Il2CppType LinkedListNode_1_t548650651_0_0_0;
static const Il2CppType* GenInst_Action_1_t803475548_0_0_0_LinkedListNode_1_t548650651_0_0_0_Types[] = { &Action_1_t803475548_0_0_0, &LinkedListNode_1_t548650651_0_0_0 };
extern const Il2CppGenericInst GenInst_Action_1_t803475548_0_0_0_LinkedListNode_1_t548650651_0_0_0 = { 2, GenInst_Action_1_t803475548_0_0_0_LinkedListNode_1_t548650651_0_0_0_Types };
static const Il2CppType* GenInst_LinkedListNode_1_t548650651_0_0_0_Types[] = { &LinkedListNode_1_t548650651_0_0_0 };
extern const Il2CppGenericInst GenInst_LinkedListNode_1_t548650651_0_0_0 = { 1, GenInst_LinkedListNode_1_t548650651_0_0_0_Types };
static const Il2CppType* GenInst_Action_1_t803475548_0_0_0_LinkedListNode_1_t548650651_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types[] = { &Action_1_t803475548_0_0_0, &LinkedListNode_1_t548650651_0_0_0, &DictionaryEntry_t3123975638_0_0_0 };
extern const Il2CppGenericInst GenInst_Action_1_t803475548_0_0_0_LinkedListNode_1_t548650651_0_0_0_DictionaryEntry_t3123975638_0_0_0 = { 3, GenInst_Action_1_t803475548_0_0_0_LinkedListNode_1_t548650651_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2864070662_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2864070662_0_0_0_Types[] = { &KeyValuePair_2_t2864070662_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2864070662_0_0_0 = { 1, GenInst_KeyValuePair_2_t2864070662_0_0_0_Types };
static const Il2CppType* GenInst_Action_1_t803475548_0_0_0_LinkedListNode_1_t548650651_0_0_0_KeyValuePair_2_t2864070662_0_0_0_Types[] = { &Action_1_t803475548_0_0_0, &LinkedListNode_1_t548650651_0_0_0, &KeyValuePair_2_t2864070662_0_0_0 };
extern const Il2CppGenericInst GenInst_Action_1_t803475548_0_0_0_LinkedListNode_1_t548650651_0_0_0_KeyValuePair_2_t2864070662_0_0_0 = { 3, GenInst_Action_1_t803475548_0_0_0_LinkedListNode_1_t548650651_0_0_0_KeyValuePair_2_t2864070662_0_0_0_Types };
static const Il2CppType* GenInst_Action_1_t803475548_0_0_0_LinkedListNode_1_t548650651_0_0_0_LinkedListNode_1_t548650651_0_0_0_Types[] = { &Action_1_t803475548_0_0_0, &LinkedListNode_1_t548650651_0_0_0, &LinkedListNode_1_t548650651_0_0_0 };
extern const Il2CppGenericInst GenInst_Action_1_t803475548_0_0_0_LinkedListNode_1_t548650651_0_0_0_LinkedListNode_1_t548650651_0_0_0 = { 3, GenInst_Action_1_t803475548_0_0_0_LinkedListNode_1_t548650651_0_0_0_LinkedListNode_1_t548650651_0_0_0_Types };
extern const Il2CppType TMP_CharacterInfo_t3185626797_0_0_0;
static const Il2CppType* GenInst_TMP_CharacterInfo_t3185626797_0_0_0_Types[] = { &TMP_CharacterInfo_t3185626797_0_0_0 };
extern const Il2CppGenericInst GenInst_TMP_CharacterInfo_t3185626797_0_0_0 = { 1, GenInst_TMP_CharacterInfo_t3185626797_0_0_0_Types };
extern const Il2CppType TMP_LineInfo_t1079631636_0_0_0;
static const Il2CppType* GenInst_TMP_LineInfo_t1079631636_0_0_0_Types[] = { &TMP_LineInfo_t1079631636_0_0_0 };
extern const Il2CppGenericInst GenInst_TMP_LineInfo_t1079631636_0_0_0 = { 1, GenInst_TMP_LineInfo_t1079631636_0_0_0_Types };
extern const Il2CppType TMP_WordInfo_t3331066303_0_0_0;
static const Il2CppType* GenInst_TMP_WordInfo_t3331066303_0_0_0_Types[] = { &TMP_WordInfo_t3331066303_0_0_0 };
extern const Il2CppGenericInst GenInst_TMP_WordInfo_t3331066303_0_0_0 = { 1, GenInst_TMP_WordInfo_t3331066303_0_0_0_Types };
extern const Il2CppType MaskingMaterial_t16896275_0_0_0;
static const Il2CppType* GenInst_MaskingMaterial_t16896275_0_0_0_Types[] = { &MaskingMaterial_t16896275_0_0_0 };
extern const Il2CppGenericInst GenInst_MaskingMaterial_t16896275_0_0_0 = { 1, GenInst_MaskingMaterial_t16896275_0_0_0_Types };
extern const Il2CppType FallbackMaterial_t4171378819_0_0_0;
static const Il2CppType* GenInst_Int64_t3736567304_0_0_0_FallbackMaterial_t4171378819_0_0_0_Types[] = { &Int64_t3736567304_0_0_0, &FallbackMaterial_t4171378819_0_0_0 };
extern const Il2CppGenericInst GenInst_Int64_t3736567304_0_0_0_FallbackMaterial_t4171378819_0_0_0 = { 2, GenInst_Int64_t3736567304_0_0_0_FallbackMaterial_t4171378819_0_0_0_Types };
static const Il2CppType* GenInst_Int64_t3736567304_0_0_0_Il2CppObject_0_0_0_Types[] = { &Int64_t3736567304_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_Int64_t3736567304_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_Int64_t3736567304_0_0_0_Il2CppObject_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2245450819_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2245450819_0_0_0_Types[] = { &KeyValuePair_2_t2245450819_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2245450819_0_0_0 = { 1, GenInst_KeyValuePair_2_t2245450819_0_0_0_Types };
static const Il2CppType* GenInst_Int64_t3736567304_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Types[] = { &Int64_t3736567304_0_0_0, &Il2CppObject_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_Int64_t3736567304_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0 = { 3, GenInst_Int64_t3736567304_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_Int64_t3736567304_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types[] = { &Int64_t3736567304_0_0_0, &Il2CppObject_0_0_0, &DictionaryEntry_t3123975638_0_0_0 };
extern const Il2CppGenericInst GenInst_Int64_t3736567304_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t3123975638_0_0_0 = { 3, GenInst_Int64_t3736567304_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types };
static const Il2CppType* GenInst_Int64_t3736567304_0_0_0_Il2CppObject_0_0_0_KeyValuePair_2_t2245450819_0_0_0_Types[] = { &Int64_t3736567304_0_0_0, &Il2CppObject_0_0_0, &KeyValuePair_2_t2245450819_0_0_0 };
extern const Il2CppGenericInst GenInst_Int64_t3736567304_0_0_0_Il2CppObject_0_0_0_KeyValuePair_2_t2245450819_0_0_0 = { 3, GenInst_Int64_t3736567304_0_0_0_Il2CppObject_0_0_0_KeyValuePair_2_t2245450819_0_0_0_Types };
static const Il2CppType* GenInst_FallbackMaterial_t4171378819_0_0_0_Types[] = { &FallbackMaterial_t4171378819_0_0_0 };
extern const Il2CppGenericInst GenInst_FallbackMaterial_t4171378819_0_0_0 = { 1, GenInst_FallbackMaterial_t4171378819_0_0_0_Types };
static const Il2CppType* GenInst_Int64_t3736567304_0_0_0_FallbackMaterial_t4171378819_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types[] = { &Int64_t3736567304_0_0_0, &FallbackMaterial_t4171378819_0_0_0, &DictionaryEntry_t3123975638_0_0_0 };
extern const Il2CppGenericInst GenInst_Int64_t3736567304_0_0_0_FallbackMaterial_t4171378819_0_0_0_DictionaryEntry_t3123975638_0_0_0 = { 3, GenInst_Int64_t3736567304_0_0_0_FallbackMaterial_t4171378819_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t3336723474_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t3336723474_0_0_0_Types[] = { &KeyValuePair_2_t3336723474_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3336723474_0_0_0 = { 1, GenInst_KeyValuePair_2_t3336723474_0_0_0_Types };
static const Il2CppType* GenInst_Int64_t3736567304_0_0_0_FallbackMaterial_t4171378819_0_0_0_KeyValuePair_2_t3336723474_0_0_0_Types[] = { &Int64_t3736567304_0_0_0, &FallbackMaterial_t4171378819_0_0_0, &KeyValuePair_2_t3336723474_0_0_0 };
extern const Il2CppGenericInst GenInst_Int64_t3736567304_0_0_0_FallbackMaterial_t4171378819_0_0_0_KeyValuePair_2_t3336723474_0_0_0 = { 3, GenInst_Int64_t3736567304_0_0_0_FallbackMaterial_t4171378819_0_0_0_KeyValuePair_2_t3336723474_0_0_0_Types };
static const Il2CppType* GenInst_Int64_t3736567304_0_0_0_FallbackMaterial_t4171378819_0_0_0_FallbackMaterial_t4171378819_0_0_0_Types[] = { &Int64_t3736567304_0_0_0, &FallbackMaterial_t4171378819_0_0_0, &FallbackMaterial_t4171378819_0_0_0 };
extern const Il2CppGenericInst GenInst_Int64_t3736567304_0_0_0_FallbackMaterial_t4171378819_0_0_0_FallbackMaterial_t4171378819_0_0_0 = { 3, GenInst_Int64_t3736567304_0_0_0_FallbackMaterial_t4171378819_0_0_0_FallbackMaterial_t4171378819_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2950945753_0_0_0_Int64_t3736567304_0_0_0_Types[] = { &Int32_t2950945753_0_0_0, &Int64_t3736567304_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2950945753_0_0_0_Int64_t3736567304_0_0_0 = { 2, GenInst_Int32_t2950945753_0_0_0_Int64_t3736567304_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t727985506_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t727985506_0_0_0_Types[] = { &KeyValuePair_2_t727985506_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t727985506_0_0_0 = { 1, GenInst_KeyValuePair_2_t727985506_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2950945753_0_0_0_Int64_t3736567304_0_0_0_Int64_t3736567304_0_0_0_Types[] = { &Int32_t2950945753_0_0_0, &Int64_t3736567304_0_0_0, &Int64_t3736567304_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2950945753_0_0_0_Int64_t3736567304_0_0_0_Int64_t3736567304_0_0_0 = { 3, GenInst_Int32_t2950945753_0_0_0_Int64_t3736567304_0_0_0_Int64_t3736567304_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2950945753_0_0_0_Int64_t3736567304_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types[] = { &Int32_t2950945753_0_0_0, &Int64_t3736567304_0_0_0, &DictionaryEntry_t3123975638_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2950945753_0_0_0_Int64_t3736567304_0_0_0_DictionaryEntry_t3123975638_0_0_0 = { 3, GenInst_Int32_t2950945753_0_0_0_Int64_t3736567304_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2950945753_0_0_0_Int64_t3736567304_0_0_0_KeyValuePair_2_t727985506_0_0_0_Types[] = { &Int32_t2950945753_0_0_0, &Int64_t3736567304_0_0_0, &KeyValuePair_2_t727985506_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2950945753_0_0_0_Int64_t3736567304_0_0_0_KeyValuePair_2_t727985506_0_0_0 = { 3, GenInst_Int32_t2950945753_0_0_0_Int64_t3736567304_0_0_0_KeyValuePair_2_t727985506_0_0_0_Types };
extern const Il2CppType List_1_t3447100432_0_0_0;
static const Il2CppType* GenInst_List_1_t3447100432_0_0_0_Types[] = { &List_1_t3447100432_0_0_0 };
extern const Il2CppGenericInst GenInst_List_1_t3447100432_0_0_0 = { 1, GenInst_List_1_t3447100432_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2950945753_0_0_0_Char_t3634460470_0_0_0_Types[] = { &Int32_t2950945753_0_0_0, &Char_t3634460470_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2950945753_0_0_0_Char_t3634460470_0_0_0 = { 2, GenInst_Int32_t2950945753_0_0_0_Char_t3634460470_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t625878672_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t625878672_0_0_0_Types[] = { &KeyValuePair_2_t625878672_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t625878672_0_0_0 = { 1, GenInst_KeyValuePair_2_t625878672_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2950945753_0_0_0_Char_t3634460470_0_0_0_Char_t3634460470_0_0_0_Types[] = { &Int32_t2950945753_0_0_0, &Char_t3634460470_0_0_0, &Char_t3634460470_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2950945753_0_0_0_Char_t3634460470_0_0_0_Char_t3634460470_0_0_0 = { 3, GenInst_Int32_t2950945753_0_0_0_Char_t3634460470_0_0_0_Char_t3634460470_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2950945753_0_0_0_Char_t3634460470_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types[] = { &Int32_t2950945753_0_0_0, &Char_t3634460470_0_0_0, &DictionaryEntry_t3123975638_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2950945753_0_0_0_Char_t3634460470_0_0_0_DictionaryEntry_t3123975638_0_0_0 = { 3, GenInst_Int32_t2950945753_0_0_0_Char_t3634460470_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2950945753_0_0_0_Char_t3634460470_0_0_0_KeyValuePair_2_t625878672_0_0_0_Types[] = { &Int32_t2950945753_0_0_0, &Char_t3634460470_0_0_0, &KeyValuePair_2_t625878672_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2950945753_0_0_0_Char_t3634460470_0_0_0_KeyValuePair_2_t625878672_0_0_0 = { 3, GenInst_Int32_t2950945753_0_0_0_Char_t3634460470_0_0_0_KeyValuePair_2_t625878672_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2950945753_0_0_0_Boolean_t97287965_0_0_0_Types[] = { &Int32_t2950945753_0_0_0, &Boolean_t97287965_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2950945753_0_0_0_Boolean_t97287965_0_0_0 = { 2, GenInst_Int32_t2950945753_0_0_0_Boolean_t97287965_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1383673463_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1383673463_0_0_0_Types[] = { &KeyValuePair_2_t1383673463_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1383673463_0_0_0 = { 1, GenInst_KeyValuePair_2_t1383673463_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2950945753_0_0_0_Boolean_t97287965_0_0_0_Boolean_t97287965_0_0_0_Types[] = { &Int32_t2950945753_0_0_0, &Boolean_t97287965_0_0_0, &Boolean_t97287965_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2950945753_0_0_0_Boolean_t97287965_0_0_0_Boolean_t97287965_0_0_0 = { 3, GenInst_Int32_t2950945753_0_0_0_Boolean_t97287965_0_0_0_Boolean_t97287965_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2950945753_0_0_0_Boolean_t97287965_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types[] = { &Int32_t2950945753_0_0_0, &Boolean_t97287965_0_0_0, &DictionaryEntry_t3123975638_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2950945753_0_0_0_Boolean_t97287965_0_0_0_DictionaryEntry_t3123975638_0_0_0 = { 3, GenInst_Int32_t2950945753_0_0_0_Boolean_t97287965_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2950945753_0_0_0_Boolean_t97287965_0_0_0_KeyValuePair_2_t1383673463_0_0_0_Types[] = { &Int32_t2950945753_0_0_0, &Boolean_t97287965_0_0_0, &KeyValuePair_2_t1383673463_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2950945753_0_0_0_Boolean_t97287965_0_0_0_KeyValuePair_2_t1383673463_0_0_0 = { 3, GenInst_Int32_t2950945753_0_0_0_Boolean_t97287965_0_0_0_KeyValuePair_2_t1383673463_0_0_0_Types };
extern const Il2CppType TMP_MeshInfo_t2771747634_0_0_0;
static const Il2CppType* GenInst_TMP_MeshInfo_t2771747634_0_0_0_Types[] = { &TMP_MeshInfo_t2771747634_0_0_0 };
extern const Il2CppGenericInst GenInst_TMP_MeshInfo_t2771747634_0_0_0 = { 1, GenInst_TMP_MeshInfo_t2771747634_0_0_0_Types };
extern const Il2CppType TMP_Style_t3479380764_0_0_0;
static const Il2CppType* GenInst_TMP_Style_t3479380764_0_0_0_Types[] = { &TMP_Style_t3479380764_0_0_0 };
extern const Il2CppGenericInst GenInst_TMP_Style_t3479380764_0_0_0 = { 1, GenInst_TMP_Style_t3479380764_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2950945753_0_0_0_TMP_Style_t3479380764_0_0_0_Types[] = { &Int32_t2950945753_0_0_0, &TMP_Style_t3479380764_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2950945753_0_0_0_TMP_Style_t3479380764_0_0_0 = { 2, GenInst_Int32_t2950945753_0_0_0_TMP_Style_t3479380764_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2950945753_0_0_0_TMP_Style_t3479380764_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types[] = { &Int32_t2950945753_0_0_0, &TMP_Style_t3479380764_0_0_0, &DictionaryEntry_t3123975638_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2950945753_0_0_0_TMP_Style_t3479380764_0_0_0_DictionaryEntry_t3123975638_0_0_0 = { 3, GenInst_Int32_t2950945753_0_0_0_TMP_Style_t3479380764_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t470798966_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t470798966_0_0_0_Types[] = { &KeyValuePair_2_t470798966_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t470798966_0_0_0 = { 1, GenInst_KeyValuePair_2_t470798966_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2950945753_0_0_0_TMP_Style_t3479380764_0_0_0_KeyValuePair_2_t470798966_0_0_0_Types[] = { &Int32_t2950945753_0_0_0, &TMP_Style_t3479380764_0_0_0, &KeyValuePair_2_t470798966_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2950945753_0_0_0_TMP_Style_t3479380764_0_0_0_KeyValuePair_2_t470798966_0_0_0 = { 3, GenInst_Int32_t2950945753_0_0_0_TMP_Style_t3479380764_0_0_0_KeyValuePair_2_t470798966_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2950945753_0_0_0_TMP_Style_t3479380764_0_0_0_TMP_Style_t3479380764_0_0_0_Types[] = { &Int32_t2950945753_0_0_0, &TMP_Style_t3479380764_0_0_0, &TMP_Style_t3479380764_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2950945753_0_0_0_TMP_Style_t3479380764_0_0_0_TMP_Style_t3479380764_0_0_0 = { 3, GenInst_Int32_t2950945753_0_0_0_TMP_Style_t3479380764_0_0_0_TMP_Style_t3479380764_0_0_0_Types };
extern const Il2CppType TextAlignmentOptions_t4036791236_0_0_0;
static const Il2CppType* GenInst_TextAlignmentOptions_t4036791236_0_0_0_Types[] = { &TextAlignmentOptions_t4036791236_0_0_0 };
extern const Il2CppGenericInst GenInst_TextAlignmentOptions_t4036791236_0_0_0 = { 1, GenInst_TextAlignmentOptions_t4036791236_0_0_0_Types };
extern const Il2CppType XML_TagAttribute_t1174424309_0_0_0;
static const Il2CppType* GenInst_XML_TagAttribute_t1174424309_0_0_0_Types[] = { &XML_TagAttribute_t1174424309_0_0_0 };
extern const Il2CppGenericInst GenInst_XML_TagAttribute_t1174424309_0_0_0 = { 1, GenInst_XML_TagAttribute_t1174424309_0_0_0_Types };
extern const Il2CppType TMP_LinkInfo_t1092083476_0_0_0;
static const Il2CppType* GenInst_TMP_LinkInfo_t1092083476_0_0_0_Types[] = { &TMP_LinkInfo_t1092083476_0_0_0 };
extern const Il2CppGenericInst GenInst_TMP_LinkInfo_t1092083476_0_0_0 = { 1, GenInst_TMP_LinkInfo_t1092083476_0_0_0_Types };
extern const Il2CppType TMP_PageInfo_t2608430633_0_0_0;
static const Il2CppType* GenInst_TMP_PageInfo_t2608430633_0_0_0_Types[] = { &TMP_PageInfo_t2608430633_0_0_0 };
extern const Il2CppGenericInst GenInst_TMP_PageInfo_t2608430633_0_0_0 = { 1, GenInst_TMP_PageInfo_t2608430633_0_0_0_Types };
extern const Il2CppType TMP_Text_t2599618874_0_0_0;
static const Il2CppType* GenInst_TMP_Text_t2599618874_0_0_0_Types[] = { &TMP_Text_t2599618874_0_0_0 };
extern const Il2CppGenericInst GenInst_TMP_Text_t2599618874_0_0_0 = { 1, GenInst_TMP_Text_t2599618874_0_0_0_Types };
extern const Il2CppType Compute_DT_EventArgs_t1071353166_0_0_0;
static const Il2CppType* GenInst_Il2CppObject_0_0_0_Compute_DT_EventArgs_t1071353166_0_0_0_Types[] = { &Il2CppObject_0_0_0, &Compute_DT_EventArgs_t1071353166_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_Compute_DT_EventArgs_t1071353166_0_0_0 = { 2, GenInst_Il2CppObject_0_0_0_Compute_DT_EventArgs_t1071353166_0_0_0_Types };
extern const Il2CppType Action_2_t461255840_0_0_0;
static const Il2CppType* GenInst_Action_2_t461255840_0_0_0_Types[] = { &Action_2_t461255840_0_0_0 };
extern const Il2CppGenericInst GenInst_Action_2_t461255840_0_0_0 = { 1, GenInst_Action_2_t461255840_0_0_0_Types };
extern const Il2CppType LinkedListNode_1_t206430943_0_0_0;
static const Il2CppType* GenInst_Action_2_t461255840_0_0_0_LinkedListNode_1_t206430943_0_0_0_Types[] = { &Action_2_t461255840_0_0_0, &LinkedListNode_1_t206430943_0_0_0 };
extern const Il2CppGenericInst GenInst_Action_2_t461255840_0_0_0_LinkedListNode_1_t206430943_0_0_0 = { 2, GenInst_Action_2_t461255840_0_0_0_LinkedListNode_1_t206430943_0_0_0_Types };
static const Il2CppType* GenInst_LinkedListNode_1_t206430943_0_0_0_Types[] = { &LinkedListNode_1_t206430943_0_0_0 };
extern const Il2CppGenericInst GenInst_LinkedListNode_1_t206430943_0_0_0 = { 1, GenInst_LinkedListNode_1_t206430943_0_0_0_Types };
static const Il2CppType* GenInst_Action_2_t461255840_0_0_0_LinkedListNode_1_t206430943_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types[] = { &Action_2_t461255840_0_0_0, &LinkedListNode_1_t206430943_0_0_0, &DictionaryEntry_t3123975638_0_0_0 };
extern const Il2CppGenericInst GenInst_Action_2_t461255840_0_0_0_LinkedListNode_1_t206430943_0_0_0_DictionaryEntry_t3123975638_0_0_0 = { 3, GenInst_Action_2_t461255840_0_0_0_LinkedListNode_1_t206430943_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2290643574_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2290643574_0_0_0_Types[] = { &KeyValuePair_2_t2290643574_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2290643574_0_0_0 = { 1, GenInst_KeyValuePair_2_t2290643574_0_0_0_Types };
static const Il2CppType* GenInst_Action_2_t461255840_0_0_0_LinkedListNode_1_t206430943_0_0_0_KeyValuePair_2_t2290643574_0_0_0_Types[] = { &Action_2_t461255840_0_0_0, &LinkedListNode_1_t206430943_0_0_0, &KeyValuePair_2_t2290643574_0_0_0 };
extern const Il2CppGenericInst GenInst_Action_2_t461255840_0_0_0_LinkedListNode_1_t206430943_0_0_0_KeyValuePair_2_t2290643574_0_0_0 = { 3, GenInst_Action_2_t461255840_0_0_0_LinkedListNode_1_t206430943_0_0_0_KeyValuePair_2_t2290643574_0_0_0_Types };
static const Il2CppType* GenInst_Action_2_t461255840_0_0_0_LinkedListNode_1_t206430943_0_0_0_LinkedListNode_1_t206430943_0_0_0_Types[] = { &Action_2_t461255840_0_0_0, &LinkedListNode_1_t206430943_0_0_0, &LinkedListNode_1_t206430943_0_0_0 };
extern const Il2CppGenericInst GenInst_Action_2_t461255840_0_0_0_LinkedListNode_1_t206430943_0_0_0_LinkedListNode_1_t206430943_0_0_0 = { 3, GenInst_Action_2_t461255840_0_0_0_LinkedListNode_1_t206430943_0_0_0_LinkedListNode_1_t206430943_0_0_0_Types };
static const Il2CppType* GenInst_Boolean_t97287965_0_0_0_Material_t340375123_0_0_0_Types[] = { &Boolean_t97287965_0_0_0, &Material_t340375123_0_0_0 };
extern const Il2CppGenericInst GenInst_Boolean_t97287965_0_0_0_Material_t340375123_0_0_0 = { 2, GenInst_Boolean_t97287965_0_0_0_Material_t340375123_0_0_0_Types };
extern const Il2CppType Action_2_t2523487705_0_0_0;
static const Il2CppType* GenInst_Action_2_t2523487705_0_0_0_Types[] = { &Action_2_t2523487705_0_0_0 };
extern const Il2CppGenericInst GenInst_Action_2_t2523487705_0_0_0 = { 1, GenInst_Action_2_t2523487705_0_0_0_Types };
extern const Il2CppType LinkedListNode_1_t2268662808_0_0_0;
static const Il2CppType* GenInst_Action_2_t2523487705_0_0_0_LinkedListNode_1_t2268662808_0_0_0_Types[] = { &Action_2_t2523487705_0_0_0, &LinkedListNode_1_t2268662808_0_0_0 };
extern const Il2CppGenericInst GenInst_Action_2_t2523487705_0_0_0_LinkedListNode_1_t2268662808_0_0_0 = { 2, GenInst_Action_2_t2523487705_0_0_0_LinkedListNode_1_t2268662808_0_0_0_Types };
static const Il2CppType* GenInst_LinkedListNode_1_t2268662808_0_0_0_Types[] = { &LinkedListNode_1_t2268662808_0_0_0 };
extern const Il2CppGenericInst GenInst_LinkedListNode_1_t2268662808_0_0_0 = { 1, GenInst_LinkedListNode_1_t2268662808_0_0_0_Types };
static const Il2CppType* GenInst_Action_2_t2523487705_0_0_0_LinkedListNode_1_t2268662808_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types[] = { &Action_2_t2523487705_0_0_0, &LinkedListNode_1_t2268662808_0_0_0, &DictionaryEntry_t3123975638_0_0_0 };
extern const Il2CppGenericInst GenInst_Action_2_t2523487705_0_0_0_LinkedListNode_1_t2268662808_0_0_0_DictionaryEntry_t3123975638_0_0_0 = { 3, GenInst_Action_2_t2523487705_0_0_0_LinkedListNode_1_t2268662808_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2019553650_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2019553650_0_0_0_Types[] = { &KeyValuePair_2_t2019553650_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2019553650_0_0_0 = { 1, GenInst_KeyValuePair_2_t2019553650_0_0_0_Types };
static const Il2CppType* GenInst_Action_2_t2523487705_0_0_0_LinkedListNode_1_t2268662808_0_0_0_KeyValuePair_2_t2019553650_0_0_0_Types[] = { &Action_2_t2523487705_0_0_0, &LinkedListNode_1_t2268662808_0_0_0, &KeyValuePair_2_t2019553650_0_0_0 };
extern const Il2CppGenericInst GenInst_Action_2_t2523487705_0_0_0_LinkedListNode_1_t2268662808_0_0_0_KeyValuePair_2_t2019553650_0_0_0 = { 3, GenInst_Action_2_t2523487705_0_0_0_LinkedListNode_1_t2268662808_0_0_0_KeyValuePair_2_t2019553650_0_0_0_Types };
static const Il2CppType* GenInst_Action_2_t2523487705_0_0_0_LinkedListNode_1_t2268662808_0_0_0_LinkedListNode_1_t2268662808_0_0_0_Types[] = { &Action_2_t2523487705_0_0_0, &LinkedListNode_1_t2268662808_0_0_0, &LinkedListNode_1_t2268662808_0_0_0 };
extern const Il2CppGenericInst GenInst_Action_2_t2523487705_0_0_0_LinkedListNode_1_t2268662808_0_0_0_LinkedListNode_1_t2268662808_0_0_0 = { 3, GenInst_Action_2_t2523487705_0_0_0_LinkedListNode_1_t2268662808_0_0_0_LinkedListNode_1_t2268662808_0_0_0_Types };
extern const Il2CppType Action_2_t4078723960_0_0_0;
static const Il2CppType* GenInst_Action_2_t4078723960_0_0_0_Types[] = { &Action_2_t4078723960_0_0_0 };
extern const Il2CppGenericInst GenInst_Action_2_t4078723960_0_0_0 = { 1, GenInst_Action_2_t4078723960_0_0_0_Types };
extern const Il2CppType LinkedListNode_1_t3823899063_0_0_0;
static const Il2CppType* GenInst_Action_2_t4078723960_0_0_0_LinkedListNode_1_t3823899063_0_0_0_Types[] = { &Action_2_t4078723960_0_0_0, &LinkedListNode_1_t3823899063_0_0_0 };
extern const Il2CppGenericInst GenInst_Action_2_t4078723960_0_0_0_LinkedListNode_1_t3823899063_0_0_0 = { 2, GenInst_Action_2_t4078723960_0_0_0_LinkedListNode_1_t3823899063_0_0_0_Types };
static const Il2CppType* GenInst_LinkedListNode_1_t3823899063_0_0_0_Types[] = { &LinkedListNode_1_t3823899063_0_0_0 };
extern const Il2CppGenericInst GenInst_LinkedListNode_1_t3823899063_0_0_0 = { 1, GenInst_LinkedListNode_1_t3823899063_0_0_0_Types };
static const Il2CppType* GenInst_Action_2_t4078723960_0_0_0_LinkedListNode_1_t3823899063_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types[] = { &Action_2_t4078723960_0_0_0, &LinkedListNode_1_t3823899063_0_0_0, &DictionaryEntry_t3123975638_0_0_0 };
extern const Il2CppGenericInst GenInst_Action_2_t4078723960_0_0_0_LinkedListNode_1_t3823899063_0_0_0_DictionaryEntry_t3123975638_0_0_0 = { 3, GenInst_Action_2_t4078723960_0_0_0_LinkedListNode_1_t3823899063_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1292723222_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1292723222_0_0_0_Types[] = { &KeyValuePair_2_t1292723222_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1292723222_0_0_0 = { 1, GenInst_KeyValuePair_2_t1292723222_0_0_0_Types };
static const Il2CppType* GenInst_Action_2_t4078723960_0_0_0_LinkedListNode_1_t3823899063_0_0_0_KeyValuePair_2_t1292723222_0_0_0_Types[] = { &Action_2_t4078723960_0_0_0, &LinkedListNode_1_t3823899063_0_0_0, &KeyValuePair_2_t1292723222_0_0_0 };
extern const Il2CppGenericInst GenInst_Action_2_t4078723960_0_0_0_LinkedListNode_1_t3823899063_0_0_0_KeyValuePair_2_t1292723222_0_0_0 = { 3, GenInst_Action_2_t4078723960_0_0_0_LinkedListNode_1_t3823899063_0_0_0_KeyValuePair_2_t1292723222_0_0_0_Types };
static const Il2CppType* GenInst_Action_2_t4078723960_0_0_0_LinkedListNode_1_t3823899063_0_0_0_LinkedListNode_1_t3823899063_0_0_0_Types[] = { &Action_2_t4078723960_0_0_0, &LinkedListNode_1_t3823899063_0_0_0, &LinkedListNode_1_t3823899063_0_0_0 };
extern const Il2CppGenericInst GenInst_Action_2_t4078723960_0_0_0_LinkedListNode_1_t3823899063_0_0_0_LinkedListNode_1_t3823899063_0_0_0 = { 3, GenInst_Action_2_t4078723960_0_0_0_LinkedListNode_1_t3823899063_0_0_0_LinkedListNode_1_t3823899063_0_0_0_Types };
static const Il2CppType* GenInst_Boolean_t97287965_0_0_0_TMP_FontAsset_t364381626_0_0_0_Types[] = { &Boolean_t97287965_0_0_0, &TMP_FontAsset_t364381626_0_0_0 };
extern const Il2CppGenericInst GenInst_Boolean_t97287965_0_0_0_TMP_FontAsset_t364381626_0_0_0 = { 2, GenInst_Boolean_t97287965_0_0_0_TMP_FontAsset_t364381626_0_0_0_Types };
extern const Il2CppType Action_2_t4102730463_0_0_0;
static const Il2CppType* GenInst_Action_2_t4102730463_0_0_0_Types[] = { &Action_2_t4102730463_0_0_0 };
extern const Il2CppGenericInst GenInst_Action_2_t4102730463_0_0_0 = { 1, GenInst_Action_2_t4102730463_0_0_0_Types };
extern const Il2CppType LinkedListNode_1_t3847905566_0_0_0;
static const Il2CppType* GenInst_Action_2_t4102730463_0_0_0_LinkedListNode_1_t3847905566_0_0_0_Types[] = { &Action_2_t4102730463_0_0_0, &LinkedListNode_1_t3847905566_0_0_0 };
extern const Il2CppGenericInst GenInst_Action_2_t4102730463_0_0_0_LinkedListNode_1_t3847905566_0_0_0 = { 2, GenInst_Action_2_t4102730463_0_0_0_LinkedListNode_1_t3847905566_0_0_0_Types };
static const Il2CppType* GenInst_LinkedListNode_1_t3847905566_0_0_0_Types[] = { &LinkedListNode_1_t3847905566_0_0_0 };
extern const Il2CppGenericInst GenInst_LinkedListNode_1_t3847905566_0_0_0 = { 1, GenInst_LinkedListNode_1_t3847905566_0_0_0_Types };
static const Il2CppType* GenInst_Action_2_t4102730463_0_0_0_LinkedListNode_1_t3847905566_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types[] = { &Action_2_t4102730463_0_0_0, &LinkedListNode_1_t3847905566_0_0_0, &DictionaryEntry_t3123975638_0_0_0 };
extern const Il2CppGenericInst GenInst_Action_2_t4102730463_0_0_0_LinkedListNode_1_t3847905566_0_0_0_DictionaryEntry_t3123975638_0_0_0 = { 3, GenInst_Action_2_t4102730463_0_0_0_LinkedListNode_1_t3847905566_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t615435930_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t615435930_0_0_0_Types[] = { &KeyValuePair_2_t615435930_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t615435930_0_0_0 = { 1, GenInst_KeyValuePair_2_t615435930_0_0_0_Types };
static const Il2CppType* GenInst_Action_2_t4102730463_0_0_0_LinkedListNode_1_t3847905566_0_0_0_KeyValuePair_2_t615435930_0_0_0_Types[] = { &Action_2_t4102730463_0_0_0, &LinkedListNode_1_t3847905566_0_0_0, &KeyValuePair_2_t615435930_0_0_0 };
extern const Il2CppGenericInst GenInst_Action_2_t4102730463_0_0_0_LinkedListNode_1_t3847905566_0_0_0_KeyValuePair_2_t615435930_0_0_0 = { 3, GenInst_Action_2_t4102730463_0_0_0_LinkedListNode_1_t3847905566_0_0_0_KeyValuePair_2_t615435930_0_0_0_Types };
static const Il2CppType* GenInst_Action_2_t4102730463_0_0_0_LinkedListNode_1_t3847905566_0_0_0_LinkedListNode_1_t3847905566_0_0_0_Types[] = { &Action_2_t4102730463_0_0_0, &LinkedListNode_1_t3847905566_0_0_0, &LinkedListNode_1_t3847905566_0_0_0 };
extern const Il2CppGenericInst GenInst_Action_2_t4102730463_0_0_0_LinkedListNode_1_t3847905566_0_0_0_LinkedListNode_1_t3847905566_0_0_0 = { 3, GenInst_Action_2_t4102730463_0_0_0_LinkedListNode_1_t3847905566_0_0_0_LinkedListNode_1_t3847905566_0_0_0_Types };
static const Il2CppType* GenInst_Boolean_t97287965_0_0_0_Object_t631007953_0_0_0_Types[] = { &Boolean_t97287965_0_0_0, &Object_t631007953_0_0_0 };
extern const Il2CppGenericInst GenInst_Boolean_t97287965_0_0_0_Object_t631007953_0_0_0 = { 2, GenInst_Boolean_t97287965_0_0_0_Object_t631007953_0_0_0_Types };
extern const Il2CppType Action_2_t74389494_0_0_0;
static const Il2CppType* GenInst_Action_2_t74389494_0_0_0_Types[] = { &Action_2_t74389494_0_0_0 };
extern const Il2CppGenericInst GenInst_Action_2_t74389494_0_0_0 = { 1, GenInst_Action_2_t74389494_0_0_0_Types };
extern const Il2CppType LinkedListNode_1_t4114531893_0_0_0;
static const Il2CppType* GenInst_Action_2_t74389494_0_0_0_LinkedListNode_1_t4114531893_0_0_0_Types[] = { &Action_2_t74389494_0_0_0, &LinkedListNode_1_t4114531893_0_0_0 };
extern const Il2CppGenericInst GenInst_Action_2_t74389494_0_0_0_LinkedListNode_1_t4114531893_0_0_0 = { 2, GenInst_Action_2_t74389494_0_0_0_LinkedListNode_1_t4114531893_0_0_0_Types };
static const Il2CppType* GenInst_LinkedListNode_1_t4114531893_0_0_0_Types[] = { &LinkedListNode_1_t4114531893_0_0_0 };
extern const Il2CppGenericInst GenInst_LinkedListNode_1_t4114531893_0_0_0 = { 1, GenInst_LinkedListNode_1_t4114531893_0_0_0_Types };
static const Il2CppType* GenInst_Action_2_t74389494_0_0_0_LinkedListNode_1_t4114531893_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types[] = { &Action_2_t74389494_0_0_0, &LinkedListNode_1_t4114531893_0_0_0, &DictionaryEntry_t3123975638_0_0_0 };
extern const Il2CppGenericInst GenInst_Action_2_t74389494_0_0_0_LinkedListNode_1_t4114531893_0_0_0_DictionaryEntry_t3123975638_0_0_0 = { 3, GenInst_Action_2_t74389494_0_0_0_LinkedListNode_1_t4114531893_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2439265374_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2439265374_0_0_0_Types[] = { &KeyValuePair_2_t2439265374_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2439265374_0_0_0 = { 1, GenInst_KeyValuePair_2_t2439265374_0_0_0_Types };
static const Il2CppType* GenInst_Action_2_t74389494_0_0_0_LinkedListNode_1_t4114531893_0_0_0_KeyValuePair_2_t2439265374_0_0_0_Types[] = { &Action_2_t74389494_0_0_0, &LinkedListNode_1_t4114531893_0_0_0, &KeyValuePair_2_t2439265374_0_0_0 };
extern const Il2CppGenericInst GenInst_Action_2_t74389494_0_0_0_LinkedListNode_1_t4114531893_0_0_0_KeyValuePair_2_t2439265374_0_0_0 = { 3, GenInst_Action_2_t74389494_0_0_0_LinkedListNode_1_t4114531893_0_0_0_KeyValuePair_2_t2439265374_0_0_0_Types };
static const Il2CppType* GenInst_Action_2_t74389494_0_0_0_LinkedListNode_1_t4114531893_0_0_0_LinkedListNode_1_t4114531893_0_0_0_Types[] = { &Action_2_t74389494_0_0_0, &LinkedListNode_1_t4114531893_0_0_0, &LinkedListNode_1_t4114531893_0_0_0 };
extern const Il2CppGenericInst GenInst_Action_2_t74389494_0_0_0_LinkedListNode_1_t4114531893_0_0_0_LinkedListNode_1_t4114531893_0_0_0 = { 3, GenInst_Action_2_t74389494_0_0_0_LinkedListNode_1_t4114531893_0_0_0_LinkedListNode_1_t4114531893_0_0_0_Types };
extern const Il2CppType TextMeshPro_t2393593166_0_0_0;
static const Il2CppType* GenInst_Boolean_t97287965_0_0_0_TextMeshPro_t2393593166_0_0_0_Types[] = { &Boolean_t97287965_0_0_0, &TextMeshPro_t2393593166_0_0_0 };
extern const Il2CppGenericInst GenInst_Boolean_t97287965_0_0_0_TextMeshPro_t2393593166_0_0_0 = { 2, GenInst_Boolean_t97287965_0_0_0_TextMeshPro_t2393593166_0_0_0_Types };
extern const Il2CppType Action_2_t1836974707_0_0_0;
static const Il2CppType* GenInst_Action_2_t1836974707_0_0_0_Types[] = { &Action_2_t1836974707_0_0_0 };
extern const Il2CppGenericInst GenInst_Action_2_t1836974707_0_0_0 = { 1, GenInst_Action_2_t1836974707_0_0_0_Types };
extern const Il2CppType LinkedListNode_1_t1582149810_0_0_0;
static const Il2CppType* GenInst_Action_2_t1836974707_0_0_0_LinkedListNode_1_t1582149810_0_0_0_Types[] = { &Action_2_t1836974707_0_0_0, &LinkedListNode_1_t1582149810_0_0_0 };
extern const Il2CppGenericInst GenInst_Action_2_t1836974707_0_0_0_LinkedListNode_1_t1582149810_0_0_0 = { 2, GenInst_Action_2_t1836974707_0_0_0_LinkedListNode_1_t1582149810_0_0_0_Types };
static const Il2CppType* GenInst_LinkedListNode_1_t1582149810_0_0_0_Types[] = { &LinkedListNode_1_t1582149810_0_0_0 };
extern const Il2CppGenericInst GenInst_LinkedListNode_1_t1582149810_0_0_0 = { 1, GenInst_LinkedListNode_1_t1582149810_0_0_0_Types };
static const Il2CppType* GenInst_Action_2_t1836974707_0_0_0_LinkedListNode_1_t1582149810_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types[] = { &Action_2_t1836974707_0_0_0, &LinkedListNode_1_t1582149810_0_0_0, &DictionaryEntry_t3123975638_0_0_0 };
extern const Il2CppGenericInst GenInst_Action_2_t1836974707_0_0_0_LinkedListNode_1_t1582149810_0_0_0_DictionaryEntry_t3123975638_0_0_0 = { 3, GenInst_Action_2_t1836974707_0_0_0_LinkedListNode_1_t1582149810_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t4104837578_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t4104837578_0_0_0_Types[] = { &KeyValuePair_2_t4104837578_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t4104837578_0_0_0 = { 1, GenInst_KeyValuePair_2_t4104837578_0_0_0_Types };
static const Il2CppType* GenInst_Action_2_t1836974707_0_0_0_LinkedListNode_1_t1582149810_0_0_0_KeyValuePair_2_t4104837578_0_0_0_Types[] = { &Action_2_t1836974707_0_0_0, &LinkedListNode_1_t1582149810_0_0_0, &KeyValuePair_2_t4104837578_0_0_0 };
extern const Il2CppGenericInst GenInst_Action_2_t1836974707_0_0_0_LinkedListNode_1_t1582149810_0_0_0_KeyValuePair_2_t4104837578_0_0_0 = { 3, GenInst_Action_2_t1836974707_0_0_0_LinkedListNode_1_t1582149810_0_0_0_KeyValuePair_2_t4104837578_0_0_0_Types };
static const Il2CppType* GenInst_Action_2_t1836974707_0_0_0_LinkedListNode_1_t1582149810_0_0_0_LinkedListNode_1_t1582149810_0_0_0_Types[] = { &Action_2_t1836974707_0_0_0, &LinkedListNode_1_t1582149810_0_0_0, &LinkedListNode_1_t1582149810_0_0_0 };
extern const Il2CppGenericInst GenInst_Action_2_t1836974707_0_0_0_LinkedListNode_1_t1582149810_0_0_0_LinkedListNode_1_t1582149810_0_0_0 = { 3, GenInst_Action_2_t1836974707_0_0_0_LinkedListNode_1_t1582149810_0_0_0_LinkedListNode_1_t1582149810_0_0_0_Types };
static const Il2CppType* GenInst_GameObject_t1113636619_0_0_0_Material_t340375123_0_0_0_Material_t340375123_0_0_0_Types[] = { &GameObject_t1113636619_0_0_0, &Material_t340375123_0_0_0, &Material_t340375123_0_0_0 };
extern const Il2CppGenericInst GenInst_GameObject_t1113636619_0_0_0_Material_t340375123_0_0_0_Material_t340375123_0_0_0 = { 3, GenInst_GameObject_t1113636619_0_0_0_Material_t340375123_0_0_0_Material_t340375123_0_0_0_Types };
extern const Il2CppType Action_3_t2823050148_0_0_0;
static const Il2CppType* GenInst_Action_3_t2823050148_0_0_0_Types[] = { &Action_3_t2823050148_0_0_0 };
extern const Il2CppGenericInst GenInst_Action_3_t2823050148_0_0_0 = { 1, GenInst_Action_3_t2823050148_0_0_0_Types };
extern const Il2CppType LinkedListNode_1_t2568225251_0_0_0;
static const Il2CppType* GenInst_Action_3_t2823050148_0_0_0_LinkedListNode_1_t2568225251_0_0_0_Types[] = { &Action_3_t2823050148_0_0_0, &LinkedListNode_1_t2568225251_0_0_0 };
extern const Il2CppGenericInst GenInst_Action_3_t2823050148_0_0_0_LinkedListNode_1_t2568225251_0_0_0 = { 2, GenInst_Action_3_t2823050148_0_0_0_LinkedListNode_1_t2568225251_0_0_0_Types };
static const Il2CppType* GenInst_LinkedListNode_1_t2568225251_0_0_0_Types[] = { &LinkedListNode_1_t2568225251_0_0_0 };
extern const Il2CppGenericInst GenInst_LinkedListNode_1_t2568225251_0_0_0 = { 1, GenInst_LinkedListNode_1_t2568225251_0_0_0_Types };
static const Il2CppType* GenInst_Action_3_t2823050148_0_0_0_LinkedListNode_1_t2568225251_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types[] = { &Action_3_t2823050148_0_0_0, &LinkedListNode_1_t2568225251_0_0_0, &DictionaryEntry_t3123975638_0_0_0 };
extern const Il2CppGenericInst GenInst_Action_3_t2823050148_0_0_0_LinkedListNode_1_t2568225251_0_0_0_DictionaryEntry_t3123975638_0_0_0 = { 3, GenInst_Action_3_t2823050148_0_0_0_LinkedListNode_1_t2568225251_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2622726630_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2622726630_0_0_0_Types[] = { &KeyValuePair_2_t2622726630_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2622726630_0_0_0 = { 1, GenInst_KeyValuePair_2_t2622726630_0_0_0_Types };
static const Il2CppType* GenInst_Action_3_t2823050148_0_0_0_LinkedListNode_1_t2568225251_0_0_0_KeyValuePair_2_t2622726630_0_0_0_Types[] = { &Action_3_t2823050148_0_0_0, &LinkedListNode_1_t2568225251_0_0_0, &KeyValuePair_2_t2622726630_0_0_0 };
extern const Il2CppGenericInst GenInst_Action_3_t2823050148_0_0_0_LinkedListNode_1_t2568225251_0_0_0_KeyValuePair_2_t2622726630_0_0_0 = { 3, GenInst_Action_3_t2823050148_0_0_0_LinkedListNode_1_t2568225251_0_0_0_KeyValuePair_2_t2622726630_0_0_0_Types };
static const Il2CppType* GenInst_Action_3_t2823050148_0_0_0_LinkedListNode_1_t2568225251_0_0_0_LinkedListNode_1_t2568225251_0_0_0_Types[] = { &Action_3_t2823050148_0_0_0, &LinkedListNode_1_t2568225251_0_0_0, &LinkedListNode_1_t2568225251_0_0_0 };
extern const Il2CppGenericInst GenInst_Action_3_t2823050148_0_0_0_LinkedListNode_1_t2568225251_0_0_0_LinkedListNode_1_t2568225251_0_0_0 = { 3, GenInst_Action_3_t2823050148_0_0_0_LinkedListNode_1_t2568225251_0_0_0_LinkedListNode_1_t2568225251_0_0_0_Types };
extern const Il2CppType Action_1_t269755560_0_0_0;
static const Il2CppType* GenInst_Action_1_t269755560_0_0_0_Types[] = { &Action_1_t269755560_0_0_0 };
extern const Il2CppGenericInst GenInst_Action_1_t269755560_0_0_0 = { 1, GenInst_Action_1_t269755560_0_0_0_Types };
extern const Il2CppType LinkedListNode_1_t14930663_0_0_0;
static const Il2CppType* GenInst_Action_1_t269755560_0_0_0_LinkedListNode_1_t14930663_0_0_0_Types[] = { &Action_1_t269755560_0_0_0, &LinkedListNode_1_t14930663_0_0_0 };
extern const Il2CppGenericInst GenInst_Action_1_t269755560_0_0_0_LinkedListNode_1_t14930663_0_0_0 = { 2, GenInst_Action_1_t269755560_0_0_0_LinkedListNode_1_t14930663_0_0_0_Types };
static const Il2CppType* GenInst_LinkedListNode_1_t14930663_0_0_0_Types[] = { &LinkedListNode_1_t14930663_0_0_0 };
extern const Il2CppGenericInst GenInst_LinkedListNode_1_t14930663_0_0_0 = { 1, GenInst_LinkedListNode_1_t14930663_0_0_0_Types };
static const Il2CppType* GenInst_Action_1_t269755560_0_0_0_LinkedListNode_1_t14930663_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types[] = { &Action_1_t269755560_0_0_0, &LinkedListNode_1_t14930663_0_0_0, &DictionaryEntry_t3123975638_0_0_0 };
extern const Il2CppGenericInst GenInst_Action_1_t269755560_0_0_0_LinkedListNode_1_t14930663_0_0_0_DictionaryEntry_t3123975638_0_0_0 = { 3, GenInst_Action_1_t269755560_0_0_0_LinkedListNode_1_t14930663_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t3809986902_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t3809986902_0_0_0_Types[] = { &KeyValuePair_2_t3809986902_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3809986902_0_0_0 = { 1, GenInst_KeyValuePair_2_t3809986902_0_0_0_Types };
static const Il2CppType* GenInst_Action_1_t269755560_0_0_0_LinkedListNode_1_t14930663_0_0_0_KeyValuePair_2_t3809986902_0_0_0_Types[] = { &Action_1_t269755560_0_0_0, &LinkedListNode_1_t14930663_0_0_0, &KeyValuePair_2_t3809986902_0_0_0 };
extern const Il2CppGenericInst GenInst_Action_1_t269755560_0_0_0_LinkedListNode_1_t14930663_0_0_0_KeyValuePair_2_t3809986902_0_0_0 = { 3, GenInst_Action_1_t269755560_0_0_0_LinkedListNode_1_t14930663_0_0_0_KeyValuePair_2_t3809986902_0_0_0_Types };
static const Il2CppType* GenInst_Action_1_t269755560_0_0_0_LinkedListNode_1_t14930663_0_0_0_LinkedListNode_1_t14930663_0_0_0_Types[] = { &Action_1_t269755560_0_0_0, &LinkedListNode_1_t14930663_0_0_0, &LinkedListNode_1_t14930663_0_0_0 };
extern const Il2CppGenericInst GenInst_Action_1_t269755560_0_0_0_LinkedListNode_1_t14930663_0_0_0_LinkedListNode_1_t14930663_0_0_0 = { 3, GenInst_Action_1_t269755560_0_0_0_LinkedListNode_1_t14930663_0_0_0_LinkedListNode_1_t14930663_0_0_0_Types };
extern const Il2CppType TMP_ColorGradient_t3678055768_0_0_0;
static const Il2CppType* GenInst_TMP_ColorGradient_t3678055768_0_0_0_Types[] = { &TMP_ColorGradient_t3678055768_0_0_0 };
extern const Il2CppGenericInst GenInst_TMP_ColorGradient_t3678055768_0_0_0 = { 1, GenInst_TMP_ColorGradient_t3678055768_0_0_0_Types };
extern const Il2CppType Action_1_t3850523363_0_0_0;
static const Il2CppType* GenInst_Action_1_t3850523363_0_0_0_Types[] = { &Action_1_t3850523363_0_0_0 };
extern const Il2CppGenericInst GenInst_Action_1_t3850523363_0_0_0 = { 1, GenInst_Action_1_t3850523363_0_0_0_Types };
extern const Il2CppType LinkedListNode_1_t3595698466_0_0_0;
static const Il2CppType* GenInst_Action_1_t3850523363_0_0_0_LinkedListNode_1_t3595698466_0_0_0_Types[] = { &Action_1_t3850523363_0_0_0, &LinkedListNode_1_t3595698466_0_0_0 };
extern const Il2CppGenericInst GenInst_Action_1_t3850523363_0_0_0_LinkedListNode_1_t3595698466_0_0_0 = { 2, GenInst_Action_1_t3850523363_0_0_0_LinkedListNode_1_t3595698466_0_0_0_Types };
static const Il2CppType* GenInst_LinkedListNode_1_t3595698466_0_0_0_Types[] = { &LinkedListNode_1_t3595698466_0_0_0 };
extern const Il2CppGenericInst GenInst_LinkedListNode_1_t3595698466_0_0_0 = { 1, GenInst_LinkedListNode_1_t3595698466_0_0_0_Types };
static const Il2CppType* GenInst_Action_1_t3850523363_0_0_0_LinkedListNode_1_t3595698466_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types[] = { &Action_1_t3850523363_0_0_0, &LinkedListNode_1_t3595698466_0_0_0, &DictionaryEntry_t3123975638_0_0_0 };
extern const Il2CppGenericInst GenInst_Action_1_t3850523363_0_0_0_LinkedListNode_1_t3595698466_0_0_0_DictionaryEntry_t3123975638_0_0_0 = { 3, GenInst_Action_1_t3850523363_0_0_0_LinkedListNode_1_t3595698466_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2695289354_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2695289354_0_0_0_Types[] = { &KeyValuePair_2_t2695289354_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2695289354_0_0_0 = { 1, GenInst_KeyValuePair_2_t2695289354_0_0_0_Types };
static const Il2CppType* GenInst_Action_1_t3850523363_0_0_0_LinkedListNode_1_t3595698466_0_0_0_KeyValuePair_2_t2695289354_0_0_0_Types[] = { &Action_1_t3850523363_0_0_0, &LinkedListNode_1_t3595698466_0_0_0, &KeyValuePair_2_t2695289354_0_0_0 };
extern const Il2CppGenericInst GenInst_Action_1_t3850523363_0_0_0_LinkedListNode_1_t3595698466_0_0_0_KeyValuePair_2_t2695289354_0_0_0 = { 3, GenInst_Action_1_t3850523363_0_0_0_LinkedListNode_1_t3595698466_0_0_0_KeyValuePair_2_t2695289354_0_0_0_Types };
static const Il2CppType* GenInst_Action_1_t3850523363_0_0_0_LinkedListNode_1_t3595698466_0_0_0_LinkedListNode_1_t3595698466_0_0_0_Types[] = { &Action_1_t3850523363_0_0_0, &LinkedListNode_1_t3595698466_0_0_0, &LinkedListNode_1_t3595698466_0_0_0 };
extern const Il2CppGenericInst GenInst_Action_1_t3850523363_0_0_0_LinkedListNode_1_t3595698466_0_0_0_LinkedListNode_1_t3595698466_0_0_0 = { 3, GenInst_Action_1_t3850523363_0_0_0_LinkedListNode_1_t3595698466_0_0_0_LinkedListNode_1_t3595698466_0_0_0_Types };
extern const Il2CppType TextMeshProUGUI_t529313277_0_0_0;
static const Il2CppType* GenInst_Boolean_t97287965_0_0_0_TextMeshProUGUI_t529313277_0_0_0_Types[] = { &Boolean_t97287965_0_0_0, &TextMeshProUGUI_t529313277_0_0_0 };
extern const Il2CppGenericInst GenInst_Boolean_t97287965_0_0_0_TextMeshProUGUI_t529313277_0_0_0 = { 2, GenInst_Boolean_t97287965_0_0_0_TextMeshProUGUI_t529313277_0_0_0_Types };
extern const Il2CppType Action_2_t4267662114_0_0_0;
static const Il2CppType* GenInst_Action_2_t4267662114_0_0_0_Types[] = { &Action_2_t4267662114_0_0_0 };
extern const Il2CppGenericInst GenInst_Action_2_t4267662114_0_0_0 = { 1, GenInst_Action_2_t4267662114_0_0_0_Types };
extern const Il2CppType LinkedListNode_1_t4012837217_0_0_0;
static const Il2CppType* GenInst_Action_2_t4267662114_0_0_0_LinkedListNode_1_t4012837217_0_0_0_Types[] = { &Action_2_t4267662114_0_0_0, &LinkedListNode_1_t4012837217_0_0_0 };
extern const Il2CppGenericInst GenInst_Action_2_t4267662114_0_0_0_LinkedListNode_1_t4012837217_0_0_0 = { 2, GenInst_Action_2_t4267662114_0_0_0_LinkedListNode_1_t4012837217_0_0_0_Types };
static const Il2CppType* GenInst_LinkedListNode_1_t4012837217_0_0_0_Types[] = { &LinkedListNode_1_t4012837217_0_0_0 };
extern const Il2CppGenericInst GenInst_LinkedListNode_1_t4012837217_0_0_0 = { 1, GenInst_LinkedListNode_1_t4012837217_0_0_0_Types };
static const Il2CppType* GenInst_Action_2_t4267662114_0_0_0_LinkedListNode_1_t4012837217_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types[] = { &Action_2_t4267662114_0_0_0, &LinkedListNode_1_t4012837217_0_0_0, &DictionaryEntry_t3123975638_0_0_0 };
extern const Il2CppGenericInst GenInst_Action_2_t4267662114_0_0_0_LinkedListNode_1_t4012837217_0_0_0_DictionaryEntry_t3123975638_0_0_0 = { 3, GenInst_Action_2_t4267662114_0_0_0_LinkedListNode_1_t4012837217_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t3478638126_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t3478638126_0_0_0_Types[] = { &KeyValuePair_2_t3478638126_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3478638126_0_0_0 = { 1, GenInst_KeyValuePair_2_t3478638126_0_0_0_Types };
static const Il2CppType* GenInst_Action_2_t4267662114_0_0_0_LinkedListNode_1_t4012837217_0_0_0_KeyValuePair_2_t3478638126_0_0_0_Types[] = { &Action_2_t4267662114_0_0_0, &LinkedListNode_1_t4012837217_0_0_0, &KeyValuePair_2_t3478638126_0_0_0 };
extern const Il2CppGenericInst GenInst_Action_2_t4267662114_0_0_0_LinkedListNode_1_t4012837217_0_0_0_KeyValuePair_2_t3478638126_0_0_0 = { 3, GenInst_Action_2_t4267662114_0_0_0_LinkedListNode_1_t4012837217_0_0_0_KeyValuePair_2_t3478638126_0_0_0_Types };
static const Il2CppType* GenInst_Action_2_t4267662114_0_0_0_LinkedListNode_1_t4012837217_0_0_0_LinkedListNode_1_t4012837217_0_0_0_Types[] = { &Action_2_t4267662114_0_0_0, &LinkedListNode_1_t4012837217_0_0_0, &LinkedListNode_1_t4012837217_0_0_0 };
extern const Il2CppGenericInst GenInst_Action_2_t4267662114_0_0_0_LinkedListNode_1_t4012837217_0_0_0_LinkedListNode_1_t4012837217_0_0_0 = { 3, GenInst_Action_2_t4267662114_0_0_0_LinkedListNode_1_t4012837217_0_0_0_LinkedListNode_1_t4012837217_0_0_0_Types };
static const Il2CppType* GenInst_KerningPair_t2270855589_0_0_0_Int32_t2950945753_0_0_0_Types[] = { &KerningPair_t2270855589_0_0_0, &Int32_t2950945753_0_0_0 };
extern const Il2CppGenericInst GenInst_KerningPair_t2270855589_0_0_0_Int32_t2950945753_0_0_0 = { 2, GenInst_KerningPair_t2270855589_0_0_0_Int32_t2950945753_0_0_0_Types };
extern const Il2CppType TMP_SubMesh_t2613037997_0_0_0;
static const Il2CppType* GenInst_TMP_SubMesh_t2613037997_0_0_0_Types[] = { &TMP_SubMesh_t2613037997_0_0_0 };
extern const Il2CppGenericInst GenInst_TMP_SubMesh_t2613037997_0_0_0 = { 1, GenInst_TMP_SubMesh_t2613037997_0_0_0_Types };
extern const Il2CppType TMP_SubMeshUI_t1578871311_0_0_0;
static const Il2CppType* GenInst_TMP_SubMeshUI_t1578871311_0_0_0_Types[] = { &TMP_SubMeshUI_t1578871311_0_0_0 };
extern const Il2CppGenericInst GenInst_TMP_SubMeshUI_t1578871311_0_0_0 = { 1, GenInst_TMP_SubMeshUI_t1578871311_0_0_0_Types };
extern const Il2CppType SpriteData_t3048397587_0_0_0;
static const Il2CppType* GenInst_SpriteData_t3048397587_0_0_0_Types[] = { &SpriteData_t3048397587_0_0_0 };
extern const Il2CppGenericInst GenInst_SpriteData_t3048397587_0_0_0 = { 1, GenInst_SpriteData_t3048397587_0_0_0_Types };
extern const Il2CppType SimulatedTarget_t1564170948_0_0_0;
static const Il2CppType* GenInst_SimulatedTarget_t1564170948_0_0_0_Types[] = { &SimulatedTarget_t1564170948_0_0_0 };
extern const Il2CppGenericInst GenInst_SimulatedTarget_t1564170948_0_0_0 = { 1, GenInst_SimulatedTarget_t1564170948_0_0_0_Types };
extern const Il2CppType ImageTarget_t2322341322_0_0_0;
static const Il2CppType* GenInst_ImageTarget_t2322341322_0_0_0_Types[] = { &ImageTarget_t2322341322_0_0_0 };
extern const Il2CppGenericInst GenInst_ImageTarget_t2322341322_0_0_0 = { 1, GenInst_ImageTarget_t2322341322_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_Single_t1397266774_0_0_0_Types[] = { &String_t_0_0_0, &Single_t1397266774_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Single_t1397266774_0_0_0 = { 2, GenInst_String_t_0_0_0_Single_t1397266774_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t847377929_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t847377929_0_0_0_Types[] = { &KeyValuePair_2_t847377929_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t847377929_0_0_0 = { 1, GenInst_KeyValuePair_2_t847377929_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_Single_t1397266774_0_0_0_Single_t1397266774_0_0_0_Types[] = { &Il2CppObject_0_0_0, &Single_t1397266774_0_0_0, &Single_t1397266774_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_Single_t1397266774_0_0_0_Single_t1397266774_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_Single_t1397266774_0_0_0_Single_t1397266774_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_Single_t1397266774_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types[] = { &Il2CppObject_0_0_0, &Single_t1397266774_0_0_0, &DictionaryEntry_t3123975638_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_Single_t1397266774_0_0_0_DictionaryEntry_t3123975638_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_Single_t1397266774_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_Single_t1397266774_0_0_0_KeyValuePair_2_t847377929_0_0_0_Types[] = { &Il2CppObject_0_0_0, &Single_t1397266774_0_0_0, &KeyValuePair_2_t847377929_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_Single_t1397266774_0_0_0_KeyValuePair_2_t847377929_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_Single_t1397266774_0_0_0_KeyValuePair_2_t847377929_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_Single_t1397266774_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types[] = { &String_t_0_0_0, &Single_t1397266774_0_0_0, &DictionaryEntry_t3123975638_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Single_t1397266774_0_0_0_DictionaryEntry_t3123975638_0_0_0 = { 3, GenInst_String_t_0_0_0_Single_t1397266774_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t3580195240_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t3580195240_0_0_0_Types[] = { &KeyValuePair_2_t3580195240_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3580195240_0_0_0 = { 1, GenInst_KeyValuePair_2_t3580195240_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_Single_t1397266774_0_0_0_KeyValuePair_2_t3580195240_0_0_0_Types[] = { &String_t_0_0_0, &Single_t1397266774_0_0_0, &KeyValuePair_2_t3580195240_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Single_t1397266774_0_0_0_KeyValuePair_2_t3580195240_0_0_0 = { 3, GenInst_String_t_0_0_0_Single_t1397266774_0_0_0_KeyValuePair_2_t3580195240_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_Single_t1397266774_0_0_0_Single_t1397266774_0_0_0_Types[] = { &String_t_0_0_0, &Single_t1397266774_0_0_0, &Single_t1397266774_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Single_t1397266774_0_0_0_Single_t1397266774_0_0_0 = { 3, GenInst_String_t_0_0_0_Single_t1397266774_0_0_0_Single_t1397266774_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t2950945753_0_0_0_String_t_0_0_0_Types[] = { &Int32_t2950945753_0_0_0, &String_t_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t2950945753_0_0_0_String_t_0_0_0 = { 2, GenInst_Int32_t2950945753_0_0_0_String_t_0_0_0_Types };
extern const Il2CppType ExtendedTrackingQuality_t364496425_0_0_0;
static const Il2CppType* GenInst_String_t_0_0_0_ExtendedTrackingQuality_t364496425_0_0_0_Types[] = { &String_t_0_0_0, &ExtendedTrackingQuality_t364496425_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_ExtendedTrackingQuality_t364496425_0_0_0 = { 2, GenInst_String_t_0_0_0_ExtendedTrackingQuality_t364496425_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_ExtendedTrackingQuality_t364496425_0_0_0_Types[] = { &Il2CppObject_0_0_0, &ExtendedTrackingQuality_t364496425_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_ExtendedTrackingQuality_t364496425_0_0_0 = { 2, GenInst_Il2CppObject_0_0_0_ExtendedTrackingQuality_t364496425_0_0_0_Types };
extern const Il2CppType Link_t1976611498_0_0_0;
static const Il2CppType* GenInst_Link_t1976611498_0_0_0_Types[] = { &Link_t1976611498_0_0_0 };
extern const Il2CppGenericInst GenInst_Link_t1976611498_0_0_0 = { 1, GenInst_Link_t1976611498_0_0_0_Types };
extern const Il2CppType Trackable_t347424808_0_0_0;
extern const Il2CppType HashSet_1_t2094861142_0_0_0;
static const Il2CppType* GenInst_Trackable_t347424808_0_0_0_HashSet_1_t2094861142_0_0_0_Types[] = { &Trackable_t347424808_0_0_0, &HashSet_1_t2094861142_0_0_0 };
extern const Il2CppGenericInst GenInst_Trackable_t347424808_0_0_0_HashSet_1_t2094861142_0_0_0 = { 2, GenInst_Trackable_t347424808_0_0_0_HashSet_1_t2094861142_0_0_0_Types };
extern const Il2CppType RecognizedTarget_t3529911668_0_0_0;
static const Il2CppType* GenInst_RecognizedTarget_t3529911668_0_0_0_Types[] = { &RecognizedTarget_t3529911668_0_0_0 };
extern const Il2CppGenericInst GenInst_RecognizedTarget_t3529911668_0_0_0 = { 1, GenInst_RecognizedTarget_t3529911668_0_0_0_Types };
extern const Il2CppType Link_t3659072477_0_0_0;
static const Il2CppType* GenInst_Link_t3659072477_0_0_0_Types[] = { &Link_t3659072477_0_0_0 };
extern const Il2CppGenericInst GenInst_Link_t3659072477_0_0_0 = { 1, GenInst_Link_t3659072477_0_0_0_Types };
static const Il2CppType* GenInst_Trackable_t347424808_0_0_0_Types[] = { &Trackable_t347424808_0_0_0 };
extern const Il2CppGenericInst GenInst_Trackable_t347424808_0_0_0 = { 1, GenInst_Trackable_t347424808_0_0_0_Types };
static const Il2CppType* GenInst_HashSet_1_t2094861142_0_0_0_Types[] = { &HashSet_1_t2094861142_0_0_0 };
extern const Il2CppGenericInst GenInst_HashSet_1_t2094861142_0_0_0 = { 1, GenInst_HashSet_1_t2094861142_0_0_0_Types };
static const Il2CppType* GenInst_Trackable_t347424808_0_0_0_HashSet_1_t2094861142_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types[] = { &Trackable_t347424808_0_0_0, &HashSet_1_t2094861142_0_0_0, &DictionaryEntry_t3123975638_0_0_0 };
extern const Il2CppGenericInst GenInst_Trackable_t347424808_0_0_0_HashSet_1_t2094861142_0_0_0_DictionaryEntry_t3123975638_0_0_0 = { 3, GenInst_Trackable_t347424808_0_0_0_HashSet_1_t2094861142_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t4167131205_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t4167131205_0_0_0_Types[] = { &KeyValuePair_2_t4167131205_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t4167131205_0_0_0 = { 1, GenInst_KeyValuePair_2_t4167131205_0_0_0_Types };
static const Il2CppType* GenInst_Trackable_t347424808_0_0_0_HashSet_1_t2094861142_0_0_0_KeyValuePair_2_t4167131205_0_0_0_Types[] = { &Trackable_t347424808_0_0_0, &HashSet_1_t2094861142_0_0_0, &KeyValuePair_2_t4167131205_0_0_0 };
extern const Il2CppGenericInst GenInst_Trackable_t347424808_0_0_0_HashSet_1_t2094861142_0_0_0_KeyValuePair_2_t4167131205_0_0_0 = { 3, GenInst_Trackable_t347424808_0_0_0_HashSet_1_t2094861142_0_0_0_KeyValuePair_2_t4167131205_0_0_0_Types };
static const Il2CppType* GenInst_Trackable_t347424808_0_0_0_HashSet_1_t2094861142_0_0_0_HashSet_1_t2094861142_0_0_0_Types[] = { &Trackable_t347424808_0_0_0, &HashSet_1_t2094861142_0_0_0, &HashSet_1_t2094861142_0_0_0 };
extern const Il2CppGenericInst GenInst_Trackable_t347424808_0_0_0_HashSet_1_t2094861142_0_0_0_HashSet_1_t2094861142_0_0_0 = { 3, GenInst_Trackable_t347424808_0_0_0_HashSet_1_t2094861142_0_0_0_HashSet_1_t2094861142_0_0_0_Types };
extern const Il2CppType InstantTarget_t3313867941_0_0_0;
static const Il2CppType* GenInst_InstantTarget_t3313867941_0_0_0_Types[] = { &InstantTarget_t3313867941_0_0_0 };
extern const Il2CppGenericInst GenInst_InstantTarget_t3313867941_0_0_0 = { 1, GenInst_InstantTarget_t3313867941_0_0_0_Types };
static const Il2CppType* GenInst_Boolean_t97287965_0_0_0_Vector2_t2156229523_0_0_0_Vector3_t3722313464_0_0_0_Types[] = { &Boolean_t97287965_0_0_0, &Vector2_t2156229523_0_0_0, &Vector3_t3722313464_0_0_0 };
extern const Il2CppGenericInst GenInst_Boolean_t97287965_0_0_0_Vector2_t2156229523_0_0_0_Vector3_t3722313464_0_0_0 = { 3, GenInst_Boolean_t97287965_0_0_0_Vector2_t2156229523_0_0_0_Vector3_t3722313464_0_0_0_Types };
extern const Il2CppType Vector3U5BU5D_t1718750761_0_0_0;
static const Il2CppType* GenInst_Vector3U5BU5D_t1718750761_0_0_0_Types[] = { &Vector3U5BU5D_t1718750761_0_0_0 };
extern const Il2CppGenericInst GenInst_Vector3U5BU5D_t1718750761_0_0_0 = { 1, GenInst_Vector3U5BU5D_t1718750761_0_0_0_Types };
extern const Il2CppType InstantTrackingState_t3973410783_0_0_0;
static const Il2CppType* GenInst_InstantTrackingState_t3973410783_0_0_0_Types[] = { &InstantTrackingState_t3973410783_0_0_0 };
extern const Il2CppGenericInst GenInst_InstantTrackingState_t3973410783_0_0_0 = { 1, GenInst_InstantTrackingState_t3973410783_0_0_0_Types };
extern const Il2CppType ObjectTarget_t688225044_0_0_0;
static const Il2CppType* GenInst_ObjectTarget_t688225044_0_0_0_Types[] = { &ObjectTarget_t688225044_0_0_0 };
extern const Il2CppGenericInst GenInst_ObjectTarget_t688225044_0_0_0 = { 1, GenInst_ObjectTarget_t688225044_0_0_0_Types };
extern const Il2CppType Frame_t1062777224_0_0_0;
static const Il2CppType* GenInst_Frame_t1062777224_0_0_0_Types[] = { &Frame_t1062777224_0_0_0 };
extern const Il2CppGenericInst GenInst_Frame_t1062777224_0_0_0 = { 1, GenInst_Frame_t1062777224_0_0_0_Types };
static const Il2CppType* GenInst_RecognizedTarget_t3529911668_0_0_0_GameObject_t1113636619_0_0_0_Types[] = { &RecognizedTarget_t3529911668_0_0_0, &GameObject_t1113636619_0_0_0 };
extern const Il2CppGenericInst GenInst_RecognizedTarget_t3529911668_0_0_0_GameObject_t1113636619_0_0_0 = { 2, GenInst_RecognizedTarget_t3529911668_0_0_0_GameObject_t1113636619_0_0_0_Types };
static const Il2CppType* GenInst_RecognizedTarget_t3529911668_0_0_0_GameObject_t1113636619_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types[] = { &RecognizedTarget_t3529911668_0_0_0, &GameObject_t1113636619_0_0_0, &DictionaryEntry_t3123975638_0_0_0 };
extern const Il2CppGenericInst GenInst_RecognizedTarget_t3529911668_0_0_0_GameObject_t1113636619_0_0_0_DictionaryEntry_t3123975638_0_0_0 = { 3, GenInst_RecognizedTarget_t3529911668_0_0_0_GameObject_t1113636619_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t831214590_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t831214590_0_0_0_Types[] = { &KeyValuePair_2_t831214590_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t831214590_0_0_0 = { 1, GenInst_KeyValuePair_2_t831214590_0_0_0_Types };
static const Il2CppType* GenInst_RecognizedTarget_t3529911668_0_0_0_GameObject_t1113636619_0_0_0_KeyValuePair_2_t831214590_0_0_0_Types[] = { &RecognizedTarget_t3529911668_0_0_0, &GameObject_t1113636619_0_0_0, &KeyValuePair_2_t831214590_0_0_0 };
extern const Il2CppGenericInst GenInst_RecognizedTarget_t3529911668_0_0_0_GameObject_t1113636619_0_0_0_KeyValuePair_2_t831214590_0_0_0 = { 3, GenInst_RecognizedTarget_t3529911668_0_0_0_GameObject_t1113636619_0_0_0_KeyValuePair_2_t831214590_0_0_0_Types };
static const Il2CppType* GenInst_RecognizedTarget_t3529911668_0_0_0_GameObject_t1113636619_0_0_0_GameObject_t1113636619_0_0_0_Types[] = { &RecognizedTarget_t3529911668_0_0_0, &GameObject_t1113636619_0_0_0, &GameObject_t1113636619_0_0_0 };
extern const Il2CppGenericInst GenInst_RecognizedTarget_t3529911668_0_0_0_GameObject_t1113636619_0_0_0_GameObject_t1113636619_0_0_0 = { 3, GenInst_RecognizedTarget_t3529911668_0_0_0_GameObject_t1113636619_0_0_0_GameObject_t1113636619_0_0_0_Types };
extern const Il2CppType TrackableBehaviour_t2469814643_0_0_0;
static const Il2CppType* GenInst_TrackableBehaviour_t2469814643_0_0_0_Types[] = { &TrackableBehaviour_t2469814643_0_0_0 };
extern const Il2CppGenericInst GenInst_TrackableBehaviour_t2469814643_0_0_0 = { 1, GenInst_TrackableBehaviour_t2469814643_0_0_0_Types };
extern const Il2CppType Link_t2598975452_0_0_0;
static const Il2CppType* GenInst_Link_t2598975452_0_0_0_Types[] = { &Link_t2598975452_0_0_0 };
extern const Il2CppGenericInst GenInst_Link_t2598975452_0_0_0 = { 1, GenInst_Link_t2598975452_0_0_0_Types };
extern const Il2CppType TargetCollectionResource_t66041399_0_0_0;
static const Il2CppType* GenInst_Int64_t3736567304_0_0_0_TargetCollectionResource_t66041399_0_0_0_Types[] = { &Int64_t3736567304_0_0_0, &TargetCollectionResource_t66041399_0_0_0 };
extern const Il2CppGenericInst GenInst_Int64_t3736567304_0_0_0_TargetCollectionResource_t66041399_0_0_0 = { 2, GenInst_Int64_t3736567304_0_0_0_TargetCollectionResource_t66041399_0_0_0_Types };
static const Il2CppType* GenInst_TargetCollectionResource_t66041399_0_0_0_Types[] = { &TargetCollectionResource_t66041399_0_0_0 };
extern const Il2CppGenericInst GenInst_TargetCollectionResource_t66041399_0_0_0 = { 1, GenInst_TargetCollectionResource_t66041399_0_0_0_Types };
extern const Il2CppType TargetSource_t2046190744_0_0_0;
static const Il2CppType* GenInst_TargetSource_t2046190744_0_0_0_Types[] = { &TargetSource_t2046190744_0_0_0 };
extern const Il2CppGenericInst GenInst_TargetSource_t2046190744_0_0_0 = { 1, GenInst_TargetSource_t2046190744_0_0_0_Types };
static const Il2CppType* GenInst_Int64_t3736567304_0_0_0_TargetCollectionResource_t66041399_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types[] = { &Int64_t3736567304_0_0_0, &TargetCollectionResource_t66041399_0_0_0, &DictionaryEntry_t3123975638_0_0_0 };
extern const Il2CppGenericInst GenInst_Int64_t3736567304_0_0_0_TargetCollectionResource_t66041399_0_0_0_DictionaryEntry_t3123975638_0_0_0 = { 3, GenInst_Int64_t3736567304_0_0_0_TargetCollectionResource_t66041399_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t3526353350_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t3526353350_0_0_0_Types[] = { &KeyValuePair_2_t3526353350_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3526353350_0_0_0 = { 1, GenInst_KeyValuePair_2_t3526353350_0_0_0_Types };
static const Il2CppType* GenInst_Int64_t3736567304_0_0_0_TargetCollectionResource_t66041399_0_0_0_KeyValuePair_2_t3526353350_0_0_0_Types[] = { &Int64_t3736567304_0_0_0, &TargetCollectionResource_t66041399_0_0_0, &KeyValuePair_2_t3526353350_0_0_0 };
extern const Il2CppGenericInst GenInst_Int64_t3736567304_0_0_0_TargetCollectionResource_t66041399_0_0_0_KeyValuePair_2_t3526353350_0_0_0 = { 3, GenInst_Int64_t3736567304_0_0_0_TargetCollectionResource_t66041399_0_0_0_KeyValuePair_2_t3526353350_0_0_0_Types };
static const Il2CppType* GenInst_Int64_t3736567304_0_0_0_TargetCollectionResource_t66041399_0_0_0_TargetCollectionResource_t66041399_0_0_0_Types[] = { &Int64_t3736567304_0_0_0, &TargetCollectionResource_t66041399_0_0_0, &TargetCollectionResource_t66041399_0_0_0 };
extern const Il2CppGenericInst GenInst_Int64_t3736567304_0_0_0_TargetCollectionResource_t66041399_0_0_0_TargetCollectionResource_t66041399_0_0_0 = { 3, GenInst_Int64_t3736567304_0_0_0_TargetCollectionResource_t66041399_0_0_0_TargetCollectionResource_t66041399_0_0_0_Types };
extern const Il2CppType CloudRecognitionService_t3866570042_0_0_0;
static const Il2CppType* GenInst_Int64_t3736567304_0_0_0_CloudRecognitionService_t3866570042_0_0_0_Types[] = { &Int64_t3736567304_0_0_0, &CloudRecognitionService_t3866570042_0_0_0 };
extern const Il2CppGenericInst GenInst_Int64_t3736567304_0_0_0_CloudRecognitionService_t3866570042_0_0_0 = { 2, GenInst_Int64_t3736567304_0_0_0_CloudRecognitionService_t3866570042_0_0_0_Types };
static const Il2CppType* GenInst_CloudRecognitionService_t3866570042_0_0_0_Types[] = { &CloudRecognitionService_t3866570042_0_0_0 };
extern const Il2CppGenericInst GenInst_CloudRecognitionService_t3866570042_0_0_0 = { 1, GenInst_CloudRecognitionService_t3866570042_0_0_0_Types };
static const Il2CppType* GenInst_Int64_t3736567304_0_0_0_CloudRecognitionService_t3866570042_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types[] = { &Int64_t3736567304_0_0_0, &CloudRecognitionService_t3866570042_0_0_0, &DictionaryEntry_t3123975638_0_0_0 };
extern const Il2CppGenericInst GenInst_Int64_t3736567304_0_0_0_CloudRecognitionService_t3866570042_0_0_0_DictionaryEntry_t3123975638_0_0_0 = { 3, GenInst_Int64_t3736567304_0_0_0_CloudRecognitionService_t3866570042_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t3031914697_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t3031914697_0_0_0_Types[] = { &KeyValuePair_2_t3031914697_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3031914697_0_0_0 = { 1, GenInst_KeyValuePair_2_t3031914697_0_0_0_Types };
static const Il2CppType* GenInst_Int64_t3736567304_0_0_0_CloudRecognitionService_t3866570042_0_0_0_KeyValuePair_2_t3031914697_0_0_0_Types[] = { &Int64_t3736567304_0_0_0, &CloudRecognitionService_t3866570042_0_0_0, &KeyValuePair_2_t3031914697_0_0_0 };
extern const Il2CppGenericInst GenInst_Int64_t3736567304_0_0_0_CloudRecognitionService_t3866570042_0_0_0_KeyValuePair_2_t3031914697_0_0_0 = { 3, GenInst_Int64_t3736567304_0_0_0_CloudRecognitionService_t3866570042_0_0_0_KeyValuePair_2_t3031914697_0_0_0_Types };
static const Il2CppType* GenInst_Int64_t3736567304_0_0_0_CloudRecognitionService_t3866570042_0_0_0_CloudRecognitionService_t3866570042_0_0_0_Types[] = { &Int64_t3736567304_0_0_0, &CloudRecognitionService_t3866570042_0_0_0, &CloudRecognitionService_t3866570042_0_0_0 };
extern const Il2CppGenericInst GenInst_Int64_t3736567304_0_0_0_CloudRecognitionService_t3866570042_0_0_0_CloudRecognitionService_t3866570042_0_0_0 = { 3, GenInst_Int64_t3736567304_0_0_0_CloudRecognitionService_t3866570042_0_0_0_CloudRecognitionService_t3866570042_0_0_0_Types };
extern const Il2CppType TrackerBehaviour_t921360922_0_0_0;
static const Il2CppType* GenInst_TrackerBehaviour_t921360922_0_0_0_Types[] = { &TrackerBehaviour_t921360922_0_0_0 };
extern const Il2CppGenericInst GenInst_TrackerBehaviour_t921360922_0_0_0 = { 1, GenInst_TrackerBehaviour_t921360922_0_0_0_Types };
extern const Il2CppType Link_t1050521731_0_0_0;
static const Il2CppType* GenInst_Link_t1050521731_0_0_0_Types[] = { &Link_t1050521731_0_0_0 };
extern const Il2CppGenericInst GenInst_Link_t1050521731_0_0_0 = { 1, GenInst_Link_t1050521731_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_String_t_0_0_0_Types[] = { &String_t_0_0_0, &String_t_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_String_t_0_0_0 = { 2, GenInst_String_t_0_0_0_String_t_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_String_t_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types[] = { &String_t_0_0_0, &String_t_0_0_0, &DictionaryEntry_t3123975638_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_String_t_0_0_0_DictionaryEntry_t3123975638_0_0_0 = { 3, GenInst_String_t_0_0_0_String_t_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t4030379155_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t4030379155_0_0_0_Types[] = { &KeyValuePair_2_t4030379155_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t4030379155_0_0_0 = { 1, GenInst_KeyValuePair_2_t4030379155_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_String_t_0_0_0_KeyValuePair_2_t4030379155_0_0_0_Types[] = { &String_t_0_0_0, &String_t_0_0_0, &KeyValuePair_2_t4030379155_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_String_t_0_0_0_KeyValuePair_2_t4030379155_0_0_0 = { 3, GenInst_String_t_0_0_0_String_t_0_0_0_KeyValuePair_2_t4030379155_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_String_t_0_0_0_String_t_0_0_0_Types[] = { &String_t_0_0_0, &String_t_0_0_0, &String_t_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_String_t_0_0_0_String_t_0_0_0 = { 3, GenInst_String_t_0_0_0_String_t_0_0_0_String_t_0_0_0_Types };
extern const Il2CppType CloudRecognitionServiceResponse_t1770101589_0_0_0;
static const Il2CppType* GenInst_CloudRecognitionServiceResponse_t1770101589_0_0_0_Types[] = { &CloudRecognitionServiceResponse_t1770101589_0_0_0 };
extern const Il2CppGenericInst GenInst_CloudRecognitionServiceResponse_t1770101589_0_0_0 = { 1, GenInst_CloudRecognitionServiceResponse_t1770101589_0_0_0_Types };
extern const Il2CppType CaptureDevicePosition_t3635123692_0_0_0;
static const Il2CppType* GenInst_CaptureDevicePosition_t3635123692_0_0_0_Types[] = { &CaptureDevicePosition_t3635123692_0_0_0 };
extern const Il2CppGenericInst GenInst_CaptureDevicePosition_t3635123692_0_0_0 = { 1, GenInst_CaptureDevicePosition_t3635123692_0_0_0_Types };
extern const Il2CppType Mode_t2127932823_0_0_0;
static const Il2CppType* GenInst_Mode_t2127932823_0_0_0_Types[] = { &Mode_t2127932823_0_0_0 };
extern const Il2CppGenericInst GenInst_Mode_t2127932823_0_0_0 = { 1, GenInst_Mode_t2127932823_0_0_0_Types };
extern const Il2CppType Highlighter_t672210414_0_0_0;
static const Il2CppType* GenInst_Highlighter_t672210414_0_0_0_Types[] = { &Highlighter_t672210414_0_0_0 };
extern const Il2CppGenericInst GenInst_Highlighter_t672210414_0_0_0 = { 1, GenInst_Highlighter_t672210414_0_0_0_Types };
extern const Il2CppType Link_t801371223_0_0_0;
static const Il2CppType* GenInst_Link_t801371223_0_0_0_Types[] = { &Link_t801371223_0_0_0 };
extern const Il2CppGenericInst GenInst_Link_t801371223_0_0_0 = { 1, GenInst_Link_t801371223_0_0_0_Types };
extern const Il2CppType HighlighterRenderer_t1776162451_0_0_0;
static const Il2CppType* GenInst_HighlighterRenderer_t1776162451_0_0_0_Types[] = { &HighlighterRenderer_t1776162451_0_0_0 };
extern const Il2CppGenericInst GenInst_HighlighterRenderer_t1776162451_0_0_0 = { 1, GenInst_HighlighterRenderer_t1776162451_0_0_0_Types };
extern const Il2CppType Renderer_t2627027031_0_0_0;
static const Il2CppType* GenInst_Renderer_t2627027031_0_0_0_Types[] = { &Renderer_t2627027031_0_0_0 };
extern const Il2CppGenericInst GenInst_Renderer_t2627027031_0_0_0 = { 1, GenInst_Renderer_t2627027031_0_0_0_Types };
extern const Il2CppType HighlightingBase_t582374880_0_0_0;
static const Il2CppType* GenInst_HighlightingBase_t582374880_0_0_0_Types[] = { &HighlightingBase_t582374880_0_0_0 };
extern const Il2CppGenericInst GenInst_HighlightingBase_t582374880_0_0_0 = { 1, GenInst_HighlightingBase_t582374880_0_0_0_Types };
extern const Il2CppType HighlightingPreset_t635619791_0_0_0;
static const Il2CppType* GenInst_HighlightingPreset_t635619791_0_0_0_Types[] = { &HighlightingPreset_t635619791_0_0_0 };
extern const Il2CppGenericInst GenInst_HighlightingPreset_t635619791_0_0_0 = { 1, GenInst_HighlightingPreset_t635619791_0_0_0_Types };
extern const Il2CppType Data_t434873072_0_0_0;
static const Il2CppType* GenInst_Data_t434873072_0_0_0_Types[] = { &Data_t434873072_0_0_0 };
extern const Il2CppGenericInst GenInst_Data_t434873072_0_0_0 = { 1, GenInst_Data_t434873072_0_0_0_Types };
extern const Il2CppType Shader_t4151988712_0_0_0;
static const Il2CppType* GenInst_Shader_t4151988712_0_0_0_Types[] = { &Shader_t4151988712_0_0_0 };
extern const Il2CppGenericInst GenInst_Shader_t4151988712_0_0_0 = { 1, GenInst_Shader_t4151988712_0_0_0_Types };
extern const Il2CppType Link_t4286314680_0_0_0;
static const Il2CppType* GenInst_Link_t4286314680_0_0_0_Types[] = { &Link_t4286314680_0_0_0 };
extern const Il2CppGenericInst GenInst_Link_t4286314680_0_0_0 = { 1, GenInst_Link_t4286314680_0_0_0_Types };
extern const Il2CppType Hashtable_t1853889766_0_0_0;
static const Il2CppType* GenInst_Hashtable_t1853889766_0_0_0_Types[] = { &Hashtable_t1853889766_0_0_0 };
extern const Il2CppGenericInst GenInst_Hashtable_t1853889766_0_0_0 = { 1, GenInst_Hashtable_t1853889766_0_0_0_Types };
extern const Il2CppType IDictionary_t1363984059_0_0_0;
static const Il2CppType* GenInst_IDictionary_t1363984059_0_0_0_Types[] = { &IDictionary_t1363984059_0_0_0 };
extern const Il2CppGenericInst GenInst_IDictionary_t1363984059_0_0_0 = { 1, GenInst_IDictionary_t1363984059_0_0_0_Types };
extern const Il2CppType Rect_t2360479859_0_0_0;
static const Il2CppType* GenInst_Rect_t2360479859_0_0_0_Types[] = { &Rect_t2360479859_0_0_0 };
extern const Il2CppGenericInst GenInst_Rect_t2360479859_0_0_0 = { 1, GenInst_Rect_t2360479859_0_0_0_Types };
extern const Il2CppType iTween_t770867771_0_0_0;
static const Il2CppType* GenInst_iTween_t770867771_0_0_0_Types[] = { &iTween_t770867771_0_0_0 };
extern const Il2CppGenericInst GenInst_iTween_t770867771_0_0_0 = { 1, GenInst_iTween_t770867771_0_0_0_Types };
extern const Il2CppType HighlighterItem_t659879438_0_0_0;
static const Il2CppType* GenInst_HighlighterItem_t659879438_0_0_0_Types[] = { &HighlighterItem_t659879438_0_0_0 };
extern const Il2CppGenericInst GenInst_HighlighterItem_t659879438_0_0_0 = { 1, GenInst_HighlighterItem_t659879438_0_0_0_Types };
extern const Il2CppType Link_t789040247_0_0_0;
static const Il2CppType* GenInst_Link_t789040247_0_0_0_Types[] = { &Link_t789040247_0_0_0 };
extern const Il2CppGenericInst GenInst_Link_t789040247_0_0_0 = { 1, GenInst_Link_t789040247_0_0_0_Types };
extern const Il2CppType IHighlightingTarget_t923341466_0_0_0;
static const Il2CppType* GenInst_IHighlightingTarget_t923341466_0_0_0_Types[] = { &IHighlightingTarget_t923341466_0_0_0 };
extern const Il2CppGenericInst GenInst_IHighlightingTarget_t923341466_0_0_0 = { 1, GenInst_IHighlightingTarget_t923341466_0_0_0_Types };
extern const Il2CppType Mesh_t3648964284_0_0_0;
static const Il2CppType* GenInst_Mesh_t3648964284_0_0_0_Types[] = { &Mesh_t3648964284_0_0_0 };
extern const Il2CppGenericInst GenInst_Mesh_t3648964284_0_0_0 = { 1, GenInst_Mesh_t3648964284_0_0_0_Types };
extern const Il2CppType KeyCode_t2599294277_0_0_0;
static const Il2CppType* GenInst_KeyCode_t2599294277_0_0_0_Types[] = { &KeyCode_t2599294277_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyCode_t2599294277_0_0_0 = { 1, GenInst_KeyCode_t2599294277_0_0_0_Types };
extern const Il2CppType JSONObject_t1339445639_0_0_0;
static const Il2CppType* GenInst_JSONObject_t1339445639_0_0_0_Types[] = { &JSONObject_t1339445639_0_0_0 };
extern const Il2CppGenericInst GenInst_JSONObject_t1339445639_0_0_0 = { 1, GenInst_JSONObject_t1339445639_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_JSONObject_t1339445639_0_0_0_Types[] = { &String_t_0_0_0, &JSONObject_t1339445639_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_JSONObject_t1339445639_0_0_0 = { 2, GenInst_String_t_0_0_0_JSONObject_t1339445639_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_JSONObject_t1339445639_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types[] = { &String_t_0_0_0, &JSONObject_t1339445639_0_0_0, &DictionaryEntry_t3123975638_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_JSONObject_t1339445639_0_0_0_DictionaryEntry_t3123975638_0_0_0 = { 3, GenInst_String_t_0_0_0_JSONObject_t1339445639_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t3522374105_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t3522374105_0_0_0_Types[] = { &KeyValuePair_2_t3522374105_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3522374105_0_0_0 = { 1, GenInst_KeyValuePair_2_t3522374105_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_JSONObject_t1339445639_0_0_0_KeyValuePair_2_t3522374105_0_0_0_Types[] = { &String_t_0_0_0, &JSONObject_t1339445639_0_0_0, &KeyValuePair_2_t3522374105_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_JSONObject_t1339445639_0_0_0_KeyValuePair_2_t3522374105_0_0_0 = { 3, GenInst_String_t_0_0_0_JSONObject_t1339445639_0_0_0_KeyValuePair_2_t3522374105_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_JSONObject_t1339445639_0_0_0_JSONObject_t1339445639_0_0_0_Types[] = { &String_t_0_0_0, &JSONObject_t1339445639_0_0_0, &JSONObject_t1339445639_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_JSONObject_t1339445639_0_0_0_JSONObject_t1339445639_0_0_0 = { 3, GenInst_String_t_0_0_0_JSONObject_t1339445639_0_0_0_JSONObject_t1339445639_0_0_0_Types };
extern const Il2CppType LeanFinger_t3506292858_0_0_0;
static const Il2CppType* GenInst_LeanFinger_t3506292858_0_0_0_Types[] = { &LeanFinger_t3506292858_0_0_0 };
extern const Il2CppGenericInst GenInst_LeanFinger_t3506292858_0_0_0 = { 1, GenInst_LeanFinger_t3506292858_0_0_0_Types };
extern const Il2CppType LeanFingerHeld_t1927732235_0_0_0;
static const Il2CppType* GenInst_LeanFingerHeld_t1927732235_0_0_0_Types[] = { &LeanFingerHeld_t1927732235_0_0_0 };
extern const Il2CppGenericInst GenInst_LeanFingerHeld_t1927732235_0_0_0 = { 1, GenInst_LeanFingerHeld_t1927732235_0_0_0_Types };
extern const Il2CppType Link_t1401011957_0_0_0;
static const Il2CppType* GenInst_Link_t1401011957_0_0_0_Types[] = { &Link_t1401011957_0_0_0 };
extern const Il2CppGenericInst GenInst_Link_t1401011957_0_0_0 = { 1, GenInst_Link_t1401011957_0_0_0_Types };
extern const Il2CppType Link_t4191133801_0_0_0;
static const Il2CppType* GenInst_Link_t4191133801_0_0_0_Types[] = { &Link_t4191133801_0_0_0 };
extern const Il2CppGenericInst GenInst_Link_t4191133801_0_0_0 = { 1, GenInst_Link_t4191133801_0_0_0_Types };
extern const Il2CppType LeanSnapshot_t4136513957_0_0_0;
static const Il2CppType* GenInst_LeanSnapshot_t4136513957_0_0_0_Types[] = { &LeanSnapshot_t4136513957_0_0_0 };
extern const Il2CppGenericInst GenInst_LeanSnapshot_t4136513957_0_0_0 = { 1, GenInst_LeanSnapshot_t4136513957_0_0_0_Types };
extern const Il2CppType LeanSelectable_t2178850769_0_0_0;
static const Il2CppType* GenInst_LeanSelectable_t2178850769_0_0_0_Types[] = { &LeanSelectable_t2178850769_0_0_0 };
extern const Il2CppGenericInst GenInst_LeanSelectable_t2178850769_0_0_0 = { 1, GenInst_LeanSelectable_t2178850769_0_0_0_Types };
extern const Il2CppType List_1_t683400304_0_0_0;
static const Il2CppType* GenInst_List_1_t683400304_0_0_0_Types[] = { &List_1_t683400304_0_0_0 };
extern const Il2CppGenericInst GenInst_List_1_t683400304_0_0_0 = { 1, GenInst_List_1_t683400304_0_0_0_Types };
extern const Il2CppType LeanTouch_t2951860335_0_0_0;
static const Il2CppType* GenInst_LeanTouch_t2951860335_0_0_0_Types[] = { &LeanTouch_t2951860335_0_0_0 };
extern const Il2CppGenericInst GenInst_LeanTouch_t2951860335_0_0_0 = { 1, GenInst_LeanTouch_t2951860335_0_0_0_Types };
extern const Il2CppType iTweenPath_t76106739_0_0_0;
static const Il2CppType* GenInst_String_t_0_0_0_iTweenPath_t76106739_0_0_0_Types[] = { &String_t_0_0_0, &iTweenPath_t76106739_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_iTweenPath_t76106739_0_0_0 = { 2, GenInst_String_t_0_0_0_iTweenPath_t76106739_0_0_0_Types };
static const Il2CppType* GenInst_iTweenPath_t76106739_0_0_0_Types[] = { &iTweenPath_t76106739_0_0_0 };
extern const Il2CppGenericInst GenInst_iTweenPath_t76106739_0_0_0 = { 1, GenInst_iTweenPath_t76106739_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_iTweenPath_t76106739_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types[] = { &String_t_0_0_0, &iTweenPath_t76106739_0_0_0, &DictionaryEntry_t3123975638_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_iTweenPath_t76106739_0_0_0_DictionaryEntry_t3123975638_0_0_0 = { 3, GenInst_String_t_0_0_0_iTweenPath_t76106739_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2259035205_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2259035205_0_0_0_Types[] = { &KeyValuePair_2_t2259035205_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2259035205_0_0_0 = { 1, GenInst_KeyValuePair_2_t2259035205_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_iTweenPath_t76106739_0_0_0_KeyValuePair_2_t2259035205_0_0_0_Types[] = { &String_t_0_0_0, &iTweenPath_t76106739_0_0_0, &KeyValuePair_2_t2259035205_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_iTweenPath_t76106739_0_0_0_KeyValuePair_2_t2259035205_0_0_0 = { 3, GenInst_String_t_0_0_0_iTweenPath_t76106739_0_0_0_KeyValuePair_2_t2259035205_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_iTweenPath_t76106739_0_0_0_iTweenPath_t76106739_0_0_0_Types[] = { &String_t_0_0_0, &iTweenPath_t76106739_0_0_0, &iTweenPath_t76106739_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_iTweenPath_t76106739_0_0_0_iTweenPath_t76106739_0_0_0 = { 3, GenInst_String_t_0_0_0_iTweenPath_t76106739_0_0_0_iTweenPath_t76106739_0_0_0_Types };
extern const Il2CppType Branch_t1428165248_0_0_0;
static const Il2CppType* GenInst_Branch_t1428165248_0_0_0_Types[] = { &Branch_t1428165248_0_0_0 };
extern const Il2CppGenericInst GenInst_Branch_t1428165248_0_0_0 = { 1, GenInst_Branch_t1428165248_0_0_0_Types };
extern const Il2CppType LightmapParams_t3719846058_0_0_0;
static const Il2CppType* GenInst_LightmapParams_t3719846058_0_0_0_Types[] = { &LightmapParams_t3719846058_0_0_0 };
extern const Il2CppGenericInst GenInst_LightmapParams_t3719846058_0_0_0 = { 1, GenInst_LightmapParams_t3719846058_0_0_0_Types };
extern const Il2CppType txtBtnModel_t2868369423_0_0_0;
static const Il2CppType* GenInst_txtBtnModel_t2868369423_0_0_0_Types[] = { &txtBtnModel_t2868369423_0_0_0 };
extern const Il2CppGenericInst GenInst_txtBtnModel_t2868369423_0_0_0 = { 1, GenInst_txtBtnModel_t2868369423_0_0_0_Types };
extern const Il2CppType SelectMesh_t688911915_0_0_0;
static const Il2CppType* GenInst_SelectMesh_t688911915_0_0_0_Types[] = { &SelectMesh_t688911915_0_0_0 };
extern const Il2CppGenericInst GenInst_SelectMesh_t688911915_0_0_0 = { 1, GenInst_SelectMesh_t688911915_0_0_0_Types };
extern const Il2CppType MenuItem_t3210774549_0_0_0;
static const Il2CppType* GenInst_MenuItem_t3210774549_0_0_0_Types[] = { &MenuItem_t3210774549_0_0_0 };
extern const Il2CppGenericInst GenInst_MenuItem_t3210774549_0_0_0 = { 1, GenInst_MenuItem_t3210774549_0_0_0_Types };
extern const Il2CppType MeshRenderer_t587009260_0_0_0;
static const Il2CppType* GenInst_MeshRenderer_t587009260_0_0_0_Types[] = { &MeshRenderer_t587009260_0_0_0 };
extern const Il2CppGenericInst GenInst_MeshRenderer_t587009260_0_0_0 = { 1, GenInst_MeshRenderer_t587009260_0_0_0_Types };
extern const Il2CppType TransparancyManager_t318855516_0_0_0;
static const Il2CppType* GenInst_TransparancyManager_t318855516_0_0_0_Types[] = { &TransparancyManager_t318855516_0_0_0 };
extern const Il2CppGenericInst GenInst_TransparancyManager_t318855516_0_0_0 = { 1, GenInst_TransparancyManager_t318855516_0_0_0_Types };
extern const Il2CppType Link_t1242797428_0_0_0;
static const Il2CppType* GenInst_Link_t1242797428_0_0_0_Types[] = { &Link_t1242797428_0_0_0 };
extern const Il2CppGenericInst GenInst_Link_t1242797428_0_0_0 = { 1, GenInst_Link_t1242797428_0_0_0_Types };
static const Il2CppType* GenInst_Char_t3634460470_0_0_0_Int32_t2950945753_0_0_0_Types[] = { &Char_t3634460470_0_0_0, &Int32_t2950945753_0_0_0 };
extern const Il2CppGenericInst GenInst_Char_t3634460470_0_0_0_Int32_t2950945753_0_0_0 = { 2, GenInst_Char_t3634460470_0_0_0_Int32_t2950945753_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_String_t_0_0_0_Int32_t2950945753_0_0_0_Types[] = { &String_t_0_0_0, &String_t_0_0_0, &Int32_t2950945753_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_String_t_0_0_0_Int32_t2950945753_0_0_0 = { 3, GenInst_String_t_0_0_0_String_t_0_0_0_Int32_t2950945753_0_0_0_Types };
static const Il2CppType* GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Int32_t2950945753_0_0_0_Types[] = { &Il2CppObject_0_0_0, &Il2CppObject_0_0_0, &Int32_t2950945753_0_0_0 };
extern const Il2CppGenericInst GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Int32_t2950945753_0_0_0 = { 3, GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Int32_t2950945753_0_0_0_Types };
extern const Il2CppType VertexAnim_t2231884842_0_0_0;
static const Il2CppType* GenInst_VertexAnim_t2231884842_0_0_0_Types[] = { &VertexAnim_t2231884842_0_0_0 };
extern const Il2CppGenericInst GenInst_VertexAnim_t2231884842_0_0_0 = { 1, GenInst_VertexAnim_t2231884842_0_0_0_Types };
extern const Il2CppType IList_1_t1242665951_0_0_0;
static const Il2CppType* GenInst_IList_1_t1242665951_0_0_0_Types[] = { &IList_1_t1242665951_0_0_0 };
extern const Il2CppGenericInst GenInst_IList_1_t1242665951_0_0_0 = { 1, GenInst_IList_1_t1242665951_0_0_0_Types };
extern const Il2CppType ICollection_1_t2255498402_0_0_0;
static const Il2CppType* GenInst_ICollection_1_t2255498402_0_0_0_Types[] = { &ICollection_1_t2255498402_0_0_0 };
extern const Il2CppGenericInst GenInst_ICollection_1_t2255498402_0_0_0 = { 1, GenInst_ICollection_1_t2255498402_0_0_0_Types };
extern const Il2CppType IEnumerable_1_t2702166353_0_0_0;
static const Il2CppType* GenInst_IEnumerable_1_t2702166353_0_0_0_Types[] = { &IEnumerable_1_t2702166353_0_0_0 };
extern const Il2CppGenericInst GenInst_IEnumerable_1_t2702166353_0_0_0 = { 1, GenInst_IEnumerable_1_t2702166353_0_0_0_Types };
extern const Il2CppType InputFrameData_t2573966900_0_0_0;
static const Il2CppType* GenInst_InputFrameData_t2573966900_0_0_0_Types[] = { &InputFrameData_t2573966900_0_0_0 };
extern const Il2CppGenericInst GenInst_InputFrameData_t2573966900_0_0_0 = { 1, GenInst_InputFrameData_t2573966900_0_0_0_Types };
extern const Il2CppType Button_t4055032469_0_0_0;
static const Il2CppType* GenInst_Button_t4055032469_0_0_0_Types[] = { &Button_t4055032469_0_0_0 };
extern const Il2CppGenericInst GenInst_Button_t4055032469_0_0_0 = { 1, GenInst_Button_t4055032469_0_0_0_Types };
extern const Il2CppType Dinosaur_t1302057126_0_0_0;
static const Il2CppType* GenInst_Dinosaur_t1302057126_0_0_0_Types[] = { &Dinosaur_t1302057126_0_0_0 };
extern const Il2CppGenericInst GenInst_Dinosaur_t1302057126_0_0_0 = { 1, GenInst_Dinosaur_t1302057126_0_0_0_Types };
extern const Il2CppType Link_t1431217935_0_0_0;
static const Il2CppType* GenInst_Link_t1431217935_0_0_0_Types[] = { &Link_t1431217935_0_0_0 };
extern const Il2CppGenericInst GenInst_Link_t1431217935_0_0_0 = { 1, GenInst_Link_t1431217935_0_0_0_Types };
extern const Il2CppType IEnumerable_1_t1615002100_gp_0_0_0_0;
static const Il2CppType* GenInst_IEnumerable_1_t1615002100_gp_0_0_0_0_Types[] = { &IEnumerable_1_t1615002100_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_IEnumerable_1_t1615002100_gp_0_0_0_0 = { 1, GenInst_IEnumerable_1_t1615002100_gp_0_0_0_0_Types };
extern const Il2CppType Array_InternalArray__IEnumerable_GetEnumerator_m2877758246_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_InternalArray__IEnumerable_GetEnumerator_m2877758246_gp_0_0_0_0_Types[] = { &Array_InternalArray__IEnumerable_GetEnumerator_m2877758246_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_InternalArray__IEnumerable_GetEnumerator_m2877758246_gp_0_0_0_0 = { 1, GenInst_Array_InternalArray__IEnumerable_GetEnumerator_m2877758246_gp_0_0_0_0_Types };
extern const Il2CppType Array_Sort_m3544654705_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_Sort_m3544654705_gp_0_0_0_0_Array_Sort_m3544654705_gp_0_0_0_0_Types[] = { &Array_Sort_m3544654705_gp_0_0_0_0, &Array_Sort_m3544654705_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m3544654705_gp_0_0_0_0_Array_Sort_m3544654705_gp_0_0_0_0 = { 2, GenInst_Array_Sort_m3544654705_gp_0_0_0_0_Array_Sort_m3544654705_gp_0_0_0_0_Types };
extern const Il2CppType Array_Sort_m2813331573_gp_0_0_0_0;
extern const Il2CppType Array_Sort_m2813331573_gp_1_0_0_0;
static const Il2CppType* GenInst_Array_Sort_m2813331573_gp_0_0_0_0_Array_Sort_m2813331573_gp_1_0_0_0_Types[] = { &Array_Sort_m2813331573_gp_0_0_0_0, &Array_Sort_m2813331573_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m2813331573_gp_0_0_0_0_Array_Sort_m2813331573_gp_1_0_0_0 = { 2, GenInst_Array_Sort_m2813331573_gp_0_0_0_0_Array_Sort_m2813331573_gp_1_0_0_0_Types };
extern const Il2CppType Array_Sort_m541372433_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_Sort_m541372433_gp_0_0_0_0_Types[] = { &Array_Sort_m541372433_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m541372433_gp_0_0_0_0 = { 1, GenInst_Array_Sort_m541372433_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_Array_Sort_m541372433_gp_0_0_0_0_Array_Sort_m541372433_gp_0_0_0_0_Types[] = { &Array_Sort_m541372433_gp_0_0_0_0, &Array_Sort_m541372433_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m541372433_gp_0_0_0_0_Array_Sort_m541372433_gp_0_0_0_0 = { 2, GenInst_Array_Sort_m541372433_gp_0_0_0_0_Array_Sort_m541372433_gp_0_0_0_0_Types };
extern const Il2CppType Array_Sort_m3128382015_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_Sort_m3128382015_gp_0_0_0_0_Types[] = { &Array_Sort_m3128382015_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m3128382015_gp_0_0_0_0 = { 1, GenInst_Array_Sort_m3128382015_gp_0_0_0_0_Types };
extern const Il2CppType Array_Sort_m3128382015_gp_1_0_0_0;
static const Il2CppType* GenInst_Array_Sort_m3128382015_gp_0_0_0_0_Array_Sort_m3128382015_gp_1_0_0_0_Types[] = { &Array_Sort_m3128382015_gp_0_0_0_0, &Array_Sort_m3128382015_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m3128382015_gp_0_0_0_0_Array_Sort_m3128382015_gp_1_0_0_0 = { 2, GenInst_Array_Sort_m3128382015_gp_0_0_0_0_Array_Sort_m3128382015_gp_1_0_0_0_Types };
extern const Il2CppType Array_Sort_m2971965616_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_Sort_m2971965616_gp_0_0_0_0_Array_Sort_m2971965616_gp_0_0_0_0_Types[] = { &Array_Sort_m2971965616_gp_0_0_0_0, &Array_Sort_m2971965616_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m2971965616_gp_0_0_0_0_Array_Sort_m2971965616_gp_0_0_0_0 = { 2, GenInst_Array_Sort_m2971965616_gp_0_0_0_0_Array_Sort_m2971965616_gp_0_0_0_0_Types };
extern const Il2CppType Array_Sort_m2294228957_gp_0_0_0_0;
extern const Il2CppType Array_Sort_m2294228957_gp_1_0_0_0;
static const Il2CppType* GenInst_Array_Sort_m2294228957_gp_0_0_0_0_Array_Sort_m2294228957_gp_1_0_0_0_Types[] = { &Array_Sort_m2294228957_gp_0_0_0_0, &Array_Sort_m2294228957_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m2294228957_gp_0_0_0_0_Array_Sort_m2294228957_gp_1_0_0_0 = { 2, GenInst_Array_Sort_m2294228957_gp_0_0_0_0_Array_Sort_m2294228957_gp_1_0_0_0_Types };
extern const Il2CppType Array_Sort_m3111895514_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_Sort_m3111895514_gp_0_0_0_0_Types[] = { &Array_Sort_m3111895514_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m3111895514_gp_0_0_0_0 = { 1, GenInst_Array_Sort_m3111895514_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_Array_Sort_m3111895514_gp_0_0_0_0_Array_Sort_m3111895514_gp_0_0_0_0_Types[] = { &Array_Sort_m3111895514_gp_0_0_0_0, &Array_Sort_m3111895514_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m3111895514_gp_0_0_0_0_Array_Sort_m3111895514_gp_0_0_0_0 = { 2, GenInst_Array_Sort_m3111895514_gp_0_0_0_0_Array_Sort_m3111895514_gp_0_0_0_0_Types };
extern const Il2CppType Array_Sort_m4162554849_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_Sort_m4162554849_gp_0_0_0_0_Types[] = { &Array_Sort_m4162554849_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m4162554849_gp_0_0_0_0 = { 1, GenInst_Array_Sort_m4162554849_gp_0_0_0_0_Types };
extern const Il2CppType Array_Sort_m4162554849_gp_1_0_0_0;
static const Il2CppType* GenInst_Array_Sort_m4162554849_gp_1_0_0_0_Types[] = { &Array_Sort_m4162554849_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m4162554849_gp_1_0_0_0 = { 1, GenInst_Array_Sort_m4162554849_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_Array_Sort_m4162554849_gp_0_0_0_0_Array_Sort_m4162554849_gp_1_0_0_0_Types[] = { &Array_Sort_m4162554849_gp_0_0_0_0, &Array_Sort_m4162554849_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m4162554849_gp_0_0_0_0_Array_Sort_m4162554849_gp_1_0_0_0 = { 2, GenInst_Array_Sort_m4162554849_gp_0_0_0_0_Array_Sort_m4162554849_gp_1_0_0_0_Types };
extern const Il2CppType Array_Sort_m2347552592_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_Sort_m2347552592_gp_0_0_0_0_Types[] = { &Array_Sort_m2347552592_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m2347552592_gp_0_0_0_0 = { 1, GenInst_Array_Sort_m2347552592_gp_0_0_0_0_Types };
extern const Il2CppType Array_Sort_m1584977544_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_Sort_m1584977544_gp_0_0_0_0_Types[] = { &Array_Sort_m1584977544_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m1584977544_gp_0_0_0_0 = { 1, GenInst_Array_Sort_m1584977544_gp_0_0_0_0_Types };
extern const Il2CppType Array_qsort_m2730016035_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_qsort_m2730016035_gp_0_0_0_0_Types[] = { &Array_qsort_m2730016035_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_qsort_m2730016035_gp_0_0_0_0 = { 1, GenInst_Array_qsort_m2730016035_gp_0_0_0_0_Types };
extern const Il2CppType Array_qsort_m2730016035_gp_1_0_0_0;
static const Il2CppType* GenInst_Array_qsort_m2730016035_gp_0_0_0_0_Array_qsort_m2730016035_gp_1_0_0_0_Types[] = { &Array_qsort_m2730016035_gp_0_0_0_0, &Array_qsort_m2730016035_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_qsort_m2730016035_gp_0_0_0_0_Array_qsort_m2730016035_gp_1_0_0_0 = { 2, GenInst_Array_qsort_m2730016035_gp_0_0_0_0_Array_qsort_m2730016035_gp_1_0_0_0_Types };
extern const Il2CppType Array_compare_m1571452883_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_compare_m1571452883_gp_0_0_0_0_Types[] = { &Array_compare_m1571452883_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_compare_m1571452883_gp_0_0_0_0 = { 1, GenInst_Array_compare_m1571452883_gp_0_0_0_0_Types };
extern const Il2CppType Array_qsort_m2056624725_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_qsort_m2056624725_gp_0_0_0_0_Types[] = { &Array_qsort_m2056624725_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_qsort_m2056624725_gp_0_0_0_0 = { 1, GenInst_Array_qsort_m2056624725_gp_0_0_0_0_Types };
extern const Il2CppType Array_Resize_m148240358_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_Resize_m148240358_gp_0_0_0_0_Types[] = { &Array_Resize_m148240358_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Resize_m148240358_gp_0_0_0_0 = { 1, GenInst_Array_Resize_m148240358_gp_0_0_0_0_Types };
extern const Il2CppType Array_TrueForAll_m2898582490_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_TrueForAll_m2898582490_gp_0_0_0_0_Types[] = { &Array_TrueForAll_m2898582490_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_TrueForAll_m2898582490_gp_0_0_0_0 = { 1, GenInst_Array_TrueForAll_m2898582490_gp_0_0_0_0_Types };
extern const Il2CppType Array_ForEach_m1810936836_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_ForEach_m1810936836_gp_0_0_0_0_Types[] = { &Array_ForEach_m1810936836_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_ForEach_m1810936836_gp_0_0_0_0 = { 1, GenInst_Array_ForEach_m1810936836_gp_0_0_0_0_Types };
extern const Il2CppType Array_ConvertAll_m3156307665_gp_0_0_0_0;
extern const Il2CppType Array_ConvertAll_m3156307665_gp_1_0_0_0;
static const Il2CppType* GenInst_Array_ConvertAll_m3156307665_gp_0_0_0_0_Array_ConvertAll_m3156307665_gp_1_0_0_0_Types[] = { &Array_ConvertAll_m3156307665_gp_0_0_0_0, &Array_ConvertAll_m3156307665_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_ConvertAll_m3156307665_gp_0_0_0_0_Array_ConvertAll_m3156307665_gp_1_0_0_0 = { 2, GenInst_Array_ConvertAll_m3156307665_gp_0_0_0_0_Array_ConvertAll_m3156307665_gp_1_0_0_0_Types };
extern const Il2CppType Array_FindLastIndex_m314143680_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_FindLastIndex_m314143680_gp_0_0_0_0_Types[] = { &Array_FindLastIndex_m314143680_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_FindLastIndex_m314143680_gp_0_0_0_0 = { 1, GenInst_Array_FindLastIndex_m314143680_gp_0_0_0_0_Types };
extern const Il2CppType Array_FindLastIndex_m460178338_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_FindLastIndex_m460178338_gp_0_0_0_0_Types[] = { &Array_FindLastIndex_m460178338_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_FindLastIndex_m460178338_gp_0_0_0_0 = { 1, GenInst_Array_FindLastIndex_m460178338_gp_0_0_0_0_Types };
extern const Il2CppType Array_FindLastIndex_m4020594960_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_FindLastIndex_m4020594960_gp_0_0_0_0_Types[] = { &Array_FindLastIndex_m4020594960_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_FindLastIndex_m4020594960_gp_0_0_0_0 = { 1, GenInst_Array_FindLastIndex_m4020594960_gp_0_0_0_0_Types };
extern const Il2CppType Array_FindIndex_m2045802321_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_FindIndex_m2045802321_gp_0_0_0_0_Types[] = { &Array_FindIndex_m2045802321_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_FindIndex_m2045802321_gp_0_0_0_0 = { 1, GenInst_Array_FindIndex_m2045802321_gp_0_0_0_0_Types };
extern const Il2CppType Array_FindIndex_m193711900_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_FindIndex_m193711900_gp_0_0_0_0_Types[] = { &Array_FindIndex_m193711900_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_FindIndex_m193711900_gp_0_0_0_0 = { 1, GenInst_Array_FindIndex_m193711900_gp_0_0_0_0_Types };
extern const Il2CppType Array_FindIndex_m3007915586_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_FindIndex_m3007915586_gp_0_0_0_0_Types[] = { &Array_FindIndex_m3007915586_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_FindIndex_m3007915586_gp_0_0_0_0 = { 1, GenInst_Array_FindIndex_m3007915586_gp_0_0_0_0_Types };
extern const Il2CppType Array_BinarySearch_m588894935_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_BinarySearch_m588894935_gp_0_0_0_0_Types[] = { &Array_BinarySearch_m588894935_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_BinarySearch_m588894935_gp_0_0_0_0 = { 1, GenInst_Array_BinarySearch_m588894935_gp_0_0_0_0_Types };
extern const Il2CppType Array_BinarySearch_m1820506567_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_BinarySearch_m1820506567_gp_0_0_0_0_Types[] = { &Array_BinarySearch_m1820506567_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_BinarySearch_m1820506567_gp_0_0_0_0 = { 1, GenInst_Array_BinarySearch_m1820506567_gp_0_0_0_0_Types };
extern const Il2CppType Array_BinarySearch_m818786257_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_BinarySearch_m818786257_gp_0_0_0_0_Types[] = { &Array_BinarySearch_m818786257_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_BinarySearch_m818786257_gp_0_0_0_0 = { 1, GenInst_Array_BinarySearch_m818786257_gp_0_0_0_0_Types };
extern const Il2CppType Array_BinarySearch_m1137121163_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_BinarySearch_m1137121163_gp_0_0_0_0_Types[] = { &Array_BinarySearch_m1137121163_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_BinarySearch_m1137121163_gp_0_0_0_0 = { 1, GenInst_Array_BinarySearch_m1137121163_gp_0_0_0_0_Types };
extern const Il2CppType Array_IndexOf_m3665873194_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_IndexOf_m3665873194_gp_0_0_0_0_Types[] = { &Array_IndexOf_m3665873194_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_IndexOf_m3665873194_gp_0_0_0_0 = { 1, GenInst_Array_IndexOf_m3665873194_gp_0_0_0_0_Types };
extern const Il2CppType Array_IndexOf_m751114778_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_IndexOf_m751114778_gp_0_0_0_0_Types[] = { &Array_IndexOf_m751114778_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_IndexOf_m751114778_gp_0_0_0_0 = { 1, GenInst_Array_IndexOf_m751114778_gp_0_0_0_0_Types };
extern const Il2CppType Array_IndexOf_m1878557700_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_IndexOf_m1878557700_gp_0_0_0_0_Types[] = { &Array_IndexOf_m1878557700_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_IndexOf_m1878557700_gp_0_0_0_0 = { 1, GenInst_Array_IndexOf_m1878557700_gp_0_0_0_0_Types };
extern const Il2CppType Array_LastIndexOf_m1660745251_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_LastIndexOf_m1660745251_gp_0_0_0_0_Types[] = { &Array_LastIndexOf_m1660745251_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_LastIndexOf_m1660745251_gp_0_0_0_0 = { 1, GenInst_Array_LastIndexOf_m1660745251_gp_0_0_0_0_Types };
extern const Il2CppType Array_LastIndexOf_m3843535007_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_LastIndexOf_m3843535007_gp_0_0_0_0_Types[] = { &Array_LastIndexOf_m3843535007_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_LastIndexOf_m3843535007_gp_0_0_0_0 = { 1, GenInst_Array_LastIndexOf_m3843535007_gp_0_0_0_0_Types };
extern const Il2CppType Array_LastIndexOf_m3595565626_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_LastIndexOf_m3595565626_gp_0_0_0_0_Types[] = { &Array_LastIndexOf_m3595565626_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_LastIndexOf_m3595565626_gp_0_0_0_0 = { 1, GenInst_Array_LastIndexOf_m3595565626_gp_0_0_0_0_Types };
extern const Il2CppType Array_FindAll_m1182922648_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_FindAll_m1182922648_gp_0_0_0_0_Types[] = { &Array_FindAll_m1182922648_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_FindAll_m1182922648_gp_0_0_0_0 = { 1, GenInst_Array_FindAll_m1182922648_gp_0_0_0_0_Types };
extern const Il2CppType Array_Exists_m1131952887_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_Exists_m1131952887_gp_0_0_0_0_Types[] = { &Array_Exists_m1131952887_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Exists_m1131952887_gp_0_0_0_0 = { 1, GenInst_Array_Exists_m1131952887_gp_0_0_0_0_Types };
extern const Il2CppType Array_AsReadOnly_m1239764914_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_AsReadOnly_m1239764914_gp_0_0_0_0_Types[] = { &Array_AsReadOnly_m1239764914_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_AsReadOnly_m1239764914_gp_0_0_0_0 = { 1, GenInst_Array_AsReadOnly_m1239764914_gp_0_0_0_0_Types };
extern const Il2CppType Array_Find_m143990730_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_Find_m143990730_gp_0_0_0_0_Types[] = { &Array_Find_m143990730_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Find_m143990730_gp_0_0_0_0 = { 1, GenInst_Array_Find_m143990730_gp_0_0_0_0_Types };
extern const Il2CppType Array_FindLast_m1741869030_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_FindLast_m1741869030_gp_0_0_0_0_Types[] = { &Array_FindLast_m1741869030_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_FindLast_m1741869030_gp_0_0_0_0 = { 1, GenInst_Array_FindLast_m1741869030_gp_0_0_0_0_Types };
extern const Il2CppType InternalEnumerator_1_t2600413744_gp_0_0_0_0;
static const Il2CppType* GenInst_InternalEnumerator_1_t2600413744_gp_0_0_0_0_Types[] = { &InternalEnumerator_1_t2600413744_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_InternalEnumerator_1_t2600413744_gp_0_0_0_0 = { 1, GenInst_InternalEnumerator_1_t2600413744_gp_0_0_0_0_Types };
extern const Il2CppType ArrayReadOnlyList_1_t221793636_gp_0_0_0_0;
static const Il2CppType* GenInst_ArrayReadOnlyList_1_t221793636_gp_0_0_0_0_Types[] = { &ArrayReadOnlyList_1_t221793636_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ArrayReadOnlyList_1_t221793636_gp_0_0_0_0 = { 1, GenInst_ArrayReadOnlyList_1_t221793636_gp_0_0_0_0_Types };
extern const Il2CppType U3CGetEnumeratorU3Ec__Iterator0_t1202911786_gp_0_0_0_0;
static const Il2CppType* GenInst_U3CGetEnumeratorU3Ec__Iterator0_t1202911786_gp_0_0_0_0_Types[] = { &U3CGetEnumeratorU3Ec__Iterator0_t1202911786_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CGetEnumeratorU3Ec__Iterator0_t1202911786_gp_0_0_0_0 = { 1, GenInst_U3CGetEnumeratorU3Ec__Iterator0_t1202911786_gp_0_0_0_0_Types };
extern const Il2CppType IList_1_t523203890_gp_0_0_0_0;
static const Il2CppType* GenInst_IList_1_t523203890_gp_0_0_0_0_Types[] = { &IList_1_t523203890_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_IList_1_t523203890_gp_0_0_0_0 = { 1, GenInst_IList_1_t523203890_gp_0_0_0_0_Types };
extern const Il2CppType ICollection_1_t1449021101_gp_0_0_0_0;
static const Il2CppType* GenInst_ICollection_1_t1449021101_gp_0_0_0_0_Types[] = { &ICollection_1_t1449021101_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ICollection_1_t1449021101_gp_0_0_0_0 = { 1, GenInst_ICollection_1_t1449021101_gp_0_0_0_0_Types };
extern const Il2CppType Nullable_1_t3772285925_gp_0_0_0_0;
static const Il2CppType* GenInst_Nullable_1_t3772285925_gp_0_0_0_0_Types[] = { &Nullable_1_t3772285925_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Nullable_1_t3772285925_gp_0_0_0_0 = { 1, GenInst_Nullable_1_t3772285925_gp_0_0_0_0_Types };
extern const Il2CppType Comparer_1_t4245720645_gp_0_0_0_0;
static const Il2CppType* GenInst_Comparer_1_t4245720645_gp_0_0_0_0_Types[] = { &Comparer_1_t4245720645_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Comparer_1_t4245720645_gp_0_0_0_0 = { 1, GenInst_Comparer_1_t4245720645_gp_0_0_0_0_Types };
extern const Il2CppType DefaultComparer_t3277344064_gp_0_0_0_0;
static const Il2CppType* GenInst_DefaultComparer_t3277344064_gp_0_0_0_0_Types[] = { &DefaultComparer_t3277344064_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_DefaultComparer_t3277344064_gp_0_0_0_0 = { 1, GenInst_DefaultComparer_t3277344064_gp_0_0_0_0_Types };
extern const Il2CppType GenericComparer_1_t3581574675_gp_0_0_0_0;
static const Il2CppType* GenInst_GenericComparer_1_t3581574675_gp_0_0_0_0_Types[] = { &GenericComparer_1_t3581574675_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_GenericComparer_1_t3581574675_gp_0_0_0_0 = { 1, GenInst_GenericComparer_1_t3581574675_gp_0_0_0_0_Types };
extern const Il2CppType Dictionary_2_t3621973219_gp_0_0_0_0;
static const Il2CppType* GenInst_Dictionary_2_t3621973219_gp_0_0_0_0_Types[] = { &Dictionary_2_t3621973219_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Dictionary_2_t3621973219_gp_0_0_0_0 = { 1, GenInst_Dictionary_2_t3621973219_gp_0_0_0_0_Types };
extern const Il2CppType Dictionary_2_t3621973219_gp_1_0_0_0;
static const Il2CppType* GenInst_Dictionary_2_t3621973219_gp_0_0_0_0_Dictionary_2_t3621973219_gp_1_0_0_0_Types[] = { &Dictionary_2_t3621973219_gp_0_0_0_0, &Dictionary_2_t3621973219_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Dictionary_2_t3621973219_gp_0_0_0_0_Dictionary_2_t3621973219_gp_1_0_0_0 = { 2, GenInst_Dictionary_2_t3621973219_gp_0_0_0_0_Dictionary_2_t3621973219_gp_1_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1772072192_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1772072192_0_0_0_Types[] = { &KeyValuePair_2_t1772072192_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1772072192_0_0_0 = { 1, GenInst_KeyValuePair_2_t1772072192_0_0_0_Types };
extern const Il2CppType Dictionary_2_Do_CopyTo_m966111031_gp_0_0_0_0;
static const Il2CppType* GenInst_Dictionary_2_t3621973219_gp_0_0_0_0_Dictionary_2_t3621973219_gp_1_0_0_0_Dictionary_2_Do_CopyTo_m966111031_gp_0_0_0_0_Types[] = { &Dictionary_2_t3621973219_gp_0_0_0_0, &Dictionary_2_t3621973219_gp_1_0_0_0, &Dictionary_2_Do_CopyTo_m966111031_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Dictionary_2_t3621973219_gp_0_0_0_0_Dictionary_2_t3621973219_gp_1_0_0_0_Dictionary_2_Do_CopyTo_m966111031_gp_0_0_0_0 = { 3, GenInst_Dictionary_2_t3621973219_gp_0_0_0_0_Dictionary_2_t3621973219_gp_1_0_0_0_Dictionary_2_Do_CopyTo_m966111031_gp_0_0_0_0_Types };
extern const Il2CppType Dictionary_2_Do_ICollectionCopyTo_m842166809_gp_0_0_0_0;
static const Il2CppType* GenInst_Dictionary_2_t3621973219_gp_0_0_0_0_Dictionary_2_t3621973219_gp_1_0_0_0_Dictionary_2_Do_ICollectionCopyTo_m842166809_gp_0_0_0_0_Types[] = { &Dictionary_2_t3621973219_gp_0_0_0_0, &Dictionary_2_t3621973219_gp_1_0_0_0, &Dictionary_2_Do_ICollectionCopyTo_m842166809_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Dictionary_2_t3621973219_gp_0_0_0_0_Dictionary_2_t3621973219_gp_1_0_0_0_Dictionary_2_Do_ICollectionCopyTo_m842166809_gp_0_0_0_0 = { 3, GenInst_Dictionary_2_t3621973219_gp_0_0_0_0_Dictionary_2_t3621973219_gp_1_0_0_0_Dictionary_2_Do_ICollectionCopyTo_m842166809_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_Dictionary_2_Do_ICollectionCopyTo_m842166809_gp_0_0_0_0_Il2CppObject_0_0_0_Types[] = { &Dictionary_2_Do_ICollectionCopyTo_m842166809_gp_0_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_Dictionary_2_Do_ICollectionCopyTo_m842166809_gp_0_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_Dictionary_2_Do_ICollectionCopyTo_m842166809_gp_0_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_Dictionary_2_t3621973219_gp_0_0_0_0_Dictionary_2_t3621973219_gp_1_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types[] = { &Dictionary_2_t3621973219_gp_0_0_0_0, &Dictionary_2_t3621973219_gp_1_0_0_0, &DictionaryEntry_t3123975638_0_0_0 };
extern const Il2CppGenericInst GenInst_Dictionary_2_t3621973219_gp_0_0_0_0_Dictionary_2_t3621973219_gp_1_0_0_0_DictionaryEntry_t3123975638_0_0_0 = { 3, GenInst_Dictionary_2_t3621973219_gp_0_0_0_0_Dictionary_2_t3621973219_gp_1_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types };
extern const Il2CppType ShimEnumerator_t3154898978_gp_0_0_0_0;
extern const Il2CppType ShimEnumerator_t3154898978_gp_1_0_0_0;
static const Il2CppType* GenInst_ShimEnumerator_t3154898978_gp_0_0_0_0_ShimEnumerator_t3154898978_gp_1_0_0_0_Types[] = { &ShimEnumerator_t3154898978_gp_0_0_0_0, &ShimEnumerator_t3154898978_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_ShimEnumerator_t3154898978_gp_0_0_0_0_ShimEnumerator_t3154898978_gp_1_0_0_0 = { 2, GenInst_ShimEnumerator_t3154898978_gp_0_0_0_0_ShimEnumerator_t3154898978_gp_1_0_0_0_Types };
extern const Il2CppType Enumerator_t135598976_gp_0_0_0_0;
extern const Il2CppType Enumerator_t135598976_gp_1_0_0_0;
static const Il2CppType* GenInst_Enumerator_t135598976_gp_0_0_0_0_Enumerator_t135598976_gp_1_0_0_0_Types[] = { &Enumerator_t135598976_gp_0_0_0_0, &Enumerator_t135598976_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerator_t135598976_gp_0_0_0_0_Enumerator_t135598976_gp_1_0_0_0 = { 2, GenInst_Enumerator_t135598976_gp_0_0_0_0_Enumerator_t135598976_gp_1_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1920611820_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1920611820_0_0_0_Types[] = { &KeyValuePair_2_t1920611820_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1920611820_0_0_0 = { 1, GenInst_KeyValuePair_2_t1920611820_0_0_0_Types };
extern const Il2CppType ValueCollection_t2327722797_gp_0_0_0_0;
extern const Il2CppType ValueCollection_t2327722797_gp_1_0_0_0;
static const Il2CppType* GenInst_ValueCollection_t2327722797_gp_0_0_0_0_ValueCollection_t2327722797_gp_1_0_0_0_Types[] = { &ValueCollection_t2327722797_gp_0_0_0_0, &ValueCollection_t2327722797_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_ValueCollection_t2327722797_gp_0_0_0_0_ValueCollection_t2327722797_gp_1_0_0_0 = { 2, GenInst_ValueCollection_t2327722797_gp_0_0_0_0_ValueCollection_t2327722797_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_ValueCollection_t2327722797_gp_1_0_0_0_Types[] = { &ValueCollection_t2327722797_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_ValueCollection_t2327722797_gp_1_0_0_0 = { 1, GenInst_ValueCollection_t2327722797_gp_1_0_0_0_Types };
extern const Il2CppType Enumerator_t1602367158_gp_0_0_0_0;
extern const Il2CppType Enumerator_t1602367158_gp_1_0_0_0;
static const Il2CppType* GenInst_Enumerator_t1602367158_gp_0_0_0_0_Enumerator_t1602367158_gp_1_0_0_0_Types[] = { &Enumerator_t1602367158_gp_0_0_0_0, &Enumerator_t1602367158_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerator_t1602367158_gp_0_0_0_0_Enumerator_t1602367158_gp_1_0_0_0 = { 2, GenInst_Enumerator_t1602367158_gp_0_0_0_0_Enumerator_t1602367158_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_Enumerator_t1602367158_gp_1_0_0_0_Types[] = { &Enumerator_t1602367158_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerator_t1602367158_gp_1_0_0_0 = { 1, GenInst_Enumerator_t1602367158_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_ValueCollection_t2327722797_gp_0_0_0_0_ValueCollection_t2327722797_gp_1_0_0_0_ValueCollection_t2327722797_gp_1_0_0_0_Types[] = { &ValueCollection_t2327722797_gp_0_0_0_0, &ValueCollection_t2327722797_gp_1_0_0_0, &ValueCollection_t2327722797_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_ValueCollection_t2327722797_gp_0_0_0_0_ValueCollection_t2327722797_gp_1_0_0_0_ValueCollection_t2327722797_gp_1_0_0_0 = { 3, GenInst_ValueCollection_t2327722797_gp_0_0_0_0_ValueCollection_t2327722797_gp_1_0_0_0_ValueCollection_t2327722797_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_ValueCollection_t2327722797_gp_1_0_0_0_ValueCollection_t2327722797_gp_1_0_0_0_Types[] = { &ValueCollection_t2327722797_gp_1_0_0_0, &ValueCollection_t2327722797_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_ValueCollection_t2327722797_gp_1_0_0_0_ValueCollection_t2327722797_gp_1_0_0_0 = { 2, GenInst_ValueCollection_t2327722797_gp_1_0_0_0_ValueCollection_t2327722797_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_DictionaryEntry_t3123975638_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types[] = { &DictionaryEntry_t3123975638_0_0_0, &DictionaryEntry_t3123975638_0_0_0 };
extern const Il2CppGenericInst GenInst_DictionaryEntry_t3123975638_0_0_0_DictionaryEntry_t3123975638_0_0_0 = { 2, GenInst_DictionaryEntry_t3123975638_0_0_0_DictionaryEntry_t3123975638_0_0_0_Types };
static const Il2CppType* GenInst_Dictionary_2_t3621973219_gp_0_0_0_0_Dictionary_2_t3621973219_gp_1_0_0_0_KeyValuePair_2_t1772072192_0_0_0_Types[] = { &Dictionary_2_t3621973219_gp_0_0_0_0, &Dictionary_2_t3621973219_gp_1_0_0_0, &KeyValuePair_2_t1772072192_0_0_0 };
extern const Il2CppGenericInst GenInst_Dictionary_2_t3621973219_gp_0_0_0_0_Dictionary_2_t3621973219_gp_1_0_0_0_KeyValuePair_2_t1772072192_0_0_0 = { 3, GenInst_Dictionary_2_t3621973219_gp_0_0_0_0_Dictionary_2_t3621973219_gp_1_0_0_0_KeyValuePair_2_t1772072192_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t1772072192_0_0_0_KeyValuePair_2_t1772072192_0_0_0_Types[] = { &KeyValuePair_2_t1772072192_0_0_0, &KeyValuePair_2_t1772072192_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1772072192_0_0_0_KeyValuePair_2_t1772072192_0_0_0 = { 2, GenInst_KeyValuePair_2_t1772072192_0_0_0_KeyValuePair_2_t1772072192_0_0_0_Types };
static const Il2CppType* GenInst_Dictionary_2_t3621973219_gp_1_0_0_0_Types[] = { &Dictionary_2_t3621973219_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Dictionary_2_t3621973219_gp_1_0_0_0 = { 1, GenInst_Dictionary_2_t3621973219_gp_1_0_0_0_Types };
extern const Il2CppType EqualityComparer_1_t1549919139_gp_0_0_0_0;
static const Il2CppType* GenInst_EqualityComparer_1_t1549919139_gp_0_0_0_0_Types[] = { &EqualityComparer_1_t1549919139_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_EqualityComparer_1_t1549919139_gp_0_0_0_0 = { 1, GenInst_EqualityComparer_1_t1549919139_gp_0_0_0_0_Types };
extern const Il2CppType DefaultComparer_t4042948011_gp_0_0_0_0;
static const Il2CppType* GenInst_DefaultComparer_t4042948011_gp_0_0_0_0_Types[] = { &DefaultComparer_t4042948011_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_DefaultComparer_t4042948011_gp_0_0_0_0 = { 1, GenInst_DefaultComparer_t4042948011_gp_0_0_0_0_Types };
extern const Il2CppType GenericEqualityComparer_1_t2270490560_gp_0_0_0_0;
static const Il2CppType* GenInst_GenericEqualityComparer_1_t2270490560_gp_0_0_0_0_Types[] = { &GenericEqualityComparer_1_t2270490560_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_GenericEqualityComparer_1_t2270490560_gp_0_0_0_0 = { 1, GenInst_GenericEqualityComparer_1_t2270490560_gp_0_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1708549516_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1708549516_0_0_0_Types[] = { &KeyValuePair_2_t1708549516_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1708549516_0_0_0 = { 1, GenInst_KeyValuePair_2_t1708549516_0_0_0_Types };
extern const Il2CppType IDictionary_2_t3177279192_gp_0_0_0_0;
extern const Il2CppType IDictionary_2_t3177279192_gp_1_0_0_0;
static const Il2CppType* GenInst_IDictionary_2_t3177279192_gp_0_0_0_0_IDictionary_2_t3177279192_gp_1_0_0_0_Types[] = { &IDictionary_2_t3177279192_gp_0_0_0_0, &IDictionary_2_t3177279192_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_IDictionary_2_t3177279192_gp_0_0_0_0_IDictionary_2_t3177279192_gp_1_0_0_0 = { 2, GenInst_IDictionary_2_t3177279192_gp_0_0_0_0_IDictionary_2_t3177279192_gp_1_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t4175610960_gp_0_0_0_0;
extern const Il2CppType KeyValuePair_2_t4175610960_gp_1_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t4175610960_gp_0_0_0_0_KeyValuePair_2_t4175610960_gp_1_0_0_0_Types[] = { &KeyValuePair_2_t4175610960_gp_0_0_0_0, &KeyValuePair_2_t4175610960_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t4175610960_gp_0_0_0_0_KeyValuePair_2_t4175610960_gp_1_0_0_0 = { 2, GenInst_KeyValuePair_2_t4175610960_gp_0_0_0_0_KeyValuePair_2_t4175610960_gp_1_0_0_0_Types };
extern const Il2CppType List_1_t284568025_gp_0_0_0_0;
static const Il2CppType* GenInst_List_1_t284568025_gp_0_0_0_0_Types[] = { &List_1_t284568025_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_List_1_t284568025_gp_0_0_0_0 = { 1, GenInst_List_1_t284568025_gp_0_0_0_0_Types };
extern const Il2CppType Enumerator_t271486022_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerator_t271486022_gp_0_0_0_0_Types[] = { &Enumerator_t271486022_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerator_t271486022_gp_0_0_0_0 = { 1, GenInst_Enumerator_t271486022_gp_0_0_0_0_Types };
extern const Il2CppType Collection_1_t968317937_gp_0_0_0_0;
static const Il2CppType* GenInst_Collection_1_t968317937_gp_0_0_0_0_Types[] = { &Collection_1_t968317937_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Collection_1_t968317937_gp_0_0_0_0 = { 1, GenInst_Collection_1_t968317937_gp_0_0_0_0_Types };
extern const Il2CppType ReadOnlyCollection_1_t2757184810_gp_0_0_0_0;
static const Il2CppType* GenInst_ReadOnlyCollection_1_t2757184810_gp_0_0_0_0_Types[] = { &ReadOnlyCollection_1_t2757184810_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ReadOnlyCollection_1_t2757184810_gp_0_0_0_0 = { 1, GenInst_ReadOnlyCollection_1_t2757184810_gp_0_0_0_0_Types };
extern const Il2CppType MonoProperty_GetterAdapterFrame_m1909347418_gp_0_0_0_0;
extern const Il2CppType MonoProperty_GetterAdapterFrame_m1909347418_gp_1_0_0_0;
static const Il2CppType* GenInst_MonoProperty_GetterAdapterFrame_m1909347418_gp_0_0_0_0_MonoProperty_GetterAdapterFrame_m1909347418_gp_1_0_0_0_Types[] = { &MonoProperty_GetterAdapterFrame_m1909347418_gp_0_0_0_0, &MonoProperty_GetterAdapterFrame_m1909347418_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_MonoProperty_GetterAdapterFrame_m1909347418_gp_0_0_0_0_MonoProperty_GetterAdapterFrame_m1909347418_gp_1_0_0_0 = { 2, GenInst_MonoProperty_GetterAdapterFrame_m1909347418_gp_0_0_0_0_MonoProperty_GetterAdapterFrame_m1909347418_gp_1_0_0_0_Types };
extern const Il2CppType MonoProperty_StaticGetterAdapterFrame_m1636979383_gp_0_0_0_0;
static const Il2CppType* GenInst_MonoProperty_StaticGetterAdapterFrame_m1636979383_gp_0_0_0_0_Types[] = { &MonoProperty_StaticGetterAdapterFrame_m1636979383_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_MonoProperty_StaticGetterAdapterFrame_m1636979383_gp_0_0_0_0 = { 1, GenInst_MonoProperty_StaticGetterAdapterFrame_m1636979383_gp_0_0_0_0_Types };
extern const Il2CppType LinkedList_1_t2189935060_gp_0_0_0_0;
static const Il2CppType* GenInst_LinkedList_1_t2189935060_gp_0_0_0_0_Types[] = { &LinkedList_1_t2189935060_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_LinkedList_1_t2189935060_gp_0_0_0_0 = { 1, GenInst_LinkedList_1_t2189935060_gp_0_0_0_0_Types };
extern const Il2CppType Enumerator_t3560274443_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerator_t3560274443_gp_0_0_0_0_Types[] = { &Enumerator_t3560274443_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerator_t3560274443_gp_0_0_0_0 = { 1, GenInst_Enumerator_t3560274443_gp_0_0_0_0_Types };
extern const Il2CppType LinkedListNode_1_t3023898186_gp_0_0_0_0;
static const Il2CppType* GenInst_LinkedListNode_1_t3023898186_gp_0_0_0_0_Types[] = { &LinkedListNode_1_t3023898186_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_LinkedListNode_1_t3023898186_gp_0_0_0_0 = { 1, GenInst_LinkedListNode_1_t3023898186_gp_0_0_0_0_Types };
extern const Il2CppType Stack_1_t1463756442_gp_0_0_0_0;
static const Il2CppType* GenInst_Stack_1_t1463756442_gp_0_0_0_0_Types[] = { &Stack_1_t1463756442_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Stack_1_t1463756442_gp_0_0_0_0 = { 1, GenInst_Stack_1_t1463756442_gp_0_0_0_0_Types };
extern const Il2CppType Enumerator_t2989469293_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerator_t2989469293_gp_0_0_0_0_Types[] = { &Enumerator_t2989469293_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerator_t2989469293_gp_0_0_0_0 = { 1, GenInst_Enumerator_t2989469293_gp_0_0_0_0_Types };
extern const Il2CppType HashSet_1_t743387557_gp_0_0_0_0;
static const Il2CppType* GenInst_HashSet_1_t743387557_gp_0_0_0_0_Types[] = { &HashSet_1_t743387557_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_HashSet_1_t743387557_gp_0_0_0_0 = { 1, GenInst_HashSet_1_t743387557_gp_0_0_0_0_Types };
extern const Il2CppType Enumerator_t3836401716_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerator_t3836401716_gp_0_0_0_0_Types[] = { &Enumerator_t3836401716_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerator_t3836401716_gp_0_0_0_0 = { 1, GenInst_Enumerator_t3836401716_gp_0_0_0_0_Types };
extern const Il2CppType PrimeHelper_t2385147435_gp_0_0_0_0;
static const Il2CppType* GenInst_PrimeHelper_t2385147435_gp_0_0_0_0_Types[] = { &PrimeHelper_t2385147435_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_PrimeHelper_t2385147435_gp_0_0_0_0 = { 1, GenInst_PrimeHelper_t2385147435_gp_0_0_0_0_Types };
extern const Il2CppType Enumerable_Any_m2256625727_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_Any_m2256625727_gp_0_0_0_0_Types[] = { &Enumerable_Any_m2256625727_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_Any_m2256625727_gp_0_0_0_0 = { 1, GenInst_Enumerable_Any_m2256625727_gp_0_0_0_0_Types };
extern const Il2CppType Enumerable_Contains_m4222801258_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_Contains_m4222801258_gp_0_0_0_0_Types[] = { &Enumerable_Contains_m4222801258_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_Contains_m4222801258_gp_0_0_0_0 = { 1, GenInst_Enumerable_Contains_m4222801258_gp_0_0_0_0_Types };
extern const Il2CppType Enumerable_Contains_m1710981724_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_Contains_m1710981724_gp_0_0_0_0_Types[] = { &Enumerable_Contains_m1710981724_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_Contains_m1710981724_gp_0_0_0_0 = { 1, GenInst_Enumerable_Contains_m1710981724_gp_0_0_0_0_Types };
extern const Il2CppType Enumerable_First_m2847869424_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_First_m2847869424_gp_0_0_0_0_Types[] = { &Enumerable_First_m2847869424_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_First_m2847869424_gp_0_0_0_0 = { 1, GenInst_Enumerable_First_m2847869424_gp_0_0_0_0_Types };
extern const Il2CppType Enumerable_FirstOrDefault_m1944909936_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_FirstOrDefault_m1944909936_gp_0_0_0_0_Types[] = { &Enumerable_FirstOrDefault_m1944909936_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_FirstOrDefault_m1944909936_gp_0_0_0_0 = { 1, GenInst_Enumerable_FirstOrDefault_m1944909936_gp_0_0_0_0_Types };
extern const Il2CppType Enumerable_Last_m3684240055_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_Last_m3684240055_gp_0_0_0_0_Types[] = { &Enumerable_Last_m3684240055_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_Last_m3684240055_gp_0_0_0_0 = { 1, GenInst_Enumerable_Last_m3684240055_gp_0_0_0_0_Types };
extern const Il2CppType Enumerable_OrderBy_m1070606102_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_OrderBy_m1070606102_gp_0_0_0_0_Types[] = { &Enumerable_OrderBy_m1070606102_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_OrderBy_m1070606102_gp_0_0_0_0 = { 1, GenInst_Enumerable_OrderBy_m1070606102_gp_0_0_0_0_Types };
extern const Il2CppType Enumerable_OrderBy_m1070606102_gp_1_0_0_0;
static const Il2CppType* GenInst_Enumerable_OrderBy_m1070606102_gp_0_0_0_0_Enumerable_OrderBy_m1070606102_gp_1_0_0_0_Types[] = { &Enumerable_OrderBy_m1070606102_gp_0_0_0_0, &Enumerable_OrderBy_m1070606102_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_OrderBy_m1070606102_gp_0_0_0_0_Enumerable_OrderBy_m1070606102_gp_1_0_0_0 = { 2, GenInst_Enumerable_OrderBy_m1070606102_gp_0_0_0_0_Enumerable_OrderBy_m1070606102_gp_1_0_0_0_Types };
extern const Il2CppType Enumerable_OrderBy_m2443884947_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_OrderBy_m2443884947_gp_0_0_0_0_Types[] = { &Enumerable_OrderBy_m2443884947_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_OrderBy_m2443884947_gp_0_0_0_0 = { 1, GenInst_Enumerable_OrderBy_m2443884947_gp_0_0_0_0_Types };
extern const Il2CppType Enumerable_OrderBy_m2443884947_gp_1_0_0_0;
static const Il2CppType* GenInst_Enumerable_OrderBy_m2443884947_gp_0_0_0_0_Enumerable_OrderBy_m2443884947_gp_1_0_0_0_Types[] = { &Enumerable_OrderBy_m2443884947_gp_0_0_0_0, &Enumerable_OrderBy_m2443884947_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_OrderBy_m2443884947_gp_0_0_0_0_Enumerable_OrderBy_m2443884947_gp_1_0_0_0 = { 2, GenInst_Enumerable_OrderBy_m2443884947_gp_0_0_0_0_Enumerable_OrderBy_m2443884947_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_Enumerable_OrderBy_m2443884947_gp_1_0_0_0_Types[] = { &Enumerable_OrderBy_m2443884947_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_OrderBy_m2443884947_gp_1_0_0_0 = { 1, GenInst_Enumerable_OrderBy_m2443884947_gp_1_0_0_0_Types };
extern const Il2CppType Enumerable_ThenBy_m1007540769_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_ThenBy_m1007540769_gp_0_0_0_0_Types[] = { &Enumerable_ThenBy_m1007540769_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_ThenBy_m1007540769_gp_0_0_0_0 = { 1, GenInst_Enumerable_ThenBy_m1007540769_gp_0_0_0_0_Types };
extern const Il2CppType Enumerable_ThenBy_m1007540769_gp_1_0_0_0;
static const Il2CppType* GenInst_Enumerable_ThenBy_m1007540769_gp_0_0_0_0_Enumerable_ThenBy_m1007540769_gp_1_0_0_0_Types[] = { &Enumerable_ThenBy_m1007540769_gp_0_0_0_0, &Enumerable_ThenBy_m1007540769_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_ThenBy_m1007540769_gp_0_0_0_0_Enumerable_ThenBy_m1007540769_gp_1_0_0_0 = { 2, GenInst_Enumerable_ThenBy_m1007540769_gp_0_0_0_0_Enumerable_ThenBy_m1007540769_gp_1_0_0_0_Types };
extern const Il2CppType Enumerable_ThenBy_m4292131248_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_ThenBy_m4292131248_gp_0_0_0_0_Types[] = { &Enumerable_ThenBy_m4292131248_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_ThenBy_m4292131248_gp_0_0_0_0 = { 1, GenInst_Enumerable_ThenBy_m4292131248_gp_0_0_0_0_Types };
extern const Il2CppType Enumerable_ThenBy_m4292131248_gp_1_0_0_0;
static const Il2CppType* GenInst_Enumerable_ThenBy_m4292131248_gp_0_0_0_0_Enumerable_ThenBy_m4292131248_gp_1_0_0_0_Types[] = { &Enumerable_ThenBy_m4292131248_gp_0_0_0_0, &Enumerable_ThenBy_m4292131248_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_ThenBy_m4292131248_gp_0_0_0_0_Enumerable_ThenBy_m4292131248_gp_1_0_0_0 = { 2, GenInst_Enumerable_ThenBy_m4292131248_gp_0_0_0_0_Enumerable_ThenBy_m4292131248_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_Enumerable_ThenBy_m4292131248_gp_1_0_0_0_Types[] = { &Enumerable_ThenBy_m4292131248_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_ThenBy_m4292131248_gp_1_0_0_0 = { 1, GenInst_Enumerable_ThenBy_m4292131248_gp_1_0_0_0_Types };
extern const Il2CppType Enumerable_ToArray_m1974389789_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_ToArray_m1974389789_gp_0_0_0_0_Types[] = { &Enumerable_ToArray_m1974389789_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_ToArray_m1974389789_gp_0_0_0_0 = { 1, GenInst_Enumerable_ToArray_m1974389789_gp_0_0_0_0_Types };
extern const Il2CppType Enumerable_ToList_m2780466421_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_ToList_m2780466421_gp_0_0_0_0_Types[] = { &Enumerable_ToList_m2780466421_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_ToList_m2780466421_gp_0_0_0_0 = { 1, GenInst_Enumerable_ToList_m2780466421_gp_0_0_0_0_Types };
extern const Il2CppType Enumerable_Where_m88697338_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_Where_m88697338_gp_0_0_0_0_Types[] = { &Enumerable_Where_m88697338_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_Where_m88697338_gp_0_0_0_0 = { 1, GenInst_Enumerable_Where_m88697338_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_Enumerable_Where_m88697338_gp_0_0_0_0_Boolean_t97287965_0_0_0_Types[] = { &Enumerable_Where_m88697338_gp_0_0_0_0, &Boolean_t97287965_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_Where_m88697338_gp_0_0_0_0_Boolean_t97287965_0_0_0 = { 2, GenInst_Enumerable_Where_m88697338_gp_0_0_0_0_Boolean_t97287965_0_0_0_Types };
extern const Il2CppType Enumerable_CreateWhereIterator_m3238110963_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_CreateWhereIterator_m3238110963_gp_0_0_0_0_Types[] = { &Enumerable_CreateWhereIterator_m3238110963_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_CreateWhereIterator_m3238110963_gp_0_0_0_0 = { 1, GenInst_Enumerable_CreateWhereIterator_m3238110963_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_Enumerable_CreateWhereIterator_m3238110963_gp_0_0_0_0_Boolean_t97287965_0_0_0_Types[] = { &Enumerable_CreateWhereIterator_m3238110963_gp_0_0_0_0, &Boolean_t97287965_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_CreateWhereIterator_m3238110963_gp_0_0_0_0_Boolean_t97287965_0_0_0 = { 2, GenInst_Enumerable_CreateWhereIterator_m3238110963_gp_0_0_0_0_Boolean_t97287965_0_0_0_Types };
extern const Il2CppType U3CCreateWhereIteratorU3Ec__Iterator1D_1_t945640688_gp_0_0_0_0;
static const Il2CppType* GenInst_U3CCreateWhereIteratorU3Ec__Iterator1D_1_t945640688_gp_0_0_0_0_Types[] = { &U3CCreateWhereIteratorU3Ec__Iterator1D_1_t945640688_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CCreateWhereIteratorU3Ec__Iterator1D_1_t945640688_gp_0_0_0_0 = { 1, GenInst_U3CCreateWhereIteratorU3Ec__Iterator1D_1_t945640688_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_U3CCreateWhereIteratorU3Ec__Iterator1D_1_t945640688_gp_0_0_0_0_Boolean_t97287965_0_0_0_Types[] = { &U3CCreateWhereIteratorU3Ec__Iterator1D_1_t945640688_gp_0_0_0_0, &Boolean_t97287965_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CCreateWhereIteratorU3Ec__Iterator1D_1_t945640688_gp_0_0_0_0_Boolean_t97287965_0_0_0 = { 2, GenInst_U3CCreateWhereIteratorU3Ec__Iterator1D_1_t945640688_gp_0_0_0_0_Boolean_t97287965_0_0_0_Types };
extern const Il2CppType IOrderedEnumerable_1_t3318875935_gp_0_0_0_0;
extern const Il2CppType IOrderedEnumerable_1_CreateOrderedEnumerable_m3379561760_gp_0_0_0_0;
static const Il2CppType* GenInst_IOrderedEnumerable_1_t3318875935_gp_0_0_0_0_IOrderedEnumerable_1_CreateOrderedEnumerable_m3379561760_gp_0_0_0_0_Types[] = { &IOrderedEnumerable_1_t3318875935_gp_0_0_0_0, &IOrderedEnumerable_1_CreateOrderedEnumerable_m3379561760_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_IOrderedEnumerable_1_t3318875935_gp_0_0_0_0_IOrderedEnumerable_1_CreateOrderedEnumerable_m3379561760_gp_0_0_0_0 = { 2, GenInst_IOrderedEnumerable_1_t3318875935_gp_0_0_0_0_IOrderedEnumerable_1_CreateOrderedEnumerable_m3379561760_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_IOrderedEnumerable_1_CreateOrderedEnumerable_m3379561760_gp_0_0_0_0_Types[] = { &IOrderedEnumerable_1_CreateOrderedEnumerable_m3379561760_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_IOrderedEnumerable_1_CreateOrderedEnumerable_m3379561760_gp_0_0_0_0 = { 1, GenInst_IOrderedEnumerable_1_CreateOrderedEnumerable_m3379561760_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_IOrderedEnumerable_1_t3318875935_gp_0_0_0_0_Types[] = { &IOrderedEnumerable_1_t3318875935_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_IOrderedEnumerable_1_t3318875935_gp_0_0_0_0 = { 1, GenInst_IOrderedEnumerable_1_t3318875935_gp_0_0_0_0_Types };
extern const Il2CppType OrderedEnumerable_1_t1372796327_gp_0_0_0_0;
static const Il2CppType* GenInst_OrderedEnumerable_1_t1372796327_gp_0_0_0_0_Types[] = { &OrderedEnumerable_1_t1372796327_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_OrderedEnumerable_1_t1372796327_gp_0_0_0_0 = { 1, GenInst_OrderedEnumerable_1_t1372796327_gp_0_0_0_0_Types };
extern const Il2CppType OrderedEnumerable_1_CreateOrderedEnumerable_m205428121_gp_0_0_0_0;
static const Il2CppType* GenInst_OrderedEnumerable_1_t1372796327_gp_0_0_0_0_OrderedEnumerable_1_CreateOrderedEnumerable_m205428121_gp_0_0_0_0_Types[] = { &OrderedEnumerable_1_t1372796327_gp_0_0_0_0, &OrderedEnumerable_1_CreateOrderedEnumerable_m205428121_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_OrderedEnumerable_1_t1372796327_gp_0_0_0_0_OrderedEnumerable_1_CreateOrderedEnumerable_m205428121_gp_0_0_0_0 = { 2, GenInst_OrderedEnumerable_1_t1372796327_gp_0_0_0_0_OrderedEnumerable_1_CreateOrderedEnumerable_m205428121_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_OrderedEnumerable_1_CreateOrderedEnumerable_m205428121_gp_0_0_0_0_Types[] = { &OrderedEnumerable_1_CreateOrderedEnumerable_m205428121_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_OrderedEnumerable_1_CreateOrderedEnumerable_m205428121_gp_0_0_0_0 = { 1, GenInst_OrderedEnumerable_1_CreateOrderedEnumerable_m205428121_gp_0_0_0_0_Types };
extern const Il2CppType OrderedSequence_2_t4114741689_gp_0_0_0_0;
static const Il2CppType* GenInst_OrderedSequence_2_t4114741689_gp_0_0_0_0_Types[] = { &OrderedSequence_2_t4114741689_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_OrderedSequence_2_t4114741689_gp_0_0_0_0 = { 1, GenInst_OrderedSequence_2_t4114741689_gp_0_0_0_0_Types };
extern const Il2CppType OrderedSequence_2_t4114741689_gp_1_0_0_0;
static const Il2CppType* GenInst_OrderedSequence_2_t4114741689_gp_0_0_0_0_OrderedSequence_2_t4114741689_gp_1_0_0_0_Types[] = { &OrderedSequence_2_t4114741689_gp_0_0_0_0, &OrderedSequence_2_t4114741689_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_OrderedSequence_2_t4114741689_gp_0_0_0_0_OrderedSequence_2_t4114741689_gp_1_0_0_0 = { 2, GenInst_OrderedSequence_2_t4114741689_gp_0_0_0_0_OrderedSequence_2_t4114741689_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_OrderedSequence_2_t4114741689_gp_1_0_0_0_Types[] = { &OrderedSequence_2_t4114741689_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_OrderedSequence_2_t4114741689_gp_1_0_0_0 = { 1, GenInst_OrderedSequence_2_t4114741689_gp_1_0_0_0_Types };
extern const Il2CppType QuickSort_1_t1667011766_gp_0_0_0_0;
static const Il2CppType* GenInst_QuickSort_1_t1667011766_gp_0_0_0_0_Types[] = { &QuickSort_1_t1667011766_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_QuickSort_1_t1667011766_gp_0_0_0_0 = { 1, GenInst_QuickSort_1_t1667011766_gp_0_0_0_0_Types };
extern const Il2CppType U3CSortU3Ec__Iterator21_t3807379664_gp_0_0_0_0;
static const Il2CppType* GenInst_U3CSortU3Ec__Iterator21_t3807379664_gp_0_0_0_0_Types[] = { &U3CSortU3Ec__Iterator21_t3807379664_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CSortU3Ec__Iterator21_t3807379664_gp_0_0_0_0 = { 1, GenInst_U3CSortU3Ec__Iterator21_t3807379664_gp_0_0_0_0_Types };
extern const Il2CppType SortContext_1_t2773119229_gp_0_0_0_0;
static const Il2CppType* GenInst_SortContext_1_t2773119229_gp_0_0_0_0_Types[] = { &SortContext_1_t2773119229_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_SortContext_1_t2773119229_gp_0_0_0_0 = { 1, GenInst_SortContext_1_t2773119229_gp_0_0_0_0_Types };
extern const Il2CppType SortSequenceContext_2_t4077189013_gp_0_0_0_0;
static const Il2CppType* GenInst_SortSequenceContext_2_t4077189013_gp_0_0_0_0_Types[] = { &SortSequenceContext_2_t4077189013_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_SortSequenceContext_2_t4077189013_gp_0_0_0_0 = { 1, GenInst_SortSequenceContext_2_t4077189013_gp_0_0_0_0_Types };
extern const Il2CppType SortSequenceContext_2_t4077189013_gp_1_0_0_0;
static const Il2CppType* GenInst_SortSequenceContext_2_t4077189013_gp_0_0_0_0_SortSequenceContext_2_t4077189013_gp_1_0_0_0_Types[] = { &SortSequenceContext_2_t4077189013_gp_0_0_0_0, &SortSequenceContext_2_t4077189013_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_SortSequenceContext_2_t4077189013_gp_0_0_0_0_SortSequenceContext_2_t4077189013_gp_1_0_0_0 = { 2, GenInst_SortSequenceContext_2_t4077189013_gp_0_0_0_0_SortSequenceContext_2_t4077189013_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_SortSequenceContext_2_t4077189013_gp_1_0_0_0_Types[] = { &SortSequenceContext_2_t4077189013_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_SortSequenceContext_2_t4077189013_gp_1_0_0_0 = { 1, GenInst_SortSequenceContext_2_t4077189013_gp_1_0_0_0_Types };
extern const Il2CppType AssetBundle_LoadAllAssets_m4266127462_gp_0_0_0_0;
static const Il2CppType* GenInst_AssetBundle_LoadAllAssets_m4266127462_gp_0_0_0_0_Types[] = { &AssetBundle_LoadAllAssets_m4266127462_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_AssetBundle_LoadAllAssets_m4266127462_gp_0_0_0_0 = { 1, GenInst_AssetBundle_LoadAllAssets_m4266127462_gp_0_0_0_0_Types };
extern const Il2CppType Component_GetComponentInChildren_m1404611044_gp_0_0_0_0;
static const Il2CppType* GenInst_Component_GetComponentInChildren_m1404611044_gp_0_0_0_0_Types[] = { &Component_GetComponentInChildren_m1404611044_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Component_GetComponentInChildren_m1404611044_gp_0_0_0_0 = { 1, GenInst_Component_GetComponentInChildren_m1404611044_gp_0_0_0_0_Types };
extern const Il2CppType Component_GetComponentsInChildren_m3615474666_gp_0_0_0_0;
static const Il2CppType* GenInst_Component_GetComponentsInChildren_m3615474666_gp_0_0_0_0_Types[] = { &Component_GetComponentsInChildren_m3615474666_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Component_GetComponentsInChildren_m3615474666_gp_0_0_0_0 = { 1, GenInst_Component_GetComponentsInChildren_m3615474666_gp_0_0_0_0_Types };
extern const Il2CppType Component_GetComponentsInChildren_m933673001_gp_0_0_0_0;
static const Il2CppType* GenInst_Component_GetComponentsInChildren_m933673001_gp_0_0_0_0_Types[] = { &Component_GetComponentsInChildren_m933673001_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Component_GetComponentsInChildren_m933673001_gp_0_0_0_0 = { 1, GenInst_Component_GetComponentsInChildren_m933673001_gp_0_0_0_0_Types };
extern const Il2CppType Component_GetComponentsInChildren_m4005089511_gp_0_0_0_0;
static const Il2CppType* GenInst_Component_GetComponentsInChildren_m4005089511_gp_0_0_0_0_Types[] = { &Component_GetComponentsInChildren_m4005089511_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Component_GetComponentsInChildren_m4005089511_gp_0_0_0_0 = { 1, GenInst_Component_GetComponentsInChildren_m4005089511_gp_0_0_0_0_Types };
extern const Il2CppType Component_GetComponentsInChildren_m1915630048_gp_0_0_0_0;
static const Il2CppType* GenInst_Component_GetComponentsInChildren_m1915630048_gp_0_0_0_0_Types[] = { &Component_GetComponentsInChildren_m1915630048_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Component_GetComponentsInChildren_m1915630048_gp_0_0_0_0 = { 1, GenInst_Component_GetComponentsInChildren_m1915630048_gp_0_0_0_0_Types };
extern const Il2CppType Component_GetComponentsInParent_m555240590_gp_0_0_0_0;
static const Il2CppType* GenInst_Component_GetComponentsInParent_m555240590_gp_0_0_0_0_Types[] = { &Component_GetComponentsInParent_m555240590_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Component_GetComponentsInParent_m555240590_gp_0_0_0_0 = { 1, GenInst_Component_GetComponentsInParent_m555240590_gp_0_0_0_0_Types };
extern const Il2CppType Component_GetComponentsInParent_m2569671904_gp_0_0_0_0;
static const Il2CppType* GenInst_Component_GetComponentsInParent_m2569671904_gp_0_0_0_0_Types[] = { &Component_GetComponentsInParent_m2569671904_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Component_GetComponentsInParent_m2569671904_gp_0_0_0_0 = { 1, GenInst_Component_GetComponentsInParent_m2569671904_gp_0_0_0_0_Types };
extern const Il2CppType Component_GetComponentsInParent_m149672228_gp_0_0_0_0;
static const Il2CppType* GenInst_Component_GetComponentsInParent_m149672228_gp_0_0_0_0_Types[] = { &Component_GetComponentsInParent_m149672228_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Component_GetComponentsInParent_m149672228_gp_0_0_0_0 = { 1, GenInst_Component_GetComponentsInParent_m149672228_gp_0_0_0_0_Types };
extern const Il2CppType Component_GetComponents_m3598713100_gp_0_0_0_0;
static const Il2CppType* GenInst_Component_GetComponents_m3598713100_gp_0_0_0_0_Types[] = { &Component_GetComponents_m3598713100_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Component_GetComponents_m3598713100_gp_0_0_0_0 = { 1, GenInst_Component_GetComponents_m3598713100_gp_0_0_0_0_Types };
extern const Il2CppType Component_GetComponents_m3914262141_gp_0_0_0_0;
static const Il2CppType* GenInst_Component_GetComponents_m3914262141_gp_0_0_0_0_Types[] = { &Component_GetComponents_m3914262141_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Component_GetComponents_m3914262141_gp_0_0_0_0 = { 1, GenInst_Component_GetComponents_m3914262141_gp_0_0_0_0_Types };
extern const Il2CppType GameObject_GetComponentInChildren_m916317014_gp_0_0_0_0;
static const Il2CppType* GenInst_GameObject_GetComponentInChildren_m916317014_gp_0_0_0_0_Types[] = { &GameObject_GetComponentInChildren_m916317014_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_GameObject_GetComponentInChildren_m916317014_gp_0_0_0_0 = { 1, GenInst_GameObject_GetComponentInChildren_m916317014_gp_0_0_0_0_Types };
extern const Il2CppType GameObject_GetComponents_m2386781050_gp_0_0_0_0;
static const Il2CppType* GenInst_GameObject_GetComponents_m2386781050_gp_0_0_0_0_Types[] = { &GameObject_GetComponents_m2386781050_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_GameObject_GetComponents_m2386781050_gp_0_0_0_0 = { 1, GenInst_GameObject_GetComponents_m2386781050_gp_0_0_0_0_Types };
extern const Il2CppType GameObject_GetComponentsInChildren_m906810621_gp_0_0_0_0;
static const Il2CppType* GenInst_GameObject_GetComponentsInChildren_m906810621_gp_0_0_0_0_Types[] = { &GameObject_GetComponentsInChildren_m906810621_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_GameObject_GetComponentsInChildren_m906810621_gp_0_0_0_0 = { 1, GenInst_GameObject_GetComponentsInChildren_m906810621_gp_0_0_0_0_Types };
extern const Il2CppType GameObject_GetComponentsInChildren_m1887210244_gp_0_0_0_0;
static const Il2CppType* GenInst_GameObject_GetComponentsInChildren_m1887210244_gp_0_0_0_0_Types[] = { &GameObject_GetComponentsInChildren_m1887210244_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_GameObject_GetComponentsInChildren_m1887210244_gp_0_0_0_0 = { 1, GenInst_GameObject_GetComponentsInChildren_m1887210244_gp_0_0_0_0_Types };
extern const Il2CppType GameObject_GetComponentsInParent_m241541908_gp_0_0_0_0;
static const Il2CppType* GenInst_GameObject_GetComponentsInParent_m241541908_gp_0_0_0_0_Types[] = { &GameObject_GetComponentsInParent_m241541908_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_GameObject_GetComponentsInParent_m241541908_gp_0_0_0_0 = { 1, GenInst_GameObject_GetComponentsInParent_m241541908_gp_0_0_0_0_Types };
extern const Il2CppType Mesh_GetAllocArrayFromChannel_m2078183201_gp_0_0_0_0;
static const Il2CppType* GenInst_Mesh_GetAllocArrayFromChannel_m2078183201_gp_0_0_0_0_Types[] = { &Mesh_GetAllocArrayFromChannel_m2078183201_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Mesh_GetAllocArrayFromChannel_m2078183201_gp_0_0_0_0 = { 1, GenInst_Mesh_GetAllocArrayFromChannel_m2078183201_gp_0_0_0_0_Types };
extern const Il2CppType Mesh_SafeLength_m1325240367_gp_0_0_0_0;
static const Il2CppType* GenInst_Mesh_SafeLength_m1325240367_gp_0_0_0_0_Types[] = { &Mesh_SafeLength_m1325240367_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Mesh_SafeLength_m1325240367_gp_0_0_0_0 = { 1, GenInst_Mesh_SafeLength_m1325240367_gp_0_0_0_0_Types };
extern const Il2CppType Mesh_SetListForChannel_m462664139_gp_0_0_0_0;
static const Il2CppType* GenInst_Mesh_SetListForChannel_m462664139_gp_0_0_0_0_Types[] = { &Mesh_SetListForChannel_m462664139_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Mesh_SetListForChannel_m462664139_gp_0_0_0_0 = { 1, GenInst_Mesh_SetListForChannel_m462664139_gp_0_0_0_0_Types };
extern const Il2CppType Mesh_SetListForChannel_m631926134_gp_0_0_0_0;
static const Il2CppType* GenInst_Mesh_SetListForChannel_m631926134_gp_0_0_0_0_Types[] = { &Mesh_SetListForChannel_m631926134_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Mesh_SetListForChannel_m631926134_gp_0_0_0_0 = { 1, GenInst_Mesh_SetListForChannel_m631926134_gp_0_0_0_0_Types };
extern const Il2CppType Mesh_SetUvsImpl_m1664867204_gp_0_0_0_0;
static const Il2CppType* GenInst_Mesh_SetUvsImpl_m1664867204_gp_0_0_0_0_Types[] = { &Mesh_SetUvsImpl_m1664867204_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Mesh_SetUvsImpl_m1664867204_gp_0_0_0_0 = { 1, GenInst_Mesh_SetUvsImpl_m1664867204_gp_0_0_0_0_Types };
extern const Il2CppType Object_Instantiate_m1874094322_gp_0_0_0_0;
static const Il2CppType* GenInst_Object_Instantiate_m1874094322_gp_0_0_0_0_Types[] = { &Object_Instantiate_m1874094322_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_Instantiate_m1874094322_gp_0_0_0_0 = { 1, GenInst_Object_Instantiate_m1874094322_gp_0_0_0_0_Types };
extern const Il2CppType Object_FindObjectsOfType_m1824391917_gp_0_0_0_0;
static const Il2CppType* GenInst_Object_FindObjectsOfType_m1824391917_gp_0_0_0_0_Types[] = { &Object_FindObjectsOfType_m1824391917_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_FindObjectsOfType_m1824391917_gp_0_0_0_0 = { 1, GenInst_Object_FindObjectsOfType_m1824391917_gp_0_0_0_0_Types };
extern const Il2CppType InvokableCall_1_t3865199217_gp_0_0_0_0;
static const Il2CppType* GenInst_InvokableCall_1_t3865199217_gp_0_0_0_0_Types[] = { &InvokableCall_1_t3865199217_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_InvokableCall_1_t3865199217_gp_0_0_0_0 = { 1, GenInst_InvokableCall_1_t3865199217_gp_0_0_0_0_Types };
extern const Il2CppType UnityAction_1_t802700511_0_0_0;
static const Il2CppType* GenInst_UnityAction_1_t802700511_0_0_0_Types[] = { &UnityAction_1_t802700511_0_0_0 };
extern const Il2CppGenericInst GenInst_UnityAction_1_t802700511_0_0_0 = { 1, GenInst_UnityAction_1_t802700511_0_0_0_Types };
extern const Il2CppType InvokableCall_2_t3865133681_gp_0_0_0_0;
extern const Il2CppType InvokableCall_2_t3865133681_gp_1_0_0_0;
static const Il2CppType* GenInst_InvokableCall_2_t3865133681_gp_0_0_0_0_InvokableCall_2_t3865133681_gp_1_0_0_0_Types[] = { &InvokableCall_2_t3865133681_gp_0_0_0_0, &InvokableCall_2_t3865133681_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_InvokableCall_2_t3865133681_gp_0_0_0_0_InvokableCall_2_t3865133681_gp_1_0_0_0 = { 2, GenInst_InvokableCall_2_t3865133681_gp_0_0_0_0_InvokableCall_2_t3865133681_gp_1_0_0_0_Types };
extern const Il2CppType UnityAction_2_t300839120_0_0_0;
static const Il2CppType* GenInst_UnityAction_2_t300839120_0_0_0_Types[] = { &UnityAction_2_t300839120_0_0_0 };
extern const Il2CppGenericInst GenInst_UnityAction_2_t300839120_0_0_0 = { 1, GenInst_UnityAction_2_t300839120_0_0_0_Types };
static const Il2CppType* GenInst_InvokableCall_2_t3865133681_gp_0_0_0_0_Types[] = { &InvokableCall_2_t3865133681_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_InvokableCall_2_t3865133681_gp_0_0_0_0 = { 1, GenInst_InvokableCall_2_t3865133681_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_InvokableCall_2_t3865133681_gp_1_0_0_0_Types[] = { &InvokableCall_2_t3865133681_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_InvokableCall_2_t3865133681_gp_1_0_0_0 = { 1, GenInst_InvokableCall_2_t3865133681_gp_1_0_0_0_Types };
extern const Il2CppType InvokableCall_3_t3865068145_gp_0_0_0_0;
extern const Il2CppType InvokableCall_3_t3865068145_gp_1_0_0_0;
extern const Il2CppType InvokableCall_3_t3865068145_gp_2_0_0_0;
static const Il2CppType* GenInst_InvokableCall_3_t3865068145_gp_0_0_0_0_InvokableCall_3_t3865068145_gp_1_0_0_0_InvokableCall_3_t3865068145_gp_2_0_0_0_Types[] = { &InvokableCall_3_t3865068145_gp_0_0_0_0, &InvokableCall_3_t3865068145_gp_1_0_0_0, &InvokableCall_3_t3865068145_gp_2_0_0_0 };
extern const Il2CppGenericInst GenInst_InvokableCall_3_t3865068145_gp_0_0_0_0_InvokableCall_3_t3865068145_gp_1_0_0_0_InvokableCall_3_t3865068145_gp_2_0_0_0 = { 3, GenInst_InvokableCall_3_t3865068145_gp_0_0_0_0_InvokableCall_3_t3865068145_gp_1_0_0_0_InvokableCall_3_t3865068145_gp_2_0_0_0_Types };
extern const Il2CppType UnityAction_3_t148588884_0_0_0;
static const Il2CppType* GenInst_UnityAction_3_t148588884_0_0_0_Types[] = { &UnityAction_3_t148588884_0_0_0 };
extern const Il2CppGenericInst GenInst_UnityAction_3_t148588884_0_0_0 = { 1, GenInst_UnityAction_3_t148588884_0_0_0_Types };
static const Il2CppType* GenInst_InvokableCall_3_t3865068145_gp_0_0_0_0_Types[] = { &InvokableCall_3_t3865068145_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_InvokableCall_3_t3865068145_gp_0_0_0_0 = { 1, GenInst_InvokableCall_3_t3865068145_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_InvokableCall_3_t3865068145_gp_1_0_0_0_Types[] = { &InvokableCall_3_t3865068145_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_InvokableCall_3_t3865068145_gp_1_0_0_0 = { 1, GenInst_InvokableCall_3_t3865068145_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_InvokableCall_3_t3865068145_gp_2_0_0_0_Types[] = { &InvokableCall_3_t3865068145_gp_2_0_0_0 };
extern const Il2CppGenericInst GenInst_InvokableCall_3_t3865068145_gp_2_0_0_0 = { 1, GenInst_InvokableCall_3_t3865068145_gp_2_0_0_0_Types };
extern const Il2CppType InvokableCall_4_t3865002609_gp_0_0_0_0;
extern const Il2CppType InvokableCall_4_t3865002609_gp_1_0_0_0;
extern const Il2CppType InvokableCall_4_t3865002609_gp_2_0_0_0;
extern const Il2CppType InvokableCall_4_t3865002609_gp_3_0_0_0;
static const Il2CppType* GenInst_InvokableCall_4_t3865002609_gp_0_0_0_0_InvokableCall_4_t3865002609_gp_1_0_0_0_InvokableCall_4_t3865002609_gp_2_0_0_0_InvokableCall_4_t3865002609_gp_3_0_0_0_Types[] = { &InvokableCall_4_t3865002609_gp_0_0_0_0, &InvokableCall_4_t3865002609_gp_1_0_0_0, &InvokableCall_4_t3865002609_gp_2_0_0_0, &InvokableCall_4_t3865002609_gp_3_0_0_0 };
extern const Il2CppGenericInst GenInst_InvokableCall_4_t3865002609_gp_0_0_0_0_InvokableCall_4_t3865002609_gp_1_0_0_0_InvokableCall_4_t3865002609_gp_2_0_0_0_InvokableCall_4_t3865002609_gp_3_0_0_0 = { 4, GenInst_InvokableCall_4_t3865002609_gp_0_0_0_0_InvokableCall_4_t3865002609_gp_1_0_0_0_InvokableCall_4_t3865002609_gp_2_0_0_0_InvokableCall_4_t3865002609_gp_3_0_0_0_Types };
static const Il2CppType* GenInst_InvokableCall_4_t3865002609_gp_0_0_0_0_Types[] = { &InvokableCall_4_t3865002609_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_InvokableCall_4_t3865002609_gp_0_0_0_0 = { 1, GenInst_InvokableCall_4_t3865002609_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_InvokableCall_4_t3865002609_gp_1_0_0_0_Types[] = { &InvokableCall_4_t3865002609_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_InvokableCall_4_t3865002609_gp_1_0_0_0 = { 1, GenInst_InvokableCall_4_t3865002609_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_InvokableCall_4_t3865002609_gp_2_0_0_0_Types[] = { &InvokableCall_4_t3865002609_gp_2_0_0_0 };
extern const Il2CppGenericInst GenInst_InvokableCall_4_t3865002609_gp_2_0_0_0 = { 1, GenInst_InvokableCall_4_t3865002609_gp_2_0_0_0_Types };
static const Il2CppType* GenInst_InvokableCall_4_t3865002609_gp_3_0_0_0_Types[] = { &InvokableCall_4_t3865002609_gp_3_0_0_0 };
extern const Il2CppGenericInst GenInst_InvokableCall_4_t3865002609_gp_3_0_0_0 = { 1, GenInst_InvokableCall_4_t3865002609_gp_3_0_0_0_Types };
extern const Il2CppType CachedInvokableCall_1_t3153979999_gp_0_0_0_0;
static const Il2CppType* GenInst_CachedInvokableCall_1_t3153979999_gp_0_0_0_0_Types[] = { &CachedInvokableCall_1_t3153979999_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_CachedInvokableCall_1_t3153979999_gp_0_0_0_0 = { 1, GenInst_CachedInvokableCall_1_t3153979999_gp_0_0_0_0_Types };
extern const Il2CppType UnityEvent_1_t74220259_gp_0_0_0_0;
static const Il2CppType* GenInst_UnityEvent_1_t74220259_gp_0_0_0_0_Types[] = { &UnityEvent_1_t74220259_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_UnityEvent_1_t74220259_gp_0_0_0_0 = { 1, GenInst_UnityEvent_1_t74220259_gp_0_0_0_0_Types };
extern const Il2CppType UnityEvent_2_t477504786_gp_0_0_0_0;
extern const Il2CppType UnityEvent_2_t477504786_gp_1_0_0_0;
static const Il2CppType* GenInst_UnityEvent_2_t477504786_gp_0_0_0_0_UnityEvent_2_t477504786_gp_1_0_0_0_Types[] = { &UnityEvent_2_t477504786_gp_0_0_0_0, &UnityEvent_2_t477504786_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_UnityEvent_2_t477504786_gp_0_0_0_0_UnityEvent_2_t477504786_gp_1_0_0_0 = { 2, GenInst_UnityEvent_2_t477504786_gp_0_0_0_0_UnityEvent_2_t477504786_gp_1_0_0_0_Types };
extern const Il2CppType UnityEvent_3_t3206388141_gp_0_0_0_0;
extern const Il2CppType UnityEvent_3_t3206388141_gp_1_0_0_0;
extern const Il2CppType UnityEvent_3_t3206388141_gp_2_0_0_0;
static const Il2CppType* GenInst_UnityEvent_3_t3206388141_gp_0_0_0_0_UnityEvent_3_t3206388141_gp_1_0_0_0_UnityEvent_3_t3206388141_gp_2_0_0_0_Types[] = { &UnityEvent_3_t3206388141_gp_0_0_0_0, &UnityEvent_3_t3206388141_gp_1_0_0_0, &UnityEvent_3_t3206388141_gp_2_0_0_0 };
extern const Il2CppGenericInst GenInst_UnityEvent_3_t3206388141_gp_0_0_0_0_UnityEvent_3_t3206388141_gp_1_0_0_0_UnityEvent_3_t3206388141_gp_2_0_0_0 = { 3, GenInst_UnityEvent_3_t3206388141_gp_0_0_0_0_UnityEvent_3_t3206388141_gp_1_0_0_0_UnityEvent_3_t3206388141_gp_2_0_0_0_Types };
extern const Il2CppType UnityEvent_4_t3609672668_gp_0_0_0_0;
extern const Il2CppType UnityEvent_4_t3609672668_gp_1_0_0_0;
extern const Il2CppType UnityEvent_4_t3609672668_gp_2_0_0_0;
extern const Il2CppType UnityEvent_4_t3609672668_gp_3_0_0_0;
static const Il2CppType* GenInst_UnityEvent_4_t3609672668_gp_0_0_0_0_UnityEvent_4_t3609672668_gp_1_0_0_0_UnityEvent_4_t3609672668_gp_2_0_0_0_UnityEvent_4_t3609672668_gp_3_0_0_0_Types[] = { &UnityEvent_4_t3609672668_gp_0_0_0_0, &UnityEvent_4_t3609672668_gp_1_0_0_0, &UnityEvent_4_t3609672668_gp_2_0_0_0, &UnityEvent_4_t3609672668_gp_3_0_0_0 };
extern const Il2CppGenericInst GenInst_UnityEvent_4_t3609672668_gp_0_0_0_0_UnityEvent_4_t3609672668_gp_1_0_0_0_UnityEvent_4_t3609672668_gp_2_0_0_0_UnityEvent_4_t3609672668_gp_3_0_0_0 = { 4, GenInst_UnityEvent_4_t3609672668_gp_0_0_0_0_UnityEvent_4_t3609672668_gp_1_0_0_0_UnityEvent_4_t3609672668_gp_2_0_0_0_UnityEvent_4_t3609672668_gp_3_0_0_0_Types };
extern const Il2CppType ExecuteEvents_Execute_m513682320_gp_0_0_0_0;
static const Il2CppType* GenInst_ExecuteEvents_Execute_m513682320_gp_0_0_0_0_Types[] = { &ExecuteEvents_Execute_m513682320_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ExecuteEvents_Execute_m513682320_gp_0_0_0_0 = { 1, GenInst_ExecuteEvents_Execute_m513682320_gp_0_0_0_0_Types };
extern const Il2CppType ExecuteEvents_ExecuteHierarchy_m2988888012_gp_0_0_0_0;
static const Il2CppType* GenInst_ExecuteEvents_ExecuteHierarchy_m2988888012_gp_0_0_0_0_Types[] = { &ExecuteEvents_ExecuteHierarchy_m2988888012_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ExecuteEvents_ExecuteHierarchy_m2988888012_gp_0_0_0_0 = { 1, GenInst_ExecuteEvents_ExecuteHierarchy_m2988888012_gp_0_0_0_0_Types };
extern const Il2CppType ExecuteEvents_GetEventList_m400222725_gp_0_0_0_0;
static const Il2CppType* GenInst_ExecuteEvents_GetEventList_m400222725_gp_0_0_0_0_Types[] = { &ExecuteEvents_GetEventList_m400222725_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ExecuteEvents_GetEventList_m400222725_gp_0_0_0_0 = { 1, GenInst_ExecuteEvents_GetEventList_m400222725_gp_0_0_0_0_Types };
extern const Il2CppType ExecuteEvents_CanHandleEvent_m1773513752_gp_0_0_0_0;
static const Il2CppType* GenInst_ExecuteEvents_CanHandleEvent_m1773513752_gp_0_0_0_0_Types[] = { &ExecuteEvents_CanHandleEvent_m1773513752_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ExecuteEvents_CanHandleEvent_m1773513752_gp_0_0_0_0 = { 1, GenInst_ExecuteEvents_CanHandleEvent_m1773513752_gp_0_0_0_0_Types };
extern const Il2CppType ExecuteEvents_GetEventHandler_m3154756557_gp_0_0_0_0;
static const Il2CppType* GenInst_ExecuteEvents_GetEventHandler_m3154756557_gp_0_0_0_0_Types[] = { &ExecuteEvents_GetEventHandler_m3154756557_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ExecuteEvents_GetEventHandler_m3154756557_gp_0_0_0_0 = { 1, GenInst_ExecuteEvents_GetEventHandler_m3154756557_gp_0_0_0_0_Types };
extern const Il2CppType TweenRunner_1_t3844461449_gp_0_0_0_0;
static const Il2CppType* GenInst_TweenRunner_1_t3844461449_gp_0_0_0_0_Types[] = { &TweenRunner_1_t3844461449_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_TweenRunner_1_t3844461449_gp_0_0_0_0 = { 1, GenInst_TweenRunner_1_t3844461449_gp_0_0_0_0_Types };
extern const Il2CppType Dropdown_GetOrAddComponent_m4080506703_gp_0_0_0_0;
static const Il2CppType* GenInst_Dropdown_GetOrAddComponent_m4080506703_gp_0_0_0_0_Types[] = { &Dropdown_GetOrAddComponent_m4080506703_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Dropdown_GetOrAddComponent_m4080506703_gp_0_0_0_0 = { 1, GenInst_Dropdown_GetOrAddComponent_m4080506703_gp_0_0_0_0_Types };
extern const Il2CppType SetPropertyUtility_SetStruct_m2798032208_gp_0_0_0_0;
static const Il2CppType* GenInst_SetPropertyUtility_SetStruct_m2798032208_gp_0_0_0_0_Types[] = { &SetPropertyUtility_SetStruct_m2798032208_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_SetPropertyUtility_SetStruct_m2798032208_gp_0_0_0_0 = { 1, GenInst_SetPropertyUtility_SetStruct_m2798032208_gp_0_0_0_0_Types };
extern const Il2CppType IndexedSet_1_t2120020791_gp_0_0_0_0;
static const Il2CppType* GenInst_IndexedSet_1_t2120020791_gp_0_0_0_0_Types[] = { &IndexedSet_1_t2120020791_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_IndexedSet_1_t2120020791_gp_0_0_0_0 = { 1, GenInst_IndexedSet_1_t2120020791_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_IndexedSet_1_t2120020791_gp_0_0_0_0_Int32_t2950945753_0_0_0_Types[] = { &IndexedSet_1_t2120020791_gp_0_0_0_0, &Int32_t2950945753_0_0_0 };
extern const Il2CppGenericInst GenInst_IndexedSet_1_t2120020791_gp_0_0_0_0_Int32_t2950945753_0_0_0 = { 2, GenInst_IndexedSet_1_t2120020791_gp_0_0_0_0_Int32_t2950945753_0_0_0_Types };
extern const Il2CppType ListPool_1_t890186770_gp_0_0_0_0;
static const Il2CppType* GenInst_ListPool_1_t890186770_gp_0_0_0_0_Types[] = { &ListPool_1_t890186770_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ListPool_1_t890186770_gp_0_0_0_0 = { 1, GenInst_ListPool_1_t890186770_gp_0_0_0_0_Types };
extern const Il2CppType List_1_t3009893961_0_0_0;
static const Il2CppType* GenInst_List_1_t3009893961_0_0_0_Types[] = { &List_1_t3009893961_0_0_0 };
extern const Il2CppGenericInst GenInst_List_1_t3009893961_0_0_0 = { 1, GenInst_List_1_t3009893961_0_0_0_Types };
extern const Il2CppType ObjectPool_1_t892185599_gp_0_0_0_0;
static const Il2CppType* GenInst_ObjectPool_1_t892185599_gp_0_0_0_0_Types[] = { &ObjectPool_1_t892185599_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ObjectPool_1_t892185599_gp_0_0_0_0 = { 1, GenInst_ObjectPool_1_t892185599_gp_0_0_0_0_Types };
extern const Il2CppType FastAction_1_t2420856216_gp_0_0_0_0;
static const Il2CppType* GenInst_FastAction_1_t2420856216_gp_0_0_0_0_Types[] = { &FastAction_1_t2420856216_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_FastAction_1_t2420856216_gp_0_0_0_0 = { 1, GenInst_FastAction_1_t2420856216_gp_0_0_0_0_Types };
extern const Il2CppType Action_1_t3240956260_0_0_0;
static const Il2CppType* GenInst_Action_1_t3240956260_0_0_0_Types[] = { &Action_1_t3240956260_0_0_0 };
extern const Il2CppGenericInst GenInst_Action_1_t3240956260_0_0_0 = { 1, GenInst_Action_1_t3240956260_0_0_0_Types };
extern const Il2CppType LinkedListNode_1_t2986131363_0_0_0;
static const Il2CppType* GenInst_Action_1_t3240956260_0_0_0_LinkedListNode_1_t2986131363_0_0_0_Types[] = { &Action_1_t3240956260_0_0_0, &LinkedListNode_1_t2986131363_0_0_0 };
extern const Il2CppGenericInst GenInst_Action_1_t3240956260_0_0_0_LinkedListNode_1_t2986131363_0_0_0 = { 2, GenInst_Action_1_t3240956260_0_0_0_LinkedListNode_1_t2986131363_0_0_0_Types };
extern const Il2CppType FastAction_2_t2421052824_gp_0_0_0_0;
extern const Il2CppType FastAction_2_t2421052824_gp_1_0_0_0;
static const Il2CppType* GenInst_FastAction_2_t2421052824_gp_0_0_0_0_FastAction_2_t2421052824_gp_1_0_0_0_Types[] = { &FastAction_2_t2421052824_gp_0_0_0_0, &FastAction_2_t2421052824_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_FastAction_2_t2421052824_gp_0_0_0_0_FastAction_2_t2421052824_gp_1_0_0_0 = { 2, GenInst_FastAction_2_t2421052824_gp_0_0_0_0_FastAction_2_t2421052824_gp_1_0_0_0_Types };
extern const Il2CppType Action_2_t3881532715_0_0_0;
static const Il2CppType* GenInst_Action_2_t3881532715_0_0_0_Types[] = { &Action_2_t3881532715_0_0_0 };
extern const Il2CppGenericInst GenInst_Action_2_t3881532715_0_0_0 = { 1, GenInst_Action_2_t3881532715_0_0_0_Types };
extern const Il2CppType LinkedListNode_1_t3626707818_0_0_0;
static const Il2CppType* GenInst_Action_2_t3881532715_0_0_0_LinkedListNode_1_t3626707818_0_0_0_Types[] = { &Action_2_t3881532715_0_0_0, &LinkedListNode_1_t3626707818_0_0_0 };
extern const Il2CppGenericInst GenInst_Action_2_t3881532715_0_0_0_LinkedListNode_1_t3626707818_0_0_0 = { 2, GenInst_Action_2_t3881532715_0_0_0_LinkedListNode_1_t3626707818_0_0_0_Types };
extern const Il2CppType FastAction_3_t2420987288_gp_0_0_0_0;
extern const Il2CppType FastAction_3_t2420987288_gp_1_0_0_0;
extern const Il2CppType FastAction_3_t2420987288_gp_2_0_0_0;
static const Il2CppType* GenInst_FastAction_3_t2420987288_gp_0_0_0_0_FastAction_3_t2420987288_gp_1_0_0_0_FastAction_3_t2420987288_gp_2_0_0_0_Types[] = { &FastAction_3_t2420987288_gp_0_0_0_0, &FastAction_3_t2420987288_gp_1_0_0_0, &FastAction_3_t2420987288_gp_2_0_0_0 };
extern const Il2CppGenericInst GenInst_FastAction_3_t2420987288_gp_0_0_0_0_FastAction_3_t2420987288_gp_1_0_0_0_FastAction_3_t2420987288_gp_2_0_0_0 = { 3, GenInst_FastAction_3_t2420987288_gp_0_0_0_0_FastAction_3_t2420987288_gp_1_0_0_0_FastAction_3_t2420987288_gp_2_0_0_0_Types };
extern const Il2CppType Action_3_t4213579327_0_0_0;
static const Il2CppType* GenInst_Action_3_t4213579327_0_0_0_Types[] = { &Action_3_t4213579327_0_0_0 };
extern const Il2CppGenericInst GenInst_Action_3_t4213579327_0_0_0 = { 1, GenInst_Action_3_t4213579327_0_0_0_Types };
extern const Il2CppType LinkedListNode_1_t3958754430_0_0_0;
static const Il2CppType* GenInst_Action_3_t4213579327_0_0_0_LinkedListNode_1_t3958754430_0_0_0_Types[] = { &Action_3_t4213579327_0_0_0, &LinkedListNode_1_t3958754430_0_0_0 };
extern const Il2CppGenericInst GenInst_Action_3_t4213579327_0_0_0_LinkedListNode_1_t3958754430_0_0_0 = { 2, GenInst_Action_3_t4213579327_0_0_0_LinkedListNode_1_t3958754430_0_0_0_Types };
extern const Il2CppType TweenRunner_1_t3899390588_gp_0_0_0_0;
static const Il2CppType* GenInst_TweenRunner_1_t3899390588_gp_0_0_0_0_Types[] = { &TweenRunner_1_t3899390588_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_TweenRunner_1_t3899390588_gp_0_0_0_0 = { 1, GenInst_TweenRunner_1_t3899390588_gp_0_0_0_0_Types };
extern const Il2CppType TMP_Dropdown_GetOrAddComponent_m1213542931_gp_0_0_0_0;
static const Il2CppType* GenInst_TMP_Dropdown_GetOrAddComponent_m1213542931_gp_0_0_0_0_Types[] = { &TMP_Dropdown_GetOrAddComponent_m1213542931_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_TMP_Dropdown_GetOrAddComponent_m1213542931_gp_0_0_0_0 = { 1, GenInst_TMP_Dropdown_GetOrAddComponent_m1213542931_gp_0_0_0_0_Types };
extern const Il2CppType SetPropertyUtility_SetEquatableStruct_m2580670824_gp_0_0_0_0;
static const Il2CppType* GenInst_SetPropertyUtility_SetEquatableStruct_m2580670824_gp_0_0_0_0_Types[] = { &SetPropertyUtility_SetEquatableStruct_m2580670824_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_SetPropertyUtility_SetEquatableStruct_m2580670824_gp_0_0_0_0 = { 1, GenInst_SetPropertyUtility_SetEquatableStruct_m2580670824_gp_0_0_0_0_Types };
extern const Il2CppType TMP_ListPool_1_t894436986_gp_0_0_0_0;
static const Il2CppType* GenInst_TMP_ListPool_1_t894436986_gp_0_0_0_0_Types[] = { &TMP_ListPool_1_t894436986_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_TMP_ListPool_1_t894436986_gp_0_0_0_0 = { 1, GenInst_TMP_ListPool_1_t894436986_gp_0_0_0_0_Types };
extern const Il2CppType List_1_t3014144177_0_0_0;
static const Il2CppType* GenInst_List_1_t3014144177_0_0_0_Types[] = { &List_1_t3014144177_0_0_0 };
extern const Il2CppGenericInst GenInst_List_1_t3014144177_0_0_0 = { 1, GenInst_List_1_t3014144177_0_0_0_Types };
extern const Il2CppType U3CU3Ec_t3046190068_gp_0_0_0_0;
static const Il2CppType* GenInst_U3CU3Ec_t3046190068_gp_0_0_0_0_Types[] = { &U3CU3Ec_t3046190068_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CU3Ec_t3046190068_gp_0_0_0_0 = { 1, GenInst_U3CU3Ec_t3046190068_gp_0_0_0_0_Types };
extern const Il2CppType TMP_ObjectPool_1_t2098678475_gp_0_0_0_0;
static const Il2CppType* GenInst_TMP_ObjectPool_1_t2098678475_gp_0_0_0_0_Types[] = { &TMP_ObjectPool_1_t2098678475_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_TMP_ObjectPool_1_t2098678475_gp_0_0_0_0 = { 1, GenInst_TMP_ObjectPool_1_t2098678475_gp_0_0_0_0_Types };
extern const Il2CppType TMP_Text_ResizeInternalArray_m2041343611_gp_0_0_0_0;
static const Il2CppType* GenInst_TMP_Text_ResizeInternalArray_m2041343611_gp_0_0_0_0_Types[] = { &TMP_Text_ResizeInternalArray_m2041343611_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_TMP_Text_ResizeInternalArray_m2041343611_gp_0_0_0_0 = { 1, GenInst_TMP_Text_ResizeInternalArray_m2041343611_gp_0_0_0_0_Types };
extern const Il2CppType TMP_TextInfo_Resize_m333743359_gp_0_0_0_0;
static const Il2CppType* GenInst_TMP_TextInfo_Resize_m333743359_gp_0_0_0_0_Types[] = { &TMP_TextInfo_Resize_m333743359_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_TMP_TextInfo_Resize_m333743359_gp_0_0_0_0 = { 1, GenInst_TMP_TextInfo_Resize_m333743359_gp_0_0_0_0_Types };
extern const Il2CppType TMP_TextInfo_Resize_m2258560236_gp_0_0_0_0;
static const Il2CppType* GenInst_TMP_TextInfo_Resize_m2258560236_gp_0_0_0_0_Types[] = { &TMP_TextInfo_Resize_m2258560236_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_TMP_TextInfo_Resize_m2258560236_gp_0_0_0_0 = { 1, GenInst_TMP_TextInfo_Resize_m2258560236_gp_0_0_0_0_Types };
extern const Il2CppType TMP_XmlTagStack_1_t3882766083_gp_0_0_0_0;
static const Il2CppType* GenInst_TMP_XmlTagStack_1_t3882766083_gp_0_0_0_0_Types[] = { &TMP_XmlTagStack_1_t3882766083_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_TMP_XmlTagStack_1_t3882766083_gp_0_0_0_0 = { 1, GenInst_TMP_XmlTagStack_1_t3882766083_gp_0_0_0_0_Types };
extern const Il2CppType TMPro_ExtensionMethods_FindInstanceID_m826387165_gp_0_0_0_0;
static const Il2CppType* GenInst_TMPro_ExtensionMethods_FindInstanceID_m826387165_gp_0_0_0_0_Types[] = { &TMPro_ExtensionMethods_FindInstanceID_m826387165_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_TMPro_ExtensionMethods_FindInstanceID_m826387165_gp_0_0_0_0 = { 1, GenInst_TMPro_ExtensionMethods_FindInstanceID_m826387165_gp_0_0_0_0_Types };
extern const Il2CppType DefaultExecutionOrder_t3059642329_0_0_0;
static const Il2CppType* GenInst_DefaultExecutionOrder_t3059642329_0_0_0_Types[] = { &DefaultExecutionOrder_t3059642329_0_0_0 };
extern const Il2CppGenericInst GenInst_DefaultExecutionOrder_t3059642329_0_0_0 = { 1, GenInst_DefaultExecutionOrder_t3059642329_0_0_0_Types };
extern const Il2CppType PlayerConnection_t3081694049_0_0_0;
static const Il2CppType* GenInst_PlayerConnection_t3081694049_0_0_0_Types[] = { &PlayerConnection_t3081694049_0_0_0 };
extern const Il2CppGenericInst GenInst_PlayerConnection_t3081694049_0_0_0 = { 1, GenInst_PlayerConnection_t3081694049_0_0_0_Types };
extern const Il2CppType ParticleSystem_t1800779281_0_0_0;
static const Il2CppType* GenInst_ParticleSystem_t1800779281_0_0_0_Types[] = { &ParticleSystem_t1800779281_0_0_0 };
extern const Il2CppGenericInst GenInst_ParticleSystem_t1800779281_0_0_0 = { 1, GenInst_ParticleSystem_t1800779281_0_0_0_Types };
extern const Il2CppType GUILayer_t2783472903_0_0_0;
static const Il2CppType* GenInst_GUILayer_t2783472903_0_0_0_Types[] = { &GUILayer_t2783472903_0_0_0 };
extern const Il2CppGenericInst GenInst_GUILayer_t2783472903_0_0_0 = { 1, GenInst_GUILayer_t2783472903_0_0_0_Types };
extern const Il2CppType EventSystem_t1003666588_0_0_0;
static const Il2CppType* GenInst_EventSystem_t1003666588_0_0_0_Types[] = { &EventSystem_t1003666588_0_0_0 };
extern const Il2CppGenericInst GenInst_EventSystem_t1003666588_0_0_0 = { 1, GenInst_EventSystem_t1003666588_0_0_0_Types };
extern const Il2CppType AxisEventData_t2331243652_0_0_0;
static const Il2CppType* GenInst_AxisEventData_t2331243652_0_0_0_Types[] = { &AxisEventData_t2331243652_0_0_0 };
extern const Il2CppGenericInst GenInst_AxisEventData_t2331243652_0_0_0 = { 1, GenInst_AxisEventData_t2331243652_0_0_0_Types };
extern const Il2CppType SpriteRenderer_t3235626157_0_0_0;
static const Il2CppType* GenInst_SpriteRenderer_t3235626157_0_0_0_Types[] = { &SpriteRenderer_t3235626157_0_0_0 };
extern const Il2CppGenericInst GenInst_SpriteRenderer_t3235626157_0_0_0 = { 1, GenInst_SpriteRenderer_t3235626157_0_0_0_Types };
extern const Il2CppType Image_t2670269651_0_0_0;
static const Il2CppType* GenInst_Image_t2670269651_0_0_0_Types[] = { &Image_t2670269651_0_0_0 };
extern const Il2CppGenericInst GenInst_Image_t2670269651_0_0_0 = { 1, GenInst_Image_t2670269651_0_0_0_Types };
extern const Il2CppType RawImage_t3182918964_0_0_0;
static const Il2CppType* GenInst_RawImage_t3182918964_0_0_0_Types[] = { &RawImage_t3182918964_0_0_0 };
extern const Il2CppGenericInst GenInst_RawImage_t3182918964_0_0_0 = { 1, GenInst_RawImage_t3182918964_0_0_0_Types };
extern const Il2CppType Slider_t3903728902_0_0_0;
static const Il2CppType* GenInst_Slider_t3903728902_0_0_0_Types[] = { &Slider_t3903728902_0_0_0 };
extern const Il2CppGenericInst GenInst_Slider_t3903728902_0_0_0 = { 1, GenInst_Slider_t3903728902_0_0_0_Types };
extern const Il2CppType Scrollbar_t1494447233_0_0_0;
static const Il2CppType* GenInst_Scrollbar_t1494447233_0_0_0_Types[] = { &Scrollbar_t1494447233_0_0_0 };
extern const Il2CppGenericInst GenInst_Scrollbar_t1494447233_0_0_0 = { 1, GenInst_Scrollbar_t1494447233_0_0_0_Types };
extern const Il2CppType InputField_t3762917431_0_0_0;
static const Il2CppType* GenInst_InputField_t3762917431_0_0_0_Types[] = { &InputField_t3762917431_0_0_0 };
extern const Il2CppGenericInst GenInst_InputField_t3762917431_0_0_0 = { 1, GenInst_InputField_t3762917431_0_0_0_Types };
extern const Il2CppType ScrollRect_t4137855814_0_0_0;
static const Il2CppType* GenInst_ScrollRect_t4137855814_0_0_0_Types[] = { &ScrollRect_t4137855814_0_0_0 };
extern const Il2CppGenericInst GenInst_ScrollRect_t4137855814_0_0_0 = { 1, GenInst_ScrollRect_t4137855814_0_0_0_Types };
extern const Il2CppType Dropdown_t2274391225_0_0_0;
static const Il2CppType* GenInst_Dropdown_t2274391225_0_0_0_Types[] = { &Dropdown_t2274391225_0_0_0 };
extern const Il2CppGenericInst GenInst_Dropdown_t2274391225_0_0_0 = { 1, GenInst_Dropdown_t2274391225_0_0_0_Types };
extern const Il2CppType GraphicRaycaster_t2999697109_0_0_0;
static const Il2CppType* GenInst_GraphicRaycaster_t2999697109_0_0_0_Types[] = { &GraphicRaycaster_t2999697109_0_0_0 };
extern const Il2CppGenericInst GenInst_GraphicRaycaster_t2999697109_0_0_0 = { 1, GenInst_GraphicRaycaster_t2999697109_0_0_0_Types };
extern const Il2CppType CanvasRenderer_t2598313366_0_0_0;
static const Il2CppType* GenInst_CanvasRenderer_t2598313366_0_0_0_Types[] = { &CanvasRenderer_t2598313366_0_0_0 };
extern const Il2CppGenericInst GenInst_CanvasRenderer_t2598313366_0_0_0 = { 1, GenInst_CanvasRenderer_t2598313366_0_0_0_Types };
extern const Il2CppType Corner_t1493259673_0_0_0;
static const Il2CppType* GenInst_Corner_t1493259673_0_0_0_Types[] = { &Corner_t1493259673_0_0_0 };
extern const Il2CppGenericInst GenInst_Corner_t1493259673_0_0_0 = { 1, GenInst_Corner_t1493259673_0_0_0_Types };
extern const Il2CppType Axis_t3613393006_0_0_0;
static const Il2CppType* GenInst_Axis_t3613393006_0_0_0_Types[] = { &Axis_t3613393006_0_0_0 };
extern const Il2CppGenericInst GenInst_Axis_t3613393006_0_0_0 = { 1, GenInst_Axis_t3613393006_0_0_0_Types };
extern const Il2CppType Constraint_t814224393_0_0_0;
static const Il2CppType* GenInst_Constraint_t814224393_0_0_0_Types[] = { &Constraint_t814224393_0_0_0 };
extern const Il2CppGenericInst GenInst_Constraint_t814224393_0_0_0 = { 1, GenInst_Constraint_t814224393_0_0_0_Types };
extern const Il2CppType SubmitEvent_t648412432_0_0_0;
static const Il2CppType* GenInst_SubmitEvent_t648412432_0_0_0_Types[] = { &SubmitEvent_t648412432_0_0_0 };
extern const Il2CppGenericInst GenInst_SubmitEvent_t648412432_0_0_0 = { 1, GenInst_SubmitEvent_t648412432_0_0_0_Types };
extern const Il2CppType OnChangeEvent_t467195904_0_0_0;
static const Il2CppType* GenInst_OnChangeEvent_t467195904_0_0_0_Types[] = { &OnChangeEvent_t467195904_0_0_0 };
extern const Il2CppGenericInst GenInst_OnChangeEvent_t467195904_0_0_0 = { 1, GenInst_OnChangeEvent_t467195904_0_0_0_Types };
extern const Il2CppType OnValidateInput_t2355412304_0_0_0;
static const Il2CppType* GenInst_OnValidateInput_t2355412304_0_0_0_Types[] = { &OnValidateInput_t2355412304_0_0_0 };
extern const Il2CppGenericInst GenInst_OnValidateInput_t2355412304_0_0_0 = { 1, GenInst_OnValidateInput_t2355412304_0_0_0_Types };
extern const Il2CppType LayoutElement_t1785403678_0_0_0;
static const Il2CppType* GenInst_LayoutElement_t1785403678_0_0_0_Types[] = { &LayoutElement_t1785403678_0_0_0 };
extern const Il2CppGenericInst GenInst_LayoutElement_t1785403678_0_0_0 = { 1, GenInst_LayoutElement_t1785403678_0_0_0_Types };
extern const Il2CppType RectOffset_t1369453676_0_0_0;
static const Il2CppType* GenInst_RectOffset_t1369453676_0_0_0_Types[] = { &RectOffset_t1369453676_0_0_0 };
extern const Il2CppGenericInst GenInst_RectOffset_t1369453676_0_0_0 = { 1, GenInst_RectOffset_t1369453676_0_0_0_Types };
extern const Il2CppType TextAnchor_t2035777396_0_0_0;
static const Il2CppType* GenInst_TextAnchor_t2035777396_0_0_0_Types[] = { &TextAnchor_t2035777396_0_0_0 };
extern const Il2CppGenericInst GenInst_TextAnchor_t2035777396_0_0_0 = { 1, GenInst_TextAnchor_t2035777396_0_0_0_Types };
extern const Il2CppType AnimationTriggers_t2532145056_0_0_0;
static const Il2CppType* GenInst_AnimationTriggers_t2532145056_0_0_0_Types[] = { &AnimationTriggers_t2532145056_0_0_0 };
extern const Il2CppGenericInst GenInst_AnimationTriggers_t2532145056_0_0_0 = { 1, GenInst_AnimationTriggers_t2532145056_0_0_0_Types };
extern const Il2CppType Animator_t434523843_0_0_0;
static const Il2CppType* GenInst_Animator_t434523843_0_0_0_Types[] = { &Animator_t434523843_0_0_0 };
extern const Il2CppGenericInst GenInst_Animator_t434523843_0_0_0 = { 1, GenInst_Animator_t434523843_0_0_0_Types };
extern const Il2CppType InlineGraphicManager_t2871008645_0_0_0;
static const Il2CppType* GenInst_InlineGraphicManager_t2871008645_0_0_0_Types[] = { &InlineGraphicManager_t2871008645_0_0_0 };
extern const Il2CppGenericInst GenInst_InlineGraphicManager_t2871008645_0_0_0 = { 1, GenInst_InlineGraphicManager_t2871008645_0_0_0_Types };
extern const Il2CppType InlineGraphic_t2901727699_0_0_0;
static const Il2CppType* GenInst_InlineGraphic_t2901727699_0_0_0_Types[] = { &InlineGraphic_t2901727699_0_0_0 };
extern const Il2CppGenericInst GenInst_InlineGraphic_t2901727699_0_0_0 = { 1, GenInst_InlineGraphic_t2901727699_0_0_0_Types };
static const Il2CppType* GenInst_TextMeshPro_t2393593166_0_0_0_Types[] = { &TextMeshPro_t2393593166_0_0_0 };
extern const Il2CppGenericInst GenInst_TextMeshPro_t2393593166_0_0_0 = { 1, GenInst_TextMeshPro_t2393593166_0_0_0_Types };
extern const Il2CppType MeshFilter_t3523625662_0_0_0;
static const Il2CppType* GenInst_MeshFilter_t3523625662_0_0_0_Types[] = { &MeshFilter_t3523625662_0_0_0 };
extern const Il2CppGenericInst GenInst_MeshFilter_t3523625662_0_0_0 = { 1, GenInst_MeshFilter_t3523625662_0_0_0_Types };
extern const Il2CppType TMP_InputField_t1099764886_0_0_0;
static const Il2CppType* GenInst_TMP_InputField_t1099764886_0_0_0_Types[] = { &TMP_InputField_t1099764886_0_0_0 };
extern const Il2CppGenericInst GenInst_TMP_InputField_t1099764886_0_0_0 = { 1, GenInst_TMP_InputField_t1099764886_0_0_0_Types };
static const Il2CppType* GenInst_TextMeshProUGUI_t529313277_0_0_0_Types[] = { &TextMeshProUGUI_t529313277_0_0_0 };
extern const Il2CppGenericInst GenInst_TextMeshProUGUI_t529313277_0_0_0 = { 1, GenInst_TextMeshProUGUI_t529313277_0_0_0_Types };
extern const Il2CppType TMP_Dropdown_t3024694699_0_0_0;
static const Il2CppType* GenInst_TMP_Dropdown_t3024694699_0_0_0_Types[] = { &TMP_Dropdown_t3024694699_0_0_0 };
extern const Il2CppGenericInst GenInst_TMP_Dropdown_t3024694699_0_0_0 = { 1, GenInst_TMP_Dropdown_t3024694699_0_0_0_Types };
extern const Il2CppType SubmitEvent_t1343580625_0_0_0;
static const Il2CppType* GenInst_SubmitEvent_t1343580625_0_0_0_Types[] = { &SubmitEvent_t1343580625_0_0_0 };
extern const Il2CppGenericInst GenInst_SubmitEvent_t1343580625_0_0_0 = { 1, GenInst_SubmitEvent_t1343580625_0_0_0_Types };
extern const Il2CppType SelectionEvent_t4268942288_0_0_0;
static const Il2CppType* GenInst_SelectionEvent_t4268942288_0_0_0_Types[] = { &SelectionEvent_t4268942288_0_0_0 };
extern const Il2CppGenericInst GenInst_SelectionEvent_t4268942288_0_0_0 = { 1, GenInst_SelectionEvent_t4268942288_0_0_0_Types };
extern const Il2CppType TextSelectionEvent_t1023421581_0_0_0;
static const Il2CppType* GenInst_TextSelectionEvent_t1023421581_0_0_0_Types[] = { &TextSelectionEvent_t1023421581_0_0_0 };
extern const Il2CppGenericInst GenInst_TextSelectionEvent_t1023421581_0_0_0 = { 1, GenInst_TextSelectionEvent_t1023421581_0_0_0_Types };
extern const Il2CppType OnChangeEvent_t4257774360_0_0_0;
static const Il2CppType* GenInst_OnChangeEvent_t4257774360_0_0_0_Types[] = { &OnChangeEvent_t4257774360_0_0_0 };
extern const Il2CppGenericInst GenInst_OnChangeEvent_t4257774360_0_0_0 = { 1, GenInst_OnChangeEvent_t4257774360_0_0_0_Types };
extern const Il2CppType OnValidateInput_t373909109_0_0_0;
static const Il2CppType* GenInst_OnValidateInput_t373909109_0_0_0_Types[] = { &OnValidateInput_t373909109_0_0_0 };
extern const Il2CppGenericInst GenInst_OnValidateInput_t373909109_0_0_0 = { 1, GenInst_OnValidateInput_t373909109_0_0_0_Types };
extern const Il2CppType LineType_t2817627283_0_0_0;
static const Il2CppType* GenInst_LineType_t2817627283_0_0_0_Types[] = { &LineType_t2817627283_0_0_0 };
extern const Il2CppGenericInst GenInst_LineType_t2817627283_0_0_0 = { 1, GenInst_LineType_t2817627283_0_0_0_Types };
extern const Il2CppType InputType_t2510134850_0_0_0;
static const Il2CppType* GenInst_InputType_t2510134850_0_0_0_Types[] = { &InputType_t2510134850_0_0_0 };
extern const Il2CppGenericInst GenInst_InputType_t2510134850_0_0_0 = { 1, GenInst_InputType_t2510134850_0_0_0_Types };
extern const Il2CppType CharacterValidation_t3801396403_0_0_0;
static const Il2CppType* GenInst_CharacterValidation_t3801396403_0_0_0_Types[] = { &CharacterValidation_t3801396403_0_0_0 };
extern const Il2CppGenericInst GenInst_CharacterValidation_t3801396403_0_0_0 = { 1, GenInst_CharacterValidation_t3801396403_0_0_0_Types };
extern const Il2CppType TMP_InputValidator_t1385053824_0_0_0;
static const Il2CppType* GenInst_TMP_InputValidator_t1385053824_0_0_0_Types[] = { &TMP_InputValidator_t1385053824_0_0_0 };
extern const Il2CppGenericInst GenInst_TMP_InputValidator_t1385053824_0_0_0 = { 1, GenInst_TMP_InputValidator_t1385053824_0_0_0_Types };
extern const Il2CppType TMP_SelectionCaret_t407257285_0_0_0;
static const Il2CppType* GenInst_TMP_SelectionCaret_t407257285_0_0_0_Types[] = { &TMP_SelectionCaret_t407257285_0_0_0 };
extern const Il2CppGenericInst GenInst_TMP_SelectionCaret_t407257285_0_0_0 = { 1, GenInst_TMP_SelectionCaret_t407257285_0_0_0_Types };
extern const Il2CppType BoxCollider_t1640800422_0_0_0;
static const Il2CppType* GenInst_BoxCollider_t1640800422_0_0_0_Types[] = { &BoxCollider_t1640800422_0_0_0 };
extern const Il2CppGenericInst GenInst_BoxCollider_t1640800422_0_0_0 = { 1, GenInst_BoxCollider_t1640800422_0_0_0_Types };
extern const Il2CppType Rigidbody_t3916780224_0_0_0;
static const Il2CppType* GenInst_Rigidbody_t3916780224_0_0_0_Types[] = { &Rigidbody_t3916780224_0_0_0 };
extern const Il2CppGenericInst GenInst_Rigidbody_t3916780224_0_0_0 = { 1, GenInst_Rigidbody_t3916780224_0_0_0_Types };
extern const Il2CppType TMP_SpriteAnimator_t2836635477_0_0_0;
static const Il2CppType* GenInst_TMP_SpriteAnimator_t2836635477_0_0_0_Types[] = { &TMP_SpriteAnimator_t2836635477_0_0_0 };
extern const Il2CppGenericInst GenInst_TMP_SpriteAnimator_t2836635477_0_0_0 = { 1, GenInst_TMP_SpriteAnimator_t2836635477_0_0_0_Types };
extern const Il2CppType TrackerManager_t2102804711_0_0_0;
static const Il2CppType* GenInst_TrackerManager_t2102804711_0_0_0_Types[] = { &TrackerManager_t2102804711_0_0_0 };
extern const Il2CppGenericInst GenInst_TrackerManager_t2102804711_0_0_0 = { 1, GenInst_TrackerManager_t2102804711_0_0_0_Types };
extern const Il2CppType WikitudeCamera_t2188881370_0_0_0;
static const Il2CppType* GenInst_WikitudeCamera_t2188881370_0_0_0_Types[] = { &WikitudeCamera_t2188881370_0_0_0 };
extern const Il2CppGenericInst GenInst_WikitudeCamera_t2188881370_0_0_0 = { 1, GenInst_WikitudeCamera_t2188881370_0_0_0_Types };
extern const Il2CppType PluginManager_t1017991214_0_0_0;
static const Il2CppType* GenInst_PluginManager_t1017991214_0_0_0_Types[] = { &PluginManager_t1017991214_0_0_0 };
extern const Il2CppGenericInst GenInst_PluginManager_t1017991214_0_0_0 = { 1, GenInst_PluginManager_t1017991214_0_0_0_Types };
extern const Il2CppType BackgroundCamera_t2368011250_0_0_0;
static const Il2CppType* GenInst_BackgroundCamera_t2368011250_0_0_0_Types[] = { &BackgroundCamera_t2368011250_0_0_0 };
extern const Il2CppGenericInst GenInst_BackgroundCamera_t2368011250_0_0_0 = { 1, GenInst_BackgroundCamera_t2368011250_0_0_0_Types };
extern const Il2CppType HighlighterBlocker_t3431127703_0_0_0;
static const Il2CppType* GenInst_HighlighterBlocker_t3431127703_0_0_0_Types[] = { &HighlighterBlocker_t3431127703_0_0_0 };
extern const Il2CppGenericInst GenInst_HighlighterBlocker_t3431127703_0_0_0 = { 1, GenInst_HighlighterBlocker_t3431127703_0_0_0_Types };
extern const Il2CppType GUITexture_t951903601_0_0_0;
static const Il2CppType* GenInst_GUITexture_t951903601_0_0_0_Types[] = { &GUITexture_t951903601_0_0_0 };
extern const Il2CppGenericInst GenInst_GUITexture_t951903601_0_0_0 = { 1, GenInst_GUITexture_t951903601_0_0_0_Types };
extern const Il2CppType GUIText_t402233326_0_0_0;
static const Il2CppType* GenInst_GUIText_t402233326_0_0_0_Types[] = { &GUIText_t402233326_0_0_0 };
extern const Il2CppGenericInst GenInst_GUIText_t402233326_0_0_0 = { 1, GenInst_GUIText_t402233326_0_0_0_Types };
extern const Il2CppType Light_t3756812086_0_0_0;
static const Il2CppType* GenInst_Light_t3756812086_0_0_0_Types[] = { &Light_t3756812086_0_0_0 };
extern const Il2CppGenericInst GenInst_Light_t3756812086_0_0_0 = { 1, GenInst_Light_t3756812086_0_0_0_Types };
extern const Il2CppType AudioSource_t3935305588_0_0_0;
static const Il2CppType* GenInst_AudioSource_t3935305588_0_0_0_Types[] = { &AudioSource_t3935305588_0_0_0 };
extern const Il2CppGenericInst GenInst_AudioSource_t3935305588_0_0_0 = { 1, GenInst_AudioSource_t3935305588_0_0_0_Types };
extern const Il2CppType Launch_Animation_t763963988_0_0_0;
static const Il2CppType* GenInst_Launch_Animation_t763963988_0_0_0_Types[] = { &Launch_Animation_t763963988_0_0_0 };
extern const Il2CppGenericInst GenInst_Launch_Animation_t763963988_0_0_0 = { 1, GenInst_Launch_Animation_t763963988_0_0_0_Types };
extern const Il2CppType ImageTracker_t271395264_0_0_0;
static const Il2CppType* GenInst_ImageTracker_t271395264_0_0_0_Types[] = { &ImageTracker_t271395264_0_0_0 };
extern const Il2CppGenericInst GenInst_ImageTracker_t271395264_0_0_0 = { 1, GenInst_ImageTracker_t271395264_0_0_0_Types };
extern const Il2CppType MeshCollider_t903564387_0_0_0;
static const Il2CppType* GenInst_MeshCollider_t903564387_0_0_0_Types[] = { &MeshCollider_t903564387_0_0_0 };
extern const Il2CppGenericInst GenInst_MeshCollider_t903564387_0_0_0 = { 1, GenInst_MeshCollider_t903564387_0_0_0_Types };
extern const Il2CppType SmoothOrbitCam_t3313789169_0_0_0;
static const Il2CppType* GenInst_SmoothOrbitCam_t3313789169_0_0_0_Types[] = { &SmoothOrbitCam_t3313789169_0_0_0 };
extern const Il2CppGenericInst GenInst_SmoothOrbitCam_t3313789169_0_0_0 = { 1, GenInst_SmoothOrbitCam_t3313789169_0_0_0_Types };
extern const Il2CppType VideoClip_t1281919028_0_0_0;
static const Il2CppType* GenInst_VideoClip_t1281919028_0_0_0_Types[] = { &VideoClip_t1281919028_0_0_0 };
extern const Il2CppGenericInst GenInst_VideoClip_t1281919028_0_0_0 = { 1, GenInst_VideoClip_t1281919028_0_0_0_Types };
extern const Il2CppType Texture2D_t3840446185_0_0_0;
static const Il2CppType* GenInst_Texture2D_t3840446185_0_0_0_Types[] = { &Texture2D_t3840446185_0_0_0 };
extern const Il2CppGenericInst GenInst_Texture2D_t3840446185_0_0_0 = { 1, GenInst_Texture2D_t3840446185_0_0_0_Types };
extern const Il2CppType MoveController_t1157877467_0_0_0;
static const Il2CppType* GenInst_MoveController_t1157877467_0_0_0_Types[] = { &MoveController_t1157877467_0_0_0 };
extern const Il2CppGenericInst GenInst_MoveController_t1157877467_0_0_0 = { 1, GenInst_MoveController_t1157877467_0_0_0_Types };
extern const Il2CppType GridRenderer_t1964421403_0_0_0;
static const Il2CppType* GenInst_GridRenderer_t1964421403_0_0_0_Types[] = { &GridRenderer_t1964421403_0_0_0 };
extern const Il2CppGenericInst GenInst_GridRenderer_t1964421403_0_0_0 = { 1, GenInst_GridRenderer_t1964421403_0_0_0_Types };
extern const Il2CppType TextMesh_t1536577757_0_0_0;
static const Il2CppType* GenInst_TextMesh_t1536577757_0_0_0_Types[] = { &TextMesh_t1536577757_0_0_0 };
extern const Il2CppGenericInst GenInst_TextMesh_t1536577757_0_0_0 = { 1, GenInst_TextMesh_t1536577757_0_0_0_Types };
extern const Il2CppType LineRenderer_t3154350270_0_0_0;
static const Il2CppType* GenInst_LineRenderer_t3154350270_0_0_0_Types[] = { &LineRenderer_t3154350270_0_0_0 };
extern const Il2CppGenericInst GenInst_LineRenderer_t3154350270_0_0_0 = { 1, GenInst_LineRenderer_t3154350270_0_0_0_Types };
extern const Il2CppType LabelController_t2075514664_0_0_0;
static const Il2CppType* GenInst_LabelController_t2075514664_0_0_0_Types[] = { &LabelController_t2075514664_0_0_0 };
extern const Il2CppGenericInst GenInst_LabelController_t2075514664_0_0_0 = { 1, GenInst_LabelController_t2075514664_0_0_0_Types };
extern const Il2CppType StreamVideo_t2698492640_0_0_0;
static const Il2CppType* GenInst_StreamVideo_t2698492640_0_0_0_Types[] = { &StreamVideo_t2698492640_0_0_0 };
extern const Il2CppGenericInst GenInst_StreamVideo_t2698492640_0_0_0 = { 1, GenInst_StreamVideo_t2698492640_0_0_0_Types };
extern const Il2CppType Image_t1825575977_0_0_0;
static const Il2CppType* GenInst_Image_t1825575977_0_0_0_Types[] = { &Image_t1825575977_0_0_0 };
extern const Il2CppGenericInst GenInst_Image_t1825575977_0_0_0 = { 1, GenInst_Image_t1825575977_0_0_0_Types };
extern const Il2CppType InstantTrackingController_t1374048463_0_0_0;
static const Il2CppType* GenInst_InstantTrackingController_t1374048463_0_0_0_Types[] = { &InstantTrackingController_t1374048463_0_0_0 };
extern const Il2CppGenericInst GenInst_InstantTrackingController_t1374048463_0_0_0 = { 1, GenInst_InstantTrackingController_t1374048463_0_0_0_Types };
extern const Il2CppType HighlightingRenderer_t1923179915_0_0_0;
static const Il2CppType* GenInst_HighlightingRenderer_t1923179915_0_0_0_Types[] = { &HighlightingRenderer_t1923179915_0_0_0 };
extern const Il2CppGenericInst GenInst_HighlightingRenderer_t1923179915_0_0_0 = { 1, GenInst_HighlightingRenderer_t1923179915_0_0_0_Types };
extern const Il2CppType arAnimationController_t1029363926_0_0_0;
static const Il2CppType* GenInst_arAnimationController_t1029363926_0_0_0_Types[] = { &arAnimationController_t1029363926_0_0_0 };
extern const Il2CppGenericInst GenInst_arAnimationController_t1029363926_0_0_0 = { 1, GenInst_arAnimationController_t1029363926_0_0_0_Types };
extern const Il2CppType InstantTrackable_t3187174337_0_0_0;
static const Il2CppType* GenInst_InstantTrackable_t3187174337_0_0_0_Types[] = { &InstantTrackable_t3187174337_0_0_0 };
extern const Il2CppGenericInst GenInst_InstantTrackable_t3187174337_0_0_0 = { 1, GenInst_InstantTrackable_t3187174337_0_0_0_Types };
extern const Il2CppType VideoPlayer_t1683042537_0_0_0;
static const Il2CppType* GenInst_VideoPlayer_t1683042537_0_0_0_Types[] = { &VideoPlayer_t1683042537_0_0_0 };
extern const Il2CppGenericInst GenInst_VideoPlayer_t1683042537_0_0_0 = { 1, GenInst_VideoPlayer_t1683042537_0_0_0_Types };
extern const Il2CppType TextMeshProFloatingText_t845872552_0_0_0;
static const Il2CppType* GenInst_TextMeshProFloatingText_t845872552_0_0_0_Types[] = { &TextMeshProFloatingText_t845872552_0_0_0 };
extern const Il2CppGenericInst GenInst_TextMeshProFloatingText_t845872552_0_0_0 = { 1, GenInst_TextMeshProFloatingText_t845872552_0_0_0_Types };
extern const Il2CppType TextContainer_t97923372_0_0_0;
static const Il2CppType* GenInst_TextContainer_t97923372_0_0_0_Types[] = { &TextContainer_t97923372_0_0_0 };
extern const Il2CppGenericInst GenInst_TextContainer_t97923372_0_0_0 = { 1, GenInst_TextContainer_t97923372_0_0_0_Types };
static const Il2CppType* GenInst_InputFrameData_t2573966900_0_0_0_InputFrameData_t2573966900_0_0_0_Types[] = { &InputFrameData_t2573966900_0_0_0, &InputFrameData_t2573966900_0_0_0 };
extern const Il2CppGenericInst GenInst_InputFrameData_t2573966900_0_0_0_InputFrameData_t2573966900_0_0_0 = { 2, GenInst_InputFrameData_t2573966900_0_0_0_InputFrameData_t2573966900_0_0_0_Types };
static const Il2CppType* GenInst_Data_t434873072_0_0_0_Data_t434873072_0_0_0_Types[] = { &Data_t434873072_0_0_0, &Data_t434873072_0_0_0 };
extern const Il2CppGenericInst GenInst_Data_t434873072_0_0_0_Data_t434873072_0_0_0 = { 2, GenInst_Data_t434873072_0_0_0_Data_t434873072_0_0_0_Types };
static const Il2CppType* GenInst_HighlightingPreset_t635619791_0_0_0_HighlightingPreset_t635619791_0_0_0_Types[] = { &HighlightingPreset_t635619791_0_0_0, &HighlightingPreset_t635619791_0_0_0 };
extern const Il2CppGenericInst GenInst_HighlightingPreset_t635619791_0_0_0_HighlightingPreset_t635619791_0_0_0 = { 2, GenInst_HighlightingPreset_t635619791_0_0_0_HighlightingPreset_t635619791_0_0_0_Types };
static const Il2CppType* GenInst_Char_t3634460470_0_0_0_Char_t3634460470_0_0_0_Types[] = { &Char_t3634460470_0_0_0, &Char_t3634460470_0_0_0 };
extern const Il2CppGenericInst GenInst_Char_t3634460470_0_0_0_Char_t3634460470_0_0_0 = { 2, GenInst_Char_t3634460470_0_0_0_Char_t3634460470_0_0_0_Types };
static const Il2CppType* GenInst_CustomAttributeNamedArgument_t287865710_0_0_0_CustomAttributeNamedArgument_t287865710_0_0_0_Types[] = { &CustomAttributeNamedArgument_t287865710_0_0_0, &CustomAttributeNamedArgument_t287865710_0_0_0 };
extern const Il2CppGenericInst GenInst_CustomAttributeNamedArgument_t287865710_0_0_0_CustomAttributeNamedArgument_t287865710_0_0_0 = { 2, GenInst_CustomAttributeNamedArgument_t287865710_0_0_0_CustomAttributeNamedArgument_t287865710_0_0_0_Types };
static const Il2CppType* GenInst_CustomAttributeTypedArgument_t2723150157_0_0_0_CustomAttributeTypedArgument_t2723150157_0_0_0_Types[] = { &CustomAttributeTypedArgument_t2723150157_0_0_0, &CustomAttributeTypedArgument_t2723150157_0_0_0 };
extern const Il2CppGenericInst GenInst_CustomAttributeTypedArgument_t2723150157_0_0_0_CustomAttributeTypedArgument_t2723150157_0_0_0 = { 2, GenInst_CustomAttributeTypedArgument_t2723150157_0_0_0_CustomAttributeTypedArgument_t2723150157_0_0_0_Types };
static const Il2CppType* GenInst_Single_t1397266774_0_0_0_Single_t1397266774_0_0_0_Types[] = { &Single_t1397266774_0_0_0, &Single_t1397266774_0_0_0 };
extern const Il2CppGenericInst GenInst_Single_t1397266774_0_0_0_Single_t1397266774_0_0_0 = { 2, GenInst_Single_t1397266774_0_0_0_Single_t1397266774_0_0_0_Types };
static const Il2CppType* GenInst_SpriteData_t3048397587_0_0_0_SpriteData_t3048397587_0_0_0_Types[] = { &SpriteData_t3048397587_0_0_0, &SpriteData_t3048397587_0_0_0 };
extern const Il2CppGenericInst GenInst_SpriteData_t3048397587_0_0_0_SpriteData_t3048397587_0_0_0 = { 2, GenInst_SpriteData_t3048397587_0_0_0_SpriteData_t3048397587_0_0_0_Types };
static const Il2CppType* GenInst_AnimatorClipInfo_t3156717155_0_0_0_AnimatorClipInfo_t3156717155_0_0_0_Types[] = { &AnimatorClipInfo_t3156717155_0_0_0, &AnimatorClipInfo_t3156717155_0_0_0 };
extern const Il2CppGenericInst GenInst_AnimatorClipInfo_t3156717155_0_0_0_AnimatorClipInfo_t3156717155_0_0_0 = { 2, GenInst_AnimatorClipInfo_t3156717155_0_0_0_AnimatorClipInfo_t3156717155_0_0_0_Types };
static const Il2CppType* GenInst_Color32_t2600501292_0_0_0_Color32_t2600501292_0_0_0_Types[] = { &Color32_t2600501292_0_0_0, &Color32_t2600501292_0_0_0 };
extern const Il2CppGenericInst GenInst_Color32_t2600501292_0_0_0_Color32_t2600501292_0_0_0 = { 2, GenInst_Color32_t2600501292_0_0_0_Color32_t2600501292_0_0_0_Types };
static const Il2CppType* GenInst_RaycastResult_t3360306849_0_0_0_RaycastResult_t3360306849_0_0_0_Types[] = { &RaycastResult_t3360306849_0_0_0, &RaycastResult_t3360306849_0_0_0 };
extern const Il2CppGenericInst GenInst_RaycastResult_t3360306849_0_0_0_RaycastResult_t3360306849_0_0_0 = { 2, GenInst_RaycastResult_t3360306849_0_0_0_RaycastResult_t3360306849_0_0_0_Types };
static const Il2CppType* GenInst_UICharInfo_t75501106_0_0_0_UICharInfo_t75501106_0_0_0_Types[] = { &UICharInfo_t75501106_0_0_0, &UICharInfo_t75501106_0_0_0 };
extern const Il2CppGenericInst GenInst_UICharInfo_t75501106_0_0_0_UICharInfo_t75501106_0_0_0 = { 2, GenInst_UICharInfo_t75501106_0_0_0_UICharInfo_t75501106_0_0_0_Types };
static const Il2CppType* GenInst_UILineInfo_t4195266810_0_0_0_UILineInfo_t4195266810_0_0_0_Types[] = { &UILineInfo_t4195266810_0_0_0, &UILineInfo_t4195266810_0_0_0 };
extern const Il2CppGenericInst GenInst_UILineInfo_t4195266810_0_0_0_UILineInfo_t4195266810_0_0_0 = { 2, GenInst_UILineInfo_t4195266810_0_0_0_UILineInfo_t4195266810_0_0_0_Types };
static const Il2CppType* GenInst_UIVertex_t4057497605_0_0_0_UIVertex_t4057497605_0_0_0_Types[] = { &UIVertex_t4057497605_0_0_0, &UIVertex_t4057497605_0_0_0 };
extern const Il2CppGenericInst GenInst_UIVertex_t4057497605_0_0_0_UIVertex_t4057497605_0_0_0 = { 2, GenInst_UIVertex_t4057497605_0_0_0_UIVertex_t4057497605_0_0_0_Types };
static const Il2CppType* GenInst_Vector2_t2156229523_0_0_0_Vector2_t2156229523_0_0_0_Types[] = { &Vector2_t2156229523_0_0_0, &Vector2_t2156229523_0_0_0 };
extern const Il2CppGenericInst GenInst_Vector2_t2156229523_0_0_0_Vector2_t2156229523_0_0_0 = { 2, GenInst_Vector2_t2156229523_0_0_0_Vector2_t2156229523_0_0_0_Types };
static const Il2CppType* GenInst_Vector3_t3722313464_0_0_0_Vector3_t3722313464_0_0_0_Types[] = { &Vector3_t3722313464_0_0_0, &Vector3_t3722313464_0_0_0 };
extern const Il2CppGenericInst GenInst_Vector3_t3722313464_0_0_0_Vector3_t3722313464_0_0_0 = { 2, GenInst_Vector3_t3722313464_0_0_0_Vector3_t3722313464_0_0_0_Types };
static const Il2CppType* GenInst_Vector4_t3319028937_0_0_0_Vector4_t3319028937_0_0_0_Types[] = { &Vector4_t3319028937_0_0_0, &Vector4_t3319028937_0_0_0 };
extern const Il2CppGenericInst GenInst_Vector4_t3319028937_0_0_0_Vector4_t3319028937_0_0_0 = { 2, GenInst_Vector4_t3319028937_0_0_0_Vector4_t3319028937_0_0_0_Types };
static const Il2CppType* GenInst_SimulatedTarget_t1564170948_0_0_0_SimulatedTarget_t1564170948_0_0_0_Types[] = { &SimulatedTarget_t1564170948_0_0_0, &SimulatedTarget_t1564170948_0_0_0 };
extern const Il2CppGenericInst GenInst_SimulatedTarget_t1564170948_0_0_0_SimulatedTarget_t1564170948_0_0_0 = { 2, GenInst_SimulatedTarget_t1564170948_0_0_0_SimulatedTarget_t1564170948_0_0_0_Types };
static const Il2CppType* GenInst_Boolean_t97287965_0_0_0_Boolean_t97287965_0_0_0_Types[] = { &Boolean_t97287965_0_0_0, &Boolean_t97287965_0_0_0 };
extern const Il2CppGenericInst GenInst_Boolean_t97287965_0_0_0_Boolean_t97287965_0_0_0 = { 2, GenInst_Boolean_t97287965_0_0_0_Boolean_t97287965_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t1383673463_0_0_0_KeyValuePair_2_t1383673463_0_0_0_Types[] = { &KeyValuePair_2_t1383673463_0_0_0, &KeyValuePair_2_t1383673463_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1383673463_0_0_0_KeyValuePair_2_t1383673463_0_0_0 = { 2, GenInst_KeyValuePair_2_t1383673463_0_0_0_KeyValuePair_2_t1383673463_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t1383673463_0_0_0_Il2CppObject_0_0_0_Types[] = { &KeyValuePair_2_t1383673463_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1383673463_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_KeyValuePair_2_t1383673463_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_Char_t3634460470_0_0_0_Il2CppObject_0_0_0_Types[] = { &Char_t3634460470_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_Char_t3634460470_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_Char_t3634460470_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t625878672_0_0_0_KeyValuePair_2_t625878672_0_0_0_Types[] = { &KeyValuePair_2_t625878672_0_0_0, &KeyValuePair_2_t625878672_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t625878672_0_0_0_KeyValuePair_2_t625878672_0_0_0 = { 2, GenInst_KeyValuePair_2_t625878672_0_0_0_KeyValuePair_2_t625878672_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t625878672_0_0_0_Il2CppObject_0_0_0_Types[] = { &KeyValuePair_2_t625878672_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t625878672_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_KeyValuePair_2_t625878672_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t4237331251_0_0_0_KeyValuePair_2_t4237331251_0_0_0_Types[] = { &KeyValuePair_2_t4237331251_0_0_0, &KeyValuePair_2_t4237331251_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t4237331251_0_0_0_KeyValuePair_2_t4237331251_0_0_0 = { 2, GenInst_KeyValuePair_2_t4237331251_0_0_0_KeyValuePair_2_t4237331251_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t4237331251_0_0_0_Il2CppObject_0_0_0_Types[] = { &KeyValuePair_2_t4237331251_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t4237331251_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_KeyValuePair_2_t4237331251_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t727985506_0_0_0_KeyValuePair_2_t727985506_0_0_0_Types[] = { &KeyValuePair_2_t727985506_0_0_0, &KeyValuePair_2_t727985506_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t727985506_0_0_0_KeyValuePair_2_t727985506_0_0_0 = { 2, GenInst_KeyValuePair_2_t727985506_0_0_0_KeyValuePair_2_t727985506_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t727985506_0_0_0_Il2CppObject_0_0_0_Types[] = { &KeyValuePair_2_t727985506_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t727985506_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_KeyValuePair_2_t727985506_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_Int64_t3736567304_0_0_0_Int64_t3736567304_0_0_0_Types[] = { &Int64_t3736567304_0_0_0, &Int64_t3736567304_0_0_0 };
extern const Il2CppGenericInst GenInst_Int64_t3736567304_0_0_0_Int64_t3736567304_0_0_0 = { 2, GenInst_Int64_t3736567304_0_0_0_Int64_t3736567304_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t71524366_0_0_0_KeyValuePair_2_t71524366_0_0_0_Types[] = { &KeyValuePair_2_t71524366_0_0_0, &KeyValuePair_2_t71524366_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t71524366_0_0_0_KeyValuePair_2_t71524366_0_0_0 = { 2, GenInst_KeyValuePair_2_t71524366_0_0_0_KeyValuePair_2_t71524366_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t71524366_0_0_0_Il2CppObject_0_0_0_Types[] = { &KeyValuePair_2_t71524366_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t71524366_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_KeyValuePair_2_t71524366_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t2245450819_0_0_0_KeyValuePair_2_t2245450819_0_0_0_Types[] = { &KeyValuePair_2_t2245450819_0_0_0, &KeyValuePair_2_t2245450819_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2245450819_0_0_0_KeyValuePair_2_t2245450819_0_0_0 = { 2, GenInst_KeyValuePair_2_t2245450819_0_0_0_KeyValuePair_2_t2245450819_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t2245450819_0_0_0_Il2CppObject_0_0_0_Types[] = { &KeyValuePair_2_t2245450819_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2245450819_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_KeyValuePair_2_t2245450819_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t3842366416_0_0_0_KeyValuePair_2_t3842366416_0_0_0_Types[] = { &KeyValuePair_2_t3842366416_0_0_0, &KeyValuePair_2_t3842366416_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3842366416_0_0_0_KeyValuePair_2_t3842366416_0_0_0 = { 2, GenInst_KeyValuePair_2_t3842366416_0_0_0_KeyValuePair_2_t3842366416_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t3842366416_0_0_0_Il2CppObject_0_0_0_Types[] = { &KeyValuePair_2_t3842366416_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3842366416_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_KeyValuePair_2_t3842366416_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t2401056908_0_0_0_KeyValuePair_2_t2401056908_0_0_0_Types[] = { &KeyValuePair_2_t2401056908_0_0_0, &KeyValuePair_2_t2401056908_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2401056908_0_0_0_KeyValuePair_2_t2401056908_0_0_0 = { 2, GenInst_KeyValuePair_2_t2401056908_0_0_0_KeyValuePair_2_t2401056908_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t2401056908_0_0_0_Il2CppObject_0_0_0_Types[] = { &KeyValuePair_2_t2401056908_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2401056908_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_KeyValuePair_2_t2401056908_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t2530217319_0_0_0_KeyValuePair_2_t2530217319_0_0_0_Types[] = { &KeyValuePair_2_t2530217319_0_0_0, &KeyValuePair_2_t2530217319_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2530217319_0_0_0_KeyValuePair_2_t2530217319_0_0_0 = { 2, GenInst_KeyValuePair_2_t2530217319_0_0_0_KeyValuePair_2_t2530217319_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t2530217319_0_0_0_Il2CppObject_0_0_0_Types[] = { &KeyValuePair_2_t2530217319_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2530217319_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_KeyValuePair_2_t2530217319_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t847377929_0_0_0_KeyValuePair_2_t847377929_0_0_0_Types[] = { &KeyValuePair_2_t847377929_0_0_0, &KeyValuePair_2_t847377929_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t847377929_0_0_0_KeyValuePair_2_t847377929_0_0_0 = { 2, GenInst_KeyValuePair_2_t847377929_0_0_0_KeyValuePair_2_t847377929_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t847377929_0_0_0_Il2CppObject_0_0_0_Types[] = { &KeyValuePair_2_t847377929_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t847377929_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_KeyValuePair_2_t847377929_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_Single_t1397266774_0_0_0_Il2CppObject_0_0_0_Types[] = { &Single_t1397266774_0_0_0, &Il2CppObject_0_0_0 };
extern const Il2CppGenericInst GenInst_Single_t1397266774_0_0_0_Il2CppObject_0_0_0 = { 2, GenInst_Single_t1397266774_0_0_0_Il2CppObject_0_0_0_Types };
static const Il2CppType* GenInst_ExtendedTrackingQuality_t364496425_0_0_0_Types[] = { &ExtendedTrackingQuality_t364496425_0_0_0 };
extern const Il2CppGenericInst GenInst_ExtendedTrackingQuality_t364496425_0_0_0 = { 1, GenInst_ExtendedTrackingQuality_t364496425_0_0_0_Types };
extern const Il2CppGenericInst* const g_Il2CppGenericInstTable[1009] = 
{
	&GenInst_Il2CppObject_0_0_0,
	&GenInst_Int32_t2950945753_0_0_0,
	&GenInst_Char_t3634460470_0_0_0,
	&GenInst_Int64_t3736567304_0_0_0,
	&GenInst_UInt32_t2560061978_0_0_0,
	&GenInst_UInt64_t4134040092_0_0_0,
	&GenInst_Byte_t1134296376_0_0_0,
	&GenInst_SByte_t1669577662_0_0_0,
	&GenInst_Int16_t2552820387_0_0_0,
	&GenInst_UInt16_t2177724958_0_0_0,
	&GenInst_String_t_0_0_0,
	&GenInst_IConvertible_t2977365677_0_0_0,
	&GenInst_IComparable_t36111218_0_0_0,
	&GenInst_IEnumerable_t1941168011_0_0_0,
	&GenInst_ICloneable_t724424198_0_0_0,
	&GenInst_IComparable_1_t1216115102_0_0_0,
	&GenInst_IEquatable_1_t2738596416_0_0_0,
	&GenInst_Type_t_0_0_0,
	&GenInst_IReflect_t2554276939_0_0_0,
	&GenInst__Type_t3588564251_0_0_0,
	&GenInst_MemberInfo_t_0_0_0,
	&GenInst_ICustomAttributeProvider_t1530824137_0_0_0,
	&GenInst__MemberInfo_t3922476713_0_0_0,
	&GenInst_Double_t594665363_0_0_0,
	&GenInst_Single_t1397266774_0_0_0,
	&GenInst_Decimal_t2948259380_0_0_0,
	&GenInst_Boolean_t97287965_0_0_0,
	&GenInst_Delegate_t1188392813_0_0_0,
	&GenInst_ISerializable_t3375760802_0_0_0,
	&GenInst_ParameterInfo_t1861056598_0_0_0,
	&GenInst__ParameterInfo_t489405856_0_0_0,
	&GenInst_ParameterModifier_t1461694466_0_0_0,
	&GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0,
	&GenInst_FieldInfo_t_0_0_0,
	&GenInst__FieldInfo_t2781946373_0_0_0,
	&GenInst_MethodInfo_t_0_0_0,
	&GenInst__MethodInfo_t3550065504_0_0_0,
	&GenInst_MethodBase_t609368412_0_0_0,
	&GenInst__MethodBase_t1657248248_0_0_0,
	&GenInst_PropertyInfo_t_0_0_0,
	&GenInst__PropertyInfo_t4070324388_0_0_0,
	&GenInst_ConstructorInfo_t5769829_0_0_0,
	&GenInst__ConstructorInfo_t3357543833_0_0_0,
	&GenInst_IntPtr_t_0_0_0,
	&GenInst_TableRange_t3332867892_0_0_0,
	&GenInst_TailoringInfo_t866433654_0_0_0,
	&GenInst_String_t_0_0_0_Int32_t2950945753_0_0_0,
	&GenInst_Il2CppObject_0_0_0_Int32_t2950945753_0_0_0,
	&GenInst_KeyValuePair_2_t2401056908_0_0_0,
	&GenInst_Link_t544317964_0_0_0,
	&GenInst_Il2CppObject_0_0_0_Int32_t2950945753_0_0_0_Int32_t2950945753_0_0_0,
	&GenInst_Il2CppObject_0_0_0_Int32_t2950945753_0_0_0_DictionaryEntry_t3123975638_0_0_0,
	&GenInst_DictionaryEntry_t3123975638_0_0_0,
	&GenInst_Il2CppObject_0_0_0_Int32_t2950945753_0_0_0_KeyValuePair_2_t2401056908_0_0_0,
	&GenInst_String_t_0_0_0_Int32_t2950945753_0_0_0_DictionaryEntry_t3123975638_0_0_0,
	&GenInst_KeyValuePair_2_t838906923_0_0_0,
	&GenInst_String_t_0_0_0_Int32_t2950945753_0_0_0_KeyValuePair_2_t838906923_0_0_0,
	&GenInst_String_t_0_0_0_Int32_t2950945753_0_0_0_Int32_t2950945753_0_0_0,
	&GenInst_Contraction_t1589275354_0_0_0,
	&GenInst_Level2Map_t3640798870_0_0_0,
	&GenInst_BigInteger_t2902905089_0_0_0,
	&GenInst_KeySizes_t85027896_0_0_0,
	&GenInst_KeyValuePair_2_t2530217319_0_0_0,
	&GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0,
	&GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t3123975638_0_0_0,
	&GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0_KeyValuePair_2_t2530217319_0_0_0,
	&GenInst_Slot_t3975888750_0_0_0,
	&GenInst_Slot_t384495010_0_0_0,
	&GenInst_StackFrame_t3217253059_0_0_0,
	&GenInst_Calendar_t1661121569_0_0_0,
	&GenInst_ModuleBuilder_t731887691_0_0_0,
	&GenInst__ModuleBuilder_t3217089703_0_0_0,
	&GenInst_Module_t2987026101_0_0_0,
	&GenInst__Module_t135161706_0_0_0,
	&GenInst_ParameterBuilder_t1137139675_0_0_0,
	&GenInst__ParameterBuilder_t3901898075_0_0_0,
	&GenInst_TypeU5BU5D_t3940880105_0_0_0,
	&GenInst_Il2CppArray_0_0_0,
	&GenInst_ICollection_t3904884886_0_0_0,
	&GenInst_IList_t2094931216_0_0_0,
	&GenInst_IList_1_t4297247_0_0_0,
	&GenInst_ICollection_1_t1017129698_0_0_0,
	&GenInst_IEnumerable_1_t1463797649_0_0_0,
	&GenInst_IList_1_t74629426_0_0_0,
	&GenInst_ICollection_1_t1087461877_0_0_0,
	&GenInst_IEnumerable_1_t1534129828_0_0_0,
	&GenInst_IList_1_t1108916738_0_0_0,
	&GenInst_ICollection_1_t2121749189_0_0_0,
	&GenInst_IEnumerable_1_t2568417140_0_0_0,
	&GenInst_IList_1_t900354228_0_0_0,
	&GenInst_ICollection_1_t1913186679_0_0_0,
	&GenInst_IEnumerable_1_t2359854630_0_0_0,
	&GenInst_IList_1_t3346143920_0_0_0,
	&GenInst_ICollection_1_t64009075_0_0_0,
	&GenInst_IEnumerable_1_t510677026_0_0_0,
	&GenInst_IList_1_t1442829200_0_0_0,
	&GenInst_ICollection_1_t2455661651_0_0_0,
	&GenInst_IEnumerable_1_t2902329602_0_0_0,
	&GenInst_IList_1_t600458651_0_0_0,
	&GenInst_ICollection_1_t1613291102_0_0_0,
	&GenInst_IEnumerable_1_t2059959053_0_0_0,
	&GenInst_ILTokenInfo_t2325775114_0_0_0,
	&GenInst_LabelData_t360167391_0_0_0,
	&GenInst_LabelFixup_t858502054_0_0_0,
	&GenInst_GenericTypeParameterBuilder_t1988827940_0_0_0,
	&GenInst_TypeBuilder_t1073948154_0_0_0,
	&GenInst__TypeBuilder_t2501637272_0_0_0,
	&GenInst_MethodBuilder_t2807316753_0_0_0,
	&GenInst__MethodBuilder_t600455149_0_0_0,
	&GenInst_ConstructorBuilder_t2813524108_0_0_0,
	&GenInst__ConstructorBuilder_t2416550571_0_0_0,
	&GenInst_PropertyBuilder_t314297007_0_0_0,
	&GenInst__PropertyBuilder_t1366136710_0_0_0,
	&GenInst_FieldBuilder_t2627049993_0_0_0,
	&GenInst__FieldBuilder_t2615792726_0_0_0,
	&GenInst_CustomAttributeTypedArgument_t2723150157_0_0_0,
	&GenInst_CustomAttributeNamedArgument_t287865710_0_0_0,
	&GenInst_CustomAttributeData_t1084486650_0_0_0,
	&GenInst_ResourceInfo_t2872965302_0_0_0,
	&GenInst_ResourceCacheItem_t51292791_0_0_0,
	&GenInst_IContextProperty_t840037424_0_0_0,
	&GenInst_Header_t549724581_0_0_0,
	&GenInst_ITrackingHandler_t1244553475_0_0_0,
	&GenInst_IContextAttribute_t176678928_0_0_0,
	&GenInst_DateTime_t3738529785_0_0_0,
	&GenInst_TimeSpan_t881159249_0_0_0,
	&GenInst_TypeTag_t3541821701_0_0_0,
	&GenInst_MonoType_t_0_0_0,
	&GenInst_StrongName_t3675724614_0_0_0,
	&GenInst_IBuiltInEvidence_t554693121_0_0_0,
	&GenInst_IIdentityPermissionFactory_t3268650966_0_0_0,
	&GenInst_DateTimeOffset_t3229287507_0_0_0,
	&GenInst_Guid_t_0_0_0,
	&GenInst_Version_t3456873960_0_0_0,
	&GenInst_BigInteger_t2902905090_0_0_0,
	&GenInst_ByteU5BU5D_t4116647657_0_0_0,
	&GenInst_IList_1_t2949616159_0_0_0,
	&GenInst_ICollection_1_t3962448610_0_0_0,
	&GenInst_IEnumerable_1_t114149265_0_0_0,
	&GenInst_X509Certificate_t713131622_0_0_0,
	&GenInst_IDeserializationCallback_t4220500054_0_0_0,
	&GenInst_ClientCertificateType_t1004704908_0_0_0,
	&GenInst_String_t_0_0_0_Boolean_t97287965_0_0_0,
	&GenInst_Il2CppObject_0_0_0_Boolean_t97287965_0_0_0,
	&GenInst_KeyValuePair_2_t3842366416_0_0_0,
	&GenInst_Il2CppObject_0_0_0_Boolean_t97287965_0_0_0_Boolean_t97287965_0_0_0,
	&GenInst_Il2CppObject_0_0_0_Boolean_t97287965_0_0_0_DictionaryEntry_t3123975638_0_0_0,
	&GenInst_Il2CppObject_0_0_0_Boolean_t97287965_0_0_0_KeyValuePair_2_t3842366416_0_0_0,
	&GenInst_String_t_0_0_0_Boolean_t97287965_0_0_0_DictionaryEntry_t3123975638_0_0_0,
	&GenInst_KeyValuePair_2_t2280216431_0_0_0,
	&GenInst_String_t_0_0_0_Boolean_t97287965_0_0_0_KeyValuePair_2_t2280216431_0_0_0,
	&GenInst_String_t_0_0_0_Boolean_t97287965_0_0_0_Boolean_t97287965_0_0_0,
	&GenInst_X509ChainStatus_t133602714_0_0_0,
	&GenInst_Capture_t2232016050_0_0_0,
	&GenInst_Group_t2468205786_0_0_0,
	&GenInst_Int32_t2950945753_0_0_0_Int32_t2950945753_0_0_0,
	&GenInst_KeyValuePair_2_t4237331251_0_0_0,
	&GenInst_Int32_t2950945753_0_0_0_Int32_t2950945753_0_0_0_Int32_t2950945753_0_0_0,
	&GenInst_Int32_t2950945753_0_0_0_Int32_t2950945753_0_0_0_DictionaryEntry_t3123975638_0_0_0,
	&GenInst_Int32_t2950945753_0_0_0_Int32_t2950945753_0_0_0_KeyValuePair_2_t4237331251_0_0_0,
	&GenInst_Mark_t3471605523_0_0_0,
	&GenInst_UriScheme_t722425697_0_0_0,
	&GenInst_Link_t3209266973_0_0_0,
	&GenInst_Object_t631007953_0_0_0,
	&GenInst_Camera_t4157153871_0_0_0,
	&GenInst_Behaviour_t1437897464_0_0_0,
	&GenInst_Component_t1923634451_0_0_0,
	&GenInst_Display_t1387065949_0_0_0,
	&GenInst_Boolean_t97287965_0_0_0_String_t_0_0_0,
	&GenInst_Boolean_t97287965_0_0_0_Il2CppObject_0_0_0,
	&GenInst_AchievementDescription_t3217594527_0_0_0,
	&GenInst_IAchievementDescription_t2514275728_0_0_0,
	&GenInst_UserProfile_t3137328177_0_0_0,
	&GenInst_IUserProfile_t360500636_0_0_0,
	&GenInst_GcLeaderboard_t4132273028_0_0_0,
	&GenInst_IAchievementDescriptionU5BU5D_t1821964849_0_0_0,
	&GenInst_IAchievementU5BU5D_t1892338339_0_0_0,
	&GenInst_IAchievement_t1421108358_0_0_0,
	&GenInst_GcAchievementData_t675222246_0_0_0,
	&GenInst_Achievement_t565359984_0_0_0,
	&GenInst_IScoreU5BU5D_t527871248_0_0_0,
	&GenInst_IScore_t2559910621_0_0_0,
	&GenInst_GcScoreData_t2125309831_0_0_0,
	&GenInst_Score_t1968645328_0_0_0,
	&GenInst_IUserProfileU5BU5D_t909679733_0_0_0,
	&GenInst_GameObject_t1113636619_0_0_0,
	&GenInst_Material_t340375123_0_0_0,
	&GenInst_LightmapData_t2568046604_0_0_0,
	&GenInst_Keyframe_t4206410242_0_0_0,
	&GenInst_Vector3_t3722313464_0_0_0,
	&GenInst_Vector4_t3319028937_0_0_0,
	&GenInst_Vector2_t2156229523_0_0_0,
	&GenInst_Color_t2555686324_0_0_0,
	&GenInst_Color32_t2600501292_0_0_0,
	&GenInst_Scene_t2348375561_0_0_0_LoadSceneMode_t3251202195_0_0_0,
	&GenInst_Scene_t2348375561_0_0_0,
	&GenInst_Scene_t2348375561_0_0_0_Scene_t2348375561_0_0_0,
	&GenInst_Particle_t1882894987_0_0_0,
	&GenInst_ContactPoint_t3758755253_0_0_0,
	&GenInst_RaycastHit_t1056001966_0_0_0,
	&GenInst_Collider_t1773347010_0_0_0,
	&GenInst_Rigidbody2D_t939494601_0_0_0,
	&GenInst_RaycastHit2D_t2279581989_0_0_0,
	&GenInst_ContactPoint2D_t3390240644_0_0_0,
	&GenInst_WebCamDevice_t1322781432_0_0_0,
	&GenInst_AnimatorClipInfo_t3156717155_0_0_0,
	&GenInst_AnimatorControllerParameter_t1758260042_0_0_0,
	&GenInst_UIVertex_t4057497605_0_0_0,
	&GenInst_UICharInfo_t75501106_0_0_0,
	&GenInst_UILineInfo_t4195266810_0_0_0,
	&GenInst_Font_t1956802104_0_0_0,
	&GenInst_GUILayoutOption_t811797299_0_0_0,
	&GenInst_GUILayoutEntry_t3214611570_0_0_0,
	&GenInst_Int32_t2950945753_0_0_0_LayoutCache_t78309876_0_0_0,
	&GenInst_Int32_t2950945753_0_0_0_Il2CppObject_0_0_0,
	&GenInst_KeyValuePair_2_t71524366_0_0_0,
	&GenInst_Int32_t2950945753_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0,
	&GenInst_Int32_t2950945753_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t3123975638_0_0_0,
	&GenInst_Int32_t2950945753_0_0_0_Il2CppObject_0_0_0_KeyValuePair_2_t71524366_0_0_0,
	&GenInst_LayoutCache_t78309876_0_0_0,
	&GenInst_Int32_t2950945753_0_0_0_LayoutCache_t78309876_0_0_0_DictionaryEntry_t3123975638_0_0_0,
	&GenInst_KeyValuePair_2_t1364695374_0_0_0,
	&GenInst_Int32_t2950945753_0_0_0_LayoutCache_t78309876_0_0_0_KeyValuePair_2_t1364695374_0_0_0,
	&GenInst_Int32_t2950945753_0_0_0_LayoutCache_t78309876_0_0_0_LayoutCache_t78309876_0_0_0,
	&GenInst_GUIStyle_t3956901511_0_0_0,
	&GenInst_String_t_0_0_0_GUIStyle_t3956901511_0_0_0,
	&GenInst_String_t_0_0_0_GUIStyle_t3956901511_0_0_0_DictionaryEntry_t3123975638_0_0_0,
	&GenInst_KeyValuePair_2_t1844862681_0_0_0,
	&GenInst_String_t_0_0_0_GUIStyle_t3956901511_0_0_0_KeyValuePair_2_t1844862681_0_0_0,
	&GenInst_String_t_0_0_0_GUIStyle_t3956901511_0_0_0_GUIStyle_t3956901511_0_0_0,
	&GenInst_DisallowMultipleComponent_t1422053217_0_0_0,
	&GenInst_Attribute_t861562559_0_0_0,
	&GenInst__Attribute_t122494719_0_0_0,
	&GenInst_ExecuteInEditMode_t3727731349_0_0_0,
	&GenInst_RequireComponent_t3490506609_0_0_0,
	&GenInst_HitInfo_t3229609740_0_0_0,
	&GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0,
	&GenInst_PersistentCall_t3407714124_0_0_0,
	&GenInst_BaseInvokableCall_t2703961024_0_0_0,
	&GenInst_MessageTypeSubscribers_t1684935770_0_0_0,
	&GenInst_MessageTypeSubscribers_t1684935770_0_0_0_Boolean_t97287965_0_0_0,
	&GenInst_MessageEventArgs_t1170575784_0_0_0,
	&GenInst_BaseInputModule_t2019268878_0_0_0,
	&GenInst_UIBehaviour_t3495933518_0_0_0,
	&GenInst_MonoBehaviour_t3962482529_0_0_0,
	&GenInst_RaycastResult_t3360306849_0_0_0,
	&GenInst_IDeselectHandler_t393712923_0_0_0,
	&GenInst_IEventSystemHandler_t3354683850_0_0_0,
	&GenInst_List_1_t531791296_0_0_0,
	&GenInst_List_1_t257213610_0_0_0,
	&GenInst_List_1_t3395709193_0_0_0,
	&GenInst_ISelectHandler_t2271418839_0_0_0,
	&GenInst_BaseRaycaster_t4150874583_0_0_0,
	&GenInst_Entry_t3344766165_0_0_0,
	&GenInst_BaseEventData_t3903027533_0_0_0,
	&GenInst_IPointerEnterHandler_t1016128679_0_0_0,
	&GenInst_IPointerExitHandler_t4182793654_0_0_0,
	&GenInst_IPointerDownHandler_t1380080529_0_0_0,
	&GenInst_IPointerUpHandler_t277099170_0_0_0,
	&GenInst_IPointerClickHandler_t132471142_0_0_0,
	&GenInst_IInitializePotentialDragHandler_t608041180_0_0_0,
	&GenInst_IBeginDragHandler_t3293314358_0_0_0,
	&GenInst_IDragHandler_t2288426503_0_0_0,
	&GenInst_IEndDragHandler_t297508562_0_0_0,
	&GenInst_IDropHandler_t3627139509_0_0_0,
	&GenInst_IScrollHandler_t4201797704_0_0_0,
	&GenInst_IUpdateSelectedHandler_t4266291469_0_0_0,
	&GenInst_IMoveHandler_t933334182_0_0_0,
	&GenInst_ISubmitHandler_t2790798304_0_0_0,
	&GenInst_ICancelHandler_t3974364820_0_0_0,
	&GenInst_Transform_t3600365921_0_0_0,
	&GenInst_BaseInput_t3630163547_0_0_0,
	&GenInst_Int32_t2950945753_0_0_0_PointerEventData_t3807901092_0_0_0,
	&GenInst_PointerEventData_t3807901092_0_0_0,
	&GenInst_AbstractEventData_t4171500731_0_0_0,
	&GenInst_Int32_t2950945753_0_0_0_PointerEventData_t3807901092_0_0_0_DictionaryEntry_t3123975638_0_0_0,
	&GenInst_KeyValuePair_2_t799319294_0_0_0,
	&GenInst_Int32_t2950945753_0_0_0_PointerEventData_t3807901092_0_0_0_KeyValuePair_2_t799319294_0_0_0,
	&GenInst_Int32_t2950945753_0_0_0_PointerEventData_t3807901092_0_0_0_PointerEventData_t3807901092_0_0_0,
	&GenInst_ButtonState_t857139936_0_0_0,
	&GenInst_ICanvasElement_t2121898866_0_0_0,
	&GenInst_ICanvasElement_t2121898866_0_0_0_Int32_t2950945753_0_0_0,
	&GenInst_ICanvasElement_t2121898866_0_0_0_Int32_t2950945753_0_0_0_DictionaryEntry_t3123975638_0_0_0,
	&GenInst_KeyValuePair_2_t1364477206_0_0_0,
	&GenInst_ICanvasElement_t2121898866_0_0_0_Int32_t2950945753_0_0_0_KeyValuePair_2_t1364477206_0_0_0,
	&GenInst_ICanvasElement_t2121898866_0_0_0_Int32_t2950945753_0_0_0_Int32_t2950945753_0_0_0,
	&GenInst_ColorBlock_t2139031574_0_0_0,
	&GenInst_OptionData_t3270282352_0_0_0,
	&GenInst_DropdownItem_t1451952895_0_0_0,
	&GenInst_FloatTween_t1274330004_0_0_0,
	&GenInst_Sprite_t280657092_0_0_0,
	&GenInst_Canvas_t3310196443_0_0_0,
	&GenInst_List_1_t487303889_0_0_0,
	&GenInst_Font_t1956802104_0_0_0_HashSet_1_t466832188_0_0_0,
	&GenInst_Text_t1901882714_0_0_0,
	&GenInst_Link_t2031043523_0_0_0,
	&GenInst_ILayoutElement_t4082016710_0_0_0,
	&GenInst_MaskableGraphic_t3839221559_0_0_0,
	&GenInst_IClippable_t1239629351_0_0_0,
	&GenInst_IMaskable_t433386433_0_0_0,
	&GenInst_IMaterialModifier_t1975025690_0_0_0,
	&GenInst_Graphic_t1660335611_0_0_0,
	&GenInst_HashSet_1_t466832188_0_0_0,
	&GenInst_Font_t1956802104_0_0_0_HashSet_1_t466832188_0_0_0_DictionaryEntry_t3123975638_0_0_0,
	&GenInst_KeyValuePair_2_t2767015899_0_0_0,
	&GenInst_Font_t1956802104_0_0_0_HashSet_1_t466832188_0_0_0_KeyValuePair_2_t2767015899_0_0_0,
	&GenInst_Font_t1956802104_0_0_0_HashSet_1_t466832188_0_0_0_HashSet_1_t466832188_0_0_0,
	&GenInst_ColorTween_t809614380_0_0_0,
	&GenInst_Canvas_t3310196443_0_0_0_IndexedSet_1_t3109723551_0_0_0,
	&GenInst_Graphic_t1660335611_0_0_0_Int32_t2950945753_0_0_0,
	&GenInst_Graphic_t1660335611_0_0_0_Int32_t2950945753_0_0_0_DictionaryEntry_t3123975638_0_0_0,
	&GenInst_KeyValuePair_2_t3639519817_0_0_0,
	&GenInst_Graphic_t1660335611_0_0_0_Int32_t2950945753_0_0_0_KeyValuePair_2_t3639519817_0_0_0,
	&GenInst_Graphic_t1660335611_0_0_0_Int32_t2950945753_0_0_0_Int32_t2950945753_0_0_0,
	&GenInst_IndexedSet_1_t3109723551_0_0_0,
	&GenInst_Canvas_t3310196443_0_0_0_IndexedSet_1_t3109723551_0_0_0_DictionaryEntry_t3123975638_0_0_0,
	&GenInst_KeyValuePair_2_t398822319_0_0_0,
	&GenInst_Canvas_t3310196443_0_0_0_IndexedSet_1_t3109723551_0_0_0_KeyValuePair_2_t398822319_0_0_0,
	&GenInst_Canvas_t3310196443_0_0_0_IndexedSet_1_t3109723551_0_0_0_IndexedSet_1_t3109723551_0_0_0,
	&GenInst_Type_t1152881528_0_0_0,
	&GenInst_FillMethod_t1167457570_0_0_0,
	&GenInst_ContentType_t1787303396_0_0_0,
	&GenInst_LineType_t4214648469_0_0_0,
	&GenInst_InputType_t1770400679_0_0_0,
	&GenInst_TouchScreenKeyboardType_t1530597702_0_0_0,
	&GenInst_CharacterValidation_t4051914437_0_0_0,
	&GenInst_Mask_t1803652131_0_0_0,
	&GenInst_ICanvasRaycastFilter_t2454702837_0_0_0,
	&GenInst_List_1_t3275726873_0_0_0,
	&GenInst_RectMask2D_t3474889437_0_0_0,
	&GenInst_IClipper_t1224123152_0_0_0,
	&GenInst_List_1_t651996883_0_0_0,
	&GenInst_Navigation_t3049316579_0_0_0,
	&GenInst_Link_t1368790160_0_0_0,
	&GenInst_Direction_t3470714353_0_0_0,
	&GenInst_Selectable_t3250028441_0_0_0,
	&GenInst_Transition_t1769908631_0_0_0,
	&GenInst_SpriteState_t1362986479_0_0_0,
	&GenInst_CanvasGroup_t4083511760_0_0_0,
	&GenInst_Direction_t337909235_0_0_0,
	&GenInst_MatEntry_t2957107092_0_0_0,
	&GenInst_Toggle_t2735377061_0_0_0,
	&GenInst_Toggle_t2735377061_0_0_0_Boolean_t97287965_0_0_0,
	&GenInst_IClipper_t1224123152_0_0_0_Int32_t2950945753_0_0_0,
	&GenInst_IClipper_t1224123152_0_0_0_Int32_t2950945753_0_0_0_DictionaryEntry_t3123975638_0_0_0,
	&GenInst_KeyValuePair_2_t1634190656_0_0_0,
	&GenInst_IClipper_t1224123152_0_0_0_Int32_t2950945753_0_0_0_KeyValuePair_2_t1634190656_0_0_0,
	&GenInst_IClipper_t1224123152_0_0_0_Int32_t2950945753_0_0_0_Int32_t2950945753_0_0_0,
	&GenInst_AspectMode_t3417192999_0_0_0,
	&GenInst_FitMode_t3267881214_0_0_0,
	&GenInst_RectTransform_t3704657025_0_0_0,
	&GenInst_LayoutRebuilder_t541313304_0_0_0,
	&GenInst_ILayoutElement_t4082016710_0_0_0_Single_t1397266774_0_0_0,
	&GenInst_Il2CppObject_0_0_0_Single_t1397266774_0_0_0,
	&GenInst_List_1_t899420910_0_0_0,
	&GenInst_List_1_t4072576034_0_0_0,
	&GenInst_List_1_t3628304265_0_0_0,
	&GenInst_List_1_t496136383_0_0_0,
	&GenInst_List_1_t128053199_0_0_0,
	&GenInst_List_1_t1234605051_0_0_0,
	&GenInst_Action_t1264377477_0_0_0,
	&GenInst_MulticastDelegate_t157516450_0_0_0,
	&GenInst_Action_t1264377477_0_0_0_LinkedListNode_1_t1009552580_0_0_0,
	&GenInst_LinkedListNode_1_t1009552580_0_0_0,
	&GenInst_Action_t1264377477_0_0_0_LinkedListNode_1_t1009552580_0_0_0_DictionaryEntry_t3123975638_0_0_0,
	&GenInst_KeyValuePair_2_t2298961218_0_0_0,
	&GenInst_Action_t1264377477_0_0_0_LinkedListNode_1_t1009552580_0_0_0_KeyValuePair_2_t2298961218_0_0_0,
	&GenInst_Action_t1264377477_0_0_0_LinkedListNode_1_t1009552580_0_0_0_LinkedListNode_1_t1009552580_0_0_0,
	&GenInst_Action_1_t3252573759_0_0_0,
	&GenInst_Action_1_t3252573759_0_0_0_LinkedListNode_1_t2997748862_0_0_0,
	&GenInst_LinkedListNode_1_t2997748862_0_0_0,
	&GenInst_Action_1_t3252573759_0_0_0_LinkedListNode_1_t2997748862_0_0_0_DictionaryEntry_t3123975638_0_0_0,
	&GenInst_KeyValuePair_2_t2444358938_0_0_0,
	&GenInst_Action_1_t3252573759_0_0_0_LinkedListNode_1_t2997748862_0_0_0_KeyValuePair_2_t2444358938_0_0_0,
	&GenInst_Action_1_t3252573759_0_0_0_LinkedListNode_1_t2997748862_0_0_0_LinkedListNode_1_t2997748862_0_0_0,
	&GenInst_Action_2_t2470008838_0_0_0,
	&GenInst_Action_2_t2470008838_0_0_0_LinkedListNode_1_t2215183941_0_0_0,
	&GenInst_LinkedListNode_1_t2215183941_0_0_0,
	&GenInst_Action_2_t2470008838_0_0_0_LinkedListNode_1_t2215183941_0_0_0_DictionaryEntry_t3123975638_0_0_0,
	&GenInst_KeyValuePair_2_t2472372766_0_0_0,
	&GenInst_Action_2_t2470008838_0_0_0_LinkedListNode_1_t2215183941_0_0_0_KeyValuePair_2_t2472372766_0_0_0,
	&GenInst_Action_2_t2470008838_0_0_0_LinkedListNode_1_t2215183941_0_0_0_LinkedListNode_1_t2215183941_0_0_0,
	&GenInst_Action_3_t3632554945_0_0_0,
	&GenInst_Action_3_t3632554945_0_0_0_LinkedListNode_1_t3377730048_0_0_0,
	&GenInst_LinkedListNode_1_t3377730048_0_0_0,
	&GenInst_Action_3_t3632554945_0_0_0_LinkedListNode_1_t3377730048_0_0_0_DictionaryEntry_t3123975638_0_0_0,
	&GenInst_KeyValuePair_2_t2246431954_0_0_0,
	&GenInst_Action_3_t3632554945_0_0_0_LinkedListNode_1_t3377730048_0_0_0_KeyValuePair_2_t2246431954_0_0_0,
	&GenInst_Action_3_t3632554945_0_0_0_LinkedListNode_1_t3377730048_0_0_0_LinkedListNode_1_t3377730048_0_0_0,
	&GenInst_TMP_Sprite_t554067146_0_0_0,
	&GenInst_TMP_TextElement_t129727469_0_0_0,
	&GenInst_Int32_t2950945753_0_0_0_Material_t340375123_0_0_0,
	&GenInst_Int32_t2950945753_0_0_0_Material_t340375123_0_0_0_DictionaryEntry_t3123975638_0_0_0,
	&GenInst_KeyValuePair_2_t1626760621_0_0_0,
	&GenInst_Int32_t2950945753_0_0_0_Material_t340375123_0_0_0_KeyValuePair_2_t1626760621_0_0_0,
	&GenInst_Int32_t2950945753_0_0_0_Material_t340375123_0_0_0_Material_t340375123_0_0_0,
	&GenInst_Int32_t2950945753_0_0_0_TMP_FontAsset_t364381626_0_0_0,
	&GenInst_TMP_FontAsset_t364381626_0_0_0,
	&GenInst_TMP_Asset_t2469957285_0_0_0,
	&GenInst_ScriptableObject_t2528358522_0_0_0,
	&GenInst_Int32_t2950945753_0_0_0_TMP_FontAsset_t364381626_0_0_0_DictionaryEntry_t3123975638_0_0_0,
	&GenInst_KeyValuePair_2_t1650767124_0_0_0,
	&GenInst_Int32_t2950945753_0_0_0_TMP_FontAsset_t364381626_0_0_0_KeyValuePair_2_t1650767124_0_0_0,
	&GenInst_Int32_t2950945753_0_0_0_TMP_FontAsset_t364381626_0_0_0_TMP_FontAsset_t364381626_0_0_0,
	&GenInst_Int32_t2950945753_0_0_0_TMP_SpriteAsset_t484820633_0_0_0,
	&GenInst_TMP_SpriteAsset_t484820633_0_0_0,
	&GenInst_Int32_t2950945753_0_0_0_TMP_SpriteAsset_t484820633_0_0_0_DictionaryEntry_t3123975638_0_0_0,
	&GenInst_KeyValuePair_2_t1771206131_0_0_0,
	&GenInst_Int32_t2950945753_0_0_0_TMP_SpriteAsset_t484820633_0_0_0_KeyValuePair_2_t1771206131_0_0_0,
	&GenInst_Int32_t2950945753_0_0_0_TMP_SpriteAsset_t484820633_0_0_0_TMP_SpriteAsset_t484820633_0_0_0,
	&GenInst_MaterialReference_t1952344632_0_0_0,
	&GenInst_OptionData_t1114640268_0_0_0,
	&GenInst_DropdownItem_t1842596711_0_0_0,
	&GenInst_FloatTween_t3783157226_0_0_0,
	&GenInst_Int32_t2950945753_0_0_0_TMP_Glyph_t581847833_0_0_0,
	&GenInst_TMP_Glyph_t581847833_0_0_0,
	&GenInst_Int32_t2950945753_0_0_0_TMP_Glyph_t581847833_0_0_0_DictionaryEntry_t3123975638_0_0_0,
	&GenInst_KeyValuePair_2_t1868233331_0_0_0,
	&GenInst_Int32_t2950945753_0_0_0_TMP_Glyph_t581847833_0_0_0_KeyValuePair_2_t1868233331_0_0_0,
	&GenInst_Int32_t2950945753_0_0_0_TMP_Glyph_t581847833_0_0_0_TMP_Glyph_t581847833_0_0_0,
	&GenInst_Int32_t2950945753_0_0_0_KerningPair_t2270855589_0_0_0,
	&GenInst_KerningPair_t2270855589_0_0_0,
	&GenInst_Int32_t2950945753_0_0_0_KerningPair_t2270855589_0_0_0_DictionaryEntry_t3123975638_0_0_0,
	&GenInst_KeyValuePair_2_t3557241087_0_0_0,
	&GenInst_Int32_t2950945753_0_0_0_KerningPair_t2270855589_0_0_0_KeyValuePair_2_t3557241087_0_0_0,
	&GenInst_Int32_t2950945753_0_0_0_KerningPair_t2270855589_0_0_0_KerningPair_t2270855589_0_0_0,
	&GenInst_TMP_FontWeights_t916301067_0_0_0,
	&GenInst_TMP_Glyph_t581847833_0_0_0_Int32_t2950945753_0_0_0,
	&GenInst_ContentType_t1128941285_0_0_0,
	&GenInst_Action_1_t803475548_0_0_0,
	&GenInst_Action_1_t803475548_0_0_0_LinkedListNode_1_t548650651_0_0_0,
	&GenInst_LinkedListNode_1_t548650651_0_0_0,
	&GenInst_Action_1_t803475548_0_0_0_LinkedListNode_1_t548650651_0_0_0_DictionaryEntry_t3123975638_0_0_0,
	&GenInst_KeyValuePair_2_t2864070662_0_0_0,
	&GenInst_Action_1_t803475548_0_0_0_LinkedListNode_1_t548650651_0_0_0_KeyValuePair_2_t2864070662_0_0_0,
	&GenInst_Action_1_t803475548_0_0_0_LinkedListNode_1_t548650651_0_0_0_LinkedListNode_1_t548650651_0_0_0,
	&GenInst_TMP_CharacterInfo_t3185626797_0_0_0,
	&GenInst_TMP_LineInfo_t1079631636_0_0_0,
	&GenInst_TMP_WordInfo_t3331066303_0_0_0,
	&GenInst_MaskingMaterial_t16896275_0_0_0,
	&GenInst_Int64_t3736567304_0_0_0_FallbackMaterial_t4171378819_0_0_0,
	&GenInst_Int64_t3736567304_0_0_0_Il2CppObject_0_0_0,
	&GenInst_KeyValuePair_2_t2245450819_0_0_0,
	&GenInst_Int64_t3736567304_0_0_0_Il2CppObject_0_0_0_Il2CppObject_0_0_0,
	&GenInst_Int64_t3736567304_0_0_0_Il2CppObject_0_0_0_DictionaryEntry_t3123975638_0_0_0,
	&GenInst_Int64_t3736567304_0_0_0_Il2CppObject_0_0_0_KeyValuePair_2_t2245450819_0_0_0,
	&GenInst_FallbackMaterial_t4171378819_0_0_0,
	&GenInst_Int64_t3736567304_0_0_0_FallbackMaterial_t4171378819_0_0_0_DictionaryEntry_t3123975638_0_0_0,
	&GenInst_KeyValuePair_2_t3336723474_0_0_0,
	&GenInst_Int64_t3736567304_0_0_0_FallbackMaterial_t4171378819_0_0_0_KeyValuePair_2_t3336723474_0_0_0,
	&GenInst_Int64_t3736567304_0_0_0_FallbackMaterial_t4171378819_0_0_0_FallbackMaterial_t4171378819_0_0_0,
	&GenInst_Int32_t2950945753_0_0_0_Int64_t3736567304_0_0_0,
	&GenInst_KeyValuePair_2_t727985506_0_0_0,
	&GenInst_Int32_t2950945753_0_0_0_Int64_t3736567304_0_0_0_Int64_t3736567304_0_0_0,
	&GenInst_Int32_t2950945753_0_0_0_Int64_t3736567304_0_0_0_DictionaryEntry_t3123975638_0_0_0,
	&GenInst_Int32_t2950945753_0_0_0_Int64_t3736567304_0_0_0_KeyValuePair_2_t727985506_0_0_0,
	&GenInst_List_1_t3447100432_0_0_0,
	&GenInst_Int32_t2950945753_0_0_0_Char_t3634460470_0_0_0,
	&GenInst_KeyValuePair_2_t625878672_0_0_0,
	&GenInst_Int32_t2950945753_0_0_0_Char_t3634460470_0_0_0_Char_t3634460470_0_0_0,
	&GenInst_Int32_t2950945753_0_0_0_Char_t3634460470_0_0_0_DictionaryEntry_t3123975638_0_0_0,
	&GenInst_Int32_t2950945753_0_0_0_Char_t3634460470_0_0_0_KeyValuePair_2_t625878672_0_0_0,
	&GenInst_Int32_t2950945753_0_0_0_Boolean_t97287965_0_0_0,
	&GenInst_KeyValuePair_2_t1383673463_0_0_0,
	&GenInst_Int32_t2950945753_0_0_0_Boolean_t97287965_0_0_0_Boolean_t97287965_0_0_0,
	&GenInst_Int32_t2950945753_0_0_0_Boolean_t97287965_0_0_0_DictionaryEntry_t3123975638_0_0_0,
	&GenInst_Int32_t2950945753_0_0_0_Boolean_t97287965_0_0_0_KeyValuePair_2_t1383673463_0_0_0,
	&GenInst_TMP_MeshInfo_t2771747634_0_0_0,
	&GenInst_TMP_Style_t3479380764_0_0_0,
	&GenInst_Int32_t2950945753_0_0_0_TMP_Style_t3479380764_0_0_0,
	&GenInst_Int32_t2950945753_0_0_0_TMP_Style_t3479380764_0_0_0_DictionaryEntry_t3123975638_0_0_0,
	&GenInst_KeyValuePair_2_t470798966_0_0_0,
	&GenInst_Int32_t2950945753_0_0_0_TMP_Style_t3479380764_0_0_0_KeyValuePair_2_t470798966_0_0_0,
	&GenInst_Int32_t2950945753_0_0_0_TMP_Style_t3479380764_0_0_0_TMP_Style_t3479380764_0_0_0,
	&GenInst_TextAlignmentOptions_t4036791236_0_0_0,
	&GenInst_XML_TagAttribute_t1174424309_0_0_0,
	&GenInst_TMP_LinkInfo_t1092083476_0_0_0,
	&GenInst_TMP_PageInfo_t2608430633_0_0_0,
	&GenInst_TMP_Text_t2599618874_0_0_0,
	&GenInst_Il2CppObject_0_0_0_Compute_DT_EventArgs_t1071353166_0_0_0,
	&GenInst_Action_2_t461255840_0_0_0,
	&GenInst_Action_2_t461255840_0_0_0_LinkedListNode_1_t206430943_0_0_0,
	&GenInst_LinkedListNode_1_t206430943_0_0_0,
	&GenInst_Action_2_t461255840_0_0_0_LinkedListNode_1_t206430943_0_0_0_DictionaryEntry_t3123975638_0_0_0,
	&GenInst_KeyValuePair_2_t2290643574_0_0_0,
	&GenInst_Action_2_t461255840_0_0_0_LinkedListNode_1_t206430943_0_0_0_KeyValuePair_2_t2290643574_0_0_0,
	&GenInst_Action_2_t461255840_0_0_0_LinkedListNode_1_t206430943_0_0_0_LinkedListNode_1_t206430943_0_0_0,
	&GenInst_Boolean_t97287965_0_0_0_Material_t340375123_0_0_0,
	&GenInst_Action_2_t2523487705_0_0_0,
	&GenInst_Action_2_t2523487705_0_0_0_LinkedListNode_1_t2268662808_0_0_0,
	&GenInst_LinkedListNode_1_t2268662808_0_0_0,
	&GenInst_Action_2_t2523487705_0_0_0_LinkedListNode_1_t2268662808_0_0_0_DictionaryEntry_t3123975638_0_0_0,
	&GenInst_KeyValuePair_2_t2019553650_0_0_0,
	&GenInst_Action_2_t2523487705_0_0_0_LinkedListNode_1_t2268662808_0_0_0_KeyValuePair_2_t2019553650_0_0_0,
	&GenInst_Action_2_t2523487705_0_0_0_LinkedListNode_1_t2268662808_0_0_0_LinkedListNode_1_t2268662808_0_0_0,
	&GenInst_Action_2_t4078723960_0_0_0,
	&GenInst_Action_2_t4078723960_0_0_0_LinkedListNode_1_t3823899063_0_0_0,
	&GenInst_LinkedListNode_1_t3823899063_0_0_0,
	&GenInst_Action_2_t4078723960_0_0_0_LinkedListNode_1_t3823899063_0_0_0_DictionaryEntry_t3123975638_0_0_0,
	&GenInst_KeyValuePair_2_t1292723222_0_0_0,
	&GenInst_Action_2_t4078723960_0_0_0_LinkedListNode_1_t3823899063_0_0_0_KeyValuePair_2_t1292723222_0_0_0,
	&GenInst_Action_2_t4078723960_0_0_0_LinkedListNode_1_t3823899063_0_0_0_LinkedListNode_1_t3823899063_0_0_0,
	&GenInst_Boolean_t97287965_0_0_0_TMP_FontAsset_t364381626_0_0_0,
	&GenInst_Action_2_t4102730463_0_0_0,
	&GenInst_Action_2_t4102730463_0_0_0_LinkedListNode_1_t3847905566_0_0_0,
	&GenInst_LinkedListNode_1_t3847905566_0_0_0,
	&GenInst_Action_2_t4102730463_0_0_0_LinkedListNode_1_t3847905566_0_0_0_DictionaryEntry_t3123975638_0_0_0,
	&GenInst_KeyValuePair_2_t615435930_0_0_0,
	&GenInst_Action_2_t4102730463_0_0_0_LinkedListNode_1_t3847905566_0_0_0_KeyValuePair_2_t615435930_0_0_0,
	&GenInst_Action_2_t4102730463_0_0_0_LinkedListNode_1_t3847905566_0_0_0_LinkedListNode_1_t3847905566_0_0_0,
	&GenInst_Boolean_t97287965_0_0_0_Object_t631007953_0_0_0,
	&GenInst_Action_2_t74389494_0_0_0,
	&GenInst_Action_2_t74389494_0_0_0_LinkedListNode_1_t4114531893_0_0_0,
	&GenInst_LinkedListNode_1_t4114531893_0_0_0,
	&GenInst_Action_2_t74389494_0_0_0_LinkedListNode_1_t4114531893_0_0_0_DictionaryEntry_t3123975638_0_0_0,
	&GenInst_KeyValuePair_2_t2439265374_0_0_0,
	&GenInst_Action_2_t74389494_0_0_0_LinkedListNode_1_t4114531893_0_0_0_KeyValuePair_2_t2439265374_0_0_0,
	&GenInst_Action_2_t74389494_0_0_0_LinkedListNode_1_t4114531893_0_0_0_LinkedListNode_1_t4114531893_0_0_0,
	&GenInst_Boolean_t97287965_0_0_0_TextMeshPro_t2393593166_0_0_0,
	&GenInst_Action_2_t1836974707_0_0_0,
	&GenInst_Action_2_t1836974707_0_0_0_LinkedListNode_1_t1582149810_0_0_0,
	&GenInst_LinkedListNode_1_t1582149810_0_0_0,
	&GenInst_Action_2_t1836974707_0_0_0_LinkedListNode_1_t1582149810_0_0_0_DictionaryEntry_t3123975638_0_0_0,
	&GenInst_KeyValuePair_2_t4104837578_0_0_0,
	&GenInst_Action_2_t1836974707_0_0_0_LinkedListNode_1_t1582149810_0_0_0_KeyValuePair_2_t4104837578_0_0_0,
	&GenInst_Action_2_t1836974707_0_0_0_LinkedListNode_1_t1582149810_0_0_0_LinkedListNode_1_t1582149810_0_0_0,
	&GenInst_GameObject_t1113636619_0_0_0_Material_t340375123_0_0_0_Material_t340375123_0_0_0,
	&GenInst_Action_3_t2823050148_0_0_0,
	&GenInst_Action_3_t2823050148_0_0_0_LinkedListNode_1_t2568225251_0_0_0,
	&GenInst_LinkedListNode_1_t2568225251_0_0_0,
	&GenInst_Action_3_t2823050148_0_0_0_LinkedListNode_1_t2568225251_0_0_0_DictionaryEntry_t3123975638_0_0_0,
	&GenInst_KeyValuePair_2_t2622726630_0_0_0,
	&GenInst_Action_3_t2823050148_0_0_0_LinkedListNode_1_t2568225251_0_0_0_KeyValuePair_2_t2622726630_0_0_0,
	&GenInst_Action_3_t2823050148_0_0_0_LinkedListNode_1_t2568225251_0_0_0_LinkedListNode_1_t2568225251_0_0_0,
	&GenInst_Action_1_t269755560_0_0_0,
	&GenInst_Action_1_t269755560_0_0_0_LinkedListNode_1_t14930663_0_0_0,
	&GenInst_LinkedListNode_1_t14930663_0_0_0,
	&GenInst_Action_1_t269755560_0_0_0_LinkedListNode_1_t14930663_0_0_0_DictionaryEntry_t3123975638_0_0_0,
	&GenInst_KeyValuePair_2_t3809986902_0_0_0,
	&GenInst_Action_1_t269755560_0_0_0_LinkedListNode_1_t14930663_0_0_0_KeyValuePair_2_t3809986902_0_0_0,
	&GenInst_Action_1_t269755560_0_0_0_LinkedListNode_1_t14930663_0_0_0_LinkedListNode_1_t14930663_0_0_0,
	&GenInst_TMP_ColorGradient_t3678055768_0_0_0,
	&GenInst_Action_1_t3850523363_0_0_0,
	&GenInst_Action_1_t3850523363_0_0_0_LinkedListNode_1_t3595698466_0_0_0,
	&GenInst_LinkedListNode_1_t3595698466_0_0_0,
	&GenInst_Action_1_t3850523363_0_0_0_LinkedListNode_1_t3595698466_0_0_0_DictionaryEntry_t3123975638_0_0_0,
	&GenInst_KeyValuePair_2_t2695289354_0_0_0,
	&GenInst_Action_1_t3850523363_0_0_0_LinkedListNode_1_t3595698466_0_0_0_KeyValuePair_2_t2695289354_0_0_0,
	&GenInst_Action_1_t3850523363_0_0_0_LinkedListNode_1_t3595698466_0_0_0_LinkedListNode_1_t3595698466_0_0_0,
	&GenInst_Boolean_t97287965_0_0_0_TextMeshProUGUI_t529313277_0_0_0,
	&GenInst_Action_2_t4267662114_0_0_0,
	&GenInst_Action_2_t4267662114_0_0_0_LinkedListNode_1_t4012837217_0_0_0,
	&GenInst_LinkedListNode_1_t4012837217_0_0_0,
	&GenInst_Action_2_t4267662114_0_0_0_LinkedListNode_1_t4012837217_0_0_0_DictionaryEntry_t3123975638_0_0_0,
	&GenInst_KeyValuePair_2_t3478638126_0_0_0,
	&GenInst_Action_2_t4267662114_0_0_0_LinkedListNode_1_t4012837217_0_0_0_KeyValuePair_2_t3478638126_0_0_0,
	&GenInst_Action_2_t4267662114_0_0_0_LinkedListNode_1_t4012837217_0_0_0_LinkedListNode_1_t4012837217_0_0_0,
	&GenInst_KerningPair_t2270855589_0_0_0_Int32_t2950945753_0_0_0,
	&GenInst_TMP_SubMesh_t2613037997_0_0_0,
	&GenInst_TMP_SubMeshUI_t1578871311_0_0_0,
	&GenInst_SpriteData_t3048397587_0_0_0,
	&GenInst_SimulatedTarget_t1564170948_0_0_0,
	&GenInst_ImageTarget_t2322341322_0_0_0,
	&GenInst_String_t_0_0_0_Single_t1397266774_0_0_0,
	&GenInst_KeyValuePair_2_t847377929_0_0_0,
	&GenInst_Il2CppObject_0_0_0_Single_t1397266774_0_0_0_Single_t1397266774_0_0_0,
	&GenInst_Il2CppObject_0_0_0_Single_t1397266774_0_0_0_DictionaryEntry_t3123975638_0_0_0,
	&GenInst_Il2CppObject_0_0_0_Single_t1397266774_0_0_0_KeyValuePair_2_t847377929_0_0_0,
	&GenInst_String_t_0_0_0_Single_t1397266774_0_0_0_DictionaryEntry_t3123975638_0_0_0,
	&GenInst_KeyValuePair_2_t3580195240_0_0_0,
	&GenInst_String_t_0_0_0_Single_t1397266774_0_0_0_KeyValuePair_2_t3580195240_0_0_0,
	&GenInst_String_t_0_0_0_Single_t1397266774_0_0_0_Single_t1397266774_0_0_0,
	&GenInst_Int32_t2950945753_0_0_0_String_t_0_0_0,
	&GenInst_String_t_0_0_0_ExtendedTrackingQuality_t364496425_0_0_0,
	&GenInst_Il2CppObject_0_0_0_ExtendedTrackingQuality_t364496425_0_0_0,
	&GenInst_Link_t1976611498_0_0_0,
	&GenInst_Trackable_t347424808_0_0_0_HashSet_1_t2094861142_0_0_0,
	&GenInst_RecognizedTarget_t3529911668_0_0_0,
	&GenInst_Link_t3659072477_0_0_0,
	&GenInst_Trackable_t347424808_0_0_0,
	&GenInst_HashSet_1_t2094861142_0_0_0,
	&GenInst_Trackable_t347424808_0_0_0_HashSet_1_t2094861142_0_0_0_DictionaryEntry_t3123975638_0_0_0,
	&GenInst_KeyValuePair_2_t4167131205_0_0_0,
	&GenInst_Trackable_t347424808_0_0_0_HashSet_1_t2094861142_0_0_0_KeyValuePair_2_t4167131205_0_0_0,
	&GenInst_Trackable_t347424808_0_0_0_HashSet_1_t2094861142_0_0_0_HashSet_1_t2094861142_0_0_0,
	&GenInst_InstantTarget_t3313867941_0_0_0,
	&GenInst_Boolean_t97287965_0_0_0_Vector2_t2156229523_0_0_0_Vector3_t3722313464_0_0_0,
	&GenInst_Vector3U5BU5D_t1718750761_0_0_0,
	&GenInst_InstantTrackingState_t3973410783_0_0_0,
	&GenInst_ObjectTarget_t688225044_0_0_0,
	&GenInst_Frame_t1062777224_0_0_0,
	&GenInst_RecognizedTarget_t3529911668_0_0_0_GameObject_t1113636619_0_0_0,
	&GenInst_RecognizedTarget_t3529911668_0_0_0_GameObject_t1113636619_0_0_0_DictionaryEntry_t3123975638_0_0_0,
	&GenInst_KeyValuePair_2_t831214590_0_0_0,
	&GenInst_RecognizedTarget_t3529911668_0_0_0_GameObject_t1113636619_0_0_0_KeyValuePair_2_t831214590_0_0_0,
	&GenInst_RecognizedTarget_t3529911668_0_0_0_GameObject_t1113636619_0_0_0_GameObject_t1113636619_0_0_0,
	&GenInst_TrackableBehaviour_t2469814643_0_0_0,
	&GenInst_Link_t2598975452_0_0_0,
	&GenInst_Int64_t3736567304_0_0_0_TargetCollectionResource_t66041399_0_0_0,
	&GenInst_TargetCollectionResource_t66041399_0_0_0,
	&GenInst_TargetSource_t2046190744_0_0_0,
	&GenInst_Int64_t3736567304_0_0_0_TargetCollectionResource_t66041399_0_0_0_DictionaryEntry_t3123975638_0_0_0,
	&GenInst_KeyValuePair_2_t3526353350_0_0_0,
	&GenInst_Int64_t3736567304_0_0_0_TargetCollectionResource_t66041399_0_0_0_KeyValuePair_2_t3526353350_0_0_0,
	&GenInst_Int64_t3736567304_0_0_0_TargetCollectionResource_t66041399_0_0_0_TargetCollectionResource_t66041399_0_0_0,
	&GenInst_Int64_t3736567304_0_0_0_CloudRecognitionService_t3866570042_0_0_0,
	&GenInst_CloudRecognitionService_t3866570042_0_0_0,
	&GenInst_Int64_t3736567304_0_0_0_CloudRecognitionService_t3866570042_0_0_0_DictionaryEntry_t3123975638_0_0_0,
	&GenInst_KeyValuePair_2_t3031914697_0_0_0,
	&GenInst_Int64_t3736567304_0_0_0_CloudRecognitionService_t3866570042_0_0_0_KeyValuePair_2_t3031914697_0_0_0,
	&GenInst_Int64_t3736567304_0_0_0_CloudRecognitionService_t3866570042_0_0_0_CloudRecognitionService_t3866570042_0_0_0,
	&GenInst_TrackerBehaviour_t921360922_0_0_0,
	&GenInst_Link_t1050521731_0_0_0,
	&GenInst_String_t_0_0_0_String_t_0_0_0,
	&GenInst_String_t_0_0_0_String_t_0_0_0_DictionaryEntry_t3123975638_0_0_0,
	&GenInst_KeyValuePair_2_t4030379155_0_0_0,
	&GenInst_String_t_0_0_0_String_t_0_0_0_KeyValuePair_2_t4030379155_0_0_0,
	&GenInst_String_t_0_0_0_String_t_0_0_0_String_t_0_0_0,
	&GenInst_CloudRecognitionServiceResponse_t1770101589_0_0_0,
	&GenInst_CaptureDevicePosition_t3635123692_0_0_0,
	&GenInst_Mode_t2127932823_0_0_0,
	&GenInst_Highlighter_t672210414_0_0_0,
	&GenInst_Link_t801371223_0_0_0,
	&GenInst_HighlighterRenderer_t1776162451_0_0_0,
	&GenInst_Renderer_t2627027031_0_0_0,
	&GenInst_HighlightingBase_t582374880_0_0_0,
	&GenInst_HighlightingPreset_t635619791_0_0_0,
	&GenInst_Data_t434873072_0_0_0,
	&GenInst_Shader_t4151988712_0_0_0,
	&GenInst_Link_t4286314680_0_0_0,
	&GenInst_Hashtable_t1853889766_0_0_0,
	&GenInst_IDictionary_t1363984059_0_0_0,
	&GenInst_Rect_t2360479859_0_0_0,
	&GenInst_iTween_t770867771_0_0_0,
	&GenInst_HighlighterItem_t659879438_0_0_0,
	&GenInst_Link_t789040247_0_0_0,
	&GenInst_IHighlightingTarget_t923341466_0_0_0,
	&GenInst_Mesh_t3648964284_0_0_0,
	&GenInst_KeyCode_t2599294277_0_0_0,
	&GenInst_JSONObject_t1339445639_0_0_0,
	&GenInst_String_t_0_0_0_JSONObject_t1339445639_0_0_0,
	&GenInst_String_t_0_0_0_JSONObject_t1339445639_0_0_0_DictionaryEntry_t3123975638_0_0_0,
	&GenInst_KeyValuePair_2_t3522374105_0_0_0,
	&GenInst_String_t_0_0_0_JSONObject_t1339445639_0_0_0_KeyValuePair_2_t3522374105_0_0_0,
	&GenInst_String_t_0_0_0_JSONObject_t1339445639_0_0_0_JSONObject_t1339445639_0_0_0,
	&GenInst_LeanFinger_t3506292858_0_0_0,
	&GenInst_LeanFingerHeld_t1927732235_0_0_0,
	&GenInst_Link_t1401011957_0_0_0,
	&GenInst_Link_t4191133801_0_0_0,
	&GenInst_LeanSnapshot_t4136513957_0_0_0,
	&GenInst_LeanSelectable_t2178850769_0_0_0,
	&GenInst_List_1_t683400304_0_0_0,
	&GenInst_LeanTouch_t2951860335_0_0_0,
	&GenInst_String_t_0_0_0_iTweenPath_t76106739_0_0_0,
	&GenInst_iTweenPath_t76106739_0_0_0,
	&GenInst_String_t_0_0_0_iTweenPath_t76106739_0_0_0_DictionaryEntry_t3123975638_0_0_0,
	&GenInst_KeyValuePair_2_t2259035205_0_0_0,
	&GenInst_String_t_0_0_0_iTweenPath_t76106739_0_0_0_KeyValuePair_2_t2259035205_0_0_0,
	&GenInst_String_t_0_0_0_iTweenPath_t76106739_0_0_0_iTweenPath_t76106739_0_0_0,
	&GenInst_Branch_t1428165248_0_0_0,
	&GenInst_LightmapParams_t3719846058_0_0_0,
	&GenInst_txtBtnModel_t2868369423_0_0_0,
	&GenInst_SelectMesh_t688911915_0_0_0,
	&GenInst_MenuItem_t3210774549_0_0_0,
	&GenInst_MeshRenderer_t587009260_0_0_0,
	&GenInst_TransparancyManager_t318855516_0_0_0,
	&GenInst_Link_t1242797428_0_0_0,
	&GenInst_Char_t3634460470_0_0_0_Int32_t2950945753_0_0_0,
	&GenInst_String_t_0_0_0_String_t_0_0_0_Int32_t2950945753_0_0_0,
	&GenInst_Il2CppObject_0_0_0_Il2CppObject_0_0_0_Int32_t2950945753_0_0_0,
	&GenInst_VertexAnim_t2231884842_0_0_0,
	&GenInst_IList_1_t1242665951_0_0_0,
	&GenInst_ICollection_1_t2255498402_0_0_0,
	&GenInst_IEnumerable_1_t2702166353_0_0_0,
	&GenInst_InputFrameData_t2573966900_0_0_0,
	&GenInst_Button_t4055032469_0_0_0,
	&GenInst_Dinosaur_t1302057126_0_0_0,
	&GenInst_Link_t1431217935_0_0_0,
	&GenInst_IEnumerable_1_t1615002100_gp_0_0_0_0,
	&GenInst_Array_InternalArray__IEnumerable_GetEnumerator_m2877758246_gp_0_0_0_0,
	&GenInst_Array_Sort_m3544654705_gp_0_0_0_0_Array_Sort_m3544654705_gp_0_0_0_0,
	&GenInst_Array_Sort_m2813331573_gp_0_0_0_0_Array_Sort_m2813331573_gp_1_0_0_0,
	&GenInst_Array_Sort_m541372433_gp_0_0_0_0,
	&GenInst_Array_Sort_m541372433_gp_0_0_0_0_Array_Sort_m541372433_gp_0_0_0_0,
	&GenInst_Array_Sort_m3128382015_gp_0_0_0_0,
	&GenInst_Array_Sort_m3128382015_gp_0_0_0_0_Array_Sort_m3128382015_gp_1_0_0_0,
	&GenInst_Array_Sort_m2971965616_gp_0_0_0_0_Array_Sort_m2971965616_gp_0_0_0_0,
	&GenInst_Array_Sort_m2294228957_gp_0_0_0_0_Array_Sort_m2294228957_gp_1_0_0_0,
	&GenInst_Array_Sort_m3111895514_gp_0_0_0_0,
	&GenInst_Array_Sort_m3111895514_gp_0_0_0_0_Array_Sort_m3111895514_gp_0_0_0_0,
	&GenInst_Array_Sort_m4162554849_gp_0_0_0_0,
	&GenInst_Array_Sort_m4162554849_gp_1_0_0_0,
	&GenInst_Array_Sort_m4162554849_gp_0_0_0_0_Array_Sort_m4162554849_gp_1_0_0_0,
	&GenInst_Array_Sort_m2347552592_gp_0_0_0_0,
	&GenInst_Array_Sort_m1584977544_gp_0_0_0_0,
	&GenInst_Array_qsort_m2730016035_gp_0_0_0_0,
	&GenInst_Array_qsort_m2730016035_gp_0_0_0_0_Array_qsort_m2730016035_gp_1_0_0_0,
	&GenInst_Array_compare_m1571452883_gp_0_0_0_0,
	&GenInst_Array_qsort_m2056624725_gp_0_0_0_0,
	&GenInst_Array_Resize_m148240358_gp_0_0_0_0,
	&GenInst_Array_TrueForAll_m2898582490_gp_0_0_0_0,
	&GenInst_Array_ForEach_m1810936836_gp_0_0_0_0,
	&GenInst_Array_ConvertAll_m3156307665_gp_0_0_0_0_Array_ConvertAll_m3156307665_gp_1_0_0_0,
	&GenInst_Array_FindLastIndex_m314143680_gp_0_0_0_0,
	&GenInst_Array_FindLastIndex_m460178338_gp_0_0_0_0,
	&GenInst_Array_FindLastIndex_m4020594960_gp_0_0_0_0,
	&GenInst_Array_FindIndex_m2045802321_gp_0_0_0_0,
	&GenInst_Array_FindIndex_m193711900_gp_0_0_0_0,
	&GenInst_Array_FindIndex_m3007915586_gp_0_0_0_0,
	&GenInst_Array_BinarySearch_m588894935_gp_0_0_0_0,
	&GenInst_Array_BinarySearch_m1820506567_gp_0_0_0_0,
	&GenInst_Array_BinarySearch_m818786257_gp_0_0_0_0,
	&GenInst_Array_BinarySearch_m1137121163_gp_0_0_0_0,
	&GenInst_Array_IndexOf_m3665873194_gp_0_0_0_0,
	&GenInst_Array_IndexOf_m751114778_gp_0_0_0_0,
	&GenInst_Array_IndexOf_m1878557700_gp_0_0_0_0,
	&GenInst_Array_LastIndexOf_m1660745251_gp_0_0_0_0,
	&GenInst_Array_LastIndexOf_m3843535007_gp_0_0_0_0,
	&GenInst_Array_LastIndexOf_m3595565626_gp_0_0_0_0,
	&GenInst_Array_FindAll_m1182922648_gp_0_0_0_0,
	&GenInst_Array_Exists_m1131952887_gp_0_0_0_0,
	&GenInst_Array_AsReadOnly_m1239764914_gp_0_0_0_0,
	&GenInst_Array_Find_m143990730_gp_0_0_0_0,
	&GenInst_Array_FindLast_m1741869030_gp_0_0_0_0,
	&GenInst_InternalEnumerator_1_t2600413744_gp_0_0_0_0,
	&GenInst_ArrayReadOnlyList_1_t221793636_gp_0_0_0_0,
	&GenInst_U3CGetEnumeratorU3Ec__Iterator0_t1202911786_gp_0_0_0_0,
	&GenInst_IList_1_t523203890_gp_0_0_0_0,
	&GenInst_ICollection_1_t1449021101_gp_0_0_0_0,
	&GenInst_Nullable_1_t3772285925_gp_0_0_0_0,
	&GenInst_Comparer_1_t4245720645_gp_0_0_0_0,
	&GenInst_DefaultComparer_t3277344064_gp_0_0_0_0,
	&GenInst_GenericComparer_1_t3581574675_gp_0_0_0_0,
	&GenInst_Dictionary_2_t3621973219_gp_0_0_0_0,
	&GenInst_Dictionary_2_t3621973219_gp_0_0_0_0_Dictionary_2_t3621973219_gp_1_0_0_0,
	&GenInst_KeyValuePair_2_t1772072192_0_0_0,
	&GenInst_Dictionary_2_t3621973219_gp_0_0_0_0_Dictionary_2_t3621973219_gp_1_0_0_0_Dictionary_2_Do_CopyTo_m966111031_gp_0_0_0_0,
	&GenInst_Dictionary_2_t3621973219_gp_0_0_0_0_Dictionary_2_t3621973219_gp_1_0_0_0_Dictionary_2_Do_ICollectionCopyTo_m842166809_gp_0_0_0_0,
	&GenInst_Dictionary_2_Do_ICollectionCopyTo_m842166809_gp_0_0_0_0_Il2CppObject_0_0_0,
	&GenInst_Dictionary_2_t3621973219_gp_0_0_0_0_Dictionary_2_t3621973219_gp_1_0_0_0_DictionaryEntry_t3123975638_0_0_0,
	&GenInst_ShimEnumerator_t3154898978_gp_0_0_0_0_ShimEnumerator_t3154898978_gp_1_0_0_0,
	&GenInst_Enumerator_t135598976_gp_0_0_0_0_Enumerator_t135598976_gp_1_0_0_0,
	&GenInst_KeyValuePair_2_t1920611820_0_0_0,
	&GenInst_ValueCollection_t2327722797_gp_0_0_0_0_ValueCollection_t2327722797_gp_1_0_0_0,
	&GenInst_ValueCollection_t2327722797_gp_1_0_0_0,
	&GenInst_Enumerator_t1602367158_gp_0_0_0_0_Enumerator_t1602367158_gp_1_0_0_0,
	&GenInst_Enumerator_t1602367158_gp_1_0_0_0,
	&GenInst_ValueCollection_t2327722797_gp_0_0_0_0_ValueCollection_t2327722797_gp_1_0_0_0_ValueCollection_t2327722797_gp_1_0_0_0,
	&GenInst_ValueCollection_t2327722797_gp_1_0_0_0_ValueCollection_t2327722797_gp_1_0_0_0,
	&GenInst_DictionaryEntry_t3123975638_0_0_0_DictionaryEntry_t3123975638_0_0_0,
	&GenInst_Dictionary_2_t3621973219_gp_0_0_0_0_Dictionary_2_t3621973219_gp_1_0_0_0_KeyValuePair_2_t1772072192_0_0_0,
	&GenInst_KeyValuePair_2_t1772072192_0_0_0_KeyValuePair_2_t1772072192_0_0_0,
	&GenInst_Dictionary_2_t3621973219_gp_1_0_0_0,
	&GenInst_EqualityComparer_1_t1549919139_gp_0_0_0_0,
	&GenInst_DefaultComparer_t4042948011_gp_0_0_0_0,
	&GenInst_GenericEqualityComparer_1_t2270490560_gp_0_0_0_0,
	&GenInst_KeyValuePair_2_t1708549516_0_0_0,
	&GenInst_IDictionary_2_t3177279192_gp_0_0_0_0_IDictionary_2_t3177279192_gp_1_0_0_0,
	&GenInst_KeyValuePair_2_t4175610960_gp_0_0_0_0_KeyValuePair_2_t4175610960_gp_1_0_0_0,
	&GenInst_List_1_t284568025_gp_0_0_0_0,
	&GenInst_Enumerator_t271486022_gp_0_0_0_0,
	&GenInst_Collection_1_t968317937_gp_0_0_0_0,
	&GenInst_ReadOnlyCollection_1_t2757184810_gp_0_0_0_0,
	&GenInst_MonoProperty_GetterAdapterFrame_m1909347418_gp_0_0_0_0_MonoProperty_GetterAdapterFrame_m1909347418_gp_1_0_0_0,
	&GenInst_MonoProperty_StaticGetterAdapterFrame_m1636979383_gp_0_0_0_0,
	&GenInst_LinkedList_1_t2189935060_gp_0_0_0_0,
	&GenInst_Enumerator_t3560274443_gp_0_0_0_0,
	&GenInst_LinkedListNode_1_t3023898186_gp_0_0_0_0,
	&GenInst_Stack_1_t1463756442_gp_0_0_0_0,
	&GenInst_Enumerator_t2989469293_gp_0_0_0_0,
	&GenInst_HashSet_1_t743387557_gp_0_0_0_0,
	&GenInst_Enumerator_t3836401716_gp_0_0_0_0,
	&GenInst_PrimeHelper_t2385147435_gp_0_0_0_0,
	&GenInst_Enumerable_Any_m2256625727_gp_0_0_0_0,
	&GenInst_Enumerable_Contains_m4222801258_gp_0_0_0_0,
	&GenInst_Enumerable_Contains_m1710981724_gp_0_0_0_0,
	&GenInst_Enumerable_First_m2847869424_gp_0_0_0_0,
	&GenInst_Enumerable_FirstOrDefault_m1944909936_gp_0_0_0_0,
	&GenInst_Enumerable_Last_m3684240055_gp_0_0_0_0,
	&GenInst_Enumerable_OrderBy_m1070606102_gp_0_0_0_0,
	&GenInst_Enumerable_OrderBy_m1070606102_gp_0_0_0_0_Enumerable_OrderBy_m1070606102_gp_1_0_0_0,
	&GenInst_Enumerable_OrderBy_m2443884947_gp_0_0_0_0,
	&GenInst_Enumerable_OrderBy_m2443884947_gp_0_0_0_0_Enumerable_OrderBy_m2443884947_gp_1_0_0_0,
	&GenInst_Enumerable_OrderBy_m2443884947_gp_1_0_0_0,
	&GenInst_Enumerable_ThenBy_m1007540769_gp_0_0_0_0,
	&GenInst_Enumerable_ThenBy_m1007540769_gp_0_0_0_0_Enumerable_ThenBy_m1007540769_gp_1_0_0_0,
	&GenInst_Enumerable_ThenBy_m4292131248_gp_0_0_0_0,
	&GenInst_Enumerable_ThenBy_m4292131248_gp_0_0_0_0_Enumerable_ThenBy_m4292131248_gp_1_0_0_0,
	&GenInst_Enumerable_ThenBy_m4292131248_gp_1_0_0_0,
	&GenInst_Enumerable_ToArray_m1974389789_gp_0_0_0_0,
	&GenInst_Enumerable_ToList_m2780466421_gp_0_0_0_0,
	&GenInst_Enumerable_Where_m88697338_gp_0_0_0_0,
	&GenInst_Enumerable_Where_m88697338_gp_0_0_0_0_Boolean_t97287965_0_0_0,
	&GenInst_Enumerable_CreateWhereIterator_m3238110963_gp_0_0_0_0,
	&GenInst_Enumerable_CreateWhereIterator_m3238110963_gp_0_0_0_0_Boolean_t97287965_0_0_0,
	&GenInst_U3CCreateWhereIteratorU3Ec__Iterator1D_1_t945640688_gp_0_0_0_0,
	&GenInst_U3CCreateWhereIteratorU3Ec__Iterator1D_1_t945640688_gp_0_0_0_0_Boolean_t97287965_0_0_0,
	&GenInst_IOrderedEnumerable_1_t3318875935_gp_0_0_0_0_IOrderedEnumerable_1_CreateOrderedEnumerable_m3379561760_gp_0_0_0_0,
	&GenInst_IOrderedEnumerable_1_CreateOrderedEnumerable_m3379561760_gp_0_0_0_0,
	&GenInst_IOrderedEnumerable_1_t3318875935_gp_0_0_0_0,
	&GenInst_OrderedEnumerable_1_t1372796327_gp_0_0_0_0,
	&GenInst_OrderedEnumerable_1_t1372796327_gp_0_0_0_0_OrderedEnumerable_1_CreateOrderedEnumerable_m205428121_gp_0_0_0_0,
	&GenInst_OrderedEnumerable_1_CreateOrderedEnumerable_m205428121_gp_0_0_0_0,
	&GenInst_OrderedSequence_2_t4114741689_gp_0_0_0_0,
	&GenInst_OrderedSequence_2_t4114741689_gp_0_0_0_0_OrderedSequence_2_t4114741689_gp_1_0_0_0,
	&GenInst_OrderedSequence_2_t4114741689_gp_1_0_0_0,
	&GenInst_QuickSort_1_t1667011766_gp_0_0_0_0,
	&GenInst_U3CSortU3Ec__Iterator21_t3807379664_gp_0_0_0_0,
	&GenInst_SortContext_1_t2773119229_gp_0_0_0_0,
	&GenInst_SortSequenceContext_2_t4077189013_gp_0_0_0_0,
	&GenInst_SortSequenceContext_2_t4077189013_gp_0_0_0_0_SortSequenceContext_2_t4077189013_gp_1_0_0_0,
	&GenInst_SortSequenceContext_2_t4077189013_gp_1_0_0_0,
	&GenInst_AssetBundle_LoadAllAssets_m4266127462_gp_0_0_0_0,
	&GenInst_Component_GetComponentInChildren_m1404611044_gp_0_0_0_0,
	&GenInst_Component_GetComponentsInChildren_m3615474666_gp_0_0_0_0,
	&GenInst_Component_GetComponentsInChildren_m933673001_gp_0_0_0_0,
	&GenInst_Component_GetComponentsInChildren_m4005089511_gp_0_0_0_0,
	&GenInst_Component_GetComponentsInChildren_m1915630048_gp_0_0_0_0,
	&GenInst_Component_GetComponentsInParent_m555240590_gp_0_0_0_0,
	&GenInst_Component_GetComponentsInParent_m2569671904_gp_0_0_0_0,
	&GenInst_Component_GetComponentsInParent_m149672228_gp_0_0_0_0,
	&GenInst_Component_GetComponents_m3598713100_gp_0_0_0_0,
	&GenInst_Component_GetComponents_m3914262141_gp_0_0_0_0,
	&GenInst_GameObject_GetComponentInChildren_m916317014_gp_0_0_0_0,
	&GenInst_GameObject_GetComponents_m2386781050_gp_0_0_0_0,
	&GenInst_GameObject_GetComponentsInChildren_m906810621_gp_0_0_0_0,
	&GenInst_GameObject_GetComponentsInChildren_m1887210244_gp_0_0_0_0,
	&GenInst_GameObject_GetComponentsInParent_m241541908_gp_0_0_0_0,
	&GenInst_Mesh_GetAllocArrayFromChannel_m2078183201_gp_0_0_0_0,
	&GenInst_Mesh_SafeLength_m1325240367_gp_0_0_0_0,
	&GenInst_Mesh_SetListForChannel_m462664139_gp_0_0_0_0,
	&GenInst_Mesh_SetListForChannel_m631926134_gp_0_0_0_0,
	&GenInst_Mesh_SetUvsImpl_m1664867204_gp_0_0_0_0,
	&GenInst_Object_Instantiate_m1874094322_gp_0_0_0_0,
	&GenInst_Object_FindObjectsOfType_m1824391917_gp_0_0_0_0,
	&GenInst_InvokableCall_1_t3865199217_gp_0_0_0_0,
	&GenInst_UnityAction_1_t802700511_0_0_0,
	&GenInst_InvokableCall_2_t3865133681_gp_0_0_0_0_InvokableCall_2_t3865133681_gp_1_0_0_0,
	&GenInst_UnityAction_2_t300839120_0_0_0,
	&GenInst_InvokableCall_2_t3865133681_gp_0_0_0_0,
	&GenInst_InvokableCall_2_t3865133681_gp_1_0_0_0,
	&GenInst_InvokableCall_3_t3865068145_gp_0_0_0_0_InvokableCall_3_t3865068145_gp_1_0_0_0_InvokableCall_3_t3865068145_gp_2_0_0_0,
	&GenInst_UnityAction_3_t148588884_0_0_0,
	&GenInst_InvokableCall_3_t3865068145_gp_0_0_0_0,
	&GenInst_InvokableCall_3_t3865068145_gp_1_0_0_0,
	&GenInst_InvokableCall_3_t3865068145_gp_2_0_0_0,
	&GenInst_InvokableCall_4_t3865002609_gp_0_0_0_0_InvokableCall_4_t3865002609_gp_1_0_0_0_InvokableCall_4_t3865002609_gp_2_0_0_0_InvokableCall_4_t3865002609_gp_3_0_0_0,
	&GenInst_InvokableCall_4_t3865002609_gp_0_0_0_0,
	&GenInst_InvokableCall_4_t3865002609_gp_1_0_0_0,
	&GenInst_InvokableCall_4_t3865002609_gp_2_0_0_0,
	&GenInst_InvokableCall_4_t3865002609_gp_3_0_0_0,
	&GenInst_CachedInvokableCall_1_t3153979999_gp_0_0_0_0,
	&GenInst_UnityEvent_1_t74220259_gp_0_0_0_0,
	&GenInst_UnityEvent_2_t477504786_gp_0_0_0_0_UnityEvent_2_t477504786_gp_1_0_0_0,
	&GenInst_UnityEvent_3_t3206388141_gp_0_0_0_0_UnityEvent_3_t3206388141_gp_1_0_0_0_UnityEvent_3_t3206388141_gp_2_0_0_0,
	&GenInst_UnityEvent_4_t3609672668_gp_0_0_0_0_UnityEvent_4_t3609672668_gp_1_0_0_0_UnityEvent_4_t3609672668_gp_2_0_0_0_UnityEvent_4_t3609672668_gp_3_0_0_0,
	&GenInst_ExecuteEvents_Execute_m513682320_gp_0_0_0_0,
	&GenInst_ExecuteEvents_ExecuteHierarchy_m2988888012_gp_0_0_0_0,
	&GenInst_ExecuteEvents_GetEventList_m400222725_gp_0_0_0_0,
	&GenInst_ExecuteEvents_CanHandleEvent_m1773513752_gp_0_0_0_0,
	&GenInst_ExecuteEvents_GetEventHandler_m3154756557_gp_0_0_0_0,
	&GenInst_TweenRunner_1_t3844461449_gp_0_0_0_0,
	&GenInst_Dropdown_GetOrAddComponent_m4080506703_gp_0_0_0_0,
	&GenInst_SetPropertyUtility_SetStruct_m2798032208_gp_0_0_0_0,
	&GenInst_IndexedSet_1_t2120020791_gp_0_0_0_0,
	&GenInst_IndexedSet_1_t2120020791_gp_0_0_0_0_Int32_t2950945753_0_0_0,
	&GenInst_ListPool_1_t890186770_gp_0_0_0_0,
	&GenInst_List_1_t3009893961_0_0_0,
	&GenInst_ObjectPool_1_t892185599_gp_0_0_0_0,
	&GenInst_FastAction_1_t2420856216_gp_0_0_0_0,
	&GenInst_Action_1_t3240956260_0_0_0,
	&GenInst_Action_1_t3240956260_0_0_0_LinkedListNode_1_t2986131363_0_0_0,
	&GenInst_FastAction_2_t2421052824_gp_0_0_0_0_FastAction_2_t2421052824_gp_1_0_0_0,
	&GenInst_Action_2_t3881532715_0_0_0,
	&GenInst_Action_2_t3881532715_0_0_0_LinkedListNode_1_t3626707818_0_0_0,
	&GenInst_FastAction_3_t2420987288_gp_0_0_0_0_FastAction_3_t2420987288_gp_1_0_0_0_FastAction_3_t2420987288_gp_2_0_0_0,
	&GenInst_Action_3_t4213579327_0_0_0,
	&GenInst_Action_3_t4213579327_0_0_0_LinkedListNode_1_t3958754430_0_0_0,
	&GenInst_TweenRunner_1_t3899390588_gp_0_0_0_0,
	&GenInst_TMP_Dropdown_GetOrAddComponent_m1213542931_gp_0_0_0_0,
	&GenInst_SetPropertyUtility_SetEquatableStruct_m2580670824_gp_0_0_0_0,
	&GenInst_TMP_ListPool_1_t894436986_gp_0_0_0_0,
	&GenInst_List_1_t3014144177_0_0_0,
	&GenInst_U3CU3Ec_t3046190068_gp_0_0_0_0,
	&GenInst_TMP_ObjectPool_1_t2098678475_gp_0_0_0_0,
	&GenInst_TMP_Text_ResizeInternalArray_m2041343611_gp_0_0_0_0,
	&GenInst_TMP_TextInfo_Resize_m333743359_gp_0_0_0_0,
	&GenInst_TMP_TextInfo_Resize_m2258560236_gp_0_0_0_0,
	&GenInst_TMP_XmlTagStack_1_t3882766083_gp_0_0_0_0,
	&GenInst_TMPro_ExtensionMethods_FindInstanceID_m826387165_gp_0_0_0_0,
	&GenInst_DefaultExecutionOrder_t3059642329_0_0_0,
	&GenInst_PlayerConnection_t3081694049_0_0_0,
	&GenInst_ParticleSystem_t1800779281_0_0_0,
	&GenInst_GUILayer_t2783472903_0_0_0,
	&GenInst_EventSystem_t1003666588_0_0_0,
	&GenInst_AxisEventData_t2331243652_0_0_0,
	&GenInst_SpriteRenderer_t3235626157_0_0_0,
	&GenInst_Image_t2670269651_0_0_0,
	&GenInst_RawImage_t3182918964_0_0_0,
	&GenInst_Slider_t3903728902_0_0_0,
	&GenInst_Scrollbar_t1494447233_0_0_0,
	&GenInst_InputField_t3762917431_0_0_0,
	&GenInst_ScrollRect_t4137855814_0_0_0,
	&GenInst_Dropdown_t2274391225_0_0_0,
	&GenInst_GraphicRaycaster_t2999697109_0_0_0,
	&GenInst_CanvasRenderer_t2598313366_0_0_0,
	&GenInst_Corner_t1493259673_0_0_0,
	&GenInst_Axis_t3613393006_0_0_0,
	&GenInst_Constraint_t814224393_0_0_0,
	&GenInst_SubmitEvent_t648412432_0_0_0,
	&GenInst_OnChangeEvent_t467195904_0_0_0,
	&GenInst_OnValidateInput_t2355412304_0_0_0,
	&GenInst_LayoutElement_t1785403678_0_0_0,
	&GenInst_RectOffset_t1369453676_0_0_0,
	&GenInst_TextAnchor_t2035777396_0_0_0,
	&GenInst_AnimationTriggers_t2532145056_0_0_0,
	&GenInst_Animator_t434523843_0_0_0,
	&GenInst_InlineGraphicManager_t2871008645_0_0_0,
	&GenInst_InlineGraphic_t2901727699_0_0_0,
	&GenInst_TextMeshPro_t2393593166_0_0_0,
	&GenInst_MeshFilter_t3523625662_0_0_0,
	&GenInst_TMP_InputField_t1099764886_0_0_0,
	&GenInst_TextMeshProUGUI_t529313277_0_0_0,
	&GenInst_TMP_Dropdown_t3024694699_0_0_0,
	&GenInst_SubmitEvent_t1343580625_0_0_0,
	&GenInst_SelectionEvent_t4268942288_0_0_0,
	&GenInst_TextSelectionEvent_t1023421581_0_0_0,
	&GenInst_OnChangeEvent_t4257774360_0_0_0,
	&GenInst_OnValidateInput_t373909109_0_0_0,
	&GenInst_LineType_t2817627283_0_0_0,
	&GenInst_InputType_t2510134850_0_0_0,
	&GenInst_CharacterValidation_t3801396403_0_0_0,
	&GenInst_TMP_InputValidator_t1385053824_0_0_0,
	&GenInst_TMP_SelectionCaret_t407257285_0_0_0,
	&GenInst_BoxCollider_t1640800422_0_0_0,
	&GenInst_Rigidbody_t3916780224_0_0_0,
	&GenInst_TMP_SpriteAnimator_t2836635477_0_0_0,
	&GenInst_TrackerManager_t2102804711_0_0_0,
	&GenInst_WikitudeCamera_t2188881370_0_0_0,
	&GenInst_PluginManager_t1017991214_0_0_0,
	&GenInst_BackgroundCamera_t2368011250_0_0_0,
	&GenInst_HighlighterBlocker_t3431127703_0_0_0,
	&GenInst_GUITexture_t951903601_0_0_0,
	&GenInst_GUIText_t402233326_0_0_0,
	&GenInst_Light_t3756812086_0_0_0,
	&GenInst_AudioSource_t3935305588_0_0_0,
	&GenInst_Launch_Animation_t763963988_0_0_0,
	&GenInst_ImageTracker_t271395264_0_0_0,
	&GenInst_MeshCollider_t903564387_0_0_0,
	&GenInst_SmoothOrbitCam_t3313789169_0_0_0,
	&GenInst_VideoClip_t1281919028_0_0_0,
	&GenInst_Texture2D_t3840446185_0_0_0,
	&GenInst_MoveController_t1157877467_0_0_0,
	&GenInst_GridRenderer_t1964421403_0_0_0,
	&GenInst_TextMesh_t1536577757_0_0_0,
	&GenInst_LineRenderer_t3154350270_0_0_0,
	&GenInst_LabelController_t2075514664_0_0_0,
	&GenInst_StreamVideo_t2698492640_0_0_0,
	&GenInst_Image_t1825575977_0_0_0,
	&GenInst_InstantTrackingController_t1374048463_0_0_0,
	&GenInst_HighlightingRenderer_t1923179915_0_0_0,
	&GenInst_arAnimationController_t1029363926_0_0_0,
	&GenInst_InstantTrackable_t3187174337_0_0_0,
	&GenInst_VideoPlayer_t1683042537_0_0_0,
	&GenInst_TextMeshProFloatingText_t845872552_0_0_0,
	&GenInst_TextContainer_t97923372_0_0_0,
	&GenInst_InputFrameData_t2573966900_0_0_0_InputFrameData_t2573966900_0_0_0,
	&GenInst_Data_t434873072_0_0_0_Data_t434873072_0_0_0,
	&GenInst_HighlightingPreset_t635619791_0_0_0_HighlightingPreset_t635619791_0_0_0,
	&GenInst_Char_t3634460470_0_0_0_Char_t3634460470_0_0_0,
	&GenInst_CustomAttributeNamedArgument_t287865710_0_0_0_CustomAttributeNamedArgument_t287865710_0_0_0,
	&GenInst_CustomAttributeTypedArgument_t2723150157_0_0_0_CustomAttributeTypedArgument_t2723150157_0_0_0,
	&GenInst_Single_t1397266774_0_0_0_Single_t1397266774_0_0_0,
	&GenInst_SpriteData_t3048397587_0_0_0_SpriteData_t3048397587_0_0_0,
	&GenInst_AnimatorClipInfo_t3156717155_0_0_0_AnimatorClipInfo_t3156717155_0_0_0,
	&GenInst_Color32_t2600501292_0_0_0_Color32_t2600501292_0_0_0,
	&GenInst_RaycastResult_t3360306849_0_0_0_RaycastResult_t3360306849_0_0_0,
	&GenInst_UICharInfo_t75501106_0_0_0_UICharInfo_t75501106_0_0_0,
	&GenInst_UILineInfo_t4195266810_0_0_0_UILineInfo_t4195266810_0_0_0,
	&GenInst_UIVertex_t4057497605_0_0_0_UIVertex_t4057497605_0_0_0,
	&GenInst_Vector2_t2156229523_0_0_0_Vector2_t2156229523_0_0_0,
	&GenInst_Vector3_t3722313464_0_0_0_Vector3_t3722313464_0_0_0,
	&GenInst_Vector4_t3319028937_0_0_0_Vector4_t3319028937_0_0_0,
	&GenInst_SimulatedTarget_t1564170948_0_0_0_SimulatedTarget_t1564170948_0_0_0,
	&GenInst_Boolean_t97287965_0_0_0_Boolean_t97287965_0_0_0,
	&GenInst_KeyValuePair_2_t1383673463_0_0_0_KeyValuePair_2_t1383673463_0_0_0,
	&GenInst_KeyValuePair_2_t1383673463_0_0_0_Il2CppObject_0_0_0,
	&GenInst_Char_t3634460470_0_0_0_Il2CppObject_0_0_0,
	&GenInst_KeyValuePair_2_t625878672_0_0_0_KeyValuePair_2_t625878672_0_0_0,
	&GenInst_KeyValuePair_2_t625878672_0_0_0_Il2CppObject_0_0_0,
	&GenInst_KeyValuePair_2_t4237331251_0_0_0_KeyValuePair_2_t4237331251_0_0_0,
	&GenInst_KeyValuePair_2_t4237331251_0_0_0_Il2CppObject_0_0_0,
	&GenInst_KeyValuePair_2_t727985506_0_0_0_KeyValuePair_2_t727985506_0_0_0,
	&GenInst_KeyValuePair_2_t727985506_0_0_0_Il2CppObject_0_0_0,
	&GenInst_Int64_t3736567304_0_0_0_Int64_t3736567304_0_0_0,
	&GenInst_KeyValuePair_2_t71524366_0_0_0_KeyValuePair_2_t71524366_0_0_0,
	&GenInst_KeyValuePair_2_t71524366_0_0_0_Il2CppObject_0_0_0,
	&GenInst_KeyValuePair_2_t2245450819_0_0_0_KeyValuePair_2_t2245450819_0_0_0,
	&GenInst_KeyValuePair_2_t2245450819_0_0_0_Il2CppObject_0_0_0,
	&GenInst_KeyValuePair_2_t3842366416_0_0_0_KeyValuePair_2_t3842366416_0_0_0,
	&GenInst_KeyValuePair_2_t3842366416_0_0_0_Il2CppObject_0_0_0,
	&GenInst_KeyValuePair_2_t2401056908_0_0_0_KeyValuePair_2_t2401056908_0_0_0,
	&GenInst_KeyValuePair_2_t2401056908_0_0_0_Il2CppObject_0_0_0,
	&GenInst_KeyValuePair_2_t2530217319_0_0_0_KeyValuePair_2_t2530217319_0_0_0,
	&GenInst_KeyValuePair_2_t2530217319_0_0_0_Il2CppObject_0_0_0,
	&GenInst_KeyValuePair_2_t847377929_0_0_0_KeyValuePair_2_t847377929_0_0_0,
	&GenInst_KeyValuePair_2_t847377929_0_0_0_Il2CppObject_0_0_0,
	&GenInst_Single_t1397266774_0_0_0_Il2CppObject_0_0_0,
	&GenInst_ExtendedTrackingQuality_t364496425_0_0_0,
};
