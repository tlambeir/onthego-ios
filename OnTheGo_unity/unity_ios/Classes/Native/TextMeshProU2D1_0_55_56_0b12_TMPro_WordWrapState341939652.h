﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_ValueType3640485471.h"
#include "TextMeshProU2D1_0_55_56_0b12_TMPro_FontStyles3828945032.h"
#include "TextMeshProU2D1_0_55_56_0b12_TMPro_TMP_LineInfo1079631636.h"
#include "UnityEngine_UnityEngine_Color322600501292.h"
#include "TextMeshProU2D1_0_55_56_0b12_TMPro_TMP_BasicXmlTag2962628096.h"
#include "TextMeshProU2D1_0_55_56_0b12_TMPro_TMP_XmlTagStack2164155836.h"
#include "TextMeshProU2D1_0_55_56_0b12_TMPro_TMP_XmlTagStack_960921318.h"
#include "TextMeshProU2D1_0_55_56_0b12_TMPro_TMP_XmlTagStack2514600297.h"
#include "TextMeshProU2D1_0_55_56_0b12_TMPro_TMP_XmlTagStack1515999176.h"
#include "TextMeshProU2D1_0_55_56_0b12_TMPro_TMP_XmlTagStack3600445780.h"
#include "TextMeshProU2D1_0_55_56_0b12_TMPro_Extents3837212874.h"

// TMPro.TMP_TextInfo
struct TMP_TextInfo_t3598145122;
// TMPro.TMP_FontAsset
struct TMP_FontAsset_t364381626;
// TMPro.TMP_SpriteAsset
struct TMP_SpriteAsset_t484820633;
// UnityEngine.Material
struct Material_t340375123;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.WordWrapState
struct  WordWrapState_t341939652 
{
public:
	// System.Int32 TMPro.WordWrapState::previous_WordBreak
	int32_t ___previous_WordBreak_0;
	// System.Int32 TMPro.WordWrapState::total_CharacterCount
	int32_t ___total_CharacterCount_1;
	// System.Int32 TMPro.WordWrapState::visible_CharacterCount
	int32_t ___visible_CharacterCount_2;
	// System.Int32 TMPro.WordWrapState::visible_SpriteCount
	int32_t ___visible_SpriteCount_3;
	// System.Int32 TMPro.WordWrapState::visible_LinkCount
	int32_t ___visible_LinkCount_4;
	// System.Int32 TMPro.WordWrapState::firstCharacterIndex
	int32_t ___firstCharacterIndex_5;
	// System.Int32 TMPro.WordWrapState::firstVisibleCharacterIndex
	int32_t ___firstVisibleCharacterIndex_6;
	// System.Int32 TMPro.WordWrapState::lastCharacterIndex
	int32_t ___lastCharacterIndex_7;
	// System.Int32 TMPro.WordWrapState::lastVisibleCharIndex
	int32_t ___lastVisibleCharIndex_8;
	// System.Int32 TMPro.WordWrapState::lineNumber
	int32_t ___lineNumber_9;
	// System.Single TMPro.WordWrapState::maxCapHeight
	float ___maxCapHeight_10;
	// System.Single TMPro.WordWrapState::maxAscender
	float ___maxAscender_11;
	// System.Single TMPro.WordWrapState::maxDescender
	float ___maxDescender_12;
	// System.Single TMPro.WordWrapState::maxLineAscender
	float ___maxLineAscender_13;
	// System.Single TMPro.WordWrapState::maxLineDescender
	float ___maxLineDescender_14;
	// System.Single TMPro.WordWrapState::previousLineAscender
	float ___previousLineAscender_15;
	// System.Single TMPro.WordWrapState::xAdvance
	float ___xAdvance_16;
	// System.Single TMPro.WordWrapState::preferredWidth
	float ___preferredWidth_17;
	// System.Single TMPro.WordWrapState::preferredHeight
	float ___preferredHeight_18;
	// System.Single TMPro.WordWrapState::previousLineScale
	float ___previousLineScale_19;
	// System.Int32 TMPro.WordWrapState::wordCount
	int32_t ___wordCount_20;
	// TMPro.FontStyles TMPro.WordWrapState::fontStyle
	int32_t ___fontStyle_21;
	// System.Single TMPro.WordWrapState::fontScale
	float ___fontScale_22;
	// System.Single TMPro.WordWrapState::fontScaleMultiplier
	float ___fontScaleMultiplier_23;
	// System.Single TMPro.WordWrapState::currentFontSize
	float ___currentFontSize_24;
	// System.Single TMPro.WordWrapState::baselineOffset
	float ___baselineOffset_25;
	// System.Single TMPro.WordWrapState::lineOffset
	float ___lineOffset_26;
	// TMPro.TMP_TextInfo TMPro.WordWrapState::textInfo
	TMP_TextInfo_t3598145122 * ___textInfo_27;
	// TMPro.TMP_LineInfo TMPro.WordWrapState::lineInfo
	TMP_LineInfo_t1079631636  ___lineInfo_28;
	// UnityEngine.Color32 TMPro.WordWrapState::vertexColor
	Color32_t2600501292  ___vertexColor_29;
	// UnityEngine.Color32 TMPro.WordWrapState::underlineColor
	Color32_t2600501292  ___underlineColor_30;
	// UnityEngine.Color32 TMPro.WordWrapState::strikethroughColor
	Color32_t2600501292  ___strikethroughColor_31;
	// UnityEngine.Color32 TMPro.WordWrapState::highlightColor
	Color32_t2600501292  ___highlightColor_32;
	// TMPro.TMP_BasicXmlTagStack TMPro.WordWrapState::basicStyleStack
	TMP_BasicXmlTagStack_t2962628096  ___basicStyleStack_33;
	// TMPro.TMP_XmlTagStack`1<UnityEngine.Color32> TMPro.WordWrapState::colorStack
	TMP_XmlTagStack_1_t2164155836  ___colorStack_34;
	// TMPro.TMP_XmlTagStack`1<UnityEngine.Color32> TMPro.WordWrapState::underlineColorStack
	TMP_XmlTagStack_1_t2164155836  ___underlineColorStack_35;
	// TMPro.TMP_XmlTagStack`1<UnityEngine.Color32> TMPro.WordWrapState::strikethroughColorStack
	TMP_XmlTagStack_1_t2164155836  ___strikethroughColorStack_36;
	// TMPro.TMP_XmlTagStack`1<UnityEngine.Color32> TMPro.WordWrapState::highlightColorStack
	TMP_XmlTagStack_1_t2164155836  ___highlightColorStack_37;
	// TMPro.TMP_XmlTagStack`1<System.Single> TMPro.WordWrapState::sizeStack
	TMP_XmlTagStack_1_t960921318  ___sizeStack_38;
	// TMPro.TMP_XmlTagStack`1<System.Single> TMPro.WordWrapState::indentStack
	TMP_XmlTagStack_1_t960921318  ___indentStack_39;
	// TMPro.TMP_XmlTagStack`1<System.Int32> TMPro.WordWrapState::fontWeightStack
	TMP_XmlTagStack_1_t2514600297  ___fontWeightStack_40;
	// TMPro.TMP_XmlTagStack`1<System.Int32> TMPro.WordWrapState::styleStack
	TMP_XmlTagStack_1_t2514600297  ___styleStack_41;
	// TMPro.TMP_XmlTagStack`1<System.Single> TMPro.WordWrapState::baselineStack
	TMP_XmlTagStack_1_t960921318  ___baselineStack_42;
	// TMPro.TMP_XmlTagStack`1<System.Int32> TMPro.WordWrapState::actionStack
	TMP_XmlTagStack_1_t2514600297  ___actionStack_43;
	// TMPro.TMP_XmlTagStack`1<TMPro.MaterialReference> TMPro.WordWrapState::materialReferenceStack
	TMP_XmlTagStack_1_t1515999176  ___materialReferenceStack_44;
	// TMPro.TMP_XmlTagStack`1<TMPro.TextAlignmentOptions> TMPro.WordWrapState::lineJustificationStack
	TMP_XmlTagStack_1_t3600445780  ___lineJustificationStack_45;
	// System.Int32 TMPro.WordWrapState::spriteAnimationID
	int32_t ___spriteAnimationID_46;
	// TMPro.TMP_FontAsset TMPro.WordWrapState::currentFontAsset
	TMP_FontAsset_t364381626 * ___currentFontAsset_47;
	// TMPro.TMP_SpriteAsset TMPro.WordWrapState::currentSpriteAsset
	TMP_SpriteAsset_t484820633 * ___currentSpriteAsset_48;
	// UnityEngine.Material TMPro.WordWrapState::currentMaterial
	Material_t340375123 * ___currentMaterial_49;
	// System.Int32 TMPro.WordWrapState::currentMaterialIndex
	int32_t ___currentMaterialIndex_50;
	// TMPro.Extents TMPro.WordWrapState::meshExtents
	Extents_t3837212874  ___meshExtents_51;
	// System.Boolean TMPro.WordWrapState::tagNoParsing
	bool ___tagNoParsing_52;

public:
	inline static int32_t get_offset_of_previous_WordBreak_0() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___previous_WordBreak_0)); }
	inline int32_t get_previous_WordBreak_0() const { return ___previous_WordBreak_0; }
	inline int32_t* get_address_of_previous_WordBreak_0() { return &___previous_WordBreak_0; }
	inline void set_previous_WordBreak_0(int32_t value)
	{
		___previous_WordBreak_0 = value;
	}

	inline static int32_t get_offset_of_total_CharacterCount_1() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___total_CharacterCount_1)); }
	inline int32_t get_total_CharacterCount_1() const { return ___total_CharacterCount_1; }
	inline int32_t* get_address_of_total_CharacterCount_1() { return &___total_CharacterCount_1; }
	inline void set_total_CharacterCount_1(int32_t value)
	{
		___total_CharacterCount_1 = value;
	}

	inline static int32_t get_offset_of_visible_CharacterCount_2() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___visible_CharacterCount_2)); }
	inline int32_t get_visible_CharacterCount_2() const { return ___visible_CharacterCount_2; }
	inline int32_t* get_address_of_visible_CharacterCount_2() { return &___visible_CharacterCount_2; }
	inline void set_visible_CharacterCount_2(int32_t value)
	{
		___visible_CharacterCount_2 = value;
	}

	inline static int32_t get_offset_of_visible_SpriteCount_3() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___visible_SpriteCount_3)); }
	inline int32_t get_visible_SpriteCount_3() const { return ___visible_SpriteCount_3; }
	inline int32_t* get_address_of_visible_SpriteCount_3() { return &___visible_SpriteCount_3; }
	inline void set_visible_SpriteCount_3(int32_t value)
	{
		___visible_SpriteCount_3 = value;
	}

	inline static int32_t get_offset_of_visible_LinkCount_4() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___visible_LinkCount_4)); }
	inline int32_t get_visible_LinkCount_4() const { return ___visible_LinkCount_4; }
	inline int32_t* get_address_of_visible_LinkCount_4() { return &___visible_LinkCount_4; }
	inline void set_visible_LinkCount_4(int32_t value)
	{
		___visible_LinkCount_4 = value;
	}

	inline static int32_t get_offset_of_firstCharacterIndex_5() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___firstCharacterIndex_5)); }
	inline int32_t get_firstCharacterIndex_5() const { return ___firstCharacterIndex_5; }
	inline int32_t* get_address_of_firstCharacterIndex_5() { return &___firstCharacterIndex_5; }
	inline void set_firstCharacterIndex_5(int32_t value)
	{
		___firstCharacterIndex_5 = value;
	}

	inline static int32_t get_offset_of_firstVisibleCharacterIndex_6() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___firstVisibleCharacterIndex_6)); }
	inline int32_t get_firstVisibleCharacterIndex_6() const { return ___firstVisibleCharacterIndex_6; }
	inline int32_t* get_address_of_firstVisibleCharacterIndex_6() { return &___firstVisibleCharacterIndex_6; }
	inline void set_firstVisibleCharacterIndex_6(int32_t value)
	{
		___firstVisibleCharacterIndex_6 = value;
	}

	inline static int32_t get_offset_of_lastCharacterIndex_7() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___lastCharacterIndex_7)); }
	inline int32_t get_lastCharacterIndex_7() const { return ___lastCharacterIndex_7; }
	inline int32_t* get_address_of_lastCharacterIndex_7() { return &___lastCharacterIndex_7; }
	inline void set_lastCharacterIndex_7(int32_t value)
	{
		___lastCharacterIndex_7 = value;
	}

	inline static int32_t get_offset_of_lastVisibleCharIndex_8() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___lastVisibleCharIndex_8)); }
	inline int32_t get_lastVisibleCharIndex_8() const { return ___lastVisibleCharIndex_8; }
	inline int32_t* get_address_of_lastVisibleCharIndex_8() { return &___lastVisibleCharIndex_8; }
	inline void set_lastVisibleCharIndex_8(int32_t value)
	{
		___lastVisibleCharIndex_8 = value;
	}

	inline static int32_t get_offset_of_lineNumber_9() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___lineNumber_9)); }
	inline int32_t get_lineNumber_9() const { return ___lineNumber_9; }
	inline int32_t* get_address_of_lineNumber_9() { return &___lineNumber_9; }
	inline void set_lineNumber_9(int32_t value)
	{
		___lineNumber_9 = value;
	}

	inline static int32_t get_offset_of_maxCapHeight_10() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___maxCapHeight_10)); }
	inline float get_maxCapHeight_10() const { return ___maxCapHeight_10; }
	inline float* get_address_of_maxCapHeight_10() { return &___maxCapHeight_10; }
	inline void set_maxCapHeight_10(float value)
	{
		___maxCapHeight_10 = value;
	}

	inline static int32_t get_offset_of_maxAscender_11() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___maxAscender_11)); }
	inline float get_maxAscender_11() const { return ___maxAscender_11; }
	inline float* get_address_of_maxAscender_11() { return &___maxAscender_11; }
	inline void set_maxAscender_11(float value)
	{
		___maxAscender_11 = value;
	}

	inline static int32_t get_offset_of_maxDescender_12() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___maxDescender_12)); }
	inline float get_maxDescender_12() const { return ___maxDescender_12; }
	inline float* get_address_of_maxDescender_12() { return &___maxDescender_12; }
	inline void set_maxDescender_12(float value)
	{
		___maxDescender_12 = value;
	}

	inline static int32_t get_offset_of_maxLineAscender_13() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___maxLineAscender_13)); }
	inline float get_maxLineAscender_13() const { return ___maxLineAscender_13; }
	inline float* get_address_of_maxLineAscender_13() { return &___maxLineAscender_13; }
	inline void set_maxLineAscender_13(float value)
	{
		___maxLineAscender_13 = value;
	}

	inline static int32_t get_offset_of_maxLineDescender_14() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___maxLineDescender_14)); }
	inline float get_maxLineDescender_14() const { return ___maxLineDescender_14; }
	inline float* get_address_of_maxLineDescender_14() { return &___maxLineDescender_14; }
	inline void set_maxLineDescender_14(float value)
	{
		___maxLineDescender_14 = value;
	}

	inline static int32_t get_offset_of_previousLineAscender_15() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___previousLineAscender_15)); }
	inline float get_previousLineAscender_15() const { return ___previousLineAscender_15; }
	inline float* get_address_of_previousLineAscender_15() { return &___previousLineAscender_15; }
	inline void set_previousLineAscender_15(float value)
	{
		___previousLineAscender_15 = value;
	}

	inline static int32_t get_offset_of_xAdvance_16() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___xAdvance_16)); }
	inline float get_xAdvance_16() const { return ___xAdvance_16; }
	inline float* get_address_of_xAdvance_16() { return &___xAdvance_16; }
	inline void set_xAdvance_16(float value)
	{
		___xAdvance_16 = value;
	}

	inline static int32_t get_offset_of_preferredWidth_17() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___preferredWidth_17)); }
	inline float get_preferredWidth_17() const { return ___preferredWidth_17; }
	inline float* get_address_of_preferredWidth_17() { return &___preferredWidth_17; }
	inline void set_preferredWidth_17(float value)
	{
		___preferredWidth_17 = value;
	}

	inline static int32_t get_offset_of_preferredHeight_18() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___preferredHeight_18)); }
	inline float get_preferredHeight_18() const { return ___preferredHeight_18; }
	inline float* get_address_of_preferredHeight_18() { return &___preferredHeight_18; }
	inline void set_preferredHeight_18(float value)
	{
		___preferredHeight_18 = value;
	}

	inline static int32_t get_offset_of_previousLineScale_19() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___previousLineScale_19)); }
	inline float get_previousLineScale_19() const { return ___previousLineScale_19; }
	inline float* get_address_of_previousLineScale_19() { return &___previousLineScale_19; }
	inline void set_previousLineScale_19(float value)
	{
		___previousLineScale_19 = value;
	}

	inline static int32_t get_offset_of_wordCount_20() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___wordCount_20)); }
	inline int32_t get_wordCount_20() const { return ___wordCount_20; }
	inline int32_t* get_address_of_wordCount_20() { return &___wordCount_20; }
	inline void set_wordCount_20(int32_t value)
	{
		___wordCount_20 = value;
	}

	inline static int32_t get_offset_of_fontStyle_21() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___fontStyle_21)); }
	inline int32_t get_fontStyle_21() const { return ___fontStyle_21; }
	inline int32_t* get_address_of_fontStyle_21() { return &___fontStyle_21; }
	inline void set_fontStyle_21(int32_t value)
	{
		___fontStyle_21 = value;
	}

	inline static int32_t get_offset_of_fontScale_22() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___fontScale_22)); }
	inline float get_fontScale_22() const { return ___fontScale_22; }
	inline float* get_address_of_fontScale_22() { return &___fontScale_22; }
	inline void set_fontScale_22(float value)
	{
		___fontScale_22 = value;
	}

	inline static int32_t get_offset_of_fontScaleMultiplier_23() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___fontScaleMultiplier_23)); }
	inline float get_fontScaleMultiplier_23() const { return ___fontScaleMultiplier_23; }
	inline float* get_address_of_fontScaleMultiplier_23() { return &___fontScaleMultiplier_23; }
	inline void set_fontScaleMultiplier_23(float value)
	{
		___fontScaleMultiplier_23 = value;
	}

	inline static int32_t get_offset_of_currentFontSize_24() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___currentFontSize_24)); }
	inline float get_currentFontSize_24() const { return ___currentFontSize_24; }
	inline float* get_address_of_currentFontSize_24() { return &___currentFontSize_24; }
	inline void set_currentFontSize_24(float value)
	{
		___currentFontSize_24 = value;
	}

	inline static int32_t get_offset_of_baselineOffset_25() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___baselineOffset_25)); }
	inline float get_baselineOffset_25() const { return ___baselineOffset_25; }
	inline float* get_address_of_baselineOffset_25() { return &___baselineOffset_25; }
	inline void set_baselineOffset_25(float value)
	{
		___baselineOffset_25 = value;
	}

	inline static int32_t get_offset_of_lineOffset_26() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___lineOffset_26)); }
	inline float get_lineOffset_26() const { return ___lineOffset_26; }
	inline float* get_address_of_lineOffset_26() { return &___lineOffset_26; }
	inline void set_lineOffset_26(float value)
	{
		___lineOffset_26 = value;
	}

	inline static int32_t get_offset_of_textInfo_27() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___textInfo_27)); }
	inline TMP_TextInfo_t3598145122 * get_textInfo_27() const { return ___textInfo_27; }
	inline TMP_TextInfo_t3598145122 ** get_address_of_textInfo_27() { return &___textInfo_27; }
	inline void set_textInfo_27(TMP_TextInfo_t3598145122 * value)
	{
		___textInfo_27 = value;
		Il2CppCodeGenWriteBarrier(&___textInfo_27, value);
	}

	inline static int32_t get_offset_of_lineInfo_28() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___lineInfo_28)); }
	inline TMP_LineInfo_t1079631636  get_lineInfo_28() const { return ___lineInfo_28; }
	inline TMP_LineInfo_t1079631636 * get_address_of_lineInfo_28() { return &___lineInfo_28; }
	inline void set_lineInfo_28(TMP_LineInfo_t1079631636  value)
	{
		___lineInfo_28 = value;
	}

	inline static int32_t get_offset_of_vertexColor_29() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___vertexColor_29)); }
	inline Color32_t2600501292  get_vertexColor_29() const { return ___vertexColor_29; }
	inline Color32_t2600501292 * get_address_of_vertexColor_29() { return &___vertexColor_29; }
	inline void set_vertexColor_29(Color32_t2600501292  value)
	{
		___vertexColor_29 = value;
	}

	inline static int32_t get_offset_of_underlineColor_30() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___underlineColor_30)); }
	inline Color32_t2600501292  get_underlineColor_30() const { return ___underlineColor_30; }
	inline Color32_t2600501292 * get_address_of_underlineColor_30() { return &___underlineColor_30; }
	inline void set_underlineColor_30(Color32_t2600501292  value)
	{
		___underlineColor_30 = value;
	}

	inline static int32_t get_offset_of_strikethroughColor_31() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___strikethroughColor_31)); }
	inline Color32_t2600501292  get_strikethroughColor_31() const { return ___strikethroughColor_31; }
	inline Color32_t2600501292 * get_address_of_strikethroughColor_31() { return &___strikethroughColor_31; }
	inline void set_strikethroughColor_31(Color32_t2600501292  value)
	{
		___strikethroughColor_31 = value;
	}

	inline static int32_t get_offset_of_highlightColor_32() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___highlightColor_32)); }
	inline Color32_t2600501292  get_highlightColor_32() const { return ___highlightColor_32; }
	inline Color32_t2600501292 * get_address_of_highlightColor_32() { return &___highlightColor_32; }
	inline void set_highlightColor_32(Color32_t2600501292  value)
	{
		___highlightColor_32 = value;
	}

	inline static int32_t get_offset_of_basicStyleStack_33() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___basicStyleStack_33)); }
	inline TMP_BasicXmlTagStack_t2962628096  get_basicStyleStack_33() const { return ___basicStyleStack_33; }
	inline TMP_BasicXmlTagStack_t2962628096 * get_address_of_basicStyleStack_33() { return &___basicStyleStack_33; }
	inline void set_basicStyleStack_33(TMP_BasicXmlTagStack_t2962628096  value)
	{
		___basicStyleStack_33 = value;
	}

	inline static int32_t get_offset_of_colorStack_34() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___colorStack_34)); }
	inline TMP_XmlTagStack_1_t2164155836  get_colorStack_34() const { return ___colorStack_34; }
	inline TMP_XmlTagStack_1_t2164155836 * get_address_of_colorStack_34() { return &___colorStack_34; }
	inline void set_colorStack_34(TMP_XmlTagStack_1_t2164155836  value)
	{
		___colorStack_34 = value;
	}

	inline static int32_t get_offset_of_underlineColorStack_35() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___underlineColorStack_35)); }
	inline TMP_XmlTagStack_1_t2164155836  get_underlineColorStack_35() const { return ___underlineColorStack_35; }
	inline TMP_XmlTagStack_1_t2164155836 * get_address_of_underlineColorStack_35() { return &___underlineColorStack_35; }
	inline void set_underlineColorStack_35(TMP_XmlTagStack_1_t2164155836  value)
	{
		___underlineColorStack_35 = value;
	}

	inline static int32_t get_offset_of_strikethroughColorStack_36() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___strikethroughColorStack_36)); }
	inline TMP_XmlTagStack_1_t2164155836  get_strikethroughColorStack_36() const { return ___strikethroughColorStack_36; }
	inline TMP_XmlTagStack_1_t2164155836 * get_address_of_strikethroughColorStack_36() { return &___strikethroughColorStack_36; }
	inline void set_strikethroughColorStack_36(TMP_XmlTagStack_1_t2164155836  value)
	{
		___strikethroughColorStack_36 = value;
	}

	inline static int32_t get_offset_of_highlightColorStack_37() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___highlightColorStack_37)); }
	inline TMP_XmlTagStack_1_t2164155836  get_highlightColorStack_37() const { return ___highlightColorStack_37; }
	inline TMP_XmlTagStack_1_t2164155836 * get_address_of_highlightColorStack_37() { return &___highlightColorStack_37; }
	inline void set_highlightColorStack_37(TMP_XmlTagStack_1_t2164155836  value)
	{
		___highlightColorStack_37 = value;
	}

	inline static int32_t get_offset_of_sizeStack_38() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___sizeStack_38)); }
	inline TMP_XmlTagStack_1_t960921318  get_sizeStack_38() const { return ___sizeStack_38; }
	inline TMP_XmlTagStack_1_t960921318 * get_address_of_sizeStack_38() { return &___sizeStack_38; }
	inline void set_sizeStack_38(TMP_XmlTagStack_1_t960921318  value)
	{
		___sizeStack_38 = value;
	}

	inline static int32_t get_offset_of_indentStack_39() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___indentStack_39)); }
	inline TMP_XmlTagStack_1_t960921318  get_indentStack_39() const { return ___indentStack_39; }
	inline TMP_XmlTagStack_1_t960921318 * get_address_of_indentStack_39() { return &___indentStack_39; }
	inline void set_indentStack_39(TMP_XmlTagStack_1_t960921318  value)
	{
		___indentStack_39 = value;
	}

	inline static int32_t get_offset_of_fontWeightStack_40() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___fontWeightStack_40)); }
	inline TMP_XmlTagStack_1_t2514600297  get_fontWeightStack_40() const { return ___fontWeightStack_40; }
	inline TMP_XmlTagStack_1_t2514600297 * get_address_of_fontWeightStack_40() { return &___fontWeightStack_40; }
	inline void set_fontWeightStack_40(TMP_XmlTagStack_1_t2514600297  value)
	{
		___fontWeightStack_40 = value;
	}

	inline static int32_t get_offset_of_styleStack_41() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___styleStack_41)); }
	inline TMP_XmlTagStack_1_t2514600297  get_styleStack_41() const { return ___styleStack_41; }
	inline TMP_XmlTagStack_1_t2514600297 * get_address_of_styleStack_41() { return &___styleStack_41; }
	inline void set_styleStack_41(TMP_XmlTagStack_1_t2514600297  value)
	{
		___styleStack_41 = value;
	}

	inline static int32_t get_offset_of_baselineStack_42() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___baselineStack_42)); }
	inline TMP_XmlTagStack_1_t960921318  get_baselineStack_42() const { return ___baselineStack_42; }
	inline TMP_XmlTagStack_1_t960921318 * get_address_of_baselineStack_42() { return &___baselineStack_42; }
	inline void set_baselineStack_42(TMP_XmlTagStack_1_t960921318  value)
	{
		___baselineStack_42 = value;
	}

	inline static int32_t get_offset_of_actionStack_43() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___actionStack_43)); }
	inline TMP_XmlTagStack_1_t2514600297  get_actionStack_43() const { return ___actionStack_43; }
	inline TMP_XmlTagStack_1_t2514600297 * get_address_of_actionStack_43() { return &___actionStack_43; }
	inline void set_actionStack_43(TMP_XmlTagStack_1_t2514600297  value)
	{
		___actionStack_43 = value;
	}

	inline static int32_t get_offset_of_materialReferenceStack_44() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___materialReferenceStack_44)); }
	inline TMP_XmlTagStack_1_t1515999176  get_materialReferenceStack_44() const { return ___materialReferenceStack_44; }
	inline TMP_XmlTagStack_1_t1515999176 * get_address_of_materialReferenceStack_44() { return &___materialReferenceStack_44; }
	inline void set_materialReferenceStack_44(TMP_XmlTagStack_1_t1515999176  value)
	{
		___materialReferenceStack_44 = value;
	}

	inline static int32_t get_offset_of_lineJustificationStack_45() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___lineJustificationStack_45)); }
	inline TMP_XmlTagStack_1_t3600445780  get_lineJustificationStack_45() const { return ___lineJustificationStack_45; }
	inline TMP_XmlTagStack_1_t3600445780 * get_address_of_lineJustificationStack_45() { return &___lineJustificationStack_45; }
	inline void set_lineJustificationStack_45(TMP_XmlTagStack_1_t3600445780  value)
	{
		___lineJustificationStack_45 = value;
	}

	inline static int32_t get_offset_of_spriteAnimationID_46() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___spriteAnimationID_46)); }
	inline int32_t get_spriteAnimationID_46() const { return ___spriteAnimationID_46; }
	inline int32_t* get_address_of_spriteAnimationID_46() { return &___spriteAnimationID_46; }
	inline void set_spriteAnimationID_46(int32_t value)
	{
		___spriteAnimationID_46 = value;
	}

	inline static int32_t get_offset_of_currentFontAsset_47() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___currentFontAsset_47)); }
	inline TMP_FontAsset_t364381626 * get_currentFontAsset_47() const { return ___currentFontAsset_47; }
	inline TMP_FontAsset_t364381626 ** get_address_of_currentFontAsset_47() { return &___currentFontAsset_47; }
	inline void set_currentFontAsset_47(TMP_FontAsset_t364381626 * value)
	{
		___currentFontAsset_47 = value;
		Il2CppCodeGenWriteBarrier(&___currentFontAsset_47, value);
	}

	inline static int32_t get_offset_of_currentSpriteAsset_48() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___currentSpriteAsset_48)); }
	inline TMP_SpriteAsset_t484820633 * get_currentSpriteAsset_48() const { return ___currentSpriteAsset_48; }
	inline TMP_SpriteAsset_t484820633 ** get_address_of_currentSpriteAsset_48() { return &___currentSpriteAsset_48; }
	inline void set_currentSpriteAsset_48(TMP_SpriteAsset_t484820633 * value)
	{
		___currentSpriteAsset_48 = value;
		Il2CppCodeGenWriteBarrier(&___currentSpriteAsset_48, value);
	}

	inline static int32_t get_offset_of_currentMaterial_49() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___currentMaterial_49)); }
	inline Material_t340375123 * get_currentMaterial_49() const { return ___currentMaterial_49; }
	inline Material_t340375123 ** get_address_of_currentMaterial_49() { return &___currentMaterial_49; }
	inline void set_currentMaterial_49(Material_t340375123 * value)
	{
		___currentMaterial_49 = value;
		Il2CppCodeGenWriteBarrier(&___currentMaterial_49, value);
	}

	inline static int32_t get_offset_of_currentMaterialIndex_50() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___currentMaterialIndex_50)); }
	inline int32_t get_currentMaterialIndex_50() const { return ___currentMaterialIndex_50; }
	inline int32_t* get_address_of_currentMaterialIndex_50() { return &___currentMaterialIndex_50; }
	inline void set_currentMaterialIndex_50(int32_t value)
	{
		___currentMaterialIndex_50 = value;
	}

	inline static int32_t get_offset_of_meshExtents_51() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___meshExtents_51)); }
	inline Extents_t3837212874  get_meshExtents_51() const { return ___meshExtents_51; }
	inline Extents_t3837212874 * get_address_of_meshExtents_51() { return &___meshExtents_51; }
	inline void set_meshExtents_51(Extents_t3837212874  value)
	{
		___meshExtents_51 = value;
	}

	inline static int32_t get_offset_of_tagNoParsing_52() { return static_cast<int32_t>(offsetof(WordWrapState_t341939652, ___tagNoParsing_52)); }
	inline bool get_tagNoParsing_52() const { return ___tagNoParsing_52; }
	inline bool* get_address_of_tagNoParsing_52() { return &___tagNoParsing_52; }
	inline void set_tagNoParsing_52(bool value)
	{
		___tagNoParsing_52 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of TMPro.WordWrapState
struct WordWrapState_t341939652_marshaled_pinvoke
{
	int32_t ___previous_WordBreak_0;
	int32_t ___total_CharacterCount_1;
	int32_t ___visible_CharacterCount_2;
	int32_t ___visible_SpriteCount_3;
	int32_t ___visible_LinkCount_4;
	int32_t ___firstCharacterIndex_5;
	int32_t ___firstVisibleCharacterIndex_6;
	int32_t ___lastCharacterIndex_7;
	int32_t ___lastVisibleCharIndex_8;
	int32_t ___lineNumber_9;
	float ___maxCapHeight_10;
	float ___maxAscender_11;
	float ___maxDescender_12;
	float ___maxLineAscender_13;
	float ___maxLineDescender_14;
	float ___previousLineAscender_15;
	float ___xAdvance_16;
	float ___preferredWidth_17;
	float ___preferredHeight_18;
	float ___previousLineScale_19;
	int32_t ___wordCount_20;
	int32_t ___fontStyle_21;
	float ___fontScale_22;
	float ___fontScaleMultiplier_23;
	float ___currentFontSize_24;
	float ___baselineOffset_25;
	float ___lineOffset_26;
	TMP_TextInfo_t3598145122 * ___textInfo_27;
	TMP_LineInfo_t1079631636  ___lineInfo_28;
	Color32_t2600501292  ___vertexColor_29;
	Color32_t2600501292  ___underlineColor_30;
	Color32_t2600501292  ___strikethroughColor_31;
	Color32_t2600501292  ___highlightColor_32;
	TMP_BasicXmlTagStack_t2962628096  ___basicStyleStack_33;
	TMP_XmlTagStack_1_t2164155836  ___colorStack_34;
	TMP_XmlTagStack_1_t2164155836  ___underlineColorStack_35;
	TMP_XmlTagStack_1_t2164155836  ___strikethroughColorStack_36;
	TMP_XmlTagStack_1_t2164155836  ___highlightColorStack_37;
	TMP_XmlTagStack_1_t960921318  ___sizeStack_38;
	TMP_XmlTagStack_1_t960921318  ___indentStack_39;
	TMP_XmlTagStack_1_t2514600297  ___fontWeightStack_40;
	TMP_XmlTagStack_1_t2514600297  ___styleStack_41;
	TMP_XmlTagStack_1_t960921318  ___baselineStack_42;
	TMP_XmlTagStack_1_t2514600297  ___actionStack_43;
	TMP_XmlTagStack_1_t1515999176  ___materialReferenceStack_44;
	TMP_XmlTagStack_1_t3600445780  ___lineJustificationStack_45;
	int32_t ___spriteAnimationID_46;
	TMP_FontAsset_t364381626 * ___currentFontAsset_47;
	TMP_SpriteAsset_t484820633 * ___currentSpriteAsset_48;
	Material_t340375123 * ___currentMaterial_49;
	int32_t ___currentMaterialIndex_50;
	Extents_t3837212874  ___meshExtents_51;
	int32_t ___tagNoParsing_52;
};
// Native definition for COM marshalling of TMPro.WordWrapState
struct WordWrapState_t341939652_marshaled_com
{
	int32_t ___previous_WordBreak_0;
	int32_t ___total_CharacterCount_1;
	int32_t ___visible_CharacterCount_2;
	int32_t ___visible_SpriteCount_3;
	int32_t ___visible_LinkCount_4;
	int32_t ___firstCharacterIndex_5;
	int32_t ___firstVisibleCharacterIndex_6;
	int32_t ___lastCharacterIndex_7;
	int32_t ___lastVisibleCharIndex_8;
	int32_t ___lineNumber_9;
	float ___maxCapHeight_10;
	float ___maxAscender_11;
	float ___maxDescender_12;
	float ___maxLineAscender_13;
	float ___maxLineDescender_14;
	float ___previousLineAscender_15;
	float ___xAdvance_16;
	float ___preferredWidth_17;
	float ___preferredHeight_18;
	float ___previousLineScale_19;
	int32_t ___wordCount_20;
	int32_t ___fontStyle_21;
	float ___fontScale_22;
	float ___fontScaleMultiplier_23;
	float ___currentFontSize_24;
	float ___baselineOffset_25;
	float ___lineOffset_26;
	TMP_TextInfo_t3598145122 * ___textInfo_27;
	TMP_LineInfo_t1079631636  ___lineInfo_28;
	Color32_t2600501292  ___vertexColor_29;
	Color32_t2600501292  ___underlineColor_30;
	Color32_t2600501292  ___strikethroughColor_31;
	Color32_t2600501292  ___highlightColor_32;
	TMP_BasicXmlTagStack_t2962628096  ___basicStyleStack_33;
	TMP_XmlTagStack_1_t2164155836  ___colorStack_34;
	TMP_XmlTagStack_1_t2164155836  ___underlineColorStack_35;
	TMP_XmlTagStack_1_t2164155836  ___strikethroughColorStack_36;
	TMP_XmlTagStack_1_t2164155836  ___highlightColorStack_37;
	TMP_XmlTagStack_1_t960921318  ___sizeStack_38;
	TMP_XmlTagStack_1_t960921318  ___indentStack_39;
	TMP_XmlTagStack_1_t2514600297  ___fontWeightStack_40;
	TMP_XmlTagStack_1_t2514600297  ___styleStack_41;
	TMP_XmlTagStack_1_t960921318  ___baselineStack_42;
	TMP_XmlTagStack_1_t2514600297  ___actionStack_43;
	TMP_XmlTagStack_1_t1515999176  ___materialReferenceStack_44;
	TMP_XmlTagStack_1_t3600445780  ___lineJustificationStack_45;
	int32_t ___spriteAnimationID_46;
	TMP_FontAsset_t364381626 * ___currentFontAsset_47;
	TMP_SpriteAsset_t484820633 * ___currentSpriteAsset_48;
	Material_t340375123 * ___currentMaterial_49;
	int32_t ___currentMaterialIndex_50;
	Extents_t3837212874  ___meshExtents_51;
	int32_t ___tagNoParsing_52;
};
