﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.Collections.Generic.List`1<Branch>
struct List_1_t2900239990;
// Branch
struct Branch_t1428165248;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TreeBranches
struct  TreeBranches_t1493482931  : public Il2CppObject
{
public:
	// System.Collections.Generic.List`1<Branch> TreeBranches::branches
	List_1_t2900239990 * ___branches_0;
	// Branch TreeBranches::owner
	Branch_t1428165248 * ___owner_1;

public:
	inline static int32_t get_offset_of_branches_0() { return static_cast<int32_t>(offsetof(TreeBranches_t1493482931, ___branches_0)); }
	inline List_1_t2900239990 * get_branches_0() const { return ___branches_0; }
	inline List_1_t2900239990 ** get_address_of_branches_0() { return &___branches_0; }
	inline void set_branches_0(List_1_t2900239990 * value)
	{
		___branches_0 = value;
		Il2CppCodeGenWriteBarrier(&___branches_0, value);
	}

	inline static int32_t get_offset_of_owner_1() { return static_cast<int32_t>(offsetof(TreeBranches_t1493482931, ___owner_1)); }
	inline Branch_t1428165248 * get_owner_1() const { return ___owner_1; }
	inline Branch_t1428165248 ** get_address_of_owner_1() { return &___owner_1; }
	inline void set_owner_1(Branch_t1428165248 * value)
	{
		___owner_1 = value;
		Il2CppCodeGenWriteBarrier(&___owner_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
