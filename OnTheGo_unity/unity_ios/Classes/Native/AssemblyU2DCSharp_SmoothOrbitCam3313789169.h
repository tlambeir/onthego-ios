﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour3962482529.h"
#include "AssemblyU2DCSharp_SmoothOrbitCam_OrbitKeyCode603913960.h"
#include "UnityEngine_UnityEngine_Quaternion2301928331.h"
#include "AssemblyU2DCSharp_SmoothOrbitCam_PanKeycode3383310823.h"
#include "UnityEngine_UnityEngine_Vector22156229523.h"
#include "UnityEngine_UnityEngine_Vector33722313464.h"

// UnityEngine.Transform
struct Transform_t3600365921;
// UnityEngine.GameObject
struct GameObject_t1113636619;
// UnityEngine.EventSystems.EventSystem
struct EventSystem_t1003666588;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SmoothOrbitCam
struct  SmoothOrbitCam_t3313789169  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Transform SmoothOrbitCam::target
	Transform_t3600365921 * ___target_2;
	// System.Boolean SmoothOrbitCam::useable
	bool ___useable_3;
	// System.Boolean SmoothOrbitCam::EnableOrbiting
	bool ___EnableOrbiting_4;
	// SmoothOrbitCam/OrbitKeyCode SmoothOrbitCam::orbitKey
	int32_t ___orbitKey_5;
	// System.Single SmoothOrbitCam::distance
	float ___distance_6;
	// System.Single SmoothOrbitCam::lerpDistance
	float ___lerpDistance_7;
	// System.Single SmoothOrbitCam::xSpeed
	float ___xSpeed_8;
	// System.Single SmoothOrbitCam::ySpeed
	float ___ySpeed_9;
	// System.Boolean SmoothOrbitCam::limitToXAxis
	bool ___limitToXAxis_10;
	// System.Boolean SmoothOrbitCam::limitToYAxis
	bool ___limitToYAxis_11;
	// System.Single SmoothOrbitCam::yMinLimit
	float ___yMinLimit_12;
	// System.Single SmoothOrbitCam::yMaxLimit
	float ___yMaxLimit_13;
	// System.Single SmoothOrbitCam::xMinLimit
	float ___xMinLimit_14;
	// System.Single SmoothOrbitCam::xMaxLimit
	float ___xMaxLimit_15;
	// System.Single SmoothOrbitCam::storedLimit
	float ___storedLimit_16;
	// System.Single SmoothOrbitCam::distanceMin
	float ___distanceMin_17;
	// System.Single SmoothOrbitCam::distanceMax
	float ___distanceMax_18;
	// System.Single SmoothOrbitCam::smoothTime
	float ___smoothTime_19;
	// System.Single SmoothOrbitCam::rotationYAxis
	float ___rotationYAxis_20;
	// System.Single SmoothOrbitCam::rotationXAxis
	float ___rotationXAxis_21;
	// UnityEngine.Quaternion SmoothOrbitCam::rotation
	Quaternion_t2301928331  ___rotation_22;
	// System.Single SmoothOrbitCam::velocityX
	float ___velocityX_23;
	// System.Single SmoothOrbitCam::velocityY
	float ___velocityY_24;
	// System.Boolean SmoothOrbitCam::enableZooming
	bool ___enableZooming_25;
	// System.Single SmoothOrbitCam::zoomSpeed
	float ___zoomSpeed_26;
	// System.Boolean SmoothOrbitCam::enablePanning
	bool ___enablePanning_27;
	// UnityEngine.GameObject SmoothOrbitCam::targetPanCam
	GameObject_t1113636619 * ___targetPanCam_28;
	// SmoothOrbitCam/PanKeycode SmoothOrbitCam::panKey
	int32_t ___panKey_29;
	// System.Single SmoothOrbitCam::panSpeed
	float ___panSpeed_30;
	// System.Boolean SmoothOrbitCam::LimitPan
	bool ___LimitPan_31;
	// UnityEngine.Vector2 SmoothOrbitCam::PanLimitsLeftRight
	Vector2_t2156229523  ___PanLimitsLeftRight_32;
	// UnityEngine.Vector2 SmoothOrbitCam::PanLimitsUpDown
	Vector2_t2156229523  ___PanLimitsUpDown_33;
	// System.Single SmoothOrbitCam::xOffset
	float ___xOffset_34;
	// System.Single SmoothOrbitCam::yOffset
	float ___yOffset_35;
	// System.Boolean SmoothOrbitCam::enableAutomaticOrbiting
	bool ___enableAutomaticOrbiting_36;
	// System.Single SmoothOrbitCam::orbitingSpeed
	float ___orbitingSpeed_37;
	// System.Boolean SmoothOrbitCam::NoObjectsBetween
	bool ___NoObjectsBetween_38;
	// System.Boolean SmoothOrbitCam::EnableGroundHovering
	bool ___EnableGroundHovering_39;
	// System.Single SmoothOrbitCam::GroundHoverDistance
	float ___GroundHoverDistance_40;
	// System.Boolean SmoothOrbitCam::UiBlocksInteraction
	bool ___UiBlocksInteraction_41;
	// System.Boolean SmoothOrbitCam::uiBlocking
	bool ___uiBlocking_42;
	// UnityEngine.Vector3 SmoothOrbitCam::tempPanPosition
	Vector3_t3722313464  ___tempPanPosition_43;
	// System.Single SmoothOrbitCam::velocityPanX
	float ___velocityPanX_44;
	// System.Single SmoothOrbitCam::velocityPanY
	float ___velocityPanY_45;
	// System.Boolean SmoothOrbitCam::doOrbit
	bool ___doOrbit_46;
	// UnityEngine.EventSystems.EventSystem SmoothOrbitCam::eventSystem
	EventSystem_t1003666588 * ___eventSystem_47;

public:
	inline static int32_t get_offset_of_target_2() { return static_cast<int32_t>(offsetof(SmoothOrbitCam_t3313789169, ___target_2)); }
	inline Transform_t3600365921 * get_target_2() const { return ___target_2; }
	inline Transform_t3600365921 ** get_address_of_target_2() { return &___target_2; }
	inline void set_target_2(Transform_t3600365921 * value)
	{
		___target_2 = value;
		Il2CppCodeGenWriteBarrier(&___target_2, value);
	}

	inline static int32_t get_offset_of_useable_3() { return static_cast<int32_t>(offsetof(SmoothOrbitCam_t3313789169, ___useable_3)); }
	inline bool get_useable_3() const { return ___useable_3; }
	inline bool* get_address_of_useable_3() { return &___useable_3; }
	inline void set_useable_3(bool value)
	{
		___useable_3 = value;
	}

	inline static int32_t get_offset_of_EnableOrbiting_4() { return static_cast<int32_t>(offsetof(SmoothOrbitCam_t3313789169, ___EnableOrbiting_4)); }
	inline bool get_EnableOrbiting_4() const { return ___EnableOrbiting_4; }
	inline bool* get_address_of_EnableOrbiting_4() { return &___EnableOrbiting_4; }
	inline void set_EnableOrbiting_4(bool value)
	{
		___EnableOrbiting_4 = value;
	}

	inline static int32_t get_offset_of_orbitKey_5() { return static_cast<int32_t>(offsetof(SmoothOrbitCam_t3313789169, ___orbitKey_5)); }
	inline int32_t get_orbitKey_5() const { return ___orbitKey_5; }
	inline int32_t* get_address_of_orbitKey_5() { return &___orbitKey_5; }
	inline void set_orbitKey_5(int32_t value)
	{
		___orbitKey_5 = value;
	}

	inline static int32_t get_offset_of_distance_6() { return static_cast<int32_t>(offsetof(SmoothOrbitCam_t3313789169, ___distance_6)); }
	inline float get_distance_6() const { return ___distance_6; }
	inline float* get_address_of_distance_6() { return &___distance_6; }
	inline void set_distance_6(float value)
	{
		___distance_6 = value;
	}

	inline static int32_t get_offset_of_lerpDistance_7() { return static_cast<int32_t>(offsetof(SmoothOrbitCam_t3313789169, ___lerpDistance_7)); }
	inline float get_lerpDistance_7() const { return ___lerpDistance_7; }
	inline float* get_address_of_lerpDistance_7() { return &___lerpDistance_7; }
	inline void set_lerpDistance_7(float value)
	{
		___lerpDistance_7 = value;
	}

	inline static int32_t get_offset_of_xSpeed_8() { return static_cast<int32_t>(offsetof(SmoothOrbitCam_t3313789169, ___xSpeed_8)); }
	inline float get_xSpeed_8() const { return ___xSpeed_8; }
	inline float* get_address_of_xSpeed_8() { return &___xSpeed_8; }
	inline void set_xSpeed_8(float value)
	{
		___xSpeed_8 = value;
	}

	inline static int32_t get_offset_of_ySpeed_9() { return static_cast<int32_t>(offsetof(SmoothOrbitCam_t3313789169, ___ySpeed_9)); }
	inline float get_ySpeed_9() const { return ___ySpeed_9; }
	inline float* get_address_of_ySpeed_9() { return &___ySpeed_9; }
	inline void set_ySpeed_9(float value)
	{
		___ySpeed_9 = value;
	}

	inline static int32_t get_offset_of_limitToXAxis_10() { return static_cast<int32_t>(offsetof(SmoothOrbitCam_t3313789169, ___limitToXAxis_10)); }
	inline bool get_limitToXAxis_10() const { return ___limitToXAxis_10; }
	inline bool* get_address_of_limitToXAxis_10() { return &___limitToXAxis_10; }
	inline void set_limitToXAxis_10(bool value)
	{
		___limitToXAxis_10 = value;
	}

	inline static int32_t get_offset_of_limitToYAxis_11() { return static_cast<int32_t>(offsetof(SmoothOrbitCam_t3313789169, ___limitToYAxis_11)); }
	inline bool get_limitToYAxis_11() const { return ___limitToYAxis_11; }
	inline bool* get_address_of_limitToYAxis_11() { return &___limitToYAxis_11; }
	inline void set_limitToYAxis_11(bool value)
	{
		___limitToYAxis_11 = value;
	}

	inline static int32_t get_offset_of_yMinLimit_12() { return static_cast<int32_t>(offsetof(SmoothOrbitCam_t3313789169, ___yMinLimit_12)); }
	inline float get_yMinLimit_12() const { return ___yMinLimit_12; }
	inline float* get_address_of_yMinLimit_12() { return &___yMinLimit_12; }
	inline void set_yMinLimit_12(float value)
	{
		___yMinLimit_12 = value;
	}

	inline static int32_t get_offset_of_yMaxLimit_13() { return static_cast<int32_t>(offsetof(SmoothOrbitCam_t3313789169, ___yMaxLimit_13)); }
	inline float get_yMaxLimit_13() const { return ___yMaxLimit_13; }
	inline float* get_address_of_yMaxLimit_13() { return &___yMaxLimit_13; }
	inline void set_yMaxLimit_13(float value)
	{
		___yMaxLimit_13 = value;
	}

	inline static int32_t get_offset_of_xMinLimit_14() { return static_cast<int32_t>(offsetof(SmoothOrbitCam_t3313789169, ___xMinLimit_14)); }
	inline float get_xMinLimit_14() const { return ___xMinLimit_14; }
	inline float* get_address_of_xMinLimit_14() { return &___xMinLimit_14; }
	inline void set_xMinLimit_14(float value)
	{
		___xMinLimit_14 = value;
	}

	inline static int32_t get_offset_of_xMaxLimit_15() { return static_cast<int32_t>(offsetof(SmoothOrbitCam_t3313789169, ___xMaxLimit_15)); }
	inline float get_xMaxLimit_15() const { return ___xMaxLimit_15; }
	inline float* get_address_of_xMaxLimit_15() { return &___xMaxLimit_15; }
	inline void set_xMaxLimit_15(float value)
	{
		___xMaxLimit_15 = value;
	}

	inline static int32_t get_offset_of_storedLimit_16() { return static_cast<int32_t>(offsetof(SmoothOrbitCam_t3313789169, ___storedLimit_16)); }
	inline float get_storedLimit_16() const { return ___storedLimit_16; }
	inline float* get_address_of_storedLimit_16() { return &___storedLimit_16; }
	inline void set_storedLimit_16(float value)
	{
		___storedLimit_16 = value;
	}

	inline static int32_t get_offset_of_distanceMin_17() { return static_cast<int32_t>(offsetof(SmoothOrbitCam_t3313789169, ___distanceMin_17)); }
	inline float get_distanceMin_17() const { return ___distanceMin_17; }
	inline float* get_address_of_distanceMin_17() { return &___distanceMin_17; }
	inline void set_distanceMin_17(float value)
	{
		___distanceMin_17 = value;
	}

	inline static int32_t get_offset_of_distanceMax_18() { return static_cast<int32_t>(offsetof(SmoothOrbitCam_t3313789169, ___distanceMax_18)); }
	inline float get_distanceMax_18() const { return ___distanceMax_18; }
	inline float* get_address_of_distanceMax_18() { return &___distanceMax_18; }
	inline void set_distanceMax_18(float value)
	{
		___distanceMax_18 = value;
	}

	inline static int32_t get_offset_of_smoothTime_19() { return static_cast<int32_t>(offsetof(SmoothOrbitCam_t3313789169, ___smoothTime_19)); }
	inline float get_smoothTime_19() const { return ___smoothTime_19; }
	inline float* get_address_of_smoothTime_19() { return &___smoothTime_19; }
	inline void set_smoothTime_19(float value)
	{
		___smoothTime_19 = value;
	}

	inline static int32_t get_offset_of_rotationYAxis_20() { return static_cast<int32_t>(offsetof(SmoothOrbitCam_t3313789169, ___rotationYAxis_20)); }
	inline float get_rotationYAxis_20() const { return ___rotationYAxis_20; }
	inline float* get_address_of_rotationYAxis_20() { return &___rotationYAxis_20; }
	inline void set_rotationYAxis_20(float value)
	{
		___rotationYAxis_20 = value;
	}

	inline static int32_t get_offset_of_rotationXAxis_21() { return static_cast<int32_t>(offsetof(SmoothOrbitCam_t3313789169, ___rotationXAxis_21)); }
	inline float get_rotationXAxis_21() const { return ___rotationXAxis_21; }
	inline float* get_address_of_rotationXAxis_21() { return &___rotationXAxis_21; }
	inline void set_rotationXAxis_21(float value)
	{
		___rotationXAxis_21 = value;
	}

	inline static int32_t get_offset_of_rotation_22() { return static_cast<int32_t>(offsetof(SmoothOrbitCam_t3313789169, ___rotation_22)); }
	inline Quaternion_t2301928331  get_rotation_22() const { return ___rotation_22; }
	inline Quaternion_t2301928331 * get_address_of_rotation_22() { return &___rotation_22; }
	inline void set_rotation_22(Quaternion_t2301928331  value)
	{
		___rotation_22 = value;
	}

	inline static int32_t get_offset_of_velocityX_23() { return static_cast<int32_t>(offsetof(SmoothOrbitCam_t3313789169, ___velocityX_23)); }
	inline float get_velocityX_23() const { return ___velocityX_23; }
	inline float* get_address_of_velocityX_23() { return &___velocityX_23; }
	inline void set_velocityX_23(float value)
	{
		___velocityX_23 = value;
	}

	inline static int32_t get_offset_of_velocityY_24() { return static_cast<int32_t>(offsetof(SmoothOrbitCam_t3313789169, ___velocityY_24)); }
	inline float get_velocityY_24() const { return ___velocityY_24; }
	inline float* get_address_of_velocityY_24() { return &___velocityY_24; }
	inline void set_velocityY_24(float value)
	{
		___velocityY_24 = value;
	}

	inline static int32_t get_offset_of_enableZooming_25() { return static_cast<int32_t>(offsetof(SmoothOrbitCam_t3313789169, ___enableZooming_25)); }
	inline bool get_enableZooming_25() const { return ___enableZooming_25; }
	inline bool* get_address_of_enableZooming_25() { return &___enableZooming_25; }
	inline void set_enableZooming_25(bool value)
	{
		___enableZooming_25 = value;
	}

	inline static int32_t get_offset_of_zoomSpeed_26() { return static_cast<int32_t>(offsetof(SmoothOrbitCam_t3313789169, ___zoomSpeed_26)); }
	inline float get_zoomSpeed_26() const { return ___zoomSpeed_26; }
	inline float* get_address_of_zoomSpeed_26() { return &___zoomSpeed_26; }
	inline void set_zoomSpeed_26(float value)
	{
		___zoomSpeed_26 = value;
	}

	inline static int32_t get_offset_of_enablePanning_27() { return static_cast<int32_t>(offsetof(SmoothOrbitCam_t3313789169, ___enablePanning_27)); }
	inline bool get_enablePanning_27() const { return ___enablePanning_27; }
	inline bool* get_address_of_enablePanning_27() { return &___enablePanning_27; }
	inline void set_enablePanning_27(bool value)
	{
		___enablePanning_27 = value;
	}

	inline static int32_t get_offset_of_targetPanCam_28() { return static_cast<int32_t>(offsetof(SmoothOrbitCam_t3313789169, ___targetPanCam_28)); }
	inline GameObject_t1113636619 * get_targetPanCam_28() const { return ___targetPanCam_28; }
	inline GameObject_t1113636619 ** get_address_of_targetPanCam_28() { return &___targetPanCam_28; }
	inline void set_targetPanCam_28(GameObject_t1113636619 * value)
	{
		___targetPanCam_28 = value;
		Il2CppCodeGenWriteBarrier(&___targetPanCam_28, value);
	}

	inline static int32_t get_offset_of_panKey_29() { return static_cast<int32_t>(offsetof(SmoothOrbitCam_t3313789169, ___panKey_29)); }
	inline int32_t get_panKey_29() const { return ___panKey_29; }
	inline int32_t* get_address_of_panKey_29() { return &___panKey_29; }
	inline void set_panKey_29(int32_t value)
	{
		___panKey_29 = value;
	}

	inline static int32_t get_offset_of_panSpeed_30() { return static_cast<int32_t>(offsetof(SmoothOrbitCam_t3313789169, ___panSpeed_30)); }
	inline float get_panSpeed_30() const { return ___panSpeed_30; }
	inline float* get_address_of_panSpeed_30() { return &___panSpeed_30; }
	inline void set_panSpeed_30(float value)
	{
		___panSpeed_30 = value;
	}

	inline static int32_t get_offset_of_LimitPan_31() { return static_cast<int32_t>(offsetof(SmoothOrbitCam_t3313789169, ___LimitPan_31)); }
	inline bool get_LimitPan_31() const { return ___LimitPan_31; }
	inline bool* get_address_of_LimitPan_31() { return &___LimitPan_31; }
	inline void set_LimitPan_31(bool value)
	{
		___LimitPan_31 = value;
	}

	inline static int32_t get_offset_of_PanLimitsLeftRight_32() { return static_cast<int32_t>(offsetof(SmoothOrbitCam_t3313789169, ___PanLimitsLeftRight_32)); }
	inline Vector2_t2156229523  get_PanLimitsLeftRight_32() const { return ___PanLimitsLeftRight_32; }
	inline Vector2_t2156229523 * get_address_of_PanLimitsLeftRight_32() { return &___PanLimitsLeftRight_32; }
	inline void set_PanLimitsLeftRight_32(Vector2_t2156229523  value)
	{
		___PanLimitsLeftRight_32 = value;
	}

	inline static int32_t get_offset_of_PanLimitsUpDown_33() { return static_cast<int32_t>(offsetof(SmoothOrbitCam_t3313789169, ___PanLimitsUpDown_33)); }
	inline Vector2_t2156229523  get_PanLimitsUpDown_33() const { return ___PanLimitsUpDown_33; }
	inline Vector2_t2156229523 * get_address_of_PanLimitsUpDown_33() { return &___PanLimitsUpDown_33; }
	inline void set_PanLimitsUpDown_33(Vector2_t2156229523  value)
	{
		___PanLimitsUpDown_33 = value;
	}

	inline static int32_t get_offset_of_xOffset_34() { return static_cast<int32_t>(offsetof(SmoothOrbitCam_t3313789169, ___xOffset_34)); }
	inline float get_xOffset_34() const { return ___xOffset_34; }
	inline float* get_address_of_xOffset_34() { return &___xOffset_34; }
	inline void set_xOffset_34(float value)
	{
		___xOffset_34 = value;
	}

	inline static int32_t get_offset_of_yOffset_35() { return static_cast<int32_t>(offsetof(SmoothOrbitCam_t3313789169, ___yOffset_35)); }
	inline float get_yOffset_35() const { return ___yOffset_35; }
	inline float* get_address_of_yOffset_35() { return &___yOffset_35; }
	inline void set_yOffset_35(float value)
	{
		___yOffset_35 = value;
	}

	inline static int32_t get_offset_of_enableAutomaticOrbiting_36() { return static_cast<int32_t>(offsetof(SmoothOrbitCam_t3313789169, ___enableAutomaticOrbiting_36)); }
	inline bool get_enableAutomaticOrbiting_36() const { return ___enableAutomaticOrbiting_36; }
	inline bool* get_address_of_enableAutomaticOrbiting_36() { return &___enableAutomaticOrbiting_36; }
	inline void set_enableAutomaticOrbiting_36(bool value)
	{
		___enableAutomaticOrbiting_36 = value;
	}

	inline static int32_t get_offset_of_orbitingSpeed_37() { return static_cast<int32_t>(offsetof(SmoothOrbitCam_t3313789169, ___orbitingSpeed_37)); }
	inline float get_orbitingSpeed_37() const { return ___orbitingSpeed_37; }
	inline float* get_address_of_orbitingSpeed_37() { return &___orbitingSpeed_37; }
	inline void set_orbitingSpeed_37(float value)
	{
		___orbitingSpeed_37 = value;
	}

	inline static int32_t get_offset_of_NoObjectsBetween_38() { return static_cast<int32_t>(offsetof(SmoothOrbitCam_t3313789169, ___NoObjectsBetween_38)); }
	inline bool get_NoObjectsBetween_38() const { return ___NoObjectsBetween_38; }
	inline bool* get_address_of_NoObjectsBetween_38() { return &___NoObjectsBetween_38; }
	inline void set_NoObjectsBetween_38(bool value)
	{
		___NoObjectsBetween_38 = value;
	}

	inline static int32_t get_offset_of_EnableGroundHovering_39() { return static_cast<int32_t>(offsetof(SmoothOrbitCam_t3313789169, ___EnableGroundHovering_39)); }
	inline bool get_EnableGroundHovering_39() const { return ___EnableGroundHovering_39; }
	inline bool* get_address_of_EnableGroundHovering_39() { return &___EnableGroundHovering_39; }
	inline void set_EnableGroundHovering_39(bool value)
	{
		___EnableGroundHovering_39 = value;
	}

	inline static int32_t get_offset_of_GroundHoverDistance_40() { return static_cast<int32_t>(offsetof(SmoothOrbitCam_t3313789169, ___GroundHoverDistance_40)); }
	inline float get_GroundHoverDistance_40() const { return ___GroundHoverDistance_40; }
	inline float* get_address_of_GroundHoverDistance_40() { return &___GroundHoverDistance_40; }
	inline void set_GroundHoverDistance_40(float value)
	{
		___GroundHoverDistance_40 = value;
	}

	inline static int32_t get_offset_of_UiBlocksInteraction_41() { return static_cast<int32_t>(offsetof(SmoothOrbitCam_t3313789169, ___UiBlocksInteraction_41)); }
	inline bool get_UiBlocksInteraction_41() const { return ___UiBlocksInteraction_41; }
	inline bool* get_address_of_UiBlocksInteraction_41() { return &___UiBlocksInteraction_41; }
	inline void set_UiBlocksInteraction_41(bool value)
	{
		___UiBlocksInteraction_41 = value;
	}

	inline static int32_t get_offset_of_uiBlocking_42() { return static_cast<int32_t>(offsetof(SmoothOrbitCam_t3313789169, ___uiBlocking_42)); }
	inline bool get_uiBlocking_42() const { return ___uiBlocking_42; }
	inline bool* get_address_of_uiBlocking_42() { return &___uiBlocking_42; }
	inline void set_uiBlocking_42(bool value)
	{
		___uiBlocking_42 = value;
	}

	inline static int32_t get_offset_of_tempPanPosition_43() { return static_cast<int32_t>(offsetof(SmoothOrbitCam_t3313789169, ___tempPanPosition_43)); }
	inline Vector3_t3722313464  get_tempPanPosition_43() const { return ___tempPanPosition_43; }
	inline Vector3_t3722313464 * get_address_of_tempPanPosition_43() { return &___tempPanPosition_43; }
	inline void set_tempPanPosition_43(Vector3_t3722313464  value)
	{
		___tempPanPosition_43 = value;
	}

	inline static int32_t get_offset_of_velocityPanX_44() { return static_cast<int32_t>(offsetof(SmoothOrbitCam_t3313789169, ___velocityPanX_44)); }
	inline float get_velocityPanX_44() const { return ___velocityPanX_44; }
	inline float* get_address_of_velocityPanX_44() { return &___velocityPanX_44; }
	inline void set_velocityPanX_44(float value)
	{
		___velocityPanX_44 = value;
	}

	inline static int32_t get_offset_of_velocityPanY_45() { return static_cast<int32_t>(offsetof(SmoothOrbitCam_t3313789169, ___velocityPanY_45)); }
	inline float get_velocityPanY_45() const { return ___velocityPanY_45; }
	inline float* get_address_of_velocityPanY_45() { return &___velocityPanY_45; }
	inline void set_velocityPanY_45(float value)
	{
		___velocityPanY_45 = value;
	}

	inline static int32_t get_offset_of_doOrbit_46() { return static_cast<int32_t>(offsetof(SmoothOrbitCam_t3313789169, ___doOrbit_46)); }
	inline bool get_doOrbit_46() const { return ___doOrbit_46; }
	inline bool* get_address_of_doOrbit_46() { return &___doOrbit_46; }
	inline void set_doOrbit_46(bool value)
	{
		___doOrbit_46 = value;
	}

	inline static int32_t get_offset_of_eventSystem_47() { return static_cast<int32_t>(offsetof(SmoothOrbitCam_t3313789169, ___eventSystem_47)); }
	inline EventSystem_t1003666588 * get_eventSystem_47() const { return ___eventSystem_47; }
	inline EventSystem_t1003666588 ** get_address_of_eventSystem_47() { return &___eventSystem_47; }
	inline void set_eventSystem_47(EventSystem_t1003666588 * value)
	{
		___eventSystem_47 = value;
		Il2CppCodeGenWriteBarrier(&___eventSystem_47, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
