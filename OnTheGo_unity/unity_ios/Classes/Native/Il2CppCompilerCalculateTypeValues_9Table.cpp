﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "Mono_Security_Mono_Math_Prime_Generator_PrimeGenera446028866.h"
#include "Mono_Security_Mono_Math_Prime_Generator_Sequential2996090508.h"
#include "Mono_Security_Mono_Security_ASN12114160832.h"
#include "Mono_Security_Mono_Security_ASN1Convert2839890152.h"
#include "Mono_Security_Mono_Security_BitConverterLE2108532978.h"
#include "Mono_Security_Mono_Security_PKCS71860834338.h"
#include "Mono_Security_Mono_Security_PKCS7_ContentInfo3218159895.h"
#include "Mono_Security_Mono_Security_PKCS7_EncryptedData3577548732.h"
#include "Mono_Security_Mono_Security_Cryptography_ARC4Manag2641858452.h"
#include "Mono_Security_Mono_Security_Cryptography_CryptoConv610933156.h"
#include "Mono_Security_Mono_Security_Cryptography_KeyBuilde2049230354.h"
#include "Mono_Security_Mono_Security_Cryptography_MD21561046427.h"
#include "Mono_Security_Mono_Security_Cryptography_MD2Manage1377101535.h"
#include "Mono_Security_Mono_Security_Cryptography_MD41560915355.h"
#include "Mono_Security_Mono_Security_Cryptography_MD4Managed957540063.h"
#include "Mono_Security_Mono_Security_Cryptography_PKCS11505584676.h"
#include "Mono_Security_Mono_Security_Cryptography_PKCS8696280612.h"
#include "Mono_Security_Mono_Security_Cryptography_PKCS8_Priv668027992.h"
#include "Mono_Security_Mono_Security_Cryptography_PKCS8_Encr862116835.h"
#include "Mono_Security_Mono_Security_Cryptography_RC42752556436.h"
#include "Mono_Security_Mono_Security_Cryptography_RSAManage1757093819.h"
#include "Mono_Security_Mono_Security_Cryptography_RSAManage3064139577.h"
#include "Mono_Security_Mono_Security_X509_SafeBag3961248199.h"
#include "Mono_Security_Mono_Security_X509_PKCS124101533060.h"
#include "Mono_Security_Mono_Security_X509_PKCS12_DeriveByte1492915135.h"
#include "Mono_Security_Mono_Security_X509_X5011758824425.h"
#include "Mono_Security_Mono_Security_X509_X509Certificate489243024.h"
#include "Mono_Security_Mono_Security_X509_X509CertificateCo1542168549.h"
#include "Mono_Security_Mono_Security_X509_X509CertificateCo3515934697.h"
#include "Mono_Security_Mono_Security_X509_X509Chain863783600.h"
#include "Mono_Security_Mono_Security_X509_X509ChainStatusFl1831553602.h"
#include "Mono_Security_Mono_Security_X509_X509Crl1148767388.h"
#include "Mono_Security_Mono_Security_X509_X509Crl_X509CrlEnt645568789.h"
#include "Mono_Security_Mono_Security_X509_X509Extension3173393652.h"
#include "Mono_Security_Mono_Security_X509_X509ExtensionColle609554708.h"
#include "Mono_Security_Mono_Security_X509_X509Store2777415283.h"
#include "Mono_Security_Mono_Security_X509_X509StoreManager1046782375.h"
#include "Mono_Security_Mono_Security_X509_X509Stores1373936237.h"
#include "Mono_Security_Mono_Security_X509_Extensions_Author1122691429.h"
#include "Mono_Security_Mono_Security_X509_Extensions_BasicC2462195278.h"
#include "Mono_Security_Mono_Security_X509_Extensions_Extend3929363080.h"
#include "Mono_Security_Mono_Security_X509_Extensions_Genera2702294159.h"
#include "Mono_Security_Mono_Security_X509_Extensions_KeyUsag820456313.h"
#include "Mono_Security_Mono_Security_X509_Extensions_KeyUsa1795615912.h"
#include "Mono_Security_Mono_Security_X509_Extensions_Netsca1524296876.h"
#include "Mono_Security_Mono_Security_X509_Extensions_Netsca3317701015.h"
#include "Mono_Security_Mono_Security_X509_Extensions_Subjec1536937677.h"
#include "Mono_Security_Mono_Security_Cryptography_HMAC3689525210.h"
#include "Mono_Security_Mono_Security_Cryptography_MD5SHA1723838944.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_AlertLeve2246417555.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_AlertDesc1549755611.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_Alert4059934885.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_CipherAlg1174400495.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_CipherSui3414744575.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_CipherSui1129639304.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_CipherSui3316559455.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_ClientCon2797401965.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_ClientRec2031137796.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_ClientSes1775821398.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_ClientSes2353595803.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_ContentTy2602934270.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_Context3971234707.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_ExchangeA1320888206.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_HandshakeS756684113.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_HashAlgor2376832258.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_HttpsClie1160552561.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_RecordPro3759049701.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_RecordPro3680907657.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_RecordPro3718352467.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_RSASslSig3558097625.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_RSASslSig2709678514.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_SecurityC4242483129.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_SecurityP2199972650.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_SecurityP1513093309.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_ServerCon3848440993.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_Validatio3834298736.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_SslClient3914624661.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_SslCipher1981645747.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_SslHandsh2107581772.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_SslStream1667413407.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_SslStream3504282820.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_TlsCipher1545013223.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_TlsClient2486039503.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_TlsExcept3534743363.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_TlsServer4144396432.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_TlsStream2365453965.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_Handshake1004704908.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_Handshake3696583168.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_Handshake3062346172.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_Handshake3519510577.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_Handshake1824902654.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_Handshake2486981163.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_Handshake_C97965998.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_Handshake_643923608.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_Handshake2716496392.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_Handshake3690397592.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_Handshake3860330041.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_Handshake3343859594.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_Handshake1850379324.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_Handshake_699469151.h"







#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize900 = { sizeof (PrimeGeneratorBase_t446028867), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize901 = { sizeof (SequentialSearchPrimeGeneratorBase_t2996090509), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize902 = { sizeof (ASN1_t2114160833), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable902[3] = 
{
	ASN1_t2114160833::get_offset_of_m_nTag_0(),
	ASN1_t2114160833::get_offset_of_m_aValue_1(),
	ASN1_t2114160833::get_offset_of_elist_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize903 = { sizeof (ASN1Convert_t2839890153), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize904 = { sizeof (BitConverterLE_t2108532979), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize905 = { sizeof (PKCS7_t1860834339), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize906 = { sizeof (ContentInfo_t3218159896), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable906[2] = 
{
	ContentInfo_t3218159896::get_offset_of_contentType_0(),
	ContentInfo_t3218159896::get_offset_of_content_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize907 = { sizeof (EncryptedData_t3577548733), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable907[4] = 
{
	EncryptedData_t3577548733::get_offset_of__version_0(),
	EncryptedData_t3577548733::get_offset_of__content_1(),
	EncryptedData_t3577548733::get_offset_of__encryptionAlgorithm_2(),
	EncryptedData_t3577548733::get_offset_of__encrypted_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize908 = { sizeof (ARC4Managed_t2641858452), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable908[5] = 
{
	ARC4Managed_t2641858452::get_offset_of_key_12(),
	ARC4Managed_t2641858452::get_offset_of_state_13(),
	ARC4Managed_t2641858452::get_offset_of_x_14(),
	ARC4Managed_t2641858452::get_offset_of_y_15(),
	ARC4Managed_t2641858452::get_offset_of_m_disposed_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize909 = { sizeof (CryptoConvert_t610933157), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize910 = { sizeof (KeyBuilder_t2049230355), -1, sizeof(KeyBuilder_t2049230355_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable910[1] = 
{
	KeyBuilder_t2049230355_StaticFields::get_offset_of_rng_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize911 = { sizeof (MD2_t1561046427), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize912 = { sizeof (MD2Managed_t1377101535), -1, sizeof(MD2Managed_t1377101535_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable912[6] = 
{
	MD2Managed_t1377101535::get_offset_of_state_4(),
	MD2Managed_t1377101535::get_offset_of_checksum_5(),
	MD2Managed_t1377101535::get_offset_of_buffer_6(),
	MD2Managed_t1377101535::get_offset_of_count_7(),
	MD2Managed_t1377101535::get_offset_of_x_8(),
	MD2Managed_t1377101535_StaticFields::get_offset_of_PI_SUBST_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize913 = { sizeof (MD4_t1560915355), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize914 = { sizeof (MD4Managed_t957540063), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable914[5] = 
{
	MD4Managed_t957540063::get_offset_of_state_4(),
	MD4Managed_t957540063::get_offset_of_buffer_5(),
	MD4Managed_t957540063::get_offset_of_count_6(),
	MD4Managed_t957540063::get_offset_of_x_7(),
	MD4Managed_t957540063::get_offset_of_digest_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize915 = { sizeof (PKCS1_t1505584677), -1, sizeof(PKCS1_t1505584677_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable915[4] = 
{
	PKCS1_t1505584677_StaticFields::get_offset_of_emptySHA1_0(),
	PKCS1_t1505584677_StaticFields::get_offset_of_emptySHA256_1(),
	PKCS1_t1505584677_StaticFields::get_offset_of_emptySHA384_2(),
	PKCS1_t1505584677_StaticFields::get_offset_of_emptySHA512_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize916 = { sizeof (PKCS8_t696280613), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize917 = { sizeof (PrivateKeyInfo_t668027993), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable917[4] = 
{
	PrivateKeyInfo_t668027993::get_offset_of__version_0(),
	PrivateKeyInfo_t668027993::get_offset_of__algorithm_1(),
	PrivateKeyInfo_t668027993::get_offset_of__key_2(),
	PrivateKeyInfo_t668027993::get_offset_of__list_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize918 = { sizeof (EncryptedPrivateKeyInfo_t862116836), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable918[4] = 
{
	EncryptedPrivateKeyInfo_t862116836::get_offset_of__algorithm_0(),
	EncryptedPrivateKeyInfo_t862116836::get_offset_of__salt_1(),
	EncryptedPrivateKeyInfo_t862116836::get_offset_of__iterations_2(),
	EncryptedPrivateKeyInfo_t862116836::get_offset_of__data_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize919 = { sizeof (RC4_t2752556436), -1, sizeof(RC4_t2752556436_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable919[2] = 
{
	RC4_t2752556436_StaticFields::get_offset_of_s_legalBlockSizes_10(),
	RC4_t2752556436_StaticFields::get_offset_of_s_legalKeySizes_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize920 = { sizeof (RSAManaged_t1757093820), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable920[13] = 
{
	RSAManaged_t1757093820::get_offset_of_isCRTpossible_2(),
	RSAManaged_t1757093820::get_offset_of_keyBlinding_3(),
	RSAManaged_t1757093820::get_offset_of_keypairGenerated_4(),
	RSAManaged_t1757093820::get_offset_of_m_disposed_5(),
	RSAManaged_t1757093820::get_offset_of_d_6(),
	RSAManaged_t1757093820::get_offset_of_p_7(),
	RSAManaged_t1757093820::get_offset_of_q_8(),
	RSAManaged_t1757093820::get_offset_of_dp_9(),
	RSAManaged_t1757093820::get_offset_of_dq_10(),
	RSAManaged_t1757093820::get_offset_of_qInv_11(),
	RSAManaged_t1757093820::get_offset_of_n_12(),
	RSAManaged_t1757093820::get_offset_of_e_13(),
	RSAManaged_t1757093820::get_offset_of_KeyGenerated_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize921 = { sizeof (KeyGeneratedEventHandler_t3064139578), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize922 = { sizeof (SafeBag_t3961248200), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable922[2] = 
{
	SafeBag_t3961248200::get_offset_of__bagOID_0(),
	SafeBag_t3961248200::get_offset_of__asn1_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize923 = { sizeof (PKCS12_t4101533061), -1, sizeof(PKCS12_t4101533061_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable923[17] = 
{
	PKCS12_t4101533061_StaticFields::get_offset_of_recommendedIterationCount_0(),
	PKCS12_t4101533061::get_offset_of__password_1(),
	PKCS12_t4101533061::get_offset_of__keyBags_2(),
	PKCS12_t4101533061::get_offset_of__secretBags_3(),
	PKCS12_t4101533061::get_offset_of__certs_4(),
	PKCS12_t4101533061::get_offset_of__keyBagsChanged_5(),
	PKCS12_t4101533061::get_offset_of__secretBagsChanged_6(),
	PKCS12_t4101533061::get_offset_of__certsChanged_7(),
	PKCS12_t4101533061::get_offset_of__iterations_8(),
	PKCS12_t4101533061::get_offset_of__safeBags_9(),
	PKCS12_t4101533061::get_offset_of__rng_10(),
	PKCS12_t4101533061_StaticFields::get_offset_of_password_max_length_11(),
	PKCS12_t4101533061_StaticFields::get_offset_of_U3CU3Ef__switchU24map5_12(),
	PKCS12_t4101533061_StaticFields::get_offset_of_U3CU3Ef__switchU24map6_13(),
	PKCS12_t4101533061_StaticFields::get_offset_of_U3CU3Ef__switchU24map7_14(),
	PKCS12_t4101533061_StaticFields::get_offset_of_U3CU3Ef__switchU24map8_15(),
	PKCS12_t4101533061_StaticFields::get_offset_of_U3CU3Ef__switchU24mapC_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize924 = { sizeof (DeriveBytes_t1492915136), -1, sizeof(DeriveBytes_t1492915136_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable924[7] = 
{
	DeriveBytes_t1492915136_StaticFields::get_offset_of_keyDiversifier_0(),
	DeriveBytes_t1492915136_StaticFields::get_offset_of_ivDiversifier_1(),
	DeriveBytes_t1492915136_StaticFields::get_offset_of_macDiversifier_2(),
	DeriveBytes_t1492915136::get_offset_of__hashName_3(),
	DeriveBytes_t1492915136::get_offset_of__iterations_4(),
	DeriveBytes_t1492915136::get_offset_of__password_5(),
	DeriveBytes_t1492915136::get_offset_of__salt_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize925 = { sizeof (X501_t1758824426), -1, sizeof(X501_t1758824426_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable925[15] = 
{
	X501_t1758824426_StaticFields::get_offset_of_countryName_0(),
	X501_t1758824426_StaticFields::get_offset_of_organizationName_1(),
	X501_t1758824426_StaticFields::get_offset_of_organizationalUnitName_2(),
	X501_t1758824426_StaticFields::get_offset_of_commonName_3(),
	X501_t1758824426_StaticFields::get_offset_of_localityName_4(),
	X501_t1758824426_StaticFields::get_offset_of_stateOrProvinceName_5(),
	X501_t1758824426_StaticFields::get_offset_of_streetAddress_6(),
	X501_t1758824426_StaticFields::get_offset_of_domainComponent_7(),
	X501_t1758824426_StaticFields::get_offset_of_userid_8(),
	X501_t1758824426_StaticFields::get_offset_of_email_9(),
	X501_t1758824426_StaticFields::get_offset_of_dnQualifier_10(),
	X501_t1758824426_StaticFields::get_offset_of_title_11(),
	X501_t1758824426_StaticFields::get_offset_of_surname_12(),
	X501_t1758824426_StaticFields::get_offset_of_givenName_13(),
	X501_t1758824426_StaticFields::get_offset_of_initial_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize926 = { sizeof (X509Certificate_t489243025), -1, sizeof(X509Certificate_t489243025_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable926[26] = 
{
	X509Certificate_t489243025::get_offset_of_decoder_0(),
	X509Certificate_t489243025::get_offset_of_m_encodedcert_1(),
	X509Certificate_t489243025::get_offset_of_m_from_2(),
	X509Certificate_t489243025::get_offset_of_m_until_3(),
	X509Certificate_t489243025::get_offset_of_issuer_4(),
	X509Certificate_t489243025::get_offset_of_m_issuername_5(),
	X509Certificate_t489243025::get_offset_of_m_keyalgo_6(),
	X509Certificate_t489243025::get_offset_of_m_keyalgoparams_7(),
	X509Certificate_t489243025::get_offset_of_subject_8(),
	X509Certificate_t489243025::get_offset_of_m_subject_9(),
	X509Certificate_t489243025::get_offset_of_m_publickey_10(),
	X509Certificate_t489243025::get_offset_of_signature_11(),
	X509Certificate_t489243025::get_offset_of_m_signaturealgo_12(),
	X509Certificate_t489243025::get_offset_of_m_signaturealgoparams_13(),
	X509Certificate_t489243025::get_offset_of_certhash_14(),
	X509Certificate_t489243025::get_offset_of__rsa_15(),
	X509Certificate_t489243025::get_offset_of__dsa_16(),
	X509Certificate_t489243025::get_offset_of_version_17(),
	X509Certificate_t489243025::get_offset_of_serialnumber_18(),
	X509Certificate_t489243025::get_offset_of_issuerUniqueID_19(),
	X509Certificate_t489243025::get_offset_of_subjectUniqueID_20(),
	X509Certificate_t489243025::get_offset_of_extensions_21(),
	X509Certificate_t489243025_StaticFields::get_offset_of_encoding_error_22(),
	X509Certificate_t489243025_StaticFields::get_offset_of_U3CU3Ef__switchU24mapF_23(),
	X509Certificate_t489243025_StaticFields::get_offset_of_U3CU3Ef__switchU24map10_24(),
	X509Certificate_t489243025_StaticFields::get_offset_of_U3CU3Ef__switchU24map11_25(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize927 = { sizeof (X509CertificateCollection_t1542168550), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize928 = { sizeof (X509CertificateEnumerator_t3515934698), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable928[1] = 
{
	X509CertificateEnumerator_t3515934698::get_offset_of_enumerator_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize929 = { sizeof (X509Chain_t863783600), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable929[5] = 
{
	X509Chain_t863783600::get_offset_of_roots_0(),
	X509Chain_t863783600::get_offset_of_certs_1(),
	X509Chain_t863783600::get_offset_of__root_2(),
	X509Chain_t863783600::get_offset_of__chain_3(),
	X509Chain_t863783600::get_offset_of__status_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize930 = { sizeof (X509ChainStatusFlags_t1831553602)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable930[8] = 
{
	X509ChainStatusFlags_t1831553602::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize931 = { sizeof (X509Crl_t1148767388), -1, sizeof(X509Crl_t1148767388_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable931[11] = 
{
	X509Crl_t1148767388::get_offset_of_issuer_0(),
	X509Crl_t1148767388::get_offset_of_version_1(),
	X509Crl_t1148767388::get_offset_of_thisUpdate_2(),
	X509Crl_t1148767388::get_offset_of_nextUpdate_3(),
	X509Crl_t1148767388::get_offset_of_entries_4(),
	X509Crl_t1148767388::get_offset_of_signatureOID_5(),
	X509Crl_t1148767388::get_offset_of_signature_6(),
	X509Crl_t1148767388::get_offset_of_extensions_7(),
	X509Crl_t1148767388::get_offset_of_encoded_8(),
	X509Crl_t1148767388::get_offset_of_hash_value_9(),
	X509Crl_t1148767388_StaticFields::get_offset_of_U3CU3Ef__switchU24map13_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize932 = { sizeof (X509CrlEntry_t645568789), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable932[3] = 
{
	X509CrlEntry_t645568789::get_offset_of_sn_0(),
	X509CrlEntry_t645568789::get_offset_of_revocationDate_1(),
	X509CrlEntry_t645568789::get_offset_of_extensions_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize933 = { sizeof (X509Extension_t3173393653), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable933[3] = 
{
	X509Extension_t3173393653::get_offset_of_extnOid_0(),
	X509Extension_t3173393653::get_offset_of_extnCritical_1(),
	X509Extension_t3173393653::get_offset_of_extnValue_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize934 = { sizeof (X509ExtensionCollection_t609554709), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable934[1] = 
{
	X509ExtensionCollection_t609554709::get_offset_of_readOnly_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize935 = { sizeof (X509Store_t2777415283), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable935[4] = 
{
	X509Store_t2777415283::get_offset_of__storePath_0(),
	X509Store_t2777415283::get_offset_of__certificates_1(),
	X509Store_t2777415283::get_offset_of__crls_2(),
	X509Store_t2777415283::get_offset_of__crl_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize936 = { sizeof (X509StoreManager_t1046782375), -1, sizeof(X509StoreManager_t1046782375_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable936[2] = 
{
	X509StoreManager_t1046782375_StaticFields::get_offset_of__userStore_0(),
	X509StoreManager_t1046782375_StaticFields::get_offset_of__machineStore_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize937 = { sizeof (X509Stores_t1373936237), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable937[2] = 
{
	X509Stores_t1373936237::get_offset_of__storePath_0(),
	X509Stores_t1373936237::get_offset_of__trusted_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize938 = { sizeof (AuthorityKeyIdentifierExtension_t1122691429), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable938[1] = 
{
	AuthorityKeyIdentifierExtension_t1122691429::get_offset_of_aki_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize939 = { sizeof (BasicConstraintsExtension_t2462195278), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable939[2] = 
{
	BasicConstraintsExtension_t2462195278::get_offset_of_cA_3(),
	BasicConstraintsExtension_t2462195278::get_offset_of_pathLenConstraint_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize940 = { sizeof (ExtendedKeyUsageExtension_t3929363080), -1, sizeof(ExtendedKeyUsageExtension_t3929363080_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable940[2] = 
{
	ExtendedKeyUsageExtension_t3929363080::get_offset_of_keyPurpose_3(),
	ExtendedKeyUsageExtension_t3929363080_StaticFields::get_offset_of_U3CU3Ef__switchU24map14_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize941 = { sizeof (GeneralNames_t2702294159), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable941[5] = 
{
	GeneralNames_t2702294159::get_offset_of_rfc822Name_0(),
	GeneralNames_t2702294159::get_offset_of_dnsName_1(),
	GeneralNames_t2702294159::get_offset_of_directoryNames_2(),
	GeneralNames_t2702294159::get_offset_of_uris_3(),
	GeneralNames_t2702294159::get_offset_of_ipAddr_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize942 = { sizeof (KeyUsages_t820456313)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable942[11] = 
{
	KeyUsages_t820456313::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize943 = { sizeof (KeyUsageExtension_t1795615912), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable943[1] = 
{
	KeyUsageExtension_t1795615912::get_offset_of_kubits_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize944 = { sizeof (NetscapeCertTypeExtension_t1524296876), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable944[1] = 
{
	NetscapeCertTypeExtension_t1524296876::get_offset_of_ctbits_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize945 = { sizeof (CertTypes_t3317701015)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable945[8] = 
{
	CertTypes_t3317701015::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize946 = { sizeof (SubjectAltNameExtension_t1536937677), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable946[1] = 
{
	SubjectAltNameExtension_t1536937677::get_offset_of__names_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize947 = { sizeof (HMAC_t3689525210), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable947[4] = 
{
	HMAC_t3689525210::get_offset_of_hash_5(),
	HMAC_t3689525210::get_offset_of_hashing_6(),
	HMAC_t3689525210::get_offset_of_innerPad_7(),
	HMAC_t3689525210::get_offset_of_outerPad_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize948 = { sizeof (MD5SHA1_t723838944), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable948[3] = 
{
	MD5SHA1_t723838944::get_offset_of_md5_4(),
	MD5SHA1_t723838944::get_offset_of_sha_5(),
	MD5SHA1_t723838944::get_offset_of_hashing_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize949 = { sizeof (AlertLevel_t2246417555)+ sizeof (Il2CppObject), sizeof(uint8_t), 0, 0 };
extern const int32_t g_FieldOffsetTable949[3] = 
{
	AlertLevel_t2246417555::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize950 = { sizeof (AlertDescription_t1549755611)+ sizeof (Il2CppObject), sizeof(uint8_t), 0, 0 };
extern const int32_t g_FieldOffsetTable950[25] = 
{
	AlertDescription_t1549755611::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize951 = { sizeof (Alert_t4059934885), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable951[2] = 
{
	Alert_t4059934885::get_offset_of_level_0(),
	Alert_t4059934885::get_offset_of_description_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize952 = { sizeof (CipherAlgorithmType_t1174400495)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable952[8] = 
{
	CipherAlgorithmType_t1174400495::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize953 = { sizeof (CipherSuite_t3414744575), -1, sizeof(CipherSuite_t3414744575_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable953[21] = 
{
	CipherSuite_t3414744575_StaticFields::get_offset_of_EmptyArray_0(),
	CipherSuite_t3414744575::get_offset_of_code_1(),
	CipherSuite_t3414744575::get_offset_of_name_2(),
	CipherSuite_t3414744575::get_offset_of_cipherAlgorithmType_3(),
	CipherSuite_t3414744575::get_offset_of_hashAlgorithmType_4(),
	CipherSuite_t3414744575::get_offset_of_exchangeAlgorithmType_5(),
	CipherSuite_t3414744575::get_offset_of_isExportable_6(),
	CipherSuite_t3414744575::get_offset_of_cipherMode_7(),
	CipherSuite_t3414744575::get_offset_of_keyMaterialSize_8(),
	CipherSuite_t3414744575::get_offset_of_keyBlockSize_9(),
	CipherSuite_t3414744575::get_offset_of_expandedKeyMaterialSize_10(),
	CipherSuite_t3414744575::get_offset_of_effectiveKeyBits_11(),
	CipherSuite_t3414744575::get_offset_of_ivSize_12(),
	CipherSuite_t3414744575::get_offset_of_blockSize_13(),
	CipherSuite_t3414744575::get_offset_of_context_14(),
	CipherSuite_t3414744575::get_offset_of_encryptionAlgorithm_15(),
	CipherSuite_t3414744575::get_offset_of_encryptionCipher_16(),
	CipherSuite_t3414744575::get_offset_of_decryptionAlgorithm_17(),
	CipherSuite_t3414744575::get_offset_of_decryptionCipher_18(),
	CipherSuite_t3414744575::get_offset_of_clientHMAC_19(),
	CipherSuite_t3414744575::get_offset_of_serverHMAC_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize954 = { sizeof (CipherSuiteCollection_t1129639304), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable954[2] = 
{
	CipherSuiteCollection_t1129639304::get_offset_of_cipherSuites_0(),
	CipherSuiteCollection_t1129639304::get_offset_of_protocol_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize955 = { sizeof (CipherSuiteFactory_t3316559455), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize956 = { sizeof (ClientContext_t2797401965), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable956[2] = 
{
	ClientContext_t2797401965::get_offset_of_sslStream_30(),
	ClientContext_t2797401965::get_offset_of_clientHelloProtocol_31(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize957 = { sizeof (ClientRecordProtocol_t2031137796), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize958 = { sizeof (ClientSessionInfo_t1775821398), -1, sizeof(ClientSessionInfo_t1775821398_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable958[6] = 
{
	ClientSessionInfo_t1775821398_StaticFields::get_offset_of_ValidityInterval_0(),
	ClientSessionInfo_t1775821398::get_offset_of_disposed_1(),
	ClientSessionInfo_t1775821398::get_offset_of_validuntil_2(),
	ClientSessionInfo_t1775821398::get_offset_of_host_3(),
	ClientSessionInfo_t1775821398::get_offset_of_sid_4(),
	ClientSessionInfo_t1775821398::get_offset_of_masterSecret_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize959 = { sizeof (ClientSessionCache_t2353595803), -1, sizeof(ClientSessionCache_t2353595803_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable959[2] = 
{
	ClientSessionCache_t2353595803_StaticFields::get_offset_of_cache_0(),
	ClientSessionCache_t2353595803_StaticFields::get_offset_of_locker_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize960 = { sizeof (ContentType_t2602934270)+ sizeof (Il2CppObject), sizeof(uint8_t), 0, 0 };
extern const int32_t g_FieldOffsetTable960[5] = 
{
	ContentType_t2602934270::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize961 = { sizeof (Context_t3971234707), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable961[30] = 
{
	Context_t3971234707::get_offset_of_securityProtocol_0(),
	Context_t3971234707::get_offset_of_sessionId_1(),
	Context_t3971234707::get_offset_of_compressionMethod_2(),
	Context_t3971234707::get_offset_of_serverSettings_3(),
	Context_t3971234707::get_offset_of_clientSettings_4(),
	Context_t3971234707::get_offset_of_current_5(),
	Context_t3971234707::get_offset_of_negotiating_6(),
	Context_t3971234707::get_offset_of_read_7(),
	Context_t3971234707::get_offset_of_write_8(),
	Context_t3971234707::get_offset_of_supportedCiphers_9(),
	Context_t3971234707::get_offset_of_lastHandshakeMsg_10(),
	Context_t3971234707::get_offset_of_handshakeState_11(),
	Context_t3971234707::get_offset_of_abbreviatedHandshake_12(),
	Context_t3971234707::get_offset_of_receivedConnectionEnd_13(),
	Context_t3971234707::get_offset_of_sentConnectionEnd_14(),
	Context_t3971234707::get_offset_of_protocolNegotiated_15(),
	Context_t3971234707::get_offset_of_writeSequenceNumber_16(),
	Context_t3971234707::get_offset_of_readSequenceNumber_17(),
	Context_t3971234707::get_offset_of_clientRandom_18(),
	Context_t3971234707::get_offset_of_serverRandom_19(),
	Context_t3971234707::get_offset_of_randomCS_20(),
	Context_t3971234707::get_offset_of_randomSC_21(),
	Context_t3971234707::get_offset_of_masterSecret_22(),
	Context_t3971234707::get_offset_of_clientWriteKey_23(),
	Context_t3971234707::get_offset_of_serverWriteKey_24(),
	Context_t3971234707::get_offset_of_clientWriteIV_25(),
	Context_t3971234707::get_offset_of_serverWriteIV_26(),
	Context_t3971234707::get_offset_of_handshakeMessages_27(),
	Context_t3971234707::get_offset_of_random_28(),
	Context_t3971234707::get_offset_of_recordProtocol_29(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize962 = { sizeof (ExchangeAlgorithmType_t1320888206)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable962[6] = 
{
	ExchangeAlgorithmType_t1320888206::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize963 = { sizeof (HandshakeState_t756684113)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable963[4] = 
{
	HandshakeState_t756684113::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize964 = { sizeof (HashAlgorithmType_t2376832258)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable964[4] = 
{
	HashAlgorithmType_t2376832258::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize965 = { sizeof (HttpsClientStream_t1160552561), -1, sizeof(HttpsClientStream_t1160552561_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable965[4] = 
{
	HttpsClientStream_t1160552561::get_offset_of__request_20(),
	HttpsClientStream_t1160552561::get_offset_of__status_21(),
	HttpsClientStream_t1160552561_StaticFields::get_offset_of_U3CU3Ef__amU24cache2_22(),
	HttpsClientStream_t1160552561_StaticFields::get_offset_of_U3CU3Ef__amU24cache3_23(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize966 = { sizeof (RecordProtocol_t3759049701), -1, sizeof(RecordProtocol_t3759049701_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable966[3] = 
{
	RecordProtocol_t3759049701_StaticFields::get_offset_of_record_processing_0(),
	RecordProtocol_t3759049701::get_offset_of_innerStream_1(),
	RecordProtocol_t3759049701::get_offset_of_context_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize967 = { sizeof (ReceiveRecordAsyncResult_t3680907657), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable967[9] = 
{
	ReceiveRecordAsyncResult_t3680907657::get_offset_of_locker_0(),
	ReceiveRecordAsyncResult_t3680907657::get_offset_of__userCallback_1(),
	ReceiveRecordAsyncResult_t3680907657::get_offset_of__userState_2(),
	ReceiveRecordAsyncResult_t3680907657::get_offset_of__asyncException_3(),
	ReceiveRecordAsyncResult_t3680907657::get_offset_of_handle_4(),
	ReceiveRecordAsyncResult_t3680907657::get_offset_of__resultingBuffer_5(),
	ReceiveRecordAsyncResult_t3680907657::get_offset_of__record_6(),
	ReceiveRecordAsyncResult_t3680907657::get_offset_of_completed_7(),
	ReceiveRecordAsyncResult_t3680907657::get_offset_of__initialBuffer_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize968 = { sizeof (SendRecordAsyncResult_t3718352467), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable968[7] = 
{
	SendRecordAsyncResult_t3718352467::get_offset_of_locker_0(),
	SendRecordAsyncResult_t3718352467::get_offset_of__userCallback_1(),
	SendRecordAsyncResult_t3718352467::get_offset_of__userState_2(),
	SendRecordAsyncResult_t3718352467::get_offset_of__asyncException_3(),
	SendRecordAsyncResult_t3718352467::get_offset_of_handle_4(),
	SendRecordAsyncResult_t3718352467::get_offset_of__message_5(),
	SendRecordAsyncResult_t3718352467::get_offset_of_completed_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize969 = { sizeof (RSASslSignatureDeformatter_t3558097625), -1, sizeof(RSASslSignatureDeformatter_t3558097625_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable969[3] = 
{
	RSASslSignatureDeformatter_t3558097625::get_offset_of_key_0(),
	RSASslSignatureDeformatter_t3558097625::get_offset_of_hash_1(),
	RSASslSignatureDeformatter_t3558097625_StaticFields::get_offset_of_U3CU3Ef__switchU24map15_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize970 = { sizeof (RSASslSignatureFormatter_t2709678514), -1, sizeof(RSASslSignatureFormatter_t2709678514_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable970[3] = 
{
	RSASslSignatureFormatter_t2709678514::get_offset_of_key_0(),
	RSASslSignatureFormatter_t2709678514::get_offset_of_hash_1(),
	RSASslSignatureFormatter_t2709678514_StaticFields::get_offset_of_U3CU3Ef__switchU24map16_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize971 = { sizeof (SecurityCompressionType_t4242483129)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable971[3] = 
{
	SecurityCompressionType_t4242483129::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize972 = { sizeof (SecurityParameters_t2199972650), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable972[3] = 
{
	SecurityParameters_t2199972650::get_offset_of_cipher_0(),
	SecurityParameters_t2199972650::get_offset_of_clientWriteMAC_1(),
	SecurityParameters_t2199972650::get_offset_of_serverWriteMAC_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize973 = { sizeof (SecurityProtocolType_t1513093309)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable973[5] = 
{
	SecurityProtocolType_t1513093309::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize974 = { sizeof (ServerContext_t3848440993), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize975 = { sizeof (ValidationResult_t3834298736), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable975[2] = 
{
	ValidationResult_t3834298736::get_offset_of_trusted_0(),
	ValidationResult_t3834298736::get_offset_of_error_code_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize976 = { sizeof (SslClientStream_t3914624661), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable976[4] = 
{
	SslClientStream_t3914624661::get_offset_of_ServerCertValidation_16(),
	SslClientStream_t3914624661::get_offset_of_ClientCertSelection_17(),
	SslClientStream_t3914624661::get_offset_of_PrivateKeySelection_18(),
	SslClientStream_t3914624661::get_offset_of_ServerCertValidation2_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize977 = { sizeof (SslCipherSuite_t1981645747), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable977[3] = 
{
	SslCipherSuite_t1981645747::get_offset_of_pad1_21(),
	SslCipherSuite_t1981645747::get_offset_of_pad2_22(),
	SslCipherSuite_t1981645747::get_offset_of_header_23(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize978 = { sizeof (SslHandshakeHash_t2107581772), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable978[8] = 
{
	SslHandshakeHash_t2107581772::get_offset_of_md5_4(),
	SslHandshakeHash_t2107581772::get_offset_of_sha_5(),
	SslHandshakeHash_t2107581772::get_offset_of_hashing_6(),
	SslHandshakeHash_t2107581772::get_offset_of_secret_7(),
	SslHandshakeHash_t2107581772::get_offset_of_innerPadMD5_8(),
	SslHandshakeHash_t2107581772::get_offset_of_outerPadMD5_9(),
	SslHandshakeHash_t2107581772::get_offset_of_innerPadSHA_10(),
	SslHandshakeHash_t2107581772::get_offset_of_outerPadSHA_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize979 = { sizeof (SslStreamBase_t1667413407), -1, sizeof(SslStreamBase_t1667413407_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable979[15] = 
{
	0,
	SslStreamBase_t1667413407_StaticFields::get_offset_of_record_processing_2(),
	SslStreamBase_t1667413407::get_offset_of_innerStream_3(),
	SslStreamBase_t1667413407::get_offset_of_inputBuffer_4(),
	SslStreamBase_t1667413407::get_offset_of_context_5(),
	SslStreamBase_t1667413407::get_offset_of_protocol_6(),
	SslStreamBase_t1667413407::get_offset_of_ownsStream_7(),
	SslStreamBase_t1667413407::get_offset_of_disposed_8(),
	SslStreamBase_t1667413407::get_offset_of_checkCertRevocationStatus_9(),
	SslStreamBase_t1667413407::get_offset_of_negotiate_10(),
	SslStreamBase_t1667413407::get_offset_of_read_11(),
	SslStreamBase_t1667413407::get_offset_of_write_12(),
	SslStreamBase_t1667413407::get_offset_of_negotiationComplete_13(),
	SslStreamBase_t1667413407::get_offset_of_recbuf_14(),
	SslStreamBase_t1667413407::get_offset_of_recordStream_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize980 = { sizeof (InternalAsyncResult_t3504282820), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable980[12] = 
{
	InternalAsyncResult_t3504282820::get_offset_of_locker_0(),
	InternalAsyncResult_t3504282820::get_offset_of__userCallback_1(),
	InternalAsyncResult_t3504282820::get_offset_of__userState_2(),
	InternalAsyncResult_t3504282820::get_offset_of__asyncException_3(),
	InternalAsyncResult_t3504282820::get_offset_of_handle_4(),
	InternalAsyncResult_t3504282820::get_offset_of_completed_5(),
	InternalAsyncResult_t3504282820::get_offset_of__bytesRead_6(),
	InternalAsyncResult_t3504282820::get_offset_of__fromWrite_7(),
	InternalAsyncResult_t3504282820::get_offset_of__proceedAfterHandshake_8(),
	InternalAsyncResult_t3504282820::get_offset_of__buffer_9(),
	InternalAsyncResult_t3504282820::get_offset_of__offset_10(),
	InternalAsyncResult_t3504282820::get_offset_of__count_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize981 = { sizeof (TlsCipherSuite_t1545013223), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable981[2] = 
{
	TlsCipherSuite_t1545013223::get_offset_of_header_21(),
	TlsCipherSuite_t1545013223::get_offset_of_headerLock_22(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize982 = { sizeof (TlsClientSettings_t2486039503), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable982[4] = 
{
	TlsClientSettings_t2486039503::get_offset_of_targetHost_0(),
	TlsClientSettings_t2486039503::get_offset_of_certificates_1(),
	TlsClientSettings_t2486039503::get_offset_of_clientCertificate_2(),
	TlsClientSettings_t2486039503::get_offset_of_certificateRSA_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize983 = { sizeof (TlsException_t3534743363), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable983[1] = 
{
	TlsException_t3534743363::get_offset_of_alert_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize984 = { sizeof (TlsServerSettings_t4144396432), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable984[8] = 
{
	TlsServerSettings_t4144396432::get_offset_of_certificates_0(),
	TlsServerSettings_t4144396432::get_offset_of_certificateRSA_1(),
	TlsServerSettings_t4144396432::get_offset_of_rsaParameters_2(),
	TlsServerSettings_t4144396432::get_offset_of_signedParams_3(),
	TlsServerSettings_t4144396432::get_offset_of_distinguisedNames_4(),
	TlsServerSettings_t4144396432::get_offset_of_serverKeyExchange_5(),
	TlsServerSettings_t4144396432::get_offset_of_certificateRequest_6(),
	TlsServerSettings_t4144396432::get_offset_of_certificateTypes_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize985 = { sizeof (TlsStream_t2365453965), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable985[4] = 
{
	TlsStream_t2365453965::get_offset_of_canRead_1(),
	TlsStream_t2365453965::get_offset_of_canWrite_2(),
	TlsStream_t2365453965::get_offset_of_buffer_3(),
	TlsStream_t2365453965::get_offset_of_temp_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize986 = { sizeof (ClientCertificateType_t1004704908)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable986[6] = 
{
	ClientCertificateType_t1004704908::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize987 = { sizeof (HandshakeMessage_t3696583168), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable987[4] = 
{
	HandshakeMessage_t3696583168::get_offset_of_context_5(),
	HandshakeMessage_t3696583168::get_offset_of_handshakeType_6(),
	HandshakeMessage_t3696583168::get_offset_of_contentType_7(),
	HandshakeMessage_t3696583168::get_offset_of_cache_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize988 = { sizeof (HandshakeType_t3062346172)+ sizeof (Il2CppObject), sizeof(uint8_t), 0, 0 };
extern const int32_t g_FieldOffsetTable988[12] = 
{
	HandshakeType_t3062346172::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize989 = { sizeof (TlsClientCertificate_t3519510577), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable989[2] = 
{
	TlsClientCertificate_t3519510577::get_offset_of_clientCertSelected_9(),
	TlsClientCertificate_t3519510577::get_offset_of_clientCert_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize990 = { sizeof (TlsClientCertificateVerify_t1824902654), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize991 = { sizeof (TlsClientFinished_t2486981163), -1, sizeof(TlsClientFinished_t2486981163_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable991[1] = 
{
	TlsClientFinished_t2486981163_StaticFields::get_offset_of_Ssl3Marker_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize992 = { sizeof (TlsClientHello_t97965998), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable992[1] = 
{
	TlsClientHello_t97965998::get_offset_of_random_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize993 = { sizeof (TlsClientKeyExchange_t643923608), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize994 = { sizeof (TlsServerCertificate_t2716496392), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable994[1] = 
{
	TlsServerCertificate_t2716496392::get_offset_of_certificates_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize995 = { sizeof (TlsServerCertificateRequest_t3690397592), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable995[2] = 
{
	TlsServerCertificateRequest_t3690397592::get_offset_of_certificateTypes_9(),
	TlsServerCertificateRequest_t3690397592::get_offset_of_distinguisedNames_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize996 = { sizeof (TlsServerFinished_t3860330041), -1, sizeof(TlsServerFinished_t3860330041_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable996[1] = 
{
	TlsServerFinished_t3860330041_StaticFields::get_offset_of_Ssl3Marker_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize997 = { sizeof (TlsServerHello_t3343859594), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable997[4] = 
{
	TlsServerHello_t3343859594::get_offset_of_compressionMethod_9(),
	TlsServerHello_t3343859594::get_offset_of_random_10(),
	TlsServerHello_t3343859594::get_offset_of_sessionId_11(),
	TlsServerHello_t3343859594::get_offset_of_cipherSuite_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize998 = { sizeof (TlsServerHelloDone_t1850379324), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize999 = { sizeof (TlsServerKeyExchange_t699469151), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable999[2] = 
{
	TlsServerKeyExchange_t699469151::get_offset_of_rsaParams_9(),
	TlsServerKeyExchange_t699469151::get_offset_of_signedParams_10(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
