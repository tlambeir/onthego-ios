﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// System.String
struct String_t;
// UnityEngine.AssetBundleRequest
struct AssetBundleRequest_t699759206;
// JSONObject
struct JSONObject_t1339445639;
// InitSceneController
struct InitSceneController_t315947004;
// System.Object
struct Il2CppObject;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// InitSceneController/<loadAssetBundle>c__Iterator0
struct  U3CloadAssetBundleU3Ec__Iterator0_t4011785967  : public Il2CppObject
{
public:
	// System.String InitSceneController/<loadAssetBundle>c__Iterator0::<url>__0
	String_t* ___U3CurlU3E__0_0;
	// UnityEngine.AssetBundleRequest InitSceneController/<loadAssetBundle>c__Iterator0::<bundleGameObjectRequest>__0
	AssetBundleRequest_t699759206 * ___U3CbundleGameObjectRequestU3E__0_1;
	// UnityEngine.AssetBundleRequest InitSceneController/<loadAssetBundle>c__Iterator0::<bundleVideoClipRequest>__0
	AssetBundleRequest_t699759206 * ___U3CbundleVideoClipRequestU3E__0_2;
	// UnityEngine.AssetBundleRequest InitSceneController/<loadAssetBundle>c__Iterator0::<bundleImagesRequest>__0
	AssetBundleRequest_t699759206 * ___U3CbundleImagesRequestU3E__0_3;
	// JSONObject InitSceneController/<loadAssetBundle>c__Iterator0::jsonObj
	JSONObject_t1339445639 * ___jsonObj_4;
	// InitSceneController InitSceneController/<loadAssetBundle>c__Iterator0::$this
	InitSceneController_t315947004 * ___U24this_5;
	// System.Object InitSceneController/<loadAssetBundle>c__Iterator0::$current
	Il2CppObject * ___U24current_6;
	// System.Boolean InitSceneController/<loadAssetBundle>c__Iterator0::$disposing
	bool ___U24disposing_7;
	// System.Int32 InitSceneController/<loadAssetBundle>c__Iterator0::$PC
	int32_t ___U24PC_8;

public:
	inline static int32_t get_offset_of_U3CurlU3E__0_0() { return static_cast<int32_t>(offsetof(U3CloadAssetBundleU3Ec__Iterator0_t4011785967, ___U3CurlU3E__0_0)); }
	inline String_t* get_U3CurlU3E__0_0() const { return ___U3CurlU3E__0_0; }
	inline String_t** get_address_of_U3CurlU3E__0_0() { return &___U3CurlU3E__0_0; }
	inline void set_U3CurlU3E__0_0(String_t* value)
	{
		___U3CurlU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CurlU3E__0_0, value);
	}

	inline static int32_t get_offset_of_U3CbundleGameObjectRequestU3E__0_1() { return static_cast<int32_t>(offsetof(U3CloadAssetBundleU3Ec__Iterator0_t4011785967, ___U3CbundleGameObjectRequestU3E__0_1)); }
	inline AssetBundleRequest_t699759206 * get_U3CbundleGameObjectRequestU3E__0_1() const { return ___U3CbundleGameObjectRequestU3E__0_1; }
	inline AssetBundleRequest_t699759206 ** get_address_of_U3CbundleGameObjectRequestU3E__0_1() { return &___U3CbundleGameObjectRequestU3E__0_1; }
	inline void set_U3CbundleGameObjectRequestU3E__0_1(AssetBundleRequest_t699759206 * value)
	{
		___U3CbundleGameObjectRequestU3E__0_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CbundleGameObjectRequestU3E__0_1, value);
	}

	inline static int32_t get_offset_of_U3CbundleVideoClipRequestU3E__0_2() { return static_cast<int32_t>(offsetof(U3CloadAssetBundleU3Ec__Iterator0_t4011785967, ___U3CbundleVideoClipRequestU3E__0_2)); }
	inline AssetBundleRequest_t699759206 * get_U3CbundleVideoClipRequestU3E__0_2() const { return ___U3CbundleVideoClipRequestU3E__0_2; }
	inline AssetBundleRequest_t699759206 ** get_address_of_U3CbundleVideoClipRequestU3E__0_2() { return &___U3CbundleVideoClipRequestU3E__0_2; }
	inline void set_U3CbundleVideoClipRequestU3E__0_2(AssetBundleRequest_t699759206 * value)
	{
		___U3CbundleVideoClipRequestU3E__0_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CbundleVideoClipRequestU3E__0_2, value);
	}

	inline static int32_t get_offset_of_U3CbundleImagesRequestU3E__0_3() { return static_cast<int32_t>(offsetof(U3CloadAssetBundleU3Ec__Iterator0_t4011785967, ___U3CbundleImagesRequestU3E__0_3)); }
	inline AssetBundleRequest_t699759206 * get_U3CbundleImagesRequestU3E__0_3() const { return ___U3CbundleImagesRequestU3E__0_3; }
	inline AssetBundleRequest_t699759206 ** get_address_of_U3CbundleImagesRequestU3E__0_3() { return &___U3CbundleImagesRequestU3E__0_3; }
	inline void set_U3CbundleImagesRequestU3E__0_3(AssetBundleRequest_t699759206 * value)
	{
		___U3CbundleImagesRequestU3E__0_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CbundleImagesRequestU3E__0_3, value);
	}

	inline static int32_t get_offset_of_jsonObj_4() { return static_cast<int32_t>(offsetof(U3CloadAssetBundleU3Ec__Iterator0_t4011785967, ___jsonObj_4)); }
	inline JSONObject_t1339445639 * get_jsonObj_4() const { return ___jsonObj_4; }
	inline JSONObject_t1339445639 ** get_address_of_jsonObj_4() { return &___jsonObj_4; }
	inline void set_jsonObj_4(JSONObject_t1339445639 * value)
	{
		___jsonObj_4 = value;
		Il2CppCodeGenWriteBarrier(&___jsonObj_4, value);
	}

	inline static int32_t get_offset_of_U24this_5() { return static_cast<int32_t>(offsetof(U3CloadAssetBundleU3Ec__Iterator0_t4011785967, ___U24this_5)); }
	inline InitSceneController_t315947004 * get_U24this_5() const { return ___U24this_5; }
	inline InitSceneController_t315947004 ** get_address_of_U24this_5() { return &___U24this_5; }
	inline void set_U24this_5(InitSceneController_t315947004 * value)
	{
		___U24this_5 = value;
		Il2CppCodeGenWriteBarrier(&___U24this_5, value);
	}

	inline static int32_t get_offset_of_U24current_6() { return static_cast<int32_t>(offsetof(U3CloadAssetBundleU3Ec__Iterator0_t4011785967, ___U24current_6)); }
	inline Il2CppObject * get_U24current_6() const { return ___U24current_6; }
	inline Il2CppObject ** get_address_of_U24current_6() { return &___U24current_6; }
	inline void set_U24current_6(Il2CppObject * value)
	{
		___U24current_6 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_6, value);
	}

	inline static int32_t get_offset_of_U24disposing_7() { return static_cast<int32_t>(offsetof(U3CloadAssetBundleU3Ec__Iterator0_t4011785967, ___U24disposing_7)); }
	inline bool get_U24disposing_7() const { return ___U24disposing_7; }
	inline bool* get_address_of_U24disposing_7() { return &___U24disposing_7; }
	inline void set_U24disposing_7(bool value)
	{
		___U24disposing_7 = value;
	}

	inline static int32_t get_offset_of_U24PC_8() { return static_cast<int32_t>(offsetof(U3CloadAssetBundleU3Ec__Iterator0_t4011785967, ___U24PC_8)); }
	inline int32_t get_U24PC_8() const { return ___U24PC_8; }
	inline int32_t* get_address_of_U24PC_8() { return &___U24PC_8; }
	inline void set_U24PC_8(int32_t value)
	{
		___U24PC_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
