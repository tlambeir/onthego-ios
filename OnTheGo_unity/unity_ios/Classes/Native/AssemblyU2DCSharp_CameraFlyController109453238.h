﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour3962482529.h"
#include "UnityEngine_UnityEngine_Vector33722313464.h"

// UnityEngine.KeyCode[]
struct KeyCodeU5BU5D_t2223234056;
// UnityEngine.Transform
struct Transform_t3600365921;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CameraFlyController
struct  CameraFlyController_t109453238  : public MonoBehaviour_t3962482529
{
public:
	// System.Single CameraFlyController::keyboardMoveSpeed
	float ___keyboardMoveSpeed_2;
	// System.Single CameraFlyController::joystickAxisThresholdMin
	float ___joystickAxisThresholdMin_3;
	// System.Single CameraFlyController::joystickAxisThresholdMax
	float ___joystickAxisThresholdMax_4;
	// System.Single CameraFlyController::joystickMoveSpeedXMin
	float ___joystickMoveSpeedXMin_5;
	// System.Single CameraFlyController::joystickMoveSpeedXMax
	float ___joystickMoveSpeedXMax_6;
	// System.Single CameraFlyController::joystickMoveSpeedY
	float ___joystickMoveSpeedY_7;
	// System.Single CameraFlyController::joystickMoveSpeedZMin
	float ___joystickMoveSpeedZMin_8;
	// System.Single CameraFlyController::joystickMoveSpeedZMax
	float ___joystickMoveSpeedZMax_9;
	// System.Single CameraFlyController::joystickRotationSpeedMin
	float ___joystickRotationSpeedMin_10;
	// System.Single CameraFlyController::joystickRotationSpeedMax
	float ___joystickRotationSpeedMax_11;
	// UnityEngine.Transform CameraFlyController::tr
	Transform_t3600365921 * ___tr_21;
	// System.Boolean CameraFlyController::rmbDownInRect
	bool ___rmbDownInRect_22;
	// UnityEngine.Vector3 CameraFlyController::mpStart
	Vector3_t3722313464  ___mpStart_23;
	// UnityEngine.Vector3 CameraFlyController::originalRotation
	Vector3_t3722313464  ___originalRotation_24;
	// System.Single CameraFlyController::t
	float ___t_25;

public:
	inline static int32_t get_offset_of_keyboardMoveSpeed_2() { return static_cast<int32_t>(offsetof(CameraFlyController_t109453238, ___keyboardMoveSpeed_2)); }
	inline float get_keyboardMoveSpeed_2() const { return ___keyboardMoveSpeed_2; }
	inline float* get_address_of_keyboardMoveSpeed_2() { return &___keyboardMoveSpeed_2; }
	inline void set_keyboardMoveSpeed_2(float value)
	{
		___keyboardMoveSpeed_2 = value;
	}

	inline static int32_t get_offset_of_joystickAxisThresholdMin_3() { return static_cast<int32_t>(offsetof(CameraFlyController_t109453238, ___joystickAxisThresholdMin_3)); }
	inline float get_joystickAxisThresholdMin_3() const { return ___joystickAxisThresholdMin_3; }
	inline float* get_address_of_joystickAxisThresholdMin_3() { return &___joystickAxisThresholdMin_3; }
	inline void set_joystickAxisThresholdMin_3(float value)
	{
		___joystickAxisThresholdMin_3 = value;
	}

	inline static int32_t get_offset_of_joystickAxisThresholdMax_4() { return static_cast<int32_t>(offsetof(CameraFlyController_t109453238, ___joystickAxisThresholdMax_4)); }
	inline float get_joystickAxisThresholdMax_4() const { return ___joystickAxisThresholdMax_4; }
	inline float* get_address_of_joystickAxisThresholdMax_4() { return &___joystickAxisThresholdMax_4; }
	inline void set_joystickAxisThresholdMax_4(float value)
	{
		___joystickAxisThresholdMax_4 = value;
	}

	inline static int32_t get_offset_of_joystickMoveSpeedXMin_5() { return static_cast<int32_t>(offsetof(CameraFlyController_t109453238, ___joystickMoveSpeedXMin_5)); }
	inline float get_joystickMoveSpeedXMin_5() const { return ___joystickMoveSpeedXMin_5; }
	inline float* get_address_of_joystickMoveSpeedXMin_5() { return &___joystickMoveSpeedXMin_5; }
	inline void set_joystickMoveSpeedXMin_5(float value)
	{
		___joystickMoveSpeedXMin_5 = value;
	}

	inline static int32_t get_offset_of_joystickMoveSpeedXMax_6() { return static_cast<int32_t>(offsetof(CameraFlyController_t109453238, ___joystickMoveSpeedXMax_6)); }
	inline float get_joystickMoveSpeedXMax_6() const { return ___joystickMoveSpeedXMax_6; }
	inline float* get_address_of_joystickMoveSpeedXMax_6() { return &___joystickMoveSpeedXMax_6; }
	inline void set_joystickMoveSpeedXMax_6(float value)
	{
		___joystickMoveSpeedXMax_6 = value;
	}

	inline static int32_t get_offset_of_joystickMoveSpeedY_7() { return static_cast<int32_t>(offsetof(CameraFlyController_t109453238, ___joystickMoveSpeedY_7)); }
	inline float get_joystickMoveSpeedY_7() const { return ___joystickMoveSpeedY_7; }
	inline float* get_address_of_joystickMoveSpeedY_7() { return &___joystickMoveSpeedY_7; }
	inline void set_joystickMoveSpeedY_7(float value)
	{
		___joystickMoveSpeedY_7 = value;
	}

	inline static int32_t get_offset_of_joystickMoveSpeedZMin_8() { return static_cast<int32_t>(offsetof(CameraFlyController_t109453238, ___joystickMoveSpeedZMin_8)); }
	inline float get_joystickMoveSpeedZMin_8() const { return ___joystickMoveSpeedZMin_8; }
	inline float* get_address_of_joystickMoveSpeedZMin_8() { return &___joystickMoveSpeedZMin_8; }
	inline void set_joystickMoveSpeedZMin_8(float value)
	{
		___joystickMoveSpeedZMin_8 = value;
	}

	inline static int32_t get_offset_of_joystickMoveSpeedZMax_9() { return static_cast<int32_t>(offsetof(CameraFlyController_t109453238, ___joystickMoveSpeedZMax_9)); }
	inline float get_joystickMoveSpeedZMax_9() const { return ___joystickMoveSpeedZMax_9; }
	inline float* get_address_of_joystickMoveSpeedZMax_9() { return &___joystickMoveSpeedZMax_9; }
	inline void set_joystickMoveSpeedZMax_9(float value)
	{
		___joystickMoveSpeedZMax_9 = value;
	}

	inline static int32_t get_offset_of_joystickRotationSpeedMin_10() { return static_cast<int32_t>(offsetof(CameraFlyController_t109453238, ___joystickRotationSpeedMin_10)); }
	inline float get_joystickRotationSpeedMin_10() const { return ___joystickRotationSpeedMin_10; }
	inline float* get_address_of_joystickRotationSpeedMin_10() { return &___joystickRotationSpeedMin_10; }
	inline void set_joystickRotationSpeedMin_10(float value)
	{
		___joystickRotationSpeedMin_10 = value;
	}

	inline static int32_t get_offset_of_joystickRotationSpeedMax_11() { return static_cast<int32_t>(offsetof(CameraFlyController_t109453238, ___joystickRotationSpeedMax_11)); }
	inline float get_joystickRotationSpeedMax_11() const { return ___joystickRotationSpeedMax_11; }
	inline float* get_address_of_joystickRotationSpeedMax_11() { return &___joystickRotationSpeedMax_11; }
	inline void set_joystickRotationSpeedMax_11(float value)
	{
		___joystickRotationSpeedMax_11 = value;
	}

	inline static int32_t get_offset_of_tr_21() { return static_cast<int32_t>(offsetof(CameraFlyController_t109453238, ___tr_21)); }
	inline Transform_t3600365921 * get_tr_21() const { return ___tr_21; }
	inline Transform_t3600365921 ** get_address_of_tr_21() { return &___tr_21; }
	inline void set_tr_21(Transform_t3600365921 * value)
	{
		___tr_21 = value;
		Il2CppCodeGenWriteBarrier(&___tr_21, value);
	}

	inline static int32_t get_offset_of_rmbDownInRect_22() { return static_cast<int32_t>(offsetof(CameraFlyController_t109453238, ___rmbDownInRect_22)); }
	inline bool get_rmbDownInRect_22() const { return ___rmbDownInRect_22; }
	inline bool* get_address_of_rmbDownInRect_22() { return &___rmbDownInRect_22; }
	inline void set_rmbDownInRect_22(bool value)
	{
		___rmbDownInRect_22 = value;
	}

	inline static int32_t get_offset_of_mpStart_23() { return static_cast<int32_t>(offsetof(CameraFlyController_t109453238, ___mpStart_23)); }
	inline Vector3_t3722313464  get_mpStart_23() const { return ___mpStart_23; }
	inline Vector3_t3722313464 * get_address_of_mpStart_23() { return &___mpStart_23; }
	inline void set_mpStart_23(Vector3_t3722313464  value)
	{
		___mpStart_23 = value;
	}

	inline static int32_t get_offset_of_originalRotation_24() { return static_cast<int32_t>(offsetof(CameraFlyController_t109453238, ___originalRotation_24)); }
	inline Vector3_t3722313464  get_originalRotation_24() const { return ___originalRotation_24; }
	inline Vector3_t3722313464 * get_address_of_originalRotation_24() { return &___originalRotation_24; }
	inline void set_originalRotation_24(Vector3_t3722313464  value)
	{
		___originalRotation_24 = value;
	}

	inline static int32_t get_offset_of_t_25() { return static_cast<int32_t>(offsetof(CameraFlyController_t109453238, ___t_25)); }
	inline float get_t_25() const { return ___t_25; }
	inline float* get_address_of_t_25() { return &___t_25; }
	inline void set_t_25(float value)
	{
		___t_25 = value;
	}
};

struct CameraFlyController_t109453238_StaticFields
{
public:
	// UnityEngine.KeyCode[] CameraFlyController::keysForward
	KeyCodeU5BU5D_t2223234056* ___keysForward_12;
	// UnityEngine.KeyCode[] CameraFlyController::keysBackward
	KeyCodeU5BU5D_t2223234056* ___keysBackward_13;
	// UnityEngine.KeyCode[] CameraFlyController::keysLeft
	KeyCodeU5BU5D_t2223234056* ___keysLeft_14;
	// UnityEngine.KeyCode[] CameraFlyController::keysRight
	KeyCodeU5BU5D_t2223234056* ___keysRight_15;
	// UnityEngine.KeyCode[] CameraFlyController::keysUp
	KeyCodeU5BU5D_t2223234056* ___keysUp_16;
	// UnityEngine.KeyCode[] CameraFlyController::keysDown
	KeyCodeU5BU5D_t2223234056* ___keysDown_17;
	// UnityEngine.KeyCode[] CameraFlyController::keysRun
	KeyCodeU5BU5D_t2223234056* ___keysRun_18;
	// UnityEngine.KeyCode[] CameraFlyController::joystickUp
	KeyCodeU5BU5D_t2223234056* ___joystickUp_19;
	// UnityEngine.KeyCode[] CameraFlyController::joystickDown
	KeyCodeU5BU5D_t2223234056* ___joystickDown_20;

public:
	inline static int32_t get_offset_of_keysForward_12() { return static_cast<int32_t>(offsetof(CameraFlyController_t109453238_StaticFields, ___keysForward_12)); }
	inline KeyCodeU5BU5D_t2223234056* get_keysForward_12() const { return ___keysForward_12; }
	inline KeyCodeU5BU5D_t2223234056** get_address_of_keysForward_12() { return &___keysForward_12; }
	inline void set_keysForward_12(KeyCodeU5BU5D_t2223234056* value)
	{
		___keysForward_12 = value;
		Il2CppCodeGenWriteBarrier(&___keysForward_12, value);
	}

	inline static int32_t get_offset_of_keysBackward_13() { return static_cast<int32_t>(offsetof(CameraFlyController_t109453238_StaticFields, ___keysBackward_13)); }
	inline KeyCodeU5BU5D_t2223234056* get_keysBackward_13() const { return ___keysBackward_13; }
	inline KeyCodeU5BU5D_t2223234056** get_address_of_keysBackward_13() { return &___keysBackward_13; }
	inline void set_keysBackward_13(KeyCodeU5BU5D_t2223234056* value)
	{
		___keysBackward_13 = value;
		Il2CppCodeGenWriteBarrier(&___keysBackward_13, value);
	}

	inline static int32_t get_offset_of_keysLeft_14() { return static_cast<int32_t>(offsetof(CameraFlyController_t109453238_StaticFields, ___keysLeft_14)); }
	inline KeyCodeU5BU5D_t2223234056* get_keysLeft_14() const { return ___keysLeft_14; }
	inline KeyCodeU5BU5D_t2223234056** get_address_of_keysLeft_14() { return &___keysLeft_14; }
	inline void set_keysLeft_14(KeyCodeU5BU5D_t2223234056* value)
	{
		___keysLeft_14 = value;
		Il2CppCodeGenWriteBarrier(&___keysLeft_14, value);
	}

	inline static int32_t get_offset_of_keysRight_15() { return static_cast<int32_t>(offsetof(CameraFlyController_t109453238_StaticFields, ___keysRight_15)); }
	inline KeyCodeU5BU5D_t2223234056* get_keysRight_15() const { return ___keysRight_15; }
	inline KeyCodeU5BU5D_t2223234056** get_address_of_keysRight_15() { return &___keysRight_15; }
	inline void set_keysRight_15(KeyCodeU5BU5D_t2223234056* value)
	{
		___keysRight_15 = value;
		Il2CppCodeGenWriteBarrier(&___keysRight_15, value);
	}

	inline static int32_t get_offset_of_keysUp_16() { return static_cast<int32_t>(offsetof(CameraFlyController_t109453238_StaticFields, ___keysUp_16)); }
	inline KeyCodeU5BU5D_t2223234056* get_keysUp_16() const { return ___keysUp_16; }
	inline KeyCodeU5BU5D_t2223234056** get_address_of_keysUp_16() { return &___keysUp_16; }
	inline void set_keysUp_16(KeyCodeU5BU5D_t2223234056* value)
	{
		___keysUp_16 = value;
		Il2CppCodeGenWriteBarrier(&___keysUp_16, value);
	}

	inline static int32_t get_offset_of_keysDown_17() { return static_cast<int32_t>(offsetof(CameraFlyController_t109453238_StaticFields, ___keysDown_17)); }
	inline KeyCodeU5BU5D_t2223234056* get_keysDown_17() const { return ___keysDown_17; }
	inline KeyCodeU5BU5D_t2223234056** get_address_of_keysDown_17() { return &___keysDown_17; }
	inline void set_keysDown_17(KeyCodeU5BU5D_t2223234056* value)
	{
		___keysDown_17 = value;
		Il2CppCodeGenWriteBarrier(&___keysDown_17, value);
	}

	inline static int32_t get_offset_of_keysRun_18() { return static_cast<int32_t>(offsetof(CameraFlyController_t109453238_StaticFields, ___keysRun_18)); }
	inline KeyCodeU5BU5D_t2223234056* get_keysRun_18() const { return ___keysRun_18; }
	inline KeyCodeU5BU5D_t2223234056** get_address_of_keysRun_18() { return &___keysRun_18; }
	inline void set_keysRun_18(KeyCodeU5BU5D_t2223234056* value)
	{
		___keysRun_18 = value;
		Il2CppCodeGenWriteBarrier(&___keysRun_18, value);
	}

	inline static int32_t get_offset_of_joystickUp_19() { return static_cast<int32_t>(offsetof(CameraFlyController_t109453238_StaticFields, ___joystickUp_19)); }
	inline KeyCodeU5BU5D_t2223234056* get_joystickUp_19() const { return ___joystickUp_19; }
	inline KeyCodeU5BU5D_t2223234056** get_address_of_joystickUp_19() { return &___joystickUp_19; }
	inline void set_joystickUp_19(KeyCodeU5BU5D_t2223234056* value)
	{
		___joystickUp_19 = value;
		Il2CppCodeGenWriteBarrier(&___joystickUp_19, value);
	}

	inline static int32_t get_offset_of_joystickDown_20() { return static_cast<int32_t>(offsetof(CameraFlyController_t109453238_StaticFields, ___joystickDown_20)); }
	inline KeyCodeU5BU5D_t2223234056* get_joystickDown_20() const { return ___joystickDown_20; }
	inline KeyCodeU5BU5D_t2223234056** get_address_of_joystickDown_20() { return &___joystickDown_20; }
	inline void set_joystickDown_20(KeyCodeU5BU5D_t2223234056* value)
	{
		___joystickDown_20 = value;
		Il2CppCodeGenWriteBarrier(&___joystickDown_20, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
