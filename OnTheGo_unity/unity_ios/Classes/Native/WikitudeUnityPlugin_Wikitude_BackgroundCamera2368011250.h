﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour3962482529.h"

// Wikitude.WikitudeCamera
struct WikitudeCamera_t2188881370;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Wikitude.BackgroundCamera
struct  BackgroundCamera_t2368011250  : public MonoBehaviour_t3962482529
{
public:
	// Wikitude.WikitudeCamera Wikitude.BackgroundCamera::WikitudeCamera
	WikitudeCamera_t2188881370 * ___WikitudeCamera_2;

public:
	inline static int32_t get_offset_of_WikitudeCamera_2() { return static_cast<int32_t>(offsetof(BackgroundCamera_t2368011250, ___WikitudeCamera_2)); }
	inline WikitudeCamera_t2188881370 * get_WikitudeCamera_2() const { return ___WikitudeCamera_2; }
	inline WikitudeCamera_t2188881370 ** get_address_of_WikitudeCamera_2() { return &___WikitudeCamera_2; }
	inline void set_WikitudeCamera_2(WikitudeCamera_t2188881370 * value)
	{
		___WikitudeCamera_2 = value;
		Il2CppCodeGenWriteBarrier(&___WikitudeCamera_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
