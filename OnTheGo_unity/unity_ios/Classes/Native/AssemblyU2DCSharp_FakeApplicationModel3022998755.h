﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour3962482529.h"

// System.String
struct String_t;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FakeApplicationModel
struct  FakeApplicationModel_t3022998755  : public MonoBehaviour_t3962482529
{
public:
	// System.String FakeApplicationModel::url
	String_t* ___url_2;
	// System.String FakeApplicationModel::model
	String_t* ___model_3;

public:
	inline static int32_t get_offset_of_url_2() { return static_cast<int32_t>(offsetof(FakeApplicationModel_t3022998755, ___url_2)); }
	inline String_t* get_url_2() const { return ___url_2; }
	inline String_t** get_address_of_url_2() { return &___url_2; }
	inline void set_url_2(String_t* value)
	{
		___url_2 = value;
		Il2CppCodeGenWriteBarrier(&___url_2, value);
	}

	inline static int32_t get_offset_of_model_3() { return static_cast<int32_t>(offsetof(FakeApplicationModel_t3022998755, ___model_3)); }
	inline String_t* get_model_3() const { return ___model_3; }
	inline String_t** get_address_of_model_3() { return &___model_3; }
	inline void set_model_3(String_t* value)
	{
		___model_3 = value;
		Il2CppCodeGenWriteBarrier(&___model_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
