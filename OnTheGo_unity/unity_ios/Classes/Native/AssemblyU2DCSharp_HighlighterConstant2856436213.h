﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_HighlighterInteractive1072699259.h"
#include "UnityEngine_UnityEngine_Color2555686324.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HighlighterConstant
struct  HighlighterConstant_t2856436213  : public HighlighterInteractive_t1072699259
{
public:
	// UnityEngine.Color HighlighterConstant::color
	Color_t2555686324  ___color_4;

public:
	inline static int32_t get_offset_of_color_4() { return static_cast<int32_t>(offsetof(HighlighterConstant_t2856436213, ___color_4)); }
	inline Color_t2555686324  get_color_4() const { return ___color_4; }
	inline Color_t2555686324 * get_address_of_color_4() { return &___color_4; }
	inline void set_color_4(Color_t2555686324  value)
	{
		___color_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
