﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "WikitudeUnityPlugin_Wikitude_TrackerBehaviour921360922.h"

// Wikitude.TargetCollectionResource
struct TargetCollectionResource_t66041399;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Wikitude.ObjectTracker
struct  ObjectTracker_t3006704158  : public TrackerBehaviour_t921360922
{
public:
	// Wikitude.TargetCollectionResource Wikitude.ObjectTracker::_targetCollectionResource
	TargetCollectionResource_t66041399 * ____targetCollectionResource_15;
	// System.Boolean Wikitude.ObjectTracker::<IsRegistered>k__BackingField
	bool ___U3CIsRegisteredU3Ek__BackingField_16;

public:
	inline static int32_t get_offset_of__targetCollectionResource_15() { return static_cast<int32_t>(offsetof(ObjectTracker_t3006704158, ____targetCollectionResource_15)); }
	inline TargetCollectionResource_t66041399 * get__targetCollectionResource_15() const { return ____targetCollectionResource_15; }
	inline TargetCollectionResource_t66041399 ** get_address_of__targetCollectionResource_15() { return &____targetCollectionResource_15; }
	inline void set__targetCollectionResource_15(TargetCollectionResource_t66041399 * value)
	{
		____targetCollectionResource_15 = value;
		Il2CppCodeGenWriteBarrier(&____targetCollectionResource_15, value);
	}

	inline static int32_t get_offset_of_U3CIsRegisteredU3Ek__BackingField_16() { return static_cast<int32_t>(offsetof(ObjectTracker_t3006704158, ___U3CIsRegisteredU3Ek__BackingField_16)); }
	inline bool get_U3CIsRegisteredU3Ek__BackingField_16() const { return ___U3CIsRegisteredU3Ek__BackingField_16; }
	inline bool* get_address_of_U3CIsRegisteredU3Ek__BackingField_16() { return &___U3CIsRegisteredU3Ek__BackingField_16; }
	inline void set_U3CIsRegisteredU3Ek__BackingField_16(bool value)
	{
		___U3CIsRegisteredU3Ek__BackingField_16 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
