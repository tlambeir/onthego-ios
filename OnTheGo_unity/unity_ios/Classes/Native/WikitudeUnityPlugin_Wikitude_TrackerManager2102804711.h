﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour3962482529.h"

// System.Collections.Generic.Dictionary`2<System.Int64,Wikitude.TargetCollectionResource>
struct Dictionary_2_t1128681183;
// System.Collections.Generic.Dictionary`2<System.Int64,Wikitude.CloudRecognitionService>
struct Dictionary_2_t634242530;
// System.Collections.Generic.HashSet`1<Wikitude.RecognizedTarget>
struct HashSet_1_t2094861142;
// Wikitude.IPlatformBridge
struct IPlatformBridge_t1124789254;
// System.Collections.Generic.HashSet`1<Wikitude.TrackerBehaviour>
struct HashSet_1_t3781277692;
// Wikitude.TrackerBehaviour
struct TrackerBehaviour_t921360922;
// Wikitude.WikitudeCamera
struct WikitudeCamera_t2188881370;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Wikitude.TrackerManager
struct  TrackerManager_t2102804711  : public MonoBehaviour_t3962482529
{
public:
	// System.Int64 Wikitude.TrackerManager::_targetCollectionResourceId
	int64_t ____targetCollectionResourceId_2;
	// System.Int64 Wikitude.TrackerManager::_cloudRecognitionServiceId
	int64_t ____cloudRecognitionServiceId_3;
	// System.Collections.Generic.Dictionary`2<System.Int64,Wikitude.TargetCollectionResource> Wikitude.TrackerManager::_registeredResources
	Dictionary_2_t1128681183 * ____registeredResources_4;
	// System.Collections.Generic.Dictionary`2<System.Int64,Wikitude.CloudRecognitionService> Wikitude.TrackerManager::_registeredCloudRecognitionServices
	Dictionary_2_t634242530 * ____registeredCloudRecognitionServices_5;
	// System.Collections.Generic.HashSet`1<Wikitude.RecognizedTarget> Wikitude.TrackerManager::_recognizedTargets
	HashSet_1_t2094861142 * ____recognizedTargets_6;
	// Wikitude.IPlatformBridge Wikitude.TrackerManager::_bridge
	Il2CppObject * ____bridge_7;
	// System.Collections.Generic.HashSet`1<Wikitude.TrackerBehaviour> Wikitude.TrackerManager::_registeredTrackers
	HashSet_1_t3781277692 * ____registeredTrackers_8;
	// Wikitude.TrackerBehaviour Wikitude.TrackerManager::_activeTracker
	TrackerBehaviour_t921360922 * ____activeTracker_9;
	// Wikitude.WikitudeCamera Wikitude.TrackerManager::_wikitudeCamera
	WikitudeCamera_t2188881370 * ____wikitudeCamera_10;

public:
	inline static int32_t get_offset_of__targetCollectionResourceId_2() { return static_cast<int32_t>(offsetof(TrackerManager_t2102804711, ____targetCollectionResourceId_2)); }
	inline int64_t get__targetCollectionResourceId_2() const { return ____targetCollectionResourceId_2; }
	inline int64_t* get_address_of__targetCollectionResourceId_2() { return &____targetCollectionResourceId_2; }
	inline void set__targetCollectionResourceId_2(int64_t value)
	{
		____targetCollectionResourceId_2 = value;
	}

	inline static int32_t get_offset_of__cloudRecognitionServiceId_3() { return static_cast<int32_t>(offsetof(TrackerManager_t2102804711, ____cloudRecognitionServiceId_3)); }
	inline int64_t get__cloudRecognitionServiceId_3() const { return ____cloudRecognitionServiceId_3; }
	inline int64_t* get_address_of__cloudRecognitionServiceId_3() { return &____cloudRecognitionServiceId_3; }
	inline void set__cloudRecognitionServiceId_3(int64_t value)
	{
		____cloudRecognitionServiceId_3 = value;
	}

	inline static int32_t get_offset_of__registeredResources_4() { return static_cast<int32_t>(offsetof(TrackerManager_t2102804711, ____registeredResources_4)); }
	inline Dictionary_2_t1128681183 * get__registeredResources_4() const { return ____registeredResources_4; }
	inline Dictionary_2_t1128681183 ** get_address_of__registeredResources_4() { return &____registeredResources_4; }
	inline void set__registeredResources_4(Dictionary_2_t1128681183 * value)
	{
		____registeredResources_4 = value;
		Il2CppCodeGenWriteBarrier(&____registeredResources_4, value);
	}

	inline static int32_t get_offset_of__registeredCloudRecognitionServices_5() { return static_cast<int32_t>(offsetof(TrackerManager_t2102804711, ____registeredCloudRecognitionServices_5)); }
	inline Dictionary_2_t634242530 * get__registeredCloudRecognitionServices_5() const { return ____registeredCloudRecognitionServices_5; }
	inline Dictionary_2_t634242530 ** get_address_of__registeredCloudRecognitionServices_5() { return &____registeredCloudRecognitionServices_5; }
	inline void set__registeredCloudRecognitionServices_5(Dictionary_2_t634242530 * value)
	{
		____registeredCloudRecognitionServices_5 = value;
		Il2CppCodeGenWriteBarrier(&____registeredCloudRecognitionServices_5, value);
	}

	inline static int32_t get_offset_of__recognizedTargets_6() { return static_cast<int32_t>(offsetof(TrackerManager_t2102804711, ____recognizedTargets_6)); }
	inline HashSet_1_t2094861142 * get__recognizedTargets_6() const { return ____recognizedTargets_6; }
	inline HashSet_1_t2094861142 ** get_address_of__recognizedTargets_6() { return &____recognizedTargets_6; }
	inline void set__recognizedTargets_6(HashSet_1_t2094861142 * value)
	{
		____recognizedTargets_6 = value;
		Il2CppCodeGenWriteBarrier(&____recognizedTargets_6, value);
	}

	inline static int32_t get_offset_of__bridge_7() { return static_cast<int32_t>(offsetof(TrackerManager_t2102804711, ____bridge_7)); }
	inline Il2CppObject * get__bridge_7() const { return ____bridge_7; }
	inline Il2CppObject ** get_address_of__bridge_7() { return &____bridge_7; }
	inline void set__bridge_7(Il2CppObject * value)
	{
		____bridge_7 = value;
		Il2CppCodeGenWriteBarrier(&____bridge_7, value);
	}

	inline static int32_t get_offset_of__registeredTrackers_8() { return static_cast<int32_t>(offsetof(TrackerManager_t2102804711, ____registeredTrackers_8)); }
	inline HashSet_1_t3781277692 * get__registeredTrackers_8() const { return ____registeredTrackers_8; }
	inline HashSet_1_t3781277692 ** get_address_of__registeredTrackers_8() { return &____registeredTrackers_8; }
	inline void set__registeredTrackers_8(HashSet_1_t3781277692 * value)
	{
		____registeredTrackers_8 = value;
		Il2CppCodeGenWriteBarrier(&____registeredTrackers_8, value);
	}

	inline static int32_t get_offset_of__activeTracker_9() { return static_cast<int32_t>(offsetof(TrackerManager_t2102804711, ____activeTracker_9)); }
	inline TrackerBehaviour_t921360922 * get__activeTracker_9() const { return ____activeTracker_9; }
	inline TrackerBehaviour_t921360922 ** get_address_of__activeTracker_9() { return &____activeTracker_9; }
	inline void set__activeTracker_9(TrackerBehaviour_t921360922 * value)
	{
		____activeTracker_9 = value;
		Il2CppCodeGenWriteBarrier(&____activeTracker_9, value);
	}

	inline static int32_t get_offset_of__wikitudeCamera_10() { return static_cast<int32_t>(offsetof(TrackerManager_t2102804711, ____wikitudeCamera_10)); }
	inline WikitudeCamera_t2188881370 * get__wikitudeCamera_10() const { return ____wikitudeCamera_10; }
	inline WikitudeCamera_t2188881370 ** get_address_of__wikitudeCamera_10() { return &____wikitudeCamera_10; }
	inline void set__wikitudeCamera_10(WikitudeCamera_t2188881370 * value)
	{
		____wikitudeCamera_10 = value;
		Il2CppCodeGenWriteBarrier(&____wikitudeCamera_10, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
