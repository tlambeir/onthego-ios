﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array3549286319.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21383673463.h"
#include "mscorlib_System_Void1185182177.h"
#include "mscorlib_System_Int322950945753.h"
#include "mscorlib_System_Boolean97287965.h"
#include "mscorlib_System_String1847450689.h"
#include "mscorlib_System_Object3080106164.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_625878672.h"
#include "mscorlib_System_Char3634460470.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_24237331251.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_727985506.h"
#include "mscorlib_System_Int643736567304.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_g71524366.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22245450819.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23842366416.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22401056908.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22530217319.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_847377929.h"
#include "mscorlib_System_Single1397266774.h"
#include "System_System_Collections_Generic_LinkedList_1_Enum139379724.h"
#include "System_System_Collections_Generic_LinkedList_1_gen1919752173.h"
#include "System_System_Collections_Generic_LinkedListNode_12825281267.h"
#include "mscorlib_System_UInt322560061978.h"
#include "mscorlib_System_ObjectDisposedException21392786.h"
#include "mscorlib_System_InvalidOperationException56020091.h"
#include "mscorlib_System_Runtime_Serialization_Serialization950877179.h"
#include "mscorlib_System_Runtime_Serialization_StreamingCon3711869237.h"
#include "mscorlib_System_ArgumentException132251570.h"
#include "mscorlib_System_ArgumentNullException1615371798.h"
#include "mscorlib_System_ArgumentOutOfRangeException777629997.h"
#include "mscorlib_System_RuntimeTypeHandle3027515415.h"
#include "mscorlib_System_Type2483944760.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1640318223.h"
#include "mscorlib_System_Collections_Generic_List_1_gen4046041642.h"
#include "AssemblyU2DCSharp_CustomCameraController_InputFram2573966900.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera3796191691.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1906947814.h"
#include "AssemblyU2DCSharpU2Dfirstpass_HighlightingSystem_Hi434873072.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera3996938410.h"
#include "mscorlib_System_Collections_Generic_List_1_gen2107694533.h"
#include "AssemblyU2DCSharpU2Dfirstpass_HighlightingSystem_Hi635619791.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera2700811793.h"
#include "mscorlib_System_Collections_Generic_List_1_gen811567916.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera2017297076.h"
#include "mscorlib_System_Collections_Generic_List_1_gen128053199.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera2146457487.h"
#include "mscorlib_System_Collections_Generic_List_1_gen257213610.h"

// System.String
struct String_t;
// System.String[]
struct StringU5BU5D_t1281789340;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.LinkedList`1<System.Object>
struct LinkedList_1_t1919752173;
// System.ObjectDisposedException
struct ObjectDisposedException_t21392786;
// System.InvalidOperationException
struct InvalidOperationException_t56020091;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t950877179;
// System.Array
struct Il2CppArray;
// System.ArgumentException
struct ArgumentException_t132251570;
// System.Collections.Generic.IEnumerator`1<System.Object>
struct IEnumerator_1_t3512676632;
// System.Collections.IEnumerator
struct IEnumerator_t1853284238;
// System.Collections.Generic.LinkedListNode`1<System.Object>
struct LinkedListNode_1_t2825281267;
// System.ArgumentNullException
struct ArgumentNullException_t1615371798;
// System.Object[]
struct ObjectU5BU5D_t2843939325;
// System.ArgumentOutOfRangeException
struct ArgumentOutOfRangeException_t777629997;
// System.Type
struct Type_t;
// System.Collections.Generic.List`1<CustomCameraController/InputFrameData>
struct List_1_t4046041642;
// System.Collections.Generic.List`1<HighlightingSystem.HighlighterRenderer/Data>
struct List_1_t1906947814;
// System.Collections.Generic.List`1<HighlightingSystem.HighlightingPreset>
struct List_1_t2107694533;
// System.Collections.Generic.List`1<System.Char>
struct List_1_t811567916;
// System.Collections.Generic.List`1<System.Int32>
struct List_1_t128053199;
// System.Collections.Generic.List`1<System.Object>
struct List_1_t257213610;
extern Il2CppClass* StringU5BU5D_t1281789340_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3452614645;
extern Il2CppCodeGenString* _stringLiteral3450517380;
extern Il2CppCodeGenString* _stringLiteral3452614643;
extern const uint32_t KeyValuePair_2_ToString_m2644562007_MetadataUsageId;
extern const uint32_t KeyValuePair_2_ToString_m823870646_MetadataUsageId;
extern const uint32_t KeyValuePair_2_ToString_m65692449_MetadataUsageId;
extern const uint32_t KeyValuePair_2_ToString_m4221841369_MetadataUsageId;
extern const uint32_t KeyValuePair_2_ToString_m1238786018_MetadataUsageId;
extern const uint32_t KeyValuePair_2_ToString_m3013572783_MetadataUsageId;
extern const uint32_t KeyValuePair_2_ToString_m2480962023_MetadataUsageId;
extern const uint32_t KeyValuePair_2_ToString_m4231614106_MetadataUsageId;
extern const uint32_t KeyValuePair_2_ToString_m510648957_MetadataUsageId;
extern const uint32_t KeyValuePair_2_ToString_m366204361_MetadataUsageId;
extern Il2CppClass* ObjectDisposedException_t21392786_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidOperationException_t56020091_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3902161842;
extern const uint32_t Enumerator_System_Collections_IEnumerator_Reset_m2550940120_MetadataUsageId;
extern const uint32_t Enumerator_get_Current_m2579293505_MetadataUsageId;
extern const uint32_t Enumerator_MoveNext_m880141662_MetadataUsageId;
extern const uint32_t Enumerator_Dispose_m2375640141_MetadataUsageId;
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern const uint32_t LinkedList_1__ctor_m3670635350_MetadataUsageId;
extern const uint32_t LinkedList_1__ctor_m2390670044_MetadataUsageId;
extern Il2CppClass* ArgumentException_t132251570_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4007973390;
extern const uint32_t LinkedList_1_System_Collections_ICollection_CopyTo_m816767247_MetadataUsageId;
extern Il2CppClass* ArgumentNullException_t1615371798_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1985170616;
extern const uint32_t LinkedList_1_VerifyReferencedNode_m3460801984_MetadataUsageId;
extern Il2CppClass* ArgumentOutOfRangeException_t777629997_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral797640427;
extern Il2CppCodeGenString* _stringLiteral587261375;
extern Il2CppCodeGenString* _stringLiteral163435716;
extern const uint32_t LinkedList_1_CopyTo_m2255490129_MetadataUsageId;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3586096867;
extern Il2CppCodeGenString* _stringLiteral1902401671;
extern const uint32_t LinkedList_1_GetObjectData_m1792615137_MetadataUsageId;
extern const uint32_t LinkedList_1_OnDeserialization_m3201462664_MetadataUsageId;
extern const uint32_t Enumerator_System_Collections_IEnumerator_get_Current_m2348568638_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral1621028992;
extern const uint32_t Enumerator_VerifyState_m2137990666_MetadataUsageId;
extern const uint32_t Enumerator_System_Collections_IEnumerator_get_Current_m2892844362_MetadataUsageId;
extern const uint32_t Enumerator_VerifyState_m1268345934_MetadataUsageId;
extern const uint32_t Enumerator_System_Collections_IEnumerator_get_Current_m3745379029_MetadataUsageId;
extern const uint32_t Enumerator_VerifyState_m2056140850_MetadataUsageId;
extern const uint32_t Enumerator_System_Collections_IEnumerator_get_Current_m1846615603_MetadataUsageId;
extern const uint32_t Enumerator_VerifyState_m1375238439_MetadataUsageId;
extern const uint32_t Enumerator_System_Collections_IEnumerator_get_Current_m323862414_MetadataUsageId;
extern const uint32_t Enumerator_VerifyState_m1898450050_MetadataUsageId;
extern const uint32_t Enumerator_System_Collections_IEnumerator_get_Current_m3681948262_MetadataUsageId;
extern const uint32_t Enumerator_VerifyState_m2933667029_MetadataUsageId;

// System.String[]
struct StringU5BU5D_t1281789340  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) String_t* m_Items[1];

public:
	inline String_t* GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline String_t** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, String_t* value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline String_t* GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline String_t** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, String_t* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Object[]
struct ObjectU5BU5D_t2843939325  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Il2CppObject * m_Items[1];

public:
	inline Il2CppObject * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Il2CppObject ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Il2CppObject * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline Il2CppObject * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Il2CppObject ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Il2CppObject * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// CustomCameraController/InputFrameData[]
struct InputFrameDataU5BU5D_t2280996477  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) InputFrameData_t2573966900  m_Items[1];

public:
	inline InputFrameData_t2573966900  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline InputFrameData_t2573966900 * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, InputFrameData_t2573966900  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline InputFrameData_t2573966900  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline InputFrameData_t2573966900 * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, InputFrameData_t2573966900  value)
	{
		m_Items[index] = value;
	}
};
// HighlightingSystem.HighlighterRenderer/Data[]
struct DataU5BU5D_t2819638097  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Data_t434873072  m_Items[1];

public:
	inline Data_t434873072  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Data_t434873072 * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Data_t434873072  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline Data_t434873072  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Data_t434873072 * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Data_t434873072  value)
	{
		m_Items[index] = value;
	}
};
// HighlightingSystem.HighlightingPreset[]
struct HighlightingPresetU5BU5D_t2765988374  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) HighlightingPreset_t635619791  m_Items[1];

public:
	inline HighlightingPreset_t635619791  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline HighlightingPreset_t635619791 * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, HighlightingPreset_t635619791  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline HighlightingPreset_t635619791  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline HighlightingPreset_t635619791 * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, HighlightingPreset_t635619791  value)
	{
		m_Items[index] = value;
	}
};
// System.Char[]
struct CharU5BU5D_t3528271667  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Il2CppChar m_Items[1];

public:
	inline Il2CppChar GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Il2CppChar* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Il2CppChar value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline Il2CppChar GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Il2CppChar* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Il2CppChar value)
	{
		m_Items[index] = value;
	}
};
// System.Int32[]
struct Int32U5BU5D_t385246372  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) int32_t m_Items[1];

public:
	inline int32_t GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline int32_t* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, int32_t value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline int32_t GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline int32_t* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, int32_t value)
	{
		m_Items[index] = value;
	}
};


// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,System.Boolean>::set_Key(TKey)
extern "C"  void KeyValuePair_2_set_Key_m2689609346_gshared (KeyValuePair_2_t1383673463 * __this, int32_t p0, const MethodInfo* method);
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,System.Boolean>::set_Value(TValue)
extern "C"  void KeyValuePair_2_set_Value_m1705789134_gshared (KeyValuePair_2_t1383673463 * __this, bool p0, const MethodInfo* method);
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,System.Boolean>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m3344522653_gshared (KeyValuePair_2_t1383673463 * __this, int32_t ___key0, bool ___value1, const MethodInfo* method);
// TKey System.Collections.Generic.KeyValuePair`2<System.Int32,System.Boolean>::get_Key()
extern "C"  int32_t KeyValuePair_2_get_Key_m2536726212_gshared (KeyValuePair_2_t1383673463 * __this, const MethodInfo* method);
// TValue System.Collections.Generic.KeyValuePair`2<System.Int32,System.Boolean>::get_Value()
extern "C"  bool KeyValuePair_2_get_Value_m3276326716_gshared (KeyValuePair_2_t1383673463 * __this, const MethodInfo* method);
// System.String System.Collections.Generic.KeyValuePair`2<System.Int32,System.Boolean>::ToString()
extern "C"  String_t* KeyValuePair_2_ToString_m2644562007_gshared (KeyValuePair_2_t1383673463 * __this, const MethodInfo* method);
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,System.Char>::set_Key(TKey)
extern "C"  void KeyValuePair_2_set_Key_m3265358347_gshared (KeyValuePair_2_t625878672 * __this, int32_t p0, const MethodInfo* method);
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,System.Char>::set_Value(TValue)
extern "C"  void KeyValuePair_2_set_Value_m3679535380_gshared (KeyValuePair_2_t625878672 * __this, Il2CppChar p0, const MethodInfo* method);
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,System.Char>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m1972131968_gshared (KeyValuePair_2_t625878672 * __this, int32_t ___key0, Il2CppChar ___value1, const MethodInfo* method);
// TKey System.Collections.Generic.KeyValuePair`2<System.Int32,System.Char>::get_Key()
extern "C"  int32_t KeyValuePair_2_get_Key_m2701016533_gshared (KeyValuePair_2_t625878672 * __this, const MethodInfo* method);
// TValue System.Collections.Generic.KeyValuePair`2<System.Int32,System.Char>::get_Value()
extern "C"  Il2CppChar KeyValuePair_2_get_Value_m4183788833_gshared (KeyValuePair_2_t625878672 * __this, const MethodInfo* method);
// System.String System.Collections.Generic.KeyValuePair`2<System.Int32,System.Char>::ToString()
extern "C"  String_t* KeyValuePair_2_ToString_m823870646_gshared (KeyValuePair_2_t625878672 * __this, const MethodInfo* method);
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>::set_Key(TKey)
extern "C"  void KeyValuePair_2_set_Key_m1660334707_gshared (KeyValuePair_2_t4237331251 * __this, int32_t p0, const MethodInfo* method);
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>::set_Value(TValue)
extern "C"  void KeyValuePair_2_set_Value_m2314909969_gshared (KeyValuePair_2_t4237331251 * __this, int32_t p0, const MethodInfo* method);
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m2493299567_gshared (KeyValuePair_2_t4237331251 * __this, int32_t ___key0, int32_t ___value1, const MethodInfo* method);
// TKey System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>::get_Key()
extern "C"  int32_t KeyValuePair_2_get_Key_m2288232422_gshared (KeyValuePair_2_t4237331251 * __this, const MethodInfo* method);
// TValue System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>::get_Value()
extern "C"  int32_t KeyValuePair_2_get_Value_m3536231958_gshared (KeyValuePair_2_t4237331251 * __this, const MethodInfo* method);
// System.String System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>::ToString()
extern "C"  String_t* KeyValuePair_2_ToString_m65692449_gshared (KeyValuePair_2_t4237331251 * __this, const MethodInfo* method);
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int64>::set_Key(TKey)
extern "C"  void KeyValuePair_2_set_Key_m975493760_gshared (KeyValuePair_2_t727985506 * __this, int32_t p0, const MethodInfo* method);
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int64>::set_Value(TValue)
extern "C"  void KeyValuePair_2_set_Value_m596466564_gshared (KeyValuePair_2_t727985506 * __this, int64_t p0, const MethodInfo* method);
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int64>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m844205450_gshared (KeyValuePair_2_t727985506 * __this, int32_t ___key0, int64_t ___value1, const MethodInfo* method);
// TKey System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int64>::get_Key()
extern "C"  int32_t KeyValuePair_2_get_Key_m1737874887_gshared (KeyValuePair_2_t727985506 * __this, const MethodInfo* method);
// TValue System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int64>::get_Value()
extern "C"  int64_t KeyValuePair_2_get_Value_m3934599775_gshared (KeyValuePair_2_t727985506 * __this, const MethodInfo* method);
// System.String System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int64>::ToString()
extern "C"  String_t* KeyValuePair_2_ToString_m4221841369_gshared (KeyValuePair_2_t727985506 * __this, const MethodInfo* method);
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>::set_Key(TKey)
extern "C"  void KeyValuePair_2_set_Key_m2121548577_gshared (KeyValuePair_2_t71524366 * __this, int32_t p0, const MethodInfo* method);
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>::set_Value(TValue)
extern "C"  void KeyValuePair_2_set_Value_m3358607572_gshared (KeyValuePair_2_t71524366 * __this, Il2CppObject * p0, const MethodInfo* method);
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m2118224448_gshared (KeyValuePair_2_t71524366 * __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
// TKey System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>::get_Key()
extern "C"  int32_t KeyValuePair_2_get_Key_m1839753989_gshared (KeyValuePair_2_t71524366 * __this, const MethodInfo* method);
// TValue System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>::get_Value()
extern "C"  Il2CppObject * KeyValuePair_2_get_Value_m3495598764_gshared (KeyValuePair_2_t71524366 * __this, const MethodInfo* method);
// System.String System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>::ToString()
extern "C"  String_t* KeyValuePair_2_ToString_m1238786018_gshared (KeyValuePair_2_t71524366 * __this, const MethodInfo* method);
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int64,System.Object>::set_Key(TKey)
extern "C"  void KeyValuePair_2_set_Key_m1382769311_gshared (KeyValuePair_2_t2245450819 * __this, int64_t p0, const MethodInfo* method);
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int64,System.Object>::set_Value(TValue)
extern "C"  void KeyValuePair_2_set_Value_m1316464643_gshared (KeyValuePair_2_t2245450819 * __this, Il2CppObject * p0, const MethodInfo* method);
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int64,System.Object>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m2056996813_gshared (KeyValuePair_2_t2245450819 * __this, int64_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
// TKey System.Collections.Generic.KeyValuePair`2<System.Int64,System.Object>::get_Key()
extern "C"  int64_t KeyValuePair_2_get_Key_m635992374_gshared (KeyValuePair_2_t2245450819 * __this, const MethodInfo* method);
// TValue System.Collections.Generic.KeyValuePair`2<System.Int64,System.Object>::get_Value()
extern "C"  Il2CppObject * KeyValuePair_2_get_Value_m392474074_gshared (KeyValuePair_2_t2245450819 * __this, const MethodInfo* method);
// System.String System.Collections.Generic.KeyValuePair`2<System.Int64,System.Object>::ToString()
extern "C"  String_t* KeyValuePair_2_ToString_m3013572783_gshared (KeyValuePair_2_t2245450819 * __this, const MethodInfo* method);
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>::set_Key(TKey)
extern "C"  void KeyValuePair_2_set_Key_m2116817417_gshared (KeyValuePair_2_t3842366416 * __this, Il2CppObject * p0, const MethodInfo* method);
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>::set_Value(TValue)
extern "C"  void KeyValuePair_2_set_Value_m3305319569_gshared (KeyValuePair_2_t3842366416 * __this, bool p0, const MethodInfo* method);
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m23191374_gshared (KeyValuePair_2_t3842366416 * __this, Il2CppObject * ___key0, bool ___value1, const MethodInfo* method);
// TKey System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>::get_Key()
extern "C"  Il2CppObject * KeyValuePair_2_get_Key_m2106922848_gshared (KeyValuePair_2_t3842366416 * __this, const MethodInfo* method);
// TValue System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>::get_Value()
extern "C"  bool KeyValuePair_2_get_Value_m1669764045_gshared (KeyValuePair_2_t3842366416 * __this, const MethodInfo* method);
// System.String System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>::ToString()
extern "C"  String_t* KeyValuePair_2_ToString_m2480962023_gshared (KeyValuePair_2_t3842366416 * __this, const MethodInfo* method);
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>::set_Key(TKey)
extern "C"  void KeyValuePair_2_set_Key_m4256290317_gshared (KeyValuePair_2_t2401056908 * __this, Il2CppObject * p0, const MethodInfo* method);
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>::set_Value(TValue)
extern "C"  void KeyValuePair_2_set_Value_m460969740_gshared (KeyValuePair_2_t2401056908 * __this, int32_t p0, const MethodInfo* method);
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m880186442_gshared (KeyValuePair_2_t2401056908 * __this, Il2CppObject * ___key0, int32_t ___value1, const MethodInfo* method);
// TKey System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>::get_Key()
extern "C"  Il2CppObject * KeyValuePair_2_get_Key_m1218836954_gshared (KeyValuePair_2_t2401056908 * __this, const MethodInfo* method);
// TValue System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>::get_Value()
extern "C"  int32_t KeyValuePair_2_get_Value_m755756747_gshared (KeyValuePair_2_t2401056908 * __this, const MethodInfo* method);
// System.String System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>::ToString()
extern "C"  String_t* KeyValuePair_2_ToString_m4231614106_gshared (KeyValuePair_2_t2401056908 * __this, const MethodInfo* method);
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>::set_Key(TKey)
extern "C"  void KeyValuePair_2_set_Key_m3170517671_gshared (KeyValuePair_2_t2530217319 * __this, Il2CppObject * p0, const MethodInfo* method);
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>::set_Value(TValue)
extern "C"  void KeyValuePair_2_set_Value_m1153752644_gshared (KeyValuePair_2_t2530217319 * __this, Il2CppObject * p0, const MethodInfo* method);
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m1794021352_gshared (KeyValuePair_2_t2530217319 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method);
// TKey System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>::get_Key()
extern "C"  Il2CppObject * KeyValuePair_2_get_Key_m1328507389_gshared (KeyValuePair_2_t2530217319 * __this, const MethodInfo* method);
// TValue System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>::get_Value()
extern "C"  Il2CppObject * KeyValuePair_2_get_Value_m3464904234_gshared (KeyValuePair_2_t2530217319 * __this, const MethodInfo* method);
// System.String System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>::ToString()
extern "C"  String_t* KeyValuePair_2_ToString_m510648957_gshared (KeyValuePair_2_t2530217319 * __this, const MethodInfo* method);
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Single>::set_Key(TKey)
extern "C"  void KeyValuePair_2_set_Key_m2965168904_gshared (KeyValuePair_2_t847377929 * __this, Il2CppObject * p0, const MethodInfo* method);
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Single>::set_Value(TValue)
extern "C"  void KeyValuePair_2_set_Value_m955705697_gshared (KeyValuePair_2_t847377929 * __this, float p0, const MethodInfo* method);
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Single>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m3552061076_gshared (KeyValuePair_2_t847377929 * __this, Il2CppObject * ___key0, float ___value1, const MethodInfo* method);
// TKey System.Collections.Generic.KeyValuePair`2<System.Object,System.Single>::get_Key()
extern "C"  Il2CppObject * KeyValuePair_2_get_Key_m3754157295_gshared (KeyValuePair_2_t847377929 * __this, const MethodInfo* method);
// TValue System.Collections.Generic.KeyValuePair`2<System.Object,System.Single>::get_Value()
extern "C"  float KeyValuePair_2_get_Value_m1635977167_gshared (KeyValuePair_2_t847377929 * __this, const MethodInfo* method);
// System.String System.Collections.Generic.KeyValuePair`2<System.Object,System.Single>::ToString()
extern "C"  String_t* KeyValuePair_2_ToString_m366204361_gshared (KeyValuePair_2_t847377929 * __this, const MethodInfo* method);
// System.Void System.Collections.Generic.LinkedList`1/Enumerator<System.Object>::.ctor(System.Collections.Generic.LinkedList`1<T>)
extern "C"  void Enumerator__ctor_m2504641326_gshared (Enumerator_t139379724 * __this, LinkedList_1_t1919752173 * ___parent0, const MethodInfo* method);
// T System.Collections.Generic.LinkedList`1/Enumerator<System.Object>::get_Current()
extern "C"  Il2CppObject * Enumerator_get_Current_m2579293505_gshared (Enumerator_t139379724 * __this, const MethodInfo* method);
// System.Object System.Collections.Generic.LinkedList`1/Enumerator<System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m3484854280_gshared (Enumerator_t139379724 * __this, const MethodInfo* method);
// System.Void System.Collections.Generic.LinkedList`1/Enumerator<System.Object>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m2550940120_gshared (Enumerator_t139379724 * __this, const MethodInfo* method);
// System.Boolean System.Collections.Generic.LinkedList`1/Enumerator<System.Object>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m880141662_gshared (Enumerator_t139379724 * __this, const MethodInfo* method);
// System.Void System.Collections.Generic.LinkedList`1/Enumerator<System.Object>::Dispose()
extern "C"  void Enumerator_Dispose_m2375640141_gshared (Enumerator_t139379724 * __this, const MethodInfo* method);
// System.Void System.Collections.Generic.List`1/Enumerator<CustomCameraController/InputFrameData>::.ctor(System.Collections.Generic.List`1<T>)
extern "C"  void Enumerator__ctor_m69053308_gshared (Enumerator_t1640318223 * __this, List_1_t4046041642 * ___l0, const MethodInfo* method);
// System.Void System.Collections.Generic.List`1/Enumerator<CustomCameraController/InputFrameData>::VerifyState()
extern "C"  void Enumerator_VerifyState_m2137990666_gshared (Enumerator_t1640318223 * __this, const MethodInfo* method);
// System.Void System.Collections.Generic.List`1/Enumerator<CustomCameraController/InputFrameData>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m3420610639_gshared (Enumerator_t1640318223 * __this, const MethodInfo* method);
// System.Object System.Collections.Generic.List`1/Enumerator<CustomCameraController/InputFrameData>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m2348568638_gshared (Enumerator_t1640318223 * __this, const MethodInfo* method);
// System.Void System.Collections.Generic.List`1/Enumerator<CustomCameraController/InputFrameData>::Dispose()
extern "C"  void Enumerator_Dispose_m2308405500_gshared (Enumerator_t1640318223 * __this, const MethodInfo* method);
// System.Boolean System.Collections.Generic.List`1/Enumerator<CustomCameraController/InputFrameData>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m1995532294_gshared (Enumerator_t1640318223 * __this, const MethodInfo* method);
// T System.Collections.Generic.List`1/Enumerator<CustomCameraController/InputFrameData>::get_Current()
extern "C"  InputFrameData_t2573966900  Enumerator_get_Current_m1721394586_gshared (Enumerator_t1640318223 * __this, const MethodInfo* method);
// System.Void System.Collections.Generic.List`1/Enumerator<HighlightingSystem.HighlighterRenderer/Data>::.ctor(System.Collections.Generic.List`1<T>)
extern "C"  void Enumerator__ctor_m583292949_gshared (Enumerator_t3796191691 * __this, List_1_t1906947814 * ___l0, const MethodInfo* method);
// System.Void System.Collections.Generic.List`1/Enumerator<HighlightingSystem.HighlighterRenderer/Data>::VerifyState()
extern "C"  void Enumerator_VerifyState_m1268345934_gshared (Enumerator_t3796191691 * __this, const MethodInfo* method);
// System.Void System.Collections.Generic.List`1/Enumerator<HighlightingSystem.HighlighterRenderer/Data>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m2985421904_gshared (Enumerator_t3796191691 * __this, const MethodInfo* method);
// System.Object System.Collections.Generic.List`1/Enumerator<HighlightingSystem.HighlighterRenderer/Data>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m2892844362_gshared (Enumerator_t3796191691 * __this, const MethodInfo* method);
// System.Void System.Collections.Generic.List`1/Enumerator<HighlightingSystem.HighlighterRenderer/Data>::Dispose()
extern "C"  void Enumerator_Dispose_m1071958056_gshared (Enumerator_t3796191691 * __this, const MethodInfo* method);
// System.Boolean System.Collections.Generic.List`1/Enumerator<HighlightingSystem.HighlighterRenderer/Data>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m4173244301_gshared (Enumerator_t3796191691 * __this, const MethodInfo* method);
// T System.Collections.Generic.List`1/Enumerator<HighlightingSystem.HighlighterRenderer/Data>::get_Current()
extern "C"  Data_t434873072  Enumerator_get_Current_m935646724_gshared (Enumerator_t3796191691 * __this, const MethodInfo* method);
// System.Void System.Collections.Generic.List`1/Enumerator<HighlightingSystem.HighlightingPreset>::.ctor(System.Collections.Generic.List`1<T>)
extern "C"  void Enumerator__ctor_m3930688850_gshared (Enumerator_t3996938410 * __this, List_1_t2107694533 * ___l0, const MethodInfo* method);
// System.Void System.Collections.Generic.List`1/Enumerator<HighlightingSystem.HighlightingPreset>::VerifyState()
extern "C"  void Enumerator_VerifyState_m2056140850_gshared (Enumerator_t3996938410 * __this, const MethodInfo* method);
// System.Void System.Collections.Generic.List`1/Enumerator<HighlightingSystem.HighlightingPreset>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m3021194315_gshared (Enumerator_t3996938410 * __this, const MethodInfo* method);
// System.Object System.Collections.Generic.List`1/Enumerator<HighlightingSystem.HighlightingPreset>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m3745379029_gshared (Enumerator_t3996938410 * __this, const MethodInfo* method);
// System.Void System.Collections.Generic.List`1/Enumerator<HighlightingSystem.HighlightingPreset>::Dispose()
extern "C"  void Enumerator_Dispose_m3559089685_gshared (Enumerator_t3996938410 * __this, const MethodInfo* method);
// System.Boolean System.Collections.Generic.List`1/Enumerator<HighlightingSystem.HighlightingPreset>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m2406304866_gshared (Enumerator_t3996938410 * __this, const MethodInfo* method);
// T System.Collections.Generic.List`1/Enumerator<HighlightingSystem.HighlightingPreset>::get_Current()
extern "C"  HighlightingPreset_t635619791  Enumerator_get_Current_m1356247204_gshared (Enumerator_t3996938410 * __this, const MethodInfo* method);
// System.Void System.Collections.Generic.List`1/Enumerator<System.Char>::.ctor(System.Collections.Generic.List`1<T>)
extern "C"  void Enumerator__ctor_m2263237050_gshared (Enumerator_t2700811793 * __this, List_1_t811567916 * ___l0, const MethodInfo* method);
// System.Void System.Collections.Generic.List`1/Enumerator<System.Char>::VerifyState()
extern "C"  void Enumerator_VerifyState_m1375238439_gshared (Enumerator_t2700811793 * __this, const MethodInfo* method);
// System.Void System.Collections.Generic.List`1/Enumerator<System.Char>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m2394182378_gshared (Enumerator_t2700811793 * __this, const MethodInfo* method);
// System.Object System.Collections.Generic.List`1/Enumerator<System.Char>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m1846615603_gshared (Enumerator_t2700811793 * __this, const MethodInfo* method);
// System.Void System.Collections.Generic.List`1/Enumerator<System.Char>::Dispose()
extern "C"  void Enumerator_Dispose_m3961787728_gshared (Enumerator_t2700811793 * __this, const MethodInfo* method);
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.Char>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m1940664149_gshared (Enumerator_t2700811793 * __this, const MethodInfo* method);
// T System.Collections.Generic.List`1/Enumerator<System.Char>::get_Current()
extern "C"  Il2CppChar Enumerator_get_Current_m1436440636_gshared (Enumerator_t2700811793 * __this, const MethodInfo* method);
// System.Void System.Collections.Generic.List`1/Enumerator<System.Int32>::.ctor(System.Collections.Generic.List`1<T>)
extern "C"  void Enumerator__ctor_m247851533_gshared (Enumerator_t2017297076 * __this, List_1_t128053199 * ___l0, const MethodInfo* method);
// System.Void System.Collections.Generic.List`1/Enumerator<System.Int32>::VerifyState()
extern "C"  void Enumerator_VerifyState_m1898450050_gshared (Enumerator_t2017297076 * __this, const MethodInfo* method);
// System.Void System.Collections.Generic.List`1/Enumerator<System.Int32>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m502339360_gshared (Enumerator_t2017297076 * __this, const MethodInfo* method);
// System.Object System.Collections.Generic.List`1/Enumerator<System.Int32>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m323862414_gshared (Enumerator_t2017297076 * __this, const MethodInfo* method);
// System.Void System.Collections.Generic.List`1/Enumerator<System.Int32>::Dispose()
extern "C"  void Enumerator_Dispose_m222348240_gshared (Enumerator_t2017297076 * __this, const MethodInfo* method);
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.Int32>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m1131351993_gshared (Enumerator_t2017297076 * __this, const MethodInfo* method);
// T System.Collections.Generic.List`1/Enumerator<System.Int32>::get_Current()
extern "C"  int32_t Enumerator_get_Current_m2734313259_gshared (Enumerator_t2017297076 * __this, const MethodInfo* method);
// System.Void System.Collections.Generic.List`1/Enumerator<System.Object>::.ctor(System.Collections.Generic.List`1<T>)
extern "C"  void Enumerator__ctor_m3170385166_gshared (Enumerator_t2146457487 * __this, List_1_t257213610 * ___l0, const MethodInfo* method);
// System.Void System.Collections.Generic.List`1/Enumerator<System.Object>::VerifyState()
extern "C"  void Enumerator_VerifyState_m2933667029_gshared (Enumerator_t2146457487 * __this, const MethodInfo* method);
// System.Void System.Collections.Generic.List`1/Enumerator<System.Object>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m959124362_gshared (Enumerator_t2146457487 * __this, const MethodInfo* method);
// System.Object System.Collections.Generic.List`1/Enumerator<System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m3681948262_gshared (Enumerator_t2146457487 * __this, const MethodInfo* method);
// System.Void System.Collections.Generic.List`1/Enumerator<System.Object>::Dispose()
extern "C"  void Enumerator_Dispose_m3007748546_gshared (Enumerator_t2146457487 * __this, const MethodInfo* method);
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.Object>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m2142368520_gshared (Enumerator_t2146457487 * __this, const MethodInfo* method);
// T System.Collections.Generic.List`1/Enumerator<System.Object>::get_Current()
extern "C"  Il2CppObject * Enumerator_get_Current_m470245444_gshared (Enumerator_t2146457487 * __this, const MethodInfo* method);

// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,System.Boolean>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m2689609346(__this, p0, method) ((  void (*) (KeyValuePair_2_t1383673463 *, int32_t, const MethodInfo*))KeyValuePair_2_set_Key_m2689609346_gshared)(__this, p0, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,System.Boolean>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m1705789134(__this, p0, method) ((  void (*) (KeyValuePair_2_t1383673463 *, bool, const MethodInfo*))KeyValuePair_2_set_Value_m1705789134_gshared)(__this, p0, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,System.Boolean>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m3344522653(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t1383673463 *, int32_t, bool, const MethodInfo*))KeyValuePair_2__ctor_m3344522653_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.Int32,System.Boolean>::get_Key()
#define KeyValuePair_2_get_Key_m2536726212(__this, method) ((  int32_t (*) (KeyValuePair_2_t1383673463 *, const MethodInfo*))KeyValuePair_2_get_Key_m2536726212_gshared)(__this, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.Int32,System.Boolean>::get_Value()
#define KeyValuePair_2_get_Value_m3276326716(__this, method) ((  bool (*) (KeyValuePair_2_t1383673463 *, const MethodInfo*))KeyValuePair_2_get_Value_m3276326716_gshared)(__this, method)
// System.String System.Int32::ToString()
extern "C"  String_t* Int32_ToString_m141394615 (int32_t* __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Boolean::ToString()
extern "C"  String_t* Boolean_ToString_m2664721875 (bool* __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.String::Concat(System.String[])
extern "C"  String_t* String_Concat_m1809518182 (Il2CppObject * __this /* static, unused */, StringU5BU5D_t1281789340* ___values0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Collections.Generic.KeyValuePair`2<System.Int32,System.Boolean>::ToString()
#define KeyValuePair_2_ToString_m2644562007(__this, method) ((  String_t* (*) (KeyValuePair_2_t1383673463 *, const MethodInfo*))KeyValuePair_2_ToString_m2644562007_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,System.Char>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m3265358347(__this, p0, method) ((  void (*) (KeyValuePair_2_t625878672 *, int32_t, const MethodInfo*))KeyValuePair_2_set_Key_m3265358347_gshared)(__this, p0, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,System.Char>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m3679535380(__this, p0, method) ((  void (*) (KeyValuePair_2_t625878672 *, Il2CppChar, const MethodInfo*))KeyValuePair_2_set_Value_m3679535380_gshared)(__this, p0, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,System.Char>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m1972131968(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t625878672 *, int32_t, Il2CppChar, const MethodInfo*))KeyValuePair_2__ctor_m1972131968_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.Int32,System.Char>::get_Key()
#define KeyValuePair_2_get_Key_m2701016533(__this, method) ((  int32_t (*) (KeyValuePair_2_t625878672 *, const MethodInfo*))KeyValuePair_2_get_Key_m2701016533_gshared)(__this, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.Int32,System.Char>::get_Value()
#define KeyValuePair_2_get_Value_m4183788833(__this, method) ((  Il2CppChar (*) (KeyValuePair_2_t625878672 *, const MethodInfo*))KeyValuePair_2_get_Value_m4183788833_gshared)(__this, method)
// System.String System.Char::ToString()
extern "C"  String_t* Char_ToString_m3588025615 (Il2CppChar* __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Collections.Generic.KeyValuePair`2<System.Int32,System.Char>::ToString()
#define KeyValuePair_2_ToString_m823870646(__this, method) ((  String_t* (*) (KeyValuePair_2_t625878672 *, const MethodInfo*))KeyValuePair_2_ToString_m823870646_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m1660334707(__this, p0, method) ((  void (*) (KeyValuePair_2_t4237331251 *, int32_t, const MethodInfo*))KeyValuePair_2_set_Key_m1660334707_gshared)(__this, p0, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m2314909969(__this, p0, method) ((  void (*) (KeyValuePair_2_t4237331251 *, int32_t, const MethodInfo*))KeyValuePair_2_set_Value_m2314909969_gshared)(__this, p0, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m2493299567(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t4237331251 *, int32_t, int32_t, const MethodInfo*))KeyValuePair_2__ctor_m2493299567_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>::get_Key()
#define KeyValuePair_2_get_Key_m2288232422(__this, method) ((  int32_t (*) (KeyValuePair_2_t4237331251 *, const MethodInfo*))KeyValuePair_2_get_Key_m2288232422_gshared)(__this, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>::get_Value()
#define KeyValuePair_2_get_Value_m3536231958(__this, method) ((  int32_t (*) (KeyValuePair_2_t4237331251 *, const MethodInfo*))KeyValuePair_2_get_Value_m3536231958_gshared)(__this, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>::ToString()
#define KeyValuePair_2_ToString_m65692449(__this, method) ((  String_t* (*) (KeyValuePair_2_t4237331251 *, const MethodInfo*))KeyValuePair_2_ToString_m65692449_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int64>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m975493760(__this, p0, method) ((  void (*) (KeyValuePair_2_t727985506 *, int32_t, const MethodInfo*))KeyValuePair_2_set_Key_m975493760_gshared)(__this, p0, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int64>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m596466564(__this, p0, method) ((  void (*) (KeyValuePair_2_t727985506 *, int64_t, const MethodInfo*))KeyValuePair_2_set_Value_m596466564_gshared)(__this, p0, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int64>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m844205450(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t727985506 *, int32_t, int64_t, const MethodInfo*))KeyValuePair_2__ctor_m844205450_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int64>::get_Key()
#define KeyValuePair_2_get_Key_m1737874887(__this, method) ((  int32_t (*) (KeyValuePair_2_t727985506 *, const MethodInfo*))KeyValuePair_2_get_Key_m1737874887_gshared)(__this, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int64>::get_Value()
#define KeyValuePair_2_get_Value_m3934599775(__this, method) ((  int64_t (*) (KeyValuePair_2_t727985506 *, const MethodInfo*))KeyValuePair_2_get_Value_m3934599775_gshared)(__this, method)
// System.String System.Int64::ToString()
extern "C"  String_t* Int64_ToString_m2986581816 (int64_t* __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int64>::ToString()
#define KeyValuePair_2_ToString_m4221841369(__this, method) ((  String_t* (*) (KeyValuePair_2_t727985506 *, const MethodInfo*))KeyValuePair_2_ToString_m4221841369_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m2121548577(__this, p0, method) ((  void (*) (KeyValuePair_2_t71524366 *, int32_t, const MethodInfo*))KeyValuePair_2_set_Key_m2121548577_gshared)(__this, p0, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m3358607572(__this, p0, method) ((  void (*) (KeyValuePair_2_t71524366 *, Il2CppObject *, const MethodInfo*))KeyValuePair_2_set_Value_m3358607572_gshared)(__this, p0, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m2118224448(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t71524366 *, int32_t, Il2CppObject *, const MethodInfo*))KeyValuePair_2__ctor_m2118224448_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>::get_Key()
#define KeyValuePair_2_get_Key_m1839753989(__this, method) ((  int32_t (*) (KeyValuePair_2_t71524366 *, const MethodInfo*))KeyValuePair_2_get_Key_m1839753989_gshared)(__this, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>::get_Value()
#define KeyValuePair_2_get_Value_m3495598764(__this, method) ((  Il2CppObject * (*) (KeyValuePair_2_t71524366 *, const MethodInfo*))KeyValuePair_2_get_Value_m3495598764_gshared)(__this, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>::ToString()
#define KeyValuePair_2_ToString_m1238786018(__this, method) ((  String_t* (*) (KeyValuePair_2_t71524366 *, const MethodInfo*))KeyValuePair_2_ToString_m1238786018_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int64,System.Object>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m1382769311(__this, p0, method) ((  void (*) (KeyValuePair_2_t2245450819 *, int64_t, const MethodInfo*))KeyValuePair_2_set_Key_m1382769311_gshared)(__this, p0, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int64,System.Object>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m1316464643(__this, p0, method) ((  void (*) (KeyValuePair_2_t2245450819 *, Il2CppObject *, const MethodInfo*))KeyValuePair_2_set_Value_m1316464643_gshared)(__this, p0, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int64,System.Object>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m2056996813(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t2245450819 *, int64_t, Il2CppObject *, const MethodInfo*))KeyValuePair_2__ctor_m2056996813_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.Int64,System.Object>::get_Key()
#define KeyValuePair_2_get_Key_m635992374(__this, method) ((  int64_t (*) (KeyValuePair_2_t2245450819 *, const MethodInfo*))KeyValuePair_2_get_Key_m635992374_gshared)(__this, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.Int64,System.Object>::get_Value()
#define KeyValuePair_2_get_Value_m392474074(__this, method) ((  Il2CppObject * (*) (KeyValuePair_2_t2245450819 *, const MethodInfo*))KeyValuePair_2_get_Value_m392474074_gshared)(__this, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.Int64,System.Object>::ToString()
#define KeyValuePair_2_ToString_m3013572783(__this, method) ((  String_t* (*) (KeyValuePair_2_t2245450819 *, const MethodInfo*))KeyValuePair_2_ToString_m3013572783_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m2116817417(__this, p0, method) ((  void (*) (KeyValuePair_2_t3842366416 *, Il2CppObject *, const MethodInfo*))KeyValuePair_2_set_Key_m2116817417_gshared)(__this, p0, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m3305319569(__this, p0, method) ((  void (*) (KeyValuePair_2_t3842366416 *, bool, const MethodInfo*))KeyValuePair_2_set_Value_m3305319569_gshared)(__this, p0, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m23191374(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t3842366416 *, Il2CppObject *, bool, const MethodInfo*))KeyValuePair_2__ctor_m23191374_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>::get_Key()
#define KeyValuePair_2_get_Key_m2106922848(__this, method) ((  Il2CppObject * (*) (KeyValuePair_2_t3842366416 *, const MethodInfo*))KeyValuePair_2_get_Key_m2106922848_gshared)(__this, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>::get_Value()
#define KeyValuePair_2_get_Value_m1669764045(__this, method) ((  bool (*) (KeyValuePair_2_t3842366416 *, const MethodInfo*))KeyValuePair_2_get_Value_m1669764045_gshared)(__this, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>::ToString()
#define KeyValuePair_2_ToString_m2480962023(__this, method) ((  String_t* (*) (KeyValuePair_2_t3842366416 *, const MethodInfo*))KeyValuePair_2_ToString_m2480962023_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m4256290317(__this, p0, method) ((  void (*) (KeyValuePair_2_t2401056908 *, Il2CppObject *, const MethodInfo*))KeyValuePair_2_set_Key_m4256290317_gshared)(__this, p0, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m460969740(__this, p0, method) ((  void (*) (KeyValuePair_2_t2401056908 *, int32_t, const MethodInfo*))KeyValuePair_2_set_Value_m460969740_gshared)(__this, p0, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m880186442(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t2401056908 *, Il2CppObject *, int32_t, const MethodInfo*))KeyValuePair_2__ctor_m880186442_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>::get_Key()
#define KeyValuePair_2_get_Key_m1218836954(__this, method) ((  Il2CppObject * (*) (KeyValuePair_2_t2401056908 *, const MethodInfo*))KeyValuePair_2_get_Key_m1218836954_gshared)(__this, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>::get_Value()
#define KeyValuePair_2_get_Value_m755756747(__this, method) ((  int32_t (*) (KeyValuePair_2_t2401056908 *, const MethodInfo*))KeyValuePair_2_get_Value_m755756747_gshared)(__this, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>::ToString()
#define KeyValuePair_2_ToString_m4231614106(__this, method) ((  String_t* (*) (KeyValuePair_2_t2401056908 *, const MethodInfo*))KeyValuePair_2_ToString_m4231614106_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m3170517671(__this, p0, method) ((  void (*) (KeyValuePair_2_t2530217319 *, Il2CppObject *, const MethodInfo*))KeyValuePair_2_set_Key_m3170517671_gshared)(__this, p0, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m1153752644(__this, p0, method) ((  void (*) (KeyValuePair_2_t2530217319 *, Il2CppObject *, const MethodInfo*))KeyValuePair_2_set_Value_m1153752644_gshared)(__this, p0, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m1794021352(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t2530217319 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))KeyValuePair_2__ctor_m1794021352_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>::get_Key()
#define KeyValuePair_2_get_Key_m1328507389(__this, method) ((  Il2CppObject * (*) (KeyValuePair_2_t2530217319 *, const MethodInfo*))KeyValuePair_2_get_Key_m1328507389_gshared)(__this, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>::get_Value()
#define KeyValuePair_2_get_Value_m3464904234(__this, method) ((  Il2CppObject * (*) (KeyValuePair_2_t2530217319 *, const MethodInfo*))KeyValuePair_2_get_Value_m3464904234_gshared)(__this, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>::ToString()
#define KeyValuePair_2_ToString_m510648957(__this, method) ((  String_t* (*) (KeyValuePair_2_t2530217319 *, const MethodInfo*))KeyValuePair_2_ToString_m510648957_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Single>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m2965168904(__this, p0, method) ((  void (*) (KeyValuePair_2_t847377929 *, Il2CppObject *, const MethodInfo*))KeyValuePair_2_set_Key_m2965168904_gshared)(__this, p0, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Single>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m955705697(__this, p0, method) ((  void (*) (KeyValuePair_2_t847377929 *, float, const MethodInfo*))KeyValuePair_2_set_Value_m955705697_gshared)(__this, p0, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Single>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m3552061076(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t847377929 *, Il2CppObject *, float, const MethodInfo*))KeyValuePair_2__ctor_m3552061076_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.Object,System.Single>::get_Key()
#define KeyValuePair_2_get_Key_m3754157295(__this, method) ((  Il2CppObject * (*) (KeyValuePair_2_t847377929 *, const MethodInfo*))KeyValuePair_2_get_Key_m3754157295_gshared)(__this, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.Object,System.Single>::get_Value()
#define KeyValuePair_2_get_Value_m1635977167(__this, method) ((  float (*) (KeyValuePair_2_t847377929 *, const MethodInfo*))KeyValuePair_2_get_Value_m1635977167_gshared)(__this, method)
// System.String System.Single::ToString()
extern "C"  String_t* Single_ToString_m3947131094 (float* __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Collections.Generic.KeyValuePair`2<System.Object,System.Single>::ToString()
#define KeyValuePair_2_ToString_m366204361(__this, method) ((  String_t* (*) (KeyValuePair_2_t847377929 *, const MethodInfo*))KeyValuePair_2_ToString_m366204361_gshared)(__this, method)
// System.Void System.Collections.Generic.LinkedList`1/Enumerator<System.Object>::.ctor(System.Collections.Generic.LinkedList`1<T>)
#define Enumerator__ctor_m2504641326(__this, ___parent0, method) ((  void (*) (Enumerator_t139379724 *, LinkedList_1_t1919752173 *, const MethodInfo*))Enumerator__ctor_m2504641326_gshared)(__this, ___parent0, method)
// T System.Collections.Generic.LinkedList`1/Enumerator<System.Object>::get_Current()
#define Enumerator_get_Current_m2579293505(__this, method) ((  Il2CppObject * (*) (Enumerator_t139379724 *, const MethodInfo*))Enumerator_get_Current_m2579293505_gshared)(__this, method)
// System.Object System.Collections.Generic.LinkedList`1/Enumerator<System.Object>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m3484854280(__this, method) ((  Il2CppObject * (*) (Enumerator_t139379724 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m3484854280_gshared)(__this, method)
// System.Void System.ObjectDisposedException::.ctor(System.String)
extern "C"  void ObjectDisposedException__ctor_m3603759869 (ObjectDisposedException_t21392786 * __this, String_t* p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.InvalidOperationException::.ctor(System.String)
extern "C"  void InvalidOperationException__ctor_m237278729 (InvalidOperationException_t56020091 * __this, String_t* p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.LinkedList`1/Enumerator<System.Object>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m2550940120(__this, method) ((  void (*) (Enumerator_t139379724 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m2550940120_gshared)(__this, method)
// System.Void System.InvalidOperationException::.ctor()
extern "C"  void InvalidOperationException__ctor_m2734335978 (InvalidOperationException_t56020091 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Collections.Generic.LinkedList`1/Enumerator<System.Object>::MoveNext()
#define Enumerator_MoveNext_m880141662(__this, method) ((  bool (*) (Enumerator_t139379724 *, const MethodInfo*))Enumerator_MoveNext_m880141662_gshared)(__this, method)
// System.Void System.Collections.Generic.LinkedList`1/Enumerator<System.Object>::Dispose()
#define Enumerator_Dispose_m2375640141(__this, method) ((  void (*) (Enumerator_t139379724 *, const MethodInfo*))Enumerator_Dispose_m2375640141_gshared)(__this, method)
// System.Void System.Object::.ctor()
extern "C"  void Object__ctor_m297566312 (Il2CppObject * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.ArgumentException::.ctor(System.String)
extern "C"  void ArgumentException__ctor_m1312628991 (ArgumentException_t132251570 * __this, String_t* p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.ArgumentNullException::.ctor(System.String)
extern "C"  void ArgumentNullException__ctor_m1170824041 (ArgumentNullException_t1615371798 * __this, String_t* p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Array::GetLowerBound(System.Int32)
extern "C"  int32_t Array_GetLowerBound_m2045984623 (Il2CppArray * __this, int32_t p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.ArgumentOutOfRangeException::.ctor(System.String)
extern "C"  void ArgumentOutOfRangeException__ctor_m3628145864 (ArgumentOutOfRangeException_t777629997 * __this, String_t* p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Array::get_Rank()
extern "C"  int32_t Array_get_Rank_m3448755881 (Il2CppArray * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.ArgumentException::.ctor(System.String,System.String)
extern "C"  void ArgumentException__ctor_m1216717135 (ArgumentException_t132251570 * __this, String_t* p0, String_t* p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type System.Type::GetTypeFromHandle(System.RuntimeTypeHandle)
extern "C"  Type_t * Type_GetTypeFromHandle_m1620074514 (Il2CppObject * __this /* static, unused */, RuntimeTypeHandle_t3027515415  p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Serialization.SerializationInfo::AddValue(System.String,System.Object,System.Type)
extern "C"  void SerializationInfo_AddValue_m3906743584 (SerializationInfo_t950877179 * __this, String_t* p0, Il2CppObject * p1, Type_t * p2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Serialization.SerializationInfo::AddValue(System.String,System.UInt32)
extern "C"  void SerializationInfo_AddValue_m3594631505 (SerializationInfo_t950877179 * __this, String_t* p0, uint32_t p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Runtime.Serialization.SerializationInfo::GetValue(System.String,System.Type)
extern "C"  Il2CppObject * SerializationInfo_GetValue_m42271953 (SerializationInfo_t950877179 * __this, String_t* p0, Type_t * p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 System.Runtime.Serialization.SerializationInfo::GetUInt32(System.String)
extern "C"  uint32_t SerializationInfo_GetUInt32_m776835457 (SerializationInfo_t950877179 * __this, String_t* p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.List`1/Enumerator<CustomCameraController/InputFrameData>::.ctor(System.Collections.Generic.List`1<T>)
#define Enumerator__ctor_m69053308(__this, ___l0, method) ((  void (*) (Enumerator_t1640318223 *, List_1_t4046041642 *, const MethodInfo*))Enumerator__ctor_m69053308_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<CustomCameraController/InputFrameData>::VerifyState()
#define Enumerator_VerifyState_m2137990666(__this, method) ((  void (*) (Enumerator_t1640318223 *, const MethodInfo*))Enumerator_VerifyState_m2137990666_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<CustomCameraController/InputFrameData>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m3420610639(__this, method) ((  void (*) (Enumerator_t1640318223 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m3420610639_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<CustomCameraController/InputFrameData>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m2348568638(__this, method) ((  Il2CppObject * (*) (Enumerator_t1640318223 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m2348568638_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<CustomCameraController/InputFrameData>::Dispose()
#define Enumerator_Dispose_m2308405500(__this, method) ((  void (*) (Enumerator_t1640318223 *, const MethodInfo*))Enumerator_Dispose_m2308405500_gshared)(__this, method)
// System.Type System.Object::GetType()
extern "C"  Type_t * Object_GetType_m88164663 (Il2CppObject * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Collections.Generic.List`1/Enumerator<CustomCameraController/InputFrameData>::MoveNext()
#define Enumerator_MoveNext_m1995532294(__this, method) ((  bool (*) (Enumerator_t1640318223 *, const MethodInfo*))Enumerator_MoveNext_m1995532294_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<CustomCameraController/InputFrameData>::get_Current()
#define Enumerator_get_Current_m1721394586(__this, method) ((  InputFrameData_t2573966900  (*) (Enumerator_t1640318223 *, const MethodInfo*))Enumerator_get_Current_m1721394586_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<HighlightingSystem.HighlighterRenderer/Data>::.ctor(System.Collections.Generic.List`1<T>)
#define Enumerator__ctor_m583292949(__this, ___l0, method) ((  void (*) (Enumerator_t3796191691 *, List_1_t1906947814 *, const MethodInfo*))Enumerator__ctor_m583292949_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<HighlightingSystem.HighlighterRenderer/Data>::VerifyState()
#define Enumerator_VerifyState_m1268345934(__this, method) ((  void (*) (Enumerator_t3796191691 *, const MethodInfo*))Enumerator_VerifyState_m1268345934_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<HighlightingSystem.HighlighterRenderer/Data>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m2985421904(__this, method) ((  void (*) (Enumerator_t3796191691 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m2985421904_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<HighlightingSystem.HighlighterRenderer/Data>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m2892844362(__this, method) ((  Il2CppObject * (*) (Enumerator_t3796191691 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m2892844362_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<HighlightingSystem.HighlighterRenderer/Data>::Dispose()
#define Enumerator_Dispose_m1071958056(__this, method) ((  void (*) (Enumerator_t3796191691 *, const MethodInfo*))Enumerator_Dispose_m1071958056_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<HighlightingSystem.HighlighterRenderer/Data>::MoveNext()
#define Enumerator_MoveNext_m4173244301(__this, method) ((  bool (*) (Enumerator_t3796191691 *, const MethodInfo*))Enumerator_MoveNext_m4173244301_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<HighlightingSystem.HighlighterRenderer/Data>::get_Current()
#define Enumerator_get_Current_m935646724(__this, method) ((  Data_t434873072  (*) (Enumerator_t3796191691 *, const MethodInfo*))Enumerator_get_Current_m935646724_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<HighlightingSystem.HighlightingPreset>::.ctor(System.Collections.Generic.List`1<T>)
#define Enumerator__ctor_m3930688850(__this, ___l0, method) ((  void (*) (Enumerator_t3996938410 *, List_1_t2107694533 *, const MethodInfo*))Enumerator__ctor_m3930688850_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<HighlightingSystem.HighlightingPreset>::VerifyState()
#define Enumerator_VerifyState_m2056140850(__this, method) ((  void (*) (Enumerator_t3996938410 *, const MethodInfo*))Enumerator_VerifyState_m2056140850_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<HighlightingSystem.HighlightingPreset>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m3021194315(__this, method) ((  void (*) (Enumerator_t3996938410 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m3021194315_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<HighlightingSystem.HighlightingPreset>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m3745379029(__this, method) ((  Il2CppObject * (*) (Enumerator_t3996938410 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m3745379029_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<HighlightingSystem.HighlightingPreset>::Dispose()
#define Enumerator_Dispose_m3559089685(__this, method) ((  void (*) (Enumerator_t3996938410 *, const MethodInfo*))Enumerator_Dispose_m3559089685_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<HighlightingSystem.HighlightingPreset>::MoveNext()
#define Enumerator_MoveNext_m2406304866(__this, method) ((  bool (*) (Enumerator_t3996938410 *, const MethodInfo*))Enumerator_MoveNext_m2406304866_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<HighlightingSystem.HighlightingPreset>::get_Current()
#define Enumerator_get_Current_m1356247204(__this, method) ((  HighlightingPreset_t635619791  (*) (Enumerator_t3996938410 *, const MethodInfo*))Enumerator_get_Current_m1356247204_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<System.Char>::.ctor(System.Collections.Generic.List`1<T>)
#define Enumerator__ctor_m2263237050(__this, ___l0, method) ((  void (*) (Enumerator_t2700811793 *, List_1_t811567916 *, const MethodInfo*))Enumerator__ctor_m2263237050_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<System.Char>::VerifyState()
#define Enumerator_VerifyState_m1375238439(__this, method) ((  void (*) (Enumerator_t2700811793 *, const MethodInfo*))Enumerator_VerifyState_m1375238439_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<System.Char>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m2394182378(__this, method) ((  void (*) (Enumerator_t2700811793 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m2394182378_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<System.Char>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m1846615603(__this, method) ((  Il2CppObject * (*) (Enumerator_t2700811793 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m1846615603_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<System.Char>::Dispose()
#define Enumerator_Dispose_m3961787728(__this, method) ((  void (*) (Enumerator_t2700811793 *, const MethodInfo*))Enumerator_Dispose_m3961787728_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.Char>::MoveNext()
#define Enumerator_MoveNext_m1940664149(__this, method) ((  bool (*) (Enumerator_t2700811793 *, const MethodInfo*))Enumerator_MoveNext_m1940664149_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<System.Char>::get_Current()
#define Enumerator_get_Current_m1436440636(__this, method) ((  Il2CppChar (*) (Enumerator_t2700811793 *, const MethodInfo*))Enumerator_get_Current_m1436440636_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<System.Int32>::.ctor(System.Collections.Generic.List`1<T>)
#define Enumerator__ctor_m247851533(__this, ___l0, method) ((  void (*) (Enumerator_t2017297076 *, List_1_t128053199 *, const MethodInfo*))Enumerator__ctor_m247851533_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<System.Int32>::VerifyState()
#define Enumerator_VerifyState_m1898450050(__this, method) ((  void (*) (Enumerator_t2017297076 *, const MethodInfo*))Enumerator_VerifyState_m1898450050_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<System.Int32>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m502339360(__this, method) ((  void (*) (Enumerator_t2017297076 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m502339360_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<System.Int32>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m323862414(__this, method) ((  Il2CppObject * (*) (Enumerator_t2017297076 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m323862414_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<System.Int32>::Dispose()
#define Enumerator_Dispose_m222348240(__this, method) ((  void (*) (Enumerator_t2017297076 *, const MethodInfo*))Enumerator_Dispose_m222348240_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.Int32>::MoveNext()
#define Enumerator_MoveNext_m1131351993(__this, method) ((  bool (*) (Enumerator_t2017297076 *, const MethodInfo*))Enumerator_MoveNext_m1131351993_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<System.Int32>::get_Current()
#define Enumerator_get_Current_m2734313259(__this, method) ((  int32_t (*) (Enumerator_t2017297076 *, const MethodInfo*))Enumerator_get_Current_m2734313259_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<System.Object>::.ctor(System.Collections.Generic.List`1<T>)
#define Enumerator__ctor_m3170385166(__this, ___l0, method) ((  void (*) (Enumerator_t2146457487 *, List_1_t257213610 *, const MethodInfo*))Enumerator__ctor_m3170385166_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<System.Object>::VerifyState()
#define Enumerator_VerifyState_m2933667029(__this, method) ((  void (*) (Enumerator_t2146457487 *, const MethodInfo*))Enumerator_VerifyState_m2933667029_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<System.Object>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m959124362(__this, method) ((  void (*) (Enumerator_t2146457487 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m959124362_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<System.Object>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m3681948262(__this, method) ((  Il2CppObject * (*) (Enumerator_t2146457487 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m3681948262_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<System.Object>::Dispose()
#define Enumerator_Dispose_m3007748546(__this, method) ((  void (*) (Enumerator_t2146457487 *, const MethodInfo*))Enumerator_Dispose_m3007748546_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.Object>::MoveNext()
#define Enumerator_MoveNext_m2142368520(__this, method) ((  bool (*) (Enumerator_t2146457487 *, const MethodInfo*))Enumerator_MoveNext_m2142368520_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<System.Object>::get_Current()
#define Enumerator_get_Current_m470245444(__this, method) ((  Il2CppObject * (*) (Enumerator_t2146457487 *, const MethodInfo*))Enumerator_get_Current_m470245444_gshared)(__this, method)
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,System.Boolean>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m3344522653_gshared (KeyValuePair_2_t1383673463 * __this, int32_t ___key0, bool ___value1, const MethodInfo* method)
{
	{
		int32_t L_0 = ___key0;
		KeyValuePair_2_set_Key_m2689609346((KeyValuePair_2_t1383673463 *)__this, (int32_t)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		bool L_1 = ___value1;
		KeyValuePair_2_set_Value_m1705789134((KeyValuePair_2_t1383673463 *)__this, (bool)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return;
	}
}
extern "C"  void KeyValuePair_2__ctor_m3344522653_AdjustorThunk (Il2CppObject * __this, int32_t ___key0, bool ___value1, const MethodInfo* method)
{
	KeyValuePair_2_t1383673463 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t1383673463 *>(__this + 1);
	KeyValuePair_2__ctor_m3344522653(_thisAdjusted, ___key0, ___value1, method);
}
// TKey System.Collections.Generic.KeyValuePair`2<System.Int32,System.Boolean>::get_Key()
extern "C"  int32_t KeyValuePair_2_get_Key_m2536726212_gshared (KeyValuePair_2_t1383673463 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (int32_t)__this->get_key_0();
		return L_0;
	}
}
extern "C"  int32_t KeyValuePair_2_get_Key_m2536726212_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t1383673463 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t1383673463 *>(__this + 1);
	return KeyValuePair_2_get_Key_m2536726212(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,System.Boolean>::set_Key(TKey)
extern "C"  void KeyValuePair_2_set_Key_m2689609346_gshared (KeyValuePair_2_t1383673463 * __this, int32_t ___value0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_key_0(L_0);
		return;
	}
}
extern "C"  void KeyValuePair_2_set_Key_m2689609346_AdjustorThunk (Il2CppObject * __this, int32_t ___value0, const MethodInfo* method)
{
	KeyValuePair_2_t1383673463 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t1383673463 *>(__this + 1);
	KeyValuePair_2_set_Key_m2689609346(_thisAdjusted, ___value0, method);
}
// TValue System.Collections.Generic.KeyValuePair`2<System.Int32,System.Boolean>::get_Value()
extern "C"  bool KeyValuePair_2_get_Value_m3276326716_gshared (KeyValuePair_2_t1383673463 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)__this->get_value_1();
		return L_0;
	}
}
extern "C"  bool KeyValuePair_2_get_Value_m3276326716_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t1383673463 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t1383673463 *>(__this + 1);
	return KeyValuePair_2_get_Value_m3276326716(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,System.Boolean>::set_Value(TValue)
extern "C"  void KeyValuePair_2_set_Value_m1705789134_gshared (KeyValuePair_2_t1383673463 * __this, bool ___value0, const MethodInfo* method)
{
	{
		bool L_0 = ___value0;
		__this->set_value_1(L_0);
		return;
	}
}
extern "C"  void KeyValuePair_2_set_Value_m1705789134_AdjustorThunk (Il2CppObject * __this, bool ___value0, const MethodInfo* method)
{
	KeyValuePair_2_t1383673463 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t1383673463 *>(__this + 1);
	KeyValuePair_2_set_Value_m1705789134(_thisAdjusted, ___value0, method);
}
// System.String System.Collections.Generic.KeyValuePair`2<System.Int32,System.Boolean>::ToString()
extern "C"  String_t* KeyValuePair_2_ToString_m2644562007_gshared (KeyValuePair_2_t1383673463 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KeyValuePair_2_ToString_m2644562007_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	bool V_1 = false;
	int32_t G_B2_0 = 0;
	StringU5BU5D_t1281789340* G_B2_1 = NULL;
	StringU5BU5D_t1281789340* G_B2_2 = NULL;
	int32_t G_B1_0 = 0;
	StringU5BU5D_t1281789340* G_B1_1 = NULL;
	StringU5BU5D_t1281789340* G_B1_2 = NULL;
	String_t* G_B3_0 = NULL;
	int32_t G_B3_1 = 0;
	StringU5BU5D_t1281789340* G_B3_2 = NULL;
	StringU5BU5D_t1281789340* G_B3_3 = NULL;
	int32_t G_B5_0 = 0;
	StringU5BU5D_t1281789340* G_B5_1 = NULL;
	StringU5BU5D_t1281789340* G_B5_2 = NULL;
	int32_t G_B4_0 = 0;
	StringU5BU5D_t1281789340* G_B4_1 = NULL;
	StringU5BU5D_t1281789340* G_B4_2 = NULL;
	String_t* G_B6_0 = NULL;
	int32_t G_B6_1 = 0;
	StringU5BU5D_t1281789340* G_B6_2 = NULL;
	StringU5BU5D_t1281789340* G_B6_3 = NULL;
	{
		StringU5BU5D_t1281789340* L_0 = (StringU5BU5D_t1281789340*)((StringU5BU5D_t1281789340*)SZArrayNew(StringU5BU5D_t1281789340_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_0);
		ArrayElementTypeCheck (L_0, _stringLiteral3452614645);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral3452614645);
		StringU5BU5D_t1281789340* L_1 = (StringU5BU5D_t1281789340*)L_0;
		int32_t L_2 = KeyValuePair_2_get_Key_m2536726212((KeyValuePair_2_t1383673463 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		G_B1_0 = 1;
		G_B1_1 = L_1;
		G_B1_2 = L_1;
	}
	{
		int32_t L_3 = KeyValuePair_2_get_Key_m2536726212((KeyValuePair_2_t1383673463 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		V_0 = (int32_t)L_3;
		String_t* L_4 = Int32_ToString_m141394615((int32_t*)(&V_0), /*hidden argument*/NULL);
		G_B3_0 = L_4;
		G_B3_1 = G_B1_0;
		G_B3_2 = G_B1_1;
		G_B3_3 = G_B1_2;
		goto IL_003e;
	}

IL_0039:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B3_0 = L_5;
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
		G_B3_3 = G_B2_2;
	}

IL_003e:
	{
		NullCheck(G_B3_2);
		ArrayElementTypeCheck (G_B3_2, G_B3_0);
		(G_B3_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B3_1), (String_t*)G_B3_0);
		StringU5BU5D_t1281789340* L_6 = (StringU5BU5D_t1281789340*)G_B3_3;
		NullCheck(L_6);
		ArrayElementTypeCheck (L_6, _stringLiteral3450517380);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral3450517380);
		StringU5BU5D_t1281789340* L_7 = (StringU5BU5D_t1281789340*)L_6;
		bool L_8 = KeyValuePair_2_get_Value_m3276326716((KeyValuePair_2_t1383673463 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		G_B4_0 = 3;
		G_B4_1 = L_7;
		G_B4_2 = L_7;
	}
	{
		bool L_9 = KeyValuePair_2_get_Value_m3276326716((KeyValuePair_2_t1383673463 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		V_1 = (bool)L_9;
		String_t* L_10 = Boolean_ToString_m2664721875((bool*)(&V_1), /*hidden argument*/NULL);
		G_B6_0 = L_10;
		G_B6_1 = G_B4_0;
		G_B6_2 = G_B4_1;
		G_B6_3 = G_B4_2;
		goto IL_0077;
	}

IL_0072:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_11 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B6_0 = L_11;
		G_B6_1 = G_B5_0;
		G_B6_2 = G_B5_1;
		G_B6_3 = G_B5_2;
	}

IL_0077:
	{
		NullCheck(G_B6_2);
		ArrayElementTypeCheck (G_B6_2, G_B6_0);
		(G_B6_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B6_1), (String_t*)G_B6_0);
		StringU5BU5D_t1281789340* L_12 = (StringU5BU5D_t1281789340*)G_B6_3;
		NullCheck(L_12);
		ArrayElementTypeCheck (L_12, _stringLiteral3452614643);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)_stringLiteral3452614643);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_13 = String_Concat_m1809518182(NULL /*static, unused*/, (StringU5BU5D_t1281789340*)L_12, /*hidden argument*/NULL);
		return L_13;
	}
}
extern "C"  String_t* KeyValuePair_2_ToString_m2644562007_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t1383673463 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t1383673463 *>(__this + 1);
	return KeyValuePair_2_ToString_m2644562007(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,System.Char>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m1972131968_gshared (KeyValuePair_2_t625878672 * __this, int32_t ___key0, Il2CppChar ___value1, const MethodInfo* method)
{
	{
		int32_t L_0 = ___key0;
		KeyValuePair_2_set_Key_m3265358347((KeyValuePair_2_t625878672 *)__this, (int32_t)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Il2CppChar L_1 = ___value1;
		KeyValuePair_2_set_Value_m3679535380((KeyValuePair_2_t625878672 *)__this, (Il2CppChar)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return;
	}
}
extern "C"  void KeyValuePair_2__ctor_m1972131968_AdjustorThunk (Il2CppObject * __this, int32_t ___key0, Il2CppChar ___value1, const MethodInfo* method)
{
	KeyValuePair_2_t625878672 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t625878672 *>(__this + 1);
	KeyValuePair_2__ctor_m1972131968(_thisAdjusted, ___key0, ___value1, method);
}
// TKey System.Collections.Generic.KeyValuePair`2<System.Int32,System.Char>::get_Key()
extern "C"  int32_t KeyValuePair_2_get_Key_m2701016533_gshared (KeyValuePair_2_t625878672 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (int32_t)__this->get_key_0();
		return L_0;
	}
}
extern "C"  int32_t KeyValuePair_2_get_Key_m2701016533_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t625878672 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t625878672 *>(__this + 1);
	return KeyValuePair_2_get_Key_m2701016533(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,System.Char>::set_Key(TKey)
extern "C"  void KeyValuePair_2_set_Key_m3265358347_gshared (KeyValuePair_2_t625878672 * __this, int32_t ___value0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_key_0(L_0);
		return;
	}
}
extern "C"  void KeyValuePair_2_set_Key_m3265358347_AdjustorThunk (Il2CppObject * __this, int32_t ___value0, const MethodInfo* method)
{
	KeyValuePair_2_t625878672 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t625878672 *>(__this + 1);
	KeyValuePair_2_set_Key_m3265358347(_thisAdjusted, ___value0, method);
}
// TValue System.Collections.Generic.KeyValuePair`2<System.Int32,System.Char>::get_Value()
extern "C"  Il2CppChar KeyValuePair_2_get_Value_m4183788833_gshared (KeyValuePair_2_t625878672 * __this, const MethodInfo* method)
{
	{
		Il2CppChar L_0 = (Il2CppChar)__this->get_value_1();
		return L_0;
	}
}
extern "C"  Il2CppChar KeyValuePair_2_get_Value_m4183788833_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t625878672 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t625878672 *>(__this + 1);
	return KeyValuePair_2_get_Value_m4183788833(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,System.Char>::set_Value(TValue)
extern "C"  void KeyValuePair_2_set_Value_m3679535380_gshared (KeyValuePair_2_t625878672 * __this, Il2CppChar ___value0, const MethodInfo* method)
{
	{
		Il2CppChar L_0 = ___value0;
		__this->set_value_1(L_0);
		return;
	}
}
extern "C"  void KeyValuePair_2_set_Value_m3679535380_AdjustorThunk (Il2CppObject * __this, Il2CppChar ___value0, const MethodInfo* method)
{
	KeyValuePair_2_t625878672 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t625878672 *>(__this + 1);
	KeyValuePair_2_set_Value_m3679535380(_thisAdjusted, ___value0, method);
}
// System.String System.Collections.Generic.KeyValuePair`2<System.Int32,System.Char>::ToString()
extern "C"  String_t* KeyValuePair_2_ToString_m823870646_gshared (KeyValuePair_2_t625878672 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KeyValuePair_2_ToString_m823870646_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	Il2CppChar V_1 = 0x0;
	int32_t G_B2_0 = 0;
	StringU5BU5D_t1281789340* G_B2_1 = NULL;
	StringU5BU5D_t1281789340* G_B2_2 = NULL;
	int32_t G_B1_0 = 0;
	StringU5BU5D_t1281789340* G_B1_1 = NULL;
	StringU5BU5D_t1281789340* G_B1_2 = NULL;
	String_t* G_B3_0 = NULL;
	int32_t G_B3_1 = 0;
	StringU5BU5D_t1281789340* G_B3_2 = NULL;
	StringU5BU5D_t1281789340* G_B3_3 = NULL;
	int32_t G_B5_0 = 0;
	StringU5BU5D_t1281789340* G_B5_1 = NULL;
	StringU5BU5D_t1281789340* G_B5_2 = NULL;
	int32_t G_B4_0 = 0;
	StringU5BU5D_t1281789340* G_B4_1 = NULL;
	StringU5BU5D_t1281789340* G_B4_2 = NULL;
	String_t* G_B6_0 = NULL;
	int32_t G_B6_1 = 0;
	StringU5BU5D_t1281789340* G_B6_2 = NULL;
	StringU5BU5D_t1281789340* G_B6_3 = NULL;
	{
		StringU5BU5D_t1281789340* L_0 = (StringU5BU5D_t1281789340*)((StringU5BU5D_t1281789340*)SZArrayNew(StringU5BU5D_t1281789340_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_0);
		ArrayElementTypeCheck (L_0, _stringLiteral3452614645);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral3452614645);
		StringU5BU5D_t1281789340* L_1 = (StringU5BU5D_t1281789340*)L_0;
		int32_t L_2 = KeyValuePair_2_get_Key_m2701016533((KeyValuePair_2_t625878672 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		G_B1_0 = 1;
		G_B1_1 = L_1;
		G_B1_2 = L_1;
	}
	{
		int32_t L_3 = KeyValuePair_2_get_Key_m2701016533((KeyValuePair_2_t625878672 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		V_0 = (int32_t)L_3;
		String_t* L_4 = Int32_ToString_m141394615((int32_t*)(&V_0), /*hidden argument*/NULL);
		G_B3_0 = L_4;
		G_B3_1 = G_B1_0;
		G_B3_2 = G_B1_1;
		G_B3_3 = G_B1_2;
		goto IL_003e;
	}

IL_0039:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B3_0 = L_5;
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
		G_B3_3 = G_B2_2;
	}

IL_003e:
	{
		NullCheck(G_B3_2);
		ArrayElementTypeCheck (G_B3_2, G_B3_0);
		(G_B3_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B3_1), (String_t*)G_B3_0);
		StringU5BU5D_t1281789340* L_6 = (StringU5BU5D_t1281789340*)G_B3_3;
		NullCheck(L_6);
		ArrayElementTypeCheck (L_6, _stringLiteral3450517380);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral3450517380);
		StringU5BU5D_t1281789340* L_7 = (StringU5BU5D_t1281789340*)L_6;
		Il2CppChar L_8 = KeyValuePair_2_get_Value_m4183788833((KeyValuePair_2_t625878672 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		G_B4_0 = 3;
		G_B4_1 = L_7;
		G_B4_2 = L_7;
	}
	{
		Il2CppChar L_9 = KeyValuePair_2_get_Value_m4183788833((KeyValuePair_2_t625878672 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		V_1 = (Il2CppChar)L_9;
		String_t* L_10 = Char_ToString_m3588025615((Il2CppChar*)(&V_1), /*hidden argument*/NULL);
		G_B6_0 = L_10;
		G_B6_1 = G_B4_0;
		G_B6_2 = G_B4_1;
		G_B6_3 = G_B4_2;
		goto IL_0077;
	}

IL_0072:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_11 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B6_0 = L_11;
		G_B6_1 = G_B5_0;
		G_B6_2 = G_B5_1;
		G_B6_3 = G_B5_2;
	}

IL_0077:
	{
		NullCheck(G_B6_2);
		ArrayElementTypeCheck (G_B6_2, G_B6_0);
		(G_B6_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B6_1), (String_t*)G_B6_0);
		StringU5BU5D_t1281789340* L_12 = (StringU5BU5D_t1281789340*)G_B6_3;
		NullCheck(L_12);
		ArrayElementTypeCheck (L_12, _stringLiteral3452614643);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)_stringLiteral3452614643);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_13 = String_Concat_m1809518182(NULL /*static, unused*/, (StringU5BU5D_t1281789340*)L_12, /*hidden argument*/NULL);
		return L_13;
	}
}
extern "C"  String_t* KeyValuePair_2_ToString_m823870646_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t625878672 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t625878672 *>(__this + 1);
	return KeyValuePair_2_ToString_m823870646(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m2493299567_gshared (KeyValuePair_2_t4237331251 * __this, int32_t ___key0, int32_t ___value1, const MethodInfo* method)
{
	{
		int32_t L_0 = ___key0;
		KeyValuePair_2_set_Key_m1660334707((KeyValuePair_2_t4237331251 *)__this, (int32_t)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_1 = ___value1;
		KeyValuePair_2_set_Value_m2314909969((KeyValuePair_2_t4237331251 *)__this, (int32_t)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return;
	}
}
extern "C"  void KeyValuePair_2__ctor_m2493299567_AdjustorThunk (Il2CppObject * __this, int32_t ___key0, int32_t ___value1, const MethodInfo* method)
{
	KeyValuePair_2_t4237331251 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t4237331251 *>(__this + 1);
	KeyValuePair_2__ctor_m2493299567(_thisAdjusted, ___key0, ___value1, method);
}
// TKey System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>::get_Key()
extern "C"  int32_t KeyValuePair_2_get_Key_m2288232422_gshared (KeyValuePair_2_t4237331251 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (int32_t)__this->get_key_0();
		return L_0;
	}
}
extern "C"  int32_t KeyValuePair_2_get_Key_m2288232422_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t4237331251 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t4237331251 *>(__this + 1);
	return KeyValuePair_2_get_Key_m2288232422(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>::set_Key(TKey)
extern "C"  void KeyValuePair_2_set_Key_m1660334707_gshared (KeyValuePair_2_t4237331251 * __this, int32_t ___value0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_key_0(L_0);
		return;
	}
}
extern "C"  void KeyValuePair_2_set_Key_m1660334707_AdjustorThunk (Il2CppObject * __this, int32_t ___value0, const MethodInfo* method)
{
	KeyValuePair_2_t4237331251 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t4237331251 *>(__this + 1);
	KeyValuePair_2_set_Key_m1660334707(_thisAdjusted, ___value0, method);
}
// TValue System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>::get_Value()
extern "C"  int32_t KeyValuePair_2_get_Value_m3536231958_gshared (KeyValuePair_2_t4237331251 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (int32_t)__this->get_value_1();
		return L_0;
	}
}
extern "C"  int32_t KeyValuePair_2_get_Value_m3536231958_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t4237331251 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t4237331251 *>(__this + 1);
	return KeyValuePair_2_get_Value_m3536231958(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>::set_Value(TValue)
extern "C"  void KeyValuePair_2_set_Value_m2314909969_gshared (KeyValuePair_2_t4237331251 * __this, int32_t ___value0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_value_1(L_0);
		return;
	}
}
extern "C"  void KeyValuePair_2_set_Value_m2314909969_AdjustorThunk (Il2CppObject * __this, int32_t ___value0, const MethodInfo* method)
{
	KeyValuePair_2_t4237331251 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t4237331251 *>(__this + 1);
	KeyValuePair_2_set_Value_m2314909969(_thisAdjusted, ___value0, method);
}
// System.String System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>::ToString()
extern "C"  String_t* KeyValuePair_2_ToString_m65692449_gshared (KeyValuePair_2_t4237331251 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KeyValuePair_2_ToString_m65692449_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t G_B2_0 = 0;
	StringU5BU5D_t1281789340* G_B2_1 = NULL;
	StringU5BU5D_t1281789340* G_B2_2 = NULL;
	int32_t G_B1_0 = 0;
	StringU5BU5D_t1281789340* G_B1_1 = NULL;
	StringU5BU5D_t1281789340* G_B1_2 = NULL;
	String_t* G_B3_0 = NULL;
	int32_t G_B3_1 = 0;
	StringU5BU5D_t1281789340* G_B3_2 = NULL;
	StringU5BU5D_t1281789340* G_B3_3 = NULL;
	int32_t G_B5_0 = 0;
	StringU5BU5D_t1281789340* G_B5_1 = NULL;
	StringU5BU5D_t1281789340* G_B5_2 = NULL;
	int32_t G_B4_0 = 0;
	StringU5BU5D_t1281789340* G_B4_1 = NULL;
	StringU5BU5D_t1281789340* G_B4_2 = NULL;
	String_t* G_B6_0 = NULL;
	int32_t G_B6_1 = 0;
	StringU5BU5D_t1281789340* G_B6_2 = NULL;
	StringU5BU5D_t1281789340* G_B6_3 = NULL;
	{
		StringU5BU5D_t1281789340* L_0 = (StringU5BU5D_t1281789340*)((StringU5BU5D_t1281789340*)SZArrayNew(StringU5BU5D_t1281789340_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_0);
		ArrayElementTypeCheck (L_0, _stringLiteral3452614645);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral3452614645);
		StringU5BU5D_t1281789340* L_1 = (StringU5BU5D_t1281789340*)L_0;
		int32_t L_2 = KeyValuePair_2_get_Key_m2288232422((KeyValuePair_2_t4237331251 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		G_B1_0 = 1;
		G_B1_1 = L_1;
		G_B1_2 = L_1;
	}
	{
		int32_t L_3 = KeyValuePair_2_get_Key_m2288232422((KeyValuePair_2_t4237331251 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		V_0 = (int32_t)L_3;
		String_t* L_4 = Int32_ToString_m141394615((int32_t*)(&V_0), /*hidden argument*/NULL);
		G_B3_0 = L_4;
		G_B3_1 = G_B1_0;
		G_B3_2 = G_B1_1;
		G_B3_3 = G_B1_2;
		goto IL_003e;
	}

IL_0039:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B3_0 = L_5;
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
		G_B3_3 = G_B2_2;
	}

IL_003e:
	{
		NullCheck(G_B3_2);
		ArrayElementTypeCheck (G_B3_2, G_B3_0);
		(G_B3_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B3_1), (String_t*)G_B3_0);
		StringU5BU5D_t1281789340* L_6 = (StringU5BU5D_t1281789340*)G_B3_3;
		NullCheck(L_6);
		ArrayElementTypeCheck (L_6, _stringLiteral3450517380);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral3450517380);
		StringU5BU5D_t1281789340* L_7 = (StringU5BU5D_t1281789340*)L_6;
		int32_t L_8 = KeyValuePair_2_get_Value_m3536231958((KeyValuePair_2_t4237331251 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		G_B4_0 = 3;
		G_B4_1 = L_7;
		G_B4_2 = L_7;
	}
	{
		int32_t L_9 = KeyValuePair_2_get_Value_m3536231958((KeyValuePair_2_t4237331251 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		V_1 = (int32_t)L_9;
		String_t* L_10 = Int32_ToString_m141394615((int32_t*)(&V_1), /*hidden argument*/NULL);
		G_B6_0 = L_10;
		G_B6_1 = G_B4_0;
		G_B6_2 = G_B4_1;
		G_B6_3 = G_B4_2;
		goto IL_0077;
	}

IL_0072:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_11 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B6_0 = L_11;
		G_B6_1 = G_B5_0;
		G_B6_2 = G_B5_1;
		G_B6_3 = G_B5_2;
	}

IL_0077:
	{
		NullCheck(G_B6_2);
		ArrayElementTypeCheck (G_B6_2, G_B6_0);
		(G_B6_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B6_1), (String_t*)G_B6_0);
		StringU5BU5D_t1281789340* L_12 = (StringU5BU5D_t1281789340*)G_B6_3;
		NullCheck(L_12);
		ArrayElementTypeCheck (L_12, _stringLiteral3452614643);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)_stringLiteral3452614643);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_13 = String_Concat_m1809518182(NULL /*static, unused*/, (StringU5BU5D_t1281789340*)L_12, /*hidden argument*/NULL);
		return L_13;
	}
}
extern "C"  String_t* KeyValuePair_2_ToString_m65692449_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t4237331251 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t4237331251 *>(__this + 1);
	return KeyValuePair_2_ToString_m65692449(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int64>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m844205450_gshared (KeyValuePair_2_t727985506 * __this, int32_t ___key0, int64_t ___value1, const MethodInfo* method)
{
	{
		int32_t L_0 = ___key0;
		KeyValuePair_2_set_Key_m975493760((KeyValuePair_2_t727985506 *)__this, (int32_t)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int64_t L_1 = ___value1;
		KeyValuePair_2_set_Value_m596466564((KeyValuePair_2_t727985506 *)__this, (int64_t)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return;
	}
}
extern "C"  void KeyValuePair_2__ctor_m844205450_AdjustorThunk (Il2CppObject * __this, int32_t ___key0, int64_t ___value1, const MethodInfo* method)
{
	KeyValuePair_2_t727985506 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t727985506 *>(__this + 1);
	KeyValuePair_2__ctor_m844205450(_thisAdjusted, ___key0, ___value1, method);
}
// TKey System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int64>::get_Key()
extern "C"  int32_t KeyValuePair_2_get_Key_m1737874887_gshared (KeyValuePair_2_t727985506 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (int32_t)__this->get_key_0();
		return L_0;
	}
}
extern "C"  int32_t KeyValuePair_2_get_Key_m1737874887_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t727985506 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t727985506 *>(__this + 1);
	return KeyValuePair_2_get_Key_m1737874887(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int64>::set_Key(TKey)
extern "C"  void KeyValuePair_2_set_Key_m975493760_gshared (KeyValuePair_2_t727985506 * __this, int32_t ___value0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_key_0(L_0);
		return;
	}
}
extern "C"  void KeyValuePair_2_set_Key_m975493760_AdjustorThunk (Il2CppObject * __this, int32_t ___value0, const MethodInfo* method)
{
	KeyValuePair_2_t727985506 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t727985506 *>(__this + 1);
	KeyValuePair_2_set_Key_m975493760(_thisAdjusted, ___value0, method);
}
// TValue System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int64>::get_Value()
extern "C"  int64_t KeyValuePair_2_get_Value_m3934599775_gshared (KeyValuePair_2_t727985506 * __this, const MethodInfo* method)
{
	{
		int64_t L_0 = (int64_t)__this->get_value_1();
		return L_0;
	}
}
extern "C"  int64_t KeyValuePair_2_get_Value_m3934599775_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t727985506 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t727985506 *>(__this + 1);
	return KeyValuePair_2_get_Value_m3934599775(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int64>::set_Value(TValue)
extern "C"  void KeyValuePair_2_set_Value_m596466564_gshared (KeyValuePair_2_t727985506 * __this, int64_t ___value0, const MethodInfo* method)
{
	{
		int64_t L_0 = ___value0;
		__this->set_value_1(L_0);
		return;
	}
}
extern "C"  void KeyValuePair_2_set_Value_m596466564_AdjustorThunk (Il2CppObject * __this, int64_t ___value0, const MethodInfo* method)
{
	KeyValuePair_2_t727985506 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t727985506 *>(__this + 1);
	KeyValuePair_2_set_Value_m596466564(_thisAdjusted, ___value0, method);
}
// System.String System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int64>::ToString()
extern "C"  String_t* KeyValuePair_2_ToString_m4221841369_gshared (KeyValuePair_2_t727985506 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KeyValuePair_2_ToString_m4221841369_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int64_t V_1 = 0;
	int32_t G_B2_0 = 0;
	StringU5BU5D_t1281789340* G_B2_1 = NULL;
	StringU5BU5D_t1281789340* G_B2_2 = NULL;
	int32_t G_B1_0 = 0;
	StringU5BU5D_t1281789340* G_B1_1 = NULL;
	StringU5BU5D_t1281789340* G_B1_2 = NULL;
	String_t* G_B3_0 = NULL;
	int32_t G_B3_1 = 0;
	StringU5BU5D_t1281789340* G_B3_2 = NULL;
	StringU5BU5D_t1281789340* G_B3_3 = NULL;
	int32_t G_B5_0 = 0;
	StringU5BU5D_t1281789340* G_B5_1 = NULL;
	StringU5BU5D_t1281789340* G_B5_2 = NULL;
	int32_t G_B4_0 = 0;
	StringU5BU5D_t1281789340* G_B4_1 = NULL;
	StringU5BU5D_t1281789340* G_B4_2 = NULL;
	String_t* G_B6_0 = NULL;
	int32_t G_B6_1 = 0;
	StringU5BU5D_t1281789340* G_B6_2 = NULL;
	StringU5BU5D_t1281789340* G_B6_3 = NULL;
	{
		StringU5BU5D_t1281789340* L_0 = (StringU5BU5D_t1281789340*)((StringU5BU5D_t1281789340*)SZArrayNew(StringU5BU5D_t1281789340_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_0);
		ArrayElementTypeCheck (L_0, _stringLiteral3452614645);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral3452614645);
		StringU5BU5D_t1281789340* L_1 = (StringU5BU5D_t1281789340*)L_0;
		int32_t L_2 = KeyValuePair_2_get_Key_m1737874887((KeyValuePair_2_t727985506 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		G_B1_0 = 1;
		G_B1_1 = L_1;
		G_B1_2 = L_1;
	}
	{
		int32_t L_3 = KeyValuePair_2_get_Key_m1737874887((KeyValuePair_2_t727985506 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		V_0 = (int32_t)L_3;
		String_t* L_4 = Int32_ToString_m141394615((int32_t*)(&V_0), /*hidden argument*/NULL);
		G_B3_0 = L_4;
		G_B3_1 = G_B1_0;
		G_B3_2 = G_B1_1;
		G_B3_3 = G_B1_2;
		goto IL_003e;
	}

IL_0039:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B3_0 = L_5;
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
		G_B3_3 = G_B2_2;
	}

IL_003e:
	{
		NullCheck(G_B3_2);
		ArrayElementTypeCheck (G_B3_2, G_B3_0);
		(G_B3_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B3_1), (String_t*)G_B3_0);
		StringU5BU5D_t1281789340* L_6 = (StringU5BU5D_t1281789340*)G_B3_3;
		NullCheck(L_6);
		ArrayElementTypeCheck (L_6, _stringLiteral3450517380);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral3450517380);
		StringU5BU5D_t1281789340* L_7 = (StringU5BU5D_t1281789340*)L_6;
		int64_t L_8 = KeyValuePair_2_get_Value_m3934599775((KeyValuePair_2_t727985506 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		G_B4_0 = 3;
		G_B4_1 = L_7;
		G_B4_2 = L_7;
	}
	{
		int64_t L_9 = KeyValuePair_2_get_Value_m3934599775((KeyValuePair_2_t727985506 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		V_1 = (int64_t)L_9;
		String_t* L_10 = Int64_ToString_m2986581816((int64_t*)(&V_1), /*hidden argument*/NULL);
		G_B6_0 = L_10;
		G_B6_1 = G_B4_0;
		G_B6_2 = G_B4_1;
		G_B6_3 = G_B4_2;
		goto IL_0077;
	}

IL_0072:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_11 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B6_0 = L_11;
		G_B6_1 = G_B5_0;
		G_B6_2 = G_B5_1;
		G_B6_3 = G_B5_2;
	}

IL_0077:
	{
		NullCheck(G_B6_2);
		ArrayElementTypeCheck (G_B6_2, G_B6_0);
		(G_B6_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B6_1), (String_t*)G_B6_0);
		StringU5BU5D_t1281789340* L_12 = (StringU5BU5D_t1281789340*)G_B6_3;
		NullCheck(L_12);
		ArrayElementTypeCheck (L_12, _stringLiteral3452614643);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)_stringLiteral3452614643);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_13 = String_Concat_m1809518182(NULL /*static, unused*/, (StringU5BU5D_t1281789340*)L_12, /*hidden argument*/NULL);
		return L_13;
	}
}
extern "C"  String_t* KeyValuePair_2_ToString_m4221841369_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t727985506 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t727985506 *>(__this + 1);
	return KeyValuePair_2_ToString_m4221841369(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m2118224448_gshared (KeyValuePair_2_t71524366 * __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method)
{
	{
		int32_t L_0 = ___key0;
		KeyValuePair_2_set_Key_m2121548577((KeyValuePair_2_t71524366 *)__this, (int32_t)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Il2CppObject * L_1 = ___value1;
		KeyValuePair_2_set_Value_m3358607572((KeyValuePair_2_t71524366 *)__this, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return;
	}
}
extern "C"  void KeyValuePair_2__ctor_m2118224448_AdjustorThunk (Il2CppObject * __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method)
{
	KeyValuePair_2_t71524366 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t71524366 *>(__this + 1);
	KeyValuePair_2__ctor_m2118224448(_thisAdjusted, ___key0, ___value1, method);
}
// TKey System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>::get_Key()
extern "C"  int32_t KeyValuePair_2_get_Key_m1839753989_gshared (KeyValuePair_2_t71524366 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (int32_t)__this->get_key_0();
		return L_0;
	}
}
extern "C"  int32_t KeyValuePair_2_get_Key_m1839753989_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t71524366 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t71524366 *>(__this + 1);
	return KeyValuePair_2_get_Key_m1839753989(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>::set_Key(TKey)
extern "C"  void KeyValuePair_2_set_Key_m2121548577_gshared (KeyValuePair_2_t71524366 * __this, int32_t ___value0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_key_0(L_0);
		return;
	}
}
extern "C"  void KeyValuePair_2_set_Key_m2121548577_AdjustorThunk (Il2CppObject * __this, int32_t ___value0, const MethodInfo* method)
{
	KeyValuePair_2_t71524366 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t71524366 *>(__this + 1);
	KeyValuePair_2_set_Key_m2121548577(_thisAdjusted, ___value0, method);
}
// TValue System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>::get_Value()
extern "C"  Il2CppObject * KeyValuePair_2_get_Value_m3495598764_gshared (KeyValuePair_2_t71524366 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_value_1();
		return L_0;
	}
}
extern "C"  Il2CppObject * KeyValuePair_2_get_Value_m3495598764_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t71524366 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t71524366 *>(__this + 1);
	return KeyValuePair_2_get_Value_m3495598764(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>::set_Value(TValue)
extern "C"  void KeyValuePair_2_set_Value_m3358607572_gshared (KeyValuePair_2_t71524366 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___value0;
		__this->set_value_1(L_0);
		return;
	}
}
extern "C"  void KeyValuePair_2_set_Value_m3358607572_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	KeyValuePair_2_t71524366 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t71524366 *>(__this + 1);
	KeyValuePair_2_set_Value_m3358607572(_thisAdjusted, ___value0, method);
}
// System.String System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>::ToString()
extern "C"  String_t* KeyValuePair_2_ToString_m1238786018_gshared (KeyValuePair_2_t71524366 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KeyValuePair_2_ToString_m1238786018_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	Il2CppObject * V_1 = NULL;
	int32_t G_B2_0 = 0;
	StringU5BU5D_t1281789340* G_B2_1 = NULL;
	StringU5BU5D_t1281789340* G_B2_2 = NULL;
	int32_t G_B1_0 = 0;
	StringU5BU5D_t1281789340* G_B1_1 = NULL;
	StringU5BU5D_t1281789340* G_B1_2 = NULL;
	String_t* G_B3_0 = NULL;
	int32_t G_B3_1 = 0;
	StringU5BU5D_t1281789340* G_B3_2 = NULL;
	StringU5BU5D_t1281789340* G_B3_3 = NULL;
	int32_t G_B5_0 = 0;
	StringU5BU5D_t1281789340* G_B5_1 = NULL;
	StringU5BU5D_t1281789340* G_B5_2 = NULL;
	int32_t G_B4_0 = 0;
	StringU5BU5D_t1281789340* G_B4_1 = NULL;
	StringU5BU5D_t1281789340* G_B4_2 = NULL;
	String_t* G_B6_0 = NULL;
	int32_t G_B6_1 = 0;
	StringU5BU5D_t1281789340* G_B6_2 = NULL;
	StringU5BU5D_t1281789340* G_B6_3 = NULL;
	{
		StringU5BU5D_t1281789340* L_0 = (StringU5BU5D_t1281789340*)((StringU5BU5D_t1281789340*)SZArrayNew(StringU5BU5D_t1281789340_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_0);
		ArrayElementTypeCheck (L_0, _stringLiteral3452614645);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral3452614645);
		StringU5BU5D_t1281789340* L_1 = (StringU5BU5D_t1281789340*)L_0;
		int32_t L_2 = KeyValuePair_2_get_Key_m1839753989((KeyValuePair_2_t71524366 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		G_B1_0 = 1;
		G_B1_1 = L_1;
		G_B1_2 = L_1;
	}
	{
		int32_t L_3 = KeyValuePair_2_get_Key_m1839753989((KeyValuePair_2_t71524366 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		V_0 = (int32_t)L_3;
		String_t* L_4 = Int32_ToString_m141394615((int32_t*)(&V_0), /*hidden argument*/NULL);
		G_B3_0 = L_4;
		G_B3_1 = G_B1_0;
		G_B3_2 = G_B1_1;
		G_B3_3 = G_B1_2;
		goto IL_003e;
	}

IL_0039:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B3_0 = L_5;
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
		G_B3_3 = G_B2_2;
	}

IL_003e:
	{
		NullCheck(G_B3_2);
		ArrayElementTypeCheck (G_B3_2, G_B3_0);
		(G_B3_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B3_1), (String_t*)G_B3_0);
		StringU5BU5D_t1281789340* L_6 = (StringU5BU5D_t1281789340*)G_B3_3;
		NullCheck(L_6);
		ArrayElementTypeCheck (L_6, _stringLiteral3450517380);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral3450517380);
		StringU5BU5D_t1281789340* L_7 = (StringU5BU5D_t1281789340*)L_6;
		Il2CppObject * L_8 = KeyValuePair_2_get_Value_m3495598764((KeyValuePair_2_t71524366 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		G_B4_0 = 3;
		G_B4_1 = L_7;
		G_B4_2 = L_7;
		if (!L_8)
		{
			G_B5_0 = 3;
			G_B5_1 = L_7;
			G_B5_2 = L_7;
			goto IL_0072;
		}
	}
	{
		Il2CppObject * L_9 = KeyValuePair_2_get_Value_m3495598764((KeyValuePair_2_t71524366 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		V_1 = (Il2CppObject *)L_9;
		NullCheck((Il2CppObject *)(*(&V_1)));
		String_t* L_10 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (Il2CppObject *)(*(&V_1)));
		G_B6_0 = L_10;
		G_B6_1 = G_B4_0;
		G_B6_2 = G_B4_1;
		G_B6_3 = G_B4_2;
		goto IL_0077;
	}

IL_0072:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_11 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B6_0 = L_11;
		G_B6_1 = G_B5_0;
		G_B6_2 = G_B5_1;
		G_B6_3 = G_B5_2;
	}

IL_0077:
	{
		NullCheck(G_B6_2);
		ArrayElementTypeCheck (G_B6_2, G_B6_0);
		(G_B6_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B6_1), (String_t*)G_B6_0);
		StringU5BU5D_t1281789340* L_12 = (StringU5BU5D_t1281789340*)G_B6_3;
		NullCheck(L_12);
		ArrayElementTypeCheck (L_12, _stringLiteral3452614643);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)_stringLiteral3452614643);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_13 = String_Concat_m1809518182(NULL /*static, unused*/, (StringU5BU5D_t1281789340*)L_12, /*hidden argument*/NULL);
		return L_13;
	}
}
extern "C"  String_t* KeyValuePair_2_ToString_m1238786018_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t71524366 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t71524366 *>(__this + 1);
	return KeyValuePair_2_ToString_m1238786018(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int64,System.Object>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m2056996813_gshared (KeyValuePair_2_t2245450819 * __this, int64_t ___key0, Il2CppObject * ___value1, const MethodInfo* method)
{
	{
		int64_t L_0 = ___key0;
		KeyValuePair_2_set_Key_m1382769311((KeyValuePair_2_t2245450819 *)__this, (int64_t)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Il2CppObject * L_1 = ___value1;
		KeyValuePair_2_set_Value_m1316464643((KeyValuePair_2_t2245450819 *)__this, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return;
	}
}
extern "C"  void KeyValuePair_2__ctor_m2056996813_AdjustorThunk (Il2CppObject * __this, int64_t ___key0, Il2CppObject * ___value1, const MethodInfo* method)
{
	KeyValuePair_2_t2245450819 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t2245450819 *>(__this + 1);
	KeyValuePair_2__ctor_m2056996813(_thisAdjusted, ___key0, ___value1, method);
}
// TKey System.Collections.Generic.KeyValuePair`2<System.Int64,System.Object>::get_Key()
extern "C"  int64_t KeyValuePair_2_get_Key_m635992374_gshared (KeyValuePair_2_t2245450819 * __this, const MethodInfo* method)
{
	{
		int64_t L_0 = (int64_t)__this->get_key_0();
		return L_0;
	}
}
extern "C"  int64_t KeyValuePair_2_get_Key_m635992374_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t2245450819 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t2245450819 *>(__this + 1);
	return KeyValuePair_2_get_Key_m635992374(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int64,System.Object>::set_Key(TKey)
extern "C"  void KeyValuePair_2_set_Key_m1382769311_gshared (KeyValuePair_2_t2245450819 * __this, int64_t ___value0, const MethodInfo* method)
{
	{
		int64_t L_0 = ___value0;
		__this->set_key_0(L_0);
		return;
	}
}
extern "C"  void KeyValuePair_2_set_Key_m1382769311_AdjustorThunk (Il2CppObject * __this, int64_t ___value0, const MethodInfo* method)
{
	KeyValuePair_2_t2245450819 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t2245450819 *>(__this + 1);
	KeyValuePair_2_set_Key_m1382769311(_thisAdjusted, ___value0, method);
}
// TValue System.Collections.Generic.KeyValuePair`2<System.Int64,System.Object>::get_Value()
extern "C"  Il2CppObject * KeyValuePair_2_get_Value_m392474074_gshared (KeyValuePair_2_t2245450819 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_value_1();
		return L_0;
	}
}
extern "C"  Il2CppObject * KeyValuePair_2_get_Value_m392474074_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t2245450819 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t2245450819 *>(__this + 1);
	return KeyValuePair_2_get_Value_m392474074(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int64,System.Object>::set_Value(TValue)
extern "C"  void KeyValuePair_2_set_Value_m1316464643_gshared (KeyValuePair_2_t2245450819 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___value0;
		__this->set_value_1(L_0);
		return;
	}
}
extern "C"  void KeyValuePair_2_set_Value_m1316464643_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	KeyValuePair_2_t2245450819 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t2245450819 *>(__this + 1);
	KeyValuePair_2_set_Value_m1316464643(_thisAdjusted, ___value0, method);
}
// System.String System.Collections.Generic.KeyValuePair`2<System.Int64,System.Object>::ToString()
extern "C"  String_t* KeyValuePair_2_ToString_m3013572783_gshared (KeyValuePair_2_t2245450819 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KeyValuePair_2_ToString_m3013572783_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int64_t V_0 = 0;
	Il2CppObject * V_1 = NULL;
	int32_t G_B2_0 = 0;
	StringU5BU5D_t1281789340* G_B2_1 = NULL;
	StringU5BU5D_t1281789340* G_B2_2 = NULL;
	int32_t G_B1_0 = 0;
	StringU5BU5D_t1281789340* G_B1_1 = NULL;
	StringU5BU5D_t1281789340* G_B1_2 = NULL;
	String_t* G_B3_0 = NULL;
	int32_t G_B3_1 = 0;
	StringU5BU5D_t1281789340* G_B3_2 = NULL;
	StringU5BU5D_t1281789340* G_B3_3 = NULL;
	int32_t G_B5_0 = 0;
	StringU5BU5D_t1281789340* G_B5_1 = NULL;
	StringU5BU5D_t1281789340* G_B5_2 = NULL;
	int32_t G_B4_0 = 0;
	StringU5BU5D_t1281789340* G_B4_1 = NULL;
	StringU5BU5D_t1281789340* G_B4_2 = NULL;
	String_t* G_B6_0 = NULL;
	int32_t G_B6_1 = 0;
	StringU5BU5D_t1281789340* G_B6_2 = NULL;
	StringU5BU5D_t1281789340* G_B6_3 = NULL;
	{
		StringU5BU5D_t1281789340* L_0 = (StringU5BU5D_t1281789340*)((StringU5BU5D_t1281789340*)SZArrayNew(StringU5BU5D_t1281789340_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_0);
		ArrayElementTypeCheck (L_0, _stringLiteral3452614645);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral3452614645);
		StringU5BU5D_t1281789340* L_1 = (StringU5BU5D_t1281789340*)L_0;
		int64_t L_2 = KeyValuePair_2_get_Key_m635992374((KeyValuePair_2_t2245450819 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		G_B1_0 = 1;
		G_B1_1 = L_1;
		G_B1_2 = L_1;
	}
	{
		int64_t L_3 = KeyValuePair_2_get_Key_m635992374((KeyValuePair_2_t2245450819 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		V_0 = (int64_t)L_3;
		String_t* L_4 = Int64_ToString_m2986581816((int64_t*)(&V_0), /*hidden argument*/NULL);
		G_B3_0 = L_4;
		G_B3_1 = G_B1_0;
		G_B3_2 = G_B1_1;
		G_B3_3 = G_B1_2;
		goto IL_003e;
	}

IL_0039:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B3_0 = L_5;
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
		G_B3_3 = G_B2_2;
	}

IL_003e:
	{
		NullCheck(G_B3_2);
		ArrayElementTypeCheck (G_B3_2, G_B3_0);
		(G_B3_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B3_1), (String_t*)G_B3_0);
		StringU5BU5D_t1281789340* L_6 = (StringU5BU5D_t1281789340*)G_B3_3;
		NullCheck(L_6);
		ArrayElementTypeCheck (L_6, _stringLiteral3450517380);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral3450517380);
		StringU5BU5D_t1281789340* L_7 = (StringU5BU5D_t1281789340*)L_6;
		Il2CppObject * L_8 = KeyValuePair_2_get_Value_m392474074((KeyValuePair_2_t2245450819 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		G_B4_0 = 3;
		G_B4_1 = L_7;
		G_B4_2 = L_7;
		if (!L_8)
		{
			G_B5_0 = 3;
			G_B5_1 = L_7;
			G_B5_2 = L_7;
			goto IL_0072;
		}
	}
	{
		Il2CppObject * L_9 = KeyValuePair_2_get_Value_m392474074((KeyValuePair_2_t2245450819 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		V_1 = (Il2CppObject *)L_9;
		NullCheck((Il2CppObject *)(*(&V_1)));
		String_t* L_10 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (Il2CppObject *)(*(&V_1)));
		G_B6_0 = L_10;
		G_B6_1 = G_B4_0;
		G_B6_2 = G_B4_1;
		G_B6_3 = G_B4_2;
		goto IL_0077;
	}

IL_0072:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_11 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B6_0 = L_11;
		G_B6_1 = G_B5_0;
		G_B6_2 = G_B5_1;
		G_B6_3 = G_B5_2;
	}

IL_0077:
	{
		NullCheck(G_B6_2);
		ArrayElementTypeCheck (G_B6_2, G_B6_0);
		(G_B6_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B6_1), (String_t*)G_B6_0);
		StringU5BU5D_t1281789340* L_12 = (StringU5BU5D_t1281789340*)G_B6_3;
		NullCheck(L_12);
		ArrayElementTypeCheck (L_12, _stringLiteral3452614643);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)_stringLiteral3452614643);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_13 = String_Concat_m1809518182(NULL /*static, unused*/, (StringU5BU5D_t1281789340*)L_12, /*hidden argument*/NULL);
		return L_13;
	}
}
extern "C"  String_t* KeyValuePair_2_ToString_m3013572783_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t2245450819 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t2245450819 *>(__this + 1);
	return KeyValuePair_2_ToString_m3013572783(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m23191374_gshared (KeyValuePair_2_t3842366416 * __this, Il2CppObject * ___key0, bool ___value1, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___key0;
		KeyValuePair_2_set_Key_m2116817417((KeyValuePair_2_t3842366416 *)__this, (Il2CppObject *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		bool L_1 = ___value1;
		KeyValuePair_2_set_Value_m3305319569((KeyValuePair_2_t3842366416 *)__this, (bool)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return;
	}
}
extern "C"  void KeyValuePair_2__ctor_m23191374_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___key0, bool ___value1, const MethodInfo* method)
{
	KeyValuePair_2_t3842366416 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t3842366416 *>(__this + 1);
	KeyValuePair_2__ctor_m23191374(_thisAdjusted, ___key0, ___value1, method);
}
// TKey System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>::get_Key()
extern "C"  Il2CppObject * KeyValuePair_2_get_Key_m2106922848_gshared (KeyValuePair_2_t3842366416 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_key_0();
		return L_0;
	}
}
extern "C"  Il2CppObject * KeyValuePair_2_get_Key_m2106922848_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t3842366416 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t3842366416 *>(__this + 1);
	return KeyValuePair_2_get_Key_m2106922848(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>::set_Key(TKey)
extern "C"  void KeyValuePair_2_set_Key_m2116817417_gshared (KeyValuePair_2_t3842366416 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___value0;
		__this->set_key_0(L_0);
		return;
	}
}
extern "C"  void KeyValuePair_2_set_Key_m2116817417_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	KeyValuePair_2_t3842366416 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t3842366416 *>(__this + 1);
	KeyValuePair_2_set_Key_m2116817417(_thisAdjusted, ___value0, method);
}
// TValue System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>::get_Value()
extern "C"  bool KeyValuePair_2_get_Value_m1669764045_gshared (KeyValuePair_2_t3842366416 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)__this->get_value_1();
		return L_0;
	}
}
extern "C"  bool KeyValuePair_2_get_Value_m1669764045_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t3842366416 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t3842366416 *>(__this + 1);
	return KeyValuePair_2_get_Value_m1669764045(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>::set_Value(TValue)
extern "C"  void KeyValuePair_2_set_Value_m3305319569_gshared (KeyValuePair_2_t3842366416 * __this, bool ___value0, const MethodInfo* method)
{
	{
		bool L_0 = ___value0;
		__this->set_value_1(L_0);
		return;
	}
}
extern "C"  void KeyValuePair_2_set_Value_m3305319569_AdjustorThunk (Il2CppObject * __this, bool ___value0, const MethodInfo* method)
{
	KeyValuePair_2_t3842366416 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t3842366416 *>(__this + 1);
	KeyValuePair_2_set_Value_m3305319569(_thisAdjusted, ___value0, method);
}
// System.String System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>::ToString()
extern "C"  String_t* KeyValuePair_2_ToString_m2480962023_gshared (KeyValuePair_2_t3842366416 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KeyValuePair_2_ToString_m2480962023_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Il2CppObject * V_0 = NULL;
	bool V_1 = false;
	int32_t G_B2_0 = 0;
	StringU5BU5D_t1281789340* G_B2_1 = NULL;
	StringU5BU5D_t1281789340* G_B2_2 = NULL;
	int32_t G_B1_0 = 0;
	StringU5BU5D_t1281789340* G_B1_1 = NULL;
	StringU5BU5D_t1281789340* G_B1_2 = NULL;
	String_t* G_B3_0 = NULL;
	int32_t G_B3_1 = 0;
	StringU5BU5D_t1281789340* G_B3_2 = NULL;
	StringU5BU5D_t1281789340* G_B3_3 = NULL;
	int32_t G_B5_0 = 0;
	StringU5BU5D_t1281789340* G_B5_1 = NULL;
	StringU5BU5D_t1281789340* G_B5_2 = NULL;
	int32_t G_B4_0 = 0;
	StringU5BU5D_t1281789340* G_B4_1 = NULL;
	StringU5BU5D_t1281789340* G_B4_2 = NULL;
	String_t* G_B6_0 = NULL;
	int32_t G_B6_1 = 0;
	StringU5BU5D_t1281789340* G_B6_2 = NULL;
	StringU5BU5D_t1281789340* G_B6_3 = NULL;
	{
		StringU5BU5D_t1281789340* L_0 = (StringU5BU5D_t1281789340*)((StringU5BU5D_t1281789340*)SZArrayNew(StringU5BU5D_t1281789340_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_0);
		ArrayElementTypeCheck (L_0, _stringLiteral3452614645);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral3452614645);
		StringU5BU5D_t1281789340* L_1 = (StringU5BU5D_t1281789340*)L_0;
		Il2CppObject * L_2 = KeyValuePair_2_get_Key_m2106922848((KeyValuePair_2_t3842366416 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		G_B1_0 = 1;
		G_B1_1 = L_1;
		G_B1_2 = L_1;
		if (!L_2)
		{
			G_B2_0 = 1;
			G_B2_1 = L_1;
			G_B2_2 = L_1;
			goto IL_0039;
		}
	}
	{
		Il2CppObject * L_3 = KeyValuePair_2_get_Key_m2106922848((KeyValuePair_2_t3842366416 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		V_0 = (Il2CppObject *)L_3;
		NullCheck((Il2CppObject *)(*(&V_0)));
		String_t* L_4 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (Il2CppObject *)(*(&V_0)));
		G_B3_0 = L_4;
		G_B3_1 = G_B1_0;
		G_B3_2 = G_B1_1;
		G_B3_3 = G_B1_2;
		goto IL_003e;
	}

IL_0039:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B3_0 = L_5;
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
		G_B3_3 = G_B2_2;
	}

IL_003e:
	{
		NullCheck(G_B3_2);
		ArrayElementTypeCheck (G_B3_2, G_B3_0);
		(G_B3_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B3_1), (String_t*)G_B3_0);
		StringU5BU5D_t1281789340* L_6 = (StringU5BU5D_t1281789340*)G_B3_3;
		NullCheck(L_6);
		ArrayElementTypeCheck (L_6, _stringLiteral3450517380);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral3450517380);
		StringU5BU5D_t1281789340* L_7 = (StringU5BU5D_t1281789340*)L_6;
		bool L_8 = KeyValuePair_2_get_Value_m1669764045((KeyValuePair_2_t3842366416 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		G_B4_0 = 3;
		G_B4_1 = L_7;
		G_B4_2 = L_7;
	}
	{
		bool L_9 = KeyValuePair_2_get_Value_m1669764045((KeyValuePair_2_t3842366416 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		V_1 = (bool)L_9;
		String_t* L_10 = Boolean_ToString_m2664721875((bool*)(&V_1), /*hidden argument*/NULL);
		G_B6_0 = L_10;
		G_B6_1 = G_B4_0;
		G_B6_2 = G_B4_1;
		G_B6_3 = G_B4_2;
		goto IL_0077;
	}

IL_0072:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_11 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B6_0 = L_11;
		G_B6_1 = G_B5_0;
		G_B6_2 = G_B5_1;
		G_B6_3 = G_B5_2;
	}

IL_0077:
	{
		NullCheck(G_B6_2);
		ArrayElementTypeCheck (G_B6_2, G_B6_0);
		(G_B6_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B6_1), (String_t*)G_B6_0);
		StringU5BU5D_t1281789340* L_12 = (StringU5BU5D_t1281789340*)G_B6_3;
		NullCheck(L_12);
		ArrayElementTypeCheck (L_12, _stringLiteral3452614643);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)_stringLiteral3452614643);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_13 = String_Concat_m1809518182(NULL /*static, unused*/, (StringU5BU5D_t1281789340*)L_12, /*hidden argument*/NULL);
		return L_13;
	}
}
extern "C"  String_t* KeyValuePair_2_ToString_m2480962023_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t3842366416 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t3842366416 *>(__this + 1);
	return KeyValuePair_2_ToString_m2480962023(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m880186442_gshared (KeyValuePair_2_t2401056908 * __this, Il2CppObject * ___key0, int32_t ___value1, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___key0;
		KeyValuePair_2_set_Key_m4256290317((KeyValuePair_2_t2401056908 *)__this, (Il2CppObject *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_1 = ___value1;
		KeyValuePair_2_set_Value_m460969740((KeyValuePair_2_t2401056908 *)__this, (int32_t)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return;
	}
}
extern "C"  void KeyValuePair_2__ctor_m880186442_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___key0, int32_t ___value1, const MethodInfo* method)
{
	KeyValuePair_2_t2401056908 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t2401056908 *>(__this + 1);
	KeyValuePair_2__ctor_m880186442(_thisAdjusted, ___key0, ___value1, method);
}
// TKey System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>::get_Key()
extern "C"  Il2CppObject * KeyValuePair_2_get_Key_m1218836954_gshared (KeyValuePair_2_t2401056908 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_key_0();
		return L_0;
	}
}
extern "C"  Il2CppObject * KeyValuePair_2_get_Key_m1218836954_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t2401056908 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t2401056908 *>(__this + 1);
	return KeyValuePair_2_get_Key_m1218836954(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>::set_Key(TKey)
extern "C"  void KeyValuePair_2_set_Key_m4256290317_gshared (KeyValuePair_2_t2401056908 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___value0;
		__this->set_key_0(L_0);
		return;
	}
}
extern "C"  void KeyValuePair_2_set_Key_m4256290317_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	KeyValuePair_2_t2401056908 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t2401056908 *>(__this + 1);
	KeyValuePair_2_set_Key_m4256290317(_thisAdjusted, ___value0, method);
}
// TValue System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>::get_Value()
extern "C"  int32_t KeyValuePair_2_get_Value_m755756747_gshared (KeyValuePair_2_t2401056908 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (int32_t)__this->get_value_1();
		return L_0;
	}
}
extern "C"  int32_t KeyValuePair_2_get_Value_m755756747_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t2401056908 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t2401056908 *>(__this + 1);
	return KeyValuePair_2_get_Value_m755756747(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>::set_Value(TValue)
extern "C"  void KeyValuePair_2_set_Value_m460969740_gshared (KeyValuePair_2_t2401056908 * __this, int32_t ___value0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_value_1(L_0);
		return;
	}
}
extern "C"  void KeyValuePair_2_set_Value_m460969740_AdjustorThunk (Il2CppObject * __this, int32_t ___value0, const MethodInfo* method)
{
	KeyValuePair_2_t2401056908 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t2401056908 *>(__this + 1);
	KeyValuePair_2_set_Value_m460969740(_thisAdjusted, ___value0, method);
}
// System.String System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>::ToString()
extern "C"  String_t* KeyValuePair_2_ToString_m4231614106_gshared (KeyValuePair_2_t2401056908 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KeyValuePair_2_ToString_m4231614106_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Il2CppObject * V_0 = NULL;
	int32_t V_1 = 0;
	int32_t G_B2_0 = 0;
	StringU5BU5D_t1281789340* G_B2_1 = NULL;
	StringU5BU5D_t1281789340* G_B2_2 = NULL;
	int32_t G_B1_0 = 0;
	StringU5BU5D_t1281789340* G_B1_1 = NULL;
	StringU5BU5D_t1281789340* G_B1_2 = NULL;
	String_t* G_B3_0 = NULL;
	int32_t G_B3_1 = 0;
	StringU5BU5D_t1281789340* G_B3_2 = NULL;
	StringU5BU5D_t1281789340* G_B3_3 = NULL;
	int32_t G_B5_0 = 0;
	StringU5BU5D_t1281789340* G_B5_1 = NULL;
	StringU5BU5D_t1281789340* G_B5_2 = NULL;
	int32_t G_B4_0 = 0;
	StringU5BU5D_t1281789340* G_B4_1 = NULL;
	StringU5BU5D_t1281789340* G_B4_2 = NULL;
	String_t* G_B6_0 = NULL;
	int32_t G_B6_1 = 0;
	StringU5BU5D_t1281789340* G_B6_2 = NULL;
	StringU5BU5D_t1281789340* G_B6_3 = NULL;
	{
		StringU5BU5D_t1281789340* L_0 = (StringU5BU5D_t1281789340*)((StringU5BU5D_t1281789340*)SZArrayNew(StringU5BU5D_t1281789340_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_0);
		ArrayElementTypeCheck (L_0, _stringLiteral3452614645);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral3452614645);
		StringU5BU5D_t1281789340* L_1 = (StringU5BU5D_t1281789340*)L_0;
		Il2CppObject * L_2 = KeyValuePair_2_get_Key_m1218836954((KeyValuePair_2_t2401056908 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		G_B1_0 = 1;
		G_B1_1 = L_1;
		G_B1_2 = L_1;
		if (!L_2)
		{
			G_B2_0 = 1;
			G_B2_1 = L_1;
			G_B2_2 = L_1;
			goto IL_0039;
		}
	}
	{
		Il2CppObject * L_3 = KeyValuePair_2_get_Key_m1218836954((KeyValuePair_2_t2401056908 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		V_0 = (Il2CppObject *)L_3;
		NullCheck((Il2CppObject *)(*(&V_0)));
		String_t* L_4 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (Il2CppObject *)(*(&V_0)));
		G_B3_0 = L_4;
		G_B3_1 = G_B1_0;
		G_B3_2 = G_B1_1;
		G_B3_3 = G_B1_2;
		goto IL_003e;
	}

IL_0039:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B3_0 = L_5;
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
		G_B3_3 = G_B2_2;
	}

IL_003e:
	{
		NullCheck(G_B3_2);
		ArrayElementTypeCheck (G_B3_2, G_B3_0);
		(G_B3_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B3_1), (String_t*)G_B3_0);
		StringU5BU5D_t1281789340* L_6 = (StringU5BU5D_t1281789340*)G_B3_3;
		NullCheck(L_6);
		ArrayElementTypeCheck (L_6, _stringLiteral3450517380);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral3450517380);
		StringU5BU5D_t1281789340* L_7 = (StringU5BU5D_t1281789340*)L_6;
		int32_t L_8 = KeyValuePair_2_get_Value_m755756747((KeyValuePair_2_t2401056908 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		G_B4_0 = 3;
		G_B4_1 = L_7;
		G_B4_2 = L_7;
	}
	{
		int32_t L_9 = KeyValuePair_2_get_Value_m755756747((KeyValuePair_2_t2401056908 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		V_1 = (int32_t)L_9;
		String_t* L_10 = Int32_ToString_m141394615((int32_t*)(&V_1), /*hidden argument*/NULL);
		G_B6_0 = L_10;
		G_B6_1 = G_B4_0;
		G_B6_2 = G_B4_1;
		G_B6_3 = G_B4_2;
		goto IL_0077;
	}

IL_0072:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_11 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B6_0 = L_11;
		G_B6_1 = G_B5_0;
		G_B6_2 = G_B5_1;
		G_B6_3 = G_B5_2;
	}

IL_0077:
	{
		NullCheck(G_B6_2);
		ArrayElementTypeCheck (G_B6_2, G_B6_0);
		(G_B6_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B6_1), (String_t*)G_B6_0);
		StringU5BU5D_t1281789340* L_12 = (StringU5BU5D_t1281789340*)G_B6_3;
		NullCheck(L_12);
		ArrayElementTypeCheck (L_12, _stringLiteral3452614643);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)_stringLiteral3452614643);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_13 = String_Concat_m1809518182(NULL /*static, unused*/, (StringU5BU5D_t1281789340*)L_12, /*hidden argument*/NULL);
		return L_13;
	}
}
extern "C"  String_t* KeyValuePair_2_ToString_m4231614106_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t2401056908 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t2401056908 *>(__this + 1);
	return KeyValuePair_2_ToString_m4231614106(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m1794021352_gshared (KeyValuePair_2_t2530217319 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___key0;
		KeyValuePair_2_set_Key_m3170517671((KeyValuePair_2_t2530217319 *)__this, (Il2CppObject *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Il2CppObject * L_1 = ___value1;
		KeyValuePair_2_set_Value_m1153752644((KeyValuePair_2_t2530217319 *)__this, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return;
	}
}
extern "C"  void KeyValuePair_2__ctor_m1794021352_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method)
{
	KeyValuePair_2_t2530217319 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t2530217319 *>(__this + 1);
	KeyValuePair_2__ctor_m1794021352(_thisAdjusted, ___key0, ___value1, method);
}
// TKey System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>::get_Key()
extern "C"  Il2CppObject * KeyValuePair_2_get_Key_m1328507389_gshared (KeyValuePair_2_t2530217319 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_key_0();
		return L_0;
	}
}
extern "C"  Il2CppObject * KeyValuePair_2_get_Key_m1328507389_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t2530217319 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t2530217319 *>(__this + 1);
	return KeyValuePair_2_get_Key_m1328507389(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>::set_Key(TKey)
extern "C"  void KeyValuePair_2_set_Key_m3170517671_gshared (KeyValuePair_2_t2530217319 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___value0;
		__this->set_key_0(L_0);
		return;
	}
}
extern "C"  void KeyValuePair_2_set_Key_m3170517671_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	KeyValuePair_2_t2530217319 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t2530217319 *>(__this + 1);
	KeyValuePair_2_set_Key_m3170517671(_thisAdjusted, ___value0, method);
}
// TValue System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>::get_Value()
extern "C"  Il2CppObject * KeyValuePair_2_get_Value_m3464904234_gshared (KeyValuePair_2_t2530217319 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_value_1();
		return L_0;
	}
}
extern "C"  Il2CppObject * KeyValuePair_2_get_Value_m3464904234_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t2530217319 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t2530217319 *>(__this + 1);
	return KeyValuePair_2_get_Value_m3464904234(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>::set_Value(TValue)
extern "C"  void KeyValuePair_2_set_Value_m1153752644_gshared (KeyValuePair_2_t2530217319 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___value0;
		__this->set_value_1(L_0);
		return;
	}
}
extern "C"  void KeyValuePair_2_set_Value_m1153752644_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	KeyValuePair_2_t2530217319 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t2530217319 *>(__this + 1);
	KeyValuePair_2_set_Value_m1153752644(_thisAdjusted, ___value0, method);
}
// System.String System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>::ToString()
extern "C"  String_t* KeyValuePair_2_ToString_m510648957_gshared (KeyValuePair_2_t2530217319 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KeyValuePair_2_ToString_m510648957_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Il2CppObject * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	int32_t G_B2_0 = 0;
	StringU5BU5D_t1281789340* G_B2_1 = NULL;
	StringU5BU5D_t1281789340* G_B2_2 = NULL;
	int32_t G_B1_0 = 0;
	StringU5BU5D_t1281789340* G_B1_1 = NULL;
	StringU5BU5D_t1281789340* G_B1_2 = NULL;
	String_t* G_B3_0 = NULL;
	int32_t G_B3_1 = 0;
	StringU5BU5D_t1281789340* G_B3_2 = NULL;
	StringU5BU5D_t1281789340* G_B3_3 = NULL;
	int32_t G_B5_0 = 0;
	StringU5BU5D_t1281789340* G_B5_1 = NULL;
	StringU5BU5D_t1281789340* G_B5_2 = NULL;
	int32_t G_B4_0 = 0;
	StringU5BU5D_t1281789340* G_B4_1 = NULL;
	StringU5BU5D_t1281789340* G_B4_2 = NULL;
	String_t* G_B6_0 = NULL;
	int32_t G_B6_1 = 0;
	StringU5BU5D_t1281789340* G_B6_2 = NULL;
	StringU5BU5D_t1281789340* G_B6_3 = NULL;
	{
		StringU5BU5D_t1281789340* L_0 = (StringU5BU5D_t1281789340*)((StringU5BU5D_t1281789340*)SZArrayNew(StringU5BU5D_t1281789340_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_0);
		ArrayElementTypeCheck (L_0, _stringLiteral3452614645);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral3452614645);
		StringU5BU5D_t1281789340* L_1 = (StringU5BU5D_t1281789340*)L_0;
		Il2CppObject * L_2 = KeyValuePair_2_get_Key_m1328507389((KeyValuePair_2_t2530217319 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		G_B1_0 = 1;
		G_B1_1 = L_1;
		G_B1_2 = L_1;
		if (!L_2)
		{
			G_B2_0 = 1;
			G_B2_1 = L_1;
			G_B2_2 = L_1;
			goto IL_0039;
		}
	}
	{
		Il2CppObject * L_3 = KeyValuePair_2_get_Key_m1328507389((KeyValuePair_2_t2530217319 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		V_0 = (Il2CppObject *)L_3;
		NullCheck((Il2CppObject *)(*(&V_0)));
		String_t* L_4 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (Il2CppObject *)(*(&V_0)));
		G_B3_0 = L_4;
		G_B3_1 = G_B1_0;
		G_B3_2 = G_B1_1;
		G_B3_3 = G_B1_2;
		goto IL_003e;
	}

IL_0039:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B3_0 = L_5;
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
		G_B3_3 = G_B2_2;
	}

IL_003e:
	{
		NullCheck(G_B3_2);
		ArrayElementTypeCheck (G_B3_2, G_B3_0);
		(G_B3_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B3_1), (String_t*)G_B3_0);
		StringU5BU5D_t1281789340* L_6 = (StringU5BU5D_t1281789340*)G_B3_3;
		NullCheck(L_6);
		ArrayElementTypeCheck (L_6, _stringLiteral3450517380);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral3450517380);
		StringU5BU5D_t1281789340* L_7 = (StringU5BU5D_t1281789340*)L_6;
		Il2CppObject * L_8 = KeyValuePair_2_get_Value_m3464904234((KeyValuePair_2_t2530217319 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		G_B4_0 = 3;
		G_B4_1 = L_7;
		G_B4_2 = L_7;
		if (!L_8)
		{
			G_B5_0 = 3;
			G_B5_1 = L_7;
			G_B5_2 = L_7;
			goto IL_0072;
		}
	}
	{
		Il2CppObject * L_9 = KeyValuePair_2_get_Value_m3464904234((KeyValuePair_2_t2530217319 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		V_1 = (Il2CppObject *)L_9;
		NullCheck((Il2CppObject *)(*(&V_1)));
		String_t* L_10 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (Il2CppObject *)(*(&V_1)));
		G_B6_0 = L_10;
		G_B6_1 = G_B4_0;
		G_B6_2 = G_B4_1;
		G_B6_3 = G_B4_2;
		goto IL_0077;
	}

IL_0072:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_11 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B6_0 = L_11;
		G_B6_1 = G_B5_0;
		G_B6_2 = G_B5_1;
		G_B6_3 = G_B5_2;
	}

IL_0077:
	{
		NullCheck(G_B6_2);
		ArrayElementTypeCheck (G_B6_2, G_B6_0);
		(G_B6_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B6_1), (String_t*)G_B6_0);
		StringU5BU5D_t1281789340* L_12 = (StringU5BU5D_t1281789340*)G_B6_3;
		NullCheck(L_12);
		ArrayElementTypeCheck (L_12, _stringLiteral3452614643);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)_stringLiteral3452614643);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_13 = String_Concat_m1809518182(NULL /*static, unused*/, (StringU5BU5D_t1281789340*)L_12, /*hidden argument*/NULL);
		return L_13;
	}
}
extern "C"  String_t* KeyValuePair_2_ToString_m510648957_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t2530217319 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t2530217319 *>(__this + 1);
	return KeyValuePair_2_ToString_m510648957(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Single>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m3552061076_gshared (KeyValuePair_2_t847377929 * __this, Il2CppObject * ___key0, float ___value1, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___key0;
		KeyValuePair_2_set_Key_m2965168904((KeyValuePair_2_t847377929 *)__this, (Il2CppObject *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		float L_1 = ___value1;
		KeyValuePair_2_set_Value_m955705697((KeyValuePair_2_t847377929 *)__this, (float)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return;
	}
}
extern "C"  void KeyValuePair_2__ctor_m3552061076_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___key0, float ___value1, const MethodInfo* method)
{
	KeyValuePair_2_t847377929 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t847377929 *>(__this + 1);
	KeyValuePair_2__ctor_m3552061076(_thisAdjusted, ___key0, ___value1, method);
}
// TKey System.Collections.Generic.KeyValuePair`2<System.Object,System.Single>::get_Key()
extern "C"  Il2CppObject * KeyValuePair_2_get_Key_m3754157295_gshared (KeyValuePair_2_t847377929 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_key_0();
		return L_0;
	}
}
extern "C"  Il2CppObject * KeyValuePair_2_get_Key_m3754157295_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t847377929 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t847377929 *>(__this + 1);
	return KeyValuePair_2_get_Key_m3754157295(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Single>::set_Key(TKey)
extern "C"  void KeyValuePair_2_set_Key_m2965168904_gshared (KeyValuePair_2_t847377929 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___value0;
		__this->set_key_0(L_0);
		return;
	}
}
extern "C"  void KeyValuePair_2_set_Key_m2965168904_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	KeyValuePair_2_t847377929 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t847377929 *>(__this + 1);
	KeyValuePair_2_set_Key_m2965168904(_thisAdjusted, ___value0, method);
}
// TValue System.Collections.Generic.KeyValuePair`2<System.Object,System.Single>::get_Value()
extern "C"  float KeyValuePair_2_get_Value_m1635977167_gshared (KeyValuePair_2_t847377929 * __this, const MethodInfo* method)
{
	{
		float L_0 = (float)__this->get_value_1();
		return L_0;
	}
}
extern "C"  float KeyValuePair_2_get_Value_m1635977167_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t847377929 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t847377929 *>(__this + 1);
	return KeyValuePair_2_get_Value_m1635977167(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Single>::set_Value(TValue)
extern "C"  void KeyValuePair_2_set_Value_m955705697_gshared (KeyValuePair_2_t847377929 * __this, float ___value0, const MethodInfo* method)
{
	{
		float L_0 = ___value0;
		__this->set_value_1(L_0);
		return;
	}
}
extern "C"  void KeyValuePair_2_set_Value_m955705697_AdjustorThunk (Il2CppObject * __this, float ___value0, const MethodInfo* method)
{
	KeyValuePair_2_t847377929 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t847377929 *>(__this + 1);
	KeyValuePair_2_set_Value_m955705697(_thisAdjusted, ___value0, method);
}
// System.String System.Collections.Generic.KeyValuePair`2<System.Object,System.Single>::ToString()
extern "C"  String_t* KeyValuePair_2_ToString_m366204361_gshared (KeyValuePair_2_t847377929 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (KeyValuePair_2_ToString_m366204361_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Il2CppObject * V_0 = NULL;
	float V_1 = 0.0f;
	int32_t G_B2_0 = 0;
	StringU5BU5D_t1281789340* G_B2_1 = NULL;
	StringU5BU5D_t1281789340* G_B2_2 = NULL;
	int32_t G_B1_0 = 0;
	StringU5BU5D_t1281789340* G_B1_1 = NULL;
	StringU5BU5D_t1281789340* G_B1_2 = NULL;
	String_t* G_B3_0 = NULL;
	int32_t G_B3_1 = 0;
	StringU5BU5D_t1281789340* G_B3_2 = NULL;
	StringU5BU5D_t1281789340* G_B3_3 = NULL;
	int32_t G_B5_0 = 0;
	StringU5BU5D_t1281789340* G_B5_1 = NULL;
	StringU5BU5D_t1281789340* G_B5_2 = NULL;
	int32_t G_B4_0 = 0;
	StringU5BU5D_t1281789340* G_B4_1 = NULL;
	StringU5BU5D_t1281789340* G_B4_2 = NULL;
	String_t* G_B6_0 = NULL;
	int32_t G_B6_1 = 0;
	StringU5BU5D_t1281789340* G_B6_2 = NULL;
	StringU5BU5D_t1281789340* G_B6_3 = NULL;
	{
		StringU5BU5D_t1281789340* L_0 = (StringU5BU5D_t1281789340*)((StringU5BU5D_t1281789340*)SZArrayNew(StringU5BU5D_t1281789340_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_0);
		ArrayElementTypeCheck (L_0, _stringLiteral3452614645);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral3452614645);
		StringU5BU5D_t1281789340* L_1 = (StringU5BU5D_t1281789340*)L_0;
		Il2CppObject * L_2 = KeyValuePair_2_get_Key_m3754157295((KeyValuePair_2_t847377929 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		G_B1_0 = 1;
		G_B1_1 = L_1;
		G_B1_2 = L_1;
		if (!L_2)
		{
			G_B2_0 = 1;
			G_B2_1 = L_1;
			G_B2_2 = L_1;
			goto IL_0039;
		}
	}
	{
		Il2CppObject * L_3 = KeyValuePair_2_get_Key_m3754157295((KeyValuePair_2_t847377929 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		V_0 = (Il2CppObject *)L_3;
		NullCheck((Il2CppObject *)(*(&V_0)));
		String_t* L_4 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (Il2CppObject *)(*(&V_0)));
		G_B3_0 = L_4;
		G_B3_1 = G_B1_0;
		G_B3_2 = G_B1_1;
		G_B3_3 = G_B1_2;
		goto IL_003e;
	}

IL_0039:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B3_0 = L_5;
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
		G_B3_3 = G_B2_2;
	}

IL_003e:
	{
		NullCheck(G_B3_2);
		ArrayElementTypeCheck (G_B3_2, G_B3_0);
		(G_B3_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B3_1), (String_t*)G_B3_0);
		StringU5BU5D_t1281789340* L_6 = (StringU5BU5D_t1281789340*)G_B3_3;
		NullCheck(L_6);
		ArrayElementTypeCheck (L_6, _stringLiteral3450517380);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral3450517380);
		StringU5BU5D_t1281789340* L_7 = (StringU5BU5D_t1281789340*)L_6;
		float L_8 = KeyValuePair_2_get_Value_m1635977167((KeyValuePair_2_t847377929 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		G_B4_0 = 3;
		G_B4_1 = L_7;
		G_B4_2 = L_7;
	}
	{
		float L_9 = KeyValuePair_2_get_Value_m1635977167((KeyValuePair_2_t847377929 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		V_1 = (float)L_9;
		String_t* L_10 = Single_ToString_m3947131094((float*)(&V_1), /*hidden argument*/NULL);
		G_B6_0 = L_10;
		G_B6_1 = G_B4_0;
		G_B6_2 = G_B4_1;
		G_B6_3 = G_B4_2;
		goto IL_0077;
	}

IL_0072:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_11 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B6_0 = L_11;
		G_B6_1 = G_B5_0;
		G_B6_2 = G_B5_1;
		G_B6_3 = G_B5_2;
	}

IL_0077:
	{
		NullCheck(G_B6_2);
		ArrayElementTypeCheck (G_B6_2, G_B6_0);
		(G_B6_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B6_1), (String_t*)G_B6_0);
		StringU5BU5D_t1281789340* L_12 = (StringU5BU5D_t1281789340*)G_B6_3;
		NullCheck(L_12);
		ArrayElementTypeCheck (L_12, _stringLiteral3452614643);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)_stringLiteral3452614643);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_13 = String_Concat_m1809518182(NULL /*static, unused*/, (StringU5BU5D_t1281789340*)L_12, /*hidden argument*/NULL);
		return L_13;
	}
}
extern "C"  String_t* KeyValuePair_2_ToString_m366204361_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t847377929 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t847377929 *>(__this + 1);
	return KeyValuePair_2_ToString_m366204361(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.LinkedList`1/Enumerator<System.Object>::.ctor(System.Collections.Generic.LinkedList`1<T>)
extern "C"  void Enumerator__ctor_m2504641326_gshared (Enumerator_t139379724 * __this, LinkedList_1_t1919752173 * ___parent0, const MethodInfo* method)
{
	{
		LinkedList_1_t1919752173 * L_0 = ___parent0;
		__this->set_list_0(L_0);
		__this->set_current_1((LinkedListNode_1_t2825281267 *)NULL);
		__this->set_index_2((-1));
		LinkedList_1_t1919752173 * L_1 = ___parent0;
		NullCheck(L_1);
		uint32_t L_2 = (uint32_t)L_1->get_version_1();
		__this->set_version_3(L_2);
		return;
	}
}
extern "C"  void Enumerator__ctor_m2504641326_AdjustorThunk (Il2CppObject * __this, LinkedList_1_t1919752173 * ___parent0, const MethodInfo* method)
{
	Enumerator_t139379724 * _thisAdjusted = reinterpret_cast<Enumerator_t139379724 *>(__this + 1);
	Enumerator__ctor_m2504641326(_thisAdjusted, ___parent0, method);
}
// System.Object System.Collections.Generic.LinkedList`1/Enumerator<System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m3484854280_gshared (Enumerator_t139379724 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = Enumerator_get_Current_m2579293505((Enumerator_t139379724 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		return L_0;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m3484854280_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t139379724 * _thisAdjusted = reinterpret_cast<Enumerator_t139379724 *>(__this + 1);
	return Enumerator_System_Collections_IEnumerator_get_Current_m3484854280(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.LinkedList`1/Enumerator<System.Object>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m2550940120_gshared (Enumerator_t139379724 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_System_Collections_IEnumerator_Reset_m2550940120_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		LinkedList_1_t1919752173 * L_0 = (LinkedList_1_t1919752173 *)__this->get_list_0();
		if (L_0)
		{
			goto IL_0012;
		}
	}
	{
		ObjectDisposedException_t21392786 * L_1 = (ObjectDisposedException_t21392786 *)il2cpp_codegen_object_new(ObjectDisposedException_t21392786_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m3603759869(L_1, (String_t*)NULL, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0012:
	{
		uint32_t L_2 = (uint32_t)__this->get_version_3();
		LinkedList_1_t1919752173 * L_3 = (LinkedList_1_t1919752173 *)__this->get_list_0();
		NullCheck(L_3);
		uint32_t L_4 = (uint32_t)L_3->get_version_1();
		if ((((int32_t)L_2) == ((int32_t)L_4)))
		{
			goto IL_0033;
		}
	}
	{
		InvalidOperationException_t56020091 * L_5 = (InvalidOperationException_t56020091 *)il2cpp_codegen_object_new(InvalidOperationException_t56020091_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m237278729(L_5, (String_t*)_stringLiteral3902161842, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_0033:
	{
		__this->set_current_1((LinkedListNode_1_t2825281267 *)NULL);
		__this->set_index_2((-1));
		return;
	}
}
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m2550940120_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t139379724 * _thisAdjusted = reinterpret_cast<Enumerator_t139379724 *>(__this + 1);
	Enumerator_System_Collections_IEnumerator_Reset_m2550940120(_thisAdjusted, method);
}
// T System.Collections.Generic.LinkedList`1/Enumerator<System.Object>::get_Current()
extern "C"  Il2CppObject * Enumerator_get_Current_m2579293505_gshared (Enumerator_t139379724 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_get_Current_m2579293505_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		LinkedList_1_t1919752173 * L_0 = (LinkedList_1_t1919752173 *)__this->get_list_0();
		if (L_0)
		{
			goto IL_0012;
		}
	}
	{
		ObjectDisposedException_t21392786 * L_1 = (ObjectDisposedException_t21392786 *)il2cpp_codegen_object_new(ObjectDisposedException_t21392786_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m3603759869(L_1, (String_t*)NULL, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0012:
	{
		LinkedListNode_1_t2825281267 * L_2 = (LinkedListNode_1_t2825281267 *)__this->get_current_1();
		if (L_2)
		{
			goto IL_0023;
		}
	}
	{
		InvalidOperationException_t56020091 * L_3 = (InvalidOperationException_t56020091 *)il2cpp_codegen_object_new(InvalidOperationException_t56020091_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2734335978(L_3, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_0023:
	{
		LinkedListNode_1_t2825281267 * L_4 = (LinkedListNode_1_t2825281267 *)__this->get_current_1();
		NullCheck((LinkedListNode_1_t2825281267 *)L_4);
		Il2CppObject * L_5 = ((  Il2CppObject * (*) (LinkedListNode_1_t2825281267 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((LinkedListNode_1_t2825281267 *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_5;
	}
}
extern "C"  Il2CppObject * Enumerator_get_Current_m2579293505_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t139379724 * _thisAdjusted = reinterpret_cast<Enumerator_t139379724 *>(__this + 1);
	return Enumerator_get_Current_m2579293505(_thisAdjusted, method);
}
// System.Boolean System.Collections.Generic.LinkedList`1/Enumerator<System.Object>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m880141662_gshared (Enumerator_t139379724 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_MoveNext_m880141662_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		LinkedList_1_t1919752173 * L_0 = (LinkedList_1_t1919752173 *)__this->get_list_0();
		if (L_0)
		{
			goto IL_0012;
		}
	}
	{
		ObjectDisposedException_t21392786 * L_1 = (ObjectDisposedException_t21392786 *)il2cpp_codegen_object_new(ObjectDisposedException_t21392786_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m3603759869(L_1, (String_t*)NULL, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0012:
	{
		uint32_t L_2 = (uint32_t)__this->get_version_3();
		LinkedList_1_t1919752173 * L_3 = (LinkedList_1_t1919752173 *)__this->get_list_0();
		NullCheck(L_3);
		uint32_t L_4 = (uint32_t)L_3->get_version_1();
		if ((((int32_t)L_2) == ((int32_t)L_4)))
		{
			goto IL_0033;
		}
	}
	{
		InvalidOperationException_t56020091 * L_5 = (InvalidOperationException_t56020091 *)il2cpp_codegen_object_new(InvalidOperationException_t56020091_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m237278729(L_5, (String_t*)_stringLiteral3902161842, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_0033:
	{
		LinkedListNode_1_t2825281267 * L_6 = (LinkedListNode_1_t2825281267 *)__this->get_current_1();
		if (L_6)
		{
			goto IL_0054;
		}
	}
	{
		LinkedList_1_t1919752173 * L_7 = (LinkedList_1_t1919752173 *)__this->get_list_0();
		NullCheck(L_7);
		LinkedListNode_1_t2825281267 * L_8 = (LinkedListNode_1_t2825281267 *)L_7->get_first_3();
		__this->set_current_1(L_8);
		goto IL_0082;
	}

IL_0054:
	{
		LinkedListNode_1_t2825281267 * L_9 = (LinkedListNode_1_t2825281267 *)__this->get_current_1();
		NullCheck(L_9);
		LinkedListNode_1_t2825281267 * L_10 = (LinkedListNode_1_t2825281267 *)L_9->get_forward_2();
		__this->set_current_1(L_10);
		LinkedListNode_1_t2825281267 * L_11 = (LinkedListNode_1_t2825281267 *)__this->get_current_1();
		LinkedList_1_t1919752173 * L_12 = (LinkedList_1_t1919752173 *)__this->get_list_0();
		NullCheck(L_12);
		LinkedListNode_1_t2825281267 * L_13 = (LinkedListNode_1_t2825281267 *)L_12->get_first_3();
		if ((!(((Il2CppObject*)(LinkedListNode_1_t2825281267 *)L_11) == ((Il2CppObject*)(LinkedListNode_1_t2825281267 *)L_13))))
		{
			goto IL_0082;
		}
	}
	{
		__this->set_current_1((LinkedListNode_1_t2825281267 *)NULL);
	}

IL_0082:
	{
		LinkedListNode_1_t2825281267 * L_14 = (LinkedListNode_1_t2825281267 *)__this->get_current_1();
		if (L_14)
		{
			goto IL_0096;
		}
	}
	{
		__this->set_index_2((-1));
		return (bool)0;
	}

IL_0096:
	{
		int32_t L_15 = (int32_t)__this->get_index_2();
		__this->set_index_2(((int32_t)((int32_t)L_15+(int32_t)1)));
		return (bool)1;
	}
}
extern "C"  bool Enumerator_MoveNext_m880141662_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t139379724 * _thisAdjusted = reinterpret_cast<Enumerator_t139379724 *>(__this + 1);
	return Enumerator_MoveNext_m880141662(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.LinkedList`1/Enumerator<System.Object>::Dispose()
extern "C"  void Enumerator_Dispose_m2375640141_gshared (Enumerator_t139379724 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_Dispose_m2375640141_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		LinkedList_1_t1919752173 * L_0 = (LinkedList_1_t1919752173 *)__this->get_list_0();
		if (L_0)
		{
			goto IL_0012;
		}
	}
	{
		ObjectDisposedException_t21392786 * L_1 = (ObjectDisposedException_t21392786 *)il2cpp_codegen_object_new(ObjectDisposedException_t21392786_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m3603759869(L_1, (String_t*)NULL, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0012:
	{
		__this->set_current_1((LinkedListNode_1_t2825281267 *)NULL);
		__this->set_list_0((LinkedList_1_t1919752173 *)NULL);
		return;
	}
}
extern "C"  void Enumerator_Dispose_m2375640141_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t139379724 * _thisAdjusted = reinterpret_cast<Enumerator_t139379724 *>(__this + 1);
	Enumerator_Dispose_m2375640141(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.LinkedList`1<System.Object>::.ctor()
extern "C"  void LinkedList_1__ctor_m3670635350_gshared (LinkedList_1_t1919752173 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (LinkedList_1__ctor_m3670635350_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint32_t V_0 = 0;
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m297566312((Il2CppObject *)__this, /*hidden argument*/NULL);
		Il2CppObject * L_0 = (Il2CppObject *)il2cpp_codegen_object_new(Il2CppObject_il2cpp_TypeInfo_var);
		Object__ctor_m297566312(L_0, /*hidden argument*/NULL);
		__this->set_syncRoot_2(L_0);
		__this->set_first_3((LinkedListNode_1_t2825281267 *)NULL);
		int32_t L_1 = (int32_t)0;
		V_0 = (uint32_t)L_1;
		__this->set_version_1(L_1);
		uint32_t L_2 = V_0;
		__this->set_count_0(L_2);
		return;
	}
}
// System.Void System.Collections.Generic.LinkedList`1<System.Object>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void LinkedList_1__ctor_m2390670044_gshared (LinkedList_1_t1919752173 * __this, SerializationInfo_t950877179 * ___info0, StreamingContext_t3711869237  ___context1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (LinkedList_1__ctor_m2390670044_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NullCheck((LinkedList_1_t1919752173 *)__this);
		((  void (*) (LinkedList_1_t1919752173 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((LinkedList_1_t1919752173 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		SerializationInfo_t950877179 * L_0 = ___info0;
		__this->set_si_4(L_0);
		Il2CppObject * L_1 = (Il2CppObject *)il2cpp_codegen_object_new(Il2CppObject_il2cpp_TypeInfo_var);
		Object__ctor_m297566312(L_1, /*hidden argument*/NULL);
		__this->set_syncRoot_2(L_1);
		return;
	}
}
// System.Void System.Collections.Generic.LinkedList`1<System.Object>::System.Collections.Generic.ICollection<T>.Add(T)
extern "C"  void LinkedList_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m2568132950_gshared (LinkedList_1_t1919752173 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___value0;
		NullCheck((LinkedList_1_t1919752173 *)__this);
		((  LinkedListNode_1_t2825281267 * (*) (LinkedList_1_t1919752173 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((LinkedList_1_t1919752173 *)__this, (Il2CppObject *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		return;
	}
}
// System.Void System.Collections.Generic.LinkedList`1<System.Object>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void LinkedList_1_System_Collections_ICollection_CopyTo_m816767247_gshared (LinkedList_1_t1919752173 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (LinkedList_1_System_Collections_ICollection_CopyTo_m816767247_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ObjectU5BU5D_t2843939325* V_0 = NULL;
	{
		Il2CppArray * L_0 = ___array0;
		V_0 = (ObjectU5BU5D_t2843939325*)((ObjectU5BU5D_t2843939325*)IsInst(L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2)));
		ObjectU5BU5D_t2843939325* L_1 = V_0;
		if (L_1)
		{
			goto IL_0018;
		}
	}
	{
		ArgumentException_t132251570 * L_2 = (ArgumentException_t132251570 *)il2cpp_codegen_object_new(ArgumentException_t132251570_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m1312628991(L_2, (String_t*)_stringLiteral4007973390, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0018:
	{
		ObjectU5BU5D_t2843939325* L_3 = V_0;
		int32_t L_4 = ___index1;
		NullCheck((LinkedList_1_t1919752173 *)__this);
		((  void (*) (LinkedList_1_t1919752173 *, ObjectU5BU5D_t2843939325*, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)((LinkedList_1_t1919752173 *)__this, (ObjectU5BU5D_t2843939325*)L_3, (int32_t)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		return;
	}
}
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.LinkedList`1<System.Object>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C"  Il2CppObject* LinkedList_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m2427432245_gshared (LinkedList_1_t1919752173 * __this, const MethodInfo* method)
{
	{
		NullCheck((LinkedList_1_t1919752173 *)__this);
		Enumerator_t139379724  L_0 = ((  Enumerator_t139379724  (*) (LinkedList_1_t1919752173 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)((LinkedList_1_t1919752173 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		Enumerator_t139379724  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5), &L_1);
		return (Il2CppObject*)L_2;
	}
}
// System.Collections.IEnumerator System.Collections.Generic.LinkedList`1<System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * LinkedList_1_System_Collections_IEnumerable_GetEnumerator_m854819135_gshared (LinkedList_1_t1919752173 * __this, const MethodInfo* method)
{
	{
		NullCheck((LinkedList_1_t1919752173 *)__this);
		Enumerator_t139379724  L_0 = ((  Enumerator_t139379724  (*) (LinkedList_1_t1919752173 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)((LinkedList_1_t1919752173 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		Enumerator_t139379724  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5), &L_1);
		return (Il2CppObject *)L_2;
	}
}
// System.Boolean System.Collections.Generic.LinkedList`1<System.Object>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool LinkedList_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m4073043204_gshared (LinkedList_1_t1919752173 * __this, const MethodInfo* method)
{
	{
		return (bool)0;
	}
}
// System.Boolean System.Collections.Generic.LinkedList`1<System.Object>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool LinkedList_1_System_Collections_ICollection_get_IsSynchronized_m345853585_gshared (LinkedList_1_t1919752173 * __this, const MethodInfo* method)
{
	{
		return (bool)0;
	}
}
// System.Object System.Collections.Generic.LinkedList`1<System.Object>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * LinkedList_1_System_Collections_ICollection_get_SyncRoot_m260305035_gshared (LinkedList_1_t1919752173 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_syncRoot_2();
		return L_0;
	}
}
// System.Void System.Collections.Generic.LinkedList`1<System.Object>::VerifyReferencedNode(System.Collections.Generic.LinkedListNode`1<T>)
extern "C"  void LinkedList_1_VerifyReferencedNode_m3460801984_gshared (LinkedList_1_t1919752173 * __this, LinkedListNode_1_t2825281267 * ___node0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (LinkedList_1_VerifyReferencedNode_m3460801984_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		LinkedListNode_1_t2825281267 * L_0 = ___node0;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t1615371798 * L_1 = (ArgumentNullException_t1615371798 *)il2cpp_codegen_object_new(ArgumentNullException_t1615371798_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m1170824041(L_1, (String_t*)_stringLiteral1985170616, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0011:
	{
		LinkedListNode_1_t2825281267 * L_2 = ___node0;
		NullCheck((LinkedListNode_1_t2825281267 *)L_2);
		LinkedList_1_t1919752173 * L_3 = ((  LinkedList_1_t1919752173 * (*) (LinkedListNode_1_t2825281267 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((LinkedListNode_1_t2825281267 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		if ((((Il2CppObject*)(LinkedList_1_t1919752173 *)L_3) == ((Il2CppObject*)(LinkedList_1_t1919752173 *)__this)))
		{
			goto IL_0023;
		}
	}
	{
		InvalidOperationException_t56020091 * L_4 = (InvalidOperationException_t56020091 *)il2cpp_codegen_object_new(InvalidOperationException_t56020091_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2734335978(L_4, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_4);
	}

IL_0023:
	{
		return;
	}
}
// System.Collections.Generic.LinkedListNode`1<T> System.Collections.Generic.LinkedList`1<System.Object>::AddLast(T)
extern "C"  LinkedListNode_1_t2825281267 * LinkedList_1_AddLast_m3177585097_gshared (LinkedList_1_t1919752173 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	LinkedListNode_1_t2825281267 * V_0 = NULL;
	{
		LinkedListNode_1_t2825281267 * L_0 = (LinkedListNode_1_t2825281267 *)__this->get_first_3();
		if (L_0)
		{
			goto IL_001f;
		}
	}
	{
		Il2CppObject * L_1 = ___value0;
		LinkedListNode_1_t2825281267 * L_2 = (LinkedListNode_1_t2825281267 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 7));
		((  void (*) (LinkedListNode_1_t2825281267 *, LinkedList_1_t1919752173 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)(L_2, (LinkedList_1_t1919752173 *)__this, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		V_0 = (LinkedListNode_1_t2825281267 *)L_2;
		LinkedListNode_1_t2825281267 * L_3 = V_0;
		__this->set_first_3(L_3);
		goto IL_0038;
	}

IL_001f:
	{
		Il2CppObject * L_4 = ___value0;
		LinkedListNode_1_t2825281267 * L_5 = (LinkedListNode_1_t2825281267 *)__this->get_first_3();
		NullCheck(L_5);
		LinkedListNode_1_t2825281267 * L_6 = (LinkedListNode_1_t2825281267 *)L_5->get_back_3();
		LinkedListNode_1_t2825281267 * L_7 = (LinkedListNode_1_t2825281267 *)__this->get_first_3();
		LinkedListNode_1_t2825281267 * L_8 = (LinkedListNode_1_t2825281267 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 7));
		((  void (*) (LinkedListNode_1_t2825281267 *, LinkedList_1_t1919752173 *, Il2CppObject *, LinkedListNode_1_t2825281267 *, LinkedListNode_1_t2825281267 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9)->methodPointer)(L_8, (LinkedList_1_t1919752173 *)__this, (Il2CppObject *)L_4, (LinkedListNode_1_t2825281267 *)L_6, (LinkedListNode_1_t2825281267 *)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
		V_0 = (LinkedListNode_1_t2825281267 *)L_8;
	}

IL_0038:
	{
		uint32_t L_9 = (uint32_t)__this->get_count_0();
		__this->set_count_0(((int32_t)((int32_t)L_9+(int32_t)1)));
		uint32_t L_10 = (uint32_t)__this->get_version_1();
		__this->set_version_1(((int32_t)((int32_t)L_10+(int32_t)1)));
		LinkedListNode_1_t2825281267 * L_11 = V_0;
		return L_11;
	}
}
// System.Void System.Collections.Generic.LinkedList`1<System.Object>::Clear()
extern "C"  void LinkedList_1_Clear_m2711926805_gshared (LinkedList_1_t1919752173 * __this, const MethodInfo* method)
{
	{
		goto IL_000b;
	}

IL_0005:
	{
		NullCheck((LinkedList_1_t1919752173 *)__this);
		((  void (*) (LinkedList_1_t1919752173 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10)->methodPointer)((LinkedList_1_t1919752173 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10));
	}

IL_000b:
	{
		LinkedListNode_1_t2825281267 * L_0 = (LinkedListNode_1_t2825281267 *)__this->get_first_3();
		if (L_0)
		{
			goto IL_0005;
		}
	}
	{
		return;
	}
}
// System.Boolean System.Collections.Generic.LinkedList`1<System.Object>::Contains(T)
extern "C"  bool LinkedList_1_Contains_m4283568576_gshared (LinkedList_1_t1919752173 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	LinkedListNode_1_t2825281267 * V_0 = NULL;
	{
		LinkedListNode_1_t2825281267 * L_0 = (LinkedListNode_1_t2825281267 *)__this->get_first_3();
		V_0 = (LinkedListNode_1_t2825281267 *)L_0;
		LinkedListNode_1_t2825281267 * L_1 = V_0;
		if (L_1)
		{
			goto IL_000f;
		}
	}
	{
		return (bool)0;
	}

IL_000f:
	{
		LinkedListNode_1_t2825281267 * L_2 = V_0;
		NullCheck((LinkedListNode_1_t2825281267 *)L_2);
		Il2CppObject * L_3 = ((  Il2CppObject * (*) (LinkedListNode_1_t2825281267 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11)->methodPointer)((LinkedListNode_1_t2825281267 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11));
		NullCheck((Il2CppObject *)(*(&___value0)));
		bool L_4 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, (Il2CppObject *)(*(&___value0)), (Il2CppObject *)L_3);
		if (!L_4)
		{
			goto IL_002e;
		}
	}
	{
		return (bool)1;
	}

IL_002e:
	{
		LinkedListNode_1_t2825281267 * L_5 = V_0;
		NullCheck(L_5);
		LinkedListNode_1_t2825281267 * L_6 = (LinkedListNode_1_t2825281267 *)L_5->get_forward_2();
		V_0 = (LinkedListNode_1_t2825281267 *)L_6;
		LinkedListNode_1_t2825281267 * L_7 = V_0;
		LinkedListNode_1_t2825281267 * L_8 = (LinkedListNode_1_t2825281267 *)__this->get_first_3();
		if ((!(((Il2CppObject*)(LinkedListNode_1_t2825281267 *)L_7) == ((Il2CppObject*)(LinkedListNode_1_t2825281267 *)L_8))))
		{
			goto IL_000f;
		}
	}
	{
		return (bool)0;
	}
}
// System.Void System.Collections.Generic.LinkedList`1<System.Object>::CopyTo(T[],System.Int32)
extern "C"  void LinkedList_1_CopyTo_m2255490129_gshared (LinkedList_1_t1919752173 * __this, ObjectU5BU5D_t2843939325* ___array0, int32_t ___index1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (LinkedList_1_CopyTo_m2255490129_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	LinkedListNode_1_t2825281267 * V_0 = NULL;
	{
		ObjectU5BU5D_t2843939325* L_0 = ___array0;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t1615371798 * L_1 = (ArgumentNullException_t1615371798 *)il2cpp_codegen_object_new(ArgumentNullException_t1615371798_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m1170824041(L_1, (String_t*)_stringLiteral4007973390, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0011:
	{
		int32_t L_2 = ___index1;
		ObjectU5BU5D_t2843939325* L_3 = ___array0;
		NullCheck((Il2CppArray *)(Il2CppArray *)L_3);
		int32_t L_4 = Array_GetLowerBound_m2045984623((Il2CppArray *)(Il2CppArray *)L_3, (int32_t)0, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_2) < ((uint32_t)L_4))))
		{
			goto IL_0029;
		}
	}
	{
		ArgumentOutOfRangeException_t777629997 * L_5 = (ArgumentOutOfRangeException_t777629997 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t777629997_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m3628145864(L_5, (String_t*)_stringLiteral797640427, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_0029:
	{
		ObjectU5BU5D_t2843939325* L_6 = ___array0;
		NullCheck((Il2CppArray *)(Il2CppArray *)L_6);
		int32_t L_7 = Array_get_Rank_m3448755881((Il2CppArray *)(Il2CppArray *)L_6, /*hidden argument*/NULL);
		if ((((int32_t)L_7) == ((int32_t)1)))
		{
			goto IL_0045;
		}
	}
	{
		ArgumentException_t132251570 * L_8 = (ArgumentException_t132251570 *)il2cpp_codegen_object_new(ArgumentException_t132251570_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m1216717135(L_8, (String_t*)_stringLiteral4007973390, (String_t*)_stringLiteral587261375, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_8);
	}

IL_0045:
	{
		ObjectU5BU5D_t2843939325* L_9 = ___array0;
		NullCheck(L_9);
		int32_t L_10 = ___index1;
		ObjectU5BU5D_t2843939325* L_11 = ___array0;
		NullCheck((Il2CppArray *)(Il2CppArray *)L_11);
		int32_t L_12 = Array_GetLowerBound_m2045984623((Il2CppArray *)(Il2CppArray *)L_11, (int32_t)0, /*hidden argument*/NULL);
		uint32_t L_13 = (uint32_t)__this->get_count_0();
		if ((((int64_t)(((int64_t)((int64_t)((int32_t)((int32_t)((int32_t)((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_9)->max_length))))-(int32_t)L_10))+(int32_t)L_12)))))) >= ((int64_t)(((int64_t)((uint64_t)L_13))))))
		{
			goto IL_006a;
		}
	}
	{
		ArgumentException_t132251570 * L_14 = (ArgumentException_t132251570 *)il2cpp_codegen_object_new(ArgumentException_t132251570_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m1312628991(L_14, (String_t*)_stringLiteral163435716, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_14);
	}

IL_006a:
	{
		LinkedListNode_1_t2825281267 * L_15 = (LinkedListNode_1_t2825281267 *)__this->get_first_3();
		V_0 = (LinkedListNode_1_t2825281267 *)L_15;
		LinkedListNode_1_t2825281267 * L_16 = (LinkedListNode_1_t2825281267 *)__this->get_first_3();
		if (L_16)
		{
			goto IL_007d;
		}
	}
	{
		return;
	}

IL_007d:
	{
		ObjectU5BU5D_t2843939325* L_17 = ___array0;
		int32_t L_18 = ___index1;
		LinkedListNode_1_t2825281267 * L_19 = V_0;
		NullCheck((LinkedListNode_1_t2825281267 *)L_19);
		Il2CppObject * L_20 = ((  Il2CppObject * (*) (LinkedListNode_1_t2825281267 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11)->methodPointer)((LinkedListNode_1_t2825281267 *)L_19, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11));
		NullCheck(L_17);
		(L_17)->SetAt(static_cast<il2cpp_array_size_t>(L_18), (Il2CppObject *)L_20);
		int32_t L_21 = ___index1;
		___index1 = (int32_t)((int32_t)((int32_t)L_21+(int32_t)1));
		LinkedListNode_1_t2825281267 * L_22 = V_0;
		NullCheck(L_22);
		LinkedListNode_1_t2825281267 * L_23 = (LinkedListNode_1_t2825281267 *)L_22->get_forward_2();
		V_0 = (LinkedListNode_1_t2825281267 *)L_23;
		LinkedListNode_1_t2825281267 * L_24 = V_0;
		LinkedListNode_1_t2825281267 * L_25 = (LinkedListNode_1_t2825281267 *)__this->get_first_3();
		if ((!(((Il2CppObject*)(LinkedListNode_1_t2825281267 *)L_24) == ((Il2CppObject*)(LinkedListNode_1_t2825281267 *)L_25))))
		{
			goto IL_007d;
		}
	}
	{
		return;
	}
}
// System.Collections.Generic.LinkedListNode`1<T> System.Collections.Generic.LinkedList`1<System.Object>::Find(T)
extern "C"  LinkedListNode_1_t2825281267 * LinkedList_1_Find_m1359986976_gshared (LinkedList_1_t1919752173 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	LinkedListNode_1_t2825281267 * V_0 = NULL;
	{
		LinkedListNode_1_t2825281267 * L_0 = (LinkedListNode_1_t2825281267 *)__this->get_first_3();
		V_0 = (LinkedListNode_1_t2825281267 *)L_0;
		LinkedListNode_1_t2825281267 * L_1 = V_0;
		if (L_1)
		{
			goto IL_000f;
		}
	}
	{
		return (LinkedListNode_1_t2825281267 *)NULL;
	}

IL_000f:
	{
		Il2CppObject * L_2 = ___value0;
		if (L_2)
		{
			goto IL_002a;
		}
	}
	{
		LinkedListNode_1_t2825281267 * L_3 = V_0;
		NullCheck((LinkedListNode_1_t2825281267 *)L_3);
		Il2CppObject * L_4 = ((  Il2CppObject * (*) (LinkedListNode_1_t2825281267 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11)->methodPointer)((LinkedListNode_1_t2825281267 *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11));
		if (!L_4)
		{
			goto IL_0052;
		}
	}

IL_002a:
	{
		Il2CppObject * L_5 = ___value0;
		if (!L_5)
		{
			goto IL_0054;
		}
	}
	{
		LinkedListNode_1_t2825281267 * L_6 = V_0;
		NullCheck((LinkedListNode_1_t2825281267 *)L_6);
		Il2CppObject * L_7 = ((  Il2CppObject * (*) (LinkedListNode_1_t2825281267 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11)->methodPointer)((LinkedListNode_1_t2825281267 *)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11));
		NullCheck((Il2CppObject *)(*(&___value0)));
		bool L_8 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, (Il2CppObject *)(*(&___value0)), (Il2CppObject *)L_7);
		if (!L_8)
		{
			goto IL_0054;
		}
	}

IL_0052:
	{
		LinkedListNode_1_t2825281267 * L_9 = V_0;
		return L_9;
	}

IL_0054:
	{
		LinkedListNode_1_t2825281267 * L_10 = V_0;
		NullCheck(L_10);
		LinkedListNode_1_t2825281267 * L_11 = (LinkedListNode_1_t2825281267 *)L_10->get_forward_2();
		V_0 = (LinkedListNode_1_t2825281267 *)L_11;
		LinkedListNode_1_t2825281267 * L_12 = V_0;
		LinkedListNode_1_t2825281267 * L_13 = (LinkedListNode_1_t2825281267 *)__this->get_first_3();
		if ((!(((Il2CppObject*)(LinkedListNode_1_t2825281267 *)L_12) == ((Il2CppObject*)(LinkedListNode_1_t2825281267 *)L_13))))
		{
			goto IL_000f;
		}
	}
	{
		return (LinkedListNode_1_t2825281267 *)NULL;
	}
}
// System.Collections.Generic.LinkedList`1/Enumerator<T> System.Collections.Generic.LinkedList`1<System.Object>::GetEnumerator()
extern "C"  Enumerator_t139379724  LinkedList_1_GetEnumerator_m1798018665_gshared (LinkedList_1_t1919752173 * __this, const MethodInfo* method)
{
	{
		Enumerator_t139379724  L_0;
		memset(&L_0, 0, sizeof(L_0));
		Enumerator__ctor_m2504641326(&L_0, (LinkedList_1_t1919752173 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 13));
		return L_0;
	}
}
// System.Void System.Collections.Generic.LinkedList`1<System.Object>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void LinkedList_1_GetObjectData_m1792615137_gshared (LinkedList_1_t1919752173 * __this, SerializationInfo_t950877179 * ___info0, StreamingContext_t3711869237  ___context1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (LinkedList_1_GetObjectData_m1792615137_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ObjectU5BU5D_t2843939325* V_0 = NULL;
	{
		uint32_t L_0 = (uint32_t)__this->get_count_0();
		V_0 = (ObjectU5BU5D_t2843939325*)((ObjectU5BU5D_t2843939325*)SZArrayNew(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 14), (uint32_t)(((uintptr_t)L_0))));
		ObjectU5BU5D_t2843939325* L_1 = V_0;
		NullCheck((LinkedList_1_t1919752173 *)__this);
		((  void (*) (LinkedList_1_t1919752173 *, ObjectU5BU5D_t2843939325*, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)((LinkedList_1_t1919752173 *)__this, (ObjectU5BU5D_t2843939325*)L_1, (int32_t)0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		SerializationInfo_t950877179 * L_2 = ___info0;
		ObjectU5BU5D_t2843939325* L_3 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_4 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, (RuntimeTypeHandle_t3027515415 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 15)), /*hidden argument*/NULL);
		NullCheck((SerializationInfo_t950877179 *)L_2);
		SerializationInfo_AddValue_m3906743584((SerializationInfo_t950877179 *)L_2, (String_t*)_stringLiteral3586096867, (Il2CppObject *)(Il2CppObject *)L_3, (Type_t *)L_4, /*hidden argument*/NULL);
		SerializationInfo_t950877179 * L_5 = ___info0;
		uint32_t L_6 = (uint32_t)__this->get_version_1();
		NullCheck((SerializationInfo_t950877179 *)L_5);
		SerializationInfo_AddValue_m3594631505((SerializationInfo_t950877179 *)L_5, (String_t*)_stringLiteral1902401671, (uint32_t)L_6, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Collections.Generic.LinkedList`1<System.Object>::OnDeserialization(System.Object)
extern "C"  void LinkedList_1_OnDeserialization_m3201462664_gshared (LinkedList_1_t1919752173 * __this, Il2CppObject * ___sender0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (LinkedList_1_OnDeserialization_m3201462664_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ObjectU5BU5D_t2843939325* V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	ObjectU5BU5D_t2843939325* V_2 = NULL;
	int32_t V_3 = 0;
	{
		SerializationInfo_t950877179 * L_0 = (SerializationInfo_t950877179 *)__this->get_si_4();
		if (!L_0)
		{
			goto IL_0074;
		}
	}
	{
		SerializationInfo_t950877179 * L_1 = (SerializationInfo_t950877179 *)__this->get_si_4();
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_2 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, (RuntimeTypeHandle_t3027515415 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 15)), /*hidden argument*/NULL);
		NullCheck((SerializationInfo_t950877179 *)L_1);
		Il2CppObject * L_3 = SerializationInfo_GetValue_m42271953((SerializationInfo_t950877179 *)L_1, (String_t*)_stringLiteral3586096867, (Type_t *)L_2, /*hidden argument*/NULL);
		V_0 = (ObjectU5BU5D_t2843939325*)((ObjectU5BU5D_t2843939325*)Castclass(L_3, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2)));
		ObjectU5BU5D_t2843939325* L_4 = V_0;
		if (!L_4)
		{
			goto IL_0057;
		}
	}
	{
		ObjectU5BU5D_t2843939325* L_5 = V_0;
		V_2 = (ObjectU5BU5D_t2843939325*)L_5;
		V_3 = (int32_t)0;
		goto IL_004e;
	}

IL_003a:
	{
		ObjectU5BU5D_t2843939325* L_6 = V_2;
		int32_t L_7 = V_3;
		NullCheck(L_6);
		int32_t L_8 = L_7;
		Il2CppObject * L_9 = (L_6)->GetAt(static_cast<il2cpp_array_size_t>(L_8));
		V_1 = (Il2CppObject *)L_9;
		Il2CppObject * L_10 = V_1;
		NullCheck((LinkedList_1_t1919752173 *)__this);
		((  LinkedListNode_1_t2825281267 * (*) (LinkedList_1_t1919752173 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((LinkedList_1_t1919752173 *)__this, (Il2CppObject *)L_10, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		int32_t L_11 = V_3;
		V_3 = (int32_t)((int32_t)((int32_t)L_11+(int32_t)1));
	}

IL_004e:
	{
		int32_t L_12 = V_3;
		ObjectU5BU5D_t2843939325* L_13 = V_2;
		NullCheck(L_13);
		if ((((int32_t)L_12) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_13)->max_length)))))))
		{
			goto IL_003a;
		}
	}

IL_0057:
	{
		SerializationInfo_t950877179 * L_14 = (SerializationInfo_t950877179 *)__this->get_si_4();
		NullCheck((SerializationInfo_t950877179 *)L_14);
		uint32_t L_15 = SerializationInfo_GetUInt32_m776835457((SerializationInfo_t950877179 *)L_14, (String_t*)_stringLiteral1902401671, /*hidden argument*/NULL);
		__this->set_version_1(L_15);
		__this->set_si_4((SerializationInfo_t950877179 *)NULL);
	}

IL_0074:
	{
		return;
	}
}
// System.Boolean System.Collections.Generic.LinkedList`1<System.Object>::Remove(T)
extern "C"  bool LinkedList_1_Remove_m776321970_gshared (LinkedList_1_t1919752173 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	LinkedListNode_1_t2825281267 * V_0 = NULL;
	{
		Il2CppObject * L_0 = ___value0;
		NullCheck((LinkedList_1_t1919752173 *)__this);
		LinkedListNode_1_t2825281267 * L_1 = ((  LinkedListNode_1_t2825281267 * (*) (LinkedList_1_t1919752173 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 16)->methodPointer)((LinkedList_1_t1919752173 *)__this, (Il2CppObject *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 16));
		V_0 = (LinkedListNode_1_t2825281267 *)L_1;
		LinkedListNode_1_t2825281267 * L_2 = V_0;
		if (L_2)
		{
			goto IL_0010;
		}
	}
	{
		return (bool)0;
	}

IL_0010:
	{
		LinkedListNode_1_t2825281267 * L_3 = V_0;
		NullCheck((LinkedList_1_t1919752173 *)__this);
		((  void (*) (LinkedList_1_t1919752173 *, LinkedListNode_1_t2825281267 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 17)->methodPointer)((LinkedList_1_t1919752173 *)__this, (LinkedListNode_1_t2825281267 *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 17));
		return (bool)1;
	}
}
// System.Void System.Collections.Generic.LinkedList`1<System.Object>::Remove(System.Collections.Generic.LinkedListNode`1<T>)
extern "C"  void LinkedList_1_Remove_m1746789234_gshared (LinkedList_1_t1919752173 * __this, LinkedListNode_1_t2825281267 * ___node0, const MethodInfo* method)
{
	{
		LinkedListNode_1_t2825281267 * L_0 = ___node0;
		NullCheck((LinkedList_1_t1919752173 *)__this);
		((  void (*) (LinkedList_1_t1919752173 *, LinkedListNode_1_t2825281267 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 18)->methodPointer)((LinkedList_1_t1919752173 *)__this, (LinkedListNode_1_t2825281267 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 18));
		uint32_t L_1 = (uint32_t)__this->get_count_0();
		__this->set_count_0(((int32_t)((int32_t)L_1-(int32_t)1)));
		uint32_t L_2 = (uint32_t)__this->get_count_0();
		if (L_2)
		{
			goto IL_0027;
		}
	}
	{
		__this->set_first_3((LinkedListNode_1_t2825281267 *)NULL);
	}

IL_0027:
	{
		LinkedListNode_1_t2825281267 * L_3 = ___node0;
		LinkedListNode_1_t2825281267 * L_4 = (LinkedListNode_1_t2825281267 *)__this->get_first_3();
		if ((!(((Il2CppObject*)(LinkedListNode_1_t2825281267 *)L_3) == ((Il2CppObject*)(LinkedListNode_1_t2825281267 *)L_4))))
		{
			goto IL_0044;
		}
	}
	{
		LinkedListNode_1_t2825281267 * L_5 = (LinkedListNode_1_t2825281267 *)__this->get_first_3();
		NullCheck(L_5);
		LinkedListNode_1_t2825281267 * L_6 = (LinkedListNode_1_t2825281267 *)L_5->get_forward_2();
		__this->set_first_3(L_6);
	}

IL_0044:
	{
		uint32_t L_7 = (uint32_t)__this->get_version_1();
		__this->set_version_1(((int32_t)((int32_t)L_7+(int32_t)1)));
		LinkedListNode_1_t2825281267 * L_8 = ___node0;
		NullCheck((LinkedListNode_1_t2825281267 *)L_8);
		((  void (*) (LinkedListNode_1_t2825281267 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 19)->methodPointer)((LinkedListNode_1_t2825281267 *)L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 19));
		return;
	}
}
// System.Void System.Collections.Generic.LinkedList`1<System.Object>::RemoveLast()
extern "C"  void LinkedList_1_RemoveLast_m2923798972_gshared (LinkedList_1_t1919752173 * __this, const MethodInfo* method)
{
	{
		LinkedListNode_1_t2825281267 * L_0 = (LinkedListNode_1_t2825281267 *)__this->get_first_3();
		if (!L_0)
		{
			goto IL_001c;
		}
	}
	{
		LinkedListNode_1_t2825281267 * L_1 = (LinkedListNode_1_t2825281267 *)__this->get_first_3();
		NullCheck(L_1);
		LinkedListNode_1_t2825281267 * L_2 = (LinkedListNode_1_t2825281267 *)L_1->get_back_3();
		NullCheck((LinkedList_1_t1919752173 *)__this);
		((  void (*) (LinkedList_1_t1919752173 *, LinkedListNode_1_t2825281267 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 17)->methodPointer)((LinkedList_1_t1919752173 *)__this, (LinkedListNode_1_t2825281267 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 17));
	}

IL_001c:
	{
		return;
	}
}
// System.Int32 System.Collections.Generic.LinkedList`1<System.Object>::get_Count()
extern "C"  int32_t LinkedList_1_get_Count_m2167432147_gshared (LinkedList_1_t1919752173 * __this, const MethodInfo* method)
{
	{
		uint32_t L_0 = (uint32_t)__this->get_count_0();
		return L_0;
	}
}
// System.Collections.Generic.LinkedListNode`1<T> System.Collections.Generic.LinkedList`1<System.Object>::get_First()
extern "C"  LinkedListNode_1_t2825281267 * LinkedList_1_get_First_m1512309353_gshared (LinkedList_1_t1919752173 * __this, const MethodInfo* method)
{
	{
		LinkedListNode_1_t2825281267 * L_0 = (LinkedListNode_1_t2825281267 *)__this->get_first_3();
		return L_0;
	}
}
// System.Void System.Collections.Generic.LinkedListNode`1<System.Object>::.ctor(System.Collections.Generic.LinkedList`1<T>,T)
extern "C"  void LinkedListNode_1__ctor_m3617645130_gshared (LinkedListNode_1_t2825281267 * __this, LinkedList_1_t1919752173 * ___list0, Il2CppObject * ___value1, const MethodInfo* method)
{
	LinkedListNode_1_t2825281267 * V_0 = NULL;
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m297566312((Il2CppObject *)__this, /*hidden argument*/NULL);
		LinkedList_1_t1919752173 * L_0 = ___list0;
		__this->set_container_1(L_0);
		Il2CppObject * L_1 = ___value1;
		__this->set_item_0(L_1);
		V_0 = (LinkedListNode_1_t2825281267 *)__this;
		__this->set_forward_2(__this);
		LinkedListNode_1_t2825281267 * L_2 = V_0;
		__this->set_back_3(L_2);
		return;
	}
}
// System.Void System.Collections.Generic.LinkedListNode`1<System.Object>::.ctor(System.Collections.Generic.LinkedList`1<T>,T,System.Collections.Generic.LinkedListNode`1<T>,System.Collections.Generic.LinkedListNode`1<T>)
extern "C"  void LinkedListNode_1__ctor_m19826542_gshared (LinkedListNode_1_t2825281267 * __this, LinkedList_1_t1919752173 * ___list0, Il2CppObject * ___value1, LinkedListNode_1_t2825281267 * ___previousNode2, LinkedListNode_1_t2825281267 * ___nextNode3, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m297566312((Il2CppObject *)__this, /*hidden argument*/NULL);
		LinkedList_1_t1919752173 * L_0 = ___list0;
		__this->set_container_1(L_0);
		Il2CppObject * L_1 = ___value1;
		__this->set_item_0(L_1);
		LinkedListNode_1_t2825281267 * L_2 = ___previousNode2;
		__this->set_back_3(L_2);
		LinkedListNode_1_t2825281267 * L_3 = ___nextNode3;
		__this->set_forward_2(L_3);
		LinkedListNode_1_t2825281267 * L_4 = ___previousNode2;
		NullCheck(L_4);
		L_4->set_forward_2(__this);
		LinkedListNode_1_t2825281267 * L_5 = ___nextNode3;
		NullCheck(L_5);
		L_5->set_back_3(__this);
		return;
	}
}
// System.Void System.Collections.Generic.LinkedListNode`1<System.Object>::Detach()
extern "C"  void LinkedListNode_1_Detach_m1651426784_gshared (LinkedListNode_1_t2825281267 * __this, const MethodInfo* method)
{
	LinkedListNode_1_t2825281267 * V_0 = NULL;
	{
		LinkedListNode_1_t2825281267 * L_0 = (LinkedListNode_1_t2825281267 *)__this->get_back_3();
		LinkedListNode_1_t2825281267 * L_1 = (LinkedListNode_1_t2825281267 *)__this->get_forward_2();
		NullCheck(L_0);
		L_0->set_forward_2(L_1);
		LinkedListNode_1_t2825281267 * L_2 = (LinkedListNode_1_t2825281267 *)__this->get_forward_2();
		LinkedListNode_1_t2825281267 * L_3 = (LinkedListNode_1_t2825281267 *)__this->get_back_3();
		NullCheck(L_2);
		L_2->set_back_3(L_3);
		V_0 = (LinkedListNode_1_t2825281267 *)NULL;
		__this->set_back_3((LinkedListNode_1_t2825281267 *)NULL);
		LinkedListNode_1_t2825281267 * L_4 = V_0;
		__this->set_forward_2(L_4);
		__this->set_container_1((LinkedList_1_t1919752173 *)NULL);
		return;
	}
}
// System.Collections.Generic.LinkedList`1<T> System.Collections.Generic.LinkedListNode`1<System.Object>::get_List()
extern "C"  LinkedList_1_t1919752173 * LinkedListNode_1_get_List_m193057344_gshared (LinkedListNode_1_t2825281267 * __this, const MethodInfo* method)
{
	{
		LinkedList_1_t1919752173 * L_0 = (LinkedList_1_t1919752173 *)__this->get_container_1();
		return L_0;
	}
}
// System.Collections.Generic.LinkedListNode`1<T> System.Collections.Generic.LinkedListNode`1<System.Object>::get_Next()
extern "C"  LinkedListNode_1_t2825281267 * LinkedListNode_1_get_Next_m569727209_gshared (LinkedListNode_1_t2825281267 * __this, const MethodInfo* method)
{
	LinkedListNode_1_t2825281267 * G_B4_0 = NULL;
	{
		LinkedList_1_t1919752173 * L_0 = (LinkedList_1_t1919752173 *)__this->get_container_1();
		if (!L_0)
		{
			goto IL_002c;
		}
	}
	{
		LinkedListNode_1_t2825281267 * L_1 = (LinkedListNode_1_t2825281267 *)__this->get_forward_2();
		LinkedList_1_t1919752173 * L_2 = (LinkedList_1_t1919752173 *)__this->get_container_1();
		NullCheck(L_2);
		LinkedListNode_1_t2825281267 * L_3 = (LinkedListNode_1_t2825281267 *)L_2->get_first_3();
		if ((((Il2CppObject*)(LinkedListNode_1_t2825281267 *)L_1) == ((Il2CppObject*)(LinkedListNode_1_t2825281267 *)L_3)))
		{
			goto IL_002c;
		}
	}
	{
		LinkedListNode_1_t2825281267 * L_4 = (LinkedListNode_1_t2825281267 *)__this->get_forward_2();
		G_B4_0 = L_4;
		goto IL_002d;
	}

IL_002c:
	{
		G_B4_0 = ((LinkedListNode_1_t2825281267 *)(NULL));
	}

IL_002d:
	{
		return G_B4_0;
	}
}
// T System.Collections.Generic.LinkedListNode`1<System.Object>::get_Value()
extern "C"  Il2CppObject * LinkedListNode_1_get_Value_m3891174027_gshared (LinkedListNode_1_t2825281267 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_item_0();
		return L_0;
	}
}
// System.Void System.Collections.Generic.List`1/Enumerator<CustomCameraController/InputFrameData>::.ctor(System.Collections.Generic.List`1<T>)
extern "C"  void Enumerator__ctor_m69053308_gshared (Enumerator_t1640318223 * __this, List_1_t4046041642 * ___l0, const MethodInfo* method)
{
	{
		List_1_t4046041642 * L_0 = ___l0;
		__this->set_l_0(L_0);
		List_1_t4046041642 * L_1 = ___l0;
		NullCheck(L_1);
		int32_t L_2 = (int32_t)L_1->get__version_3();
		__this->set_ver_2(L_2);
		return;
	}
}
extern "C"  void Enumerator__ctor_m69053308_AdjustorThunk (Il2CppObject * __this, List_1_t4046041642 * ___l0, const MethodInfo* method)
{
	Enumerator_t1640318223 * _thisAdjusted = reinterpret_cast<Enumerator_t1640318223 *>(__this + 1);
	Enumerator__ctor_m69053308(_thisAdjusted, ___l0, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<CustomCameraController/InputFrameData>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m3420610639_gshared (Enumerator_t1640318223 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyState_m2137990666((Enumerator_t1640318223 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		__this->set_next_1(0);
		return;
	}
}
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m3420610639_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1640318223 * _thisAdjusted = reinterpret_cast<Enumerator_t1640318223 *>(__this + 1);
	Enumerator_System_Collections_IEnumerator_Reset_m3420610639(_thisAdjusted, method);
}
// System.Object System.Collections.Generic.List`1/Enumerator<CustomCameraController/InputFrameData>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m2348568638_gshared (Enumerator_t1640318223 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_System_Collections_IEnumerator_get_Current_m2348568638_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Enumerator_VerifyState_m2137990666((Enumerator_t1640318223 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) > ((int32_t)0)))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t56020091 * L_1 = (InvalidOperationException_t56020091 *)il2cpp_codegen_object_new(InvalidOperationException_t56020091_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2734335978(L_1, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		InputFrameData_t2573966900  L_2 = (InputFrameData_t2573966900 )__this->get_current_3();
		InputFrameData_t2573966900  L_3 = L_2;
		Il2CppObject * L_4 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_3);
		return L_4;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m2348568638_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1640318223 * _thisAdjusted = reinterpret_cast<Enumerator_t1640318223 *>(__this + 1);
	return Enumerator_System_Collections_IEnumerator_get_Current_m2348568638(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<CustomCameraController/InputFrameData>::Dispose()
extern "C"  void Enumerator_Dispose_m2308405500_gshared (Enumerator_t1640318223 * __this, const MethodInfo* method)
{
	{
		__this->set_l_0((List_1_t4046041642 *)NULL);
		return;
	}
}
extern "C"  void Enumerator_Dispose_m2308405500_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1640318223 * _thisAdjusted = reinterpret_cast<Enumerator_t1640318223 *>(__this + 1);
	Enumerator_Dispose_m2308405500(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<CustomCameraController/InputFrameData>::VerifyState()
extern "C"  void Enumerator_VerifyState_m2137990666_gshared (Enumerator_t1640318223 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_VerifyState_m2137990666_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		List_1_t4046041642 * L_0 = (List_1_t4046041642 *)__this->get_l_0();
		if (L_0)
		{
			goto IL_0026;
		}
	}
	{
		Enumerator_t1640318223  L_1 = (*(Enumerator_t1640318223 *)__this);
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_1);
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m88164663((Il2CppObject *)L_2, /*hidden argument*/NULL);
		NullCheck((Type_t *)L_3);
		String_t* L_4 = VirtFuncInvoker0< String_t* >::Invoke(18 /* System.String System.Type::get_FullName() */, (Type_t *)L_3);
		ObjectDisposedException_t21392786 * L_5 = (ObjectDisposedException_t21392786 *)il2cpp_codegen_object_new(ObjectDisposedException_t21392786_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m3603759869(L_5, (String_t*)L_4, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_0026:
	{
		int32_t L_6 = (int32_t)__this->get_ver_2();
		List_1_t4046041642 * L_7 = (List_1_t4046041642 *)__this->get_l_0();
		NullCheck(L_7);
		int32_t L_8 = (int32_t)L_7->get__version_3();
		if ((((int32_t)L_6) == ((int32_t)L_8)))
		{
			goto IL_0047;
		}
	}
	{
		InvalidOperationException_t56020091 * L_9 = (InvalidOperationException_t56020091 *)il2cpp_codegen_object_new(InvalidOperationException_t56020091_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m237278729(L_9, (String_t*)_stringLiteral1621028992, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_9);
	}

IL_0047:
	{
		return;
	}
}
extern "C"  void Enumerator_VerifyState_m2137990666_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1640318223 * _thisAdjusted = reinterpret_cast<Enumerator_t1640318223 *>(__this + 1);
	Enumerator_VerifyState_m2137990666(_thisAdjusted, method);
}
// System.Boolean System.Collections.Generic.List`1/Enumerator<CustomCameraController/InputFrameData>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m1995532294_gshared (Enumerator_t1640318223 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		Enumerator_VerifyState_m2137990666((Enumerator_t1640318223 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		int32_t L_1 = (int32_t)__this->get_next_1();
		List_1_t4046041642 * L_2 = (List_1_t4046041642 *)__this->get_l_0();
		NullCheck(L_2);
		int32_t L_3 = (int32_t)L_2->get__size_2();
		if ((((int32_t)L_1) >= ((int32_t)L_3)))
		{
			goto IL_0053;
		}
	}
	{
		List_1_t4046041642 * L_4 = (List_1_t4046041642 *)__this->get_l_0();
		NullCheck(L_4);
		InputFrameDataU5BU5D_t2280996477* L_5 = (InputFrameDataU5BU5D_t2280996477*)L_4->get__items_1();
		int32_t L_6 = (int32_t)__this->get_next_1();
		int32_t L_7 = (int32_t)L_6;
		V_0 = (int32_t)L_7;
		__this->set_next_1(((int32_t)((int32_t)L_7+(int32_t)1)));
		int32_t L_8 = V_0;
		NullCheck(L_5);
		int32_t L_9 = L_8;
		InputFrameData_t2573966900  L_10 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		__this->set_current_3(L_10);
		return (bool)1;
	}

IL_0053:
	{
		__this->set_next_1((-1));
		return (bool)0;
	}
}
extern "C"  bool Enumerator_MoveNext_m1995532294_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1640318223 * _thisAdjusted = reinterpret_cast<Enumerator_t1640318223 *>(__this + 1);
	return Enumerator_MoveNext_m1995532294(_thisAdjusted, method);
}
// T System.Collections.Generic.List`1/Enumerator<CustomCameraController/InputFrameData>::get_Current()
extern "C"  InputFrameData_t2573966900  Enumerator_get_Current_m1721394586_gshared (Enumerator_t1640318223 * __this, const MethodInfo* method)
{
	{
		InputFrameData_t2573966900  L_0 = (InputFrameData_t2573966900 )__this->get_current_3();
		return L_0;
	}
}
extern "C"  InputFrameData_t2573966900  Enumerator_get_Current_m1721394586_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1640318223 * _thisAdjusted = reinterpret_cast<Enumerator_t1640318223 *>(__this + 1);
	return Enumerator_get_Current_m1721394586(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<HighlightingSystem.HighlighterRenderer/Data>::.ctor(System.Collections.Generic.List`1<T>)
extern "C"  void Enumerator__ctor_m583292949_gshared (Enumerator_t3796191691 * __this, List_1_t1906947814 * ___l0, const MethodInfo* method)
{
	{
		List_1_t1906947814 * L_0 = ___l0;
		__this->set_l_0(L_0);
		List_1_t1906947814 * L_1 = ___l0;
		NullCheck(L_1);
		int32_t L_2 = (int32_t)L_1->get__version_3();
		__this->set_ver_2(L_2);
		return;
	}
}
extern "C"  void Enumerator__ctor_m583292949_AdjustorThunk (Il2CppObject * __this, List_1_t1906947814 * ___l0, const MethodInfo* method)
{
	Enumerator_t3796191691 * _thisAdjusted = reinterpret_cast<Enumerator_t3796191691 *>(__this + 1);
	Enumerator__ctor_m583292949(_thisAdjusted, ___l0, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<HighlightingSystem.HighlighterRenderer/Data>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m2985421904_gshared (Enumerator_t3796191691 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyState_m1268345934((Enumerator_t3796191691 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		__this->set_next_1(0);
		return;
	}
}
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m2985421904_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3796191691 * _thisAdjusted = reinterpret_cast<Enumerator_t3796191691 *>(__this + 1);
	Enumerator_System_Collections_IEnumerator_Reset_m2985421904(_thisAdjusted, method);
}
// System.Object System.Collections.Generic.List`1/Enumerator<HighlightingSystem.HighlighterRenderer/Data>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m2892844362_gshared (Enumerator_t3796191691 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_System_Collections_IEnumerator_get_Current_m2892844362_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Enumerator_VerifyState_m1268345934((Enumerator_t3796191691 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) > ((int32_t)0)))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t56020091 * L_1 = (InvalidOperationException_t56020091 *)il2cpp_codegen_object_new(InvalidOperationException_t56020091_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2734335978(L_1, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		Data_t434873072  L_2 = (Data_t434873072 )__this->get_current_3();
		Data_t434873072  L_3 = L_2;
		Il2CppObject * L_4 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_3);
		return L_4;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m2892844362_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3796191691 * _thisAdjusted = reinterpret_cast<Enumerator_t3796191691 *>(__this + 1);
	return Enumerator_System_Collections_IEnumerator_get_Current_m2892844362(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<HighlightingSystem.HighlighterRenderer/Data>::Dispose()
extern "C"  void Enumerator_Dispose_m1071958056_gshared (Enumerator_t3796191691 * __this, const MethodInfo* method)
{
	{
		__this->set_l_0((List_1_t1906947814 *)NULL);
		return;
	}
}
extern "C"  void Enumerator_Dispose_m1071958056_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3796191691 * _thisAdjusted = reinterpret_cast<Enumerator_t3796191691 *>(__this + 1);
	Enumerator_Dispose_m1071958056(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<HighlightingSystem.HighlighterRenderer/Data>::VerifyState()
extern "C"  void Enumerator_VerifyState_m1268345934_gshared (Enumerator_t3796191691 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_VerifyState_m1268345934_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		List_1_t1906947814 * L_0 = (List_1_t1906947814 *)__this->get_l_0();
		if (L_0)
		{
			goto IL_0026;
		}
	}
	{
		Enumerator_t3796191691  L_1 = (*(Enumerator_t3796191691 *)__this);
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_1);
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m88164663((Il2CppObject *)L_2, /*hidden argument*/NULL);
		NullCheck((Type_t *)L_3);
		String_t* L_4 = VirtFuncInvoker0< String_t* >::Invoke(18 /* System.String System.Type::get_FullName() */, (Type_t *)L_3);
		ObjectDisposedException_t21392786 * L_5 = (ObjectDisposedException_t21392786 *)il2cpp_codegen_object_new(ObjectDisposedException_t21392786_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m3603759869(L_5, (String_t*)L_4, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_0026:
	{
		int32_t L_6 = (int32_t)__this->get_ver_2();
		List_1_t1906947814 * L_7 = (List_1_t1906947814 *)__this->get_l_0();
		NullCheck(L_7);
		int32_t L_8 = (int32_t)L_7->get__version_3();
		if ((((int32_t)L_6) == ((int32_t)L_8)))
		{
			goto IL_0047;
		}
	}
	{
		InvalidOperationException_t56020091 * L_9 = (InvalidOperationException_t56020091 *)il2cpp_codegen_object_new(InvalidOperationException_t56020091_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m237278729(L_9, (String_t*)_stringLiteral1621028992, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_9);
	}

IL_0047:
	{
		return;
	}
}
extern "C"  void Enumerator_VerifyState_m1268345934_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3796191691 * _thisAdjusted = reinterpret_cast<Enumerator_t3796191691 *>(__this + 1);
	Enumerator_VerifyState_m1268345934(_thisAdjusted, method);
}
// System.Boolean System.Collections.Generic.List`1/Enumerator<HighlightingSystem.HighlighterRenderer/Data>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m4173244301_gshared (Enumerator_t3796191691 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		Enumerator_VerifyState_m1268345934((Enumerator_t3796191691 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		int32_t L_1 = (int32_t)__this->get_next_1();
		List_1_t1906947814 * L_2 = (List_1_t1906947814 *)__this->get_l_0();
		NullCheck(L_2);
		int32_t L_3 = (int32_t)L_2->get__size_2();
		if ((((int32_t)L_1) >= ((int32_t)L_3)))
		{
			goto IL_0053;
		}
	}
	{
		List_1_t1906947814 * L_4 = (List_1_t1906947814 *)__this->get_l_0();
		NullCheck(L_4);
		DataU5BU5D_t2819638097* L_5 = (DataU5BU5D_t2819638097*)L_4->get__items_1();
		int32_t L_6 = (int32_t)__this->get_next_1();
		int32_t L_7 = (int32_t)L_6;
		V_0 = (int32_t)L_7;
		__this->set_next_1(((int32_t)((int32_t)L_7+(int32_t)1)));
		int32_t L_8 = V_0;
		NullCheck(L_5);
		int32_t L_9 = L_8;
		Data_t434873072  L_10 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		__this->set_current_3(L_10);
		return (bool)1;
	}

IL_0053:
	{
		__this->set_next_1((-1));
		return (bool)0;
	}
}
extern "C"  bool Enumerator_MoveNext_m4173244301_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3796191691 * _thisAdjusted = reinterpret_cast<Enumerator_t3796191691 *>(__this + 1);
	return Enumerator_MoveNext_m4173244301(_thisAdjusted, method);
}
// T System.Collections.Generic.List`1/Enumerator<HighlightingSystem.HighlighterRenderer/Data>::get_Current()
extern "C"  Data_t434873072  Enumerator_get_Current_m935646724_gshared (Enumerator_t3796191691 * __this, const MethodInfo* method)
{
	{
		Data_t434873072  L_0 = (Data_t434873072 )__this->get_current_3();
		return L_0;
	}
}
extern "C"  Data_t434873072  Enumerator_get_Current_m935646724_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3796191691 * _thisAdjusted = reinterpret_cast<Enumerator_t3796191691 *>(__this + 1);
	return Enumerator_get_Current_m935646724(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<HighlightingSystem.HighlightingPreset>::.ctor(System.Collections.Generic.List`1<T>)
extern "C"  void Enumerator__ctor_m3930688850_gshared (Enumerator_t3996938410 * __this, List_1_t2107694533 * ___l0, const MethodInfo* method)
{
	{
		List_1_t2107694533 * L_0 = ___l0;
		__this->set_l_0(L_0);
		List_1_t2107694533 * L_1 = ___l0;
		NullCheck(L_1);
		int32_t L_2 = (int32_t)L_1->get__version_3();
		__this->set_ver_2(L_2);
		return;
	}
}
extern "C"  void Enumerator__ctor_m3930688850_AdjustorThunk (Il2CppObject * __this, List_1_t2107694533 * ___l0, const MethodInfo* method)
{
	Enumerator_t3996938410 * _thisAdjusted = reinterpret_cast<Enumerator_t3996938410 *>(__this + 1);
	Enumerator__ctor_m3930688850(_thisAdjusted, ___l0, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<HighlightingSystem.HighlightingPreset>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m3021194315_gshared (Enumerator_t3996938410 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyState_m2056140850((Enumerator_t3996938410 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		__this->set_next_1(0);
		return;
	}
}
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m3021194315_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3996938410 * _thisAdjusted = reinterpret_cast<Enumerator_t3996938410 *>(__this + 1);
	Enumerator_System_Collections_IEnumerator_Reset_m3021194315(_thisAdjusted, method);
}
// System.Object System.Collections.Generic.List`1/Enumerator<HighlightingSystem.HighlightingPreset>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m3745379029_gshared (Enumerator_t3996938410 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_System_Collections_IEnumerator_get_Current_m3745379029_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Enumerator_VerifyState_m2056140850((Enumerator_t3996938410 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) > ((int32_t)0)))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t56020091 * L_1 = (InvalidOperationException_t56020091 *)il2cpp_codegen_object_new(InvalidOperationException_t56020091_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2734335978(L_1, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		HighlightingPreset_t635619791  L_2 = (HighlightingPreset_t635619791 )__this->get_current_3();
		HighlightingPreset_t635619791  L_3 = L_2;
		Il2CppObject * L_4 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_3);
		return L_4;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m3745379029_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3996938410 * _thisAdjusted = reinterpret_cast<Enumerator_t3996938410 *>(__this + 1);
	return Enumerator_System_Collections_IEnumerator_get_Current_m3745379029(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<HighlightingSystem.HighlightingPreset>::Dispose()
extern "C"  void Enumerator_Dispose_m3559089685_gshared (Enumerator_t3996938410 * __this, const MethodInfo* method)
{
	{
		__this->set_l_0((List_1_t2107694533 *)NULL);
		return;
	}
}
extern "C"  void Enumerator_Dispose_m3559089685_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3996938410 * _thisAdjusted = reinterpret_cast<Enumerator_t3996938410 *>(__this + 1);
	Enumerator_Dispose_m3559089685(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<HighlightingSystem.HighlightingPreset>::VerifyState()
extern "C"  void Enumerator_VerifyState_m2056140850_gshared (Enumerator_t3996938410 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_VerifyState_m2056140850_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		List_1_t2107694533 * L_0 = (List_1_t2107694533 *)__this->get_l_0();
		if (L_0)
		{
			goto IL_0026;
		}
	}
	{
		Enumerator_t3996938410  L_1 = (*(Enumerator_t3996938410 *)__this);
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_1);
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m88164663((Il2CppObject *)L_2, /*hidden argument*/NULL);
		NullCheck((Type_t *)L_3);
		String_t* L_4 = VirtFuncInvoker0< String_t* >::Invoke(18 /* System.String System.Type::get_FullName() */, (Type_t *)L_3);
		ObjectDisposedException_t21392786 * L_5 = (ObjectDisposedException_t21392786 *)il2cpp_codegen_object_new(ObjectDisposedException_t21392786_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m3603759869(L_5, (String_t*)L_4, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_0026:
	{
		int32_t L_6 = (int32_t)__this->get_ver_2();
		List_1_t2107694533 * L_7 = (List_1_t2107694533 *)__this->get_l_0();
		NullCheck(L_7);
		int32_t L_8 = (int32_t)L_7->get__version_3();
		if ((((int32_t)L_6) == ((int32_t)L_8)))
		{
			goto IL_0047;
		}
	}
	{
		InvalidOperationException_t56020091 * L_9 = (InvalidOperationException_t56020091 *)il2cpp_codegen_object_new(InvalidOperationException_t56020091_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m237278729(L_9, (String_t*)_stringLiteral1621028992, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_9);
	}

IL_0047:
	{
		return;
	}
}
extern "C"  void Enumerator_VerifyState_m2056140850_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3996938410 * _thisAdjusted = reinterpret_cast<Enumerator_t3996938410 *>(__this + 1);
	Enumerator_VerifyState_m2056140850(_thisAdjusted, method);
}
// System.Boolean System.Collections.Generic.List`1/Enumerator<HighlightingSystem.HighlightingPreset>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m2406304866_gshared (Enumerator_t3996938410 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		Enumerator_VerifyState_m2056140850((Enumerator_t3996938410 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		int32_t L_1 = (int32_t)__this->get_next_1();
		List_1_t2107694533 * L_2 = (List_1_t2107694533 *)__this->get_l_0();
		NullCheck(L_2);
		int32_t L_3 = (int32_t)L_2->get__size_2();
		if ((((int32_t)L_1) >= ((int32_t)L_3)))
		{
			goto IL_0053;
		}
	}
	{
		List_1_t2107694533 * L_4 = (List_1_t2107694533 *)__this->get_l_0();
		NullCheck(L_4);
		HighlightingPresetU5BU5D_t2765988374* L_5 = (HighlightingPresetU5BU5D_t2765988374*)L_4->get__items_1();
		int32_t L_6 = (int32_t)__this->get_next_1();
		int32_t L_7 = (int32_t)L_6;
		V_0 = (int32_t)L_7;
		__this->set_next_1(((int32_t)((int32_t)L_7+(int32_t)1)));
		int32_t L_8 = V_0;
		NullCheck(L_5);
		int32_t L_9 = L_8;
		HighlightingPreset_t635619791  L_10 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		__this->set_current_3(L_10);
		return (bool)1;
	}

IL_0053:
	{
		__this->set_next_1((-1));
		return (bool)0;
	}
}
extern "C"  bool Enumerator_MoveNext_m2406304866_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3996938410 * _thisAdjusted = reinterpret_cast<Enumerator_t3996938410 *>(__this + 1);
	return Enumerator_MoveNext_m2406304866(_thisAdjusted, method);
}
// T System.Collections.Generic.List`1/Enumerator<HighlightingSystem.HighlightingPreset>::get_Current()
extern "C"  HighlightingPreset_t635619791  Enumerator_get_Current_m1356247204_gshared (Enumerator_t3996938410 * __this, const MethodInfo* method)
{
	{
		HighlightingPreset_t635619791  L_0 = (HighlightingPreset_t635619791 )__this->get_current_3();
		return L_0;
	}
}
extern "C"  HighlightingPreset_t635619791  Enumerator_get_Current_m1356247204_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3996938410 * _thisAdjusted = reinterpret_cast<Enumerator_t3996938410 *>(__this + 1);
	return Enumerator_get_Current_m1356247204(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<System.Char>::.ctor(System.Collections.Generic.List`1<T>)
extern "C"  void Enumerator__ctor_m2263237050_gshared (Enumerator_t2700811793 * __this, List_1_t811567916 * ___l0, const MethodInfo* method)
{
	{
		List_1_t811567916 * L_0 = ___l0;
		__this->set_l_0(L_0);
		List_1_t811567916 * L_1 = ___l0;
		NullCheck(L_1);
		int32_t L_2 = (int32_t)L_1->get__version_3();
		__this->set_ver_2(L_2);
		return;
	}
}
extern "C"  void Enumerator__ctor_m2263237050_AdjustorThunk (Il2CppObject * __this, List_1_t811567916 * ___l0, const MethodInfo* method)
{
	Enumerator_t2700811793 * _thisAdjusted = reinterpret_cast<Enumerator_t2700811793 *>(__this + 1);
	Enumerator__ctor_m2263237050(_thisAdjusted, ___l0, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<System.Char>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m2394182378_gshared (Enumerator_t2700811793 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyState_m1375238439((Enumerator_t2700811793 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		__this->set_next_1(0);
		return;
	}
}
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m2394182378_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2700811793 * _thisAdjusted = reinterpret_cast<Enumerator_t2700811793 *>(__this + 1);
	Enumerator_System_Collections_IEnumerator_Reset_m2394182378(_thisAdjusted, method);
}
// System.Object System.Collections.Generic.List`1/Enumerator<System.Char>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m1846615603_gshared (Enumerator_t2700811793 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_System_Collections_IEnumerator_get_Current_m1846615603_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Enumerator_VerifyState_m1375238439((Enumerator_t2700811793 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) > ((int32_t)0)))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t56020091 * L_1 = (InvalidOperationException_t56020091 *)il2cpp_codegen_object_new(InvalidOperationException_t56020091_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2734335978(L_1, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		Il2CppChar L_2 = (Il2CppChar)__this->get_current_3();
		Il2CppChar L_3 = L_2;
		Il2CppObject * L_4 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_3);
		return L_4;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m1846615603_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2700811793 * _thisAdjusted = reinterpret_cast<Enumerator_t2700811793 *>(__this + 1);
	return Enumerator_System_Collections_IEnumerator_get_Current_m1846615603(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<System.Char>::Dispose()
extern "C"  void Enumerator_Dispose_m3961787728_gshared (Enumerator_t2700811793 * __this, const MethodInfo* method)
{
	{
		__this->set_l_0((List_1_t811567916 *)NULL);
		return;
	}
}
extern "C"  void Enumerator_Dispose_m3961787728_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2700811793 * _thisAdjusted = reinterpret_cast<Enumerator_t2700811793 *>(__this + 1);
	Enumerator_Dispose_m3961787728(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<System.Char>::VerifyState()
extern "C"  void Enumerator_VerifyState_m1375238439_gshared (Enumerator_t2700811793 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_VerifyState_m1375238439_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		List_1_t811567916 * L_0 = (List_1_t811567916 *)__this->get_l_0();
		if (L_0)
		{
			goto IL_0026;
		}
	}
	{
		Enumerator_t2700811793  L_1 = (*(Enumerator_t2700811793 *)__this);
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_1);
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m88164663((Il2CppObject *)L_2, /*hidden argument*/NULL);
		NullCheck((Type_t *)L_3);
		String_t* L_4 = VirtFuncInvoker0< String_t* >::Invoke(18 /* System.String System.Type::get_FullName() */, (Type_t *)L_3);
		ObjectDisposedException_t21392786 * L_5 = (ObjectDisposedException_t21392786 *)il2cpp_codegen_object_new(ObjectDisposedException_t21392786_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m3603759869(L_5, (String_t*)L_4, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_0026:
	{
		int32_t L_6 = (int32_t)__this->get_ver_2();
		List_1_t811567916 * L_7 = (List_1_t811567916 *)__this->get_l_0();
		NullCheck(L_7);
		int32_t L_8 = (int32_t)L_7->get__version_3();
		if ((((int32_t)L_6) == ((int32_t)L_8)))
		{
			goto IL_0047;
		}
	}
	{
		InvalidOperationException_t56020091 * L_9 = (InvalidOperationException_t56020091 *)il2cpp_codegen_object_new(InvalidOperationException_t56020091_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m237278729(L_9, (String_t*)_stringLiteral1621028992, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_9);
	}

IL_0047:
	{
		return;
	}
}
extern "C"  void Enumerator_VerifyState_m1375238439_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2700811793 * _thisAdjusted = reinterpret_cast<Enumerator_t2700811793 *>(__this + 1);
	Enumerator_VerifyState_m1375238439(_thisAdjusted, method);
}
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.Char>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m1940664149_gshared (Enumerator_t2700811793 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		Enumerator_VerifyState_m1375238439((Enumerator_t2700811793 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		int32_t L_1 = (int32_t)__this->get_next_1();
		List_1_t811567916 * L_2 = (List_1_t811567916 *)__this->get_l_0();
		NullCheck(L_2);
		int32_t L_3 = (int32_t)L_2->get__size_2();
		if ((((int32_t)L_1) >= ((int32_t)L_3)))
		{
			goto IL_0053;
		}
	}
	{
		List_1_t811567916 * L_4 = (List_1_t811567916 *)__this->get_l_0();
		NullCheck(L_4);
		CharU5BU5D_t3528271667* L_5 = (CharU5BU5D_t3528271667*)L_4->get__items_1();
		int32_t L_6 = (int32_t)__this->get_next_1();
		int32_t L_7 = (int32_t)L_6;
		V_0 = (int32_t)L_7;
		__this->set_next_1(((int32_t)((int32_t)L_7+(int32_t)1)));
		int32_t L_8 = V_0;
		NullCheck(L_5);
		int32_t L_9 = L_8;
		Il2CppChar L_10 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		__this->set_current_3(L_10);
		return (bool)1;
	}

IL_0053:
	{
		__this->set_next_1((-1));
		return (bool)0;
	}
}
extern "C"  bool Enumerator_MoveNext_m1940664149_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2700811793 * _thisAdjusted = reinterpret_cast<Enumerator_t2700811793 *>(__this + 1);
	return Enumerator_MoveNext_m1940664149(_thisAdjusted, method);
}
// T System.Collections.Generic.List`1/Enumerator<System.Char>::get_Current()
extern "C"  Il2CppChar Enumerator_get_Current_m1436440636_gshared (Enumerator_t2700811793 * __this, const MethodInfo* method)
{
	{
		Il2CppChar L_0 = (Il2CppChar)__this->get_current_3();
		return L_0;
	}
}
extern "C"  Il2CppChar Enumerator_get_Current_m1436440636_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2700811793 * _thisAdjusted = reinterpret_cast<Enumerator_t2700811793 *>(__this + 1);
	return Enumerator_get_Current_m1436440636(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<System.Int32>::.ctor(System.Collections.Generic.List`1<T>)
extern "C"  void Enumerator__ctor_m247851533_gshared (Enumerator_t2017297076 * __this, List_1_t128053199 * ___l0, const MethodInfo* method)
{
	{
		List_1_t128053199 * L_0 = ___l0;
		__this->set_l_0(L_0);
		List_1_t128053199 * L_1 = ___l0;
		NullCheck(L_1);
		int32_t L_2 = (int32_t)L_1->get__version_3();
		__this->set_ver_2(L_2);
		return;
	}
}
extern "C"  void Enumerator__ctor_m247851533_AdjustorThunk (Il2CppObject * __this, List_1_t128053199 * ___l0, const MethodInfo* method)
{
	Enumerator_t2017297076 * _thisAdjusted = reinterpret_cast<Enumerator_t2017297076 *>(__this + 1);
	Enumerator__ctor_m247851533(_thisAdjusted, ___l0, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<System.Int32>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m502339360_gshared (Enumerator_t2017297076 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyState_m1898450050((Enumerator_t2017297076 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		__this->set_next_1(0);
		return;
	}
}
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m502339360_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2017297076 * _thisAdjusted = reinterpret_cast<Enumerator_t2017297076 *>(__this + 1);
	Enumerator_System_Collections_IEnumerator_Reset_m502339360(_thisAdjusted, method);
}
// System.Object System.Collections.Generic.List`1/Enumerator<System.Int32>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m323862414_gshared (Enumerator_t2017297076 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_System_Collections_IEnumerator_get_Current_m323862414_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Enumerator_VerifyState_m1898450050((Enumerator_t2017297076 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) > ((int32_t)0)))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t56020091 * L_1 = (InvalidOperationException_t56020091 *)il2cpp_codegen_object_new(InvalidOperationException_t56020091_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2734335978(L_1, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_current_3();
		int32_t L_3 = L_2;
		Il2CppObject * L_4 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_3);
		return L_4;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m323862414_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2017297076 * _thisAdjusted = reinterpret_cast<Enumerator_t2017297076 *>(__this + 1);
	return Enumerator_System_Collections_IEnumerator_get_Current_m323862414(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<System.Int32>::Dispose()
extern "C"  void Enumerator_Dispose_m222348240_gshared (Enumerator_t2017297076 * __this, const MethodInfo* method)
{
	{
		__this->set_l_0((List_1_t128053199 *)NULL);
		return;
	}
}
extern "C"  void Enumerator_Dispose_m222348240_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2017297076 * _thisAdjusted = reinterpret_cast<Enumerator_t2017297076 *>(__this + 1);
	Enumerator_Dispose_m222348240(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<System.Int32>::VerifyState()
extern "C"  void Enumerator_VerifyState_m1898450050_gshared (Enumerator_t2017297076 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_VerifyState_m1898450050_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		List_1_t128053199 * L_0 = (List_1_t128053199 *)__this->get_l_0();
		if (L_0)
		{
			goto IL_0026;
		}
	}
	{
		Enumerator_t2017297076  L_1 = (*(Enumerator_t2017297076 *)__this);
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_1);
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m88164663((Il2CppObject *)L_2, /*hidden argument*/NULL);
		NullCheck((Type_t *)L_3);
		String_t* L_4 = VirtFuncInvoker0< String_t* >::Invoke(18 /* System.String System.Type::get_FullName() */, (Type_t *)L_3);
		ObjectDisposedException_t21392786 * L_5 = (ObjectDisposedException_t21392786 *)il2cpp_codegen_object_new(ObjectDisposedException_t21392786_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m3603759869(L_5, (String_t*)L_4, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_0026:
	{
		int32_t L_6 = (int32_t)__this->get_ver_2();
		List_1_t128053199 * L_7 = (List_1_t128053199 *)__this->get_l_0();
		NullCheck(L_7);
		int32_t L_8 = (int32_t)L_7->get__version_3();
		if ((((int32_t)L_6) == ((int32_t)L_8)))
		{
			goto IL_0047;
		}
	}
	{
		InvalidOperationException_t56020091 * L_9 = (InvalidOperationException_t56020091 *)il2cpp_codegen_object_new(InvalidOperationException_t56020091_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m237278729(L_9, (String_t*)_stringLiteral1621028992, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_9);
	}

IL_0047:
	{
		return;
	}
}
extern "C"  void Enumerator_VerifyState_m1898450050_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2017297076 * _thisAdjusted = reinterpret_cast<Enumerator_t2017297076 *>(__this + 1);
	Enumerator_VerifyState_m1898450050(_thisAdjusted, method);
}
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.Int32>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m1131351993_gshared (Enumerator_t2017297076 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		Enumerator_VerifyState_m1898450050((Enumerator_t2017297076 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		int32_t L_1 = (int32_t)__this->get_next_1();
		List_1_t128053199 * L_2 = (List_1_t128053199 *)__this->get_l_0();
		NullCheck(L_2);
		int32_t L_3 = (int32_t)L_2->get__size_2();
		if ((((int32_t)L_1) >= ((int32_t)L_3)))
		{
			goto IL_0053;
		}
	}
	{
		List_1_t128053199 * L_4 = (List_1_t128053199 *)__this->get_l_0();
		NullCheck(L_4);
		Int32U5BU5D_t385246372* L_5 = (Int32U5BU5D_t385246372*)L_4->get__items_1();
		int32_t L_6 = (int32_t)__this->get_next_1();
		int32_t L_7 = (int32_t)L_6;
		V_0 = (int32_t)L_7;
		__this->set_next_1(((int32_t)((int32_t)L_7+(int32_t)1)));
		int32_t L_8 = V_0;
		NullCheck(L_5);
		int32_t L_9 = L_8;
		int32_t L_10 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		__this->set_current_3(L_10);
		return (bool)1;
	}

IL_0053:
	{
		__this->set_next_1((-1));
		return (bool)0;
	}
}
extern "C"  bool Enumerator_MoveNext_m1131351993_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2017297076 * _thisAdjusted = reinterpret_cast<Enumerator_t2017297076 *>(__this + 1);
	return Enumerator_MoveNext_m1131351993(_thisAdjusted, method);
}
// T System.Collections.Generic.List`1/Enumerator<System.Int32>::get_Current()
extern "C"  int32_t Enumerator_get_Current_m2734313259_gshared (Enumerator_t2017297076 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (int32_t)__this->get_current_3();
		return L_0;
	}
}
extern "C"  int32_t Enumerator_get_Current_m2734313259_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2017297076 * _thisAdjusted = reinterpret_cast<Enumerator_t2017297076 *>(__this + 1);
	return Enumerator_get_Current_m2734313259(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<System.Object>::.ctor(System.Collections.Generic.List`1<T>)
extern "C"  void Enumerator__ctor_m3170385166_gshared (Enumerator_t2146457487 * __this, List_1_t257213610 * ___l0, const MethodInfo* method)
{
	{
		List_1_t257213610 * L_0 = ___l0;
		__this->set_l_0(L_0);
		List_1_t257213610 * L_1 = ___l0;
		NullCheck(L_1);
		int32_t L_2 = (int32_t)L_1->get__version_3();
		__this->set_ver_2(L_2);
		return;
	}
}
extern "C"  void Enumerator__ctor_m3170385166_AdjustorThunk (Il2CppObject * __this, List_1_t257213610 * ___l0, const MethodInfo* method)
{
	Enumerator_t2146457487 * _thisAdjusted = reinterpret_cast<Enumerator_t2146457487 *>(__this + 1);
	Enumerator__ctor_m3170385166(_thisAdjusted, ___l0, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<System.Object>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m959124362_gshared (Enumerator_t2146457487 * __this, const MethodInfo* method)
{
	{
		Enumerator_VerifyState_m2933667029((Enumerator_t2146457487 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		__this->set_next_1(0);
		return;
	}
}
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m959124362_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2146457487 * _thisAdjusted = reinterpret_cast<Enumerator_t2146457487 *>(__this + 1);
	Enumerator_System_Collections_IEnumerator_Reset_m959124362(_thisAdjusted, method);
}
// System.Object System.Collections.Generic.List`1/Enumerator<System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m3681948262_gshared (Enumerator_t2146457487 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_System_Collections_IEnumerator_get_Current_m3681948262_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Enumerator_VerifyState_m2933667029((Enumerator_t2146457487 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) > ((int32_t)0)))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t56020091 * L_1 = (InvalidOperationException_t56020091 *)il2cpp_codegen_object_new(InvalidOperationException_t56020091_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2734335978(L_1, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		Il2CppObject * L_2 = (Il2CppObject *)__this->get_current_3();
		return L_2;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m3681948262_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2146457487 * _thisAdjusted = reinterpret_cast<Enumerator_t2146457487 *>(__this + 1);
	return Enumerator_System_Collections_IEnumerator_get_Current_m3681948262(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<System.Object>::Dispose()
extern "C"  void Enumerator_Dispose_m3007748546_gshared (Enumerator_t2146457487 * __this, const MethodInfo* method)
{
	{
		__this->set_l_0((List_1_t257213610 *)NULL);
		return;
	}
}
extern "C"  void Enumerator_Dispose_m3007748546_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2146457487 * _thisAdjusted = reinterpret_cast<Enumerator_t2146457487 *>(__this + 1);
	Enumerator_Dispose_m3007748546(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<System.Object>::VerifyState()
extern "C"  void Enumerator_VerifyState_m2933667029_gshared (Enumerator_t2146457487 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_VerifyState_m2933667029_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		List_1_t257213610 * L_0 = (List_1_t257213610 *)__this->get_l_0();
		if (L_0)
		{
			goto IL_0026;
		}
	}
	{
		Enumerator_t2146457487  L_1 = (*(Enumerator_t2146457487 *)__this);
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_1);
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m88164663((Il2CppObject *)L_2, /*hidden argument*/NULL);
		NullCheck((Type_t *)L_3);
		String_t* L_4 = VirtFuncInvoker0< String_t* >::Invoke(18 /* System.String System.Type::get_FullName() */, (Type_t *)L_3);
		ObjectDisposedException_t21392786 * L_5 = (ObjectDisposedException_t21392786 *)il2cpp_codegen_object_new(ObjectDisposedException_t21392786_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m3603759869(L_5, (String_t*)L_4, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_0026:
	{
		int32_t L_6 = (int32_t)__this->get_ver_2();
		List_1_t257213610 * L_7 = (List_1_t257213610 *)__this->get_l_0();
		NullCheck(L_7);
		int32_t L_8 = (int32_t)L_7->get__version_3();
		if ((((int32_t)L_6) == ((int32_t)L_8)))
		{
			goto IL_0047;
		}
	}
	{
		InvalidOperationException_t56020091 * L_9 = (InvalidOperationException_t56020091 *)il2cpp_codegen_object_new(InvalidOperationException_t56020091_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m237278729(L_9, (String_t*)_stringLiteral1621028992, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_9);
	}

IL_0047:
	{
		return;
	}
}
extern "C"  void Enumerator_VerifyState_m2933667029_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2146457487 * _thisAdjusted = reinterpret_cast<Enumerator_t2146457487 *>(__this + 1);
	Enumerator_VerifyState_m2933667029(_thisAdjusted, method);
}
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.Object>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m2142368520_gshared (Enumerator_t2146457487 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		Enumerator_VerifyState_m2933667029((Enumerator_t2146457487 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		int32_t L_1 = (int32_t)__this->get_next_1();
		List_1_t257213610 * L_2 = (List_1_t257213610 *)__this->get_l_0();
		NullCheck(L_2);
		int32_t L_3 = (int32_t)L_2->get__size_2();
		if ((((int32_t)L_1) >= ((int32_t)L_3)))
		{
			goto IL_0053;
		}
	}
	{
		List_1_t257213610 * L_4 = (List_1_t257213610 *)__this->get_l_0();
		NullCheck(L_4);
		ObjectU5BU5D_t2843939325* L_5 = (ObjectU5BU5D_t2843939325*)L_4->get__items_1();
		int32_t L_6 = (int32_t)__this->get_next_1();
		int32_t L_7 = (int32_t)L_6;
		V_0 = (int32_t)L_7;
		__this->set_next_1(((int32_t)((int32_t)L_7+(int32_t)1)));
		int32_t L_8 = V_0;
		NullCheck(L_5);
		int32_t L_9 = L_8;
		Il2CppObject * L_10 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		__this->set_current_3(L_10);
		return (bool)1;
	}

IL_0053:
	{
		__this->set_next_1((-1));
		return (bool)0;
	}
}
extern "C"  bool Enumerator_MoveNext_m2142368520_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2146457487 * _thisAdjusted = reinterpret_cast<Enumerator_t2146457487 *>(__this + 1);
	return Enumerator_MoveNext_m2142368520(_thisAdjusted, method);
}
// T System.Collections.Generic.List`1/Enumerator<System.Object>::get_Current()
extern "C"  Il2CppObject * Enumerator_get_Current_m470245444_gshared (Enumerator_t2146457487 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_current_3();
		return L_0;
	}
}
extern "C"  Il2CppObject * Enumerator_get_Current_m470245444_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2146457487 * _thisAdjusted = reinterpret_cast<Enumerator_t2146457487 *>(__this + 1);
	return Enumerator_get_Current_m470245444(_thisAdjusted, method);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
