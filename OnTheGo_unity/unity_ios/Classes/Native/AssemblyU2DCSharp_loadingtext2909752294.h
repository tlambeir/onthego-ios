﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour3962482529.h"

// UnityEngine.RectTransform
struct RectTransform_t3704657025;
// UnityEngine.UI.Image
struct Image_t2670269651;
// UnityEngine.UI.Text
struct Text_t1901882714;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// loadingtext
struct  loadingtext_t2909752294  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.RectTransform loadingtext::rectComponent
	RectTransform_t3704657025 * ___rectComponent_2;
	// UnityEngine.UI.Image loadingtext::imageComp
	Image_t2670269651 * ___imageComp_3;
	// System.Single loadingtext::speed
	float ___speed_4;
	// UnityEngine.UI.Text loadingtext::text
	Text_t1901882714 * ___text_5;
	// UnityEngine.UI.Text loadingtext::textNormal
	Text_t1901882714 * ___textNormal_6;

public:
	inline static int32_t get_offset_of_rectComponent_2() { return static_cast<int32_t>(offsetof(loadingtext_t2909752294, ___rectComponent_2)); }
	inline RectTransform_t3704657025 * get_rectComponent_2() const { return ___rectComponent_2; }
	inline RectTransform_t3704657025 ** get_address_of_rectComponent_2() { return &___rectComponent_2; }
	inline void set_rectComponent_2(RectTransform_t3704657025 * value)
	{
		___rectComponent_2 = value;
		Il2CppCodeGenWriteBarrier(&___rectComponent_2, value);
	}

	inline static int32_t get_offset_of_imageComp_3() { return static_cast<int32_t>(offsetof(loadingtext_t2909752294, ___imageComp_3)); }
	inline Image_t2670269651 * get_imageComp_3() const { return ___imageComp_3; }
	inline Image_t2670269651 ** get_address_of_imageComp_3() { return &___imageComp_3; }
	inline void set_imageComp_3(Image_t2670269651 * value)
	{
		___imageComp_3 = value;
		Il2CppCodeGenWriteBarrier(&___imageComp_3, value);
	}

	inline static int32_t get_offset_of_speed_4() { return static_cast<int32_t>(offsetof(loadingtext_t2909752294, ___speed_4)); }
	inline float get_speed_4() const { return ___speed_4; }
	inline float* get_address_of_speed_4() { return &___speed_4; }
	inline void set_speed_4(float value)
	{
		___speed_4 = value;
	}

	inline static int32_t get_offset_of_text_5() { return static_cast<int32_t>(offsetof(loadingtext_t2909752294, ___text_5)); }
	inline Text_t1901882714 * get_text_5() const { return ___text_5; }
	inline Text_t1901882714 ** get_address_of_text_5() { return &___text_5; }
	inline void set_text_5(Text_t1901882714 * value)
	{
		___text_5 = value;
		Il2CppCodeGenWriteBarrier(&___text_5, value);
	}

	inline static int32_t get_offset_of_textNormal_6() { return static_cast<int32_t>(offsetof(loadingtext_t2909752294, ___textNormal_6)); }
	inline Text_t1901882714 * get_textNormal_6() const { return ___textNormal_6; }
	inline Text_t1901882714 ** get_address_of_textNormal_6() { return &___textNormal_6; }
	inline void set_textNormal_6(Text_t1901882714 * value)
	{
		___textNormal_6 = value;
		Il2CppCodeGenWriteBarrier(&___textNormal_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
