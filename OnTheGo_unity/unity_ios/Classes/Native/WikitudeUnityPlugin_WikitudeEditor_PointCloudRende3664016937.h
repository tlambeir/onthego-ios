﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object3080106164.h"

// UnityEngine.Mesh
struct Mesh_t3648964284;
// UnityEngine.Camera
struct Camera_t4157153871;
// UnityEngine.Material
struct Material_t340375123;
// System.Collections.Generic.List`1<UnityEngine.Vector3>
struct List_1_t899420910;
// System.Collections.Generic.List`1<System.Int32>
struct List_1_t128053199;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t1718750761;
// System.Int32[]
struct Int32U5BU5D_t385246372;
// UnityEngine.Color[]
struct ColorU5BU5D_t941916413;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WikitudeEditor.PointCloudRenderer
struct  PointCloudRenderer_t3664016937  : public Il2CppObject
{
public:
	// UnityEngine.Mesh WikitudeEditor.PointCloudRenderer::_testMesh
	Mesh_t3648964284 * ____testMesh_0;
	// UnityEngine.Camera WikitudeEditor.PointCloudRenderer::_sceneCamera
	Camera_t4157153871 * ____sceneCamera_1;
	// UnityEngine.Material WikitudeEditor.PointCloudRenderer::_renderMaterial
	Material_t340375123 * ____renderMaterial_2;
	// System.Collections.Generic.List`1<UnityEngine.Vector3> WikitudeEditor.PointCloudRenderer::_cubePoints
	List_1_t899420910 * ____cubePoints_3;
	// System.Collections.Generic.List`1<System.Int32> WikitudeEditor.PointCloudRenderer::_cubeIndices
	List_1_t128053199 * ____cubeIndices_4;
	// System.Int32 WikitudeEditor.PointCloudRenderer::_meshVertexCount
	int32_t ____meshVertexCount_5;
	// System.Boolean WikitudeEditor.PointCloudRenderer::_drawWithCommandBuffer
	bool ____drawWithCommandBuffer_6;
	// UnityEngine.Vector3[] WikitudeEditor.PointCloudRenderer::_vertices
	Vector3U5BU5D_t1718750761* ____vertices_7;
	// System.Int32[] WikitudeEditor.PointCloudRenderer::_indices
	Int32U5BU5D_t385246372* ____indices_8;
	// UnityEngine.Color[] WikitudeEditor.PointCloudRenderer::_vertexColors
	ColorU5BU5D_t941916413* ____vertexColors_9;

public:
	inline static int32_t get_offset_of__testMesh_0() { return static_cast<int32_t>(offsetof(PointCloudRenderer_t3664016937, ____testMesh_0)); }
	inline Mesh_t3648964284 * get__testMesh_0() const { return ____testMesh_0; }
	inline Mesh_t3648964284 ** get_address_of__testMesh_0() { return &____testMesh_0; }
	inline void set__testMesh_0(Mesh_t3648964284 * value)
	{
		____testMesh_0 = value;
		Il2CppCodeGenWriteBarrier(&____testMesh_0, value);
	}

	inline static int32_t get_offset_of__sceneCamera_1() { return static_cast<int32_t>(offsetof(PointCloudRenderer_t3664016937, ____sceneCamera_1)); }
	inline Camera_t4157153871 * get__sceneCamera_1() const { return ____sceneCamera_1; }
	inline Camera_t4157153871 ** get_address_of__sceneCamera_1() { return &____sceneCamera_1; }
	inline void set__sceneCamera_1(Camera_t4157153871 * value)
	{
		____sceneCamera_1 = value;
		Il2CppCodeGenWriteBarrier(&____sceneCamera_1, value);
	}

	inline static int32_t get_offset_of__renderMaterial_2() { return static_cast<int32_t>(offsetof(PointCloudRenderer_t3664016937, ____renderMaterial_2)); }
	inline Material_t340375123 * get__renderMaterial_2() const { return ____renderMaterial_2; }
	inline Material_t340375123 ** get_address_of__renderMaterial_2() { return &____renderMaterial_2; }
	inline void set__renderMaterial_2(Material_t340375123 * value)
	{
		____renderMaterial_2 = value;
		Il2CppCodeGenWriteBarrier(&____renderMaterial_2, value);
	}

	inline static int32_t get_offset_of__cubePoints_3() { return static_cast<int32_t>(offsetof(PointCloudRenderer_t3664016937, ____cubePoints_3)); }
	inline List_1_t899420910 * get__cubePoints_3() const { return ____cubePoints_3; }
	inline List_1_t899420910 ** get_address_of__cubePoints_3() { return &____cubePoints_3; }
	inline void set__cubePoints_3(List_1_t899420910 * value)
	{
		____cubePoints_3 = value;
		Il2CppCodeGenWriteBarrier(&____cubePoints_3, value);
	}

	inline static int32_t get_offset_of__cubeIndices_4() { return static_cast<int32_t>(offsetof(PointCloudRenderer_t3664016937, ____cubeIndices_4)); }
	inline List_1_t128053199 * get__cubeIndices_4() const { return ____cubeIndices_4; }
	inline List_1_t128053199 ** get_address_of__cubeIndices_4() { return &____cubeIndices_4; }
	inline void set__cubeIndices_4(List_1_t128053199 * value)
	{
		____cubeIndices_4 = value;
		Il2CppCodeGenWriteBarrier(&____cubeIndices_4, value);
	}

	inline static int32_t get_offset_of__meshVertexCount_5() { return static_cast<int32_t>(offsetof(PointCloudRenderer_t3664016937, ____meshVertexCount_5)); }
	inline int32_t get__meshVertexCount_5() const { return ____meshVertexCount_5; }
	inline int32_t* get_address_of__meshVertexCount_5() { return &____meshVertexCount_5; }
	inline void set__meshVertexCount_5(int32_t value)
	{
		____meshVertexCount_5 = value;
	}

	inline static int32_t get_offset_of__drawWithCommandBuffer_6() { return static_cast<int32_t>(offsetof(PointCloudRenderer_t3664016937, ____drawWithCommandBuffer_6)); }
	inline bool get__drawWithCommandBuffer_6() const { return ____drawWithCommandBuffer_6; }
	inline bool* get_address_of__drawWithCommandBuffer_6() { return &____drawWithCommandBuffer_6; }
	inline void set__drawWithCommandBuffer_6(bool value)
	{
		____drawWithCommandBuffer_6 = value;
	}

	inline static int32_t get_offset_of__vertices_7() { return static_cast<int32_t>(offsetof(PointCloudRenderer_t3664016937, ____vertices_7)); }
	inline Vector3U5BU5D_t1718750761* get__vertices_7() const { return ____vertices_7; }
	inline Vector3U5BU5D_t1718750761** get_address_of__vertices_7() { return &____vertices_7; }
	inline void set__vertices_7(Vector3U5BU5D_t1718750761* value)
	{
		____vertices_7 = value;
		Il2CppCodeGenWriteBarrier(&____vertices_7, value);
	}

	inline static int32_t get_offset_of__indices_8() { return static_cast<int32_t>(offsetof(PointCloudRenderer_t3664016937, ____indices_8)); }
	inline Int32U5BU5D_t385246372* get__indices_8() const { return ____indices_8; }
	inline Int32U5BU5D_t385246372** get_address_of__indices_8() { return &____indices_8; }
	inline void set__indices_8(Int32U5BU5D_t385246372* value)
	{
		____indices_8 = value;
		Il2CppCodeGenWriteBarrier(&____indices_8, value);
	}

	inline static int32_t get_offset_of__vertexColors_9() { return static_cast<int32_t>(offsetof(PointCloudRenderer_t3664016937, ____vertexColors_9)); }
	inline ColorU5BU5D_t941916413* get__vertexColors_9() const { return ____vertexColors_9; }
	inline ColorU5BU5D_t941916413** get_address_of__vertexColors_9() { return &____vertexColors_9; }
	inline void set__vertexColors_9(ColorU5BU5D_t941916413* value)
	{
		____vertexColors_9 = value;
		Il2CppCodeGenWriteBarrier(&____vertexColors_9, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
