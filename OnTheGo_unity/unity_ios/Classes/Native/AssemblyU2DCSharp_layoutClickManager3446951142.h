﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour3962482529.h"

// LeftNav
struct LeftNav_t2375583198;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// layoutClickManager
struct  layoutClickManager_t3446951142  : public MonoBehaviour_t3962482529
{
public:
	// LeftNav layoutClickManager::leftNav
	LeftNav_t2375583198 * ___leftNav_2;

public:
	inline static int32_t get_offset_of_leftNav_2() { return static_cast<int32_t>(offsetof(layoutClickManager_t3446951142, ___leftNav_2)); }
	inline LeftNav_t2375583198 * get_leftNav_2() const { return ___leftNav_2; }
	inline LeftNav_t2375583198 ** get_address_of_leftNav_2() { return &___leftNav_2; }
	inline void set_leftNav_2(LeftNav_t2375583198 * value)
	{
		___leftNav_2 = value;
		Il2CppCodeGenWriteBarrier(&___leftNav_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
