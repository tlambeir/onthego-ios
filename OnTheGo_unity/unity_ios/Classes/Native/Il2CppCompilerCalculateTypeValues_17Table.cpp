﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UI_UnityEngine_UI_DefaultControls_Reso1597885468.h"
#include "UnityEngine_UI_UnityEngine_UI_Dropdown2274391225.h"
#include "UnityEngine_UI_UnityEngine_UI_Dropdown_DropdownIte1451952895.h"
#include "UnityEngine_UI_UnityEngine_UI_Dropdown_OptionData3270282352.h"
#include "UnityEngine_UI_UnityEngine_UI_Dropdown_OptionDataL1438173104.h"
#include "UnityEngine_UI_UnityEngine_UI_Dropdown_DropdownEve4040729994.h"
#include "UnityEngine_UI_UnityEngine_UI_Dropdown_U3CShowU3Ec1106527198.h"
#include "UnityEngine_UI_UnityEngine_UI_Dropdown_U3CDelayedD3853912249.h"
#include "UnityEngine_UI_UnityEngine_UI_FontData746620069.h"
#include "UnityEngine_UI_UnityEngine_UI_FontUpdateTracker1839077620.h"
#include "UnityEngine_UI_UnityEngine_UI_Graphic1660335611.h"
#include "UnityEngine_UI_UnityEngine_UI_GraphicRaycaster2999697109.h"
#include "UnityEngine_UI_UnityEngine_UI_GraphicRaycaster_Bloc612090948.h"
#include "UnityEngine_UI_UnityEngine_UI_GraphicRegistry3479976429.h"
#include "UnityEngine_UI_UnityEngine_UI_Image2670269651.h"
#include "UnityEngine_UI_UnityEngine_UI_Image_Type1152881528.h"
#include "UnityEngine_UI_UnityEngine_UI_Image_FillMethod1167457570.h"
#include "UnityEngine_UI_UnityEngine_UI_Image_OriginHorizont1174417785.h"
#include "UnityEngine_UI_UnityEngine_UI_Image_OriginVertical2256455259.h"
#include "UnityEngine_UI_UnityEngine_UI_Image_Origin901855765812.h"
#include "UnityEngine_UI_UnityEngine_UI_Image_Origin180325369132.h"
#include "UnityEngine_UI_UnityEngine_UI_Image_Origin360707706162.h"
#include "UnityEngine_UI_UnityEngine_UI_InputField3762917431.h"
#include "UnityEngine_UI_UnityEngine_UI_InputField_ContentTy1787303396.h"
#include "UnityEngine_UI_UnityEngine_UI_InputField_InputType1770400679.h"
#include "UnityEngine_UI_UnityEngine_UI_InputField_Character4051914437.h"
#include "UnityEngine_UI_UnityEngine_UI_InputField_LineType4214648469.h"
#include "UnityEngine_UI_UnityEngine_UI_InputField_OnValidat2355412304.h"
#include "UnityEngine_UI_UnityEngine_UI_InputField_SubmitEven648412432.h"
#include "UnityEngine_UI_UnityEngine_UI_InputField_OnChangeEv467195904.h"
#include "UnityEngine_UI_UnityEngine_UI_InputField_EditState3741896775.h"
#include "UnityEngine_UI_UnityEngine_UI_InputField_U3CCaretB2589889038.h"
#include "UnityEngine_UI_UnityEngine_UI_InputField_U3CMouseD3909241878.h"
#include "UnityEngine_UI_UnityEngine_UI_Mask1803652131.h"
#include "UnityEngine_UI_UnityEngine_UI_MaskableGraphic3839221559.h"
#include "UnityEngine_UI_UnityEngine_UI_MaskableGraphic_Cull3661388177.h"
#include "UnityEngine_UI_UnityEngine_UI_MaskUtilities4151184739.h"
#include "UnityEngine_UI_UnityEngine_UI_Misc1447421763.h"
#include "UnityEngine_UI_UnityEngine_UI_Navigation3049316579.h"
#include "UnityEngine_UI_UnityEngine_UI_Navigation_Mode1066900953.h"
#include "UnityEngine_UI_UnityEngine_UI_RawImage3182918964.h"
#include "UnityEngine_UI_UnityEngine_UI_RectMask2D3474889437.h"
#include "UnityEngine_UI_UnityEngine_UI_Scrollbar1494447233.h"
#include "UnityEngine_UI_UnityEngine_UI_Scrollbar_Direction3470714353.h"
#include "UnityEngine_UI_UnityEngine_UI_Scrollbar_ScrollEvent149898510.h"
#include "UnityEngine_UI_UnityEngine_UI_Scrollbar_Axis1697763317.h"
#include "UnityEngine_UI_UnityEngine_UI_Scrollbar_U3CClickRe3442648935.h"
#include "UnityEngine_UI_UnityEngine_UI_ScrollRect4137855814.h"
#include "UnityEngine_UI_UnityEngine_UI_ScrollRect_MovementT4072922106.h"
#include "UnityEngine_UI_UnityEngine_UI_ScrollRect_ScrollbarV705693775.h"
#include "UnityEngine_UI_UnityEngine_UI_ScrollRect_ScrollRect343079324.h"
#include "UnityEngine_UI_UnityEngine_UI_Selectable3250028441.h"
#include "UnityEngine_UI_UnityEngine_UI_Selectable_Transitio1769908631.h"
#include "UnityEngine_UI_UnityEngine_UI_Selectable_Selection2656606514.h"
#include "UnityEngine_UI_UnityEngine_UI_SetPropertyUtility3359423571.h"
#include "UnityEngine_UI_UnityEngine_UI_Slider3903728902.h"
#include "UnityEngine_UI_UnityEngine_UI_Slider_Direction337909235.h"
#include "UnityEngine_UI_UnityEngine_UI_Slider_SliderEvent3180273144.h"
#include "UnityEngine_UI_UnityEngine_UI_Slider_Axis809944411.h"
#include "UnityEngine_UI_UnityEngine_UI_SpriteState1362986479.h"
#include "UnityEngine_UI_UnityEngine_UI_StencilMaterial3850132571.h"
#include "UnityEngine_UI_UnityEngine_UI_StencilMaterial_MatE2957107092.h"
#include "UnityEngine_UI_UnityEngine_UI_Text1901882714.h"
#include "UnityEngine_UI_UnityEngine_UI_Toggle2735377061.h"
#include "UnityEngine_UI_UnityEngine_UI_Toggle_ToggleTransit3587297765.h"
#include "UnityEngine_UI_UnityEngine_UI_Toggle_ToggleEvent1873685584.h"
#include "UnityEngine_UI_UnityEngine_UI_ToggleGroup123837990.h"
#include "UnityEngine_UI_UnityEngine_UI_ClipperRegistry2428680409.h"
#include "UnityEngine_UI_UnityEngine_UI_Clipping312708592.h"
#include "UnityEngine_UI_UnityEngine_UI_RectangularVertexClip626611136.h"
#include "UnityEngine_UI_UnityEngine_UI_AspectRatioFitter3312407083.h"
#include "UnityEngine_UI_UnityEngine_UI_AspectRatioFitter_As3417192999.h"
#include "UnityEngine_UI_UnityEngine_UI_CanvasScaler2767979955.h"
#include "UnityEngine_UI_UnityEngine_UI_CanvasScaler_ScaleMo2604066427.h"
#include "UnityEngine_UI_UnityEngine_UI_CanvasScaler_ScreenM3675272090.h"
#include "UnityEngine_UI_UnityEngine_UI_CanvasScaler_Unit2218508340.h"
#include "UnityEngine_UI_UnityEngine_UI_ContentSizeFitter3850442145.h"
#include "UnityEngine_UI_UnityEngine_UI_ContentSizeFitter_Fi3267881214.h"
#include "UnityEngine_UI_UnityEngine_UI_GridLayoutGroup3046220461.h"
#include "UnityEngine_UI_UnityEngine_UI_GridLayoutGroup_Corn1493259673.h"
#include "UnityEngine_UI_UnityEngine_UI_GridLayoutGroup_Axis3613393006.h"
#include "UnityEngine_UI_UnityEngine_UI_GridLayoutGroup_Const814224393.h"
#include "UnityEngine_UI_UnityEngine_UI_HorizontalLayoutGrou2586782146.h"
#include "UnityEngine_UI_UnityEngine_UI_HorizontalOrVerticalL729725570.h"
#include "UnityEngine_UI_UnityEngine_UI_LayoutElement1785403678.h"
#include "UnityEngine_UI_UnityEngine_UI_LayoutGroup2436138090.h"
#include "UnityEngine_UI_UnityEngine_UI_LayoutGroup_U3CDelay3170500204.h"
#include "UnityEngine_UI_UnityEngine_UI_LayoutRebuilder541313304.h"
#include "UnityEngine_UI_UnityEngine_UI_LayoutUtility2745813735.h"
#include "UnityEngine_UI_UnityEngine_UI_VerticalLayoutGroup923838031.h"







#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1700 = { sizeof (Resources_t1597885468)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1700[7] = 
{
	Resources_t1597885468::get_offset_of_standard_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Resources_t1597885468::get_offset_of_background_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Resources_t1597885468::get_offset_of_inputField_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Resources_t1597885468::get_offset_of_knob_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Resources_t1597885468::get_offset_of_checkmark_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Resources_t1597885468::get_offset_of_dropdown_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Resources_t1597885468::get_offset_of_mask_6() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1701 = { sizeof (Dropdown_t2274391225), -1, sizeof(Dropdown_t2274391225_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1701[14] = 
{
	Dropdown_t2274391225::get_offset_of_m_Template_16(),
	Dropdown_t2274391225::get_offset_of_m_CaptionText_17(),
	Dropdown_t2274391225::get_offset_of_m_CaptionImage_18(),
	Dropdown_t2274391225::get_offset_of_m_ItemText_19(),
	Dropdown_t2274391225::get_offset_of_m_ItemImage_20(),
	Dropdown_t2274391225::get_offset_of_m_Value_21(),
	Dropdown_t2274391225::get_offset_of_m_Options_22(),
	Dropdown_t2274391225::get_offset_of_m_OnValueChanged_23(),
	Dropdown_t2274391225::get_offset_of_m_Dropdown_24(),
	Dropdown_t2274391225::get_offset_of_m_Blocker_25(),
	Dropdown_t2274391225::get_offset_of_m_Items_26(),
	Dropdown_t2274391225::get_offset_of_m_AlphaTweenRunner_27(),
	Dropdown_t2274391225::get_offset_of_validTemplate_28(),
	Dropdown_t2274391225_StaticFields::get_offset_of_s_NoOptionData_29(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1702 = { sizeof (DropdownItem_t1451952895), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1702[4] = 
{
	DropdownItem_t1451952895::get_offset_of_m_Text_2(),
	DropdownItem_t1451952895::get_offset_of_m_Image_3(),
	DropdownItem_t1451952895::get_offset_of_m_RectTransform_4(),
	DropdownItem_t1451952895::get_offset_of_m_Toggle_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1703 = { sizeof (OptionData_t3270282352), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1703[2] = 
{
	OptionData_t3270282352::get_offset_of_m_Text_0(),
	OptionData_t3270282352::get_offset_of_m_Image_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1704 = { sizeof (OptionDataList_t1438173104), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1704[1] = 
{
	OptionDataList_t1438173104::get_offset_of_m_Options_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1705 = { sizeof (DropdownEvent_t4040729994), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1706 = { sizeof (U3CShowU3Ec__AnonStorey1_t1106527198), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1706[2] = 
{
	U3CShowU3Ec__AnonStorey1_t1106527198::get_offset_of_item_0(),
	U3CShowU3Ec__AnonStorey1_t1106527198::get_offset_of_U24this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1707 = { sizeof (U3CDelayedDestroyDropdownListU3Ec__Iterator0_t3853912249), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1707[5] = 
{
	U3CDelayedDestroyDropdownListU3Ec__Iterator0_t3853912249::get_offset_of_delay_0(),
	U3CDelayedDestroyDropdownListU3Ec__Iterator0_t3853912249::get_offset_of_U24this_1(),
	U3CDelayedDestroyDropdownListU3Ec__Iterator0_t3853912249::get_offset_of_U24current_2(),
	U3CDelayedDestroyDropdownListU3Ec__Iterator0_t3853912249::get_offset_of_U24disposing_3(),
	U3CDelayedDestroyDropdownListU3Ec__Iterator0_t3853912249::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1708 = { sizeof (FontData_t746620069), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1708[12] = 
{
	FontData_t746620069::get_offset_of_m_Font_0(),
	FontData_t746620069::get_offset_of_m_FontSize_1(),
	FontData_t746620069::get_offset_of_m_FontStyle_2(),
	FontData_t746620069::get_offset_of_m_BestFit_3(),
	FontData_t746620069::get_offset_of_m_MinSize_4(),
	FontData_t746620069::get_offset_of_m_MaxSize_5(),
	FontData_t746620069::get_offset_of_m_Alignment_6(),
	FontData_t746620069::get_offset_of_m_AlignByGeometry_7(),
	FontData_t746620069::get_offset_of_m_RichText_8(),
	FontData_t746620069::get_offset_of_m_HorizontalOverflow_9(),
	FontData_t746620069::get_offset_of_m_VerticalOverflow_10(),
	FontData_t746620069::get_offset_of_m_LineSpacing_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1709 = { sizeof (FontUpdateTracker_t1839077620), -1, sizeof(FontUpdateTracker_t1839077620_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1709[3] = 
{
	FontUpdateTracker_t1839077620_StaticFields::get_offset_of_m_Tracked_0(),
	FontUpdateTracker_t1839077620_StaticFields::get_offset_of_U3CU3Ef__mgU24cache0_1(),
	FontUpdateTracker_t1839077620_StaticFields::get_offset_of_U3CU3Ef__mgU24cache1_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1710 = { sizeof (Graphic_t1660335611), -1, sizeof(Graphic_t1660335611_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1710[17] = 
{
	Graphic_t1660335611_StaticFields::get_offset_of_s_DefaultUI_2(),
	Graphic_t1660335611_StaticFields::get_offset_of_s_WhiteTexture_3(),
	Graphic_t1660335611::get_offset_of_m_Material_4(),
	Graphic_t1660335611::get_offset_of_m_Color_5(),
	Graphic_t1660335611::get_offset_of_m_RaycastTarget_6(),
	Graphic_t1660335611::get_offset_of_m_RectTransform_7(),
	Graphic_t1660335611::get_offset_of_m_CanvasRender_8(),
	Graphic_t1660335611::get_offset_of_m_Canvas_9(),
	Graphic_t1660335611::get_offset_of_m_VertsDirty_10(),
	Graphic_t1660335611::get_offset_of_m_MaterialDirty_11(),
	Graphic_t1660335611::get_offset_of_m_OnDirtyLayoutCallback_12(),
	Graphic_t1660335611::get_offset_of_m_OnDirtyVertsCallback_13(),
	Graphic_t1660335611::get_offset_of_m_OnDirtyMaterialCallback_14(),
	Graphic_t1660335611_StaticFields::get_offset_of_s_Mesh_15(),
	Graphic_t1660335611_StaticFields::get_offset_of_s_VertexHelper_16(),
	Graphic_t1660335611::get_offset_of_m_ColorTweenRunner_17(),
	Graphic_t1660335611::get_offset_of_U3CuseLegacyMeshGenerationU3Ek__BackingField_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1711 = { sizeof (GraphicRaycaster_t2999697109), -1, sizeof(GraphicRaycaster_t2999697109_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1711[8] = 
{
	0,
	GraphicRaycaster_t2999697109::get_offset_of_m_IgnoreReversedGraphics_3(),
	GraphicRaycaster_t2999697109::get_offset_of_m_BlockingObjects_4(),
	GraphicRaycaster_t2999697109::get_offset_of_m_BlockingMask_5(),
	GraphicRaycaster_t2999697109::get_offset_of_m_Canvas_6(),
	GraphicRaycaster_t2999697109::get_offset_of_m_RaycastResults_7(),
	GraphicRaycaster_t2999697109_StaticFields::get_offset_of_s_SortedGraphics_8(),
	GraphicRaycaster_t2999697109_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1712 = { sizeof (BlockingObjects_t612090948)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1712[5] = 
{
	BlockingObjects_t612090948::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1713 = { sizeof (GraphicRegistry_t3479976429), -1, sizeof(GraphicRegistry_t3479976429_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1713[3] = 
{
	GraphicRegistry_t3479976429_StaticFields::get_offset_of_s_Instance_0(),
	GraphicRegistry_t3479976429::get_offset_of_m_Graphics_1(),
	GraphicRegistry_t3479976429_StaticFields::get_offset_of_s_EmptyList_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1714 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1715 = { sizeof (Image_t2670269651), -1, sizeof(Image_t2670269651_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1715[15] = 
{
	Image_t2670269651_StaticFields::get_offset_of_s_ETC1DefaultUI_28(),
	Image_t2670269651::get_offset_of_m_Sprite_29(),
	Image_t2670269651::get_offset_of_m_OverrideSprite_30(),
	Image_t2670269651::get_offset_of_m_Type_31(),
	Image_t2670269651::get_offset_of_m_PreserveAspect_32(),
	Image_t2670269651::get_offset_of_m_FillCenter_33(),
	Image_t2670269651::get_offset_of_m_FillMethod_34(),
	Image_t2670269651::get_offset_of_m_FillAmount_35(),
	Image_t2670269651::get_offset_of_m_FillClockwise_36(),
	Image_t2670269651::get_offset_of_m_FillOrigin_37(),
	Image_t2670269651::get_offset_of_m_AlphaHitTestMinimumThreshold_38(),
	Image_t2670269651_StaticFields::get_offset_of_s_VertScratch_39(),
	Image_t2670269651_StaticFields::get_offset_of_s_UVScratch_40(),
	Image_t2670269651_StaticFields::get_offset_of_s_Xy_41(),
	Image_t2670269651_StaticFields::get_offset_of_s_Uv_42(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1716 = { sizeof (Type_t1152881528)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1716[5] = 
{
	Type_t1152881528::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1717 = { sizeof (FillMethod_t1167457570)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1717[6] = 
{
	FillMethod_t1167457570::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1718 = { sizeof (OriginHorizontal_t1174417785)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1718[3] = 
{
	OriginHorizontal_t1174417785::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1719 = { sizeof (OriginVertical_t2256455259)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1719[3] = 
{
	OriginVertical_t2256455259::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1720 = { sizeof (Origin90_t1855765812)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1720[5] = 
{
	Origin90_t1855765812::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1721 = { sizeof (Origin180_t325369132)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1721[5] = 
{
	Origin180_t325369132::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1722 = { sizeof (Origin360_t707706162)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1722[5] = 
{
	Origin360_t707706162::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1723 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1724 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1725 = { sizeof (InputField_t3762917431), -1, sizeof(InputField_t3762917431_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1725[47] = 
{
	InputField_t3762917431::get_offset_of_m_Keyboard_16(),
	InputField_t3762917431_StaticFields::get_offset_of_kSeparators_17(),
	InputField_t3762917431::get_offset_of_m_TextComponent_18(),
	InputField_t3762917431::get_offset_of_m_Placeholder_19(),
	InputField_t3762917431::get_offset_of_m_ContentType_20(),
	InputField_t3762917431::get_offset_of_m_InputType_21(),
	InputField_t3762917431::get_offset_of_m_AsteriskChar_22(),
	InputField_t3762917431::get_offset_of_m_KeyboardType_23(),
	InputField_t3762917431::get_offset_of_m_LineType_24(),
	InputField_t3762917431::get_offset_of_m_HideMobileInput_25(),
	InputField_t3762917431::get_offset_of_m_CharacterValidation_26(),
	InputField_t3762917431::get_offset_of_m_CharacterLimit_27(),
	InputField_t3762917431::get_offset_of_m_OnEndEdit_28(),
	InputField_t3762917431::get_offset_of_m_OnValueChanged_29(),
	InputField_t3762917431::get_offset_of_m_OnValidateInput_30(),
	InputField_t3762917431::get_offset_of_m_CaretColor_31(),
	InputField_t3762917431::get_offset_of_m_CustomCaretColor_32(),
	InputField_t3762917431::get_offset_of_m_SelectionColor_33(),
	InputField_t3762917431::get_offset_of_m_Text_34(),
	InputField_t3762917431::get_offset_of_m_CaretBlinkRate_35(),
	InputField_t3762917431::get_offset_of_m_CaretWidth_36(),
	InputField_t3762917431::get_offset_of_m_ReadOnly_37(),
	InputField_t3762917431::get_offset_of_m_CaretPosition_38(),
	InputField_t3762917431::get_offset_of_m_CaretSelectPosition_39(),
	InputField_t3762917431::get_offset_of_caretRectTrans_40(),
	InputField_t3762917431::get_offset_of_m_CursorVerts_41(),
	InputField_t3762917431::get_offset_of_m_InputTextCache_42(),
	InputField_t3762917431::get_offset_of_m_CachedInputRenderer_43(),
	InputField_t3762917431::get_offset_of_m_PreventFontCallback_44(),
	InputField_t3762917431::get_offset_of_m_Mesh_45(),
	InputField_t3762917431::get_offset_of_m_AllowInput_46(),
	InputField_t3762917431::get_offset_of_m_ShouldActivateNextUpdate_47(),
	InputField_t3762917431::get_offset_of_m_UpdateDrag_48(),
	InputField_t3762917431::get_offset_of_m_DragPositionOutOfBounds_49(),
	0,
	0,
	InputField_t3762917431::get_offset_of_m_CaretVisible_52(),
	InputField_t3762917431::get_offset_of_m_BlinkCoroutine_53(),
	InputField_t3762917431::get_offset_of_m_BlinkStartTime_54(),
	InputField_t3762917431::get_offset_of_m_DrawStart_55(),
	InputField_t3762917431::get_offset_of_m_DrawEnd_56(),
	InputField_t3762917431::get_offset_of_m_DragCoroutine_57(),
	InputField_t3762917431::get_offset_of_m_OriginalText_58(),
	InputField_t3762917431::get_offset_of_m_WasCanceled_59(),
	InputField_t3762917431::get_offset_of_m_HasDoneFocusTransition_60(),
	0,
	InputField_t3762917431::get_offset_of_m_ProcessingEvent_62(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1726 = { sizeof (ContentType_t1787303396)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1726[11] = 
{
	ContentType_t1787303396::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1727 = { sizeof (InputType_t1770400679)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1727[4] = 
{
	InputType_t1770400679::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1728 = { sizeof (CharacterValidation_t4051914437)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1728[7] = 
{
	CharacterValidation_t4051914437::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1729 = { sizeof (LineType_t4214648469)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1729[4] = 
{
	LineType_t4214648469::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1730 = { sizeof (OnValidateInput_t2355412304), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1731 = { sizeof (SubmitEvent_t648412432), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1732 = { sizeof (OnChangeEvent_t467195904), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1733 = { sizeof (EditState_t3741896775)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1733[3] = 
{
	EditState_t3741896775::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1734 = { sizeof (U3CCaretBlinkU3Ec__Iterator0_t2589889038), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1734[6] = 
{
	U3CCaretBlinkU3Ec__Iterator0_t2589889038::get_offset_of_U3CblinkPeriodU3E__1_0(),
	U3CCaretBlinkU3Ec__Iterator0_t2589889038::get_offset_of_U3CblinkStateU3E__1_1(),
	U3CCaretBlinkU3Ec__Iterator0_t2589889038::get_offset_of_U24this_2(),
	U3CCaretBlinkU3Ec__Iterator0_t2589889038::get_offset_of_U24current_3(),
	U3CCaretBlinkU3Ec__Iterator0_t2589889038::get_offset_of_U24disposing_4(),
	U3CCaretBlinkU3Ec__Iterator0_t2589889038::get_offset_of_U24PC_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1735 = { sizeof (U3CMouseDragOutsideRectU3Ec__Iterator1_t3909241878), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1735[8] = 
{
	U3CMouseDragOutsideRectU3Ec__Iterator1_t3909241878::get_offset_of_eventData_0(),
	U3CMouseDragOutsideRectU3Ec__Iterator1_t3909241878::get_offset_of_U3ClocalMousePosU3E__1_1(),
	U3CMouseDragOutsideRectU3Ec__Iterator1_t3909241878::get_offset_of_U3CrectU3E__1_2(),
	U3CMouseDragOutsideRectU3Ec__Iterator1_t3909241878::get_offset_of_U3CdelayU3E__1_3(),
	U3CMouseDragOutsideRectU3Ec__Iterator1_t3909241878::get_offset_of_U24this_4(),
	U3CMouseDragOutsideRectU3Ec__Iterator1_t3909241878::get_offset_of_U24current_5(),
	U3CMouseDragOutsideRectU3Ec__Iterator1_t3909241878::get_offset_of_U24disposing_6(),
	U3CMouseDragOutsideRectU3Ec__Iterator1_t3909241878::get_offset_of_U24PC_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1736 = { sizeof (Mask_t1803652131), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1736[5] = 
{
	Mask_t1803652131::get_offset_of_m_RectTransform_2(),
	Mask_t1803652131::get_offset_of_m_ShowMaskGraphic_3(),
	Mask_t1803652131::get_offset_of_m_Graphic_4(),
	Mask_t1803652131::get_offset_of_m_MaskMaterial_5(),
	Mask_t1803652131::get_offset_of_m_UnmaskMaterial_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1737 = { sizeof (MaskableGraphic_t3839221559), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1737[9] = 
{
	MaskableGraphic_t3839221559::get_offset_of_m_ShouldRecalculateStencil_19(),
	MaskableGraphic_t3839221559::get_offset_of_m_MaskMaterial_20(),
	MaskableGraphic_t3839221559::get_offset_of_m_ParentMask_21(),
	MaskableGraphic_t3839221559::get_offset_of_m_Maskable_22(),
	MaskableGraphic_t3839221559::get_offset_of_m_IncludeForMasking_23(),
	MaskableGraphic_t3839221559::get_offset_of_m_OnCullStateChanged_24(),
	MaskableGraphic_t3839221559::get_offset_of_m_ShouldRecalculate_25(),
	MaskableGraphic_t3839221559::get_offset_of_m_StencilValue_26(),
	MaskableGraphic_t3839221559::get_offset_of_m_Corners_27(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1738 = { sizeof (CullStateChangedEvent_t3661388177), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1739 = { sizeof (MaskUtilities_t4151184739), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1740 = { sizeof (Misc_t1447421763), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1741 = { sizeof (Navigation_t3049316579)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1741[5] = 
{
	Navigation_t3049316579::get_offset_of_m_Mode_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Navigation_t3049316579::get_offset_of_m_SelectOnUp_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Navigation_t3049316579::get_offset_of_m_SelectOnDown_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Navigation_t3049316579::get_offset_of_m_SelectOnLeft_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Navigation_t3049316579::get_offset_of_m_SelectOnRight_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1742 = { sizeof (Mode_t1066900953)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1742[6] = 
{
	Mode_t1066900953::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1743 = { sizeof (RawImage_t3182918964), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1743[2] = 
{
	RawImage_t3182918964::get_offset_of_m_Texture_28(),
	RawImage_t3182918964::get_offset_of_m_UVRect_29(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1744 = { sizeof (RectMask2D_t3474889437), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1744[8] = 
{
	RectMask2D_t3474889437::get_offset_of_m_VertexClipper_2(),
	RectMask2D_t3474889437::get_offset_of_m_RectTransform_3(),
	RectMask2D_t3474889437::get_offset_of_m_ClipTargets_4(),
	RectMask2D_t3474889437::get_offset_of_m_ShouldRecalculateClipRects_5(),
	RectMask2D_t3474889437::get_offset_of_m_Clippers_6(),
	RectMask2D_t3474889437::get_offset_of_m_LastClipRectCanvasSpace_7(),
	RectMask2D_t3474889437::get_offset_of_m_LastValidClipRect_8(),
	RectMask2D_t3474889437::get_offset_of_m_ForceClip_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1745 = { sizeof (Scrollbar_t1494447233), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1745[11] = 
{
	Scrollbar_t1494447233::get_offset_of_m_HandleRect_16(),
	Scrollbar_t1494447233::get_offset_of_m_Direction_17(),
	Scrollbar_t1494447233::get_offset_of_m_Value_18(),
	Scrollbar_t1494447233::get_offset_of_m_Size_19(),
	Scrollbar_t1494447233::get_offset_of_m_NumberOfSteps_20(),
	Scrollbar_t1494447233::get_offset_of_m_OnValueChanged_21(),
	Scrollbar_t1494447233::get_offset_of_m_ContainerRect_22(),
	Scrollbar_t1494447233::get_offset_of_m_Offset_23(),
	Scrollbar_t1494447233::get_offset_of_m_Tracker_24(),
	Scrollbar_t1494447233::get_offset_of_m_PointerDownRepeat_25(),
	Scrollbar_t1494447233::get_offset_of_isPointerDownAndNotDragging_26(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1746 = { sizeof (Direction_t3470714353)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1746[5] = 
{
	Direction_t3470714353::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1747 = { sizeof (ScrollEvent_t149898510), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1748 = { sizeof (Axis_t1697763317)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1748[3] = 
{
	Axis_t1697763317::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1749 = { sizeof (U3CClickRepeatU3Ec__Iterator0_t3442648935), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1749[5] = 
{
	U3CClickRepeatU3Ec__Iterator0_t3442648935::get_offset_of_eventData_0(),
	U3CClickRepeatU3Ec__Iterator0_t3442648935::get_offset_of_U24this_1(),
	U3CClickRepeatU3Ec__Iterator0_t3442648935::get_offset_of_U24current_2(),
	U3CClickRepeatU3Ec__Iterator0_t3442648935::get_offset_of_U24disposing_3(),
	U3CClickRepeatU3Ec__Iterator0_t3442648935::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1750 = { sizeof (ScrollRect_t4137855814), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1750[36] = 
{
	ScrollRect_t4137855814::get_offset_of_m_Content_2(),
	ScrollRect_t4137855814::get_offset_of_m_Horizontal_3(),
	ScrollRect_t4137855814::get_offset_of_m_Vertical_4(),
	ScrollRect_t4137855814::get_offset_of_m_MovementType_5(),
	ScrollRect_t4137855814::get_offset_of_m_Elasticity_6(),
	ScrollRect_t4137855814::get_offset_of_m_Inertia_7(),
	ScrollRect_t4137855814::get_offset_of_m_DecelerationRate_8(),
	ScrollRect_t4137855814::get_offset_of_m_ScrollSensitivity_9(),
	ScrollRect_t4137855814::get_offset_of_m_Viewport_10(),
	ScrollRect_t4137855814::get_offset_of_m_HorizontalScrollbar_11(),
	ScrollRect_t4137855814::get_offset_of_m_VerticalScrollbar_12(),
	ScrollRect_t4137855814::get_offset_of_m_HorizontalScrollbarVisibility_13(),
	ScrollRect_t4137855814::get_offset_of_m_VerticalScrollbarVisibility_14(),
	ScrollRect_t4137855814::get_offset_of_m_HorizontalScrollbarSpacing_15(),
	ScrollRect_t4137855814::get_offset_of_m_VerticalScrollbarSpacing_16(),
	ScrollRect_t4137855814::get_offset_of_m_OnValueChanged_17(),
	ScrollRect_t4137855814::get_offset_of_m_PointerStartLocalCursor_18(),
	ScrollRect_t4137855814::get_offset_of_m_ContentStartPosition_19(),
	ScrollRect_t4137855814::get_offset_of_m_ViewRect_20(),
	ScrollRect_t4137855814::get_offset_of_m_ContentBounds_21(),
	ScrollRect_t4137855814::get_offset_of_m_ViewBounds_22(),
	ScrollRect_t4137855814::get_offset_of_m_Velocity_23(),
	ScrollRect_t4137855814::get_offset_of_m_Dragging_24(),
	ScrollRect_t4137855814::get_offset_of_m_PrevPosition_25(),
	ScrollRect_t4137855814::get_offset_of_m_PrevContentBounds_26(),
	ScrollRect_t4137855814::get_offset_of_m_PrevViewBounds_27(),
	ScrollRect_t4137855814::get_offset_of_m_HasRebuiltLayout_28(),
	ScrollRect_t4137855814::get_offset_of_m_HSliderExpand_29(),
	ScrollRect_t4137855814::get_offset_of_m_VSliderExpand_30(),
	ScrollRect_t4137855814::get_offset_of_m_HSliderHeight_31(),
	ScrollRect_t4137855814::get_offset_of_m_VSliderWidth_32(),
	ScrollRect_t4137855814::get_offset_of_m_Rect_33(),
	ScrollRect_t4137855814::get_offset_of_m_HorizontalScrollbarRect_34(),
	ScrollRect_t4137855814::get_offset_of_m_VerticalScrollbarRect_35(),
	ScrollRect_t4137855814::get_offset_of_m_Tracker_36(),
	ScrollRect_t4137855814::get_offset_of_m_Corners_37(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1751 = { sizeof (MovementType_t4072922106)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1751[4] = 
{
	MovementType_t4072922106::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1752 = { sizeof (ScrollbarVisibility_t705693775)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1752[4] = 
{
	ScrollbarVisibility_t705693775::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1753 = { sizeof (ScrollRectEvent_t343079324), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1754 = { sizeof (Selectable_t3250028441), -1, sizeof(Selectable_t3250028441_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1754[14] = 
{
	Selectable_t3250028441_StaticFields::get_offset_of_s_List_2(),
	Selectable_t3250028441::get_offset_of_m_Navigation_3(),
	Selectable_t3250028441::get_offset_of_m_Transition_4(),
	Selectable_t3250028441::get_offset_of_m_Colors_5(),
	Selectable_t3250028441::get_offset_of_m_SpriteState_6(),
	Selectable_t3250028441::get_offset_of_m_AnimationTriggers_7(),
	Selectable_t3250028441::get_offset_of_m_Interactable_8(),
	Selectable_t3250028441::get_offset_of_m_TargetGraphic_9(),
	Selectable_t3250028441::get_offset_of_m_GroupsAllowInteraction_10(),
	Selectable_t3250028441::get_offset_of_m_CurrentSelectionState_11(),
	Selectable_t3250028441::get_offset_of_U3CisPointerInsideU3Ek__BackingField_12(),
	Selectable_t3250028441::get_offset_of_U3CisPointerDownU3Ek__BackingField_13(),
	Selectable_t3250028441::get_offset_of_U3ChasSelectionU3Ek__BackingField_14(),
	Selectable_t3250028441::get_offset_of_m_CanvasGroupCache_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1755 = { sizeof (Transition_t1769908631)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1755[5] = 
{
	Transition_t1769908631::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1756 = { sizeof (SelectionState_t2656606514)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1756[5] = 
{
	SelectionState_t2656606514::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1757 = { sizeof (SetPropertyUtility_t3359423571), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1758 = { sizeof (Slider_t3903728902), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1758[15] = 
{
	Slider_t3903728902::get_offset_of_m_FillRect_16(),
	Slider_t3903728902::get_offset_of_m_HandleRect_17(),
	Slider_t3903728902::get_offset_of_m_Direction_18(),
	Slider_t3903728902::get_offset_of_m_MinValue_19(),
	Slider_t3903728902::get_offset_of_m_MaxValue_20(),
	Slider_t3903728902::get_offset_of_m_WholeNumbers_21(),
	Slider_t3903728902::get_offset_of_m_Value_22(),
	Slider_t3903728902::get_offset_of_m_OnValueChanged_23(),
	Slider_t3903728902::get_offset_of_m_FillImage_24(),
	Slider_t3903728902::get_offset_of_m_FillTransform_25(),
	Slider_t3903728902::get_offset_of_m_FillContainerRect_26(),
	Slider_t3903728902::get_offset_of_m_HandleTransform_27(),
	Slider_t3903728902::get_offset_of_m_HandleContainerRect_28(),
	Slider_t3903728902::get_offset_of_m_Offset_29(),
	Slider_t3903728902::get_offset_of_m_Tracker_30(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1759 = { sizeof (Direction_t337909235)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1759[5] = 
{
	Direction_t337909235::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1760 = { sizeof (SliderEvent_t3180273144), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1761 = { sizeof (Axis_t809944411)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1761[3] = 
{
	Axis_t809944411::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1762 = { sizeof (SpriteState_t1362986479)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1762[3] = 
{
	SpriteState_t1362986479::get_offset_of_m_HighlightedSprite_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	SpriteState_t1362986479::get_offset_of_m_PressedSprite_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	SpriteState_t1362986479::get_offset_of_m_DisabledSprite_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1763 = { sizeof (StencilMaterial_t3850132571), -1, sizeof(StencilMaterial_t3850132571_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1763[1] = 
{
	StencilMaterial_t3850132571_StaticFields::get_offset_of_m_List_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1764 = { sizeof (MatEntry_t2957107092), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1764[10] = 
{
	MatEntry_t2957107092::get_offset_of_baseMat_0(),
	MatEntry_t2957107092::get_offset_of_customMat_1(),
	MatEntry_t2957107092::get_offset_of_count_2(),
	MatEntry_t2957107092::get_offset_of_stencilId_3(),
	MatEntry_t2957107092::get_offset_of_operation_4(),
	MatEntry_t2957107092::get_offset_of_compareFunction_5(),
	MatEntry_t2957107092::get_offset_of_readMask_6(),
	MatEntry_t2957107092::get_offset_of_writeMask_7(),
	MatEntry_t2957107092::get_offset_of_useAlphaClip_8(),
	MatEntry_t2957107092::get_offset_of_colorMask_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1765 = { sizeof (Text_t1901882714), -1, sizeof(Text_t1901882714_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1765[7] = 
{
	Text_t1901882714::get_offset_of_m_FontData_28(),
	Text_t1901882714::get_offset_of_m_Text_29(),
	Text_t1901882714::get_offset_of_m_TextCache_30(),
	Text_t1901882714::get_offset_of_m_TextCacheForLayout_31(),
	Text_t1901882714_StaticFields::get_offset_of_s_DefaultText_32(),
	Text_t1901882714::get_offset_of_m_DisableFontTextureRebuiltCallback_33(),
	Text_t1901882714::get_offset_of_m_TempVerts_34(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1766 = { sizeof (Toggle_t2735377061), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1766[5] = 
{
	Toggle_t2735377061::get_offset_of_toggleTransition_16(),
	Toggle_t2735377061::get_offset_of_graphic_17(),
	Toggle_t2735377061::get_offset_of_m_Group_18(),
	Toggle_t2735377061::get_offset_of_onValueChanged_19(),
	Toggle_t2735377061::get_offset_of_m_IsOn_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1767 = { sizeof (ToggleTransition_t3587297765)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1767[3] = 
{
	ToggleTransition_t3587297765::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1768 = { sizeof (ToggleEvent_t1873685584), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1769 = { sizeof (ToggleGroup_t123837990), -1, sizeof(ToggleGroup_t123837990_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1769[4] = 
{
	ToggleGroup_t123837990::get_offset_of_m_AllowSwitchOff_2(),
	ToggleGroup_t123837990::get_offset_of_m_Toggles_3(),
	ToggleGroup_t123837990_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_4(),
	ToggleGroup_t123837990_StaticFields::get_offset_of_U3CU3Ef__amU24cache1_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1770 = { sizeof (ClipperRegistry_t2428680409), -1, sizeof(ClipperRegistry_t2428680409_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1770[2] = 
{
	ClipperRegistry_t2428680409_StaticFields::get_offset_of_s_Instance_0(),
	ClipperRegistry_t2428680409::get_offset_of_m_Clippers_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1771 = { sizeof (Clipping_t312708592), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1772 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1773 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1774 = { sizeof (RectangularVertexClipper_t626611136), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1774[2] = 
{
	RectangularVertexClipper_t626611136::get_offset_of_m_WorldCorners_0(),
	RectangularVertexClipper_t626611136::get_offset_of_m_CanvasCorners_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1775 = { sizeof (AspectRatioFitter_t3312407083), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1775[4] = 
{
	AspectRatioFitter_t3312407083::get_offset_of_m_AspectMode_2(),
	AspectRatioFitter_t3312407083::get_offset_of_m_AspectRatio_3(),
	AspectRatioFitter_t3312407083::get_offset_of_m_Rect_4(),
	AspectRatioFitter_t3312407083::get_offset_of_m_Tracker_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1776 = { sizeof (AspectMode_t3417192999)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1776[6] = 
{
	AspectMode_t3417192999::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1777 = { sizeof (CanvasScaler_t2767979955), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1777[14] = 
{
	CanvasScaler_t2767979955::get_offset_of_m_UiScaleMode_2(),
	CanvasScaler_t2767979955::get_offset_of_m_ReferencePixelsPerUnit_3(),
	CanvasScaler_t2767979955::get_offset_of_m_ScaleFactor_4(),
	CanvasScaler_t2767979955::get_offset_of_m_ReferenceResolution_5(),
	CanvasScaler_t2767979955::get_offset_of_m_ScreenMatchMode_6(),
	CanvasScaler_t2767979955::get_offset_of_m_MatchWidthOrHeight_7(),
	0,
	CanvasScaler_t2767979955::get_offset_of_m_PhysicalUnit_9(),
	CanvasScaler_t2767979955::get_offset_of_m_FallbackScreenDPI_10(),
	CanvasScaler_t2767979955::get_offset_of_m_DefaultSpriteDPI_11(),
	CanvasScaler_t2767979955::get_offset_of_m_DynamicPixelsPerUnit_12(),
	CanvasScaler_t2767979955::get_offset_of_m_Canvas_13(),
	CanvasScaler_t2767979955::get_offset_of_m_PrevScaleFactor_14(),
	CanvasScaler_t2767979955::get_offset_of_m_PrevReferencePixelsPerUnit_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1778 = { sizeof (ScaleMode_t2604066427)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1778[4] = 
{
	ScaleMode_t2604066427::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1779 = { sizeof (ScreenMatchMode_t3675272090)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1779[4] = 
{
	ScreenMatchMode_t3675272090::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1780 = { sizeof (Unit_t2218508340)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1780[6] = 
{
	Unit_t2218508340::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1781 = { sizeof (ContentSizeFitter_t3850442145), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1781[4] = 
{
	ContentSizeFitter_t3850442145::get_offset_of_m_HorizontalFit_2(),
	ContentSizeFitter_t3850442145::get_offset_of_m_VerticalFit_3(),
	ContentSizeFitter_t3850442145::get_offset_of_m_Rect_4(),
	ContentSizeFitter_t3850442145::get_offset_of_m_Tracker_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1782 = { sizeof (FitMode_t3267881214)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1782[4] = 
{
	FitMode_t3267881214::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1783 = { sizeof (GridLayoutGroup_t3046220461), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1783[6] = 
{
	GridLayoutGroup_t3046220461::get_offset_of_m_StartCorner_10(),
	GridLayoutGroup_t3046220461::get_offset_of_m_StartAxis_11(),
	GridLayoutGroup_t3046220461::get_offset_of_m_CellSize_12(),
	GridLayoutGroup_t3046220461::get_offset_of_m_Spacing_13(),
	GridLayoutGroup_t3046220461::get_offset_of_m_Constraint_14(),
	GridLayoutGroup_t3046220461::get_offset_of_m_ConstraintCount_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1784 = { sizeof (Corner_t1493259673)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1784[5] = 
{
	Corner_t1493259673::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1785 = { sizeof (Axis_t3613393006)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1785[3] = 
{
	Axis_t3613393006::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1786 = { sizeof (Constraint_t814224393)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1786[4] = 
{
	Constraint_t814224393::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1787 = { sizeof (HorizontalLayoutGroup_t2586782146), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1788 = { sizeof (HorizontalOrVerticalLayoutGroup_t729725570), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1788[5] = 
{
	HorizontalOrVerticalLayoutGroup_t729725570::get_offset_of_m_Spacing_10(),
	HorizontalOrVerticalLayoutGroup_t729725570::get_offset_of_m_ChildForceExpandWidth_11(),
	HorizontalOrVerticalLayoutGroup_t729725570::get_offset_of_m_ChildForceExpandHeight_12(),
	HorizontalOrVerticalLayoutGroup_t729725570::get_offset_of_m_ChildControlWidth_13(),
	HorizontalOrVerticalLayoutGroup_t729725570::get_offset_of_m_ChildControlHeight_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1789 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1790 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1791 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1792 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1793 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1794 = { sizeof (LayoutElement_t1785403678), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1794[7] = 
{
	LayoutElement_t1785403678::get_offset_of_m_IgnoreLayout_2(),
	LayoutElement_t1785403678::get_offset_of_m_MinWidth_3(),
	LayoutElement_t1785403678::get_offset_of_m_MinHeight_4(),
	LayoutElement_t1785403678::get_offset_of_m_PreferredWidth_5(),
	LayoutElement_t1785403678::get_offset_of_m_PreferredHeight_6(),
	LayoutElement_t1785403678::get_offset_of_m_FlexibleWidth_7(),
	LayoutElement_t1785403678::get_offset_of_m_FlexibleHeight_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1795 = { sizeof (LayoutGroup_t2436138090), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1795[8] = 
{
	LayoutGroup_t2436138090::get_offset_of_m_Padding_2(),
	LayoutGroup_t2436138090::get_offset_of_m_ChildAlignment_3(),
	LayoutGroup_t2436138090::get_offset_of_m_Rect_4(),
	LayoutGroup_t2436138090::get_offset_of_m_Tracker_5(),
	LayoutGroup_t2436138090::get_offset_of_m_TotalMinSize_6(),
	LayoutGroup_t2436138090::get_offset_of_m_TotalPreferredSize_7(),
	LayoutGroup_t2436138090::get_offset_of_m_TotalFlexibleSize_8(),
	LayoutGroup_t2436138090::get_offset_of_m_RectChildren_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1796 = { sizeof (U3CDelayedSetDirtyU3Ec__Iterator0_t3170500204), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1796[4] = 
{
	U3CDelayedSetDirtyU3Ec__Iterator0_t3170500204::get_offset_of_rectTransform_0(),
	U3CDelayedSetDirtyU3Ec__Iterator0_t3170500204::get_offset_of_U24current_1(),
	U3CDelayedSetDirtyU3Ec__Iterator0_t3170500204::get_offset_of_U24disposing_2(),
	U3CDelayedSetDirtyU3Ec__Iterator0_t3170500204::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1797 = { sizeof (LayoutRebuilder_t541313304), -1, sizeof(LayoutRebuilder_t541313304_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1797[9] = 
{
	LayoutRebuilder_t541313304::get_offset_of_m_ToRebuild_0(),
	LayoutRebuilder_t541313304::get_offset_of_m_CachedHashFromTransform_1(),
	LayoutRebuilder_t541313304_StaticFields::get_offset_of_s_Rebuilders_2(),
	LayoutRebuilder_t541313304_StaticFields::get_offset_of_U3CU3Ef__mgU24cache0_3(),
	LayoutRebuilder_t541313304_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_4(),
	LayoutRebuilder_t541313304_StaticFields::get_offset_of_U3CU3Ef__amU24cache1_5(),
	LayoutRebuilder_t541313304_StaticFields::get_offset_of_U3CU3Ef__amU24cache2_6(),
	LayoutRebuilder_t541313304_StaticFields::get_offset_of_U3CU3Ef__amU24cache3_7(),
	LayoutRebuilder_t541313304_StaticFields::get_offset_of_U3CU3Ef__amU24cache4_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1798 = { sizeof (LayoutUtility_t2745813735), -1, sizeof(LayoutUtility_t2745813735_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1798[8] = 
{
	LayoutUtility_t2745813735_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_0(),
	LayoutUtility_t2745813735_StaticFields::get_offset_of_U3CU3Ef__amU24cache1_1(),
	LayoutUtility_t2745813735_StaticFields::get_offset_of_U3CU3Ef__amU24cache2_2(),
	LayoutUtility_t2745813735_StaticFields::get_offset_of_U3CU3Ef__amU24cache3_3(),
	LayoutUtility_t2745813735_StaticFields::get_offset_of_U3CU3Ef__amU24cache4_4(),
	LayoutUtility_t2745813735_StaticFields::get_offset_of_U3CU3Ef__amU24cache5_5(),
	LayoutUtility_t2745813735_StaticFields::get_offset_of_U3CU3Ef__amU24cache6_6(),
	LayoutUtility_t2745813735_StaticFields::get_offset_of_U3CU3Ef__amU24cache7_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1799 = { sizeof (VerticalLayoutGroup_t923838031), -1, 0, 0 };
#ifdef __clang__
#pragma clang diagnostic pop
#endif
