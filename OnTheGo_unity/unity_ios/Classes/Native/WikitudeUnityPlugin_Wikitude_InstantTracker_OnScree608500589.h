﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_Events_UnityEvent_3_gen1153900776.h"





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Wikitude.InstantTracker/OnScreenConversionComputedEvent
struct  OnScreenConversionComputedEvent_t608500589  : public UnityEvent_3_t1153900776
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
