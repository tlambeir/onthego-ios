﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "AssemblyU2DCSharp_SampleController2879308770.h"
#include "UnityEngine_UnityEngine_Color2555686324.h"
#include "WikitudeUnityPlugin_Wikitude_InstantTrackingState3973410783.h"

// UnityEngine.GameObject
struct GameObject_t1113636619;
// UnityEngine.UI.Text
struct Text_t1901882714;
// Wikitude.InstantTracker
struct InstantTracker_t684352120;
// System.Collections.Generic.List`1<UnityEngine.UI.Button>
struct List_1_t1232139915;
// System.Collections.Generic.List`1<UnityEngine.GameObject>
struct List_1_t2585711361;
// UnityEngine.UI.Image
struct Image_t2670269651;
// MoveController
struct MoveController_t1157877467;
// GridRenderer
struct GridRenderer_t1964421403;
// System.Collections.Generic.HashSet`1<UnityEngine.GameObject>
struct HashSet_1_t3973553389;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// InstantTrackingController
struct  InstantTrackingController_t1374048463  : public SampleController_t2879308770
{
public:
	// UnityEngine.GameObject InstantTrackingController::ButtonDock
	GameObject_t1113636619 * ___ButtonDock_2;
	// UnityEngine.GameObject InstantTrackingController::InitializationControls
	GameObject_t1113636619 * ___InitializationControls_3;
	// UnityEngine.GameObject InstantTrackingController::active
	GameObject_t1113636619 * ___active_4;
	// UnityEngine.UI.Text InstantTrackingController::HeightLabel
	Text_t1901882714 * ___HeightLabel_5;
	// UnityEngine.UI.Text InstantTrackingController::ScaleLabel
	Text_t1901882714 * ___ScaleLabel_6;
	// Wikitude.InstantTracker InstantTrackingController::Tracker
	InstantTracker_t684352120 * ___Tracker_7;
	// System.Collections.Generic.List`1<UnityEngine.UI.Button> InstantTrackingController::Buttons
	List_1_t1232139915 * ___Buttons_8;
	// System.Collections.Generic.List`1<UnityEngine.GameObject> InstantTrackingController::Models
	List_1_t2585711361 * ___Models_9;
	// UnityEngine.UI.Image InstantTrackingController::ActivityIndicator
	Image_t2670269651 * ___ActivityIndicator_10;
	// UnityEngine.Color InstantTrackingController::EnabledColor
	Color_t2555686324  ___EnabledColor_11;
	// UnityEngine.Color InstantTrackingController::DisabledColor
	Color_t2555686324  ___DisabledColor_12;
	// System.Single InstantTrackingController::_currentDeviceHeightAboveGround
	float ____currentDeviceHeightAboveGround_13;
	// MoveController InstantTrackingController::_moveController
	MoveController_t1157877467 * ____moveController_14;
	// GridRenderer InstantTrackingController::_gridRenderer
	GridRenderer_t1964421403 * ____gridRenderer_15;
	// System.Collections.Generic.HashSet`1<UnityEngine.GameObject> InstantTrackingController::_activeModels
	HashSet_1_t3973553389 * ____activeModels_16;
	// Wikitude.InstantTrackingState InstantTrackingController::_currentState
	int32_t ____currentState_17;
	// System.Boolean InstantTrackingController::_isTracking
	bool ____isTracking_18;

public:
	inline static int32_t get_offset_of_ButtonDock_2() { return static_cast<int32_t>(offsetof(InstantTrackingController_t1374048463, ___ButtonDock_2)); }
	inline GameObject_t1113636619 * get_ButtonDock_2() const { return ___ButtonDock_2; }
	inline GameObject_t1113636619 ** get_address_of_ButtonDock_2() { return &___ButtonDock_2; }
	inline void set_ButtonDock_2(GameObject_t1113636619 * value)
	{
		___ButtonDock_2 = value;
		Il2CppCodeGenWriteBarrier(&___ButtonDock_2, value);
	}

	inline static int32_t get_offset_of_InitializationControls_3() { return static_cast<int32_t>(offsetof(InstantTrackingController_t1374048463, ___InitializationControls_3)); }
	inline GameObject_t1113636619 * get_InitializationControls_3() const { return ___InitializationControls_3; }
	inline GameObject_t1113636619 ** get_address_of_InitializationControls_3() { return &___InitializationControls_3; }
	inline void set_InitializationControls_3(GameObject_t1113636619 * value)
	{
		___InitializationControls_3 = value;
		Il2CppCodeGenWriteBarrier(&___InitializationControls_3, value);
	}

	inline static int32_t get_offset_of_active_4() { return static_cast<int32_t>(offsetof(InstantTrackingController_t1374048463, ___active_4)); }
	inline GameObject_t1113636619 * get_active_4() const { return ___active_4; }
	inline GameObject_t1113636619 ** get_address_of_active_4() { return &___active_4; }
	inline void set_active_4(GameObject_t1113636619 * value)
	{
		___active_4 = value;
		Il2CppCodeGenWriteBarrier(&___active_4, value);
	}

	inline static int32_t get_offset_of_HeightLabel_5() { return static_cast<int32_t>(offsetof(InstantTrackingController_t1374048463, ___HeightLabel_5)); }
	inline Text_t1901882714 * get_HeightLabel_5() const { return ___HeightLabel_5; }
	inline Text_t1901882714 ** get_address_of_HeightLabel_5() { return &___HeightLabel_5; }
	inline void set_HeightLabel_5(Text_t1901882714 * value)
	{
		___HeightLabel_5 = value;
		Il2CppCodeGenWriteBarrier(&___HeightLabel_5, value);
	}

	inline static int32_t get_offset_of_ScaleLabel_6() { return static_cast<int32_t>(offsetof(InstantTrackingController_t1374048463, ___ScaleLabel_6)); }
	inline Text_t1901882714 * get_ScaleLabel_6() const { return ___ScaleLabel_6; }
	inline Text_t1901882714 ** get_address_of_ScaleLabel_6() { return &___ScaleLabel_6; }
	inline void set_ScaleLabel_6(Text_t1901882714 * value)
	{
		___ScaleLabel_6 = value;
		Il2CppCodeGenWriteBarrier(&___ScaleLabel_6, value);
	}

	inline static int32_t get_offset_of_Tracker_7() { return static_cast<int32_t>(offsetof(InstantTrackingController_t1374048463, ___Tracker_7)); }
	inline InstantTracker_t684352120 * get_Tracker_7() const { return ___Tracker_7; }
	inline InstantTracker_t684352120 ** get_address_of_Tracker_7() { return &___Tracker_7; }
	inline void set_Tracker_7(InstantTracker_t684352120 * value)
	{
		___Tracker_7 = value;
		Il2CppCodeGenWriteBarrier(&___Tracker_7, value);
	}

	inline static int32_t get_offset_of_Buttons_8() { return static_cast<int32_t>(offsetof(InstantTrackingController_t1374048463, ___Buttons_8)); }
	inline List_1_t1232139915 * get_Buttons_8() const { return ___Buttons_8; }
	inline List_1_t1232139915 ** get_address_of_Buttons_8() { return &___Buttons_8; }
	inline void set_Buttons_8(List_1_t1232139915 * value)
	{
		___Buttons_8 = value;
		Il2CppCodeGenWriteBarrier(&___Buttons_8, value);
	}

	inline static int32_t get_offset_of_Models_9() { return static_cast<int32_t>(offsetof(InstantTrackingController_t1374048463, ___Models_9)); }
	inline List_1_t2585711361 * get_Models_9() const { return ___Models_9; }
	inline List_1_t2585711361 ** get_address_of_Models_9() { return &___Models_9; }
	inline void set_Models_9(List_1_t2585711361 * value)
	{
		___Models_9 = value;
		Il2CppCodeGenWriteBarrier(&___Models_9, value);
	}

	inline static int32_t get_offset_of_ActivityIndicator_10() { return static_cast<int32_t>(offsetof(InstantTrackingController_t1374048463, ___ActivityIndicator_10)); }
	inline Image_t2670269651 * get_ActivityIndicator_10() const { return ___ActivityIndicator_10; }
	inline Image_t2670269651 ** get_address_of_ActivityIndicator_10() { return &___ActivityIndicator_10; }
	inline void set_ActivityIndicator_10(Image_t2670269651 * value)
	{
		___ActivityIndicator_10 = value;
		Il2CppCodeGenWriteBarrier(&___ActivityIndicator_10, value);
	}

	inline static int32_t get_offset_of_EnabledColor_11() { return static_cast<int32_t>(offsetof(InstantTrackingController_t1374048463, ___EnabledColor_11)); }
	inline Color_t2555686324  get_EnabledColor_11() const { return ___EnabledColor_11; }
	inline Color_t2555686324 * get_address_of_EnabledColor_11() { return &___EnabledColor_11; }
	inline void set_EnabledColor_11(Color_t2555686324  value)
	{
		___EnabledColor_11 = value;
	}

	inline static int32_t get_offset_of_DisabledColor_12() { return static_cast<int32_t>(offsetof(InstantTrackingController_t1374048463, ___DisabledColor_12)); }
	inline Color_t2555686324  get_DisabledColor_12() const { return ___DisabledColor_12; }
	inline Color_t2555686324 * get_address_of_DisabledColor_12() { return &___DisabledColor_12; }
	inline void set_DisabledColor_12(Color_t2555686324  value)
	{
		___DisabledColor_12 = value;
	}

	inline static int32_t get_offset_of__currentDeviceHeightAboveGround_13() { return static_cast<int32_t>(offsetof(InstantTrackingController_t1374048463, ____currentDeviceHeightAboveGround_13)); }
	inline float get__currentDeviceHeightAboveGround_13() const { return ____currentDeviceHeightAboveGround_13; }
	inline float* get_address_of__currentDeviceHeightAboveGround_13() { return &____currentDeviceHeightAboveGround_13; }
	inline void set__currentDeviceHeightAboveGround_13(float value)
	{
		____currentDeviceHeightAboveGround_13 = value;
	}

	inline static int32_t get_offset_of__moveController_14() { return static_cast<int32_t>(offsetof(InstantTrackingController_t1374048463, ____moveController_14)); }
	inline MoveController_t1157877467 * get__moveController_14() const { return ____moveController_14; }
	inline MoveController_t1157877467 ** get_address_of__moveController_14() { return &____moveController_14; }
	inline void set__moveController_14(MoveController_t1157877467 * value)
	{
		____moveController_14 = value;
		Il2CppCodeGenWriteBarrier(&____moveController_14, value);
	}

	inline static int32_t get_offset_of__gridRenderer_15() { return static_cast<int32_t>(offsetof(InstantTrackingController_t1374048463, ____gridRenderer_15)); }
	inline GridRenderer_t1964421403 * get__gridRenderer_15() const { return ____gridRenderer_15; }
	inline GridRenderer_t1964421403 ** get_address_of__gridRenderer_15() { return &____gridRenderer_15; }
	inline void set__gridRenderer_15(GridRenderer_t1964421403 * value)
	{
		____gridRenderer_15 = value;
		Il2CppCodeGenWriteBarrier(&____gridRenderer_15, value);
	}

	inline static int32_t get_offset_of__activeModels_16() { return static_cast<int32_t>(offsetof(InstantTrackingController_t1374048463, ____activeModels_16)); }
	inline HashSet_1_t3973553389 * get__activeModels_16() const { return ____activeModels_16; }
	inline HashSet_1_t3973553389 ** get_address_of__activeModels_16() { return &____activeModels_16; }
	inline void set__activeModels_16(HashSet_1_t3973553389 * value)
	{
		____activeModels_16 = value;
		Il2CppCodeGenWriteBarrier(&____activeModels_16, value);
	}

	inline static int32_t get_offset_of__currentState_17() { return static_cast<int32_t>(offsetof(InstantTrackingController_t1374048463, ____currentState_17)); }
	inline int32_t get__currentState_17() const { return ____currentState_17; }
	inline int32_t* get_address_of__currentState_17() { return &____currentState_17; }
	inline void set__currentState_17(int32_t value)
	{
		____currentState_17 = value;
	}

	inline static int32_t get_offset_of__isTracking_18() { return static_cast<int32_t>(offsetof(InstantTrackingController_t1374048463, ____isTracking_18)); }
	inline bool get__isTracking_18() const { return ____isTracking_18; }
	inline bool* get_address_of__isTracking_18() { return &____isTracking_18; }
	inline void set__isTracking_18(bool value)
	{
		____isTracking_18 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
