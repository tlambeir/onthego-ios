﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "TextMeshProU2D1_0_55_56_0b12_TMPro_TMP_Asset2469957285.h"

// System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>
struct Dictionary_2_t1839659084;
// TMPro.TMP_SpriteAsset
struct TMP_SpriteAsset_t484820633;
// UnityEngine.Texture
struct Texture_t3661962703;
// System.Collections.Generic.List`1<TMPro.TMP_Sprite>
struct List_1_t2026141888;
// System.Collections.Generic.List`1<TMPro.TMP_SpriteAsset>
struct List_1_t1956895375;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.TMP_SpriteAsset
struct  TMP_SpriteAsset_t484820633  : public TMP_Asset_t2469957285
{
public:
	// System.Collections.Generic.Dictionary`2<System.Int32,System.Int32> TMPro.TMP_SpriteAsset::m_UnicodeLookup
	Dictionary_2_t1839659084 * ___m_UnicodeLookup_5;
	// System.Collections.Generic.Dictionary`2<System.Int32,System.Int32> TMPro.TMP_SpriteAsset::m_NameLookup
	Dictionary_2_t1839659084 * ___m_NameLookup_6;
	// UnityEngine.Texture TMPro.TMP_SpriteAsset::spriteSheet
	Texture_t3661962703 * ___spriteSheet_8;
	// System.Collections.Generic.List`1<TMPro.TMP_Sprite> TMPro.TMP_SpriteAsset::spriteInfoList
	List_1_t2026141888 * ___spriteInfoList_9;
	// System.Collections.Generic.Dictionary`2<System.Int32,System.Int32> TMPro.TMP_SpriteAsset::m_SpriteUnicodeLookup
	Dictionary_2_t1839659084 * ___m_SpriteUnicodeLookup_10;
	// System.Collections.Generic.List`1<TMPro.TMP_SpriteAsset> TMPro.TMP_SpriteAsset::fallbackSpriteAssets
	List_1_t1956895375 * ___fallbackSpriteAssets_11;

public:
	inline static int32_t get_offset_of_m_UnicodeLookup_5() { return static_cast<int32_t>(offsetof(TMP_SpriteAsset_t484820633, ___m_UnicodeLookup_5)); }
	inline Dictionary_2_t1839659084 * get_m_UnicodeLookup_5() const { return ___m_UnicodeLookup_5; }
	inline Dictionary_2_t1839659084 ** get_address_of_m_UnicodeLookup_5() { return &___m_UnicodeLookup_5; }
	inline void set_m_UnicodeLookup_5(Dictionary_2_t1839659084 * value)
	{
		___m_UnicodeLookup_5 = value;
		Il2CppCodeGenWriteBarrier(&___m_UnicodeLookup_5, value);
	}

	inline static int32_t get_offset_of_m_NameLookup_6() { return static_cast<int32_t>(offsetof(TMP_SpriteAsset_t484820633, ___m_NameLookup_6)); }
	inline Dictionary_2_t1839659084 * get_m_NameLookup_6() const { return ___m_NameLookup_6; }
	inline Dictionary_2_t1839659084 ** get_address_of_m_NameLookup_6() { return &___m_NameLookup_6; }
	inline void set_m_NameLookup_6(Dictionary_2_t1839659084 * value)
	{
		___m_NameLookup_6 = value;
		Il2CppCodeGenWriteBarrier(&___m_NameLookup_6, value);
	}

	inline static int32_t get_offset_of_spriteSheet_8() { return static_cast<int32_t>(offsetof(TMP_SpriteAsset_t484820633, ___spriteSheet_8)); }
	inline Texture_t3661962703 * get_spriteSheet_8() const { return ___spriteSheet_8; }
	inline Texture_t3661962703 ** get_address_of_spriteSheet_8() { return &___spriteSheet_8; }
	inline void set_spriteSheet_8(Texture_t3661962703 * value)
	{
		___spriteSheet_8 = value;
		Il2CppCodeGenWriteBarrier(&___spriteSheet_8, value);
	}

	inline static int32_t get_offset_of_spriteInfoList_9() { return static_cast<int32_t>(offsetof(TMP_SpriteAsset_t484820633, ___spriteInfoList_9)); }
	inline List_1_t2026141888 * get_spriteInfoList_9() const { return ___spriteInfoList_9; }
	inline List_1_t2026141888 ** get_address_of_spriteInfoList_9() { return &___spriteInfoList_9; }
	inline void set_spriteInfoList_9(List_1_t2026141888 * value)
	{
		___spriteInfoList_9 = value;
		Il2CppCodeGenWriteBarrier(&___spriteInfoList_9, value);
	}

	inline static int32_t get_offset_of_m_SpriteUnicodeLookup_10() { return static_cast<int32_t>(offsetof(TMP_SpriteAsset_t484820633, ___m_SpriteUnicodeLookup_10)); }
	inline Dictionary_2_t1839659084 * get_m_SpriteUnicodeLookup_10() const { return ___m_SpriteUnicodeLookup_10; }
	inline Dictionary_2_t1839659084 ** get_address_of_m_SpriteUnicodeLookup_10() { return &___m_SpriteUnicodeLookup_10; }
	inline void set_m_SpriteUnicodeLookup_10(Dictionary_2_t1839659084 * value)
	{
		___m_SpriteUnicodeLookup_10 = value;
		Il2CppCodeGenWriteBarrier(&___m_SpriteUnicodeLookup_10, value);
	}

	inline static int32_t get_offset_of_fallbackSpriteAssets_11() { return static_cast<int32_t>(offsetof(TMP_SpriteAsset_t484820633, ___fallbackSpriteAssets_11)); }
	inline List_1_t1956895375 * get_fallbackSpriteAssets_11() const { return ___fallbackSpriteAssets_11; }
	inline List_1_t1956895375 ** get_address_of_fallbackSpriteAssets_11() { return &___fallbackSpriteAssets_11; }
	inline void set_fallbackSpriteAssets_11(List_1_t1956895375 * value)
	{
		___fallbackSpriteAssets_11 = value;
		Il2CppCodeGenWriteBarrier(&___fallbackSpriteAssets_11, value);
	}
};

struct TMP_SpriteAsset_t484820633_StaticFields
{
public:
	// TMPro.TMP_SpriteAsset TMPro.TMP_SpriteAsset::m_defaultSpriteAsset
	TMP_SpriteAsset_t484820633 * ___m_defaultSpriteAsset_7;

public:
	inline static int32_t get_offset_of_m_defaultSpriteAsset_7() { return static_cast<int32_t>(offsetof(TMP_SpriteAsset_t484820633_StaticFields, ___m_defaultSpriteAsset_7)); }
	inline TMP_SpriteAsset_t484820633 * get_m_defaultSpriteAsset_7() const { return ___m_defaultSpriteAsset_7; }
	inline TMP_SpriteAsset_t484820633 ** get_address_of_m_defaultSpriteAsset_7() { return &___m_defaultSpriteAsset_7; }
	inline void set_m_defaultSpriteAsset_7(TMP_SpriteAsset_t484820633 * value)
	{
		___m_defaultSpriteAsset_7 = value;
		Il2CppCodeGenWriteBarrier(&___m_defaultSpriteAsset_7, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
