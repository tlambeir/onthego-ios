//
//  StepStructure.h
//  OnTheGo
//
//  Created by Alessio Crestani on 23/11/2017.
//Copyright © 2017 Nokia. All rights reserved.
//

#import <Realm/Realm.h>

@class StructureObject;

@interface StepStructure : RLMObject
@property NSString *uuid;
@property NSString *stepString;
@property StructureObject *structureObject;
@end

RLM_ARRAY_TYPE(StepStructure)
