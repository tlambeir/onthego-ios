//
//  StepStructure.m
//  OnTheGo
//
//  Created by Alessio Crestani on 23/11/2017.
//Copyright © 2017 Nokia. All rights reserved.
//

#import "StepStructure.h"

@implementation StepStructure

+ (NSString *)primaryKey
{
    return @"uuid";
}

+ (NSDictionary *)defaultPropertyValues
{
    return @{@"uuid": [[NSUUID UUID] UUIDString]};
}

@end
