      //
//  DownloadedObject.m
//  OnTheGo
//
//  Created by Alessio Crestani on 17/11/2017.
//Copyright © 2017 Nokia. All rights reserved.
//

#import "DownloadedObject.h"
#import "GlobalImport.h"

@implementation DownloadedObject

+ (NSString *)primaryKey
{
    return @"uuid";
}

+ (NSDictionary *)defaultPropertyValues
{
    return @{@"uuid": [[NSUUID UUID] UUIDString], @"isHistory": @YES, @"isDownload": @YES};
}

- (UIImage *)imageDescription
{
    return [FileManager getImageForObject:self];
}

- (BOOL)isOnDevice
{
    return (((TypeOfile)[[self type] intValue] == WEB_OBJ) && [[self objURL] containsString:@"wbt"]) || (TypeOfile)[[self type] intValue] == WBT_OBJ || (TypeOfile)[[self type] intValue] == URL_OBJ || (TypeOfile)[[self type] intValue] == AGGREGATED_OBJ ||  [FileManager searchFileOnStorageOfObj:self] != nil;
}

- (NSString *)stringFileType
{
    return [Utilities getStringFileTypeForType:[self.type intValue]];
}

- (NSURL *)getObjFilePath
{
    NSString *fileDirectory = [[[NSFileManager defaultManager] URLForDirectory:NSDocumentDirectory inDomain:NSUserDomainMask appropriateForURL:nil create:NO error:nil] absoluteString];
    
    switch ([[self type] intValue]) {
        case PDF_OBJ:
            fileDirectory = [FileManager pathForPDFFolder];
            break;
        case WEB_OBJ:
            fileDirectory = [FileManager pathForWebFolder];
            break;
        case AR_3D_OBJ:
            fileDirectory = [FileManager pathForARFolder];
            break;
        case VIDEO_OBJ:
            fileDirectory = [FileManager pathForVideoFolder];
            break;
        case VIDEO_360_OBJ:
            fileDirectory = [FileManager pathForVideo360Folder];
            break;
            
    }
    
    NSDirectoryEnumerator *enumerator = [[NSFileManager defaultManager] enumeratorAtPath:fileDirectory];
    for (NSString *singleFilePath in enumerator) {
        if([[[[NSURL fileURLWithPath:singleFilePath] lastPathComponent] stringByDeletingPathExtension] isEqualToString:[self uuid]]){
            return [NSURL fileURLWithPath:[fileDirectory stringByAppendingPathComponent:singleFilePath]];
            break;
        }
    }
    
    return nil;
}

- (NSURL *)getDownloadFilePath
{
    NSString *fileDirectory = [[[NSFileManager defaultManager] URLForDirectory:NSDocumentDirectory inDomain:NSUserDomainMask appropriateForURL:nil create:NO error:nil] absoluteString];
    
    NSString *resultString;
    
    switch ([[self type] intValue]) {
        case PDF_OBJ:
            fileDirectory = [FileManager pathForPDFFolder];
            break;
        case WEB_OBJ:
            fileDirectory = [FileManager pathForWebFolder];
            break;
            case AR_3D_OBJ:
            fileDirectory = [FileManager pathForARFolder];
            break;
        case VIDEO_OBJ:
            fileDirectory = [FileManager pathForVideoFolder];
            break;
        case VIDEO_360_OBJ:
            fileDirectory = [FileManager pathForVideo360Folder];
            break;
    }
    
    resultString = [fileDirectory stringByAppendingPathComponent:[[self uuid] stringByAppendingPathExtension:[[self objURL] pathExtension]]];
    NSURL *result = [NSURL fileURLWithPath:resultString];
    return result;
}

- (NSURL *)getObjThumb
{
    NSString *fileDirectory = [FileManager pathForObjImgFolder];
   
    NSString *fileLocation = [[fileDirectory stringByAppendingString:[@"/" stringByAppendingString:[self uuid]]] stringByAppendingString:@".png"];
    
    return [NSURL fileURLWithPath:fileLocation];
}

- (NSString *)getObjJSONStringForAR
{
    NSString *result = @"";
    NSError * err;
    NSData *data =[self.JSONObj dataUsingEncoding:NSUTF8StringEncoding];
    NSMutableDictionary * jsonObj;
    if(data!=nil){
        jsonObj = [(NSDictionary *)[NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&err] mutableCopy];
        if([self getUnityFile] != nil)
            [jsonObj setValue:[[[self getObjFilePath]  URLByAppendingPathComponent:[self getUnityFile]] absoluteString] forKey:OBJ_PATH_JSON];
        if([self getMapFile] != nil)
            [jsonObj setValue:[[[self getObjFilePath] URLByAppendingPathComponent:[self getMapFile]] absoluteString] forKey:OBJ_MAP_PATH_JSON];
    }
    
    NSError * errString;
    NSData * jsonData = [NSJSONSerialization  dataWithJSONObject:jsonObj options:0 error:&errString];
    result = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];

    
    return result;
    
}

- (NSString *)getMapFile
{
    NSString *path = [[self getObjFilePath] path];
    NSDirectoryEnumerator *enumerator = [[NSFileManager defaultManager] enumeratorAtPath:path];
    for (NSString *filePath in enumerator) {
        if([[filePath pathExtension] isEqualToString:UNITY_WTC_EXTENSION]){
            return [filePath lastPathComponent];
            break;
        }
    }
    
    return @"";
}

- (NSString *)getUnityFile
{
    NSString *path = [[self getObjFilePath] path];
    NSDirectoryEnumerator *enumerator = [[NSFileManager defaultManager] enumeratorAtPath:path];
    for (NSString *filePath in enumerator) {
        if(![[filePath pathExtension] isEqualToString:UNITY_WTC_EXTENSION] && ![[filePath lastPathComponent] containsString:@".DS_Store"]){
            return [filePath lastPathComponent];
            break;
        }
    }
    
    return @"";
}

- (void)changeLastOpenedDate
{
    [[[RealmManager sharedInstance] realm] beginWriteTransaction];
    [self setLastOpenedDate:[NSDate new]];
    [[[RealmManager sharedInstance] realm] commitWriteTransaction];

}

- (BOOL)downloadableFile
{
     return !(((TypeOfile)[[self type] intValue] == AGGREGATED_OBJ || (TypeOfile)[[self type] intValue] == WBT_OBJ || (TypeOfile)[[self type] intValue] == URL_OBJ));
}

@end
