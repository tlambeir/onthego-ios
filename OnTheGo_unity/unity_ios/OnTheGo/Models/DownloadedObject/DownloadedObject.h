//
//  DownloadedObject.h
//  OnTheGo
//
//  Created by Alessio Crestani on 17/11/2017.
//Copyright © 2017 Nokia. All rights reserved.
//

/**
 * Local DB Model to save downloaded objects from JSON
 **/

#import <Realm/Realm.h>
#import "StructureObject.h"

@interface DownloadedObject : RLMObject

@property NSString *uuid;
@property NSString *uuidParent;
@property NSString *title;
@property NSString *objDescription;
@property NSString *fileSize;
@property NSString *thumbURL;
@property NSString *objURL;
@property NSString *appType;
@property NSString *model;
@property NSString *storageType;
@property NSString *JSONObj;
@property NSDate *lastOpenedDate;
@property NSDate *creationDate;
@property NSNumber<RLMInt> *identifier;
@property NSNumber<RLMBool> *isHistory;
@property NSNumber<RLMBool> *isDownload;
@property NSNumber<RLMInt> *type;
@property RLMArray<StructureObject *><StructureObject> *structures;

- (UIImage *)imageDescription;
- (BOOL)isOnDevice;
- (NSString *)stringFileType;
- (NSURL *)getObjFilePath;
- (NSURL *)getObjThumb;
- (NSURL *)getDownloadFilePath;
- (NSString *)objURL;
- (NSString *)getObjJSONStringForAR;
- (void)changeLastOpenedDate;
- (BOOL)downloadableFile;
@end

// This protocol enables typed collections. i.e.:
// RLMArray<DownloadedObject *><DownloadedObject>
RLM_ARRAY_TYPE(DownloadedObject)
