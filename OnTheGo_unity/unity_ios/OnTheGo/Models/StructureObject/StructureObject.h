//
//  StructureObject.h
//  OnTheGo
//
//  Created by Alessio Crestani on 23/11/2017.
//Copyright © 2017 Nokia. All rights reserved.
//

/**
 * Local DB Model to save structures inside Download Object. Used for 3D Model
 **/

#import <Realm/Realm.h>
#import "StepStructure.h"

@class DownloadedObject;

RLM_ARRAY_TYPE(StructureObject)

@interface StructureObject : RLMObject

@property NSString *uuid;
@property NSString *mesh;
@property NSString *name;
@property NSString *structureDescription;
@property NSNumber<RLMBool> *isAnimation;
@property NSNumber<RLMBool> *showLabel;
@property NSString *animationTrigger;
@property RLMArray<StepStructure *><StepStructure> *stepStructures;
@property RLMArray<StructureObject *><StructureObject> *children;

@end

