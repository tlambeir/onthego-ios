//
//  DownloadObjPopup.m
//  OnTheGo
//
//  Created by Alessio Crestani on 17/11/17.
//  Copyright © 2017 Si14. All rights reserved.
//

#import "DownloadObjPopup.h"
#import "DownloadedObject.h"
#import "GlobalImport.h"
#import <QuartzCore/QuartzCore.h>

@implementation DownloadObjPopup

- (void)setupViewWithDictionary:(NSDictionary *)downloadedObjectDictionary
{
    _currentDownloadedObjectDictionary = downloadedObjectDictionary;
    
    [_objName setText:[downloadedObjectDictionary objectForKey:OBJ_TITLE_JSON]];
    [_objDescription setText:[downloadedObjectDictionary objectForKey:OBJ_DESCRIPTION_JSON]];
    [_objName setFont:[UIFont fontWithName:@"Nokia Pure Headline Bold" size:_objName.font.pointSize]];
    [_objDescription setFont:[UIFont fontWithName:@"Nokia Pure Headline" size:_objDescription.font.pointSize]];
    [_objTypeAndSize setFont:[UIFont fontWithName:@"Nokia Pure Headline" size:_objTypeAndSize.font.pointSize]];


    _objDescription.numberOfLines = 0;
    NSString *typeAndSize;
    if([[downloadedObjectDictionary allKeys] containsObject:OBJ_FILE_SIZE_JSON])
        typeAndSize = [NSString stringWithFormat:@"%@ | %@", [downloadedObjectDictionary objectForKey:OBJ_APP_TYPE_JSON], [downloadedObjectDictionary objectForKey:OBJ_FILE_SIZE_JSON]];
    else
        typeAndSize = [downloadedObjectDictionary objectForKey:OBJ_APP_TYPE_JSON];
    
    [_objTypeAndSize setText:typeAndSize];

    NSData *imageData = [NSData dataWithContentsOfURL:[NSURL URLWithString:(NSString *)[downloadedObjectDictionary objectForKey:OBJ_THUMB_URL_JSON]]];
    UIImage *image = [UIImage imageWithData:imageData scale:2.0];
    [_objImage setImage: image];
    
    DownloadedObject *obj = [[RealmManager sharedInstance] objectAlreadySavedWithDictionary:_currentDownloadedObjectDictionary];
    if(obj != nil){
        if([FileManager fileDownloadedForObj:obj])
            [_downloadObjButton setTitle:NSLocalizedString(@"Open", nil) forState:UIControlStateNormal];
        else
            [_downloadObjButton setTitle:NSLocalizedString(@"Download", nil) forState:UIControlStateNormal];

    } else {
        [_downloadObjButton setTitle:NSLocalizedString(@"Download", nil) forState:UIControlStateNormal];
    }
  //  _downloadObjButton.layer.cornerRadius = 10; // this value vary as per your desire
  //  _downloadObjButton.clipsToBounds = YES;

    
    [_cancelObjButton setTitle:NSLocalizedString(@"Cancel", nil) forState:UIControlStateNormal];
    
    [_downloadObjButton addTarget:self action:@selector(downloadContent:) forControlEvents:UIControlEventTouchUpInside];
    [_cancelObjButton addTarget:self action:@selector(stopSavingProcedure:) forControlEvents:UIControlEventTouchUpInside];
    
    
}

- (void)downloadContent:(id)sender
{
    if(_delegate != nil)
        [_delegate startSavingProcedureForObject:_currentDownloadedObjectDictionary];
}

- (void)stopSavingProcedure:(id)sender
{
    if(_delegate != nil)
        [_delegate stopSavingProcedure];
}


@end
