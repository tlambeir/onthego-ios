//
//  DownloadObjPopup.h
//  OnTheGo
//
//  Created by Alessio Crestani on 17/11/17.
//  Copyright © 2017 Si14. All rights reserved.
//


#import "DownloadedObject.h"

@protocol DownloadObjPopupDelegate <NSObject>

- (void)startSavingProcedureForObject:(NSDictionary *)downloadedObjectJSON;
- (void)stopSavingProcedure;

@end

@interface DownloadObjPopup : UIView

@property (nonatomic, strong) id<DownloadObjPopupDelegate> delegate;
@property (nonatomic, strong) IBOutlet UILabel *objName;
@property (nonatomic, strong) IBOutlet UILabel *objDescription;
@property (nonatomic, strong) IBOutlet UILabel *objTypeAndSize;
@property (nonatomic, strong) IBOutlet UIImageView *objImage;
@property (nonatomic, strong) IBOutlet UIButton *downloadObjButton;
@property (nonatomic, strong) IBOutlet UIButton *cancelObjButton;
@property (nonatomic, strong) NSDictionary *passedObj;


@property (nonatomic, strong) NSDictionary * currentDownloadedObjectDictionary;

- (void)setupViewWithDictionary:(NSDictionary *)downloadedObjectDictionary;
- (void)downloadContent:(id)sender;
- (void)stopSavingProcedure:(id)sender;

@end
