//
//  DownloadedObjectCell.h
//  OnTheGo
//
//  Created by Alessio Crestani on 17/11/17.
//  Copyright © 2017 Si14. All rights reserved.
//


#import "DownloadedObject.h"

@interface DownloadedObjectCell : UITableViewCell

@property (nonatomic, strong) IBOutlet UILabel *objName;
@property (nonatomic, strong) IBOutlet UITextView *objDescription;
@property (nonatomic, strong) IBOutlet UILabel *objTypeAndSize;
@property (nonatomic, strong) IBOutlet UIImageView *objImage;

@property (nonatomic,strong) UILongPressGestureRecognizer *lpgr;
@property (nonatomic,strong) UISwipeGestureRecognizer *slgr;
@property (nonatomic,strong) UISwipeGestureRecognizer *srgr;
@property (nonatomic, strong) DownloadedObject * currentDownloadedObject;

- (void)setupViewWithDownloadedObject:(DownloadedObject *)downloadedObject;

@end
