//
//  DownloadedObjectCell.m
//  OnTheGo
//
//  Created by Alessio Crestani on 17/11/17.
//  Copyright © 2017 Si14. All rights reserved.
//

#import "DownloadedObjectCell.h"
#import "DownloadedObject.h"

@implementation DownloadedObjectCell

- (void)setupViewWithDownloadedObject:(DownloadedObject *)downloadedObject
{
    [_objName setText:[downloadedObject title]];
    [_objDescription setText:[downloadedObject objDescription]];
    [_objDescription setUserInteractionEnabled:NO];
    NSString *typeAndSize;
    if([downloadedObject fileSize] != nil)
        typeAndSize = [NSString stringWithFormat:@"%@ | %@", [downloadedObject appType], [downloadedObject fileSize]];
    else
        typeAndSize = [downloadedObject appType];
    
    [_objTypeAndSize setText:typeAndSize];
    [_objImage setImage:[downloadedObject imageDescription]];
    [_objTypeAndSize setText:typeAndSize];
    
    [_objName setFont:[UIFont fontWithName:@"Nokia Pure Headline Bold" size:_objName.font.pointSize]];
    [_objDescription setFont:[UIFont fontWithName:@"Nokia Pure Headline" size:_objDescription.font.pointSize]];
    [_objTypeAndSize setFont:[UIFont fontWithName:@"Nokia Pure Headline Bold" size:_objTypeAndSize.font.pointSize]]; 

}

@end
