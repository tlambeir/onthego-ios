//
//  WebViewController.h
//  OnTheGo
//
//  Created by Alessio Crestani on 16/11/2017.
//  Copyright © 2017 Nokia. All rights reserved.
//

/*******************************************************
 This is the controller that opens HTML 5 pages contents
 *******************************************************/

#import "GlobalImport.h"

@interface HTMLViewController : UIViewController

@property(nonatomic, strong) DownloadedObject *obj;
@property(nonatomic, strong) NSString *URL;

@property (nonatomic, strong) IBOutlet UIWebView *webViewer;

@end
