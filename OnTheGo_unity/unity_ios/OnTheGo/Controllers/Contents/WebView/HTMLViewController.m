//
//  WebViewController.m
//  OnTheGo
//
//  Created by Alessio Crestani on 16/11/2017.
//  Copyright © 2017 Nokia. All rights reserved.
//

#import "HTMLViewController.h"

@interface HTMLViewController ()

@end

@implementation HTMLViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self setupView];
}

- (void)setupView
{
    [self.navigationController.navigationBar setTitleTextAttributes:@{NSFontAttributeName:[UIFont fontWithName:@"Nokia Pure Headline" size:18.0f],
                                                                      NSForegroundColorAttributeName:[UIColor colorWithRed:0.29 green:0.32 blue:0.32 alpha:1.0]
                                                                      }];
    NSURLRequest *request;

    if(_obj != nil){
        [self setTitle:[_obj title]];
        NSURL *url = [NSURL URLWithString:[[[FileManager pathForWebFolder] stringByAppendingPathComponent:[_obj uuid]] stringByAppendingPathComponent:@"index.html"]];
        if([[NSFileManager defaultManager] fileExistsAtPath:[url path]]){
            request = [NSURLRequest requestWithURL:url];
        } else {
            request = [NSURLRequest requestWithURL:[NSURL URLWithString:[_obj objURL]]];
        }
    } else {
        [self setTitle:NSLocalizedString(@"Custom URL", nil)];
        request = [NSURLRequest requestWithURL:[NSURL URLWithString:_URL]];
    }
   
    [_webViewer loadRequest:request];
}

- (BOOL)canRotate
{
    return YES;
}

@end
