//
//  OnTheGoVideoPlayerController.h
//  Unity-iPhone
//
//  Created by Alessio Crestani on 18/01/2018.
//

#import <AVKit/AVKit.h>

@interface OnTheGoVideoPlayerController : AVPlayerViewController

@end
