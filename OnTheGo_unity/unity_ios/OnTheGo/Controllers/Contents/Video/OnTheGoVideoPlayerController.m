//
//  OnTheGoVideoPlayerController.m
//  Unity-iPhone
//
//  Created by Alessio Crestani on 18/01/2018.
//

#import "OnTheGoVideoPlayerController.h"

#define IS_IPAD UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad

@interface OnTheGoVideoPlayerController ()

@property UIButton *doneLandscapeButton;

@end

@implementation OnTheGoVideoPlayerController


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(videoDidRotate) name:UIDeviceOrientationDidChangeNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(closedFullScreen:) name:UIWindowDidBecomeHiddenNotification object:nil];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [self videoDidRotate];
}

- (void)setupDoneButtonForHide:(BOOL)hide
{
    if(_doneLandscapeButton == nil){
        _doneLandscapeButton = [[UIButton alloc] initWithFrame:CGRectMake(IS_IPAD ? 80 : 40, 10, 100, 30)];
        [_doneLandscapeButton setTitle:NSLocalizedString(@"Back", nil) forState:UIControlStateNormal];
        [_doneLandscapeButton.titleLabel setFont:[UIFont fontWithName:@"Nokia Pure Headline" size:_doneLandscapeButton.titleLabel.font.pointSize]];
        [self.view addSubview:_doneLandscapeButton];
        [self.view bringSubviewToFront:_doneLandscapeButton];
        [_doneLandscapeButton addTarget:self action:@selector(backTapped:) forControlEvents:UIControlEventTouchUpInside];

    }
    [[self navigationController] setNavigationBarHidden:!hide animated:YES];
    [_doneLandscapeButton setHidden:hide];
}

- (void)backTapped:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)closedFullScreen:(NSNotification *)myNotification
{
    [[UIDevice currentDevice] setValue:[NSNumber numberWithInteger: UIInterfaceOrientationPortrait] forKey:@"orientation"];
}

- (BOOL)prefersStatusBarHidden
{
    return UIInterfaceOrientationIsLandscape([UIApplication sharedApplication].statusBarOrientation);
}

- (void)videoDidRotate
{
    [[self navigationController] setNavigationBarHidden:UIInterfaceOrientationIsLandscape([UIApplication sharedApplication].statusBarOrientation) animated:YES];
    [self setupDoneButtonForHide:!UIInterfaceOrientationIsLandscape([UIApplication sharedApplication].statusBarOrientation)];
    [self setNeedsStatusBarAppearanceUpdate];
}

- (BOOL)canRotate
{
    return YES;
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [[self navigationController] setNavigationBarHidden:NO animated:YES];

}


@end
