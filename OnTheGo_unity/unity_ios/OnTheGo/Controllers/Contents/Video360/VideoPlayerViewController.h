//
//  VideoPlayerViewController.h
//  OnTheGo
//
//  Created by Alessio Crestani on 24/11/2017.
//  Copyright © 2017 Nokia. All rights reserved.
//


#import <GVRKit/GVRKit.h>
#import <GVRVideoView.h>
#import "GlobalImport.h"

@interface VideoPlayerViewController : UIViewController<GVRWidgetViewDelegate, GVRVideoViewDelegate>

@property(nonatomic, strong) DownloadedObject *obj;

@property (weak, nonatomic) IBOutlet GVRVideoView *gvrVideoView;
@property(nonatomic) IBOutlet UIToolbar *toolbar;
@property(nonatomic) UIBarButtonItem *progressBar;
@property(nonatomic) BOOL isPlaying;

@end
