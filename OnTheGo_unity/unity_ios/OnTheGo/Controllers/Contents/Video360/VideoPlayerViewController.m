//
//  VideoPlayerViewController.m
//  OnTheGo
//
//  Created by Alessio Crestani on 24/11/2017.
//  Copyright © 2017 Nokia. All rights reserved.
//

#import "VideoPlayerViewController.h"

@implementation VideoPlayerViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self setupView];
    [self setupVideo];
    
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)viewWillLayoutSubviews
{
    [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationNone];
}

- (void)setupView
{
  UISlider *progressView = [[UISlider alloc] initWithFrame:CGRectMake(0, 0 ,_toolbar.frame.size.width, _toolbar.frame.size.height)]; progressView.continuous = NO;
    [progressView addTarget:self action:@selector(userEndTouchSlider) forControlEvents:UIControlEventValueChanged];
    [progressView sizeToFit];
    
    _progressBar = [[UIBarButtonItem alloc] initWithCustomView:progressView];
    _toolbar.items = @[_progressBar];
    
    [self setTitle:[_obj title]];
    [self.navigationController.navigationBar setTitleTextAttributes:@{NSFontAttributeName:[UIFont fontWithName:@"Nokia Pure Headline" size:18.0f],
                                                                      NSForegroundColorAttributeName:[UIColor colorWithRed:0.29 green:0.32 blue:0.32 alpha:1.0]
                                                                      }];
}

- (void)userEndTouchSlider
{
    NSTimeInterval videoLength = _gvrVideoView.duration;  // Gets the video duration
    UISlider *slider = (UISlider *)_progressBar.customView;
    [_gvrVideoView seekTo:(videoLength * [slider value])];
}


- (void)setupVideo
{
    NSURL *filePath = [_obj getObjFilePath];
    NSError *err;
    [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryPlayback withOptions:nil error:&err];
    [_gvrVideoView loadFromUrl:filePath ofType:kGVRVideoTypeMono];
    [_gvrVideoView setDelegate:self];
    [_gvrVideoView setEnableFullscreenButton:YES];
    [_gvrVideoView setEnableInfoButton:YES];
    [_gvrVideoView setEnableCardboardButton:YES];
}

//The event handling method
- (void)handleSingleTap:(UITapGestureRecognizer *)recognizer
{
    [self updateVideoPlayback];
}

- (BOOL)shouldAutorotate
{
    return YES;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self updateVideoPlayback];
}

#pragma mark - Actions

- (IBAction)didTapPlayPause:(id)sender
{
    [self updateVideoPlayback];
}

- (void)videoView:(GVRVideoView *)videoView didUpdatePosition:(NSTimeInterval)position
{
    UISlider *progressView = (UISlider *)_progressBar.customView;
    if(!progressView.isTracking){
        double duration = _gvrVideoView.duration;
        progressView.value = (CGFloat)(position / duration);
    }
}

#pragma mark - AVPlayer

- (void)playerItemDidReachEnd:(NSNotification *)notification
{
    AVPlayerItem *player = [notification object];
    [player seekToTime:kCMTimeZero completionHandler:nil];
}

#pragma mark - GVRRendererViewControllerDelegate

- (void)didTapTriggerButton
{
    [self updateVideoPlayback];
}

#pragma mark - Private

- (void)updateVideoPlayback
{
    if(_isPlaying){
        [_gvrVideoView pause];
        _isPlaying = NO;
    } else {
        [_gvrVideoView play];
        _isPlaying = YES;
    }
}

- (BOOL)canRotate
{
    return YES;
}


#pragma mark - GVRWidgetViewDelegate methods

- (void)widgetViewDidTap:(GVRWidgetView *)widgetView
{
    [self updateVideoPlayback];
}

- (void)widgetView:(GVRWidgetView *)widgetView didLoadContent:(id)content
{
    [_gvrVideoView play];
    _isPlaying = YES;
    NSLog(@"Video is ready. Playing...");
}

- (void)widgetView:(GVRWidgetView *)widgetView didFailToLoadContent:(id)content withErrorMessage:(NSString *)errorMessage
{
    NSLog(@"cannot open video");
    [SVProgressHUD showWithStatus:NSLocalizedString(@"Cannot open content", nil)];
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
