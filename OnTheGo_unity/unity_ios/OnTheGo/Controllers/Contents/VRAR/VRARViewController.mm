//
//  VRARViewController.m
//  OnTheGo
//
//  Created by Alessio Crestani on 16/11/2017.
//  Copyright © 2017 Nokia. All rights reserved.
//

#import "VRARViewController.h"
#import "UnityViewControllerBase.h"
#import "OnTheGoDelegate.h"
#import "Const.h"
#import "SVProgressHUD.h"

@interface VRARViewController ()

@property (nonatomic, strong) UIView *unityView;

@end

@implementation VRARViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
}

- (void)setupView
{
    [_testbutton setHidden:YES];
    [self setTitle:_downloadedObj.title];
    [self.navigationController.navigationBar setTitleTextAttributes:@{NSFontAttributeName:[UIFont fontWithName:@"Nokia Pure Headline" size:18.0f],
                                                                      NSForegroundColorAttributeName:[UIColor colorWithRed:0.29 green:0.32 blue:0.32 alpha:1.0]}];
    
    OnTheGoDelegate *appDelegate = (OnTheGoDelegate *)[[UIApplication sharedApplication] delegate];
    [appDelegate startUnity];
    _unityView = UnityGetGLView();
    [_unityView setFrame:_frameHolderView.frame];
    [self.view addSubview:_unityView];
    NSLog([_downloadedObj getObjJSONStringForAR], nil);

    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 2 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
        UnitySendMessage([UNITY_INIT_MESSAGE cStringUsingEncoding:NSUTF8StringEncoding], [UNITY_INIT_SCENE_MESSAGE cStringUsingEncoding:NSUTF8StringEncoding], [[_downloadedObj getObjJSONStringForAR] cStringUsingEncoding:NSUTF8StringEncoding]);
    });
    // Disable iOS 7 back gesture
    if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
        self.navigationController.interactivePopGestureRecognizer.enabled = NO;
        self.navigationController.interactivePopGestureRecognizer.delegate = self;
    }
    
    [[UIDevice currentDevice] setValue:@(UIInterfaceOrientationPortrait) forKey:@"orientation"];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self setupView];

}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [self.view bringSubviewToFront:_testbutton];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [self callUnityObject:"Dummy" Method:"ChangeScene" Parameter:"Init"];
    [_unityView removeFromSuperview];
    _unityView = nil;
    
    [super viewWillDisappear:animated];
    OnTheGoDelegate *appDelegate = (OnTheGoDelegate *)[[UIApplication sharedApplication] delegate];
    [appDelegate stopUnity];
    
    // Enable iOS 7 back gesture
    if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
        self.navigationController.interactivePopGestureRecognizer.enabled = YES;
        self.navigationController.interactivePopGestureRecognizer.delegate = nil;
    }
    
    [SVProgressHUD dismiss];

}

//- (BOOL)canRotate
//{
//    return YES;
//}

- (BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer
{
    return NO;
}


- (IBAction)testButtonClick:(id)sender
{
    NSLog(@"resetting view");
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 2 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
        UnitySendMessage([UNITY_INIT_MESSAGE cStringUsingEncoding:NSUTF8StringEncoding], [UNITY_INIT_SCENE_MESSAGE cStringUsingEncoding:NSUTF8StringEncoding], [[_downloadedObj getObjJSONStringForAR] cStringUsingEncoding:NSUTF8StringEncoding]);
    });
    
}

- (BOOL)shouldAutorotate
{
    return YES;
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}


//- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation
//{
//    [SVProgressHUD showWithStatus:NSLocalizedString(@"Loading", nil)];
//    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 3 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
//        [self callUnityObject:"Dummy" Method:"RestartScene" Parameter:"Istant"];
//    });
//
//    [SVProgressHUD dismissWithDelay:5];
//
//}

- (void)callUnityObject:(const char*)object Method:(const char*)method Parameter:(const char*)parameter {
    UnitySendMessage(object, method, parameter);
}

@end
