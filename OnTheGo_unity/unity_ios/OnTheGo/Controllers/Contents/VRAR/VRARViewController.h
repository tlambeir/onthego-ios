//
//  VRARViewController.h
//  OnTheGo
//
//  Created by Alessio Crestani on 16/11/2017.
//  Copyright © 2017 Nokia. All rights reserved.
//

/***********************************************************
 This is the controller that opens The Unity+Vuforia feature
 ***********************************************************/

#import "VRARViewController.h"
#import "DownloadedObject.h"

@interface VRARViewController : UIViewController <UIGestureRecognizerDelegate>

@property (nonatomic, strong) DownloadedObject *downloadedObj;
@property (weak, nonatomic) IBOutlet UIButton *testbutton;

@property (weak, nonatomic) IBOutlet UIView *frameHolderView;

@end
