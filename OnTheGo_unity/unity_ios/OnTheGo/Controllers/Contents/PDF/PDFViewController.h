//
//  PDFViewController.h
//  OnTheGo
//
//  Created by Alessio Crestani on 16/11/2017.
//  Copyright © 2017 Nokia. All rights reserved.
//

/**********************************************
 This is the controller that opens PDF contents. DownloadedObject must exists and PDF File must be on the PDF folder.
 **********************************************/

#import "GlobalImport.h"

@interface PDFViewController : UIViewController

@property(nonatomic, strong) DownloadedObject *obj;

@property (nonatomic, strong) IBOutlet UIWebView *pdfViewer;

@end
