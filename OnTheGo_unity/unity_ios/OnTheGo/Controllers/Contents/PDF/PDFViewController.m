//
//  PDFViewController.m
//  OnTheGo
//
//  Created by Alessio Crestani on 16/11/2017.
//  Copyright © 2017 Nokia. All rights reserved.
//

#import "PDFViewController.h"

@interface PDFViewController ()

@end

@implementation PDFViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self setupView];
}

- (void)setupView
{
    [self setTitle:[_obj title]];
    [self.navigationController.navigationBar setTitleTextAttributes:@{NSFontAttributeName:[UIFont fontWithName:@"Nokia Pure Headline" size:18.0f],
                                                                      NSForegroundColorAttributeName:[UIColor colorWithRed:0.29 green:0.32 blue:0.32 alpha:1.0]
                                                                      }];
    [_pdfViewer loadData:[FileManager searchFileOnStorageOfObj:_obj] MIMEType:@"application/pdf" textEncodingName:@"utf-8" baseURL:[NSURL new]];
}

- (BOOL)canRotate
{
    return YES;
}


@end
