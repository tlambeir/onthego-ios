//
//  LearningStoreViewController.m
//  OnTheGo
//
//  Created by Alessio Crestani on 16/11/2017.
//  Copyright © 2017 Nokia. All rights reserved.
//

#import "LearningStoreViewController.h"
#import "GlobalImport.h"

@interface LearningStoreViewController ()

@end

@implementation LearningStoreViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self setupView];
}

- (void)setupView
{
    [self setTitle:NSLocalizedString(@"Learning Store", nil)];
    [self.navigationController.navigationBar setTitleTextAttributes:@{NSFontAttributeName:[UIFont fontWithName:@"Nokia Pure Headline" size:18.0f],
                                                                      NSForegroundColorAttributeName:[UIColor colorWithRed:0.29 green:0.32 blue:0.32 alpha:1.0]
                                                                      }];
    [_webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:LEARNING_STORE_URL]]];
}

@end
