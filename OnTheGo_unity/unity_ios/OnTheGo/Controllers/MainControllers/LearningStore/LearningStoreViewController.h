//
//  LearningStoreViewController.h
//  OnTheGo
//
//  Created by Alessio Crestani on 16/11/2017.
//  Copyright © 2017 Nokia. All rights reserved.
//

/**********************
 This controller allows user to enter on Nokia Education Store.
 Nokia Education Store is a web application that this application opens on a webview.
 **********************/



@interface LearningStoreViewController : UIViewController

@property(nonatomic, strong) IBOutlet UIWebView *webView;

@end
