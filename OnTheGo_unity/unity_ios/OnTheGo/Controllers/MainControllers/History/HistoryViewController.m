//
//  HistoryViewController.m
//  OnTheGo
//
//  Created by Alessio Crestani on 16/11/2017.
//  Copyright © 2017 Nokia. All rights reserved.
//

#import "HistoryViewController.h"
#import "PDFViewController.h"
#import "HTMLViewController.h"
#import "VideoPlayerViewController.h"


@implementation HistoryViewController

#pragma mark ## Default Methods ##

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self setupView];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if([[segue destinationViewController] class] == [ObjectListViewController class]){
        [(ObjectListViewController *)[segue destinationViewController] setIsDownloads:NO];
        _objectListController = (ObjectListViewController *)[segue destinationViewController];
    }
}

#pragma mark ## Setup View methods ##

- (void)setupView
{
    [self setTitle:NSLocalizedString(@"History", nil)];
    [self.navigationController.navigationBar setTitleTextAttributes:@{NSFontAttributeName:[UIFont fontWithName:@"Nokia Pure Headline" size:18.0f],
                                                                      NSForegroundColorAttributeName:[UIColor colorWithRed:0.29 green:0.32 blue:0.32 alpha:1.0]
                                                                      }];
    [_clearHistoryButton setTitle:@"Clear History" forState:UIControlStateNormal];
    if([_objectListController.downloadedObjects count] == 0){
        [_clearHistoryButton setHidden:YES];
    }
}

#pragma mark - ## Button Actions ##

- (IBAction)clearHistoryPressed:(id)sender
{
    NSString *alertTitle = NSLocalizedString(@"Clear History", nil);
    NSString *alertMessage = NSLocalizedString(@"Remove all the object from the History list?", nil);
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:alertTitle message:alertMessage preferredStyle:UIAlertControllerStyleAlert];
    
    [alert addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"Yes", nil) style:UIAlertActionStyleDestructive handler:^(UIAlertAction *action){
        [alert dismissViewControllerAnimated:YES completion:nil];
        [_objectListController clearHistory];
    }]];
    
    [alert addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"No", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
        [alert dismissViewControllerAnimated:YES completion:nil];
    }]];
    
    [self presentViewController:alert animated:YES completion:nil];
}


@end

