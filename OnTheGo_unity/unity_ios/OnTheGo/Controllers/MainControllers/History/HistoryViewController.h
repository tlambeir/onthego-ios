//
//  HistoryViewController.h
//  OnTheGo
//
//  Created by Alessio Crestani on 16/11/2017.
//  Copyright © 2017 Nokia. All rights reserved.
//


/********************
This class is accessed from the settings icon of the main controller. It allows user to see the history of the previous opened content with this device.
The single items can be removed and the entire list can be totally cleared.
*********************/

#import <AVFoundation/AVFoundation.h>
#import "ObjectListViewController.h"


@interface HistoryViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIButton *clearHistoryButton;
@property (weak, nonatomic) ObjectListViewController *objectListController;
@end
