//
//  ObjectListViewController.m
//  OnTheGo
//
//  Created by Alessio Crestani on 29/11/2017.
//  Copyright © 2017 Nokia. All rights reserved.
//

#import "ObjectListViewController.h"
#import "UITableView+NXEmptyView.h"
#import "PDFViewController.h"
#import "HTMLViewController.h"
#import "VideoPlayerViewController.h"
#import "TutorialViewController.h"
#import "GlobalImport.h"
#import "OnTheGoDelegate.h"
#import "VRARViewController.h"
#import "OnTheGoVideoPlayerController.h"
#import "AggregatedItemsListViewController.h"
#import <PopupKit/PopupView.h>

@interface ObjectListViewController()

@property(nonatomic) BOOL firstLoad;
@property (nonatomic, strong) DownloadObjPopup* popup;
@property (nonatomic) AVPlayer *player;

@end

@implementation ObjectListViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    _firstLoad = YES;
    [self loadObjects];
    [self setupView];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(didRotateDeviceChangeNotification:)
                                                 name:UIDeviceOrientationDidChangeNotification
                                               object:nil];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:UIDeviceOrientationDidChangeNotification];
}

-(void)didRotateDeviceChangeNotification:(NSNotification *)notification
{
    if(_popup != nil){
        [_popup removeFromSuperview];
        NSDictionary *obj = [_popup passedObj];
        _popup = nil;
        [self proceedWithDownloadPopup:obj];
        
    }
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    if(!_firstLoad)
        [self loadObjects];
    else
        _firstLoad = NO;
    
    if(_player != nil)
        [_player pause];
    
}

- (void)viewWillDisappear:(BOOL)animated
{
    [self stopSavingProcedure];
}

- (void)loadObjects
{
    if(_parentManagingUUID == nil){
        if(_isDownloads)
            _downloadedObjects = [[RealmManager sharedInstance] downloadedObjects];
        else
            _downloadedObjects = [[RealmManager sharedInstance] historyObjects];
    } else {
        _downloadedObjects = [[RealmManager sharedInstance] getChildrenItemsForParentUUID:_parentManagingUUID];
    }
    
    [_objectsTableView reloadData];
}

#pragma mark ## Setup View methods ##

- (void)setupView
{
    [self setupDownloadedContentTableView];
}

- (void)setupDownloadedContentTableView
{
    
    UILabel *placeholder = [[UILabel alloc] init];
    placeholder.font = [UIFont fontWithName:@"Nokia Pure Headline" size:22.0];
    placeholder.numberOfLines = 0;
    placeholder.text = NSLocalizedString(@"No content downloaded yet.", nil);
    placeholder.textAlignment = NSTextAlignmentCenter;
    placeholder.textColor = [UIColor lightGrayColor];
    placeholder.backgroundColor = [UIColor whiteColor];
    [placeholder setFrame:_objectsTableView.frame];
    _objectsTableView.nxEV_hideSeparatorLinesWhenShowingEmptyView = YES;
    [_objectsTableView setNxEV_emptyView:placeholder];
    
    [_objectsTableView setDataSource:self];
    [_objectsTableView setDelegate:self];
    _objectsTableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];

}

#pragma mark ## Button Actions ##

- (void)openContentWithDownloadedObject:(DownloadedObject *)downloadedObj
{
    [downloadedObj changeLastOpenedDate];
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Contents" bundle:nil];
    UIStoryboard *storyboardMain = [UIStoryboard storyboardWithName:@"Main" bundle:nil];

    switch ([[downloadedObj type] intValue]) {
        case PDF_OBJ:{
            PDFViewController *PDFViewerController = (PDFViewController *)[storyboard instantiateViewControllerWithIdentifier:@"PDFViewController"];
            [PDFViewerController setObj:downloadedObj];
            [self.navigationController pushViewController:PDFViewerController animated:YES];
            break;
        }
        case WEB_OBJ:
        case URL_OBJ:
        case WBT_OBJ:{
            HTMLViewController *webViewController = (HTMLViewController *)[storyboard instantiateViewControllerWithIdentifier:@"HTMLViewController"];
            [webViewController setObj:downloadedObj];
            [self.navigationController pushViewController:webViewController animated:YES];
            break;
        }
        case VIDEO_OBJ:
        case VIDEO_360_OBJ:
            [self openVideoWithObject:downloadedObj];
            break;
        case AR_3D_OBJ:{
            OnTheGoDelegate *appDelegate = (OnTheGoDelegate *)[[UIApplication sharedApplication] delegate];
            UIDeviceOrientation orientation = [[UIDevice currentDevice] orientation];
            if(orientation == UIDeviceOrientationPortrait)
                [appDelegate setLockedOrientation:UIInterfaceOrientationMaskPortrait];
            else if(orientation == UIDeviceOrientationLandscapeLeft)
                [appDelegate setLockedOrientation:UIInterfaceOrientationMaskLandscapeRight];
            else if(orientation == UIDeviceOrientationLandscapeRight)
                [appDelegate setLockedOrientation:UIInterfaceOrientationMaskLandscapeLeft];
            else
                [appDelegate setLockedOrientation:UIInterfaceOrientationMaskPortrait];
            
            VRARViewController *arController = (VRARViewController *)[storyboard instantiateViewControllerWithIdentifier:@"VRARViewController"];
            [arController setDownloadedObj:downloadedObj];
            [self.navigationController pushViewController:arController animated:YES];
            break;
            
        } case AGGREGATED_OBJ:{
            AggregatedItemsListViewController *aggregatedChildrenList = (AggregatedItemsListViewController *)[storyboardMain instantiateViewControllerWithIdentifier:@"AggregatedItemsListViewController"];
            [aggregatedChildrenList setParentManagingUUID:downloadedObj.uuid];
            [self.navigationController pushViewController:aggregatedChildrenList animated:YES];
        }
        default:
            break;
    }
    
}

- (void)openContentFromURL:(NSString *)URL
{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Contents" bundle:nil];
    HTMLViewController *webViewController = (HTMLViewController *)[storyboard instantiateViewControllerWithIdentifier:@"HTMLViewController"];
    [webViewController setURL:URL];
    [self.navigationController pushViewController:webViewController animated:YES];
}

- (void)openVideoWithObject:(DownloadedObject *)downloadedObj
{
    if([downloadedObj.type intValue] == VIDEO_OBJ) {
        NSURL *videoURL = [downloadedObj getObjFilePath];
        _player = [AVPlayer playerWithURL:videoURL];
        NSError *err;
        [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryPlayback withOptions:@{} error:&err];
        OnTheGoVideoPlayerController *controller = [[OnTheGoVideoPlayerController alloc] init];
        controller.player = _player;
        [controller setTitle:downloadedObj.title];
        // show the view controller
        [self.navigationController pushViewController:controller animated:YES];
        
        [_player play];
        
    } else if([downloadedObj.type intValue] == VIDEO_360_OBJ){
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Contents" bundle:nil];
        VideoPlayerViewController *videoViewerController = (VideoPlayerViewController *)[storyboard instantiateViewControllerWithIdentifier:@"VideoPlayerViewController"];
        [videoViewerController setObj:downloadedObj];
        [self.navigationController pushViewController:videoViewerController animated:YES];
    }
}

#pragma mark ## QR Reading ##

-(void)captureOutput:(AVCaptureOutput *)captureOutput didOutputMetadataObjects:(NSArray *)metadataObjects fromConnection:(AVCaptureConnection *)connection
{
}

#pragma mark ## UITableViewDataDelegates ##


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(nonnull NSIndexPath *)indexPath
{
    DownloadedObjectCell *cell = [tableView dequeueReusableCellWithIdentifier:@"DownloadedObjectCell"];
    if (cell == nil)
        cell = [[DownloadedObjectCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"DownloadedObjectCell"];
        DownloadedObject *downloadedObject = [_downloadedObjects objectAtIndex:indexPath.row];
        [cell setupViewWithDownloadedObject:downloadedObject];
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 140.0f;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _downloadedObjects.count;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    DownloadedObject *downloadedObject = [_downloadedObjects objectAtIndex:indexPath.row];
    
    NSString *alertTitle;
    NSString *alertMessage;
    
    if(_isDownloads){
        alertTitle = NSLocalizedString(@"Delete content from download list", nil);
        alertMessage = [NSString stringWithFormat:NSLocalizedString(@"Would you delete %@? Local files will be deleted but It can be restored from History.", nil), [downloadedObject title]];
    } else{
        alertTitle = NSLocalizedString(@"Delete content from history", nil);
        alertMessage = [NSString stringWithFormat:NSLocalizedString(@"Would you delete %@ from history? Content will remain on download list. Local files will remain.", nil), [downloadedObject title]];
    }
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:alertTitle message:alertMessage preferredStyle:UIAlertControllerStyleAlert];
    
    [alert addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"Yes", nil) style:UIAlertActionStyleDestructive handler:^(UIAlertAction *action){
        [alert dismissViewControllerAnimated:YES completion:nil];
        if(_isDownloads)
            [[RealmManager sharedInstance] removeDownloadedObjectForDownload:downloadedObject];
        else
            [[RealmManager sharedInstance] removeDownloadedObjectForHistory:downloadedObject];
        [_objectsTableView reloadData];
    }]];
    
    [alert addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"No", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
        [alert dismissViewControllerAnimated:YES completion:nil];
    }]];
    
    [self presentViewController:alert animated:YES completion:nil];
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    DownloadedObject *downloadedObject = [_downloadedObjects objectAtIndex:indexPath.row];
    if([downloadedObject isOnDevice])
        [self openContentWithDownloadedObject:downloadedObject];
    else {
        if(_parentManagingUUID == nil){
            NSString *alertTitle = NSLocalizedString(@"Selected content is not on device", nil);
            NSString *alertMessage = NSLocalizedString(@"Would you download It?", nil);
        
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:alertTitle message:alertMessage preferredStyle:UIAlertControllerStyleAlert];
        
            [alert addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"Yes", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                [alert dismissViewControllerAnimated:YES completion:nil];
                NSMutableDictionary *dictObj = [NSJSONSerialization JSONObjectWithData:[[downloadedObject JSONObj] dataUsingEncoding:NSUTF8StringEncoding] options:kNilOptions error:nil];
                [self startSavingProcedureForObject:dictObj];
            }]];
        
            [alert addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"No", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                [alert dismissViewControllerAnimated:YES completion:nil];
            }]];
        
            [self presentViewController:alert animated:YES completion:nil];
        } else {
            [self proceedSavingProcedureWithDownloadedObject:downloadedObject];

        }
    }
}

#pragma mark ## Popup for Downloaded Content

- (void)showPopupToDownloadContent:(NSDictionary *)downloadedObjContent
{
    [self proceedWithDownloadPopup:downloadedObjContent];
}

- (void)proceedWithDownloadPopup:(NSDictionary *)downloadedObjContent
{
    DownloadObjPopup* objContentView = [[[NSBundle mainBundle] loadNibNamed:@"DownloadObjPopup" owner:self options:nil] lastObject];
    [objContentView setPassedObj:downloadedObjContent];
    [objContentView setDelegate:self];
    [objContentView setupViewWithDictionary:downloadedObjContent];
    if(objContentView.frame.size.width > [[UIScreen mainScreen] bounds].size.width){
        float ratio = (([[UIScreen mainScreen] bounds].size.width - 20) / objContentView.frame.size.width);
        [objContentView setFrame:CGRectMake(objContentView.frame.origin.x, objContentView.frame.origin.y, [[UIScreen mainScreen] bounds].size.width - 20, objContentView.frame.size.height * ratio)];
    }
    
    [objContentView setFrame:CGRectMake(([[UIScreen mainScreen] bounds].size.width - objContentView.frame.size.width)/2, ([[UIScreen mainScreen] bounds].size.height - objContentView.frame.size.height) / 2 + 50, objContentView.frame.size.width, objContentView.frame.size.height)];
    
    [self.parentViewController.view addSubview:objContentView];
    [self.parentViewController.view bringSubviewToFront:objContentView];
    
    _popup = objContentView;
}

- (void)startSavingProcedureForObject:(NSDictionary *)downloadedObjectJSON
{
    dispatch_async(dispatch_get_main_queue(), ^{
        if(_popup != nil)
            [_popup removeFromSuperview];
        _popup = nil;
        DownloadedObject *downloadedObj = [[RealmManager sharedInstance] createDownloadedObjectWithData:downloadedObjectJSON];
        [self proceedSavingProcedureWithDownloadedObject:downloadedObj];
    });
}

- (void)proceedSavingProcedureWithDownloadedObject:(DownloadedObject *)downloadedObj
{
    [SVProgressHUD showWithStatus:NSLocalizedString(@"Downloading content", nil)];
    if(![FileManager fileDownloadedForObj:downloadedObj] && [downloadedObj downloadableFile]){
        if([[downloadedObj storageType] isEqualToString:STORAGE_TYPE_SHAREPOINT] || [[downloadedObj objURL] containsString:@"sharepoint.com/sites"]){
            [SVProgressHUD dismiss];
            [self proceedWithSharePoint:downloadedObj];
        } else {
            NSLog(@"File not downloaded yet. Proceeding with download...", nil);
            [self downloadObjectFiles:downloadedObj andOptionalSharepointTemporaryURL:nil];
        }
    } else{
        NSLog(@"File already downloaded on the storage or not downloadable. Proceeding with thumb...", nil);
        [self proceedWithThumbDownload:downloadedObj];
    }
}

- (void)proceedWithSharePoint:(DownloadedObject *)downloadedObj
{
    NSString *objID = [downloadedObj uuid];
    [NetworkManager getTokenForMSGraph:^(NSString * _Nullable token) {
        dispatch_async(dispatch_get_main_queue(), ^{
            DownloadedObject *obj = [[RealmManager sharedInstance] getDownloadedObjWithUUID:objID];
            if(token != nil && ![token isEqualToString:@""]){ //token OK
                [SVProgressHUD showWithStatus:NSLocalizedString(@"Downloading content", nil)];
                NSString *sharepointBaseURL;
                if(![SHAREPOINT_TENANT isEqualToString:@"si14spa615"]){
                    sharepointBaseURL = [obj objURL];
                } else {
                    sharepointBaseURL = [[[[obj objURL] stringByReplacingOccurrencesOfString:@"nokia" withString:@"si14spa615"] stringByReplacingOccurrencesOfString:@"testingonthego" withString:@"testOnTheGo"] stringByReplacingOccurrencesOfString:@"Shared%20Documents" withString:@"Documenti%20Condivisi"];
                }

//                if((TypeOfile)[[obj type] intValue] == WEB_OBJ){
//                  //  sharepointBaseURL = [sharepointBaseURL stringByAppendingString:ZIP_URL_PATH_COMPONENT];
//                } else if((TypeOfile)[[obj type] intValue] == AR_3D_OBJ){
//                    sharepointBaseURL = [sharepointBaseURL stringByAppendingString:IOS_MODEL_PATH_COMPONENT];
//                }
                [NetworkManager downloadSharePointFileWithBaseURL:sharepointBaseURL withFileType:[[obj type] intValue] andMSToken:token andSuccessfulBlock:^(NSString *downloadURL) {
                    NSLog(@"Sharepoint download success");
                    [self downloadObjectFiles:obj andOptionalSharepointTemporaryURL:downloadURL];
                } andFailureBlock:^(NSString *errorDescription) {
                    NSLog(@"%@", errorDescription);
                    [SVProgressHUD dismiss];
                    //[SVProgressHUD showErrorWithStatus:NSLocalizedString(@"Fail to retrieve file from sharepoint", nil)];
                    [self setManagingObject:NO];
                    [[RealmManager sharedInstance] removeDownloadedObjectFromRealm:obj withLocalFileDeletion:YES];
                    [_objectsTableView reloadData];
                }];
            } else{
                [SVProgressHUD showErrorWithStatus:NSLocalizedString(@"Sharepoint Auth system fails", nil)];
                [self setManagingObject:NO];
                [[RealmManager sharedInstance] removeDownloadedObjectFromRealm:obj withLocalFileDeletion:YES];
                [_objectsTableView reloadData];
            }
        });
    }];
}


- (void)downloadObjectFiles:(DownloadedObject *)downloadedObj andOptionalSharepointTemporaryURL:(NSString *)sharepointDownloadURL
{
    NSString *fileURLToDownload = sharepointDownloadURL == nil ? [downloadedObj objURL] : sharepointDownloadURL;
    [NetworkManager downloadDataFromURL:[NSURL URLWithString:fileURLToDownload] withFilePath:[downloadedObj getDownloadFilePath] withFileType:[[downloadedObj type] intValue] withProgress:^(NSProgress * _Nonnull downloadProgress) {
        [SVProgressHUD showProgress:downloadProgress.fractionCompleted status:NSLocalizedString(@"Downloading content", nil)];
    } withCompletion:^(NSURLResponse *response, NSURL *filePath, NSError *error) {
        if(error == nil || [[downloadedObj type] intValue] == WEB_OBJ){
            if(error != nil && [[downloadedObj type] intValue] == WEB_OBJ){ //when the object is HTML but can't download, just save the original URL and open from Online
                [FileManager deleteEmptyWebZipFiles:downloadedObj];
                [[RealmManager sharedInstance] changeURLOfWebObject:downloadedObj];
            }
            NSLog(@"File Downloaded. Proceeding with thumbs", nil);
            [Utilities manageOperationsOnDownloadedFiles:downloadedObj withCompletion:^(BOOL success) {
                if(success){
                    NSLog(@"done", nil);
                    [self proceedWithThumbDownload:downloadedObj];
                } else {
                    [self setManagingObject:NO];
                    [[RealmManager sharedInstance] removeDownloadedObjectFromRealm:downloadedObj withLocalFileDeletion:YES];
                    [SVProgressHUD showErrorWithStatus:NSLocalizedString(@"Some error occours. Please retry.", nil)];
                }
            }];
        } else{
            [SVProgressHUD dismiss];
            [self setManagingObject:NO];
            [[RealmManager sharedInstance] removeDownloadedObjectFromRealm:downloadedObj withLocalFileDeletion:YES];
            NSLog([NSString stringWithFormat:@"File Download error. Error:%@ Aborting....", [error localizedDescription]], nil);
            [SVProgressHUD showErrorWithStatus:NSLocalizedString(@"Some error occours. Please retry.", nil)];
        }
    }];
}

- (void)proceedWithThumbDownload:(DownloadedObject *)downloadedObj
{
    if(![FileManager thumbDownloadedForObj:downloadedObj] && ([downloadedObj thumbURL] != nil) && ![[downloadedObj thumbURL] isEqualToString:@""]){
        NSLog(@"Thumb not downloaded yet. Proceeding with download...", nil);
        [NetworkManager downloadDataFromURL:[NSURL URLWithString:[downloadedObj thumbURL]] withFilePath:[downloadedObj getObjThumb] withFileType:IMAGE_OBJ withProgress:^(NSProgress * _Nonnull downloadProgress) {} withCompletion:^(NSURLResponse *response, NSURL *filePath, NSError *error) {
            [SVProgressHUD dismiss];
            if(error == nil){
                NSLog(@"Thumb Downloaded. Opening file...", nil);
                [self downloadOfObjectEndend:downloadedObj];
            } else {
                NSLog([NSString stringWithFormat:@"Thumb Download error. Error:%@ Aborting....", [error localizedDescription]], nil);
                [SVProgressHUD showErrorWithStatus:NSLocalizedString(@"Some error occours. Please retry.", nil)];
            }
        }];
    } else {
        NSLog(@"Thumb already downloaded. Proceeding...", nil);
        [self downloadOfObjectEndend:downloadedObj];
    }
}

- (void)downloadOfObjectEndend:(DownloadedObject *)downloadedObj
{
    [_objectsTableView reloadData];
    [self setManagingObject:NO];
    [SVProgressHUD dismiss];
    if(([[downloadedObj type] intValue] != AGGREGATED_OBJ) && [downloadedObj uuidParent] == nil){
        [self openContentWithDownloadedObject:downloadedObj];
    } else if([downloadedObj uuidParent] != nil){
        [self manageBrothersDownloadOfAggregatedObj:downloadedObj];
    } else
        [self manageChildrenDownloadOfAggregatedObj:downloadedObj];
}

- (void)manageChildrenDownloadOfAggregatedObj:(DownloadedObject *)parentObj
{
    RLMResults<DownloadedObject *> *children = [[RealmManager  sharedInstance] getChildrenItemsForParentUUID:[parentObj uuid]];
    [self manageChildrenDownload:children];
}

- (void)manageBrothersDownloadOfAggregatedObj:(DownloadedObject *)childrenObj
{
    RLMResults<DownloadedObject *> *children = [[RealmManager  sharedInstance] getChildrenItemsForParentUUID:childrenObj.uuidParent];
    [self manageChildrenDownload:children];
}

- (void)manageChildrenDownload:(RLMResults<DownloadedObject *> *)children
{
    for(DownloadedObject *child in children){
        if([self haveToDownloadChildrenFileOrThumb:child]){
            [self proceedSavingProcedureWithDownloadedObject:child];
            return;
        }
    }
    DownloadedObject *parent = [[RealmManager sharedInstance] getDownloadedObjWithUUID:children.firstObject.uuidParent];
    [self openContentWithDownloadedObject:parent];
}

- (BOOL)haveToDownloadChildrenFileOrThumb:(DownloadedObject *)downloadedObj
{
    return (![FileManager fileDownloadedForObj:downloadedObj] && [downloadedObj downloadableFile]) || (![FileManager thumbDownloadedForObj:downloadedObj] && ([downloadedObj thumbURL] != nil) && ![[downloadedObj thumbURL] isEqualToString:@""]);
}

- (void)stopSavingProcedure
{
    dispatch_async(dispatch_get_main_queue(), ^{
        if(_popup != nil)
            [_popup removeFromSuperview];
        [self setManagingObject:NO];
        _popup = nil;
    });
}

-(void)manageURLString:(NSString *)urlString
{
    if(!_isManagingAnObject){
        [self setManagingObject:YES];
        [SVProgressHUD showWithStatus:NSLocalizedString(@"Downloading QR informations...", nil)];
        [NetworkManager getObjectWithURL:urlString withCompletion:^(NSURLSessionDataTask *task, id responseObject) {
            [SVProgressHUD dismiss];
            if(task.error == nil){
                NSDictionary *response = responseObject;
                if([Utilities hasDownloadedObjTheMinimumDataSet:response]){
                    [self showPopupToDownloadContent:response];
                } else{
                    [SVProgressHUD showErrorWithStatus:NSLocalizedString(@"Incomplete Object", nil)];
                    [self setManagingObject:NO];
                }
            } else{
                [SVProgressHUD showErrorWithStatus:[task.error localizedDescription]];
                [self setManagingObject:NO];
            }
        } andFailure:^(NSURLSessionDataTask *task, NSError *error) {
            [SVProgressHUD dismiss];
            [self setManagingObject:NO];
            if(error.code != 3840)
                [self openContentFromURL:urlString];
            else
                [SVProgressHUD showErrorWithStatus:NSLocalizedString(@"QR is not in the right format.", nil)];
        }];
    }
}

- (void)setManagingObject:(BOOL)managingObj
{
    if(!managingObj){
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 2 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
            _isManagingAnObject = managingObj;
        });
    }
    else {
        _isManagingAnObject = managingObj;
    }
    NSLog(@"NOKIA TEST: Managing obj: %@", managingObj ? @"YES" : @"NO");
}

-(void)clearHistory
{
    if(![self isDownloads]){
        for(DownloadedObject *obj in _downloadedObjects){
            [[RealmManager sharedInstance] removeDownloadedObjectForHistory:obj];
        }
    }
    [_objectsTableView reloadData];
}



@end
