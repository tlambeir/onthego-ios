//
//  ObjectListViewController.h
//  OnTheGo
//
//  Created by Alessio Crestani on 29/11/2017.
//  Copyright © 2017 Nokia. All rights reserved.
//

/**
 * Used to embed the same logic of Downloaded Table on MainViewController and HistoryViewController
 **/

#import "DownloadedObjectCell.h"
#import "GlobalImport.h"
#import <AVFoundation/AVFoundation.h>
#import <AVKit/AVKit.h>


@interface ObjectListViewController : UIViewController<UITableViewDelegate, UITableViewDataSource, DownloadObjPopupDelegate, AVPlayerViewControllerDelegate>

@property (weak, nonatomic) IBOutlet UITableView *objectsTableView;
@property (nonatomic) BOOL isDownloads;
@property (nonatomic) BOOL isManagingAnObject;
@property (strong, nonatomic) NSString *parentManagingUUID;
@property (nonatomic, strong) RLMResults *downloadedObjects;

-(void)manageURLString:(NSString *)urlString;
-(void)clearHistory;
@end
