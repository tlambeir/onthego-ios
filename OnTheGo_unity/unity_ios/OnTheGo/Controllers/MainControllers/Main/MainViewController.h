//
//  MainViewController.h
//  OnTheGo
//
//  Created by Alessio Crestani on 16/11/2017.
//  Copyright © 2017 Nokia. All rights reserved.
//


/***********************************************
 This is the main class of OnTheGo. It allows user to access all the featura of the application:
 - With the settings icon It allows to open the slideshow tutorial, the education store and the history of the previous opened contents
 - With the main part of the controller, It allows to scan QR and download content. After the download, It opens the right controller to open the downloaded file.
 - It allows to have the list of the previous downloaded content on the device. If user tap on one of them, It opens the right controller to open the file.
 **********************************************/


#import <AVFoundation/AVFoundation.h>
#import "ObjectListViewController.h"
#import "GlobalImport.h"

@interface MainViewController : UIViewController <AVCaptureMetadataOutputObjectsDelegate>

@property (strong, nonatomic) UIBarButtonItem *scanOrListBarButton;
@property (strong, nonatomic) UIButton *scanOrListButton;

@property (strong, nonatomic) UIBarButtonItem *moreActionsBarButton;
@property (strong, nonatomic) UIButton *moreActionsButton;


@property (weak, nonatomic) IBOutlet UIView *QRModeContainer;
@property (weak, nonatomic) IBOutlet UIView *QRPreview;
@property (weak, nonatomic) IBOutlet UIView *QROverlayView;
@property (weak, nonatomic) IBOutlet UIImageView *QROverlaySquareImage;
@property (weak, nonatomic) IBOutlet UILabel *QROverlayMessageLabel;

@property (weak, nonatomic) IBOutlet UIView *downloadListContainer;
@property (weak, nonatomic) IBOutlet UIView *containerViewList;
@property (weak, nonatomic) ObjectListViewController *objectsListViewController;

@property (nonatomic) BOOL isReadingQR;
@property (nonatomic, strong) AVCaptureMetadataOutput *captureMetadataOutput;
@property (nonatomic, strong) UIBezierPath *overlayBezier;
@property (nonatomic, strong) UIBezierPath *hole;
@property (nonatomic, strong) CAShapeLayer *shapeLayer;
@property (nonatomic) UIDeviceOrientation orientation;
@property (nonatomic) BOOL isFirstTime;

@end

