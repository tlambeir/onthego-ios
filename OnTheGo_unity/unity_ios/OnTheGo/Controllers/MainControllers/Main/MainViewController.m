//
//  MainViewController.m
//  OnTheGo
//
//  Created by Alessio Crestani on 16/11/2017.
//  Copyright © 2017 Nokia. All rights reserved.
//

#import "MainViewController.h"
#import "DropDown-Swift.h"
#import "PDFViewController.h"
#import "HTMLViewController.h"
#import "VideoPlayerViewController.h"
#import "TutorialViewController.h"
#import <Crashlytics/Crashlytics.h>
#import "OnTheGoDelegate.h"


@interface MainViewController ()

@property (nonatomic) BOOL isInQRView;
@property (nonatomic, strong) DropDown *dropDown;
@property (nonatomic, strong) AVCaptureSession *captureSession;
@property (nonatomic, strong) AVCaptureVideoPreviewLayer *videoPreviewLayer;

@end

@implementation MainViewController

#pragma mark ## Default Methods ##

- (void)viewDidLoad
{
    [super viewDidLoad];
    if(![PreferencesManager getIntroSeen]){
        [self performSegueWithIdentifier:TUTORIAL_SEGUE sender:[NSNumber numberWithBool:YES]];
    }
    _isInQRView = [[[RealmManager sharedInstance] downloadedObjects] count] == 0;
    _captureSession = nil;
    _isFirstTime = YES;
    [self setupView];
}



- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    if(![PreferencesManager getGDPRAccepted]){
        [self performSegueWithIdentifier:GDPR_SEGUE sender:nil];
    }
    [[NSNotificationCenter defaultCenter] addObserver:self  selector:@selector(orientationChanged:) name:UIDeviceOrientationDidChangeNotification  object:nil];
    
    OnTheGoDelegate *appDelegate = (OnTheGoDelegate *)[[UIApplication sharedApplication] delegate];
    if([PreferencesManager getIntroSeen] && _isFirstTime){
        [SVProgressHUD showWithStatus:NSLocalizedString(@"Initializing", nil)];
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 1 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
            [appDelegate startUnity];
            [appDelegate stopUnity];
            [SVProgressHUD dismiss];
            _isFirstTime = NO;
        });

    }
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:UIDeviceOrientationDidChangeNotification];
}

- (void)orientationChanged:(NSNotification *)notification
{
//    NSLog(@"Status bar hidden: %d", [[UIApplication sharedApplication] isStatusBarHidden]);
//    if([[UIApplication sharedApplication] isStatusBarHidden]){
//        [_QRModeContainer setFrame:CGRectMake(_QRModeContainer.frame.origin.x, _QRModeContainer.frame.origin.y - 20, _QRModeContainer.frame.size.width, _QRModeContainer.frame.size.height)];
//    } else {
//         [_QRModeContainer setFrame:CGRectMake(_QRModeContainer.frame.origin.x , _QRModeContainer.frame.origin.y + 20, _QRModeContainer.frame.size.width, _QRModeContainer.frame.size.height)];
//    }
    // [self setupQRView];
    // [_videoPreviewLayer.connection setVideoOrientation:AVCaptureVideoOrientationLandscapeLeft];
}

- (AVCaptureVideoOrientation)videoOrientationFromCurrentDeviceOrientation
{
    switch([UIApplication sharedApplication].statusBarOrientation){
        case portrait:
            return AVCaptureVideoOrientationPortrait;
        case landscapeLeft:
            return AVCaptureVideoOrientationLandscapeRight;
        case landscapeRight:
            return AVCaptureVideoOrientationLandscapeLeft;
        case portraitUpsideDown:
            return AVCaptureVideoOrientationPortraitUpsideDown;
        default:
            return AVCaptureVideoOrientationPortrait;
    }
}


- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if([[segue destinationViewController] class] == [TutorialViewController class]){
        [(TutorialViewController *)[segue destinationViewController] setIsFirstStart:[((NSNumber *)sender) boolValue]];
    } else if([[segue destinationViewController] class] == [ObjectListViewController class]){
        [(ObjectListViewController *)[segue destinationViewController] setIsDownloads:YES];
        _objectsListViewController = (ObjectListViewController *)[segue destinationViewController];
    }
}

#pragma mark ## Setup View methods ##


- (void)setupView
{
    [self setupButtonBarForQR];
    [self setupDropDownButton];
    [self setupDropDownMenu];
    [self setupChangeViewForQR:_isInQRView animated:NO];
    [self checkCameraPermission];
    
}

- (void)checkCameraPermission
{
    NSString *mediaType = AVMediaTypeVideo;
    AVAuthorizationStatus authStatus = [AVCaptureDevice authorizationStatusForMediaType:mediaType];
    if(authStatus == AVAuthorizationStatusDenied || authStatus == AVAuthorizationStatusRestricted){
        [SVProgressHUD showErrorWithStatus:NSLocalizedString(@"You denied access to camera, please go to device settings and grant to OnTheGo camera permission", nil)];
    } else if(authStatus == AVAuthorizationStatusNotDetermined){
        [AVCaptureDevice requestAccessForMediaType:mediaType completionHandler:^(BOOL granted) {
            if(granted){
                NSLog(@"Granted access to %@", mediaType);
            } else {
                NSLog(@"Not granted access to %@", mediaType);
            }
        }];
    }
}

- (void)setupQRView
{
    NSError *error;
    
    AVCaptureDevice *captureDevice = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
    
    AVCaptureDeviceInput *input = [AVCaptureDeviceInput deviceInputWithDevice:captureDevice error:&error];
    if (!input) {
        NSLog(@"%@", [error localizedDescription]);
        return;
    }
    
    if(_captureSession != nil){
        [_captureSession stopRunning];
        _captureSession = nil;
        _captureMetadataOutput = nil;
        _videoPreviewLayer = nil;
    }
    
    _captureSession = [[AVCaptureSession alloc] init];
    [_captureSession addInput:input];
    [_shapeLayer removeFromSuperlayer];
    
    
    _captureMetadataOutput = [[AVCaptureMetadataOutput alloc] init];
    [_captureSession addOutput:_captureMetadataOutput];
    
    dispatch_queue_t dispatchQueue;
    dispatchQueue = dispatch_queue_create("QrCodeQueue", NULL);
    [_captureMetadataOutput setMetadataObjectsDelegate:self queue:dispatchQueue];
    [_captureMetadataOutput setMetadataObjectTypes:[NSArray arrayWithObject:AVMetadataObjectTypeQRCode]];
    
    _videoPreviewLayer = [[AVCaptureVideoPreviewLayer alloc] initWithSession:_captureSession];
    [_videoPreviewLayer setVideoGravity:AVLayerVideoGravityResizeAspectFill];
    [_videoPreviewLayer setFrame:_QRPreview.layer.bounds];
    [_videoPreviewLayer.connection setVideoOrientation:[self videoOrientationFromCurrentDeviceOrientation]];
    [_QRPreview.layer addSublayer:_videoPreviewLayer];
    [_captureSession startRunning];
    [_QROverlayMessageLabel setText:NSLocalizedString(@"Scan a QR code to start", nil)];
    _overlayBezier = [UIBezierPath bezierPathWithRect:CGRectMake(_QROverlayView.frame.origin.x, _QROverlayView.frame.origin.y, _QROverlayView.frame.size.width, _QROverlayView.frame.size.height)];
    _hole = [UIBezierPath bezierPathWithRect:CGRectMake(_QROverlaySquareImage.frame.origin.x + 5, _QROverlaySquareImage.frame.origin.y + 5, _QROverlaySquareImage.frame.size.width - 10, _QROverlaySquareImage.frame.size.height - 10)];
    [_overlayBezier appendPath:_hole];
    [_overlayBezier setUsesEvenOddFillRule:YES];
    _shapeLayer = [[CAShapeLayer alloc] init];
    [_shapeLayer setPath:_overlayBezier.CGPath];
    [_shapeLayer setFillRule:kCAFillRuleEvenOdd];
    [_shapeLayer setFillColor:_QROverlayView.backgroundColor.CGColor];
    [_QROverlayView.layer addSublayer:_shapeLayer];
    [_QROverlayView bringSubviewToFront:_QROverlayMessageLabel];
    [_QROverlayMessageLabel setFont:[UIFont fontWithName:@"Nokia Pure Headline" size:17.0]];
    [_QROverlayView bringSubviewToFront:_QROverlaySquareImage];
    [_QROverlayMessageLabel setFrame:CGRectMake(_QROverlayMessageLabel.frame.origin.x, _QROverlaySquareImage.frame.origin.y + _QROverlaySquareImage.frame.size.height, _QROverlayMessageLabel.frame.size.width, _QROverlayMessageLabel.frame.size.height)];
    
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [self setupQRView];
    [self setRectOfInterest];
}

- (void)setRectOfInterest
{
    CGRect rectOfInterest = [_videoPreviewLayer metadataOutputRectOfInterestForRect:_QROverlaySquareImage.frame];
    [_captureMetadataOutput setRectOfInterest:rectOfInterest];
}

- (void)dismissQRView
{
    if(_captureSession != nil){
        [_captureSession stopRunning];
        _captureSession = nil;
    }
    if(_videoPreviewLayer != nil)
        [_videoPreviewLayer removeFromSuperlayer];
}

- (void)setupChangeViewForQR:(BOOL)isForQRCode animated:(BOOL)animated
{
    if(isForQRCode){
        self.navigationItem.titleView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"title_logo"]];
    } else {
        self.navigationItem.titleView = nil;
        [self setTitle:NSLocalizedString(@"Downloads", nil)];
        [self.navigationController.navigationBar setTitleTextAttributes:@{NSFontAttributeName:[UIFont fontWithName:@"Nokia Pure Headline" size:18.0f],
                                                                          NSForegroundColorAttributeName:[UIColor colorWithRed:0.29 green:0.32 blue:0.32 alpha:1.0]
                                                                          }];
        [_objectsListViewController stopSavingProcedure];
    }
    
    [self setupButtonBarForQR];
    if(isForQRCode){
        if(animated){
            [_scanOrListButton setUserInteractionEnabled:NO];
            [UIView animateWithDuration:0.5
                                  delay:0.0
                                options:UIViewAnimationOptionCurveEaseOut
                             animations:^(void) {
                                 [_downloadListContainer setFrame:CGRectMake(_containerViewList.frame.origin.x, self.view.frame.size.height, _downloadListContainer.frame.size.width, self.view.frame.size.height)];
                             }
                             completion:^(BOOL finished) {
                                 if(finished){
                                     [_downloadListContainer setHidden:YES];
                                     [_scanOrListButton setUserInteractionEnabled:YES];
                                 }
                             }];
            
            [_QRModeContainer setHidden:NO];
        } else {
            [_QRModeContainer setHidden:NO];
            [_downloadListContainer setFrame:CGRectMake(_containerViewList.frame.origin.x, self.view.frame.size.height, _downloadListContainer.frame.size.width, self.view.frame.size.height)];
            [_downloadListContainer setHidden:YES];
        }
        
    } else {
        if(animated){
            [_scanOrListButton setUserInteractionEnabled:NO];
            [_downloadListContainer setFrame:CGRectMake(_downloadListContainer.frame.origin.x, self.view.frame.size.height, _downloadListContainer.frame.size.width, _downloadListContainer.frame.size.height)];
            [_downloadListContainer setHidden:NO];
            [UIView animateWithDuration:0.5
                                  delay:0.0
                                options:UIViewAnimationOptionCurveEaseIn
                             animations:^(void) {
                                 [_downloadListContainer setFrame:CGRectMake(_downloadListContainer.frame.origin.x, 0, _downloadListContainer.frame.size.width, self.view.frame.size.height)];
                             }
                             completion:^(BOOL finished) {
                                 if(finished)
                                     [_scanOrListButton setUserInteractionEnabled:YES];
                                 [_QRModeContainer setHidden:YES];
                                 
                             }];
        } else {
            [_downloadListContainer setHidden:NO];
            [_downloadListContainer setFrame:CGRectMake(_downloadListContainer.frame.origin.x, 0, _downloadListContainer.frame.size.width, self.view.frame.size.height)];
            [_QRModeContainer setHidden:YES];
        }
    }
}

- (void)setupDropDownButton
{
    if(_moreActionsBarButton != nil){
        [_moreActionsBarButton.customView removeFromSuperview];
    }
    
    UIImage *scanOrListImage;
    scanOrListImage = [UIImage imageNamed:@"more_options_icon"];
    _moreActionsButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [_moreActionsButton addTarget:self action:@selector(openDropDown:) forControlEvents:UIControlEventTouchUpInside];
    [_moreActionsButton setImage:scanOrListImage forState:UIControlStateNormal];
    _moreActionsButton.showsTouchWhenHighlighted = YES;
    _moreActionsButton.frame = CGRectMake(0.0, 3.0, 20, 20);
    _scanOrListBarButton = [[UIBarButtonItem alloc] initWithCustomView:_moreActionsButton];
    [self.navigationItem setRightBarButtonItem:_scanOrListBarButton];
    
    if (@available(iOS 9, *)) {
        [_moreActionsButton.widthAnchor constraintEqualToConstant:32].active = YES;
        [_moreActionsButton.heightAnchor constraintEqualToConstant:32].active = YES;
    }
    
}

- (void)setupButtonBarForQR
{
    if(_scanOrListBarButton != nil){
        [_scanOrListBarButton.customView removeFromSuperview];
    }
    
    UIImage *scanOrListImage;
    _scanOrListButton = [UIButton buttonWithType:UIButtonTypeCustom];
    if(_isInQRView){
        scanOrListImage = [UIImage imageNamed:@"menuicon"];
        [_scanOrListButton addTarget:self action:@selector(swapToListView:) forControlEvents:UIControlEventTouchUpInside];
        
    } else {
        scanOrListImage = [UIImage imageNamed:@"qr_icon"];
        [_scanOrListButton addTarget:self action:@selector(swapToQRView:) forControlEvents:UIControlEventTouchUpInside];
        
    }
    
    [_scanOrListButton setImage:scanOrListImage forState:UIControlStateNormal];
    _scanOrListButton.showsTouchWhenHighlighted = YES;
    _scanOrListButton.frame = CGRectMake(0.0, 3.0, 20, 20);
    _scanOrListBarButton = [[UIBarButtonItem alloc] initWithCustomView:_scanOrListButton];
    [self.navigationItem setLeftBarButtonItem:_scanOrListBarButton];
    
    if (@available(iOS 9, *)) {
        [_scanOrListButton.widthAnchor constraintEqualToConstant:32].active = YES;
        [_scanOrListButton.heightAnchor constraintEqualToConstant:32].active = YES;
    }
}

- (void)setupDropDownMenu
{
    // Create an array of strings you want to show in the picker:
    NSArray *actions;
    if([PreferencesManager getMSLastUserID] != nil && ![[PreferencesManager getMSLastUserID] isEqualToString:@""])
        actions = [NSArray arrayWithObjects:NSLocalizedString(@"History", nil), NSLocalizedString(@"Tutorial", nil), NSLocalizedString(@"Logout Sharepoint", nil),  nil];
    else
        actions = [NSArray arrayWithObjects:NSLocalizedString(@"History", nil), NSLocalizedString(@"Tutorial", nil), nil];
    
    _dropDown = [DropDown new];
    
    [_dropDown setAnchorView:_moreActionsButton];
    [_dropDown setDataSource:actions];
    [_dropDown setTextFont:[UIFont fontWithName:@"Nokia Pure Headline" size:_dropDown.textFont.pointSize]];
    
    //customize dropDown view
    [_dropDown setBottomOffset:CGPointMake(-100, self.navigationController.navigationBar.frame.size.height)];
    [_dropDown setCellHeight:60.0f];
    [_dropDown setShadowRadius:1];
    
    __weak MainViewController *weakSelf = self;
    [_dropDown setSelectionAction:^(NSInteger index, NSString * _Nonnull title) {
        switch (index) {
            case 0:
                [weakSelf openController:TYPE_HISTORY];
                break;
            case 1:
                [weakSelf openController:TYPE_TUTORIAL];
                break;
            case 2:
                [weakSelf manageMSLogout];
                break;
        }
    }];
}

- (void)manageMSLogout
{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Logout", nil) message:NSLocalizedString(@"Logout from Sharepoint? The next QR content from Sharepoint will require login.", nil) preferredStyle:UIAlertControllerStyleAlert];
    
    [alert addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"Yes", nil) style:UIAlertActionStyleDestructive handler:^(UIAlertAction *action){
        [alert dismissViewControllerAnimated:YES completion:nil];
        [NetworkManager removeAuthTokenForMicrosoftGraph];
        [PreferencesManager saveMSLastUserID:nil];
        [self setupDropDownMenu];
    }]];
    
    [alert addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"No", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
        [alert dismissViewControllerAnimated:YES completion:nil];
    }]];
    
    [self presentViewController:alert animated:YES completion:nil];
}

#pragma mark ## Button Actions ##

- (void)openController:(TypeOfOpening)controller
{
    NSLog(@"%@", [NSString stringWithFormat:@"Opening Controller %@", controller == TYPE_LEARNING_STORE ? @"Learning" : (controller == TYPE_TUTORIAL ? @"Tutorial" : @"History")]);
    
    switch (controller) {
        case TYPE_TUTORIAL:
            [self performSegueWithIdentifier:TUTORIAL_SEGUE sender:[NSNumber numberWithBool:NO]];
            break;
        case TYPE_HISTORY:
            [self performSegueWithIdentifier:HISTORY_SEGUE sender:self];
            break;
        case TYPE_LEARNING_STORE:
            [self performSegueWithIdentifier:LEARNING_STORE_SEGUE sender:self];
            break;
        default:
            NSLog(@"%@", [NSString stringWithFormat:@"unrecognized controller requested %d", controller]);
            break;
    }
}

- (void)swapToQRView:(id)sender
{
    _isInQRView = YES;
    self.navigationItem.titleView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"title_logo"]];
    [self setupChangeViewForQR:YES animated:YES];
}

- (void)swapToListView:(id)sender
{
    _isInQRView = NO;
    self.navigationItem.titleView = nil;
    [self setTitle:NSLocalizedString(@"Downloads", nil)];
    [self.navigationController.navigationBar setTitleTextAttributes:@{NSFontAttributeName:[UIFont fontWithName:@"Nokia Pure Headline" size:18.0f],
                                                                      NSForegroundColorAttributeName:[UIColor colorWithRed:0.29 green:0.32 blue:0.32 alpha:1.0]
                                                                      }];
    [self setupChangeViewForQR:NO animated:YES];
    
}

- (void)openDropDown:(id)sender
{
    if(_dropDown.isHidden)
        [_dropDown show];
    else
        [_dropDown hide];
}

#pragma mark ## QR Reading ##

-(void)captureOutput:(AVCaptureOutput *)captureOutput didOutputMetadataObjects:(NSArray *)metadataObjects fromConnection:(AVCaptureConnection *)connection
{
    if(_isInQRView){
        if (metadataObjects != nil && [metadataObjects count] > 0) {
            AVMetadataMachineReadableCodeObject *metadataObj = [metadataObjects objectAtIndex:0];
            if ([[metadataObj type] isEqualToString:AVMetadataObjectTypeQRCode]) {
                if([Utilities stringIsAValidURL:[metadataObj stringValue]] ){
                    if(_objectsListViewController != nil)
                        [_objectsListViewController manageURLString:[metadataObj stringValue]];
                }
            }
        }
    }
}

#pragma mark ## Rotation Method ##

- (BOOL)canRotate
{
    return YES;
}

- (BOOL)shouldAutorotate
{
    return YES;
}

- (void)viewWillTransitionToSize:(CGSize)size withTransitionCoordinator:(id<UIViewControllerTransitionCoordinator>)coordinator
{
    [coordinator animateAlongsideTransition:^(id<UIViewControllerTransitionCoordinatorContext>  _Nonnull context) {
        // change any properties on your views
    } completion:^(id<UIViewControllerTransitionCoordinatorContext>  _Nonnull context) {
        _orientation = [UIDevice currentDevice].orientation;
        [self setupQRView];
        [self setRectOfInterest];
        [_videoPreviewLayer.connection setVideoOrientation:[self videoOrientationFromCurrentDeviceOrientation]];
        [self setupView];
    }];
}


@end

