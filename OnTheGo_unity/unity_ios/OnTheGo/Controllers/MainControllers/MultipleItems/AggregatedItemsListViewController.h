//
//  AggregatedItemsListViewController.h
//  Unity-iPhone
//
//  Created by Alessio Crestani on 17/07/2018.
//

#import <UIKit/UIKit.h>
#import "ObjectListViewController.h"

@interface AggregatedItemsListViewController : UIViewController

@property (weak, nonatomic) ObjectListViewController *objectListController;
@property (nonatomic) NSString *parentManagingUUID;

@end
