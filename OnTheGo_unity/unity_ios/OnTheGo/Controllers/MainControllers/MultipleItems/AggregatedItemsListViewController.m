//
//  AggregatedItemsListViewController.m
//  Unity-iPhone
//
//  Created by Alessio Crestani on 17/07/2018.
//

#import "AggregatedItemsListViewController.h"

@interface AggregatedItemsListViewController ()

@end

@implementation AggregatedItemsListViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self downloadChildrenThumbnails];
    [self setupView];
}

- (void)setupView
{
    DownloadedObject *obj = [[RealmManager sharedInstance] getDownloadedObjWithUUID:_parentManagingUUID];
    [self setTitle:obj.title];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if([[segue destinationViewController] class] == [ObjectListViewController class]){
        [(ObjectListViewController *)[segue destinationViewController] setParentManagingUUID:_parentManagingUUID];
        _objectListController = (ObjectListViewController *)[segue destinationViewController];
    }
}

//for every child of the aggregated user, this method will download
- (void)downloadChildrenThumbnails
{
    NSMutableArray *thumbToDownload = [NSMutableArray new];
    RLMResults<DownloadedObject *> *children = [[RealmManager sharedInstance] getChildrenItemsForParentUUID:_parentManagingUUID];
    for(DownloadedObject *child in children){
        if(![FileManager thumbDownloadedForObj:child] && ([child thumbURL] != nil) && ![[child thumbURL] isEqualToString:@""]){
            [thumbToDownload addObject:child];
        }
    }
    
    if(thumbToDownload.count > 0){
        [SVProgressHUD showWithStatus:NSLocalizedString(@"Downloading thumbnials...", nil)];
        [self downloadRemainingThumbsForList:thumbToDownload];
    }
    
}

- (void)downloadRemainingThumbsForList:(NSMutableArray *)remainingThumbs
{
    if(remainingThumbs.count == 0){
        [self endDownloadThumb];
    } else {
        DownloadedObject *downloadedObj = remainingThumbs.firstObject;
        [NetworkManager downloadDataFromURL:[NSURL URLWithString:[downloadedObj thumbURL]] withFilePath:[downloadedObj getObjThumb] withFileType:IMAGE_OBJ withProgress:^(NSProgress * _Nonnull downloadProgress) {} withCompletion:^(NSURLResponse *response, NSURL *filePath, NSError *error) {
            if(error == nil)
                NSLog(@"Thumb Downloaded. Proceeding", nil);
             else
                NSLog([NSString stringWithFormat:@"Thumb Download error. Error:%@ Continue....", [error localizedDescription]], nil);
            
            [remainingThumbs removeObject:downloadedObj];
            [self downloadRemainingThumbsForList:remainingThumbs];
        }];
    }
}



- (void)endDownloadThumb
{
    [SVProgressHUD dismiss];
    [_objectListController.objectsTableView reloadData];
}

@end
