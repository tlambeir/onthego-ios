//
//  SlideshowController.h
//  OnTheGo
//  Created by Alessio Crestani on 01/02/17.
//  Copyright (c) 2015 OnTheGo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TutorialViewController: UIViewController <UIScrollViewDelegate>

@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIView *bottomControl;
@property (weak, nonatomic) IBOutlet UIPageControl *pageControl;
@property (weak, nonatomic) IBOutlet UIImageView *sampleImageView;
@property (weak, nonatomic) IBOutlet UILabel *sampleDescriptionLabel;

@property (weak, nonatomic) IBOutlet UIButton *closeTutorial;
@property (nonatomic, strong) NSMutableArray *images;
@property (nonatomic, strong) NSMutableArray *titles;
@property (nonatomic, strong) NSMutableArray *descriptions;
@property (nonatomic) BOOL isFirstStart;

@end
