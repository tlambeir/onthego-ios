//
//  TutorialController.m
//  OnTheGo
//
//  Created by Alessio Crestani on 17/11/15.
//  Copyright (c) 2015 D-Heart. All rights reserved.
//

#import "TutorialViewController.h"
#import "GlobalImport.h"

#define NUM_OF_PAGES 5

@implementation TutorialViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    _images = [NSMutableArray new];
    _descriptions = [NSMutableArray new];
    _titles = [NSMutableArray new];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
  //  [[UIDevice currentDevice] setValue:@(UIInterfaceOrientationPortrait) forKey:@"orientation"];
    [self setupView];
}

//- (UIInterfaceOrientationMask)supportedInterfaceOrientations
//{
//    return UIInterfaceOrientationMaskPortrait;
//}

- (BOOL)shouldAutorotate
{
    return YES;
}

- (BOOL)canRotate
{
    return YES;
}

- (void)setupView
{
    [self.view setBackgroundColor:[UIColor colorWithRed:234.0f/255.0f green:238.0f/255.0f blue:239.0f/255.0f alpha:1.0f]];
    [_scrollView setBackgroundColor:[UIColor clearColor]];
    
    if(_isFirstStart){
        [_closeTutorial setHidden:NO];
        self.navigationItem.hidesBackButton = YES;
    } else{
        [self setTitle:NSLocalizedString(@"Tutorial", nil)];
        [self.navigationController.navigationBar setTitleTextAttributes:@{NSFontAttributeName:[UIFont fontWithName:@"Nokia Pure Headline" size:18.0f],
                                                                          NSForegroundColorAttributeName:[UIColor colorWithRed:0.29 green:0.32 blue:0.32 alpha:1.0]
                                                                          }];
        [_closeTutorial setHidden:YES];
        self.navigationItem.hidesBackButton = NO;
    }
    
    for (int i = 1; i <= NUM_OF_PAGES; i++){
        NSString *infoTitleString = [NSString stringWithFormat:@"info_%d_title", i];
        NSString *infoDescriptionString = [NSString stringWithFormat:@"info_%d_description", i];
        NSString *infoDataString = [NSString stringWithFormat:@"info_%d", i];
        [_images addObject:[UIImage imageNamed:infoDataString]];
        [_titles addObject:NSLocalizedString(infoTitleString, nil)];
        [_descriptions addObject:NSLocalizedString(infoDescriptionString, nil)];
    }
    
    
    _scrollView.pagingEnabled = YES;
    _scrollView.scrollsToTop = NO;
    _scrollView.delegate = self;
    
    _pageControl.numberOfPages = [_images count];
    _pageControl.currentPage = 0;
    
    [self.view bringSubviewToFront:_pageControl];
    
    [self setupScrollViewWithSize:self.view.frame.size];
    [_closeTutorial setTitle:NSLocalizedString(@"Finish", nil) forState:UIControlStateNormal];
    [_closeTutorial.titleLabel setFont:[UIFont fontWithName:@"Nokia Pure Headline" size:_closeTutorial.titleLabel.font.pointSize]];
}

- (void)setupScrollViewWithSize:(CGSize)size
{
    NSLog(@"setup with width:%f, height:%f", size.width, size.height);
    _scrollView.contentSize = CGSizeMake(size.width * (_pageControl.numberOfPages), size.height);
    self.automaticallyAdjustsScrollViewInsets = NO;

    [[_scrollView subviews] makeObjectsPerformSelector:@selector(removeFromSuperview)];
    [_scrollView setFrame:CGRectMake(0, 0, size.width, size.height)];


    for (UIView *i in _images) {
        
        UIImageView *imageView;
        UILabel *description;

        
        if(size.height > size.width){ //PORTRAIT
            
            float imageViewStartingPointY = (size.height / 100) * 20;
            float imageViewStartingPointX = (size.width / 100) * 15;
            float imageViewSizeWidth = (size.width / 100) * 70;
            float imageViewHeight = (size.height / 100) * 40;

            imageView = [[UIImageView alloc] initWithFrame:CGRectMake((imageViewStartingPointX + (size.width * [_images indexOfObject:i])), imageViewStartingPointY, imageViewSizeWidth , imageViewHeight)];
            
            float descriptionLabelStartingPointY = imageViewStartingPointY + imageView.frame.size.height;

            
            description = [[UILabel alloc] initWithFrame:CGRectMake((20 + (size.width) * [_images indexOfObject:i]), descriptionLabelStartingPointY, size.width - 40, ((size.height / 100) * 30))];
        } else { //LANDSCAPE
            
            float imageViewStartingPointY = (size.height / 100) * 20;
            float imageViewStartingPointX = (size.width / 100) * 25;
            float imageViewSizeWidth = (size.width / 100) * 50;
            float imageViewHeight = (size.height / 100) * 40;
            
            imageView = [[UIImageView alloc] initWithFrame:CGRectMake((imageViewStartingPointX + (size.width * [_images indexOfObject:i])), imageViewStartingPointY, imageViewSizeWidth , imageViewHeight)];

            float descriptionLabelStartingPointY = imageViewStartingPointY + imageView.frame.size.height;
            
            description = [[UILabel alloc] initWithFrame:CGRectMake((20 + (size.width) * [_images indexOfObject:i]), descriptionLabelStartingPointY, size.width - 40, ((size.height / 100) * 30))];
            
        }
        
        [imageView setImage:(UIImage *)i];
        [imageView setContentMode:UIViewContentModeScaleAspectFit];
        
       
        NSMutableAttributedString *descriptionString = [[NSMutableAttributedString alloc] initWithString:[[[_titles objectAtIndex:[_images indexOfObject:i]] stringByAppendingString:@"\n"] stringByAppendingString:[_descriptions objectAtIndex:[_images indexOfObject:i]]]];
        [descriptionString addAttribute:NSFontAttributeName
                      value:[UIFont fontWithName:@"Nokia Pure Headline" size:21.0f]
                      range:NSMakeRange(0, [[_titles objectAtIndex:[_images indexOfObject:i]] length])];
        [descriptionString addAttribute:NSFontAttributeName
                                  value:[UIFont fontWithName:@"Nokia Pure Headline" size:15.0f]
                                  range:NSMakeRange([[_titles objectAtIndex:[_images indexOfObject:i]] length], [[_descriptions objectAtIndex:[_images indexOfObject:i]] length])];
        
        [description setAttributedText:descriptionString];
        [description setNumberOfLines:0];
        [description setTextAlignment:NSTextAlignmentCenter];
        [description setTextColor:[UIColor colorWithRed:0.00 green:0.07 blue:0.21 alpha:1.0]];
        
        [_scrollView addSubview:imageView];
        [_scrollView addSubview:description];
    }
    
    [_sampleDescriptionLabel setHidden:NO];
    [_sampleDescriptionLabel setHidden:NO];
    
    CGRect frame = _scrollView.frame;
    frame.origin.x = frame.size.width * _pageControl.currentPage;
    frame.origin.y = 0;
    [_scrollView scrollRectToVisible:frame animated:YES];
}

- (void)viewWillTransitionToSize:(CGSize)size withTransitionCoordinator:(id<UIViewControllerTransitionCoordinator>)coordinator
{
    [self setupScrollViewWithSize:size];
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    CGFloat pageWidth = scrollView.frame.size.width;
    int page = floor((scrollView.contentOffset.x - pageWidth / 2) / pageWidth) + 1;
    if(page <= _pageControl.numberOfPages){
        [_pageControl setCurrentPage:page];
    } else {
        float maximumHorizontalOffset = scrollView.contentSize.width - scrollView.frame.size.width;
        float currentHorizontalOffset = scrollView.contentOffset.x;
        float percentageHorizontalOffset = currentHorizontalOffset / maximumHorizontalOffset;
        if(percentageHorizontalOffset > (1.0f - (1.0f / (_pageControl.numberOfPages - 1)))){
            percentageHorizontalOffset = (percentageHorizontalOffset - (1.0f - (1.0f / (_pageControl.numberOfPages - 1)))) * (_pageControl.numberOfPages);
            [[self view] setAlpha:1 - percentageHorizontalOffset];
        }
    }
    
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    CGFloat pageWidth = scrollView.frame.size.width;
    int page = floor((scrollView.contentOffset.x - pageWidth / 2) / pageWidth) + 1;
    if(page == _pageControl.numberOfPages)
        [self closeSlideShow];
}

- (void)closeSlideShow
{
    [PreferencesManager setIntroSeen];
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - Buttons Actions

- (IBAction)skipToMain:(id)sender
{
    [self closeSlideShow];
}

@end
