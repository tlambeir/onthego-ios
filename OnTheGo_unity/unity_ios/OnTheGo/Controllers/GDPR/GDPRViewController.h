//
//  GDPRViewController.h
//  Unity-iPhone
//
//  Created by Alessio Crestani on 17/07/2018.
//

#import <UIKit/UIKit.h>


@interface GDPRViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIScrollView *agreementScrollView;
@property (weak, nonatomic) IBOutlet UILabel *agreementTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *agreementSwitchLabel;
@property (weak, nonatomic) IBOutlet UISwitch *agreementSwitch;
@property (weak, nonatomic) IBOutlet UITextView *agreementDescriptionTextView;
@property (weak, nonatomic) IBOutlet UITextView *GDPRLinkableTextView;
@property (weak, nonatomic) IBOutlet UIButton *GDPRAgreeButton;

@end
