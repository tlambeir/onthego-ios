//
//  GDPRViewController.m
//  Unity-iPhone
//
//  Created by Alessio Crestani on 17/07/2018.
//

#import "GDPRViewController.h"
#import "SVProgressHUD.h"
#import "Const.h"
#import "PreferencesManager.h"
#import "Colors.h"
#import <QuartzCore/QuartzCore.h>

@interface GDPRViewController ()

@end

@implementation GDPRViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    [SVProgressHUD showWithStatus:NSLocalizedString(@"Please wait", nil)];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [self localizePage];
    [self setupView];
    [SVProgressHUD dismiss];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}

- (void)setupView
{
    [_GDPRAgreeButton setTitle:NSLocalizedString(@"I Agree", nil) forState:UIControlStateNormal];
    
    [self setupButtonForSwitch:[_agreementSwitch isOn]];
    [_agreementScrollView scrollsToTop];
    
    CGFloat height = 0;
    CGSize size = [_agreementDescriptionTextView sizeThatFits:CGSizeMake(_agreementDescriptionTextView.frame.size.width, CGFLOAT_MAX)];
    CGRect frame = _agreementDescriptionTextView.frame;
    frame.size.height = size.height;
    _agreementDescriptionTextView.frame = frame;
    
    [_GDPRLinkableTextView setFrame:CGRectMake(_agreementDescriptionTextView.frame.origin.x, _agreementDescriptionTextView.frame.origin.y + _agreementDescriptionTextView.frame.size.height + 25, _GDPRLinkableTextView.frame.size.width, _GDPRLinkableTextView.frame.size.height)];
    
    [_agreementSwitchLabel setNumberOfLines:0];
    CGRect frameContent = _agreementSwitchLabel.frame;
    [_agreementSwitchLabel sizeToFit];
    frameContent.size.height = _agreementSwitchLabel.frame.size.height;
    _agreementSwitchLabel.frame = frameContent;
    [_agreementSwitchLabel setContentMode:UIViewContentModeScaleToFill];
    
    
    
    [_agreementSwitch setFrame:CGRectMake(_agreementSwitch.frame.origin.x, _GDPRLinkableTextView.frame.size.height + _GDPRLinkableTextView.frame.origin.y + 25, _agreementDescriptionTextView.frame.size.width, _agreementSwitch.frame.size.height)];
    [_agreementSwitchLabel setFrame:CGRectMake(_agreementSwitchLabel.frame.origin.x, _GDPRLinkableTextView.frame.size.height + _GDPRLinkableTextView.frame.origin.y + 25, _agreementSwitchLabel.frame.size.width, _agreementSwitchLabel.frame.size.height)];
    [_agreementSwitchLabel sizeToFit];
    
    height = _GDPRLinkableTextView.frame.size.height + _agreementSwitchLabel.frame.size.height + _agreementTitleLabel.frame.origin.y + _agreementDescriptionTextView.frame.size.height + _GDPRLinkableTextView.frame.size.height +150;
    [_agreementScrollView setContentSize:CGSizeMake(_agreementScrollView.frame.size.width, height)];
    
    
    
    [_agreementDescriptionTextView setScrollEnabled:NO];
    
}

- (void)localizePage
{
    [self setTitle:NSLocalizedString(@"Pricavy Statement", nil)];
    [_agreementTitleLabel setText:NSLocalizedString(@"Privacy Statement", nil)];
    [_agreementDescriptionTextView setText:NSLocalizedString(@"Nokia Learning on the GO uses QR codes to improve quick access to dedicated learning content. It supports 3D models, videos, 360 videos, HTML and PDF. To make this happen, the app requires access to your devices storage, internet access and camera.", nil)];
    
    [_agreementSwitchLabel setText:NSLocalizedString(@"I have read and agreed to the Terms and the privacy statement.", nil)];
    [_GDPRAgreeButton setTitle:NSLocalizedString(@"I Agree", nil) forState:UIControlStateNormal];
    [_GDPRLinkableTextView setText:NSLocalizedString(@"Please read the Terms and the Privacy Statement", nil)];
    //[_GDPRLinkableLabel linkSubstring:@"Terms" withURL:TERMS_URL];
    [self putLink:[NSURL URLWithString:TERMS_URL] onTextView:_GDPRLinkableTextView atSubstring:@"Terms"];
    [self putLink:[NSURL URLWithString:PRIVACY_URL] onTextView:_GDPRLinkableTextView atSubstring:@"Privacy Statement"];

    [_GDPRLinkableTextView setUserInteractionEnabled:YES];

}

- (void)putLink:(NSURL *)link onTextView:(UITextView *)textView atSubstring:(NSString *)subString
{
    NSRange range = [textView.text rangeOfString:subString];
    NSMutableAttributedString *attribued = textView.attributedText != nil ? [textView.attributedText mutableCopy] : [[NSMutableAttributedString alloc] initWithString:textView.text];
    [attribued addAttribute:NSLinkAttributeName value:link range:range];
    [textView setAttributedText:attribued];
}

#pragma mark - Buttons Actions



- (IBAction)closeAgreementAction:(id)sender
{
    if([_agreementSwitch isOn]){
        [PreferencesManager setGDPRAccepted];
        [self dismissViewControllerAnimated:YES completion:nil];
    } else
        [SVProgressHUD showErrorWithStatus:NSLocalizedString(@"You have to accept Terms in order to proceed", nil)];
}

- (IBAction)agreementSwitchChangesValue:(id)sender
{
    [self setupButtonForSwitch:[_agreementSwitch isOn]];
}

- (void)setupButtonForSwitch:(BOOL)switchON
{
    if(switchON){
        _GDPRAgreeButton.layer.borderWidth = 0;
        [_GDPRAgreeButton setBackgroundColor:[Colors blueNokiaColor]];
        [_GDPRAgreeButton.titleLabel setTextColor:[UIColor whiteColor]];
        
    } else {
        _GDPRAgreeButton.layer.borderWidth = 2.0f;
        _GDPRAgreeButton.layer.borderColor = [Colors blueNokiaColor].CGColor;
        [_GDPRAgreeButton setBackgroundColor:[UIColor whiteColor]];
        [_GDPRAgreeButton.titleLabel setTextColor:[Colors blueNokiaColor]];
    }
    
}


@end
