//
//  OnTheGoDelegate.m
//  OnTheGo
//
//  Created by Alessio Crestani on 16/11/2017.
//  Copyright © 2017 Nokia. All rights reserved.
//

#import "OnTheGoDelegate.h"
#import "VideoPlayerViewController.h"
#import <AVFoundation/AVFoundation.h>
#import <AVKit/AVKit.h>
#import "VRARViewController.h"

@import UIKit;
@import Firebase;

@implementation OnTheGoDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    [FIRApp configure];
    //[Utilities cleanApplicationFromObjectsDEBUG];
    // Override point for customization after application launch.
    _application = application;
    [self setStatusBarBackgroundColor:[UIColor colorWithRed:0.85 green:0.91 blue:0.93 alpha:1.0]];
    unity_init(0, nil);
    _currentUnityController = [UnityAppController new];
    [_currentUnityController application:_application didFinishLaunchingWithOptions:launchOptions];
//    [self startUnity];
//    [self stopUnity];

    return YES;
}


- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    if(_isUnityRunning)
        [_currentUnityController applicationWillResignActive:_application];
}


- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    if(_isUnityRunning)
        [_currentUnityController applicationDidEnterBackground:_application];
}


- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    if(_isUnityRunning)
        [_currentUnityController applicationWillEnterForeground:_application];
}


- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    if(_isUnityRunning)
        [_currentUnityController applicationDidBecomeActive:_application];
}


- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    if(_isUnityRunning)
        [_currentUnityController applicationWillTerminate:_application];
}

- (void)startUnity
{
    if(!_isUnityRunning){
        _isUnityRunning = YES;
        [_currentUnityController applicationDidBecomeActive:_application];
    }
}

- (void)stopUnity
{
    if(_isUnityRunning){
        NSLog(@"NOKIA_ONTHEGO SHUTTING DOWN UNITY");
        [_currentUnityController applicationWillResignActive:_application];
        _isUnityRunning = NO;
    }
}

#pragma mark - ## Change Status Bar Color ##

- (void)setStatusBarBackgroundColor:(UIColor *)color
{
    
    UIView *statusBar = [[[UIApplication sharedApplication] valueForKey:@"statusBarWindow"] valueForKey:@"statusBar"];
    
    if ([statusBar respondsToSelector:@selector(setBackgroundColor:)]) {
        statusBar.backgroundColor = color;
    }
}

#pragma mark - ## rotation of single controller

/* Allow Landscape mode for specific ViewControllers */
-(UIInterfaceOrientationMask)application:(UIApplication *)application supportedInterfaceOrientationsForWindow:(UIWindow *)window {
    UIViewController* topVC = [self topViewControllerWith: self.window.rootViewController];
    if ([topVC respondsToSelector:@selector(canRotate)] || [topVC isKindOfClass:[VideoPlayerViewController class]] || [topVC isKindOfClass:[AVPlayerViewController class]]) {
        return UIInterfaceOrientationMaskAllButUpsideDown;
    }
    
    return UIInterfaceOrientationMaskPortrait;
}

/* get the top ViewController */
- (UIViewController*) topViewControllerWith:(UIViewController *)rootViewController {
    if (rootViewController == nil) { return nil; }
    if ([rootViewController isKindOfClass: [UITabBarController class]]) {
        return [self topViewControllerWith: ((UITabBarController*) rootViewController).selectedViewController];
    }
    else if ([rootViewController isKindOfClass: [UINavigationController class]]) {
        return [self topViewControllerWith: ((UINavigationController*) rootViewController).visibleViewController];
    }
    else if (rootViewController.presentedViewController != nil) {
        return [self topViewControllerWith: [rootViewController presentedViewController]];
    }
    return rootViewController;
}


@end
