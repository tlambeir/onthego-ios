//
//  OnTheGoDelegate.h
//  OnTheGo
//
//  Created by Alessio Crestani on 16/11/2017.
//  Copyright © 2017 Nokia. All rights reserved.
//


#import "DropDown-Swift.h"
#import "UnityAppController+UnityInterface.h"
#import "UnityUtils.h"

@interface OnTheGoDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (nonatomic, strong) UnityAppController *currentUnityController;
@property (nonatomic) BOOL isUnityRunning;
@property (nonatomic, strong) UIApplication *application;
@property UIInterfaceOrientationMask lockedOrientation;

- (void)startUnity;
- (void)stopUnity;

@end
