 //
//  main.m
//  OnTheGo
//
//  Created by Alessio Crestani on 16/11/2017.
//  Copyright © 2017 Nokia. All rights reserved.
//


#import "OnTheGoDelegate.h"

int main(int argc, char * argv[]) {
    
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([OnTheGoDelegate class]));
    }
}
