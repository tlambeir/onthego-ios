
//
//  RealmManager.m
//  OnTheGo
//
//  Created by Alessio Crestani on 16/11/2017.
//  Copyright © 2017 Nokia. All rights reserved.
//

#import "RealmManager.h"

static RealmManager *sharedInstance = nil;

@implementation RealmManager

#pragma mark ## Realm Base methods ##

+ (RealmManager *)sharedInstance
{
    if (sharedInstance == nil)
        sharedInstance = [super new];
    
    if([sharedInstance realm] == nil)
        [RealmManager initializeRealm];
    
    return sharedInstance;
}

+ (void)initializeRealm
{
    NSError *err;
    RLMRealm *realm = [RLMRealm realmWithConfiguration:[RealmManager getRealmConfiguration] error:&err];
    if(err == nil && realm != nil)
        [sharedInstance setRealm:realm];
}

+ (RLMRealmConfiguration *)getRealmConfiguration
{
    // Open the encrypted Realm file
    RLMRealmConfiguration *config = [RLMRealmConfiguration defaultConfiguration];
    config.objectClasses = @[DownloadedObject.class, StructureObject.class, StepStructure.class];
    
    config.schemaVersion = 5;
    config.migrationBlock = ^(RLMMigration *migration, uint64_t oldSchemaVersion) {
        // We haven’t migrated anything yet, so oldSchemaVersion == 0
        if (oldSchemaVersion < 1) {
            // Nothing to do!
            // Realm will automatically detect new properties and removed properties
            // And will update the schema on disk automatically
        }
    };
    
    return config;
}

#pragma mark ## DownloadedObject management

- (DownloadedObject *)createDownloadedObjectWithData:(NSDictionary *)downloadedObjectDictionary
{
    DownloadedObject *newDownloaded;
    NSString *uuid;
    if([[downloadedObjectDictionary allKeys] containsObject:OBJ_PARENT_UUID_JSON])
        uuid = [[NSUUID UUID] UUIDString];
    else if([[downloadedObjectDictionary allKeys] containsObject:OBJ_UUID_JSON])
        uuid = [NSString stringWithFormat:@"%@", [downloadedObjectDictionary objectForKey:OBJ_UUID_JSON]];
    else if([[downloadedObjectDictionary allKeys] containsObject:OBJ_UUID_QR_GENERATOR_JSON])
        uuid = [NSString stringWithFormat:@"%@", [downloadedObjectDictionary objectForKey:OBJ_UUID_QR_GENERATOR_JSON]];
    else
        uuid = [[NSUUID UUID] UUIDString];
    
    if([[RealmManager sharedInstance] getDownloadedObjWithUUID:uuid] == nil){
        NSLog(@"requested object is not present on Realm. Creating a new one...");
        [_realm beginWriteTransaction];
        
        //Base Object
        newDownloaded = [DownloadedObject new];
        [newDownloaded setUuid:uuid];
        if([[downloadedObjectDictionary allKeys] containsObject:OBJ_PARENT_UUID_JSON])
            [newDownloaded setUuidParent:[NSString stringWithFormat:@"%@", [downloadedObjectDictionary objectForKey:OBJ_PARENT_UUID_JSON]]];
        [newDownloaded setTitle:[downloadedObjectDictionary objectForKey:OBJ_TITLE_JSON]];
        [newDownloaded setObjDescription:[downloadedObjectDictionary objectForKey:OBJ_DESCRIPTION_JSON]];
        [newDownloaded setFileSize:[downloadedObjectDictionary objectForKey:OBJ_FILE_SIZE_JSON]];
        [newDownloaded setThumbURL:[downloadedObjectDictionary objectForKey:OBJ_THUMB_URL_JSON]];
        [newDownloaded setAppType:[downloadedObjectDictionary objectForKey:OBJ_APP_TYPE_JSON]];
        [newDownloaded setModel:[downloadedObjectDictionary objectForKey:OBJ_APP_MODEL_JSON]];
        [newDownloaded setCreationDate:[NSDate new]];
        [newDownloaded setLastOpenedDate:[NSDate new]];
        [newDownloaded setStorageType:[downloadedObjectDictionary objectForKey:OBJ_STORAGE_TYPE_JSON]];
        [newDownloaded setType:[NSNumber numberWithInt:[Utilities getTypeFromString:[downloadedObjectDictionary objectForKey:OBJ_TYPE_JSON] withStorageType:[downloadedObjectDictionary objectForKey:OBJ_STORAGE_TYPE_JSON]]]];
        [newDownloaded setObjURL:[downloadedObjectDictionary objectForKey:OBJ_URL_JSON]];
        if((TypeOfile)[[newDownloaded type] intValue] == WEB_OBJ){
            [newDownloaded setObjURL:[newDownloaded.objURL stringByAppendingString:ZIP_URL_PATH_COMPONENT]];
        } else if((TypeOfile)[[newDownloaded type] intValue] == AR_3D_OBJ){
            [newDownloaded setObjURL:[newDownloaded.objURL stringByAppendingString:IOS_MODEL_PATH_COMPONENT]];
        }
        if([[downloadedObjectDictionary allKeys] containsObject:OBJ_STRUCTURE_JSON]){
            NSMutableArray *structures = [NSMutableArray new];
            for(NSDictionary *structDict in [downloadedObjectDictionary mutableArrayValueForKey:OBJ_STRUCTURE_JSON]){
                StructureObject *structureObj = [[RealmManager sharedInstance] createStructuredObjectWithData:structDict];
                [structures addObject:structureObj];
            }
            [newDownloaded setStructures:(RLMArray<StructureObject *><StructureObject> *)structures];
        }
        
        NSError * err;
        NSData * jsonData = [NSJSONSerialization dataWithJSONObject:downloadedObjectDictionary options:0 error:&err];
        NSString * jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        [newDownloaded setJSONObj:jsonString];
        
        [_realm addOrUpdateObject:newDownloaded];
        [_realm commitWriteTransaction];
        
        if([[downloadedObjectDictionary allKeys] containsObject:OBJ_AGGREGATED_CHILDER_JSON]){
            NSMutableArray *children = [downloadedObjectDictionary objectForKey:OBJ_AGGREGATED_CHILDER_JSON];
            for(NSDictionary *child in children){
                [self createDownloadedObjectWithData:child];
            }
        }
        
    } else {
        NSLog(@"requested object is present on Realm. Not downloading It");
        newDownloaded = [[RealmManager sharedInstance] getDownloadedObjWithUUID:uuid];
        [_realm beginWriteTransaction];
        [newDownloaded setIsHistory:[NSNumber numberWithBool:YES]];
        [newDownloaded setIsDownload:[NSNumber numberWithBool:YES]];
        [_realm addOrUpdateObject:newDownloaded];
        [_realm commitWriteTransaction];

    }
    
    return newDownloaded;
}

- (DownloadedObject *)objectAlreadySavedWithDictionary:(NSDictionary *)objDict
{
    NSString *uuid;
    if([[objDict allKeys] containsObject:OBJ_PARENT_UUID_JSON])
        uuid = [[NSUUID UUID] UUIDString];
    else if([[objDict allKeys] containsObject:OBJ_UUID_JSON])
        uuid = [NSString stringWithFormat:@"%@", [objDict objectForKey:OBJ_UUID_JSON]];
    else if([[objDict allKeys] containsObject:OBJ_UUID_QR_GENERATOR_JSON])
        uuid = [NSString stringWithFormat:@"%@", [objDict objectForKey:OBJ_UUID_QR_GENERATOR_JSON]];
    else
        uuid = [[NSUUID UUID] UUIDString];
    
    return [[RealmManager sharedInstance] getDownloadedObjWithUUID:uuid];
}

- (DownloadedObject *)changeURLOfWebObject:(DownloadedObject *)downloadedObject
{
    [_realm beginWriteTransaction];
    [downloadedObject setObjURL:[[downloadedObject objURL] stringByReplacingOccurrencesOfString:ZIP_URL_PATH_COMPONENT withString:@""]];
    [_realm addOrUpdateObject:downloadedObject];
    [_realm commitWriteTransaction];
    return downloadedObject;
}

- (DownloadedObject *)getDownloadedObjWithUUID:(NSString *)objUUID
{
    return [[DownloadedObject objectsInRealm:_realm where:@"uuid = %@", objUUID] firstObject];

}

- (RLMResults<DownloadedObject *> *)allObjects
{
    return [[DownloadedObject allObjectsInRealm:_realm] sortedResultsUsingKeyPath:@"creationDate" ascending:NO];
}

- (RLMResults<DownloadedObject *> *)historyObjects
{
    return [[DownloadedObject objectsWhere:@"isHistory = %@ && uuidParent == nil", [NSNumber numberWithBool:YES]] sortedResultsUsingKeyPath:@"lastOpenedDate" ascending:NO];
}

- (RLMResults<DownloadedObject *> *)downloadedObjects
{
      return [[DownloadedObject objectsWhere:@"isDownload = %@ && uuidParent == nil", [NSNumber numberWithBool:YES]] sortedResultsUsingKeyPath:@"creationDate" ascending:NO];
}

- (RLMResults<DownloadedObject *> *)getChildrenItemsForParentUUID:(NSString *)parentUUID
{
    // Query using an NSPredicate
    NSPredicate *pred = [NSPredicate predicateWithFormat:@"isDownload = %@ AND uuidParent == %@",
                         [NSNumber numberWithBool:YES], parentUUID];
    
    return [[DownloadedObject objectsWithPredicate:pred] sortedResultsUsingKeyPath:@"creationDate" ascending:NO];
}

- (void)removeDownloadedObjectForHistory:(DownloadedObject *)downloadedObj
{
    [_realm beginWriteTransaction];
        [downloadedObj setIsHistory:[NSNumber numberWithBool:NO]];
    [_realm commitWriteTransaction];
    
    if(![downloadedObj isDownload])
        [self removeDownloadedObjectFromRealm:downloadedObj withLocalFileDeletion:NO];
}

- (void)removeDownloadedObjectForDownload:(DownloadedObject *)downloadedObj
{
    [_realm beginWriteTransaction];
    [downloadedObj setIsDownload:[NSNumber numberWithBool:NO]];
    [_realm commitWriteTransaction];
    
    [[RealmManager sharedInstance] removeDownloadedObjectFromLocalDisk:downloadedObj];
    if(![downloadedObj isHistory])
        [self removeDownloadedObjectFromRealm:downloadedObj withLocalFileDeletion:NO];
}

- (BOOL)removeDownloadedObjectFromRealm:(DownloadedObject *)downloadedObj withLocalFileDeletion:(BOOL)localFileDeletion
{
    if(localFileDeletion)
        [self removeDownloadedObjectFromLocalDisk:downloadedObj];
    if([[downloadedObj type] intValue] == AGGREGATED_OBJ){
        RLMResults<DownloadedObject *> *children = [[RealmManager sharedInstance] getChildrenItemsForParentUUID:downloadedObj.uuid];
        for(DownloadedObject *child in children){
            [self removeDownloadedObjectFromRealm:child withLocalFileDeletion:localFileDeletion];
        }
    }
    
    NSError *errThumb;
    [[NSFileManager defaultManager] removeItemAtPath:[[downloadedObj getObjThumb] path] error:&errThumb];

    [_realm beginWriteTransaction];
    for(StructureObject *structChild in [downloadedObj structures]){
        [[RealmManager sharedInstance] removeStructureObject:structChild];
    }
    [_realm deleteObject:downloadedObj];
    [_realm commitWriteTransaction];
    NSLog(@"Object deleted.", nil);
    return YES;
}

- (BOOL)removeDownloadedObjectFromLocalDisk:(DownloadedObject *)downloadedObj
{
    NSError *errFile;
    [[NSFileManager defaultManager] removeItemAtPath:[[downloadedObj getObjFilePath] path] error:&errFile];
    return YES;
}

#pragma mark ## Structure Object management

- (void)removeStructureObject:(StructureObject *)structureObj
{
    for(StepStructure *step in [structureObj stepStructures]){
        [_realm deleteObject:step];
    }
    
    for(StructureObject *structChild in [structureObj children]){
        [[RealmManager sharedInstance] removeStructureObject:structChild];
    }
    [_realm deleteObject:structureObj];
}

- (StructureObject *)createStructuredObjectWithData:(NSDictionary *)downloadedStructureDictionary
{
    StructureObject *newStructured;
    NSLog(@"requested object is not present on Realm. Creating a new one...");
    if(![_realm inWriteTransaction])
    [_realm beginWriteTransaction];
    
    
    //Base Object
    newStructured = [StructureObject new];
    [newStructured setName:[downloadedStructureDictionary objectForKey:OBJ_MESH_JSON]];
    [newStructured setStructureDescription:[downloadedStructureDictionary objectForKey:OBJ_STRUCTURE_DESCRIPTION_JSON]];
    [newStructured setName:[downloadedStructureDictionary objectForKey:OBJ_NAME_JSON]];
    [newStructured setShowLabel:[NSNumber numberWithBool:[[downloadedStructureDictionary objectForKey:OBJ_MESH_JSON] boolValue]]];
    [newStructured setIsAnimation:[NSNumber numberWithBool:[[downloadedStructureDictionary objectForKey:OBJ_IS_ANIMATION_JSON] boolValue]]];
    [newStructured setAnimationTrigger:[downloadedStructureDictionary objectForKey:OBJ_ANIMATION_TRIGGER_JSON]];
    if([[downloadedStructureDictionary allKeys] containsObject:OBJ_STEPS_JSON]){
        NSMutableArray *steps = [NSMutableArray new];
        for(NSString *step in [downloadedStructureDictionary objectForKey:OBJ_STEPS_JSON]){
            StepStructure *stepStructure = [StepStructure new];
            [stepStructure setStepString:step];
            [_realm addOrUpdateObject:stepStructure];
            [steps addObject:stepStructure];
        }
        [newStructured setStepStructures:(RLMArray<StepStructure *><StepStructure> *)steps];
    }
    if([[downloadedStructureDictionary allKeys] containsObject:OBJ_CHILDER_JSON]){
        NSMutableArray *children = [NSMutableArray new];
        for(NSDictionary *child in [downloadedStructureDictionary objectForKey:OBJ_CHILDER_JSON]){
            StructureObject *structureChild = [[RealmManager sharedInstance] createStructuredObjectWithData:child];
            [children addObject:structureChild];
        }
        [newStructured setChildren:(RLMArray<StructureObject *><StructureObject> *)children];
    }

    [_realm addOrUpdateObject:newStructured];
    
    if(![_realm inWriteTransaction])
    [_realm commitWriteTransaction];
    
    return newStructured;
}

@end
