//
//  RealmManager.h
//  OnTheGo
//
//  Created by Alessio Crestani on 16/11/2017.
//  Copyright © 2017 Nokia. All rights reserved.
//

/***********************************************************************
 This is the manager of the Database made with Realm Frameworks.
 Model of the database can be found on Model Group of this application.
 Application will save on the database:
 - Downloaded contents metadata
 - Opened content History
 ***********************************************************************/

#import "DownloadedObject.h"
#import "StepStructure.h"
#import "StructureObject.h"
#import "GlobalImport.h"

@interface RealmManager : NSObject

@property RLMRealm *realm;

// Instance
+ (RealmManager *)sharedInstance;


/**
 @brief Function generate and save a DownloadedObject from NSDictionary to Realm

 @param downloadedObjectDictionary NSDictionary with DownloadedObject data
 @return the created DownloadedObject on Realm
 */
- (DownloadedObject *)createDownloadedObjectWithData:(NSDictionary *)downloadedObjectDictionary;

/**
 @brief Function that find if current obj is already saved on Realm
 
 @param downloadedObjectDictionary NSDictionary with DownloadedObject data
 @return the created DownloadedObject on Realm, if Exists, nil otherwise
 */
- (DownloadedObject *)objectAlreadySavedWithDictionary:(NSDictionary *)objDict;
- (DownloadedObject *)changeURLOfWebObject:(DownloadedObject *)downloadedObject;
- (RLMResults<DownloadedObject *> *)allObjects;
- (RLMResults<DownloadedObject *> *)historyObjects;
- (RLMResults<DownloadedObject *> *)downloadedObjects;
- (RLMResults<DownloadedObject *> *)getChildrenItemsForParentUUID:(NSString *)parentUUID;
- (DownloadedObject *)getDownloadedObjWithUUID:(NSString *)objUUID;
- (void)removeDownloadedObjectForHistory:(DownloadedObject *)downloadedObj;
- (void)removeDownloadedObjectForDownload:(DownloadedObject *)downloadedObj;
- (BOOL)removeDownloadedObjectFromRealm:(DownloadedObject *)downloadedObj withLocalFileDeletion:(BOOL)localFileDeletion;
- (BOOL)removeDownloadedObjectFromLocalDisk:(DownloadedObject *)downloadedObj;



@end
