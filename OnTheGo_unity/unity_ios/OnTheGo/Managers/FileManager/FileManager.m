//
//  FileManager.m
//  OnTheGo
//
//  Created by Alessio Crestani on 16/11/2017.
//  Copyright © 2017 Nokia. All rights reserved.
//

#import "FileManager.h"
#import "GlobalImport.h"

@implementation FileManager

#pragma mark ## Downloaded Object methods ##

+ (NSData *)searchFileOnStorageOfObj:(DownloadedObject *)obj
{
    NSString *path = [FileManager pathForType:(TypeOfile)[[obj type] intValue]];
    NSDirectoryEnumerator *enumerator = [[NSFileManager defaultManager] enumeratorAtPath:path];
    // Enumerate all the files in the folder containing the Realm file
    for (NSString *filePath in enumerator) {
        if([[filePath stringByDeletingPathExtension] isEqualToString:[obj uuid]]){
            if((TypeOfile)[[obj type] intValue] == WEB_OBJ){
                NSDirectoryEnumerator *enumeratorHTML = [[NSFileManager defaultManager] enumeratorAtPath:[path stringByAppendingPathComponent:filePath]];
                for (NSString *filePathHTML in enumeratorHTML) {
                    if([filePathHTML containsString:@"html"])
                        return [[NSFileManager defaultManager] contentsAtPath:[[path stringByAppendingPathComponent:filePath] stringByAppendingPathComponent:filePathHTML]];
                }
                return nil;
            } else if((TypeOfile)[[obj type] intValue] == AR_3D_OBJ){
                NSDirectoryEnumerator *enumeratorWTC = [[NSFileManager defaultManager] enumeratorAtPath:[path stringByAppendingPathComponent:filePath]];
                for (NSString *filePathWTC in enumeratorWTC) {
                    if([[filePathWTC pathExtension] containsString:@"wtc"] || [[filePathWTC pathExtension] containsString:@"2dmap"])
                        return [[NSFileManager defaultManager] contentsAtPath:[[path stringByAppendingPathComponent:filePath] stringByAppendingPathComponent:filePathWTC]];
                }
                return nil;
            } else{
                return [[NSFileManager defaultManager] contentsAtPath:[path stringByAppendingPathComponent:filePath]];
            }
                break;
        }
    }
    
    return nil;
}

+ (BOOL)fileDownloadedForObj:(DownloadedObject *)obj
{
    NSString *path;
    if((TypeOfile)[[obj type] intValue] != WEB_OBJ)
        path = [[[FileManager pathForType:(TypeOfile)[[obj type] intValue]] stringByAppendingPathComponent:[obj uuid]] stringByAppendingPathExtension:[[obj objURL] pathExtension]];
    else
        path = [[FileManager pathForType:(TypeOfile)[[obj type] intValue]] stringByAppendingPathComponent:[obj uuid]];
    return (path != nil && [[NSFileManager defaultManager] fileExistsAtPath:path]);
}

+ (BOOL)thumbDownloadedForObj:(DownloadedObject *)obj
{
    NSString *path = [[[FileManager pathForObjImgFolder] stringByAppendingPathComponent:[obj uuid]] stringByAppendingString:@".png"];
    return (path != nil && [[NSFileManager defaultManager] fileExistsAtPath:path]);
}

+ (UIImage *)getImageForObject:(DownloadedObject *)obj
{
    return [UIImage imageWithData:[[NSFileManager defaultManager] contentsAtPath:[[obj getObjThumb] path]]];
}

#pragma mark ## Object Image methods ##

#pragma mark ## Object File methods ##

+ (NSString *)pathForObjImgFolder
{
    return [FileManager pathForType:IMAGE_OBJ];
}

+ (NSString *)pathForPDFFolder
{
    return [FileManager pathForType:PDF_OBJ];
}

+ (NSString *)pathFor3DModelsFolder
{
    return [FileManager pathForType:MODEL_OBJ];
}

+ (NSString *)pathForARFolder
{
    return [FileManager pathForType:AR_3D_OBJ];
}

+ (NSString *)pathForWebFolder
{
    return [FileManager pathForType:WEB_OBJ];
}

+ (NSString *)pathForVideoFolder
{
    return [FileManager pathForType:VIDEO_OBJ];
}

+ (NSString *)pathForVideo360Folder
{
    return [FileManager pathForType:VIDEO_360_OBJ];
}


+ (NSString *)pathForType:(TypeOfile)type
{
    NSString *typeDirectory = @"";
    switch (type) {
        case PDF_OBJ:
            typeDirectory = @"/PDFS";
            break;
        case VIDEO_OBJ:
            typeDirectory = @"/VIDEOS";
            break;
        case VIDEO_360_OBJ:
            typeDirectory = @"/360VIDEOS";
            break;
        case MODEL_OBJ:
            typeDirectory = @"/3DMODELS";
            break;
        case WEB_OBJ:
            typeDirectory = @"/HTML";
            break;
        case AR_3D_OBJ:
            typeDirectory = @"/AUGMENTED_REALITY";
            break;
        case IMAGE_OBJ:
            typeDirectory = @"IMGS";
            break;
        case UNKNOWN_OBJ:
            typeDirectory = @"";
            break;
    }
    typeDirectory = [typeDirectory stringByAppendingString:@"/"];
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsPath = [paths objectAtIndex:0];
    NSString *dataPath = [documentsPath stringByAppendingPathComponent:typeDirectory];
    if(![[NSFileManager defaultManager] fileExistsAtPath:dataPath])
        [[NSFileManager defaultManager] createDirectoryAtPath:dataPath withIntermediateDirectories:YES attributes:nil error:nil];
    
    return dataPath;
}

//when a WEB_OBJ cannot be downloaded, just delete the zip file (empty) and open It from internet
+ (BOOL)deleteEmptyWebZipFiles:(DownloadedObject *)obj
{
    NSError *error;
    return [[NSFileManager defaultManager] removeItemAtPath:[[obj getDownloadFilePath] path] error:&error];
}

@end
