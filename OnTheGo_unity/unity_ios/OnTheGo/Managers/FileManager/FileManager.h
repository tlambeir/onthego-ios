//
//  FileManager.h
//  OnTheGo
//
//  Created by Alessio Crestani on 16/11/2017.
//  Copyright © 2017 Nokia. All rights reserved.
//

/******************************************************************************************************************
 This is the Manager to allow application to read, write, create and delete file of contents from the local storage
 ******************************************************************************************************************/

#import "GlobalImport.h"

@interface FileManager : NSObject

#pragma mark ## Downloaded Object methods ##

+ (NSData *)searchFileOnStorageOfObj:(DownloadedObject *)obj;
+ (BOOL)fileDownloadedForObj:(DownloadedObject *)obj;
+ (BOOL)thumbDownloadedForObj:(DownloadedObject *)obj;
+ (UIImage *)getImageForObject:(DownloadedObject *)obj;
+ (BOOL)deleteEmptyWebZipFiles:(DownloadedObject *)obj;
#pragma mark ## Object Image methods ##

#pragma mark ## Object File methods ##

+ (NSString *)pathForObjImgFolder;
+ (NSString *)pathForPDFFolder;
+ (NSString *)pathFor3DModelsFolder;
+ (NSString *)pathForARFolder;
+ (NSString *)pathForWebFolder;
+ (NSString *)pathForVideoFolder;
+ (NSString *)pathForVideo360Folder;

@end
