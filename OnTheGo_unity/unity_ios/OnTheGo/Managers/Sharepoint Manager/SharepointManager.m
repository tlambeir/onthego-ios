//
//  SharepointManager.m
//  OnTheGo
//
//  Created by Alessio Crestani on 24/11/2017.
//  Copyright © 2017 Nokia. All rights reserved.
//

#import "SharepointManager.h"

@implementation SharepointManager

// You will set your application's clientId
NSString * const kClientId = @"1ce7be68-1a30-4fa5-adb2-ae998bd24f85";

NSString * const AUTHENTICATION_RESOURCE_ID = @"https://graph.microsoft.com";
NSString * const AUTHORITY_URL = @"https://login.microsoftonline.com/common";
NSString * const REDIRECT_URI = @"http://localhost";
NSString * const kScopes = @"https://graph.microsoft.com/User.Read, https://graph.microsoft.com/Mail.ReadWrite, https://graph.microsoft.com/Mail.Send, https://graph.microsoft.com/Files.ReadWrite";




@end
