//
//  SharepointManager.h
//  OnTheGo
//
//  Created by Alessio Crestani on 24/11/2017.
//  Copyright © 2017 Nokia. All rights reserved.
//

/**
 * Used to manage signin and download of sharepoint and microsoft contents.
 **/


#import <Foundation/Foundation.h>

@interface SharepointManager : NSObject


@end
