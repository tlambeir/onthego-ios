//
//  NetworkManager.m
//  OnTheGo
//
//  Created by Alessio Crestani on 16/11/2017.
//  Copyright © 2017 Nokia. All rights reserved.
//

#import "NetworkManager.h"
#import "AFURLSessionManager.h"
#import "AFHTTPSessionManager.h"
#import "AFURLSessionManager.h"
#import <ADAL/ADAL.h>

#import "GlobalImport.h"

@implementation NetworkManager

//retrieve a new object from an URL
+ (void)getObjectWithURL:(NSString *)urlString withCompletion:(void (^)(NSURLSessionDataTask * task, id responseObject))completionSuccess andFailure:(void (^)(NSURLSessionDataTask * task, NSError * error))completionFailure
{
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc]initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [manager GET:urlString parameters:nil progress:nil success:completionSuccess failure:completionFailure];
    
}

//download a new object
+ (void)downloadDataFromURL:(NSURL *)downloadedObjURL withFilePath:(NSURL *)filePath withFileType:(TypeOfile)fileType withProgress:(void (^)(NSProgress * _Nonnull downloadProgress))progress withCompletion:(void (^)(NSURLResponse *response, NSURL *filePath, NSError *error))completion
{
    NSLog([NSString stringWithFormat: @"Downloading with filepath '%@'.", filePath], nil);
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:configuration];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:downloadedObjURL];
   
    NSURLSessionDownloadTask *downloadTask = [manager downloadTaskWithRequest:request progress:progress destination:^NSURL *(NSURL *targetPath,NSURLResponse *response) {
        return filePath;
    } completionHandler:completion];
    
   
    [downloadTask resume];
}

+ (void)downloadSharePointFileWithBaseURL:(NSString *)baseURL withFileType:(TypeOfile)fileType andMSToken:(NSString *)tokenID andSuccessfulBlock:(void (^)(NSString* downloadURL))successBlock andFailureBlock:(void (^)(NSString* errorDescription))failureBlock
{
    NSString *siteName = [[baseURL pathComponents] objectAtIndex:3];
    NSArray* pathComponents = [baseURL pathComponents];
    NSString *filePath = @"";
    if ([pathComponents count] > 2) {
        NSArray* lastPart = [pathComponents subarrayWithRange:NSMakeRange(5,[pathComponents count] - 5)];
        filePath = [NSString pathWithComponents:lastPart];
    }
    if(siteName == nil || [siteName isEqualToString:@""] || filePath == nil || [filePath isEqualToString:@""]){
        failureBlock(NSLocalizedString(@"Qr Data has some problem. Please retry", nil));
        return;
    }
    
    [NetworkManager getAppIDWithToken:tokenID withSite:siteName andCompletionBlock:^(NSString * _Nullable appID, NSString * _Nullable errorLog) {
        if(appID != nil && ![appID isEqualToString:@""]){
            [NetworkManager getFileIDWithTokenID:tokenID andDrive:appID andFilePath:filePath andCompletionBlock:^(NSString * _Nullable downloadableURL) {
                if(downloadableURL != nil && ![downloadableURL isEqualToString:@""])
                    successBlock(downloadableURL);
                else
                    failureBlock([NSString stringWithFormat:@"Error while searching the file on SharePoint. Please retry or contact your administrator. \n %@", errorLog]);
            }];
        } else{
            failureBlock([NSString stringWithFormat:@"Sharepoint requested appears to be invalid. Please contact your IT administrator \n %@", errorLog]);
        }
    }];
}

+ (void)getTokenForMSGraph:(void (^)(NSString* _Nullable token))completionBlock
{
    ADAuthenticationError *error = nil;
    ADAuthenticationContext *authContext = [ADAuthenticationContext authenticationContextWithAuthority:SHAREPOINT_AUTHORITY_ID error:&error];
    
    [authContext setCredentialsType:AD_CREDENTIALS_AUTO];
    
    [authContext acquireTokenWithResource:SHAREPOINT_RESOURCE_ID
                                  clientId:SHAREPOINT_CLIENT_ID                          // Comes from App Portal
                               redirectUri:[NSURL URLWithString:SHAREPOINT_REDIRECT_URI] // Comes from App Portal
                           completionBlock:^(ADAuthenticationResult *result)
     {
         [[NSOperationQueue mainQueue] addOperationWithBlock:^ {
             if (AD_SUCCEEDED != result.status)
                 completionBlock(nil);
             
             else
                 completionBlock(result.accessToken);
             
             
         }];
         
     }];
}

+ (void)forceNewTokenForMSGraph:(void (^)(NSString* _Nullable token))completionBlock
{
    ADAuthenticationError *error = nil;
    ADAuthenticationContext *authContext = [ADAuthenticationContext authenticationContextWithAuthority:@"https://login.microsoftonline.com/common" error:&error];
    [authContext acquireTokenWithResource:SHAREPOINT_RESOURCE_ID
                                 clientId:SHAREPOINT_CLIENT_ID   // Comes from App Portal
                              redirectUri:[NSURL URLWithString:SHAREPOINT_REDIRECT_URI] // Comes from App Portal
                          completionBlock:^(ADAuthenticationResult *result)
     {
         if (AD_SUCCEEDED != result.status)
             completionBlock(nil);
         else{
             //[PreferencesManager saveMSLastUserID:result.tokenCacheStoreItem.userInformation.userId];
             completionBlock(result.accessToken);
         }
     }];
}

+ (void)getAppIDWithToken:(NSString *)tokenID withSite:(NSString *)site andCompletionBlock:(void (^)(NSString* _Nullable appID, NSString* _Nullable errorLog))completionBlock
{
    NSString *log = @"";
    NSString *url = [NSString stringWithFormat:@"https://graph.microsoft.com/v1.0/sites/%@.sharepoint.com:/sites/%@:/drive", SHAREPOINT_TENANT, site];
    log = [NSString stringWithFormat:@"url to request: %@ \n", url];
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc]initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    [manager.requestSerializer setValue:tokenID forHTTPHeaderField:@"Authorization"];
    [manager GET:url parameters:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSLog(@"SUCCESS");
        NSString *siteID = [responseObject objectForKey:@"id"];
        if(siteID != nil && ![siteID isEqualToString:@""]){
            NSString *newLog = [log stringByAppendingString:[NSString stringWithFormat:@"\n siteID: %@", siteID]];
            completionBlock(siteID, newLog);
        } else
            completionBlock(nil, log);
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSString *newLog = [log stringByAppendingString:[NSString stringWithFormat:@"\n failure errorDescription: %@", error.localizedDescription]];
        completionBlock(nil, newLog);
    }];
}

+ (void)getFileIDWithTokenID:(NSString *)tokenID andDrive:(NSString *)drive andFilePath:(NSString *)filePath andCompletionBlock:(void (^)(NSString* _Nullable downloadableURL))completionBlock
{
    NSString *url = [NSString stringWithFormat:@"https://graph.microsoft.com/v1.0/drives/%@/root:/%@", drive, filePath];
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc]initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    NSMutableCharacterSet *chars = NSCharacterSet.URLQueryAllowedCharacterSet.mutableCopy;
    [chars removeCharactersInRange:NSMakeRange('&', 1)]; // %26
    url = [url stringByAddingPercentEncodingWithAllowedCharacters:chars];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    [manager.requestSerializer setValue:tokenID forHTTPHeaderField:@"Authorization"];
    [manager GET:url parameters:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSLog(@"SUCCESS");
        if([responseObject objectForKey:@"@microsoft.graph.downloadUrl"] != nil && ![[responseObject objectForKey:@"@microsoft.graph.downloadUrl"] isEqualToString:@""])
            completionBlock([responseObject objectForKey:@"@microsoft.graph.downloadUrl"]);
        else
            completionBlock(nil);
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        completionBlock(nil);
    }];
}


+ (void)removeAuthTokenForMicrosoftGraph
{
//    ADAuthenticationError *error = nil;
//    ADAuthenticationContext *authContext = [ADAuthenticationContext authenticationContextWithAuthority:@"https://login.microsoftonline.com/common" error:&error];
//    [authContext.tokenCacheStore removeAllWithError:&error];
//    authContext.tokenCacheStore = nil;
//    
//    NSHTTPCookie *cookie;
//    
//    NSHTTPCookieStorage *storage = [NSHTTPCookieStorage sharedHTTPCookieStorage];
//    for (cookie in [storage cookies])
//    {
//        [storage deleteCookie:cookie];
//    }
}
    
@end
