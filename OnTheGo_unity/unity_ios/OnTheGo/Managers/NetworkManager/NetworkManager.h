//
//  NetworkManager.h
//  OnTheGo
//
//  Created by Alessio Crestani on 16/11/2017.
//  Copyright © 2017 Nokia. All rights reserved.
//

/***********************************************************************************************
 This is the manager of all the network calls of the application.
 When a QR code has been scanned, Network Manager will call the API/URL and download the content
 ***********************************************************************************************/

#import "GlobalImport.h"


@interface NetworkManager : NSObject

+ (void)getObjectWithURL:(NSString *_Nullable)urlString withCompletion:(void (^)(NSURLSessionDataTask * _Nonnull task, id responseObject))completionSuccess andFailure:(void (^)(NSURLSessionDataTask * task, NSError * error))completionFailure;
+ (void)downloadDataFromURL:(NSURL *)downloadedObjURL withFilePath:(NSURL *)filePath withFileType:(TypeOfile)fileType withProgress:(void (^_Nonnull)(NSProgress * _Nonnull downloadProgress))progress withCompletion:(void (^_Nullable)(NSURLResponse * _Nullable response, NSURL *filePath, NSError * _Nullable error))completion;
+ (void)downloadSharePointFileWithBaseURL:(NSString *)baseURL withFileType:(TypeOfile)fileType andMSToken:(NSString *)tokenID andSuccessfulBlock:(void (^)(NSString* downloadURL))successBlock andFailureBlock:(void (^)(NSString* errorDescription))failureBlock;
+ (void)getTokenForMSGraph:(void (^)(NSString* _Nullable token))completionBlock;
+(void)removeAuthTokenForMicrosoftGraph;
@end
