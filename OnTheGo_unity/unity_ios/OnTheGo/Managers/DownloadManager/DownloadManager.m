//
//  DownloadManager.m
//  Unity-iPhone
//
//  Created by Alessio Crestani on 24/07/2018.
//

#import "DownloadManager.h"
#import "FileManager.h"

static DownloadManager *sharedInstance = nil;

typedef enum {
    ALREADY_DOWNLOADED,
    SHAREPOINT,
    REGULAR_DOWNLOAD,
    NO_DOWNLOAD
} CheckDownloadResult;

@implementation DownloadManager

#pragma mark ## DownloadManager Base methods ##

+ (DownloadManager *)sharedInstance
{
    if (sharedInstance == nil){
        sharedInstance = [super new];
        sharedInstance.isDownloading = NO;
        sharedInstance.delegates = [NSMutableArray new];

    }
    
    return sharedInstance;
}

#pragma mark - Delegate Managements

- (void)addNewDownloadDelegate:(id<DownloadManagerDelegate>)delegate
{
    if(_delegates == nil)
        _delegates = [NSMutableArray new];
    if(![_delegates containsObject:delegate])
        [_delegates addObject:delegate];
}

- (void)removeDownloadDelegate:(id<DownloadManagerDelegate>)delegate
{
    if(_delegates != nil)
        [_delegates removeObject:delegate];
}

#pragma mark - class methods

- (void)downloadObjects:(NSMutableArray *)objectToDownload
{
    if(!_isDownloading){
        _isDownloading = YES;
        if(objectToDownload.count > 0){
            for (id<DownloadManagerDelegate> delegate in sharedInstance.delegates) {
                if([((NSObject*)delegate) respondsToSelector:@selector(startDownloadingObjectsWithTotal:)])
                    [delegate startDownloadingObjectsWithTotal:(int)objectToDownload.count];
            }
            [self proceedSavingProcedureWithDownloadedObjects:objectToDownload];
        }
    } else {
        [self callDelegateForDownloadEndedWithSuccess:NO withErrorString:NSLocalizedString(@"ontheGO is already downloading", nil)];
    }
}

- (CheckDownloadResult)haveToDownloadFile:(DownloadedObject *)obj
{
    if(![FileManager fileDownloadedForObj:obj]){
        if([[obj storageType] isEqualToString:STORAGE_TYPE_SHAREPOINT]){
            return SHAREPOINT;
        } else if((TypeOfile)[[obj type] intValue] == AGGREGATED_OBJ || (TypeOfile)[[obj type] intValue] == WBT_OBJ || (TypeOfile)[[obj type] intValue] == URL_OBJ) {
            return NO_DOWNLOAD;
        } else {
            NSLog(@"File not downloaded yet. Proceeding with download...", nil);
            return REGULAR_DOWNLOAD;
        }
    } else{
        return ALREADY_DOWNLOADED;
    }
}

- (void)proceedSavingProcedureWithDownloadedObjects:(NSMutableArray *)objects
{
    switch ([self haveToDownloadFile:[objects firstObject]]) {
        case SHAREPOINT:
            [self proceedWithSharePoint:objects];
            break;
        case NO_DOWNLOAD:
        case ALREADY_DOWNLOADED:
            [objects removeObject:[objects firstObject]];
            [self proceedSavingProcedureWithDownloadedObjects:objects];
            break;
        case REGULAR_DOWNLOAD:
            [self downloadObjectFiles:objects andOptionalSharepointTemporaryURL:nil];
            break;
        
        default:
            break;
    }
}

- (void)proceedWithSharePoint:(NSMutableArray *)objects
{
    DownloadedObject *downloadedObj = [objects firstObject];
    NSString *objID = [downloadedObj uuid];
    [NetworkManager getTokenForMSGraph:^(NSString * _Nullable token) {
        dispatch_async(dispatch_get_main_queue(), ^{
            DownloadedObject *obj = [[RealmManager sharedInstance] getDownloadedObjWithUUID:objID];
            if(token != nil && ![token isEqualToString:@""]){ //token OK
                [SVProgressHUD showWithStatus:NSLocalizedString(@"Downloading content", nil)];
                NSString *sharepointBaseURL;
                if(![SHAREPOINT_TENANT isEqualToString:@"si14spa615"]){
                    sharepointBaseURL = [obj objURL];
                } else {
                    sharepointBaseURL = [[[[obj objURL] stringByReplacingOccurrencesOfString:@"nokia" withString:@"si14spa615"] stringByReplacingOccurrencesOfString:@"testingonthego" withString:@"testOnTheGo"] stringByReplacingOccurrencesOfString:@"Shared%20Documents" withString:@"Documenti%20Condivisi"];
                }
                [NetworkManager downloadSharePointFileWithBaseURL:sharepointBaseURL withFileType:[[obj type] intValue] andMSToken:token andSuccessfulBlock:^(NSString *downloadURL) {
                    NSLog(@"Sharepoint download success");
                    [self downloadObjectFiles:objects andOptionalSharepointTemporaryURL:downloadURL];
                } andFailureBlock:^(NSString *errorDescription) {
                    NSLog(@"%@", errorDescription);
                    [self callDelegateForDownloadEndedWithSuccess:NO withErrorString:NSLocalizedString(@"Fail to retrieve file from sharepoint", nil)];
                }];
            } else{
                [self callDelegateForDownloadEndedWithSuccess:NO withErrorString:NSLocalizedString(@"Sharepoint Auth system fails", nil)];
            }
        });
    }];
}


- (void)downloadObjectFiles:(NSMutableArray *)objects andOptionalSharepointTemporaryURL:(NSString *)sharepointDownloadURL
{
    DownloadedObject *downloadedObj = [objects firstObject];
    NSString *fileURLToDownload = sharepointDownloadURL == nil ? [downloadedObj objURL] : sharepointDownloadURL;
    [NetworkManager downloadDataFromURL:[NSURL URLWithString:fileURLToDownload] withFilePath:[downloadedObj getDownloadFilePath] withFileType:[[downloadedObj type] intValue] withProgress:^(NSProgress * _Nonnull downloadProgress) {
        [SVProgressHUD showProgress:downloadProgress.fractionCompleted status:NSLocalizedString(@"Downloading content", nil)];
    } withCompletion:^(NSURLResponse *response, NSURL *filePath, NSError *error) {
        if(error == nil || [[downloadedObj type] intValue] == WEB_OBJ){
            if(error != nil && [[downloadedObj type] intValue] == WEB_OBJ){ //when the object is HTML but can't download, just save the original URL and open from Online
                [FileManager deleteEmptyWebZipFiles:downloadedObj];
                [[RealmManager sharedInstance] changeURLOfWebObject:downloadedObj];
            }
            NSLog(@"File Downloaded. Proceeding with thumbs", nil);
            [Utilities manageOperationsOnDownloadedFiles:downloadedObj withCompletion:^(BOOL success) {
                if(success)
                    [self proceedWithThumbDownload:downloadedObj];
                else
                    [self callDelegateForDownloadEndedWithSuccess:NO withErrorString:NSLocalizedString(@"Some error occours. Please retry.", nil)];
            }];
        } else{
            [self callDelegateForDownloadEndedWithSuccess:NO withErrorString:NSLocalizedString(@"Some error occours. Please retry.", nil)];

        }
    }];
}

- (void)proceedWithThumbDownload:(DownloadedObject *)downloadedObj
{
    if(![FileManager thumbDownloadedForObj:downloadedObj] && ([downloadedObj thumbURL] != nil) && ![[downloadedObj thumbURL] isEqualToString:@""]){
        NSLog(@"Thumb not downloaded yet. Proceeding with download...", nil);
        [NetworkManager downloadDataFromURL:[NSURL URLWithString:[downloadedObj thumbURL]] withFilePath:[downloadedObj getObjThumb] withFileType:IMAGE_OBJ withProgress:^(NSProgress * _Nonnull downloadProgress) {} withCompletion:^(NSURLResponse *response, NSURL *filePath, NSError *error) {
            [SVProgressHUD dismiss];
            if(error == nil){
                NSLog(@"Thumb Downloaded. Opening file...", nil);
                [self callDelegateForDownloadEndedWithSuccess:YES withErrorString:nil];
            } else{
                NSLog([NSString stringWithFormat:@"Thumb Download error. Error:%@ Aborting....", [error localizedDescription]], nil);
                [SVProgressHUD showErrorWithStatus:NSLocalizedString(@"Some error occours. Please retry.", nil)];
            }
        }];
    } else {
        NSLog(@"Thumb already downloaded. Proceeding...", nil);
        [self callDelegateForDownloadEndedWithSuccess:YES withErrorString:nil];
    }
}

- (void)callDelegateForSingleObjEndedwithRemaining:(int)remaining andTotal:(int)total
{
    for (id<DownloadManagerDelegate> delegate in sharedInstance.delegates) {
        if([((NSObject*)delegate) respondsToSelector:@selector(downloadedObjWithRemainingCount:andStartingTotal:)])
            [delegate downloadedObjWithRemainingCount:remaining andStartingTotal:total];
    }
}

- (void)callDelegateForDownloadEndedWithSuccess:(BOOL)success withErrorString:(NSString *)errorString
{
    sharedInstance.isDownloading = NO;
    for (id<DownloadManagerDelegate> delegate in sharedInstance.delegates) {
        if([((NSObject*)delegate) respondsToSelector:@selector(downloadEndWithSuccess:andLocalizedError:)])
            [delegate downloadEndWithSuccess:success andLocalizedError:errorString];
    }
}

@end
