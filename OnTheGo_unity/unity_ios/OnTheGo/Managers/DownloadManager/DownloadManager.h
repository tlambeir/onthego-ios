//
//  DownloadManager.h
//  Unity-iPhone
//
//  Created by Alessio Crestani on 24/07/2018.
//

#import <Foundation/Foundation.h>

@protocol DownloadManagerDelegate

@optional
- (void)startDownloadingObjectsWithTotal:(int)totalObjsToDownload;
- (void)downloadedObjWithRemainingCount:(int)remainObjToDownload andStartingTotal:(int)total;
- (void)downloadEndWithSuccess:(BOOL)success andLocalizedError:(NSString *)error;
@end

@interface DownloadManager : NSObject

// Instance
+ (DownloadManager *)sharedInstance;

@property(nonatomic, strong) NSMutableArray<id<DownloadManagerDelegate>> *delegates;
@property (nonatomic) BOOL isDownloading;

- (void)downloadObjects:(NSMutableArray *)objectToDownload;
- (void)addNewDownloadDelegate:(id<DownloadManagerDelegate>)delegate;
- (void)removeDownloadDelegate:(id<DownloadManagerDelegate>)delegate;

@end
