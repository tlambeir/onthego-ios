//
//  PreferenceManager.h
//  OnTheGo
//
//  Created by Alessio Crestani on 24/11/2017.
//  Copyright © 2017 Nokia. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PreferencesManager : NSObject

+ (NSString *)getIntroSeen;
+ (void)setIntroSeen;

+ (BOOL)getGDPRAccepted;
+ (void)setGDPRAccepted;

+ (void)saveMSLastUserID:(NSString *)userID;
+ (NSString *)getMSLastUserID;

@end
