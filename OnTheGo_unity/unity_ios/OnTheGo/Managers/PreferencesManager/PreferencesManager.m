//
//  PreferenceManager.m
//  OnTheGo
//
//  Created by Alessio Crestani on 24/11/2017.
//  Copyright © 2017 Nokia. All rights reserved.
//

#import "PreferencesManager.h"
#import "GlobalImport.h"

@implementation PreferencesManager

+ (NSString *)getIntroSeen
{
    return [[NSUserDefaults standardUserDefaults] stringForKey:INTRO_SEEN_PREF];
}

+ (void)setIntroSeen
{
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:INTRO_SEEN_PREF];
}

+ (BOOL)getGDPRAccepted
{
    return [[NSUserDefaults standardUserDefaults] stringForKey:GDPR_SEEN_PREF];
}

+ (void)setGDPRAccepted
{
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:GDPR_SEEN_PREF];
}

+ (void)saveMSLastUserID:(NSString *)userID
{
    [[NSUserDefaults standardUserDefaults] setObject:userID forKey:MS_USER_LOGIN];
}

+ (NSString *)getMSLastUserID
{
    return [[NSUserDefaults standardUserDefaults] stringForKey:MS_USER_LOGIN];
}

@end
