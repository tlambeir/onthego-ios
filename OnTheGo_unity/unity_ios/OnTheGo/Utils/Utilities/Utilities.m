//
//  Utilities.m
//  OnTheGo
//
//  Created by Alessio Crestani on 16/11/2017.
//  Copyright © 2017 Nokia. All rights reserved.
//

#import "Utilities.h"
#import "GlobalImport.h"

@implementation Utilities

+(NSString *)getStringFileTypeForType:(TypeOfile)type
{
    switch (type) {
        case PDF_OBJ:
            return NSLocalizedString(@"PDF", nil);
        case WEB_OBJ:
            return NSLocalizedString(@"HTML", nil);
        case AR_3D_OBJ:
            return NSLocalizedString(@"Unity", nil);
        case MODEL_OBJ:
            return NSLocalizedString(@"3D Model", nil);
        case VIDEO_OBJ:
            return NSLocalizedString(@"Video", nil);
        case VIDEO_360_OBJ:
            return NSLocalizedString(@"360Video", nil);
        case WBT_OBJ:
            return NSLocalizedString(@"wbt", nil);
        default:
            return NSLocalizedString(@"Unknown", nil);
    }
}

+ (TypeOfile)getTypeFromString:(NSString *)downloadedString withStorageType:(NSString *)storageType
{
    if([downloadedString isEqualToString:@"pdf"]){
        return PDF_OBJ;
    } else if([downloadedString isEqualToString:@"html5"]){
        return WEB_OBJ;
    } else if([downloadedString isEqualToString:@"video"]){
        return VIDEO_OBJ;
    } else if([downloadedString isEqualToString:@"threesixty"]){
        return VIDEO_360_OBJ;
    } else if([downloadedString isEqualToString:@"unity"]){
        return AR_3D_OBJ;
    } else if([downloadedString isEqualToString:@"wbt"]){
        return WBT_OBJ;
    } else if([downloadedString isEqualToString:@"url"]){
        return URL_OBJ;
    } else if([downloadedString isEqualToString:@"aggregated"]){
        return AGGREGATED_OBJ;
    }
    
    return UNKNOWN_OBJ;
}

+(BOOL)hasDownloadedObjTheMinimumDataSet:(NSDictionary *)downloadedObj
{
    NSArray *jsonKeys = [downloadedObj allKeys];
    return  [jsonKeys containsObject:OBJ_TITLE_JSON] && [jsonKeys containsObject:OBJ_DESCRIPTION_JSON] && [jsonKeys containsObject:OBJ_URL_JSON] && [jsonKeys containsObject:OBJ_TYPE_JSON];
}

//manage secondary operation on downloaded files. Example: HTML5 files must be unzipped
+ (void)manageOperationsOnDownloadedFiles:(DownloadedObject *)downloadedObj withCompletion:(void (^)(BOOL success))completionHandler
{
    switch ([[downloadedObj type] intValue]) {
        case WEB_OBJ:{
            if([[NSFileManager defaultManager] fileExistsAtPath:[[downloadedObj getObjFilePath] path]]){
                NSString *zipPath = [[downloadedObj getObjFilePath] path];
                NSString *unzipPath = [[[downloadedObj getObjFilePath] path] stringByDeletingPathExtension];
                [SSZipArchive unzipFileAtPath:zipPath toDestination:unzipPath progressHandler:^(NSString * _Nonnull entry, unz_file_info zipInfo, long entryNumber, long total) {
                    NSLog(@"progressing unzip file", nil);
                } completionHandler:^(NSString * _Nonnull path, BOOL succeeded, NSError * _Nullable error) {
                    BOOL successDeletion = [[NSFileManager defaultManager] removeItemAtPath:zipPath error:nil];
                    if(succeeded){
                        completionHandler(successDeletion);
                    } else
                        completionHandler(succeeded);
                }];
            } else {
                completionHandler(YES);
            }
            break;
        }
        case AR_3D_OBJ:{
            if([[NSFileManager defaultManager] fileExistsAtPath:[[downloadedObj getObjFilePath] path]]){
                NSString *zipPath = [[downloadedObj getObjFilePath] path];
                NSString *unzipPath = [[[downloadedObj getObjFilePath] path] stringByDeletingPathExtension];
                [SSZipArchive unzipFileAtPath:zipPath toDestination:unzipPath progressHandler:^(NSString * _Nonnull entry, unz_file_info zipInfo, long entryNumber, long total) {
                    NSLog(@"progressing unzip file", nil);
                } completionHandler:^(NSString * _Nonnull path, BOOL succeeded, NSError * _Nullable error) {
                    if(succeeded){
                        BOOL successDeletion = [[NSFileManager defaultManager] removeItemAtPath:zipPath error:nil];
                        completionHandler(successDeletion);
                    } else
                    completionHandler(succeeded);
                }];
            } else {
                completionHandler(NO);
            }
            break;
        }
        default:
            completionHandler(YES);
            break;
    }
}

+ (BOOL)stringIsAValidURL:(NSString *)string
{
    NSURLRequest *req = [NSURLRequest requestWithURL:[NSURL URLWithString:string]];
    return [NSURLConnection canHandleRequest:req];
}

+ (void)cleanApplicationFromObjectsDEBUG
{
    RLMResults *result = [[RealmManager sharedInstance] allObjects];
    for(DownloadedObject *obj in result){
        [[RealmManager sharedInstance] removeDownloadedObjectFromRealm:obj withLocalFileDeletion:YES];
    }
}

@end
