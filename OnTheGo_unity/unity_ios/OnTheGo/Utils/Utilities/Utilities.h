//
//  Utilities.h
//  OnTheGo
//
//  Created by Alessio Crestani on 16/11/2017.
//  Copyright © 2017 Nokia. All rights reserved.
//

/************************************************
 Common static function shared across application
 ************************************************/

#import "GlobalImport.h"


@interface Utilities : NSObject

+ (NSString *)getStringFileTypeForType:(TypeOfile)type;
+ (TypeOfile)getTypeFromString:(NSString *)downloadedString withStorageType:(NSString *)storageType;
+ (BOOL)hasDownloadedObjTheMinimumDataSet:(NSDictionary *)downloadedObj;
+ (void)manageOperationsOnDownloadedFiles:(DownloadedObject *)downloadedObj withCompletion:(void (^)(BOOL success))completionHandler;
+ (BOOL)stringIsAValidURL:(NSString *)string;
+ (void)cleanApplicationFromObjectsDEBUG;

@end
