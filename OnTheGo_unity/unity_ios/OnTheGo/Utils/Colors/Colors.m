//
//  Colors.m
//  OnTheGo
//
//  Created by Alessio Crestani on 16/11/2017.
//  Copyright © 2017 Nokia. All rights reserved.
//

#import "Colors.h"

@implementation Colors

+(UIColor *)blueNokiaColor
{
    return [UIColor colorWithRed:18.0f/255.0f green:65.0f/255.0f blue:145.0f/255.0f alpha:1.0f];
}

@end
