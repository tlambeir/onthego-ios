//
//  Colors.h
//  OnTheGo
//
//  Created by Alessio Crestani on 16/11/2017.
//  Copyright © 2017 Nokia. All rights reserved.
//

/********************************************************
 Common static calss that hosts colors of the application
 ********************************************************/


@interface Colors : NSObject

+(UIColor *)blueNokiaColor;

@end
