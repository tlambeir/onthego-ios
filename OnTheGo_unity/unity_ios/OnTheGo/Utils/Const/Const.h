//
//  Const.h
//  OnTheGo
//
//  Created by Alessio Crestani on 16/11/2017.
//  Copyright © 2017 Nokia. All rights reserved.
//


/*************************************************
 Common static constants shared across application
 *************************************************/

#ifndef Const_h
#define Const_h

#define IS_IPAD UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad
#define IS_IPHONE !IS_IPAD


//ENUMS

typedef enum {
    TYPE_LEARNING_STORE,
    TYPE_HISTORY,
    TYPE_TUTORIAL
} TypeOfOpening;

typedef enum {
    PDF_OBJ,
    MODEL_OBJ,
    AR_3D_OBJ,
    WEB_OBJ,
    VIDEO_OBJ,
    VIDEO_360_OBJ,
    UNKNOWN_OBJ,
    IMAGE_OBJ,
    WBT_OBJ,
    URL_OBJ,
    AGGREGATED_OBJ
} TypeOfile;

//unity

#define UNITY_INIT_MESSAGE @"Init"
#define UNITY_INIT_SCENE_MESSAGE @"InitSceneLoad"
#define UNITY_OBJECT_FILES_SUBFOLDER @"IOS"
#define UNITY_2DMAP_EXTENSION @"2dmap"
#define UNITY_WTC_EXTENSION @"wtc"

//NETWORK ENDPOINT

//SI14 TEST Sharepoint

/**
#define SHAREPOINT_TENANT @"si14spa615"
#define SHAREPOINT_CLIENT_ID @"a3874070-f389-4d5b-a8e7-e3db0bfc549e"
#define SHAREPOINT_RESOURCE_ID @"https://graph.microsoft.com"
#define SHAREPOINT_REDIRECT_URI @"nokiaonthego://com.nokia.learningonthego"
#define SHAREPOINT_AUTHORITY_ID @"https://login.microsoftonline.com/common"
**/


//Nokia Sharepoint Endpoints
#define SHAREPOINT_TENANT @"nokia"
#define SHAREPOINT_CLIENT_ID @"1ce7be68-1a30-4fa5-adb2-ae998bd24f85"
#define SHAREPOINT_RESOURCE_ID @"https://graph.microsoft.com"
#define SHAREPOINT_REDIRECT_URI @"onthego://com.nokia.learningonthego"
#define SHAREPOINT_AUTHORITY_ID @"https://login.microsoftonline.com/common"


//PREFERENCES ON DEFAULT MANAGER
#define GDPR_SEEN_PREF @"GDPR_SEEN"
#define INTRO_SEEN_PREF @"INTRO_SEEN"
#define MS_USER_LOGIN @"MS_USER_LOGIN"

//EXAMPLES
#define PDF_EXAMPLE @"http://ec2-52-51-31-225.eu-west-1.compute.amazonaws.com/nokiaqr-content/api/6.json"
#define VIDEO_EXAMPLE @"http://ec2-52-51-31-225.eu-west-1.compute.amazonaws.com/nokiaqr-content/api/1.json"
#define AR_EXAMPLE @"http://ec2-52-51-31-225.eu-west-1.compute.amazonaws.com/nokiaqr-content/api/3.json"
#define AIRSCALE_EXAMPLE @"http://ec2-52-51-31-225.eu-west-1.compute.amazonaws.com/nokiaqr-content-alpha/api/5.json"
#define OBJ_3D_INSPECTOR_EXAMPLE @"http://ec2-52-51-31-225.eu-west-1.compute.amazonaws.com/nokiaqr-content/api/8.json"
#define MICROCONTENT_EXAMPLE @"http://ec2-52-51-31-225.eu-west-1.compute.amazonaws.com/nokiaqr-content/api/2.json"
#define WBT_CONTENT_EXAMPLE @"http://ec2-52-51-31-225.eu-west-1.compute.amazonaws.com/nokiaqr-content/api/7.json"
#define OBJ_3D_INSPECTOR_EXAMPLE_2 @"http://ec2-52-51-31-225.eu-west-1.compute.amazonaws.com/nokiaqr-content/api/9.json"
#define SHARE_POINT_EXAMPLE @"http://ec2-52-51-31-225.eu-west-1.compute.amazonaws.com/nokiaqr-content/api/10.json"
#define VIDEO_360_EXAMPLE @"http://ec2-52-51-31-225.eu-west-1.compute.amazonaws.com/nokiaqr-content/api/11.json"
#define AR_VIDEO_EXAMPLE @"http://ec2-52-51-31-225.eu-west-1.compute.amazonaws.com/nokiaqr-content/api/12.json"
#define AIRFRAME_INSPECTOR_EXAMPLE @"http://ec2-52-51-31-225.eu-west-1.compute.amazonaws.com/nokiaqr-content/api/15.json"
#define CLOUDBAND_3D_INSPECTOR_EXAMPLE @"http://ec2-52-51-31-225.eu-west-1.compute.amazonaws.com/nokiaqr-content/api/14.json"
#define TAS_3D_INSPECTOR_EXAMPLE @"http://ec2-52-51-31-225.eu-west-1.compute.amazonaws.com/nokiaqr-content/api/13.json"
#define SRE_PCE_3D_INSPECTOR_EXAMPLE @"http://ec2-52-51-31-225.eu-west-1.compute.amazonaws.com/nokiaqr-content/api/16.json"
#define MPLS_3D_INSPECTOR_EXAMPLE @"http://ec2-52-51-31-225.eu-west-1.compute.amazonaws.com/nokiaqr-content/api/3.json"
#define MODEL_7750_INSPECTOR_EXAMPLE @"http://ec2-52-51-31-225.eu-west-1.compute.amazonaws.com/nokiaqr-content/api/17.json"
#define AIRSCALE_3D_INSPECTOR_EXAMPLE @"http://ec2-52-51-31-225.eu-west-1.compute.amazonaws.com/nokiaqr-content/api/5.json"
#define FX8_3D_INSPECTOR_EXAMPLE @"http://ec2-52-51-31-225.eu-west-1.compute.amazonaws.com/nokiaqr-content/api/18.json"


//MISC
#define ZIP_URL_PATH_COMPONENT @"/compressed.zip"
#define IOS_MODEL_PATH_COMPONENT @"/IOS/compressed.zip"

//URLS
#define LEARNING_STORE_URL @"http://learningstore.nokia.com/employee/"
#define TERMS_URL @"https://learningstore.nokia.com/onthego/terms/"
#define PRIVACY_URL @"https://www.nokia.com/en_int/privacy"

//NAME OF SEGUE TO MOVE CONTROLLERS

#define TUTORIAL_SEGUE @"TutorialShowSegue"
#define HISTORY_SEGUE @"ShowHistorySegue"
#define LEARNING_STORE_SEGUE @"ShowLearningSegue"
#define OBJECTS_LIST_SEGUE @"ObjectsListSegue"
#define GDPR_SEGUE @"ShowGDRPSegue"

//JSON for DOWNLOADED OBJECTS NAME-VALUE

#define STORAGE_TYPE_SHAREPOINT @"sharepoint"
#define OBJ_UUID_JSON @"id"
#define OBJ_TITLE_JSON @"title"
#define OBJ_DESCRIPTION_JSON @"description"
#define OBJ_FILE_SIZE_JSON @"file_size"
#define OBJ_THUMB_URL_JSON @"thumb"
#define OBJ_URL_JSON @"url"
#define OBJ_DURATION_JSON @"duration"
#define OBJ_TYPE_JSON @"type"
#define OBJ_APP_TYPE_JSON @"apptype"
#define OBJ_APP_MODEL_JSON @"model"
#define OBJ_STRUCTURE_JSON @"structure"
#define OBJ_STORAGE_TYPE_JSON @"storageType"
#define OBJ_PATH_JSON @"path"
#define OBJ_MAP_PATH_JSON @"mapFilePath"


//JSON for STRUCTURE OBJECTS NAME-VALUE
#define OBJ_UUID_JSON @"id"
#define OBJ_PARENT_UUID_JSON @"parent"
#define OBJ_UUID_QR_GENERATOR_JSON @"_id"
#define OBJ_MESH_JSON @"mesh"
#define OBJ_STRUCTURE_DESCRIPTION_JSON @"Description"
#define OBJ_NAME_JSON @"name"
#define OBJ_SHOWLABEL_JSON @"showLabel"
#define OBJ_IS_ANIMATION_JSON @"isAnimation"
#define OBJ_ANIMATION_TRIGGER_JSON @"animationTrigger"
#define OBJ_STEPS_JSON @"steps"
#define OBJ_CHILDER_JSON @"Children"
#define OBJ_AGGREGATED_CHILDER_JSON @"children"

#endif /* Const_h */
